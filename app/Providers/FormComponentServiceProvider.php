<?php

namespace App\Providers;

use Form;
use Html;
use Illuminate\Support\ServiceProvider;

class FormComponentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Simply Amazing Components
         */
        Form::component(
            'formText',
            'collectives.forms.form_text',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formNumber',
            'collectives.forms.form_number',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true, 'min' => 0]
        );

        Form::component(
            'formPassword',
            'collectives.forms.form_password',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formSelect',
            'collectives.forms.form_select',
            ['name', 'list' => [], 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formContactNumber',
            'collectives.forms.form_contact_number',
            ['name', 'value' => null, 'label' => null, 'prepend' => '', 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formCheckbox',
            'collectives.forms.form_checkbox',
            ['name', 'id', 'value' => null, 'label' => null, 'attributes' => []]
        );

        Form::component(
            'formRadio',
            'collectives.forms.form_radio',
            ['name', 'id', 'value' => null, 'checked' => false, 'label' => null, 'attributes' => []]
        );

        Form::component(
            'formTextArea',
            'collectives.forms.form_textarea',
            ['name', 'value' => null, 'label' => null, 'attributes' => []]
        );

        Form::component(
            'formDate',
            'collectives.forms.form_date',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formDateRange',
            'collectives.forms.form_date_range',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal'=> true]
        );

        Form::component(
            'formTime',
            'collectives.forms.form_time',
            ['name', 'value' => null, 'label' => null, 'attributes' => [], 'isHorizontal' => true]
        );

        Form::component(
            'formLabel',
            'collectives.forms.form_label',
            ['label' => null, 'value' => null]
        );

        Form::component(
            'formHide',
            'collectives.forms.form_hide',
            ['name', 'value' => null, 'label' => null, 'attributes' => []]
        );

        Form::component(
            'formFileUpload',
            'collectives.forms.form_file_upload',
            ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => [], 'isVerticalAligned' => true, 'required' => null, 'labelNote' => null, 'footerNote' => null, 'icon' => null]
        );

        Form::component(
            'formFileUploadThumbnail',
            'collectives.forms.form_file_upload_thumbnail',
            ['name', 'id', 'src' => null, 'label' => null, 'max_size_in_mb' => 0, 'src_big' => null, 'can_upload' => true]
        );

        Form::component(
            'formWidgetInfo',
            'collectives.forms.form_widget_info',
            ['id', 'label', 'value' => null, 'label_class' => 'col-6', 'value_class' => 'col-6', 'dashboard' => false, 'bold' => true]
        );

        Form::component(
            'formTabPrimary',
            'collectives.forms.form_tab_primary',
            ['name', 'url', 'new_tab' => null]
        );

        Form::component(
            'formTabSecondary',
            'collectives.forms.form_tab_secondary',
            ['name', 'url', 'new_tab' => null]
        );

        Form::component(
            'formButtonPrimary',
            'collectives.forms.form_button_primary',
            ['name', 'label', 'type' => 'submit', 'attributes' => []]
        );

        Form::component(
            'formButtonSecondary',
            'collectives.forms.form_button_secondary',
            ['name', 'label', 'type' => 'submit', 'attributes' => []]
        );

        /**
         * Html customs builds
         */
        HTML::component(
            'htmlActiveMenu',
            'collectives.html.html_active_menu',
            ['route' => null, 'class' => 'active']
        );

        HTML::component(
            'setLanguage',
            'collectives.html.set_language',
            ['label' => null, 'languages' => [], 'current_language' => 'en', 'attributes' => [], 'isVerticalAligned' => true, 'labelNote' => null, 'footerNote' => null, 'icon' => null]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
