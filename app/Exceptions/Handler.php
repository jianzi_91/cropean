<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Plus65\Utility\Exceptions\WebException;
use Prophecy\Exception\Doubler\MethodNotFoundException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        NotFoundHttpException::class,
        ValidationException::class,
        MethodNotFoundException::class,
        AuthenticationException::class,
        TokenMismatchException::class,
        MaintenanceModeException::class,
        MethodNotAllowedHttpException::class,
        AuthorizationException::class,

    ];

    /**oic
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (!$this->shouldntReport($exception)) {
            $exception = new WebException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *okya
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof TokenMismatchException) {
            return redirect()->back()
                ->withError(__('s_error.session has expired, please try again'));
        }

        if ($exception instanceof PostTooLargeException) {
            return redirect()->back()
                ->withError(__('s_error.file too large'));
        }

        if ($exception instanceof ValidationException) {
            if ($request->expectsJson()) {
                return $this->returnValidationError($exception);
            }

            return parent::render($request, $exception)->withError(__('s_error.there are some errors with the data submitted'));
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest(url('login?expired=1'));
    }

    private function returnValidationError($exception)
    {
        $validation_errors           = $exception->validator->getMessageBag()->toArray();
        $formatted_validation_errors = [];

        if ($validation_errors) {
            foreach ($validation_errors as $key => $error) {
                $formatted_validation_errors[$key] = $error[0];
            }
        }

        return response()->json(['message' => __('s_validation.validation errors'), 'errors' => $formatted_validation_errors], 400);
    }
}
