<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Campaign\Console\CampaignStatusUpdateCommand;
use Modules\Campaign\Console\CampaignPayoutCommand;
use Modules\Credit\Console\BulkConversionApprovalCommand;
use Modules\Deposit\Console\DepositInvoiceRegenerationCommand;
use Modules\Deposit\Console\ImportDummyDepositsCommand;
use Modules\Promotion\Console\AugustPromotionPayoutCommand;
use Modules\Promotion\Console\JulyPromotionPayoutCommand;
use Modules\Promotion\Console\JunePromotionClearanceCommand;
use Modules\Promotion\Console\JunePromotionPayoutCommand;
use Modules\Promotion\Console\PromotionCheckingCommand;
use Modules\Promotion\Console\ReferralBonusPayoutCommand;
use Modules\Rank\Console\RestoreDefaultRankCommand;
use Modules\Translation\Console\ImportTranslationIntoDBCommand;
use Modules\User\Console\ImportDummyUserCommand;
use Modules\User\Console\TerminateUnqualifiedMembersCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImportTranslationIntoDBCommand::class,
        ImportDummyUserCommand::class,
        DepositInvoiceRegenerationCommand::class,
        TerminateUnqualifiedMembersCommand::class,
        JunePromotionPayoutCommand::class,
        ImportDummyDepositsCommand::class,
        PromotionCheckingCommand::class,
        JunePromotionClearanceCommand::class,
        JulyPromotionPayoutCommand::class,
        AugustPromotionPayoutCommand::class,
        ReferralBonusPayoutCommand::class,
        CampaignStatusUpdateCommand::class,
        CampaignPayoutCommand::class,
        RestoreDefaultRankCommand::class,
        BulkConversionApprovalCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $now                   = now();
        $promotionStartingDate = Carbon::parse('2020-07-01');
        $promotionEndingDate   = Carbon::parse('2020-11-01');

        #campaign status update
        $campaignStatusLogs = storage_path('logs/cron/campaign/status_update_' . date('ymd') . '.log');
        $schedule->command('campaign:update-status')
            ->dailyAt('00:00')
            ->sendOutputTo($campaignStatusLogs);

        #bank account credit snapshot
        $schedule->command('snapshot:bank_account_credits')->dailyAt('00:00');

        #special bonus snapshot payout percentage
        $schedule->command('snapshot:user_special_bonus_percentage')->dailyAt('00:00');

        $schedule->command('snapshot:sponsor_tree')->dailyAt('00:00');

        $schedule->command('activitylog:clean')->daily();

        #promotion daily maintenance
        if ($now->lessThanOrEqualTo($promotionEndingDate)) {
            if ($now->greaterThan($promotionStartingDate)) {
                $promotionLog = storage_path('logs/cron/promotion_' . date('ymd') . '.log');
                $schedule->command('promotions:check')
                    ->dailyAt('00:30')
                    ->sendOutputTo($promotionLog);
            }
        }

        #roi calculate and payout daily
        $roiCalculatePayoutLogs = storage_path('logs/cron/roi_' . date('ymd') . '.log');
        $schedule->command('rebate:payout')
            ->dailyAt('01:00')
            ->sendOutputTo($roiCalculatePayoutLogs);

        #campaign calculate and payout daily
        $campaignPayoutLogs = storage_path('logs/cron/campaign/payout_' . date('ymd') . '.log');
        $schedule->command('campaign:payout')
            ->dailyAt('01:30')
            ->sendOutputTo($campaignPayoutLogs);

        #goldmine rank qualification
        $goldmineRankQualificationLogs = storage_path('logs/cron/goldmine_rank_' . date('ymd') . '.log');
        $schedule->command('rank-qualification:goldmine')
            ->dailyAt('02:00')
            ->sendOutputTo($goldmineRankQualificationLogs);

        #leader bonus rank qualification
        $leaderBonusRankQualificationLogs = storage_path('logs/cron/leader_bonus_rank_' . date('ymd') . '.log');
        $schedule->command('rank-qualification:leader_bonus')
            ->dailyAt('03:00')
            ->sendOutputTo($leaderBonusRankQualificationLogs);

        if (strtolower(config('app.env')) == 'uat' || strtolower(config('app.env')) == 'production') {
            $schedule->command('password:generate_sysadmin')
                ->weekly()
                ->mondays()
                ->at('02:00');

            #special bonus calculate daily
            $specialBonusCalculateLogs = storage_path('logs/cron/special_bonus_calculate_' . date('ymd') . '.log');
            $schedule->command('commissions:special_bonus')
                ->dailyAt('04:30')
                ->sendOutputTo($specialBonusCalculateLogs);

            #user termination cron
            // $usersTerminationLogs = storage_path('logs/cron/users_termination_' . date('ymd') . '.log');
            // $schedule->command('users:terminate')
            //     ->dailyAt('04:30')
            //     ->sendOutputTo($usersTerminationLogs);
        }

        #promotion payout
        $promotionLog = storage_path('logs/cron/promotion_' . date('ymd') . '.log');
        $schedule->command('august-promotion:payout')
            ->cron('0 5 17 8 *') // At 05:00 on  of July
            ->sendOutputTo($promotionLog);

        #promotion end
        $schedule->command('promotions:clear')
            ->dailyAt('05:00')
            ->sendOutputTo($promotionLog);

        $schedule->command('referral-bonus:payout')
            ->dailyAt('05:15');

        //Blockchain parser, please remove those unused from ur system
        $schedule->command('blockchain-parser:process-blocks erc20')
            ->everyMinute()
            ->withoutOverlapping();

        //Blockchain parser update confirmation and issue credits
        $schedule->command('blockchain-parser:update-deposit-confirmations')
            ->everyMinute()
            ->withoutOverlapping();

        //Blockchain parser update withdrawal confirmation
        $schedule->command('blockchain-parser:update-withdrawal-confirmations')
            ->everyMinute()
            ->withoutOverlapping();

        //Blockchain Fee
        $schedule->command('blockchain:update-fees')
            ->everyMinute()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
