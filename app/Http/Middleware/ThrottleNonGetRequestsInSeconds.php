<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Support\Facades\Session;
use Response;

class ThrottleNonGetRequestsInSeconds extends ThrottleRequests
{
    /**
     * The route name that should be excluded from throttle requests
     *
     * @var array
     */
    protected $except = [

    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param int|string $maxAttempts
     * @param int $delaySeconds
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function handle($request, Closure $next, $maxAttempts = 60, $delaySeconds = 60, $prefix = '')
    {
        if (!$request->user() || !in_array($request->getMethod(), ['POST', 'PUT', 'DELETE', 'PATCH']) || $this->inExceptArray($request)) {
            return $next($request);
        }

        $key = $this->resolveRequestSignature($request);

        $maxAttempts = $this->resolveMaxAttempts($request, $maxAttempts);

        if ($this->limiter->tooManyAttempts($key, $maxAttempts)) {
            if ($request->expectsJson()) {
                response()->json([
                    'meta' => [
                        'message' => __('s_error.too_many_requests at the same time, please check if your request has been processed'),
                    ]
                ], 401);
            } else {
                Session::flash('error', __('s_error.too_many_requests at the same time, please check if your request has been processed'));
                return back();
            }
        }

        $this->limiter->hit($key, $delaySeconds);

        $response = $next($request);

        return $this->addHeaders(
            $response,
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts)
        );
    }

    /**
     * Determine if the request has a URI that should pass through CSRFsxxxxxxxccvffgrrtyuiojkm,/ verification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($request->routeIs($except)) {
                return true;
            }
        }

        return false;
    }
}
