<?php

use Illuminate\Database\Seeder;
use Modules\Announcement\Database\Seeders\AnnouncementPermissionsSeeder;
use Modules\Announcement\Database\Seeders\WelcomeMessageVideoTableSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainAddressManagementPermissionTableSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainCreditTypeBankAccountTableSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainCreditTypeTableSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainGasFeeTransactionTypeSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainSettingPermissionTableSeeder;
use Modules\Blockchain\Database\Seeders\BlockchainTransactionTypeDatabaseSeeder;
use Modules\Blockchain\Database\Seeders\CreateWalletForExistingMemberSeeder;
use Modules\Blockchain\Database\Seeders\EthereumGasPriceSettingSeeder;
use Modules\Blockchain\Database\Seeders\Usdc\UsdcSettingTableSeeder;
use Modules\Blockchain\Database\Seeders\Usdc\UsdcTokensSeeder;
use Modules\Blockchain\Database\Seeders\UsdtErc20\UsdtErc20MasterWalletSettingSeeder;
use Modules\Blockchain\Database\Seeders\UsdtErc20\UsdtErc20NodesTableSeeder;
use Modules\Blockchain\Database\Seeders\UsdtErc20\UsdtErc20SettingTableSeeder;
use Modules\Blockchain\Database\Seeders\UsdtErc20\UsdtErc20TokensSeeder;
use Modules\Blockchain\Database\Seeders\ViewBlockchainTransactionsPermissionsTableSeeder;
use Modules\BlockchainDeposit\Database\Seeders\BlockchainDepositPermissionsSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\BlockchainWithdrawalAddressesPermissionTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\BlockchainWithdrawalApprovalPermissionsTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\BlockchainWithdrawalPermissionTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\BlockchainWithdrawalRefundTransactionTypeTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\BlockchainWithdrawalTypesTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\DefaultBlockchainWithdrawalStatusTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\UpdateCurrentBlockchainWithdrawalStatusesTableSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\Usdc\UsdcWithdrawalSettingDatabaseSeeder;
use Modules\BlockchainWithdrawal\Database\Seeders\UsdtErc20\UsdtErc20WithdrawalSettingDatabaseSeeder;
use Modules\Campaign\Database\Seeders\AddScheduledStatusToCampaignStatusesTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignStatusesTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignBankTransactionTypeTableSeeder;
use Modules\Campaign\Database\Seeders\CampaignPermissionsTableSeeder;
use Modules\Commission\Database\Seeders\AddGoldmineRankSettingTableSeeder;
use Modules\Commission\Database\Seeders\AdminManageUserSpecialBonusTableSeeder;
use Modules\Commission\Database\Seeders\CommissionPayoutSettingTableSeeder;
use Modules\Commission\Database\Seeders\CommissionPermissionsSeeder;
use Modules\Commission\Database\Seeders\ExportBreakdownTableSeeder;
use Modules\Commission\Database\Seeders\GoldmineRankAndLevelTableSeeder;
use Modules\Commission\Database\Seeders\GoldmineRankLevelPercentageTableSeeder;
use Modules\Commission\Database\Seeders\MemberRankCommissionManualOverridePermissionTableSeeder;
use Modules\Country\Database\Seeders\CountryDatabaseSeeder;
use Modules\Credit\Database\Seeders\AddBankTransactionTypeTableSeeder;
use Modules\Credit\Database\Seeders\AddedCreditConversionTransactionTypeSeeder;
use Modules\Credit\Database\Seeders\AdminViewMemberBalancePermissionTableSeeder;
use Modules\Credit\Database\Seeders\AlterCurrentConversionsStatusTableSeeder;
use Modules\Credit\Database\Seeders\ConversionPermissionTableSeeder;
use Modules\Credit\Database\Seeders\ConversionRefundTransactionTypeTableSeeder;
use Modules\Credit\Database\Seeders\ConvertibleCreditTypesTableSeeder;
use Modules\Credit\Database\Seeders\CreditAdjustmentListingPermissionTableSeeder;
use Modules\Credit\Database\Seeders\CreditConversionAdminFeeSettingSeeder;
use Modules\Credit\Database\Seeders\CreditConversionPermissionTableSeeder;
use Modules\Credit\Database\Seeders\CreditConversionSettingTableSeeder;
use Modules\Credit\Database\Seeders\CreditConversionStatusTableSeeder;
use Modules\Credit\Database\Seeders\CreditPermissionsSeeder;
use Modules\Credit\Database\Seeders\CreditSeeder;
use Modules\Credit\Database\Seeders\CreditTransferPermissionsSeeder;
use Modules\Credit\Database\Seeders\DefaultConversionSettingTableSeeder;
use Modules\Credit\Database\Seeders\MemberCreditConvertPermissionTableSeeder;
use Modules\Credit\Database\Seeders\PromotionCreditTableSeeder;
use Modules\Credit\Database\Seeders\PromotionTransactionTypeTableSeeder;
use Modules\Credit\Database\Seeders\UpdateAdjustCreditTableSeeder;
use Modules\Credit\Database\Seeders\UpdateCanConvertTableSeeder;
use Modules\Credit\Database\Seeders\UsdtUsdcConversionSettingTableSeeder;
use Modules\Credit\Database\Seeders\UsdtUsdcStatementPermissionTableSeeder;
use Modules\Dashboard\Database\Seeders\AddDashboardKycRequestTableSeeder;
use Modules\Dashboard\Database\Seeders\AdminDashboardDepositsWithdrawalsPermissionsTableSeeder;
use Modules\Dashboard\Database\Seeders\CreateDashboardPermissionTableSeeder;
use Modules\Dashboard\Database\Seeders\MemberUsdtUsdcDashboardCreditPermissionTableSeeder;
use Modules\Dashboard\Database\Seeders\NewAdminDashboardWidgetPermissionTableSeeder;
use Modules\Deposit\Database\Seeders\DepositAttachmentPermissionsSeeder;
use Modules\Deposit\Database\Seeders\DepositBankPermissionsSeeder;
use Modules\Deposit\Database\Seeders\DepositMethodTableSeeder;
use Modules\Deposit\Database\Seeders\DepositPermissionsSeeder;
use Modules\Deposit\Database\Seeders\DepositStatusTableSeeder;
use Modules\Deposit\Database\Seeders\DepositTypeTableSeeder;
use Modules\Deposit\Database\Seeders\EpoaPermissionTableSeeder;
use Modules\Deposit\Database\Seeders\SecurityPasswordSettingSeederTableSeeder;
use Modules\DocumentVerification\Database\Seeders\CreateDocumentStatusesTableSeeder;
use Modules\DocumentVerification\Database\Seeders\CreateDocumentTypesTableSeeder;
use Modules\DocumentVerification\Database\Seeders\DocumentVerificationPermissionTableSeeder;
use Modules\DocumentVerification\Database\Seeders\SeedDocumentVerificationInUserTableSeeder;
use Modules\ExchangeRate\Database\Seeders\AddCreditToExchangeRateTableSeeder;
use Modules\ExchangeRate\Database\Seeders\ExchangeRateDatabaseSeeder;
use Modules\ExchangeRate\Database\Seeders\ExchangeRateMappingsTableSeeder;
use Modules\ExchangeRate\Database\Seeders\ExchangeRatePermissionsSeeder;
use Modules\ExchangeRate\Database\Seeders\UsdtUsdcExchangeRateTableSeeder;
use Modules\Promotion\Database\Seeders\AugustPromotionTableSeeder;
use Modules\Password\Database\Seeders\PasswordPermissionsSeeder;
use Modules\Promotion\Database\Seeders\JulyPromotionTableSeeder;
use Modules\Promotion\Database\Seeders\JunePromotionTableSeeder;
use Modules\Promotion\Database\Seeders\AugustPromotionPermissionTableSeeder;
use Modules\Promotion\Database\Seeders\PopulateExistingMemberReferralMidWithSponsorMidTableSeeder;
use Modules\Promotion\Database\Seeders\ReferralBonusPermissionTableSeeder;
use Modules\Promotion\Database\Seeders\ReferralBonusSettingTableSeeder;
use Modules\Promotion\Database\Seeders\ReferralBonusTransactionTypeTableSeeder;
use Modules\Rank\Database\Seeders\AddRankSettingTableSeeder;
use Modules\Rank\Database\Seeders\DefaultGoldmineRankTableSeeder;
use Modules\Rank\Database\Seeders\DefaultRankTableSeeder;
use Modules\Rbac\Database\Seeders\RbacDatabaseSeeder;
use Modules\Rbac\Database\Seeders\RbacPermissionsSeeder;
use Modules\Rebate\Database\Seeders\RoiManagementPermissionTableSeeder;
use Modules\Rebate\Database\Seeders\RoiTierSettingTableSeeder;
use Modules\Rebate\Database\Seeders\RoiTierTableSeeder;
use Modules\Setting\Database\Seeders\PenaltySettingSeeder;
use Modules\Setting\Database\Seeders\SponsorTreeSettingSeeder;
use Modules\SupportTicket\Database\Seeders\SupportTicketAttachmentTableSeeder;
use Modules\SupportTicket\Database\Seeders\SupportTicketPermissionsSeeder;
use Modules\SupportTicket\Database\Seeders\SupportTicketSeeder;
use Modules\Translation\Database\Seeders\TranslationPermissionsSeeder;
use Modules\Translation\Database\Seeders\TranslatorGroupTableSeeder;
use Modules\Translation\Database\Seeders\TranslatorLanguageSeeder;
use Modules\Translation\Database\Seeders\TranslatorPageTableSeeder;
use Modules\Tree\Database\Seeders\AddUplinePermissionTableSeeder;
use Modules\Tree\Database\Seeders\TreePermissionsSeeder;
use Modules\User\Database\Seeders\AddInitialCropeanClientTableSeeder;
use Modules\User\Database\Seeders\EpoaContractTranslationTableSeeder;
use Modules\User\Database\Seeders\InitialUsersTableSeeder;
use Modules\User\Database\Seeders\MemberQualificationSettingTableSeeder;
use Modules\User\Database\Seeders\SystemsUserTableSeeder;
use Modules\User\Database\Seeders\UserDatabaseSeeder;
use Modules\User\Database\Seeders\UserPermissionsSeeder;
use Modules\User\Database\Seeders\UserSettingSeederTable;
use Modules\Withdrawal\Database\Seeders\AdminMemberManagementBankPermissionsTableSeeder;
use Modules\Withdrawal\Database\Seeders\BulkUpdateWithdrawalStatusTableSeeder;
use Modules\Withdrawal\Database\Seeders\SetWithdrawalTypeToInclusiveTableSeeder;
use Modules\Withdrawal\Database\Seeders\WithdrawalBankNameTableSeeder;
use Modules\Withdrawal\Database\Seeders\WithdrawalBankPermissionsSeeder;
use Modules\Withdrawal\Database\Seeders\WithdrawalDatabaseSeeder;
use Modules\Withdrawal\Database\Seeders\WithdrawalPermissionsSeeder;
use Modules\Withdrawal\Database\Seeders\WithdrawalStatusPageTableSeeder;
use Modules\Promotion\Database\Seeders\PromotionPermissionTableSeeder;

class DatabaseSeeder extends Seeder
{
    use \Plus65\Utility\Models\Concerns\HasSeeder;

    /**
     * Get seeders.
     *
     * @return string[]
     */
    protected function getSeeders()
    {
        return [
            TranslatorLanguageSeeder::class,
            TranslatorGroupTableSeeder::class,
            TranslatorPageTableSeeder::class,
            CountryDatabaseSeeder::class,
            CreditSeeder::class,
            UserDatabaseSeeder::class,
            RbacDatabaseSeeder::class,
            InitialUsersTableSeeder::class,
            SupportTicketSeeder::class,
            AnnouncementPermissionsSeeder::class,
            ExchangeRatePermissionsSeeder::class,
            RbacPermissionsSeeder::class,
            SupportTicketPermissionsSeeder::class,
            SupportTicketAttachmentTableSeeder::class,
            TranslationPermissionsSeeder::class,
            TreePermissionsSeeder::class,
            UserPermissionsSeeder::class,
            SponsorTreeSettingSeeder::class,
            SystemsUserTableSeeder::class,
            ExchangeRateDatabaseSeeder::class,
            ExchangeRateMappingsTableSeeder::class,
            CommissionPermissionsSeeder::class,
            UserSettingSeederTable::class,
            PasswordPermissionsSeeder::class,
            DefaultRankTableSeeder::class,
            GoldmineRankLevelPercentageTableSeeder::class,
            AddCreditToExchangeRateTableSeeder::class,
            CreditPermissionsSeeder::class,
            DefaultGoldmineRankTableSeeder::class,
            MemberRankCommissionManualOverridePermissionTableSeeder::class,
            CommissionPayoutSettingTableSeeder::class,
            AdminManageUserSpecialBonusTableSeeder::class,
            DepositTypeTableSeeder::class,
            DepositMethodTableSeeder::class,
            DepositStatusTableSeeder::class,
            SecurityPasswordSettingSeederTableSeeder::class,
            UpdateAdjustCreditTableSeeder::class,
            DepositPermissionsSeeder::class,
            DepositBankPermissionsSeeder::class,
            AdminViewMemberBalancePermissionTableSeeder::class,
            AddUplinePermissionTableSeeder::class,
            RoiTierTableSeeder::class,
            RoiTierSettingTableSeeder::class,
            RoiManagementPermissionTableSeeder::class,
            AddGoldmineRankSettingTableSeeder::class,
            AddInitialCropeanClientTableSeeder::class,
            DocumentVerificationPermissionTableSeeder::class,
            CreateDocumentStatusesTableSeeder::class,
            CreateDocumentTypesTableSeeder::class,
            SeedDocumentVerificationInUserTableSeeder::class,
            UpdateCanConvertTableSeeder::class,
            CreditConversionAdminFeeSettingSeeder::class,
            AddedCreditConversionTransactionTypeSeeder::class,
            PenaltySettingSeeder::class,
            AddBankTransactionTypeTableSeeder::class,
            AddRankSettingTableSeeder::class,
            GoldmineRankAndLevelTableSeeder::class,
            CreateDashboardPermissionTableSeeder::class,
            WithdrawalPermissionsSeeder::class,
            WithdrawalStatusPageTableSeeder::class,
            WithdrawalDatabaseSeeder::class,
            AdminMemberManagementBankPermissionsTableSeeder::class,
            WithdrawalBankPermissionsSeeder::class,
            MemberCreditConvertPermissionTableSeeder::class,
            AddDashboardKycRequestTableSeeder::class,
            DepositAttachmentPermissionsSeeder::class,
            BulkUpdateWithdrawalStatusTableSeeder::class,
            CreditConversionSettingTableSeeder::class,
            CreditAdjustmentListingPermissionTableSeeder::class,
            SetWithdrawalTypeToInclusiveTableSeeder::class,
            CreditConversionPermissionTableSeeder::class,
            EpoaContractTranslationTableSeeder::class,
            WithdrawalBankNameTableSeeder::class,
            ExportBreakdownTableSeeder::class,
            MemberQualificationSettingTableSeeder::class,
            EpoaPermissionTableSeeder::class,
            NewAdminDashboardWidgetPermissionTableSeeder::class,
            JunePromotionTableSeeder::class,
            PromotionCreditTableSeeder::class,
            PromotionTransactionTypeTableSeeder::class,
            BlockchainCreditTypeTableSeeder::class,
            BlockchainTransactionTypeDatabaseSeeder::class,
            UsdtErc20NodesTableSeeder::class,
            UsdtErc20TokensSeeder::class,
            UsdcTokensSeeder::class,
            UsdtErc20MasterWalletSettingSeeder::class,
            CreateWalletForExistingMemberSeeder::class,
            UsdtErc20SettingTableSeeder::class,
            UsdcSettingTableSeeder::class,
            BlockchainGasFeeTransactionTypeSeeder::class,
            EthereumGasPriceSettingSeeder::class,
            BlockchainDepositPermissionsSeeder::class,
            BlockchainWithdrawalPermissionTableSeeder::class,
            BlockchainWithdrawalAddressesPermissionTableSeeder::class,
            BlockchainWithdrawalTypesTableSeeder::class,
            UsdtErc20WithdrawalSettingDatabaseSeeder::class,
            UsdcWithdrawalSettingDatabaseSeeder::class,
            BlockchainCreditTypeBankAccountTableSeeder::class,
            UsdtUsdcExchangeRateTableSeeder::class,
            ViewBlockchainTransactionsPermissionsTableSeeder::class,
            MemberUsdtUsdcDashboardCreditPermissionTableSeeder::class,
            UsdtUsdcStatementPermissionTableSeeder::class,
            BlockchainSettingPermissionTableSeeder::class,
            BlockchainAddressManagementPermissionTableSeeder::class,
            CreditConversionStatusTableSeeder::class,
            AlterCurrentConversionsStatusTableSeeder::class,
            ConversionRefundTransactionTypeTableSeeder::class,
            UsdtUsdcConversionSettingTableSeeder::class,
            ConversionPermissionTableSeeder::class,
            CreditTransferPermissionsSeeder::class,
            JulyPromotionTableSeeder::class,
            PromotionPermissionTableSeeder::class,
            AdminDashboardDepositsWithdrawalsPermissionsTableSeeder::class,
            ConvertibleCreditTypesTableSeeder::class,
            DefaultConversionSettingTableSeeder::class,
            AugustPromotionTableSeeder::class,
            AugustPromotionPermissionTableSeeder::class,
            PopulateExistingMemberReferralMidWithSponsorMidTableSeeder::class,
            ReferralBonusSettingTableSeeder::class,
            ReferralBonusTransactionTypeTableSeeder::class,
            ReferralBonusPermissionTableSeeder::class,
            WelcomeMessageVideoTableSeeder::class,
            CampaignStatusesTableSeeder::class,
            CampaignBankTransactionTypeTableSeeder::class,
            CampaignPermissionsTableSeeder::class,
            DefaultBlockchainWithdrawalStatusTableSeeder::class,
            UpdateCurrentBlockchainWithdrawalStatusesTableSeeder::class,
            BlockchainWithdrawalRefundTransactionTypeTableSeeder::class,
            BlockchainWithdrawalApprovalPermissionsTableSeeder::class,
            AddScheduledStatusToCampaignStatusesTableSeeder::class,
        ];
    }
}
