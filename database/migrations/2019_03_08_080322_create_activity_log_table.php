<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (!Schema::connection('mysql_logs')->hasTable(config('activitylog.table_name'))) {
            Schema::connection('mysql_logs')->create(config('activitylog.table_name'), function (Blueprint $table) {
                $table->increments('id');
                $table->string('log_name')->nullable();
                $table->text('description');
                $table->integer('subject_id')->nullable();
                $table->string('subject_type')->nullable();
                $table->integer('causer_id')->nullable();
                $table->string('causer_type')->nullable();
                $table->text('properties')->nullable();
                $table->timestamps();

                $table->index('log_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('mysql_logs')->dropIfExists(config('activitylog.table_name'));
    }
}
