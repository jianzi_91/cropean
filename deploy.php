<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'cropean');
// Project repository
set('repository', 'git@repo.plus65host.net:cropean/cropean-laravel-app.git');

// [Optional] Allocate tty for git clone. Default value is false.
// set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', ['resources/lang/en.json', 'resources/lang/cn.json']);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', ['resources/lang']);
set('allow_anonymous_stats', false);

// Hosts
inventory('hosts.yml');

// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});

task(
    'reload:php-fpm',
    function () {
        run('sudo service php-fpm reload');
    }
);

task(
    'cloudflare:cache_purge',
    function () {
        run('cd {{release_path}} && bash scripts/cloudflare.sh');
    }
);

task(
    'setting:flush',
    function () {
        run('cd {{release_path}} && artisan setting:flush');
    }
);

task(
    'queue:restart',
    function () {
        run('{{bin/php}} {{release_path}}/artisan queue:restart');
    }
);

task(
    'queue:retry',
    function () {
        run('{{bin/php}} {{release_path}}/artisan queue:retry all');
    }
);

task(
    'translation:find_keys',
    function () {
        run('cd {{release_path}} && {{bin/php}} {{release_path}}/artisan translation:find_keys');
    }
);

task(
    'deploy:assets',
    function () {
        if (input()->hasOption('no-yarn') && input()->getOption('no-yarn')) {
            return;
        }

        run("cd {{release_path}} && yarn && yarn run production");
    }
);

task(
    'artisan:config:clear',
    function () {
        run('{{bin/php}} {{release_path}}/artisan config:clear');
    }
);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
after('rollback', 'reload:php-fpm');
after('deploy:vendors', 'deploy:assets');
after('artisan:db:seed', 'translation:find_keys');

after('artisan:migrate', 'artisan:db:seed');
after('artisan:config:clear', 'setting:flush');
after('setting:flush', 'artisan:config:cache');
after('cleanup', 'queue:restart');
//after('queue:restart', 'queue:retry');

after('deploy', 'reload:php-fpm');
after('reload:php-fpm', 'cloudflare:cache_purge');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
