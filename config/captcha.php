<?php

return [

    'characters' => env('CAPTCHA_CHARACTERS', '2346789abcdefghjmnpqrtuxyzABCDEFGHJMNPQRTUXYZ'),

    'default' => [
        'length'  => 9,
        'width'   => 120,
        'height'  => 36,
        'quality' => 90,
        'math'    => true,
    ],

    'flat' => [
        'length'     => env('CAPTCHA_LENGTHS', '6'),
        'width'      => 120,
        'height'     => 58,
        'quality'    => 50,
        'lines'      => 2,
        'bgImage'    => false,
        'bgColor'    => '#cda674',
        'fontColors' => ['#fff'],
        'contrast'   => -5,
    ],

    'mini' => [
        'length' => 3,
        'width'  => 60,
        'height' => 32,
    ],

    'inverse' => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ]

];
