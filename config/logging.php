<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver'            => 'stack',
            'channels'          => ['daily'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
        ],
        'daily' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
            'days'   => 14,
        ],

        'slack' => [
            'driver'   => 'slack',
            'url'      => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji'    => ':boom:',
            'level'    => 'critical',
        ],

        'papertrail' => [
            'driver'       => 'monolog',
            'level'        => 'debug',
            'handler'      => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver'    => 'monolog',
            'handler'   => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with'      => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level'  => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level'  => 'debug',
        ],

        'http' => [
            'driver' => 'daily',
            'path'   => storage_path('http/http.log'),
            'level'  => 'debug',
            'days'   => 14,
        ],

        'processBlocksCron' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/processBlocksCron/processBlocksCron.log'),
            'level'  => 'info',
            'days'   => 90,
        ],

        'processBlocksConfirmation' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/processBlocksConfirmationCron/processBlocksConfirmationCron.log'),
            'level'  => 'info',
            'days'   => 90,
        ],

        'transferOut' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/transferOut/processTransferOut.log'),
            'level'  => 'info',
            'days'   => 90,
        ],

        'masterWalletRedirection' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/masterWalletRedirection/processMasterWalletRedirection.log'),
            'level'  => 'info',
            'days'   => 90,
        ],

        'erc20Processor' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/processBlocksCron/erc20Processor.log'),
            'level'  => 'info',
            'days'   => 90,
        ],

        'eContractPdf' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/ePOA/ePOA.log'),
            'level'  => 'info',
            'days'   => 10,
        ],

        'userTermination' => [
            'driver' => 'daily',
            'path'   => storage_path('/logs/cron/termination/userTermination.log'),
            'level'  => 'info',
            'days'   => 10,
        ],
    ],

];
