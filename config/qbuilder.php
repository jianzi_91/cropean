<?php

return [
    'filter' => [
        'components' => [
            /*
    		|--------------------------------------------------------------------------
    		| Date Range default filter
    		|--------------------------------------------------------------------------
    		|
    		| Here you may configure the date range settings
    		|
    		*/
            'date_range' => [
                'delimeter' => ' to ', // 2019-02-01 to 2019-02-03
            ],

            /*
    		|--------------------------------------------------------------------------
    		| Date default filter
    		|--------------------------------------------------------------------------
    		|
    		| Here you may define the default filter, when no value.
    		| today - Default date to today
    		|
    		*/
            'date' => null,
        ],
    ],

    'query' => [
        /*
        |--------------------------------------------------------------------------
        | Per page limit
        |--------------------------------------------------------------------------
        |
        | The pagination page limit
        |
        */
        'page' => env('PAGE_LIMIT', 50),

        /*
         |--------------------------------------------------------------------------
         | The sort
         |--------------------------------------------------------------------------
         |
         | The default sort
         |
         */
        'sort' => 'asc',
    ],

    'export' => [
        /*
        |--------------------------------------------------------------------------
        | Export Limit
        |--------------------------------------------------------------------------
        |
        | If number of rows more than limit, it will be send to member's email
        |
        */
        'limit' => env('EXPORT_LIMIT', 5000),
        /*
        |--------------------------------------------------------------------------
        | Number of rows per file
        |--------------------------------------------------------------------------
        |
        | If number of rows more than limit, it will be send to member's email
        |
        */
        'rows' => env('EXPORT_ROWS', 5000),
        /*
        |--------------------------------------------------------------------------
        | Default email
        |--------------------------------------------------------------------------
        |
        | If recipient email is null, default to
        |
        */
        'email' => env('EXPORT_EMAIL', 'backend@plus65.com.sg'),
        /*
         * This queue will be used to generate report and send email .
         * Leave empty to use the default queue.
         */
        'queue_name' => 'qbuilder-export',
    ]
];
