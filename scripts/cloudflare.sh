#!/bin/bash

ZONE_ID="b7a5e1b2030e5cb714cfb4a444fa2c2c"
API_KEY="b63fb99f3809bef4dc487a78ec295aa47fa1c"
EMAIL_ID="jeffongjs@gmail.com"

curl -X DELETE "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/purge_cache" \
-H "X-Auth-Email: $EMAIL_ID" \
-H "X-Auth-Key: $API_KEY" \
-H "Content-Type: application/json" \
--data '{"purge_everything":true}'
