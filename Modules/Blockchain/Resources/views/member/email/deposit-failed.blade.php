@component('mail::panel')
<div style="background-color: #315774; padding: 40px 10px 40px 10px !important;">
  <img src="{{ url('/images/logo.png') }}" />
  <div style="padding: 40px 0px 40px 0px !important;">
    <div style="background-color: #315774; padding: 20px 40px; border: 5px solid #FFFEFC !important;">
      <p style="color: #FFFEFC;">This deposit had failed.</p>
      <p style="color: #FFFEFC;">ID: {{ $deposit->id }}</p>
      <p style="color: #FFFEFC;">Created At: {{ $deposit->created_at }}</p>
      <p style="color: #FFFEFC;">User: {{ $deposit->user->username }}</p>
      <p style="color: #FFFEFC;">Credit Type: {{ $deposit->depositCreditType->name }}</p>
      <p style="color: #FFFEFC;">Reference: {{ $deposit->reference_number }}</p>
      <p style="color: #FFFEFC;">Block: {{ $deposit->block }}</p>
      <p style="color: #FFFEFC;">Hash: {{ $deposit->transaction_hash }}</p>
      <p style="color: #FFFEFC;">Sending Wallet: {{ $deposit->sending_address }}</p>
      <p style="color: #FFFEFC;">Receiving Wallet: {{ $deposit->receiving_address }}</p>
      <p style="color: #FFFEFC;">Amount: {{ $deposit->amount }}</p>
      <p style="color: #FFFEFC;">Fee: {{ $deposit->blockchain_fee }}</p>
      <p style="color: #FFFEFC;">Confirmation: {{ $deposit->confirmations }}</p>
      <br/>
    </div>
  </div>
</div>
@endcomponent
