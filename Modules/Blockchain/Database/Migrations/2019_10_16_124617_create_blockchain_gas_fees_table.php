<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockchainGasFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('bank_credit_type_id');
            $table->unsignedInteger('blockchain_fee_transaction_type_id');
            $table->string('transaction_hash');
            $table->unsignedDecimal('blockchain_fee', 40, 20);
            $table->string('causer_reference_number')->index();

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('blockchain_fee_transaction_type_id', 'fee_transaction_type_id_foreign')
                ->references('id')
                ->on('blockchain_fee_transaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_fees');
    }
}
