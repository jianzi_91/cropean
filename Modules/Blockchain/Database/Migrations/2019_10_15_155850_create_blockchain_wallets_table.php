<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockchainWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('address')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('bank_credit_type_id')->index();
            $table->text('private_key')->nullable();
            $table->string('qr_code')->nullable();

            //constraints
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bank_credit_type_id')->references('id')->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_wallets');
    }
}
