<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockchainProcessedBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::connection('blockchain')->hasTable('blockchain_processed_blocks')) {
            Schema::connection('blockchain')->create('blockchain_processed_blocks', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('block_processor_type');
                $table->unsignedBigInteger('block_number')->index();

                $table->unique(['block_processor_type', 'block_number'], 'unique_block_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('blockchain')->dropIfExists('blockchain_processed_blocks');
    }
}
