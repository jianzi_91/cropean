<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockchainTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('bank_credit_type_id');
            $table->string('ticker');
            $table->string('implementation_class');
            $table->string('contract_address')->unique()->nullable();
            $table->unsignedInteger('property_id')->unique()->nullable();

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_tokens');
    }
}
