<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockchainEtherRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_ether_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->unsignedInteger('bank_credit_type_id');
            $table->string('erc20_wallet_address');
            $table->string('eth_master_wallet_address');
            $table->decimal('request_amount', 40, 20);
            $table->string('transaction_hash')->nullable()->unique();
            $table->boolean('is_mined')
                ->default(false);

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_ether_requests');
    }
}
