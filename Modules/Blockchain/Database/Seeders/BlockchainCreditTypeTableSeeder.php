<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BlockchainCreditTypeTableSeeder extends Seeder
{
    /**
     * Class constructor
     */
    public function __construct(BankCreditTypeContract $bankCreditTypeRepo)
    {
        $this->bankCreditTypeRepo = $bankCreditTypeRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $creditTypes = [
            [
                'name'             => 'eth',
                'name_translation' => 'eth',
                'is_adjustable'    => false,
                'is_transferable'  => false,
                'can_deposit'      => false,
                'can_withdraw'     => false,
                'can_convert'      => false,
                'is_admin_only'    => false,
                'network'          => 'ETH',
                'is_active'        => true,
            ],
            [
                'name'             => 'usdt_erc20',
                'name_translation' => 'usdt erc20',
                'is_adjustable'    => true,
                'is_transferable'  => true,
                'can_deposit'      => true,
                'can_withdraw'     => true,
                'can_convert'      => false,
                'is_admin_only'    => false,
                'network'          => 'ETH',
                'is_active'        => true,
            ],
            [
                'name'             => 'usdc',
                'name_translation' => 'usdc',
                'is_adjustable'    => true,
                'is_transferable'  => true,
                'can_deposit'      => true,
                'can_withdraw'     => true,
                'can_convert'      => false,
                'is_admin_only'    => false,
                'network'          => 'ETH',
                'is_active'        => true,
            ],
        ];

        try {
            DB::beginTransaction();

            foreach ($creditTypes as $creditType) {
                $this->bankCreditTypeRepo->add($creditType);
            }

            $usdtErc20 = $this->bankCreditTypeRepo->getModel()->where('name', 'usdt_erc20')->first();
            $usdc      = $this->bankCreditTypeRepo->getModel()->where('name', 'usdc')->first();

            $usdtErc20->update([
                'deposit_credit_type_id'    => $usdtErc20->id,
                'withdrawal_credit_type_id' => $usdtErc20->id,
            ]);

            $usdc->update([
                'deposit_credit_type_id'    => $usdc->id,
                'withdrawal_credit_type_id' => $usdc->id,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e->getMessage());
        }
    }
}
