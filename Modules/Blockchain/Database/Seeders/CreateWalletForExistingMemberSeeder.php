<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\User\Models\User;

/**
 * Seeder for ERC20 tokens.
 *
 * @package Modules\Blockchain\Database\Seeders
 */
class CreateWalletForExistingMemberSeeder extends Seeder
{
    /**
     * Class constructor.
     *
     * @param BlockchainTokenContract $erc20TokenService
     */
    public function __construct(BankCreditTypeContract $creditTypeRepo, BlockchainWalletContract $walletRepo, EthereumContract $ethRepo)
    {
        $this->creditTypeRepo = $creditTypeRepo;
        $this->walletRepo     = $walletRepo;
        $this->ethRepo        = $ethRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usdtErc20Credit = $this->creditTypeRepo->findBySlug('usdt_erc20');
        $usdcCredit      = $this->creditTypeRepo->findBySlug('usdc');

        $users = User::all();

        foreach ($users as $user) {
            $wallet = $this->ethRepo->createNewAddress();
            $data   = [
                'address'     => $wallet['address'],
                'private_key' => encrypt($wallet['private_key']),
                'qr_code'     => $this->walletRepo->generateQrCode($wallet['address']),
            ];

            $this->walletRepo->add([
                'address'             => $data['address'],
                'user_id'             => $user->id,
                'bank_credit_type_id' => $usdtErc20Credit->id,
                'private_key'         => $data['private_key'],
                'qr_code'             => $data['qr_code'],
            ]);

            $this->walletRepo->add([
                'address'             => $data['address'],
                'user_id'             => $user->id,
                'bank_credit_type_id' => $usdcCredit->id,
                'private_key'         => $data['private_key'],
                'qr_code'             => $data['qr_code'],
            ]);
        }
    }
}
