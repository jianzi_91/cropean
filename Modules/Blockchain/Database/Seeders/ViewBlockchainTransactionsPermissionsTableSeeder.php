<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ViewBlockchainTransactionsPermissionsTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_blockchain_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Blockchain',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_blockchain_transactions_view',
                        'title' => 'Admin Blockchain Transactions View',
                    ],
                ]
            ],
            'member_blockchain_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Blockchain',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_blockchain_transactions_view',
                        'title' => 'Member Blockchain Transactions View',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_blockchain_transactions_view',
            ],
            Role::ADMIN => [
                'admin_blockchain_transactions_view',
            ],
            Role::MEMBER => [
                'member_blockchain_transactions_view',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
