<?php

namespace Modules\Blockchain\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Modules\Credit\Models\BankAccount;
use Modules\Credit\Models\BankCreditType;

class BlockchainCreditTypeBankAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $ethCredit       = BankCreditType::where('name', 'eth')->first();
            $usdtErc20Credit = BankCreditType::where('name', 'usdt_erc20')->first();
            $usdcCredit      = BankCreditType::where('name', 'usdc')->first();

            $bankAccounts = BankAccount::all();

            foreach ($bankAccounts as $bankAccount) {
                $bankAccount->accountCredits()->create([
                    'bank_credit_type_id' => $ethCredit->id
                ]);

                $bankAccount->accountCredits()->create([
                    'bank_credit_type_id' => $usdtErc20Credit->id
                ]);

                $bankAccount->accountCredits()->create([
                    'bank_credit_type_id' => $usdcCredit->id
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e);
        }
    }
}
