<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Models\BlockchainFeeTransactionType;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class BlockchainGasFeeTransactionTypeSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->pageRepo->add([
            'name'                => 's_blockchain_fee_transaction_types',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $types = [
            'master_wallet_redirection',
            'ether_request',
            'withdrawal'
        ];

        foreach ($types as $type) {
            (new BlockchainFeeTransactionType([
                'name'      => $type,
                'is_active' => true,
            ]))->save();
        }
    }
}
