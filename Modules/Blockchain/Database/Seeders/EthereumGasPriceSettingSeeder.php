<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Contracts\SettingContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;

class EthereumGasPriceSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param SettingContract $settingRepo
     * @return void
     * @throws \Throwable
     */
    public function run(SettingContract $settingRepo)
    {
        DB::beginTransaction();

        try {
            $settingCategory = SettingCategory::firstOrCreate([
                'name'      => 'blockchain',
                'is_active' => 1,
            ]);

            $key   = 'blockchain_eth_gas_price_gwei';
            $value = 0;

            Setting::create([
                'name'                => $key,
                'setting_category_id' => $settingCategory->id,
                'is_hidden'           => 1,
                'is_active'           => 1,
            ]);

            $settingRepo->addValuesBySlug($key, $value);

            DB::commit();
        } catch (\Throwable $t) {
            DB::rollback();
            throw $t;
        }
    }
}
