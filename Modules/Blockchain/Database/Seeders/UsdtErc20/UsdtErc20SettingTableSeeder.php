<?php

namespace Modules\Blockchain\Database\Seeders\UsdtErc20;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtErc20SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain_deposit',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        if (app()->environment() == 'production') {
            $limits = [
                'deposit_usdt_erc20_min_confirmation'           => 30,
                'deposit_eth_min_confirmation'                  => 30,
                'minimum_usdt_erc20_deposit_redirection_amount' => 0.1,
                'minimum_eth_deposit_redirection_amount'        => 0.1,
                'withdrawal_usdt_erc20_min_confirmation'        => 30,
                'withdrawal_eth_min_confirmation'               => 30,
                'blockchain_process_erc20_flag'                 => 0,
            ];
        } else {
            $limits = [
                'deposit_usdt_erc20_min_confirmation'           => 4,
                'deposit_eth_min_confirmation'                  => 4,
                'minimum_usdt_erc20_deposit_redirection_amount' => 0.1,
                'minimum_eth_deposit_redirection_amount'        => 0.1,
                'withdrawal_usdt_erc20_min_confirmation'        => 4,
                'withdrawal_eth_min_confirmation'               => 4,
                'blockchain_process_erc20_flag'                 => 0,
            ];
        }

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 0;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
