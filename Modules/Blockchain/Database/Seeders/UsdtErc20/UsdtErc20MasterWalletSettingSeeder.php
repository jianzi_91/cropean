<?php

namespace Modules\Blockchain\Database\Seeders\UsdtErc20;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtErc20MasterWalletSettingSeeder extends Seeder
{
    /**
     * Bitcoin repository.
     *
     * @var BitcoinContract
     */
    protected $btcRepo;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param BitcoinContract $btcContract
     * @param EthereumContract $ethContract
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(BitcoinContract $btcContract, EthereumContract $ethContract, BlockchainWalletContract $walletContract)
    {
        $this->ethRepo    = $ethContract;
        $this->walletRepo = $walletContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'master_wallet',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        // ETH can be used directly for ERC20 tokens, it is ok since address format is same
        $ethWallet = $this->ethRepo->createNewAddress();
        $ethQrCode = $this->walletRepo->generateQrCode($ethWallet['address']);

        $limits = [
            'master_wallet_eth_address'            => $ethWallet['address'],
            'master_wallet_eth_private_key'        => encrypt($ethWallet['private_key']),
            'master_wallet_eth_qr_code'            => $ethQrCode,
            'master_wallet_usdt_erc20_address'     => $ethWallet['address'],
            'master_wallet_usdt_erc20_private_key' => encrypt($ethWallet['private_key']),
            'master_wallet_usdt_erc20_qr_code'     => $ethQrCode,
            'master_wallet_usdc_address'           => $ethWallet['address'],
            'master_wallet_usdc_private_key'       => encrypt($ethWallet['private_key']),
            'master_wallet_usdc_qr_code'           => $ethQrCode,
        ];

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 1;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
