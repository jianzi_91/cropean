<?php

namespace Modules\Blockchain\Database\Seeders\UsdtErc20;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainNodeContract;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Seeder for ERC20 tokens.
 *
 */
class UsdtErc20NodesTableSeeder extends Seeder
{
    /**
     * Blockchain node service.
     *
     * @var BlockchainNodeContract
     */
    protected $blockchainNodeRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainNodeContract $blockchainNodeRepo
     */
    public function __construct(BlockchainNodeContract $blockchainNodeRepo, BankCreditTypeContract $bankCreditTypeRepo)
    {
        $this->blockchainNodeRepo = $blockchainNodeRepo;
        $this->bankCreditTypeRepo = $bankCreditTypeRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $configs = $this->getNodes();

        $btcTokens = $this->bankCreditTypeRepo->getCreditsByNetwork('BTC');
        $ethTokens = $this->bankCreditTypeRepo->getCreditsByNetwork('ETH');

        foreach ($configs as $bankCreditType => $config) {
            $node = $this->blockchainNodeRepo->add($config);

            if ($node->network == 'BTC') {
                foreach ($btcTokens as $btcToken) {
                    $node->bankCreditTypes()->attach($btcToken->id);
                }
            }

            if ($node->network == 'ETH') {
                foreach ($ethTokens as $ethToken) {
                    $node->bankCreditTypes()->attach($ethToken->id);
                }
            }
        }
    }

    /**
     * Get nodes.
     *
     * @return string[][]
     */
    protected function getNodes()
    {
        $config = [
            'local' => [
                'eth1' => [
                    'host'      => ip2long('157.245.193.41'),
                    'port'      => '8545',
                    'network'   => 'ETH',
                    'is_active' => true,
                ]
            ],
            'staging' => [
                'eth1' => [
                    'host'      => ip2long('157.245.193.41'),
                    'port'      => '8545',
                    'network'   => 'ETH',
                    'is_active' => true,
                ],
            ],
            'production' => [
                'eth1' => [
                    'host'      => ip2long('54.151.221.248'),
                    'port'      => '8545',
                    'network'   => 'ETH',
                    'is_active' => true,
                ],
                'eth2' => [
                    'host'      => ip2long('18.141.235.42'),
                    'port'      => '8545',
                    'network'   => 'ETH',
                    'is_active' => true,
                ]
            ]
        ];

        $config['uat'] = $config['staging'];

        return $config[app()->environment()];
    }
}
