<?php

namespace Modules\Blockchain\Database\Seeders\UsdtErc20;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Seeder for ERC20 tokens.
 *
 * @package Modules\Blockchain\Database\Seeders
 */
class UsdtErc20TokensSeeder extends Seeder
{
    /**
     * Blockchain token service.
     *
     * @var BlockchainTokenContract
     */
    protected $blockchainTokenRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainTokenContract $erc20TokenService
     */
    public function __construct(BlockchainTokenContract $erc20TokenService, BankCreditTypeContract $bankCreditTypeService)
    {
        $this->blockchainTokenRepo = $erc20TokenService;
        $this->bankCreditTypeRepo  = $bankCreditTypeService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //USDT ERC20
        $usdtErc20Config = [
            'local' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_ERC20),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdt_erc20',
                'contract_address'     => '0x4c36cb1b0f7bb2b7d3b8b7e01a0e9061167eff05'
            ],
            'staging' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_ERC20),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdt_erc20',
                'contract_address'     => '0x4c36cb1b0f7bb2b7d3b8b7e01a0e9061167eff05'
            ],
            'production' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_ERC20),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdt_erc20',
                'contract_address'     => '0xdac17f958d2ee523a2206206994597c13d831ec7'
            ]
        ];
        $usdtErc20Config['uat'] = $usdtErc20Config['staging'];

        $this->blockchainTokenRepo->add($usdtErc20Config[app()->environment()]);
    }
}
