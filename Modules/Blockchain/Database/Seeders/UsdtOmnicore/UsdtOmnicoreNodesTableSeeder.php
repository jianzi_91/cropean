<?php

namespace Modules\Blockchain\Database\Seeders\UsdtOmnicore;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainNodeContract;
use Modules\Credit\Contracts\BankCreditTypeContract;

class UsdtOmnicoreNodesTableSeeder extends Seeder
{
    /**
     * Blockchain node service.
     *
     * @var BlockchainNodeContract
     */
    protected $blockchainNodeRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainNodeContract $blockchainNodeRepo
     */
    public function __construct(BlockchainNodeContract $blockchainNodeRepo, BankCreditTypeContract $bankCreditTypeRepo)
    {
        $this->blockchainNodeRepo = $blockchainNodeRepo;
        $this->bankCreditTypeRepo = $bankCreditTypeRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $configs = $this->getNodes();

        $btcTokens = $this->bankCreditTypeRepo->getCreditsByNetwork('BTC');
        $ethTokens = $this->bankCreditTypeRepo->getCreditsByNetwork('ETH');

        foreach ($configs as $bankCreditType => $config) {
            $node = $this->blockchainNodeRepo->add($config);

            if ($node->network == 'BTC') {
                foreach ($btcTokens as $btcToken) {
                    $node->bankCreditTypes()->attach($btcToken->id);
                }
            }

            if ($node->network == 'ETH') {
                foreach ($ethTokens as $ethToken) {
                    $node->bankCreditTypes()->attach($ethToken->id);
                }
            }
        }
    }

    /**
     * Get nodes.
     *
     * @return string[][]
     */
    protected function getNodes()
    {
        $config = [
            'local' => [
                'usdt1' => [
                    'host'      => ip2long('137.59.186.117'),
                    'port'      => '18332',
                    'username'  => 'db3usdttestnet',
                    'password'  => encrypt('jfMWjDbw58g3KtpP8DpcXtDQTT7jUV'),
                    'network'   => 'BTC',
                    'is_active' => true,
                ],
            ],
            'staging' => [
                'usdt1' => [
                    'host'      => ip2long(''),
                    'port'      => '8332',
                    'username'  => '',
                    'password'  => encrypt(''),
                    'network'   => 'BTC',
                    'is_active' => true,
                ],
                'usdt2' => [
                    'host'      => ip2long(''),
                    'port'      => '8332',
                    'username'  => '',
                    'password'  => encrypt(''),
                    'network'   => 'BTC',
                    'is_active' => true,
                ],
            ],
            'production' => [
                'usdt1' => [
                    'host'      => ip2long(''),
                    'port'      => '8332',
                    'username'  => '',
                    'password'  => encrypt(''),
                    'network'   => 'BTC',
                    'is_active' => true,
                ],
                'usdt2' => [
                    'host'      => ip2long(''),
                    'port'      => '8332',
                    'username'  => '',
                    'password'  => encrypt(''),
                    'network'   => 'BTC',
                    'is_active' => true,
                ],
            ]
        ];

        $config['uat'] = $config['staging'];

        return $config[app()->environment()];
    }
}
