<?php

namespace Modules\Blockchain\Database\Seeders\UsdtOmnicore;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Seeder for ERC20 tokens.
 *
 * @package Modules\Blockchain\Database\Seeders
 */
class UsdtOmnicoreTokensSeeder extends Seeder
{
    /**
     * Blockchain token service.
     *
     * @var BlockchainTokenContract
     */
    protected $blockchainTokenRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainTokenContract $erc20TokenService
     */
    public function __construct(BlockchainTokenContract $erc20TokenService, BankCreditTypeContract $bankCreditTypeService)
    {
        $this->blockchainTokenRepo = $erc20TokenService;
        $this->bankCreditTypeRepo  = $bankCreditTypeService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //USDT OMNI
        $usdtOmniConfig = [
            'local' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_OMNI),
                'implementation_class' => UsdtRepository::class,
                'ticker'               => 'usdt_omni',
                'property_id'          => '2',
            ],
            'staging' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_OMNI),
                'implementation_class' => UsdtRepository::class,
                'ticker'               => 'usdt_omni',
                'property_id'          => '2',
            ],
            'production' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDT_OMNI),
                'implementation_class' => UsdtRepository::class,
                'ticker'               => 'usdt_omni',
                'property_id'          => '31',
            ]
        ];

        $usdtOmniConfig['uat'] = $usdtOmniConfig['staging'];

        $this->blockchainTokenRepo->add($usdtOmniConfig[app()->environment()]);
    }
}
