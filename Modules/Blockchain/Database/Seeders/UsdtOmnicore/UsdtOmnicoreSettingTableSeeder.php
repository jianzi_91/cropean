<?php

namespace Modules\Blockchain\Database\Seeders\UsdtOmnicore;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtOmnicoreSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain_deposit',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        if (app()->environment() == 'production') {
            $limits = [
                'deposit_btc_min_confirmation'                 => 6,
                'deposit_usdt_omni_min_confirmation'           => 6,
                'minimum_btc_deposit_redirection_amount'       => 0.1,
                'minimum_usdt_omni_deposit_redirection_amount' => 0.1,
                'withdrawal_btc_min_confirmation'              => 6,
                'withdrawal_usdt_omni_min_confirmation'        => 6,
                'blockchain_process_omnicore_flag'             => 0,
                'blockchain_process_btc_flag'                  => 0,
            ];
        } else {
            $limits = [
                'deposit_btc_min_confirmation'                 => 2,
                'deposit_usdt_omni_min_confirmation'           => 2,
                'minimum_btc_deposit_redirection_amount'       => 0.1,
                'minimum_usdt_omni_deposit_redirection_amount' => 0.1,
                'withdrawal_btc_min_confirmation'              => 2,
                'withdrawal_usdt_omni_min_confirmation'        => 2,
                'blockchain_process_omnicore_flag'             => 0,
                'blockchain_process_btc_flag'                  => 0,
            ];
        }

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 0;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
