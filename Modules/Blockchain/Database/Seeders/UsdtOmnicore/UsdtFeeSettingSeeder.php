<?php

namespace Modules\Blockchain\Database\Seeders\UsdtOmnicore;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtFeeSettingSeeder extends Seeder
{
    /**
     * Bitcoin repository.
     *
     * @var BitcoinContract
     */
    protected $btcRepo;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param BitcoinContract $btcContract
     * @param EthereumContract $ethContract
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(BitcoinContract $btcContract, EthereumContract $ethContract, BlockchainWalletContract $walletContract)
    {
        $this->btcRepo    = $btcContract;
        $this->ethRepo    = $ethContract;
        $this->walletRepo = $walletContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain fee',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        $settings = [
            'blockchain_usdt_redirection_satoshi_per_byte' => 5,
            'blockchain_usdt_withdrawal_satoshi_per_byte'  => 22,
        ];

        foreach ($settings as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 1;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
