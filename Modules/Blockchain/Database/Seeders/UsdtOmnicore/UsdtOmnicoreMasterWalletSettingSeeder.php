<?php

namespace Modules\Blockchain\Database\Seeders\UsdtOmnicore;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtOmnicoreMasterWalletSettingSeeder extends Seeder
{
    /**
     * Bitcoin repository.
     *
     * @var BitcoinContract
     */
    protected $btcRepo;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param BitcoinContract $btcContract
     * @param EthereumContract $ethContract
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(BitcoinContract $btcContract, EthereumContract $ethContract, BlockchainWalletContract $walletContract)
    {
        $this->btcRepo    = $btcContract;
        $this->ethRepo    = $ethContract;
        $this->walletRepo = $walletContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'master wallet',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        // BTC can be used directly for Omnicore tokens, it is ok since address format is same
        $btcWallet = $this->btcRepo->createNewAddress();
        $btcQrCode = $this->walletRepo->generateQrCode($btcWallet['address']);

        $limits = [
            'master_wallet_btc_address'           => $btcWallet['address'],
            'master_wallet_btc_private_key'       => encrypt($btcWallet['private_key']),
            'master_wallet_btc_qr_code'           => $btcQrCode,
            'master_wallet_usdt_omni_address'     => $btcWallet['address'],
            'master_wallet_usdt_omni_private_key' => encrypt($btcWallet['private_key']),
            'master_wallet_usdt_omni_qr_code'     => $btcQrCode,
        ];

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 1;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
