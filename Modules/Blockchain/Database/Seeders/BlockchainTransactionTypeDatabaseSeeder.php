<?php

namespace Modules\Blockchain\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Models\BankTransactionType;

class BlockchainTransactionTypeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blockchainDepositTransaction            = new BankTransactionType();
        $blockchainDepositTransaction->name      = BankTransactionTypeContract::BLOCKCHAIN_DEPOSIT;
        $blockchainDepositTransaction->is_active = 1;
        $blockchainDepositTransaction->save();

        $blockchainWithdrawalTransaction            = new BankTransactionType();
        $blockchainWithdrawalTransaction->name      = BankTransactionTypeContract::BLOCKCHAIN_WITHDRAWAL;
        $blockchainWithdrawalTransaction->is_active = 1;
        $blockchainWithdrawalTransaction->save();

        $blockchainWithdrawalTransaction            = new BankTransactionType();
        $blockchainWithdrawalTransaction->name      = BankTransactionTypeContract::BLOCKCHAIN_WITHDRAWAL_FEE;
        $blockchainWithdrawalTransaction->is_active = 1;
        $blockchainWithdrawalTransaction->save();
    }
}
