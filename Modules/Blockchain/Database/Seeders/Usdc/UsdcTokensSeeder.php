<?php

namespace Modules\Blockchain\Database\Seeders\Usdc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Seeder for USDC tokens.
 *
 * @package Modules\Blockchain\Database\Seeders
 */
class UsdcTokensSeeder extends Seeder
{
    /**
     * Blockchain token service.
     *
     * @var BlockchainTokenContract
     */
    protected $blockchainTokenRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainTokenContract $erc20TokenService
     */
    public function __construct(BlockchainTokenContract $erc20TokenService, BankCreditTypeContract $bankCreditTypeService)
    {
        $this->blockchainTokenRepo = $erc20TokenService;
        $this->bankCreditTypeRepo  = $bankCreditTypeService;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //USDT ERC20
        $usdtErc20Config = [
            'local' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDC),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdc',
                'contract_address'     => '0x8dc689efab9260a642769666fe52a9cc8eb2e658'
            ],
            'staging' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDC),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdc',
                'contract_address'     => '0x8dc689efab9260a642769666fe52a9cc8eb2e658'
            ],
            'production' => [
                'bank_credit_type_id'  => bank_credit_type_id($this->bankCreditTypeRepo::USDC),
                'implementation_class' => UsdtErc20Repository::class,
                'ticker'               => 'usdc',
                'contract_address'     => '0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48'
            ]
        ];
        $usdtErc20Config['uat'] = $usdtErc20Config['staging'];

        $this->blockchainTokenRepo->add($usdtErc20Config[app()->environment()]);
    }
}
