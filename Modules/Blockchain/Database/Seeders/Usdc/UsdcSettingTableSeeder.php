<?php

namespace Modules\Blockchain\Database\Seeders\Usdc;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdcSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain_deposit',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        if (app()->environment() == 'production') {
            $limits = [
                'deposit_usdc_min_confirmation'           => 30,
                'minimum_usdc_deposit_redirection_amount' => 100,
                'withdrawal_usdc_min_confirmation'        => 30,
            ];
        } else {
            $limits = [
                'deposit_usdc_min_confirmation'           => 4,
                'minimum_usdc_deposit_redirection_amount' => 0,
                'withdrawal_usdc_min_confirmation'        => 4,
            ];
        }

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 0;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
