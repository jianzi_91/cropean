<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'bindings' => [
        \Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract::class      => \Modules\Blockchain\Repositories\CryptoCurrencies\BitcoinRepository::class,
        \Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract::class     => \Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository::class,
        \Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract::class     => \Modules\Blockchain\Repositories\CryptoCurrencies\OmnicoreRepository::class,
        \Modules\Blockchain\Contracts\BlockchainNodeContract::class                => \Modules\Blockchain\Repositories\BlockchainNodeRepository::class,
        \Modules\Blockchain\Contracts\BlockchainTokenContract::class               => \Modules\Blockchain\Repositories\BlockchainTokenRepository::class,
        \Modules\Blockchain\Contracts\BlockchainWalletContract::class              => \Modules\Blockchain\Repositories\BlockchainWalletRepository::class,
        \Modules\Blockchain\Contracts\BlockchainFeeContract::class                 => \Modules\Blockchain\Repositories\BlockchainFeeRepository::class,
        \Modules\Blockchain\Contracts\Parser\ProcessedBlockContract::class         => \Modules\Blockchain\Repositories\Parser\ProcessedBlockRepository::class,
        \Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract::class  => \Modules\Blockchain\Repositories\BlockchainFeeTransactionTypeRepository::class,
        \Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract::class => \Modules\Blockchain\Repositories\Parser\BlockchainEtherRequestRepository::class,

    ],
    'nonce_generators' => [
        \Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository::class => [
            'implementation_class' => \Modules\Blockchain\Repositories\NonceGenerators\ParityNonceGenerator::class,
            'bank_credit_type'     => 'eth',
        ],
        \Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository::class => [
            'implementation_class' => \Modules\Blockchain\Repositories\NonceGenerators\ParityNonceGenerator::class,
            'bank_credit_type'     => \Modules\Credit\Contracts\BankCreditTypeContract::USDT_ERC20,
        ],
        \Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository::class => [
            'implementation_class' => \Modules\Blockchain\Repositories\NonceGenerators\ParityNonceGenerator::class,
            'bank_credit_type'     => \Modules\Credit\Contracts\BankCreditTypeContract::USDC,
        ],
    ],
    'erc20_gas_limit' => '65000',
    'storage'         => [
        'path_prefix' => env('WALLET_FILES_PATH_PREFIX', storage_path()),
        'private_key' => '/wallet/private_key',
        'qr_code'     => '/wallet/qr_code',
    ],
    'network' => [
        'btc' => 'BTC',
        'eth' => 'ETH',
    ],
    'confirmations_required' => [
        'eth'        => env('ETH_CONFIRMATIONS', 12),
        'usdt_erc20' => env('USDT_ERC20_CONFIRMATIONS', 12),
        'btc'        => env('BTC_CONFIRMATIONS', 3),
        'usdt_omni'  => env('USDT_OMNI_CONFIRMATIONS', 3)
    ],
    'transaction_processors' => [
        // 'btc'      => \Modules\Blockchain\Repositories\Parser\BlockProcessors\BitcoinBlockProcessor::class,
        // 'omnicore' => \Modules\Blockchain\Repositories\Parser\BlockProcessors\OmnicoreBlockProcessor::class,
        'erc20' => \Modules\Blockchain\Repositories\Parser\BlockProcessors\EthereumBlockProcessor::class,
    ],
    'data_collectors' => [
        \Modules\Blockchain\Repositories\Parser\BlockProcessors\EthereumBlockProcessor::class => [
            \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\EthereumDataCollector::class,
            \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\ERC20DataCollector::class,
        ],
        //  \Modules\Blockchain\Repositories\Parser\BlockProcessors\BitcoinBlockProcessor::class => [
        //      \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\BitcoinDataCollector::class,
        //  ],
        //  \Modules\Blockchain\Repositories\Parser\BlockProcessors\OmnicoreBlockProcessor::class => [
        //      \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\OmnicoreDataCollector::class,
        //  ],

    ],
    'data_handlers' => [
        //  \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\BitcoinDataCollector::class => [
        //      \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\BitcoinDepositHandler::class,
        //      \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\BitcoinMasterWalletDepositHandler::class,
        //  ],
        \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\EthereumDataCollector::class => [
            // \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\EthereumDepositHandler::class,
            \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\EthereumMasterWalletDepositHandler::class,
            \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\ERC20EtherRequestHandler::class,
        ],
        //  \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\OmnicoreDataCollector::class => [
        //      \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\OmnicoreDepositHandler::class,
        //  ],
        \Modules\Blockchain\Repositories\Parser\BlockDataCollectors\ERC20DataCollector::class => [
            \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\ERC20DepositHandler::class,
            \Modules\Blockchain\Repositories\Parser\FilteredDataHandlers\ERC20MasterWalletDepositHandler::class,
        ],
    ],
];
