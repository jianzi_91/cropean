<?php

namespace Modules\Blockchain\Repositories;

use Modules\Blockchain\Contracts\BlockchainNodeContract;
use Modules\Blockchain\Models\BlockchainNode;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class BlockchainNodeRepository extends Repository implements BlockchainNodeContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param BlockchainNode $model
     */
    public function __construct(BlockchainNode $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }
}
