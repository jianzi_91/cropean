<?php

namespace Modules\Blockchain\Repositories\NonceGenerators;

use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;
use Modules\Blockchain\Traits\JsonRpcTrait;

/**
 * Use Geth node to generate nonce.
 *
 * @package Modules\Blockchain\Repositories\NonceGenerators
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class GethNonceGenerator implements NonceGeneratorContract
{
    use JsonRpcTrait;

    /**
     * Bank credit type.
     *
     * @var string
     */
    protected $bankCreditType;

    /**
     * Class constructor.
     *
     * @param string $bankCreditType
     */
    public function __construct(string $bankCreditType)
    {
        $this->bankCreditType = $bankCreditType;
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract::nextNonce()
     * @throws \Exception
     */
    public function nextNonce(string $address, string $format = 'hex')
    {
        $countHex = $this->sendRpcRequest('eth_getTransactionCount', [$address, 'pending']);
        return 'hex' == $format ? $countHex : bigHexToDec($countHex);
    }

    /**
     * @inheritDoc
     */
    protected function getBankCreditTypeId()
    {
        return bank_credit_type_id($this->bankCreditType);
    }
}
