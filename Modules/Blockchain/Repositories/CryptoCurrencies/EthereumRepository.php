<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use Graze\GuzzleHttp\JsonRpc\Exception\ServerException;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;
use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use Modules\Blockchain\Models\BlockchainNode;
use Modules\Blockchain\Traits\EthereumTrait;

/**
 * Class EthereumRepository
 *
 * @package Trading\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class EthereumRepository extends AbstractCryptoCurrencyRepository implements EthereumContract
{
    use EthereumTrait;

    /**
     * Class constructor.
     *
     * @param NonceGeneratorContract $nonceGenerator
     */
    public function __construct(NonceGeneratorContract $nonceGenerator)
    {
        $this->nonceGenerator = $nonceGenerator;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTicker()
     */
    public function getTicker()
    {
        return 'eth';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNetwork()
     */
    public function getNetwork()
    {
        return 'ETH';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNodes()
     */
    public function getNodes()
    {
        return BlockchainNode::where('network', $this->getNetwork())->get();
    }

    /**
     * @param string $address
     * @return mixed
     * @throws \Exception
     */
    public function nextNonce(string $address)
    {
        return $this->nonceGenerator->nextNonce($address);
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::transfer()
     * @throws InsufficientBlockchainFundsException
     * @throws \Exception
     */
    public function transfer(
        string $fromAddress,
        string $toAddress,
        string $amount,
        array $options = []
    ) {
        $feeInclusive = array_key_exists('fee_inclusive', $options) ? $options['fee_inclusive'] : false;

        if (array_key_exists('nonce', $options) && !empty($options['nonce'])) {
            $this->specificNonce = $options['nonce'];
        } else {
            $this->specificNonce = 0;
        }

        if (array_key_exists('private_key', $options)) {
            $signedTransactionData = $this->signRawTransactionForEtherTransfer(
                $this->nonceGenerator,
                $options['private_key'],
                $fromAddress,
                $toAddress,
                $this->convertToFractionalUnit($amount),
                $feeInclusive
            );

            try {
                return $this->sendRpcRequest('eth_sendRawTransaction', [$signedTransactionData]);
            } catch (ServerException $e) {
                if (false !== stripos($e->getMessage(), 'insufficient funds')) {
                    throw new InsufficientBlockchainFundsException();
                }
            }
        }

        try {
            return $this->sendRpcRequest('personal_sendTransaction', [
                [
                    'from'  => $fromAddress,
                    'to'    => $toAddress,
                    'value' => '0x' . bigDecToHex($this->convertToFractionalUnit($amount)),
                ],
                array_key_exists('password', $options) ? $options['password'] : ''
            ]);
        } catch (ServerException $e) {
            if (false !== stripos($e->getMessage(), 'insufficient funds')) {
                throw new InsufficientBlockchainFundsException();
            }
        }
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getDecimalPlaces()
     */
    public function getDecimalPlaces()
    {
        return 18;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Traits\Js
     *
     * @return int
     */
    protected function getBankCreditTypeId()
    {
        return bank_credit_type_id($this->getTicker());
    }
}
