<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract;

/**
 * Abstract class to implement common functions shared by all cryptocurrencies.
 *
 * @package Modules\Blockchain\Repositories\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
abstract class AbstractCryptoCurrencyRepository implements CryptoCurrencyContract
{
    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\\CryptoCurrencyContract::convertToMainUnit()
     */
    final public function convertToMainUnit(string $fractionalUnitAmount)
    {
        $decimalPlaces = $this->getDecimalPlaces();
        return bcdiv($fractionalUnitAmount, bcpow(10, $decimalPlaces), $decimalPlaces);
    }

    /**
    * @inheritdoc
    * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::convertToFractionalUnit()
    */
    final public function convertToFractionalUnit(string $mainUnitAmount)
    {
        $decimalPlaces = $this->getDecimalPlaces();
        return bcmul($mainUnitAmount, bcpow(10, $decimalPlaces), 0);
    }
}
