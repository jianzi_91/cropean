<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use BitWasp\Bitcoin\Address\AddressCreator;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Exceptions\UnrecognizedAddressException;
use BitWasp\Bitcoin\Key\Factory\PrivateKeyFactory;
use BitWasp\Bitcoin\Script\ScriptFactory;
use BitWasp\Bitcoin\Transaction\Factory\Signer;
use BitWasp\Bitcoin\Transaction\OutPoint;
use BitWasp\Bitcoin\Transaction\TransactionFactory;
use BitWasp\Bitcoin\Transaction\TransactionOutput;
use BitWasp\Bitcoin\Utxo\Utxo;
use BitWasp\Buffertools\Buffer;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use Modules\Blockchain\Models\BlockchainNode;
use Modules\Blockchain\Traits\BitcoinAddressGenerator;
use Modules\Blockchain\Traits\JsonRpcTrait;
use Modules\Core\Traits\LogCronOutput;

/**
 * Service class for Bitcoin.
 *
 * @package Trading\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class BitcoinRepository extends AbstractCryptoCurrencyRepository implements BitcoinContract
{
    use BitcoinAddressGenerator, JsonRpcTrait, LogCronOutput;

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTicker()
     */
    public function getTicker()
    {
        return 'btc';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNetwork()
     */
    public function getNetwork()
    {
        return 'BTC';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNodes()
     */
    public function getNodes()
    {
        return BlockchainNode::where('network', $this->getNetwork())->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::createNewAddress()
     * @throws \Exception
     */
    public function createNewAddress()
    {
        $newAddress = $this->generateAddress();

        if (config('app.env') != 'testing') {
            $nodes = $this->getNodes();

            // import the address into all available nodes
            foreach ($nodes as $node) {
                $this->sendRpcRequest('importaddress', [$newAddress['address'], '', false], $node); // register this address in Bitcoin node as watch-only
            }
        }

        return $newAddress;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getLatestBlockNumber()
     * @throws \Exception
     */
    public function getLatestBlockNumber()
    {
        return $this->sendRpcRequest('getblockcount');
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getBlockTransactions()
     * @throws \Exception
     */
    public function getBlockTransactions(int $blockNo)
    {
        // To get the transactions by block number, we first need to fetch the block hash first, then query using the hash
        $blockHash = $this->sendRpcRequest('getblockhash', [$blockNo]);

        // The 2 means to get the transaction details also and return in JSON
        $blockDetails = $this->sendRpcRequest('getblock', [$blockHash, true]);

        return $blockDetails['tx'];
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTransactionDetails()
     * @throws \Exception
     */
    public function getTransactionDetails(string $transactionHash)
    {
        return $this->sendRpcRequest('getrawtransaction', [$transactionHash, 1]);
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getBalance()
     * @throws \Exception
     */
    public function getBalance(string $address)
    {
        $addressUnspents = $this->listUnspent([$address]);
        $balance         = '0';

        foreach ($addressUnspents as $key => $unspent) {
            $balance = bcadd($balance, $unspent['amount'], $this->getDecimalPlaces());
        }

        return $balance;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::transfer()
     * @throws \Exception
     */
    public function transfer(
        string $fromAddress,
        string $toAddress,
        string $amount,
        array $options = []
    ) {
        $txData = $this->generateInputsAndRawTransaction($fromAddress, $toAddress, $amount, $options);

        // The purpose of change address is to handle the unspent return to
        // If it is not configured, there is a risk of the remaining unspents change to the fee required for the transaction
        if (!array_key_exists('change_address', $options)) {
            throw new Exception('Change address is required for the unspent to be returned to.');
        }

        // If no private key is given, it means we should get the node to create the raw transaction and do the signing
        if (!array_key_exists('private_key', $options)) {
            // Fix for hot wallet withdrawals, have to get private keys for every inputs
            if (empty($fromAddress)) {
                $signer = new Signer($txData->transaction, Bitcoin::getEcAdapter());

                foreach ($txData->utxos as $idx => $utxo) {
                    $privateKey = (new PrivateKeyFactory)->fromWif($utxo->privateKey);
                    $signer->sign($idx, $privateKey, $utxo->utxoObject->getOutput());
                }

                $signedTransaction = $signer->get()->getHex();
            } else {
                $rawTransactionHex = $this->sendRpcRequest('createrawtransaction', [$txData->inputs, $txData->outputs]);
                $rpcResult         = $this->sendRpcRequest('signrawtransaction', [$rawTransactionHex]);

                if ($rpcResult['complete']) {
                    $signedTransaction = $rpcResult['hex'];
                } else {
                    throw new \Exception('Transaction signing error');
                }
            }
        } else { // Else we will create the raw transaction and sign ourselves
            $signer     = new Signer($txData->transaction, Bitcoin::getEcAdapter());
            $privateKey = (new PrivateKeyFactory)->fromWif($options['private_key']);

            foreach ($txData->utxos as $idx => $utxo) {
                $signer->sign($idx, $privateKey, $utxo->utxoObject->getOutput());
            }

            $signedTransaction = $signer->get()->getHex();
        }

        return $this->sendRpcRequest('sendrawtransaction', [$signedTransaction]);
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getDecimalPlaces()
     */
    public function getDecimalPlaces()
    {
        return 8;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract::listUnspent()
     */
    public function listUnspent($addresses, int $minConfirmations = 1, int $maxConfirmations = 9999999)
    {
        $rpcParams = [$minConfirmations, $maxConfirmations];

        if (!is_array($addresses)) {
            $addresses = [$addresses];
        }

        $addressesToQuery = [];

        foreach ($addresses as $address) {
            if (!empty($address)) {
                $addressesToQuery[] = $address;
            }
        }

        if (!empty($addressesToQuery)) {
            $rpcParams[] = $addressesToQuery;
        }

        $unspents = $this->sendRpcRequest('listunspent', $rpcParams);

        // The amounts may be in scientific notation. A lot of Bitcoin functions depend on this function. So fix here, the rest will also be ok.
        if (!empty($unspents)) {
            foreach ($unspents as &$unspent) {
                $unspent['amount'] = number_format($unspent['amount'], $this->getDecimalPlaces(), '.', '');
            }
        }

        return $unspents;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract::createRawTransaction()
     */
    public function createRawTransaction(array $inputs, array $outputs)
    {
        return $this->sendRpcRequest('createrawtransaction', [$inputs, $outputs]);
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract::sendRawTransaction()
     */
    public function sendRawTransaction(string $hexData)
    {
        return $this->sendRpcRequest('sendrawtransaction', [$hexData]);
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::validateAddress()
     *
     * @param string $address
     * @return bool
     */
    public function validateAddress(string $address)
    {
        $addressCreator = new AddressCreator();

        try {
            $addressCreator->fromString($address);
            return true;
        } catch (UnrecognizedAddressException $e) {
            return false;
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Block
     */
    public function getConfirmations(string $transactionHash)
    {
        $details = $this->getTransactionDetails($transactionHash);

        if (!empty($details)) {
            return $details['confirmations'];
        }

        throw new \Exception('details not found');
    }

    /**
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $amount
     * @param array $options
     *
     * @return object
     * @throws InsufficientBlockchainFundsException
     * @throws \BitWasp\Bitcoin\Exceptions\InvalidHashLengthException
     * @throws \BitWasp\Bitcoin\Exceptions\UnrecognizedAddressException
     * @throws \Exception
     */
    protected function generateInputsAndRawTransaction(string $fromAddress, string $toAddress, string $amount, array $options = [])
    {
        $masterWalletAddress    = setting('master_wallet_btc_address');
        $masterWalletPrivateKey = setting('master_wallet_btc_private_key');

        $addressCreator = new AddressCreator();

        $transaction = TransactionFactory::build()
            ->payToAddress($this->convertToFractionalUnit($amount), $addressCreator->fromString($toAddress));

        $decimalPlaces   = $this->getDecimalPlaces();
        $addressUnspents = $this->listUnspent([$fromAddress]);

        $numInputs     = 0;
        $inputTotal    = '0';
        $amountWithFee = '0';

        $utxos   = [];
        $inputs  = [];
        $outputs = [$toAddress => $amount];

        $this->logTransferOut('unspents: ' . count($addressUnspents));
        foreach ($addressUnspents as $key => $unspent) {
            // We cannot use spendable inputs, because we don't have the private keys in database, skip
            if ($unspent['spendable']) {
                continue;
            }

            $walletRepo = resolve(BlockchainWalletContract::class);
            $wallet     = $walletRepo->findByAddress($unspent['address'], $this->getBankCreditTypeId());

            if (empty($wallet)) {
                if ($unspent['address'] == $masterWalletAddress) {
                    $wallet = (object) [
                        'private_key' => $masterWalletPrivateKey,
                    ];
                } else {
                    continue;
                }
            }

            $numInputs++;

            // Register input, this object is only used if signing the transaction ourselves
            $transaction->input($unspent['txid'], $unspent['vout']);

            // Register input, this array is only used if signing using the node
            $inputs[] = [
                'txid'         => $unspent['txid'],
                'vout'         => $unspent['vout'],
                'scriptPubKey' => $unspent['scriptPubKey'],
            ];

            // Create UTXO object, which is only used if signing the transaction ourselves
            $utxos[] = (object) [
                'utxoObject' => new Utxo(
                    new OutPoint(Buffer::hex($unspent['txid'], 32), $unspent['vout']),
                    new TransactionOutput(0, ScriptFactory::fromHex($unspent['scriptPubKey']))
                ),
                'privateKey' => decrypt($wallet->private_key),
            ];

            $inputTotal = bcadd($inputTotal, $unspent['amount'], $decimalPlaces);

            // Re-calculate fee due to input size change
            $fee           = $this->calculateFee($numInputs, 2);
            $amountWithFee = bcadd($amount, $fee, $decimalPlaces);

            // If we now have enough inputs to satisfy tranaction, just break, no need to include any more inputs
            if (-1 != bccomp($inputTotal, $amountWithFee, $decimalPlaces)) {
                break;
            }
        }

        $this->logTransferOut('inputTotal' . $inputTotal);
        $this->logTransferOut('inputTotal' . $amountWithFee);
        $this->logTransferOut(bccomp($inputTotal, $amountWithFee, $decimalPlaces));
        $this->logTransferOut(is_zero($inputTotal));
        // The total inputs may not be enough, and total input cannot be zero. Check first before proceeding.
        if (-1 == bccomp($inputTotal, $amountWithFee, $decimalPlaces) || is_zero($inputTotal)) {
            throw new InsufficientBlockchainFundsException();
        }

        $change = bcsub($inputTotal, $amountWithFee, $decimalPlaces);

        // If a change address is given, use it
        if (array_key_exists('change_address', $options)) {
            $changeSatoshiAmount = $this->convertToFractionalUnit($change);
            $transaction->payToAddress($changeSatoshiAmount, $addressCreator->fromString($options['change_address']));
            $outputs[$options['change_address']] = $change;
        }

        return (object) [
            'transaction' => $transaction->get(),
            'utxos'       => $utxos,
            'inputs'      => $inputs,
            'outputs'     => $outputs,
        ];
    }

    /**
     * Calculate a reasonable transaction fee.
     *
     * @param int $numInputs
     * @param int $numOutputs
     * @return string
     */
    protected function calculateFee(int $numInputs, int $numOutputs)
    {
        /*
         * ------------------------
         * Reference to algorithm |
         * ------------------------
         * https://bitcoin.stackexchange.com/questions/1195/how-to-calculate-transaction-size-before-sending-legacy-non-segwit-p2pkh-p2sh
         */
        $satoshiPerByte      = setting('blockchain_usdt_withdrawal_satoshi_per_byte');
        $btcPerByte          = $this->convertToMainUnit($satoshiPerByte);
        $estimatedInputSize  = bcmul(strval($numInputs), '148', 0); // 148 bytes per input
        $estimatedOutputSize = bcmul(strval($numOutputs), '34', 0); // 34 bytes per output
        $totalSize           = bcadd($estimatedInputSize, $estimatedOutputSize, 0);
        $totalSize           = bcadd($totalSize, '10', 0); // fixed 10 bytes of data that's always present
        $totalSize           = bcadd($totalSize, strval($numInputs), 0); // refer to reference URL above for explanation of this step

        return bcmul($totalSize, $btcPerByte, $this->getDecimalPlaces());
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Traits\Js
     */
    protected function getBankCreditTypeId()
    {
        return bank_credit_type_id($this->getTicker());
    }
}
