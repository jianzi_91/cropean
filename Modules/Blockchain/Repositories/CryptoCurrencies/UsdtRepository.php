<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract;
use Modules\Blockchain\Traits\JsonRpcTrait;

/**
 * Service class for USDT.
 *
 * @package Plus65\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class UsdtRepository extends OmnicoreRepository implements OmnicoreContract
{
    use JsonRpcTrait;

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTicker()
     */
    public function getTicker()
    {
        return 'usdt_omni';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNetwork()
     */
    public function getNetwork()
    {
        return 'BTC';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Traits\Js
     *
     * @return int
     */
    protected function getBankCreditTypeId()
    {
        return bank_credit_type_id($this->getTicker());
    }
}
