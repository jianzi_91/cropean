<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;
use Modules\Blockchain\Models\BlockchainNode;
use Modules\Blockchain\Traits\ERC20TokenTrait;

/**
 * Service class for USDT (ERC20).
 *
 * @package Trading\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class UsdtErc20Repository extends AbstractERC20TokenRepository
{
    use ERC20TokenTrait;

    /**
     * Class constructor.
     *
     * @param NonceGeneratorContract $nonceGenerator
     * @param BlockchainTokenContract $erc20Service
     */
    public function __construct(NonceGeneratorContract $nonceGenerator, BlockchainTokenContract $erc20Service)
    {
        parent::__construct($erc20Service);
        $this->nonceGenerator = $nonceGenerator;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTicker()
     */
    public function getTicker()
    {
        return 'usdt_erc20';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNetwork()
     */
    public function getNetwork()
    {
        return 'ETH';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getNodes()
     */
    public function getNodes()
    {
        return BlockchainNode::where('network', $this->getNetwork())->get();
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getDecimalPlaces()
     */
    public function getDecimalPlaces()
    {
        return 6;//This changes must follow ERC20 contract decimal places
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Traits\Js
     */
    protected function getBankCreditTypeId()
    {
        return bank_credit_type_id($this->getTicker());
    }
}
