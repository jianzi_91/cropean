<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use BitWasp\Bitcoin\Base58;
use Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract;
use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use Modules\Blockchain\Models\BlockchainToken;
use Modules\Blockchain\Traits\JsonRpcTrait;
use Modules\Core\Traits\LogCronOutput;

/**
 * Service class for Omnicore.
 *
 * @package Plus65\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
abstract class OmnicoreRepository extends BitcoinRepository implements OmnicoreContract
{
    use JsonRpcTrait, LogCronOutput;

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract::getPropertyId()
     */
    public function getPropertyId()
    {
        return BlockchainToken::where('ticker', $this->getTicker())
            ->whereNotNull('property_id')
            ->whereNull('contract_address')
            ->firstOrFail()
            ->property_id;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getWalletTransactions()
     */
    public function getWalletTransactions(string $txId, int $count, int $skip, int $startBlock, int $endBlock)
    {
        return $this->sendRpcRequest('omni_listtransactions', [$txId, $count, $skip, $startBlock, $endBlock]);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getTransactionDetails()
     */
    public function getTransactionDetails(string $transactionHash)
    {
        return $this->sendRpcRequest('omni_gettransaction', [$transactionHash]);
    }

    /**
     * @inheritDoc
     *
     * @throws \Exception
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract::getWalletBitcoinBalance()
     */
    public function getWalletBitcoinBalance()
    {
        return number_format($this->sendRpcRequest('getbalance', ['*', 1, true]), 8, '.', '');
    }

    /**
     * @inheritDoc
     *
     * @throws \Exception
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\OmnicoreContract::getWalletBalance()
     */
    public function getWalletBalance()
    {
        $result = $this->sendRpcRequest('omni_getwalletbalances', [true]);

        foreach ($result as $token) {
            if ($token['propertyid'] == $this->getPropertyId()) {
                return number_format($token['balance'], 8, '.', '');
            }
        }

        return '0.00000000';
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getBalance()
     * @throws \Exception
     */
    public function getBalance(string $address)
    {
        try {
            $result = $this->sendRpcRequest('omni_getallbalancesforaddress', [$address]);

            foreach ($result as $token) {
                if ($token['propertyid'] == $this->getPropertyId()) {
                    return number_format($token['balance'], 8, '.', '');
                }
            }

            return '0.00000000';
        } catch (\Exception $e) {
            return '0.00000000';
        }
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::transfer()
     */
    public function transfer(
        string $fromAddress,
        string $toAddress,
        string $amount,
        array $options = []
    ) {
        // Reference for raw transaction: https://wiki.plus65host.net/index.php/Blockchain/USDT-RawTx
        // TODO: Current implementation only supports raw transaction. Maybe can support normal non-raw implmenetation also in future.

        // Step 1: Determine the unspent(s) that will be used for the fee
        $unspents    = $this->listUnspent($fromAddress);
        $feeRequired = '0';

        $unspentsToUse = [];
        $totalInputFee = '0';

        $satoshiPerByte = setting('blockchain_usdt_withdrawal_satoshi_per_byte');

        foreach ($unspents as $unspent) {
            $unspentsToUse[] = $unspent;

            // Omni transactions always have 3 outputs
            $feeRequired = $this->calculateFee(count($unspentsToUse), 3, $satoshiPerByte);

            $totalInputFee = bcadd($totalInputFee, $unspent['amount'], 8);

            if (-1 != bccomp($totalInputFee, $feeRequired, 8)) {
                break;
            }
        }

        // If we still do not have enough for fees, have to stop here
        if (-1 == bccomp($totalInputFee, $feeRequired, 8)) {
            throw new InsufficientBlockchainFundsException('not enough BTC to pay for fee');
        }

        // Create inputs to spend
        $inputs = [];

        foreach ($unspentsToUse as $unspent) {
            $inputs[] = (object) [
                'txid'         => $unspent['txid'],
                'vout'         => $unspent['vout'],
                'scriptPubKey' => $unspent['scriptPubKey'],
                'value'        => $unspent['amount'],
            ];
        }

        // Step 2, 3, 4 and 5 combined: Add Omni amount to Omni script
        $step5Output = $this->simulateCreateRawTransactionReference($inputs, $amount, $toAddress);
        $this->logTransferOut('USDT raw transaction data: ' . $step5Output);

        // Step 6: Creating out puts and charging fees
        // TODO: See if we can manually construct the hex instead of using RPC call
        $step6Output = $this->sendRpcRequest('omni_createrawtx_change', [$step5Output, $inputs, $fromAddress, $feeRequired]);

        // Step 7: Sign raw transaction
        $privateKeys = array_fill(0, count($inputs), $options['private_key']);
        $step7Output = $this->sendRpcRequest('signrawtransaction', [$step6Output, $inputs, $privateKeys]);

        if ($step7Output['complete']) {
            return [
                'hash'           => $this->sendRpcRequest('sendrawtransaction', [$step7Output['hex']]),
                'fee'            => $feeRequired,
                'satoshiPerByte' => $satoshiPerByte,
            ];
        }

        throw new \Exception('Private key signing error: ' . json_encode($step7Output));
    }

    /**
     * @inheritdoc
     *
     * @throws InsufficientBlockchainFundsException
     * @throws \BitWasp\Bitcoin\Exceptions\Base58InvalidCharacter
     * @throws \Exception
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::transfer()
     */
    public function redirect(
        string $fromAddress,
        string $toAddress,
        string $amount,
        array $options = []
    ) {
        // Reference for raw transaction: https://wiki.plus65host.net/index.php/Blockchain/USDT-RawTx
        // TODO: Current implementation only supports raw transaction. Maybe can support normal non-raw implmenetation also in future.

        // Step 1: Determine the unspent(s) that will be used for the fee
        $unspents    = $this->listUnspent($fromAddress);
        $feeRequired = '0';

        // Use just 1 unspent from the origin, the rest should take from the recipient, which is most likely master wallet
        if (0 == count($unspents)) {
            throw new \Exception("Unable to redirect $amount from $fromAddress to $toAddress due to no unspents for $fromAddress");
        }

        $unspentsToUse = [$unspents[0]];
        $totalInputFee = $unspents[0]['amount'];

        // Now use the unspents in the recipient address, which is most likely master wallet
        $unspents = $this->listUnspent($toAddress);

        $satoshiPerByte = setting('blockchain_usdt_redirection_satoshi_per_byte');

        foreach ($unspents as $unspent) {
            $unspentsToUse[] = $unspent;

            // Omni transactions always have 3 outputs
            $feeRequired = $this->calculateFee(count($unspentsToUse), 3, $satoshiPerByte);

            $totalInputFee = bcadd($totalInputFee, $unspent['amount'], 8);

            if (-1 != bccomp($totalInputFee, $feeRequired, 8)) {
                break;
            }
        }

        // If we still do not have enough for fees, have to stop here
        if (-1 == bccomp($totalInputFee, $feeRequired, 8)) {
            logger('something is wrong here, no money for fee');
            throw new InsufficientBlockchainFundsException('not enough BTC to pay for fee');
        }

        // Create inputs to spend
        $inputs = [];

        foreach ($unspentsToUse as $unspent) {
            $inputs[] = (object) [
                'txid'         => $unspent['txid'],
                'vout'         => $unspent['vout'],
                'scriptPubKey' => $unspent['scriptPubKey'],
                'value'        => $unspent['amount'],
            ];
        }

        // Step 2, 3, 4 and 5 combined: Add Omni amount to Omni script
        $step5Output = $this->simulateCreateRawTransactionReference($inputs, $amount, $toAddress);
        $this->logMasterWalletRedirection('USDT raw transaction data: ' . $step5Output);

        // Step 6: Creating out puts and charging fees
        // TODO: See if we can manually construct the hex instead of using RPC call
        $step6Output = $this->sendRpcRequest('omni_createrawtx_change', [$step5Output, $inputs, $toAddress, $feeRequired]);

        // Step 7: Sign raw transaction
        $privateKeys = array_fill(0, count($inputs) - 1, $options['receiver_private_key']);
        array_unshift($privateKeys, $options['private_key']);

        $step7Output = $this->sendRpcRequest('signrawtransaction', [$step6Output, $inputs, $privateKeys]);

        if ($step7Output['complete']) {
            return [
                'hash'           => $this->sendRpcRequest('sendrawtransaction', [$step7Output['hex']]),
                'fee'            => $feeRequired,
                'satoshiPerByte' => $satoshiPerByte,
            ];
        }

        throw new \Exception('Private key signing error: ' . json_encode($step7Output));
    }

    /**
     * @inheritdoc
     * @throws \Exception
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getConfirmations()
     */
    public function getConfirmations(string $transactionHash)
    {
        $details = $this->getTransactionDetails($transactionHash);

        if (!empty($details)) {
            return $details['confirmations'];
        }

        throw new \Exception('details not found');
    }

    /**
     * Simulate create raw transaction reference.
     *
     * @param array $inputs
     * @param string $amount
     * @param string $receiverAddress
     * @return string
     * @throws \BitWasp\Bitcoin\Exceptions\Base58InvalidCharacter
     * @throws \Exception
     */
    protected function simulateCreateRawTransactionReference(array $inputs, string $amount, string $receiverAddress)
    {
        $satoshiAmount = $this->convertToFractionalUnit($amount);

        $output = '01000000'; // 4 bytes version
        $output .= str_pad(bigDecToHex(count($inputs)), 2, '0', STR_PAD_LEFT); // number of inputs

        foreach ($inputs as $input) {
            $output .= pairInvert($input->txid);
            $output .= pairInvert(str_pad(bigDecToHex($input->vout), 8, '0', STR_PAD_LEFT));
            $output .= '00'; // script length, always 00 for Omnicore
            $output .= 'ffffffff'; // sequence, always ffffffff for Omnicore
        }

        $output .= '02'; // number of outputs, always 2 for Omnicore
        $output .= '0000000000000000'; // outputs, always zero for Omnicore
        $output .= '166a146f6d6e69'; // specify Omni, ASCII conversion
        $output .= str_pad(bigDecToHex($this->getPropertyId()), 16, '0', STR_PAD_LEFT); // asset ID in hex
        $output .= str_pad(bigDecToHex($satoshiAmount), 16, '0', STR_PAD_LEFT); // amount in Satoshi

        $addressFirstCharacter = substr($receiverAddress, 0, 1);
        if (config('app.env') == 'production') {
            if ($addressFirstCharacter == '1') {
                $output .= '2202000000000000'; // P2PKH
                $output .= '19'; // size of PK script
                $output .= '76'; // OP_Dup
                $output .= 'a9'; // OP_hash160
                $output .= '14'; // bytes to push
                $output .= substr(Base58::decode($receiverAddress)->getHex(), 2, 40);
                $output .= '88'; // OP_EqualVerify
                $output .= 'ac'; // OP_CheckSig
            } elseif ($addressFirstCharacter == '3') {
                $output .= '1c02000000000000'; // P2PSH
                $output .= '17'; // size of PK script
                $output .= 'a9'; // OP_hash160
                $output .= '14'; // bytes to push
                $output .= substr(Base58::decode($receiverAddress)->getHex(), 2, 40);
                $output .= '87'; // OP_Equal
            } else {
                throw new \Exception('Invalid receiver address in ' . config('app.env'));
            }
        } else {
            if ($addressFirstCharacter == 'm' || $addressFirstCharacter == 'n') {
                $output .= '2202000000000000'; // P2PKH
                $output .= '19'; // size of PK script
                $output .= '76'; // OP_Dup
                $output .= 'a9'; // OP_hash160
                $output .= '14'; // bytes to push
                $output .= substr(Base58::decode($receiverAddress)->getHex(), 2, 40);
                $output .= '88'; // OP_EqualVerify
                $output .= 'ac'; // OP_CheckSig
            } elseif ($addressFirstCharacter == '2') {
                $output .= '1c02000000000000'; // P2PSH
                $output .= '17'; // size of PK script
                $output .= 'a9'; // OP_hash160
                $output .= '14'; // bytes to push
                $output .= substr(Base58::decode($receiverAddress)->getHex(), 2, 40);
                $output .= '87'; // OP_Equal
            } else {
                throw new \Exception('Invalid receiver address in ' . config('app.env'));
            }
        }
        $output .= '00000000'; // block lock time

        return $output;
    }
}
