<?php

namespace Modules\Blockchain\Repositories\CryptoCurrencies;

use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\AbstractCryptoCurrencyRepository;

/**
 * Abstract class to implement common functions shared by all ERC20 tokens.
 *
 * @package Trading\Blockchain\Services
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
abstract class AbstractERC20TokenRepository extends AbstractCryptoCurrencyRepository
{
    /**
     * ERC20 token service.
     *
     * @var BlockchainTokenContract
     */
    protected $erc20Repo;

    /**
     * Class constructor.
     *
     * @param BlockchainTokenContract $erc20Repo
     */
    public function __construct(BlockchainTokenContract $erc20Repo)
    {
        $this->erc20Repo = $erc20Repo;
    }

    /**
     * @inheritDoc
     */
    public function getContractAddress()
    {
        return $this->erc20Repo->getContractAddress($this->getBankCreditTypeId());
    }

    /**
     * Get bank credit type ID.
     *
     * @return mixed
     */
    abstract protected function getBankCreditTypeId();
}
