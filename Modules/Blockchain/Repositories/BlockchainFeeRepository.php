<?php

namespace Modules\Blockchain\Repositories;

use Modules\Blockchain\Contracts\BlockchainFeeContract;
use Modules\Blockchain\Models\BlockchainFee;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class BlockchainFeeRepository extends Repository implements BlockchainFeeContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param BlockchainFee $model
     */
    public function __construct(BlockchainFee $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainFeeContract::getEthFees()
     */
    public function getEthFees(bool $isUpdated = false)
    {
        return $this->model
            ->where('bank_credit_type_id', bank_credit_type_id(BankCreditTypeContract::ETH))
            ->where('blockchain_fee', 0)
            ->get();
    }
}
