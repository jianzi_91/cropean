<?php

namespace Modules\Blockchain\Repositories;

use Illuminate\Support\Str;
use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Models\BlockchainWallet;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Traits\ValidateCredit;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BlockchainWalletRepository extends Repository implements BlockchainWalletContract
{
    use HasCrud, HasSlug, ValidateCredit;

    /**
     * Credit system type
     * @var string
     */
    protected $creditTypeRepo;

    /**
     * The bank account credit repository
     * @var unknown
     */
    protected $credit;

    /**
     * ERC20 token service.
     *
     * @var BlockchainTokenContract
     */
    protected $blockchainTokenRepo;

    /**
     * The backup wallet storage path
     * @var unknown
     */
    protected $storagePath;

    /**
     * The file system
     * @var unknown
     */
    protected $fileSystem;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Bitcoin repository.
     *
     * @var BitcoinContract
     */
    protected $btcRepo;

    /**
     * Class constructor
     *
     * @param BlockchainWallet $model
     * @param BankAccountCreditContract $credit
     * @param BitcoinContract $btcRepo
     * @param EthereumContract $ethRepo
     * @param BlockchainTokenContract $erc20Service
     */
    public function __construct(
        BlockchainWallet $model,
        BankAccountCreditContract $credit,
        BitcoinContract $btcRepo,
        EthereumContract $ethRepo,
        BlockchainTokenContract $erc20Service = null,
        BankCreditTypeContract $creditTypeRepo
    ) {
        parent::__construct($model);

        $this->credit              = $credit;
        $this->blockchainTokenRepo = $erc20Service;
        $this->slug                = 'address';
        $this->storagePath         = config('wallet.storage.private_key');
        $this->fileSystem          = app('files');
        $this->btcRepo             = $btcRepo;
        $this->ethRepo             = $ethRepo;
        $this->creditTypeRepo      = $creditTypeRepo;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::createWallets()
     */
    public function createWallets($userId)
    {
        $bankCreditTypes = $this->creditTypeRepo->all();

        foreach ($bankCreditTypes as $bankCreditType) {
            if ($bankCreditType->network) {
                if ($bankCreditType->network === $this->creditTypeRepo::BTC_NETWORK) {
                    $wallet = $this->btcRepo->createNewAddress();
                }

                if ($bankCreditType->network === $this->creditTypeRepo::ETH_NETWORK) {
                    $wallet = $this->ethRepo->createNewAddress();
                }

                $this->add([
                    'address'             => $wallet['address'],
                    'user_id'             => $userId,
                    'bank_credit_type_id' => $bankCreditType->id,
                    'private_key'         => encrypt($wallet['private_key']),
                    'qr_code'             => $this->generateQrCode($wallet['address']),
                ]);
            }
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::getBalanceByUser()
     */
    public function getBalanceByUser(int $userId)
    {
        return $this->credit->getBalanceByReference($userId, $this->creditType);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::getBalanceByAddress()
     */
    public function getBalanceByAddress(string $address)
    {
        $wallet = $this->findBySlug($address);

        if ($wallet) {
            return $this->getBalanceByUser($wallet->user_id);
        }

        return 0;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::getUserAddress()
     */
    public function getUserAddress(int $userId, array $with = [], int $bankCreditTypeId = 0)
    {
        $query = $this->model
            ->where('user_id', $userId)
            ->with($with);

        if (!empty($bankCreditTypeId)) {
            $query->where('bank_credit_type_id', $bankCreditTypeId);
        }

        return empty($bankCreditTypeId) ? $query->get() : $query->firstOrFail();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::transferByAddress()
     */
    public function transferByAddress(
        string $fromAddress,
        string $toAddress,
        string $amount,
        string $remarks = null,
        bool $isWithdrawal = true,
        bool $feeInclusive = false
    ) {
        // To be implemented by subclasses
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::transferByUser()
     */
    public function transferByUser(int $fromUserId, int $toUserId, string $amount, string $remarks = null)
    {
        // To be implemented by subclasses
    }

    /**
     * Get qr code
     *
     * @param unknown $walletId
     * @return bool
     */
    public function getQrCode($walletId)
    {
        $wallet = $this->model->findOrFail($walletId);
        if ($wallet->qr_code) {
            return $this->fileSystem->get($wallet->qr_code);
        }

        return false;
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::filterExistingAddresses()
     */
    public function filterExistingAddresses(array $addresses, int $bankCreditTypeId)
    {
        $query = $this->model
            ->newQuery()
            ->whereIn('address', $addresses)
            ->where('bank_credit_type_id', $bankCreditTypeId);

        return $query->pluck('user_id', 'address')->toArray();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::filterERC20Addresses()
     */
    public function filterERC20Addresses(array $addresses)
    {
        return $this->model
            ->newQuery()
            ->whereIn('address', $addresses)
            ->whereIn('bank_credit_type_id', $this->blockchainTokenRepo->getErc20CreditTypeIds())
            ->pluck('user_id', 'address')
            ->toArray();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::findByAddress()
     */
    public function findByAddress(string $address, int $bankCreditTypeId)
    {
        $query = $this->model
            ->newQuery()
            ->where('address', $address);

        if (0 < $bankCreditTypeId) {
            $query->where('bank_credit_type_id', $bankCreditTypeId);
        }

        return $query->first();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainWalletContract::findByUserId()
     */
    public function findByUserIdAndCreditType(int $userId, int $bankCreditTypeId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->where('bank_credit_type_id', $bankCreditTypeId)
            ->first();
    }

    public function getAllBalances(int $userId)
    {
        // TODO: Use credit repo
        $creditTypes = BankCreditType::where('is_active', true)
            ->where('is_admin_only', false)
            ->get();

        $balances = [];

        // TODO: getBalanceByReference() supports querying multiple types
        foreach ($creditTypes as $creditType) {
            $balances[] = (object) [
                'id'      => $creditType->id,
                'slug'    => $creditType->rawname,
                'name'    => $creditType->name,
                'balance' => $this->credit->getBalanceByReference($userId, $creditType->rawname),
            ];
        }

        return collect($balances);
    }

    /**
     * Generate QR code.
     *
     * @param string $text
     * @param int $size
     * @return string
     */
    public function generateQrCode($text, $size = 280)
    {
        $path = config('blockchain.storage.path_prefix') . config('blockchain.storage.qr_code');

        if (!$this->fileSystem->exists($path)) {
            $this->fileSystem->makeDirectory($path, 0775, true);
        }

        $fullpath = $path . DIRECTORY_SEPARATOR . Str::random(10) . '.svg';
        QrCode::size($size)->generate($text, $fullpath);

        return basename($fullpath);
    }
}
