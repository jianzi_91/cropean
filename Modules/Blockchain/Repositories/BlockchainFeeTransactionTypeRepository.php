<?php

namespace Modules\Blockchain\Repositories;

use Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract;
use Modules\Blockchain\Models\BlockchainFeeTransactionType;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BlockchainFeeTransactionTypeRepository extends Repository implements BlockchainFeeTransactionTypeContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor.
     *
     * @param BlockchainFeeTransactionType $model
     */
    public function __construct(BlockchainFeeTransactionType $model)
    {
        parent::__construct($model);
    }
}
