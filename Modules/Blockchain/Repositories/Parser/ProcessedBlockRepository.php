<?php

namespace Modules\Blockchain\Repositories\Parser;

use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;
use Modules\Blockchain\Models\ProcessedBlock;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

/**
 * Repository for processed blocks.
 *
 * @package Modules\BlockchainParser\Contracts
 */
class ProcessedBlockRepository extends Repository implements ProcessedBlockContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param ProcessedBlock $model
     */
    public function __construct(ProcessedBlock $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\ProcessedBlockContract::getLatestBlockNumber()
     */
    public function getLatestBlockNumber(string $blockProcessorType)
    {
        $latestRecord = $this->model
            ->newQuery()
            ->where('block_processor_type', $blockProcessorType)
            ->orderBy('block_number', 'DESC')
            ->limit(1)
            ->first(['block_number']);

        return empty($latestRecord) ? 0 : $latestRecord->block_number;
    }
}
