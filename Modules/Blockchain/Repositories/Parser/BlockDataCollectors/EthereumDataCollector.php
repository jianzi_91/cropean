<?php

namespace Modules\Blockchain\Repositories\Parser\BlockDataCollectors;

/**
 * Data collector for Ethereum transactions.
 *
 * @package Modules\BlockchainParser\Repositories\BlockDataCollectors
 */
class EthereumDataCollector extends AbstractDataCollector
{
    /**
     * Class constructor.
     *
     * @param array $dataHandlerClassNames
     */
    public function __construct(array $dataHandlerClassNames)
    {
        parent::__construct($dataHandlerClassNames);
    }

    /**
     * @inheritDoc
     * @see \Trading\BlockchainParser\Contracts\BlockDataCollector::collectTransactions()
     */
    public function collectTransactions(array $transactions, \stdClass $miscData = null)
    {
        $etherTransactionsByAddresses = [];

        foreach ($transactions as $transaction) {
            if (!array_key_exists($transaction->to, $etherTransactionsByAddresses)) {
                $etherTransactionsByAddresses[$transaction->to] = (object) [
                    'userId'       => 0,
                    'transactions' => [],
                ];
            }

            $etherTransactionsByAddresses[$transaction->to]->transactions[] = $transaction;
        }

        return $etherTransactionsByAddresses;
    }
}
