<?php

namespace Modules\Blockchain\Repositories\Parser\BlockDataCollectors;

use stdClass;

/**
 * Data collector for Bitcoin transactions
 *
 * @package Modules\BlockchainParser\Services\BlockDataCollectors
 */
class BitcoinDataCollector extends AbstractDataCollector
{
    /**
     * Class constructor.
     *
     * @param array $dataHandlerClassNames
     */
    public function __construct(array $dataHandlerClassNames)
    {
        parent::__construct($dataHandlerClassNames);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockDataCollector::collectTransactions()
     */
    public function collectTransactions(array $transactions, stdClass $miscData = null)
    {
        /**
         * ------------
         * | Strategy |
         * -----------
         *
         * We have a list of transactions that are confirmed, but we don't have to process every one of them, because
         * we are now only concerned with transactions that are received by addresses in our database. So for efficiency
         * purpose, what needs to be done is as follows:
         *
         * 1) Group all the transactions by using the recipient address as the key (i.e. the $transactionsByAddress variable).
         * 2) Extract all the keys (which are the unique addresses), and use 1 single SQL to find out which addresses exist in the database.
         * 3) Process only the transactions which the address exists in database. The rest, we can ignore. It's none of our business. :P
         *
         * Why go through all the data re-organization? Main reason is, we want to filter out the addresses that we are not interested
         * in by using only 1 single SQL. Imagine if we have 1000 transactions, then for each transaction, we query the database once to
         * check the existance of the address. This is extremely inefficient.
         */
        $transactionsByAddress = [];

        foreach ($transactions as $transaction) {
            foreach ($transaction->outputs as $output) {
                if (1 == count($output->recipients)) {
                    $blockNumber      = $transaction->blockNumber;
                    $recipientAddress = $output->recipients[0];
                    $value            = number_format($output->value, 8, '.', '');

                    if (!array_key_exists($recipientAddress, $transactionsByAddress)) {
                        $transactionsByAddress[$recipientAddress] = new stdClass();
                    }

                    // If a transaction has multiple outputs that point to the same recipient, we just update the same record and add the value.
                    if (array_key_exists($recipientAddress, $transactionsByAddress) && property_exists($transactionsByAddress[$recipientAddress], 'transactions')) {
                        foreach ($transactionsByAddress[$recipientAddress]->transactions as &$addressTransaction) {
                            if ($addressTransaction->blockNumber == $blockNumber && $addressTransaction->transactionHash == $transaction->transactionHash) {
                                $addressTransaction->value = bcadd($addressTransaction->value, $value, 8);
                                continue 2;
                            }
                        }
                    }

                    $transactionsByAddress[$recipientAddress]->transactions[] = (object) [
                        'blockNumber'     => $blockNumber,
                        'transactionHash' => $transaction->transactionHash,
                        'value'           => $value
                    ];
                }
            }
        }

        return $transactionsByAddress;
    }
}
