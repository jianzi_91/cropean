<?php

namespace Modules\Blockchain\Repositories\Parser\BlockDataCollectors;

use Modules\Blockchain\Traits\ERC20TokenAbiTrait;
use Modules\Blockchain\Traits\EthereumAddressTrait;
use Modules\Core\Traits\LogCronOutput;

/**
 * Data collector for ERC20 token transactions.
 *
 * @package Modules\BlockchainParser\Services\BlockDataCollectors
 */
class ERC20DataCollector extends AbstractDataCollector
{
    use EthereumAddressTrait, ERC20TokenAbiTrait, LogCronOutput;

    /**
     * Class constructor.
     *
     * @param array $dataHandlerClassNames
     */
    public function __construct(array $dataHandlerClassNames)
    {
        parent::__construct($dataHandlerClassNames);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockDataCollector::collectTransactions()
     * @throws \Exception
     */
    public function collectTransactions(array $transactions, \stdClass $miscData = null)
    {
        $erc20TransactionsByAddresses = [];

        foreach ($transactions as $transaction) {
            // If this is an ERC20 token transaction, parse it
            if (in_array($transaction->to, array_keys($miscData->erc20TokensInfo))) { // $miscData contains ERC20 tokens info, see EthereumBlockProcessor
                $this->logErc20Processor('transaction input' . $transaction->input);

                try {
                    $erc20TransactionInfo = $this->decodeAbi($transaction->input);

                    if (empty($erc20TransactionInfo)) {
                        $this->logErc20Processor('Transaction Info Empty: ' . $erc20TransactionInfo);
                        continue;
                    }
                } catch (\Exception $e) {
                    continue;
                }

                $erc20Service = $miscData->erc20TokensInfo[$transaction->to]->service;
                $this->logErc20Processor('$erc20TransactionInfo' . json_encode($erc20TransactionInfo));

                $erc20TransactionInfo->tokenType   = $miscData->erc20TokensInfo[$transaction->to];
                $erc20TransactionInfo->from        = $this->convertToChecksumEncoding($transaction->from);
                $erc20TransactionInfo->to          = $this->convertToChecksumEncoding($erc20TransactionInfo->to);
                $erc20TransactionInfo->blockNumber = $transaction->blockNumber;
                $erc20TransactionInfo->hash        = $transaction->hash;
                $erc20TransactionInfo->value       = $erc20Service->convertToMainUnit($erc20TransactionInfo->fractionalAmount);
                $erc20TransactionInfo->gas         = $transaction->gas;
                $erc20TransactionInfo->gasPrice    = trim_zero($transaction->gasPrice);

                if (!array_key_exists($erc20TransactionInfo->to, $erc20TransactionsByAddresses)) {
                    $erc20TransactionsByAddresses[$erc20TransactionInfo->to] = (object) [
                        'userId'       => 0,
                        'transactions' => [],
                    ];
                }

                $erc20TransactionsByAddresses[$erc20TransactionInfo->to]->transactions[] = $erc20TransactionInfo;
            }
        }

        return $erc20TransactionsByAddresses;
    }
}
