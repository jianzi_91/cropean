<?php

namespace Modules\Blockchain\Repositories\Parser\BlockDataCollectors;

use Modules\Blockchain\Contracts\Parser\BlockDataCollector;

/**
 * Abstract data collector for Bitcoin transactions
 *
 * @package Modules\Blockchain\Repositories\Parser\BlockDataCollectors
 */
abstract class AbstractDataCollector implements BlockDataCollector
{
    /**
     * Data handlers.
     *
     * @var array
     */
    protected $dataHandlers = [];

    /**
     * Class constructor.
     *
     * @param array $dataHandlerClassNames
     */
    public function __construct(array $dataHandlerClassNames)
    {
        foreach ($dataHandlerClassNames as $dataHandlerClassName) {
            $this->dataHandlers[] = resolve($dataHandlerClassName);
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockDataCollector::collectHandlerEvents()
     */
    final public function collectHandlerEvents(array $collectedData)
    {
        $handlerEvents = [];

        foreach ($this->dataHandlers as $dataHandler) {
            $filteredData  = $dataHandler->filter($collectedData);
            $handlerEvents = array_merge($handlerEvents, $dataHandler->processFilteredData($filteredData));
        }

        return $handlerEvents;
    }
}
