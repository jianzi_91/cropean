<?php

namespace Modules\Blockchain\Repositories\Parser\BlockDataCollectors;

use stdClass;

/**
 * Data collector for Omnicore transactions
 *
 * @package Modules\BlockchainParser\Repositories\BlockDataCollectors
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class OmnicoreDataCollector extends AbstractDataCollector
{
    /**
     * Class constructor.
     *
     * @param array $dataHandlerClassNames
     */
    public function __construct(array $dataHandlerClassNames)
    {
        parent::__construct($dataHandlerClassNames);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockDataCollector::collectTransactions()
     */
    public function collectTransactions(array $transactions, stdClass $miscData = null)
    {
        // For Omnicore, no need to do anything special at all, just return
        return $transactions;
    }
}
