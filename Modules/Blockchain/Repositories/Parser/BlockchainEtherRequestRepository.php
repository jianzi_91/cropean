<?php

namespace Modules\Blockchain\Repositories\Parser;

use Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract;
use Modules\Blockchain\Models\BlockchainEtherRequest;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

/**
 * Repository for processed blocks.
 *
 * @package Modules\BlockchainParser\Contracts
 */
class BlockchainEtherRequestRepository extends Repository implements BlockchainEtherRequestContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param BlockchainEtherRequest $model
     */
    public function __construct(BlockchainEtherRequest $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract::addressHasPendingRequest()
     */
    public function addressHasPendingRequest(string $address)
    {
        $numUnmined = $this->model
            ->where('erc20_wallet_address', $address)
            ->where('is_mined', false)
            ->count();

        return $numUnmined > 0;
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract::filterExistingHashes()
     */
    public function filterExistingHashes(array $hashes)
    {
        return $this->model
            ->whereIn('transaction_hash', $hashes)
            ->where('is_mined', false)
            ->pluck('id', 'transaction_hash')
            ->toArray();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Repositories\Parser\BlockchainEtherRequestContract::markHashAsMined()
     */
    public function markHashAsMined(string $hash)
    {
        $this->model
            ->where('transaction_hash', $hash)
            ->update(['is_mined' => true]);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Repositories\Parser\BlockchainEtherRequestContract::addressPendingRequest()
     */
    public function addressPendingRequest(string $address)
    {
        return  $this->model
            ->where('erc20_wallet_address', $address)
            ->where('is_mined', false)
            ->first();
    }
}
