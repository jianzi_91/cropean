<?php

namespace Modules\Blockchain\Repositories\Parser\BlockProcessors;

use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;
use Modules\Blockchain\Traits\ERC20TokenAbiTrait;
use Modules\Blockchain\Traits\EthereumAddressTrait;
use Modules\Blockchain\Traits\LogBlock;

/**
 * Process Ethereum transactions.
 *
 * @package Modules\Blockchain\Repositories\TransactionProcessors
 */
class EthereumBlockProcessor extends AbstractBlockProcessor
{
    use ERC20TokenAbiTrait, EthereumAddressTrait, LogBlock;

    /**
     * ERC20 token service.
     *
     * @var BlockchainTokenContract
     */
    protected $erc20Service;

    /**
     * Data collectors.
     *
     * @var array
     */
    protected $dataCollectors = [];

    /**
     * Class constructor.
     *
     * @param EthereumContract $cryptoService
     * @param BlockchainTokenContract $erc20Service
     * @param ProcessedBlockContract $processedBlockService
     * @param array $dataCollectorClassNames
     */
    public function __construct(
        EthereumContract $cryptoService,
        BlockchainTokenContract $erc20Service,
        ProcessedBlockContract $processedBlockService,
        array $dataCollectorClassNames
    ) {
        parent::__construct($cryptoService, $processedBlockService);
        $this->erc20Service = $erc20Service;

        foreach ($dataCollectorClassNames as $dataCollectorClassName) {
            $this->dataCollectors[] = resolve($dataCollectorClassName);
        }
    }

    /**
     * @inheritDoc
     * @see \Trading\Blockchain\Contracts\BlockProcessor::prepareTransactionsForBlock()
     * @throws \Exception
     */
    public function processBlock(int $blockNumber)
    {
        $transactions = $this->cryptoService->getBlockTransactions($blockNumber);

        $supportedErc20Tokens = [];
        $erc20Tokens          = $this->erc20Service->getModel()->whereIn('ticker', ['usdt_erc20', 'usdc'])->get();

        foreach ($erc20Tokens as $erc20Token) {
            // Convert to checksum encoding so that all other logic will be consistent
            $erc20Token->contract_address = $this->convertToChecksumEncoding($erc20Token->contract_address);

            $supportedErc20Tokens[$erc20Token->contract_address] = (object) [
                'bankCreditTypeId' => $erc20Token->bank_credit_type_id,
                'bankCreditType'   => bank_credit_type_name($erc20Token->bank_credit_type_id),
                'service'          => resolve($erc20Token->implementation_class),
            ];
        }

        $successfulTransactions = [];

        foreach ($transactions as &$transaction) {
            // Convert to object for syntax purposes
            $transaction = (object) $transaction;

            // Some of the outputs are in hex, so convert to decimal first
            $transaction->blockNumber = bigHexToDec($transaction->blockNumber);
            $transaction->value       = $this->cryptoService->convertToMainUnit(bigHexToDec($transaction->value));
            $transaction->gas         = bigHexToDec($transaction->gas);
            $transaction->gasPrice    = $this->cryptoService->convertToMainUnit(bigHexToDec($transaction->gasPrice));

            // Convert the from and to addresses to checksum encoding in order to match the database records
            $transaction->from = $this->convertToChecksumEncoding($transaction->from);
            // The to address may be null, because it will be null if the transaction is about creating a contract, check first
            $transaction->to = empty($transaction->to) ? null : $this->convertToChecksumEncoding($transaction->to);

            $successfulTransactions[] = $transaction;
        }

        $handlerEvents = [];

        // For every collector, simply collect the transactions they want, and let their respective handlers take care of the rest
        foreach ($this->dataCollectors as $collector) {
            $collectedTransactions = $collector->collectTransactions($successfulTransactions, (object) ['erc20TokensInfo' => $supportedErc20Tokens]);
            $handlerEvents         = array_merge($handlerEvents, $collector->collectHandlerEvents($collectedTransactions));
        }

        // Now fire all events that all the handlers want to broadcast
        foreach ($handlerEvents as $event) {
            event($event);
        }

        /*
         * If execution reaches here successfully, that means no exceptions are encountered during the event thrown above.
         * Now we can safely assume that the block has been processed successfully without any issues. Save the block record
         * into MongoDB to flag it as "processed".
         */
        $this->logBlock($this->getCryptoCurrencyService()->getTicker(), $blockNumber);
    }
}
