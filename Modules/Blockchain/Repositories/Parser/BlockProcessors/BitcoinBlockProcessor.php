<?php

namespace Modules\Blockchain\Repositories\Parser\BlockProcessors;

use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;
use Modules\Blockchain\Traits\LogBlock;

/**
 * Process Bitcoin transactions.ess
 *
 * @package Moduleas\Blockchain\Repositories\TransactionProcessors
 */
class BitcoinBlockProcessor extends AbstractBlockProcessor
{
    use LogBlock;

    /**
     * Data collectors.
     *
     * @var array
     */
    protected $dataCollectors = [];

    /**
     * Class constructor.
     *
     * @param BitcoinContract $bitcoinService
     * @param ProcessedBlockContract $processedBlockService
     * @param array $dataCollectorClassNames
     */
    public function __construct(BitcoinContract $bitcoinService, ProcessedBlockContract $processedBlockService, array $dataCollectorClassNames)
    {
        parent::__construct($bitcoinService, $processedBlockService);

        foreach ($dataCollectorClassNames as $dataCollectorClassName) {
            $this->dataCollectors[] = resolve($dataCollectorClassName);
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser\BlockProcessor::prepareTransactionsForBlock()
     */
    public function processBlock(int $blockNumber)
    {
        $transactions = $this->cryptoService->getBlockTransactions($blockNumber);

        $confirmedTransactions = [];

        foreach ($transactions as $txId) {
            $transaction = $this->cryptoService->getTransactionDetails($txId);

            if (array_key_exists('vout', $transaction)) {
                $outputs = [];

                if ($this->isOmniTransaction($transaction)) {
                    continue;
                }

                foreach ($transaction['vout'] as $output) {
                    if (array_key_exists('scriptPubKey', $output) && array_key_exists('addresses', $output['scriptPubKey'])) {
                        $outputs[] = (object) [
                            'value'      => $output['value'],
                            'recipients' => $output['scriptPubKey']['addresses'],
                        ];
                    }
                }

                if (0 < count($outputs)) {
                    $confirmedTransactions[] = (object) [
                        'blockNumber'     => $blockNumber,
                        'transactionHash' => $transaction['txid'],
                        'outputs'         => $outputs,
                    ];
                }
            }
        }

        $handlerEvents = [];

        // For every collector, simply collect the transactions they want, and let their respective handlers take care of the rest
        foreach ($this->dataCollectors as $collector) {
            $collectedTransactions = $collector->collectTransactions($confirmedTransactions, (object) ['blockNumber' => $blockNumber]);
            $handlerEvents         = array_merge($handlerEvents, $collector->collectHandlerEvents($collectedTransactions));
        }

        // Now fire all events that all the handlers want to broadcast
        foreach ($handlerEvents as $event) {
            event($event);
        }

        /*
         * If execution reaches here successfully, that means no exceptions are encountered durig the event thrown above.
         * Now we can safely assume that the block has been processed successfully without any issues. Time to throw another
         * event now. But first, save the block record into MongoDB.
         */
        $this->logBlock($this->getCryptoCurrencyService()->getTicker(), $blockNumber);
    }

    /**
     * Check if transaction is Omni. If yes, we should ignore.
     *
     * @param $transaction
     * @return bool
     */
    public function isOmniTransaction($transaction)
    {
        if (3 != count($transaction['vout'])) {
            return false;
        }

        foreach ($transaction['vout'] as $output) {
            if (array_key_exists('scriptPubKey', $output) && array_key_exists('asm', $output['scriptPubKey']) && false !== strpos($output['scriptPubKey']['asm'], 'OP_RETURN 6f6d6e69')) {
                return true;
            }
        }

        return false;
    }
}
