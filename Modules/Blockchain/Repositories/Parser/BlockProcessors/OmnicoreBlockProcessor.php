<?php

namespace Modules\Blockchain\Repositories\Parser\BlockProcessors;

use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\Blockchain\Traits\LogBlock;

/**
 * Process Omnicore transactions.
 *
 * @package Modules\BlockchainParser\Repositories\BlockProcessors
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class OmnicoreBlockProcessor extends AbstractBlockProcessor
{
    use LogBlock;

    /**
     * Data collectors.
     *
     * @var array
     */
    protected $dataCollectors = [];

    /**
     * Class constructor.
     *
     * @param UsdtRepository $omnicoreService
     * @param ProcessedBlockContract $processedBlockService
     * @param array $dataCollectorClassNames
     */
    public function __construct(UsdtRepository $omnicoreService, ProcessedBlockContract $processedBlockService, array $dataCollectorClassNames)
    {
        parent::__construct($omnicoreService, $processedBlockService);

        foreach ($dataCollectorClassNames as $dataCollectorClassName) {
            $this->dataCollectors[] = resolve($dataCollectorClassName);
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser::prepareTransactionsForBlock()
     */
    public function processBlock(int $blockNumber)
    {
        $transactions = $this->cryptoService->getWalletTransactions('*', 1000000, 0, $blockNumber, $blockNumber);

        $handlerEvents = [];

        // For every collector, simply collect the transactions they want, and let their respective handlers take care of the rest
        foreach ($this->dataCollectors as $collector) {
            $collectedTransactions = $collector->collectTransactions($transactions, (object) ['blockNumber' => $blockNumber]);
            $handlerEvents         = array_merge($handlerEvents, $collector->collectHandlerEvents($collectedTransactions));
        }

        // Now fire all events that all the handlers want to broadcast
        foreach ($handlerEvents as $event) {
            event($event);
        }

        /*
         * If execution reaches here successfully, that means no exceptions are encountered durig the event thrown above.
         * Now we can safely assume that the block has been processed successfully without any issues. Time to throw another
         * event now. But first, save the block record.
         */
        $this->logBlock($this->getCryptoCurrencyService()->getTicker(), $blockNumber);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\Parser::getMinimumConfirmationsRequired()
     */
    public function getMinimumConfirmationsRequired()
    {
        return 1;
    }
}
