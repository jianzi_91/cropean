<?php

namespace Modules\Blockchain\Repositories\Parser\BlockProcessors;

use Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract;
use Modules\Blockchain\Contracts\Parser\BlockProcessor;
use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;

/**
 * Abstract block processor.
 *
 * @package Modules\Blockchain\Repositories\TransactionProcessors
 */
abstract class AbstractBlockProcessor implements BlockProcessor
{
    /**
     * Cryptocurrency service.
     *
     * @var CryptoCurrencyContract
     */
    protected $cryptoService;

    /**
     * Processed block service.
     *
     * @var ProcessedBlockContract
     */
    protected $processedBlockService;

    /**
     * Class constructor.
     *
     * @param CryptoCurrencyContract $cryptoService
     * @param ProcessedBlockContract $processedBlockService
     */
    public function __construct(CryptoCurrencyContract $cryptoService, ProcessedBlockContract $processedBlockService)
    {
        $this->cryptoService         = $cryptoService;
        $this->processedBlockService = $processedBlockService;
    }

    /**
     * @inheritDoc
     * @see \Trading\Blockchain\Contracts\BlockProcessor::getCryptoCurrencyService()
     */
    final public function getCryptoCurrencyService()
    {
        return $this->cryptoService;
    }

    /**
     * @inheritDoc
     * @see \Trading\Blockchain\Contracts\BlockProcessor::getMinimumConfirmationsRequired()
     */
    public function getMinimumConfirmationsRequired()
    {
        return config('blockchain.confirmations_required')[$this->cryptoService->getTicker()];
    }

    /**
     * @inheritDoc
     * @see \Trading\Blockchain\Contracts\BlockProcessor::getLatestProcessedBlockNumber()
     */
    public function getLatestProcessedBlockNumber()
    {
        return $this->processedBlockService->getLatestBlockNumber($this->cryptoService->getTicker());
    }
}
