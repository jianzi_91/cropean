<?php

namespace Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors;

use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\Parser\MasterWalletRedirectorContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\ScgcRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;

class UsdtRedirector implements MasterWalletRedirectorContract
{
    /**
     * USDT repository.
     *
     * @var ScgcRepository
     */
    protected $usdtRepo;

    /**
     * Wallet repository.
     *
     * @var WalletContract
     */
    protected $walletRepo;

    /**
     * Fee.
     *
     * @var string
     */
    protected $fee = '0';

    /**
     * Class constructor.
     *
     * @param UsdtRepository $usdtRepo
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(UsdtRepository $usdtRepo, BlockchainWalletContract $walletContract)
    {
        $this->usdtRepo   = $usdtRepo;
        $this->walletRepo = $walletContract;
    }

    /**
     * Redirect.
     *
     * @param int $userId
     * @param string $amount
     * @return array|mixed
     * @throws \BitWasp\Bitcoin\Exceptions\Base58InvalidCharacter
     * @throws \Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException
     */
    public function redirect(int $userId, string $amount)
    {
        $wallet = $this->walletRepo->findByUserIdAndCreditType($userId, bank_credit_type_id(BankCreditTypeContract::USDT_OMNI));

        if (!empty($wallet)) {
            $info = $this->usdtRepo->redirect(
                $wallet->address,
                setting('master_wallet_btc_address'),
                $amount,
                ['private_key' => decrypt($wallet->private_key), 'receiver_private_key' => decrypt(setting('master_wallet_btc_private_key'))]
            );

            $this->fee = $info['fee'];

            return $info['hash'];
        }
    }

    /**
     * Get fee.
     *
     * @return mixed|string
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Get minimum deposit amount to be redirected
     */
    public function getMinimumRedirectionAmount()
    {
        return setting('minimum_usdt_omni_deposit_redirection_amount', 0.1);
    }
}
