<?php

namespace Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors;

use Illuminate\Support\Facades\Mail;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract;
use Modules\Blockchain\Contracts\Parser\MasterWalletRedirectorContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;

class UsdcRedirector implements MasterWalletRedirectorContract
{
    use LogCronOutput;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * USDT ERC20 repository.
     *
     * @var UsdcRepository
     */
    protected $usdcRepo;

    /**
     * Ether request repository.
     *
     * @var BlockchainEtherRequestContract
     */
    protected $etherRequestRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param EthereumContract $ethRepo
     * @param UsdcRepository $usdcRepo
     * @param BlockchainEtherRequestContract $etherRequestContract
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(
        EthereumContract $ethRepo,
        UsdcRepository $usdcRepo,
        BlockchainEtherRequestContract $etherRequestContract,
        BlockchainWalletContract $walletContract
    ) {
        $this->ethRepo          = $ethRepo;
        $this->usdcRepo         = $usdcRepo;
        $this->etherRequestRepo = $etherRequestContract;
        $this->walletRepo       = $walletContract;
    }

    /**
     * @inheritDoc
     * @throws \Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException
     * @throws \Exception
     */
    public function redirect(int $userId, string $amount)
    {
        $ethMasterWalletAddress    = setting('master_wallet_eth_address');
        $ethMasterWalletPrivateKey = decrypt(setting('master_wallet_eth_private_key'));

        $usdcMasterWalletAddress = setting('master_wallet_usdc_address');

        $userWallet = $this->walletRepo->findByUserIdAndCreditType($userId, bank_credit_type_id(BankCreditTypeContract::USDC));

        if (!empty($userWallet)) {
            /*
             * In order to do a balance transfer to hot wallet, we need ethers. If the ERC20 address has sufficient ethers,
             * we just send the entire ERC20 balance to hot wallet immediately. Else we need to request for ethers from the
             * hot wallet.
             *
             * If even the hot wallet has insufficient ethers, we need to send an email to notify.
             */

            // Does address have enough ethers? First calculate the minimum we need.
            $gasLimit            = config('blockchain.erc20_gas_limit'); // It's rough, because this is just an estimate, but make it higher to be safe
            $minAmountRequired   = bcmul($this->ethRepo->getGasPrice('dec'), $gasLimit, 0);
            $currentEtherBalance = $this->ethRepo->getBalance($userWallet->address);
            $currentWeiBalance   = $this->ethRepo->convertToFractionalUnit($currentEtherBalance);

            if (1 == bccomp($minAmountRequired, $currentWeiBalance, $this->ethRepo->getDecimalPlaces())) {
                $this->logMasterWalletRedirection("Wallet redirection Job: {$userWallet->address} does not have enough ethers to redirect his USDT ERC20 balance to master wallet.");
                $this->logMasterWalletRedirection("Wallet redirection Job: Going to request for ethers from ETH master wallet $ethMasterWalletAddress.");

                // If address currently has an ether request in progress, don't do anything further.
                $etherRequest = $this->etherRequestRepo->addressPendingRequest($userWallet->address);

                if ($etherRequest) {
                    if (!empty($etherRequest->transaction_hash)) {
                        $this->logMasterWalletRedirection("Wallet redirection Job: $ethMasterWalletAddress currently has at least 1 ether request in progress. Not going to request again.");
                        return;
                    }
                }

                $shortageWei   = bcsub($minAmountRequired, $currentWeiBalance);
                $shortageEther = $this->ethRepo->convertToMainUnit($shortageWei);

                // Check if the master wallet has enough money
                $masterWalletBalance = $this->ethRepo->getBalance($ethMasterWalletAddress);

                // Create a request
                if (empty($etherRequest)) {
                    $etherRequest = $this->etherRequestRepo->add([
                        'bank_credit_type_id'       => bank_credit_type_id(BankCreditTypeContract::USDC),
                        'erc20_wallet_address'      => $userWallet->address,
                        'eth_master_wallet_address' => $ethMasterWalletAddress,
                        'request_amount'            => $shortageEther,
                    ]);
                }

                if (1 == bccomp($shortageEther, $masterWalletBalance, $this->ethRepo->getDecimalPlaces())) {
//                    Mail::send(new InsufficientHotWalletBalanceMail());
                    $this->logMasterWalletRedirection("Master wallet does not have enough money for an ether request.");
                    return;
                }

                // If execution reaches here, that means hot wallet has enough balance, transfer now
                $transactionHash = $this->ethRepo->transfer(
                    $ethMasterWalletAddress,
                    $userWallet->address,
                    $shortageEther,
                    ['private_key' => $ethMasterWalletPrivateKey]
                );

                $etherRequest->update(['transaction_hash' => $transactionHash]);
                $this->logMasterWalletRedirection("Wallet redirection Job: Ether request has been submitted with hash $transactionHash");

                return;
            }

            // If execution reaches here, that means actually the ERC20 address has enough ethers. Just redirect straight.
            return $this->usdcRepo->transfer($userWallet->address, $usdcMasterWalletAddress, $amount, ['private_key' => decrypt($userWallet->private_key)]);
        }
    }

    /**
     * Get fee.
     *
     * @return mixed|string
     */
    public function getFee()
    {
        return '0';
    }

    /**
     * Get minimum deposit amount to be redirected
     */
    public function getMinimumRedirectionAmount()
    {
        return setting('minimum_usdc_deposit_redirection_amount', 0.1);
    }
}
