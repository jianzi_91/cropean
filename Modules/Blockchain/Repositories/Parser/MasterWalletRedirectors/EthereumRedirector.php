<?php

namespace Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors;

use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\MasterWalletRedirectorContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;

class EthereumRedirector implements MasterWalletRedirectorContract
{
    use LogCronOutput;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param EthereumContract $ethRepo
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(
        EthereumContract $ethRepo,
        BlockchainWalletContract $walletContract
    ) {
        $this->ethRepo    = $ethRepo;
        $this->walletRepo = $walletContract;
    }

    /**
     * @inheritDoc
     * @throws \Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException
     * @throws \Exception
     */
    public function redirect(int $userId, string $amount)
    {
        $ethMasterWalletAddress = setting('master_wallet_eth_address');

        $userWallet = $this->walletRepo->findByUserIdAndCreditType($userId, bank_credit_type_id(BankCreditTypeContract::ETH));

        if (!empty($userWallet)) {

            // Does address have enough ethers? First calculate the minimum we need.
            $gasLimit            = config('blockchain.erc20_gas_limit'); // It's rough, because this is just an estimate, but make it higher to be safe
            $minAmountRequired   = bcmul($this->ethRepo->getGasPrice('dec'), $gasLimit, 0);
            $currentEtherBalance = $this->ethRepo->getBalance($userWallet->address);
            $currentWeiBalance   = $this->ethRepo->convertToFractionalUnit($currentEtherBalance);

            if (1 == bccomp($minAmountRequired, $currentWeiBalance, $this->ethRepo->getDecimalPlaces())) {
                $this->logMasterWalletRedirection("Wallet redirection Job: {$userWallet->address} does not have enough ethers to redirect his ETH to master wallet.");
                return;
            }

            // If execution reaches here, that means actually the ERC20 address has enough ethers. Just redirect straight.
            return $this->ethRepo->transfer($userWallet->address, $ethMasterWalletAddress, $amount, ['private_key' => decrypt($userWallet->private_key), 'fee_inclusive' => true]);
        }
    }

    /**
     * Get fee.
     *
     * @return mixed|string
     */
    public function getFee()
    {
        return '0';
    }

    /**
     * Get minimum deposit amount to be redirected
     */
    public function getMinimumRedirectionAmount()
    {
        return setting('minimum_eth_deposit_redirection_amount', 0.1);
    }
}
