<?php

namespace Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors;

use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\Parser\MasterWalletRedirectorContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\ScgcRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;

class ScgcRedirector implements MasterWalletRedirectorContract
{
    /**
     * SCGC repository.
     *
     * @var ScgcRepository
     */
    protected $scgcRepo;

    /**
     * Wallet repository.
     *
     * @var BlockchainWalletContract
     */
    protected $walletRepo;

    /**
     * Class constructor.
     *
     * @param ScgcRepository $scgcRepo
     * @param BlockchainWalletContract $walletContract
     */
    public function __construct(ScgcRepository $scgcRepo, BlockchainWalletContract $walletContract)
    {
        $this->scgcRepo   = $scgcRepo;
        $this->walletRepo = $walletContract;
    }

    /**
     * @inheritDoc
     */
    public function redirect(int $userId, string $amount)
    {
        $wallet = $this->walletRepo->findByUserIdAndCreditType($userId, bank_credit_type_id(BankCreditTypeContract::SCGC));

        if (!empty($wallet)) {
            return $this->scgcRepo->transfer($wallet->address, setting('master_wallet_scgc_embargo_address'), $amount, ['private_key' => decrypt($wallet->private_key)]);
        }
    }

    /**
     * Get fee.
     *
     * @return mixed|string
     */
    public function getFee()
    {
        return $this->fee;
    }
}
