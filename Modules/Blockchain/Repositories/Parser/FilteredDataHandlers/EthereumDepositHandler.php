<?php

namespace Modules\Blockchain\Repositories\Parser\FilteredDataHandlers;

use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract;
use Modules\Blockchain\Traits\EthereumAddressTrait;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

class EthereumDepositHandler implements FilteredDataHandlerContract
{
    use EthereumAddressTrait, LogCronOutput;

    /**
     * Bank credit type repository.
     *
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepo;

    /**
     * Credit service.
     *
     * @var BankAccountCreditContract
     */
    protected $creditService;

    /**
     * Deposit service.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Reference number generator
     *
     * @var SimpleGenerator
     */
    protected $referenceNumberGenerator;

    /**
     * Wallet service.
     *
     * @var WalletContract
     */
    protected $walletService;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Class constructor.
     *
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BlockchainDepositContract $depositService \
     * @param SimpleGenerator $referenceNumberGenerator
     * @param BlockchainWalletContract $walletService
     * @param EthereumContract $ethereumContract
     */
    public function __construct(
        BankCreditTypeContract $bankCreditTypeContract,
        BlockchainDepositContract $depositService,
        SimpleGenerator $referenceNumberGenerator,
        BlockchainWalletContract $walletService,
        EthereumContract $ethereumContract
    ) {
        $this->bankCreditTypeRepo       = $bankCreditTypeContract;
        $this->depositService           = $depositService;
        $this->referenceNumberGenerator = $referenceNumberGenerator;
        $this->walletService            = $walletService;
        $this->ethRepo                  = $ethereumContract;
    }

    /**
     * Filter out transactions that we are not interested in.
     *
     * @param array $transactions
     * @return array
     */
    public function filter(array $transactions)
    {
        $addressesToProcess = $this->walletService->filterExistingAddresses(array_keys($transactions), bank_credit_type_id($this->bankCreditTypeRepo::ETH));

        if (empty($addressesToProcess)) {
            return [];
        }

        $filteredTransactionsByAddress = array_intersect_key($transactions, array_flip(array_keys($addressesToProcess)));

        foreach ($filteredTransactionsByAddress as $address => &$data) {
            $data->userId = $addressesToProcess[$address];
        }

        return $filteredTransactionsByAddress;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract::processFilteredData()
     * @throws \Exception
     */
    public function processFilteredData(array $filteredTransactions)
    {
        if (empty($filteredTransactions)) {
            $this->logErc20Processor("No ETH deposits can be processed.");
            return [];
        }

        $this->logErc20Processor("ETH deposits found! The following deposits will be credited:");

        $events = [];

        foreach ($filteredTransactions as $address => $data) {
            $this->logErc20Processor("Address $address (User ID {$data->userId})");
            $this->logErc20Processor('------------------------------------------------------------');

            foreach ($data->transactions as $transaction) {
                $receipt = $this->ethRepo->getTransactionReceipt($transaction->hash);

                // Ensure transaction is valid first
                if ('0x0' === $receipt->status) {
                    $this->logErc20Processor("Invalid transaction hash {$transaction->hash}, not going to process this hash, continue...");
                    continue;
                }

                $gasUsed = bigHexToDec($receipt->gasUsed);
                $fee     = bcmul($gasUsed, $transaction->gasPrice, 18);

                $blockNumber = $transaction->blockNumber;
                $value       = $transaction->value;
                $hash        = $transaction->hash;
                $from        = $transaction->from;
                $to          = $transaction->to;

                $this->insertDepositRecord(
                    $data->userId,
                    $blockNumber,
                    $from,
                    $to,
                    $value,
                    $fee,
                    $hash,
                    bank_credit_type_id(BankCreditTypeContract::ETH)
                );
            }
        }

        $this->logErc20Processor("End of ETH deposits");

        return $events;
    }

    /**
     * Insert deposit records.
     *
     * @param int $userId
     * @param int $block
     * @param string $from
     * @param string $to
     * @param string $value
     * @param string $fee
     * @param string $transactionHash
     * @param int $bankCreditTypeId
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model
     */
    protected function insertDepositRecord(int $userId, int $block, string $from, string $to, string $value, string $fee, string $transactionHash, int $bankCreditTypeId)
    {
        $creditType = $this->bankCreditTypeRepo->find($bankCreditTypeId);

        return $this->depositService->add([
            'reference_number'           => $this->referenceNumberGenerator->generate('DEP'),
            'user_id'                    => $userId ?: null,
            'deposit_credit_type_id'     => $bankCreditTypeId,
            'destination_credit_type_id' => $creditType->deposit_credit_type_id,
            'block'                      => $block,
            'transaction_hash'           => $transactionHash,
            'sending_address'            => $from,
            'receiving_address'          => $to,
            'amount'                     => $value,
            'blockchain_fee'             => $fee,
            'confirmations'              => '5',
        ]);
    }
}
