<?php

namespace Modules\Blockchain\Repositories\Parser\FilteredDataHandlers;

use Illuminate\Database\Eloquent\Model;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

/**
 * Processor for handling master wallet deposits.
 *
 */
class ERC20MasterWalletDepositHandler implements FilteredDataHandlerContract
{
    use LogCronOutput;

    /**
     * Credit service.
     *
     * @var BankAccountCreditContract
     */
    protected $creditService;

    /**
     * Bank credit type repo.
     *
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepo;

    /**
     * Bank transaction type service.
     *
     * @var BankTransactionTypeContract
     */
    protected $transactionTypeService;

    /**
     * Deposit service.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Reference number generator
     *
     * @var SimpleGenerator
     */
    protected $referenceNumberGenerator;

    /**
     * Wallet service.
     *
     * @var BlockchainWalletContract
     */
    protected $walletService;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Class constructor.
     *
     * @param BankAccountCreditContract $creditService
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BankTransactionTypeContract $transactionTypeService
     * @param BlockchainDepositContract $depositService
     * @param SimpleGenerator $referenceNumberGenerator
     * @param BlockchainWalletContract $walletService
     * @param EthereumContract $ethereumContract
     */
    public function __construct(
        BankAccountCreditContract $creditService,
        BankCreditTypeContract $bankCreditTypeContract,
        BankTransactionTypeContract $transactionTypeService,
        BlockchainDepositContract $depositService,
        SimpleGenerator $referenceNumberGenerator,
        BlockchainWalletContract $walletService,
        EthereumContract $ethereumContract
    ) {
        $this->creditService            = $creditService;
        $this->bankCreditTypeRepo       = $bankCreditTypeContract;
        $this->transactionTypeService   = $transactionTypeService;
        $this->depositService           = $depositService;
        $this->referenceNumberGenerator = $referenceNumberGenerator;
        $this->walletService            = $walletService;
        $this->ethRepo                  = $ethereumContract;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract::filter()
     */
    public function filter(array $transactions)
    {
        $addressesToProcess = [
            setting('master_wallet_usdt_erc20_address') => 0,
        ];

        if (empty($addressesToProcess)) {
            return [];
        }

        $filteredTransactionsByAddress = array_intersect_key($transactions, array_flip(array_keys($addressesToProcess)));

        foreach ($filteredTransactionsByAddress as $address => &$data) {
            $data->userId = $addressesToProcess[$address];
        }

        return $filteredTransactionsByAddress;
    }

    /**
     * Process filtered data.
     *
     * @param array $filteredTransactions
     * @return mixed
     * @throws \Exception
     */
    public function processFilteredData(array $filteredTransactions)
    {
        if (empty($filteredTransactions)) {
            $this->logErc20Processor('No ERC20 master wallet transactions can be processed.');
            return [];
        }

        $this->logErc20Processor('Master wallet deposits found! The following deposits will be credited:');

        $events = [];

        foreach ($filteredTransactions as $address => $data) {
            $this->logErc20Processor("Address $address");
            $this->logErc20Processor('------------------------------------------------------------');

            foreach ($data->transactions as $transaction) {
                $receipt = $this->ethRepo->getTransactionReceipt($transaction->hash);

                // Ensure transaction is valid first
                if ('0x0' === $receipt->status) {
                    $this->logErc20Processor("Invalid transaction hash {$transaction->hash}, not going to process this hash, continue...");
                    continue;
                }

                $gasUsed = bigHexToDec($receipt->gasUsed);
                $fee     = bcmul($gasUsed, $transaction->gasPrice, 18);

                $blockNumber      = $transaction->blockNumber;
                $from             = $transaction->from;
                $to               = $transaction->to;
                $value            = $transaction->value;
                $hash             = $transaction->hash;
                $bankCreditTypeId = $transaction->tokenType->bankCreditTypeId;

                $this->logErc20Processor("  - Block $blockNumber, Value $value, Transaction hash $hash");

                $this->insertDepositRecord($data->userId, $blockNumber, $from, $to, $value, $fee, $hash, $bankCreditTypeId);
            }
        }

        $this->logErc20Processor('End of ERC20 deposits');

        return $events;
    }

    /**
     * Insert deposit records.
     *
     * @param int $userId
     * @param int $block
     * @param string $from
     * @param string $to
     * @param string $value
     * @param string $fee
     * @param string $transactionHash
     * @param int $bankCreditTypeId
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model
     */
    protected function insertDepositRecord(int $userId, int $block, string $from, string $to, string $value, string $fee, string $transactionHash, int $bankCreditTypeId)
    {
        $creditType = $this->bankCreditTypeRepo->find($bankCreditTypeId);

        return $this->depositService->add([
            'reference_number'           => $this->referenceNumberGenerator->generate('DEP'),
            'user_id'                    => $userId ?: null,
            'deposit_credit_type_id'     => $bankCreditTypeId,
            'destination_credit_type_id' => $creditType->deposit_credit_type_id,
            'block'                      => $block,
            'transaction_hash'           => $transactionHash,
            'sending_address'            => $from,
            'receiving_address'          => $to,
            'amount'                     => $value,
            'blockchain_fee'             => $fee,
            'confirmations'              => '5',
            'is_master_wallet'           => true,
        ]);
    }
}
