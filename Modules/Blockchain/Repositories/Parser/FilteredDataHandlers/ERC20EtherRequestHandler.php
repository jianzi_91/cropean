<?php

namespace Modules\Blockchain\Repositories\Parser\FilteredDataHandlers;

use Modules\Blockchain\Contracts\BlockchainFeeContract;
use Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Contracts\Parser\BlockchainEtherRequestContract;
use Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

/**
 * Processor for handling user deposits.
 *
 */
class ERC20EtherRequestHandler implements FilteredDataHandlerContract
{
    use LogCronOutput;

    /**
     * Ether request repository.
     *
     * @var BlockchainEtherRequestContract
     */
    protected $etherRequestRepo;

    /**
     * Blockchain fee repository.
     *
     * @var BlockchainFeeContract
     */
    protected $blockchainFeeRepo;

    /**
     * Reference number generator.
     *
     * @var SimpleGenerator
     */
    protected $generator;

    /**
     * Ethereum repository.
     *
     * @var EthereumContract
     */
    protected $ethRepo;

    /**
     * Class constructor.
     *
     * @param BlockchainEtherRequestContract $etherRequestContract
     * @param BlockchainFeeContract $blockchainFeeContract
     * @param SimpleGenerator $generator
     * @param EthereumContract $ethereumContract
     */
    public function __construct(BlockchainEtherRequestContract $etherRequestContract, BlockchainFeeContract $blockchainFeeContract, SimpleGenerator $generator, EthereumContract $ethereumContract)
    {
        $this->etherRequestRepo  = $etherRequestContract;
        $this->blockchainFeeRepo = $blockchainFeeContract;
        $this->generator         = $generator;
        $this->ethRepo           = $ethereumContract;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract::filter()
     */
    public function filter(array $transactions)
    {
        $transactionsByHash = [];

        /*
         * Re-organize the data, such that the array key is the transaction hash instead of receiver address.
         * This will make it much easier to filter later, as we just need to execute 1 single SQL to filter everything.
         */
        foreach ($transactions as $receiverAddress => $transactionsData) {
            foreach ($transactionsData->transactions as $transaction) {
                $transactionsByHash[$transaction->hash] = $transaction;
            }
        }

        $transactionsToProcess = $this->etherRequestRepo->filterExistingHashes(array_keys($transactionsByHash));

        return array_intersect_key($transactionsByHash, array_flip(array_keys($transactionsToProcess)));
    }

    /**
     * Process filtered data.
     *
     * @param array $filteredTransactions
     * @return mixed
     * @throws \Exception
     */
    public function processFilteredData(array $filteredTransactions)
    {
        if (empty($filteredTransactions)) {
            $this->logErc20Processor('No ether requests transactions can be processed.');
            return [];
        }

        $this->logErc20Processor('Ether requests found!');

        $events = [];

        foreach ($filteredTransactions as $hash => $transaction) {
            $receipt = $this->ethRepo->getTransactionReceipt($transaction->hash);

            // Ensure transaction is valid first
            if ('0x0' === $receipt->status) {
                $this->logErc20Processor("Invalid transaction hash {$transaction->hash}, not going to process this hash, continue...");
                continue;
            }

            $gasUsed = bigHexToDec($receipt->gasUsed);
            $fee     = bcmul($gasUsed, $transaction->gasPrice, 18);

            $this->logErc20Processor("Hash {$transaction->hash}, ERC20 wallet address {$transaction->to}, ETH master wallet address {$transaction->from}");

            $this->etherRequestRepo->markHashAsMined($hash);

            $this->blockchainFeeRepo->add([
                'bank_credit_type_id'                => bank_credit_type_id(BankCreditTypeContract::ETH),
                'blockchain_fee_transaction_type_id' => blockchainFeeTransactionTypeId(BlockchainFeeTransactionTypeContract::ETHER_REQUEST),
                'transaction_hash'                   => $hash,
                'blockchain_fee'                     => $fee,
                'causer_reference_number'            => $this->generator->generate('ERQ'),
            ]);
        }

        $this->logErc20Processor('End of ether requests');

        return $events;
    }
}
