<?php

namespace Modules\Blockchain\Repositories\Parser\FilteredDataHandlers;

use Illuminate\Database\Eloquent\Model;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

/**
 * Processor for handling user deposits.
 *
 */
class BitcoinMasterWalletDepositHandler implements FilteredDataHandlerContract
{
    use LogCronOutput;

    /**
     * Bank credit type repository.
     *
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepo;

    /**
     * Credit service.
     *
     * @var BankAccountCreditContract
     */
    protected $creditService;

    /**
     * Deposit service.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Reference number generator
     *
     * @var SimpleGenerator
     */
    protected $referenceNumberGenerator;

    /**
     * Wallet service.
     *
     * @var BlockchainWalletContract
     */
    protected $walletService;

    /**
     * Class constructor.
     *
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BlockchainDepositContract $depositService \
     * @param SimpleGenerator $referenceNumberGenerator
     * @param BlockchainWalletContract $walletService
     */
    public function __construct(
        BankCreditTypeContract $bankCreditTypeContract,
        BlockchainDepositContract $depositService,
        SimpleGenerator $referenceNumberGenerator,
        BlockchainWalletContract $walletService
    ) {
        $this->bankCreditTypeRepo       = $bankCreditTypeContract;
        $this->depositService           = $depositService;
        $this->referenceNumberGenerator = $referenceNumberGenerator;
        $this->walletService            = $walletService;
    }

    /**
     * Filter out transactions that we are not interested in.
     *
     * @param array $transactions
     * @return array
     */
    public function filter(array $transactions)
    {
        $addressesToProcess = [
            setting('master_wallet_btc_address') => 0,
        ];

        if (empty($addressesToProcess)) {
            return [];
        }

        $filteredTransactionsByAddress = array_intersect_key($transactions, array_flip(array_keys($addressesToProcess)));

        foreach ($filteredTransactionsByAddress as $address => &$data) {
            $data->userId = $addressesToProcess[$address];
        }

        return $filteredTransactionsByAddress;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract::processFilteredData()
     * @throws \Exception
     */
    public function processFilteredData(array $filteredTransactions)
    {
        if (empty($filteredTransactions)) {
            $this->logBtcProcessor("No Bitcoin master wallet transactions can be processed.\n");
            return [];
        }

        $this->logBtcProcessor("\nMaster wallet deposits found! The following deposits will be credited:\n");

        $events = [];

        foreach ($filteredTransactions as $address => $data) {
            $this->logBtcProcessor("\nAddress $address (User ID {$data->userId})\n");
            $this->logBtcProcessor("------------------------------------------------------------\n");

            foreach ($data->transactions as $transaction) {
                $blockNumber = $transaction->blockNumber;
                $value       = $transaction->value;
                $hash        = $transaction->transactionHash;

                $this->logBtcProcessor("  - Block $blockNumber, Value $value, Transaction hash $hash\n");

                if ('0.00000256' == $value) {
                    $this->logBtcProcessor('Value is 0.00000256, potentially an Omnicore transfer, ignoring...');
                } else {
                    $this->insertDepositRecord(
                        $data->userId,
                        $blockNumber,
                        '',
                        $address,
                        $value,
                        $hash,
                        bank_credit_type_id(BankCreditTypeContract::BTC)
                    );
                }
            }
        }

        $this->logBtcProcessor("\nEnd of BTC deposits\n");

        return $events;
    }

    /**
     * Insert deposit records.
     *
     * @param int $userId
     * @param int $block
     * @param string $from
     * @param string $to
     * @param string $value
     * @param string $transactionHash
     * @param int $bankCreditTypeId
     * @return Model
     * @throws \Exception
     */
    protected function insertDepositRecord(int $userId, int $block, string $from, string $to, string $value, string $transactionHash, int $bankCreditTypeId)
    {
        $creditType = $this->bankCreditTypeRepo->find($bankCreditTypeId);

        return $this->depositService->add([
            'reference_number'           => $this->referenceNumberGenerator->generate('DEP'),
            'user_id'                    => $userId ?: null,
            'deposit_credit_type_id'     => $bankCreditTypeId,
            'destination_credit_type_id' => $creditType->deposit_credit_type_id,
            'block'                      => $block,
            'transaction_hash'           => $transactionHash,
            'sending_address'            => $from ?: null,
            'receiving_address'          => $to,
            'amount'                     => $value,
            'blockchain_fee'             => '0',
            'confirmations'              => '1',
            'is_master_wallet'           => '1',
        ]);
    }
}
