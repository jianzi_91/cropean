<?php

namespace Modules\Blockchain\Repositories\Parser\FilteredDataHandlers;

use Illuminate\Database\Eloquent\Model;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract;
use Modules\Blockchain\Models\BlockchainToken;
use Modules\Blockchain\Models\BlockchainWallet;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

/**
 * Processor for handling Omnicore deposits.
 *
 */
class OmnicoreDepositHandler implements FilteredDataHandlerContract
{
    use LogCronOutput;

    /**
     * Deposit service.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Wallet service.
     *
     * @var BlockchainWalletContract
     */
    protected $walletService;

    /**
     * Credit service.
     *
     * @var BankCreditTypeContract
     */
    protected $creditTypeService;

    /**
     * Reference number generator.
     *
     * @var SimpleGenerator
     */
    protected $referenceNumberGenerator;

    /**
     * Class constructor.
     *
     * @param BlockchainDepositContract $depositService
     * @param BlockchainWalletContract $walletService
     * @param BankCreditTypeContract $creditTypeService
     * @param SimpleGenerator $referenceNumberGenerator
     */
    public function __construct(
        BlockchainDepositContract $depositService,
        BlockchainWalletContract $walletService,
        BankCreditTypeContract $creditTypeService,
        SimpleGenerator $referenceNumberGenerator
    ) {
        $this->depositService           = $depositService;
        $this->walletService            = $walletService;
        $this->creditTypeService        = $creditTypeService;
        $this->referenceNumberGenerator = $referenceNumberGenerator;
    }

    /**
     * Filter out transactions that we are not interested in.
     *
     * @param array $transactions
     * @return array
     */
    public function filter(array $transactions)
    {
        // For Omnicore, no need to filter, because the transactions returned are guaranteed to be wallet transactions
        return $transactions;
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\Parser\FilteredDataHandlerContract::processFilteredData()
     * @throws \Exception
     */
    public function processFilteredData(array $filteredTransactions)
    {
        if (empty($filteredTransactions)) {
            $this->logOmniProcessor("No Omnicore transactions to process.\n");
            return [];
        }

        // Get supported Omnicore tokens
        $supportedTokens = BlockchainToken::whereNotNull('property_id')
            ->pluck('id', 'property_id')->toArray();

        $this->logOmniProcessor("\nDeposits found!\n");

        $events = [];

        foreach ($filteredTransactions as $transaction) {
            if (!array_key_exists('referenceaddress', $transaction)) {
                $this->logOmniProcessor("referenceaddress key not found for transaction {$transaction['txid']}... skipping");
                continue;
            }

            if (array_key_exists('valid', $transaction) && !$transaction['valid']) {
                $this->logOmniProcessor("transaction invalid for transaction {$transaction['txid']}... skipping");
                continue;
            }

            $this->logOmniProcessor("Deposit detected for {$transaction['referenceaddress']} for property ID {$transaction['propertyid']}");

            if (array_key_exists($transaction['propertyid'], $supportedTokens)) {
                // Ensure the receiving address exist in database, just to be safe
                $wallet = BlockchainWallet::where('address', $transaction['referenceaddress'])
                    ->first();

                if (!empty($wallet)) {
                    $deposit = $this->insertDepositRecord(
                        $wallet->user_id,
                        $transaction['block'],
                        $transaction['sendingaddress'],
                        $transaction['referenceaddress'],
                        $transaction['amount'],
                        $transaction['fee'],
                        $transaction['txid'],
                        bank_credit_type_id(BankCreditTypeContract::USDT_OMNI)
                    );

                    $this->logOmniProcessor("New deposit record with reference number {$deposit->reference_number} created");
                } elseif ($transaction['referenceaddress'] == setting('master_wallet_btc_address')) {
                    $deposit = $this->insertDepositRecord(
                        0,
                        $transaction['block'],
                        $transaction['sendingaddress'],
                        $transaction['referenceaddress'],
                        $transaction['amount'],
                        $transaction['fee'],
                        $transaction['txid'],
                        bank_credit_type_id(BankCreditTypeContract::USDT_OMNI),
                        true
                    );

                    $this->logOmniProcessor("New deposit record with reference number {$deposit->reference_number} created for master wallet");
                }
            } else {
                $this->logOmniProcessor("Property ID {$transaction['propertyid']} not supported, ignoring...");
            }
        }

        $this->logOmniProcessor("\nEnd of Omnicore deposits\n");

        return $events;
    }

    /**
     * Insert deposit records.
     *
     * @param int $userId
     * @param int $block
     * @param string $from
     * @param string $to
     * @param string $value
     * @param string $fee
     * @param string $transactionHash
     * @param int $bankCreditTypeId
     * @param bool $isMasterWallet
     * @return Model
     */
    protected function insertDepositRecord(int $userId, int $block, string $from, string $to, string $value, string $fee, string $transactionHash, int $bankCreditTypeId, bool $isMasterWallet = false)
    {
        $creditType = $this->creditTypeService->find($bankCreditTypeId);

        return $this->depositService->add([
            'reference_number'           => $this->referenceNumberGenerator->generate('DEP'),
            'user_id'                    => $userId ?: null,
            'deposit_credit_type_id'     => $bankCreditTypeId,
            'destination_credit_type_id' => $creditType->deposit_credit_type_id,
            'block'                      => $block,
            'transaction_hash'           => $transactionHash,
            'sending_address'            => $from,
            'receiving_address'          => $to,
            'amount'                     => $value,
            'blockchain_fee'             => $fee,
            'confirmations'              => '1',
            'is_master_wallet'           => $isMasterWallet,
        ]);
    }
}
