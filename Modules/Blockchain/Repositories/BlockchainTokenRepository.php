<?php

namespace Modules\Blockchain\Repositories;

use Modules\Blockchain\Contracts\BlockchainTokenContract;
use Modules\Blockchain\Models\BlockchainToken;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

/**
 * Repository class for blockchain tokens.
 *
 */
class BlockchainTokenRepository extends Repository implements BlockchainTokenContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param BlockchainToken $model
     */
    public function __construct(BlockchainToken $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainTokenContract::getContractAddress()
     */
    public function getContractAddress(int $bankCreditTypeId)
    {
        return $this->model
            ->newQuery()
            ->where('bank_credit_type_id', $bankCreditTypeId)
            ->firstOrFail()
            ->contract_address;
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainTokenContract::getPropertyId()
     */
    public function getPropertyId(int $bankCreditTypeId)
    {
        return $this->model
            ->newQuery()
            ->where('bank_credit_type_id', $bankCreditTypeId)
            ->firstOrFail()
            ->property_id;
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainTokenContract::getErc20CreditTypeIds()
     */
    public function getErc20CreditTypeIds()
    {
        return $this->model
            ->whereNotNull('contract_address')
            ->pluck('bank_credit_type_id')
            ->toArray();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainTokenContract::getErc20Tokens()
     */
    public function getErc20Tokens()
    {
        return $this->model
            ->whereNotNull('contract_address')
            ->get();
    }
}
