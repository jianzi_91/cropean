<?php

namespace Modules\Blockchain\Traits;

use kornrunner\Keccak;

/**
 * Trait which contains Ethereum address functions.
 *
 * @package Modules\Blockchain\Traits
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
trait EthereumAddressTrait
{
    /**
     * Convert address to checksum encoding.
     *
     * @param string $address
     * @return string
     * @throws \Exception
     */
    public function convertToChecksumEncoding(string $address)
    {
        if (0 === strpos($address, '0x')) {
            $address = substr($address, 2);
        }

        $address = strtolower($address);
        $hash    = Keccak::hash($address, 256);

        $addressParts = str_split($address);

        for ($i = 0; $i < count($addressParts); $i++) {
            if (in_array($hash[$i], ['8', '9', 'a', 'b', 'c', 'd', 'e', 'f'])) {
                $addressParts[$i] = strtoupper($addressParts[$i]);
            }
        }

        return '0x' . implode('', $addressParts);
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\CryptoCurrencyContract::validateAddress()
     * @throws \Exception
     */
    public function validateAddress(string $address)
    {
        $address = trim($address);

        // Address can only contain hex characters
        if (preg_match('/^(0x)[0-9a-f]{40}$/i', $address)) {
            // No need to check for checksum encoding if address is all in the same case
            if (strtolower($address) == $address || strtoupper($address) == $address) {
                return true;
            }

            return $this->convertToChecksumEncoding($address) == $address;
        }

        return false;
    }
}
