<?php

namespace Modules\Blockchain\Traits;

use stdClass;

/**
 * Trait EventMiscDataTrait
 *
 * @package Modules\Blockchain\Traits
 */
trait EventMiscDataTrait
{
    /**
     * An array where processors can insert stuffs into, so that the data can be shared with others.
     *
     * @var stdClass
     */
    protected $miscData;

    /**
     * Get misc data.
     *
     * @return stdClass
     */
    public function getMiscData()
    {
        return $this->miscData;
    }

    /**
     * Get misc data value.
     *
     * @param string $key
     * @return stdClass
     */
    public function getMiscDataValue(string $key)
    {
        return $this->hasMiscDataKey($key) ? $this->miscData->$key : null;
    }

    /**
     * Check if misc data has key.
     *
     * @param string $key
     * @return bool
     */
    public function hasMiscDataKey(string $key)
    {
        return property_exists($this->miscData, $key);
    }

    /**
     * Insert misc data.
     *
     * @param string $key
     * @param $data
     * @param bool $override
     */
    public function insertMiscData(string $key, $data, bool $override = false)
    {
        if ($this->hasMiscDataKey($key)) {
            if ($override) {
                $this->miscData->$key = $data;
            } else {
                $existingData = '';
            }
        }

        $this->miscData->$key = $data;
    }
}
