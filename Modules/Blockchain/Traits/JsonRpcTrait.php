<?php

namespace Modules\Blockchain\Traits;

use Graze\GuzzleHttp\JsonRpc\Client as RpcClient;
use Graze\GuzzleHttp\JsonRpc\Exception\RequestException;
use Illuminate\Support\Str;
use kornrunner\Keccak;
use Modules\Credit\Models\BankCreditType;

/**
 * Trait for JSON-RPC related functions.
 *
 * @package Modules\Wallets\Traits
 */
trait JsonRpcTrait
{
    /**
     * The client connection.
     *
     * @var RpcClient
     */
    protected $guzzle;

    /**
     * Utility function to send RPC request.
     *
     * @param string $method
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    protected function sendRpcRequest(string $method, $params = [], $node = null)
    {
        if (!is_array($params)) {
            $params = [$params];
        }

        // Prevent the guzzle being cache for looping of rpc request
        if (isset($this->guzzle)) {
            unset($this->guzzle);
        }

        if (empty($this->guzzle)) {
            $node = isset($node) ? $node : $this->getNode($this->getBankCreditTypeId());

            if (empty($node)) {
                // throw new \Exception('Blockchain node not found. Check the database whether the node exists and is set to active.');
            }

            $ip = long2ip($node->host);

            $protocol = $node->requires_https ? 'https' : 'http';

            if (!empty($node->username) && !empty($node->username)) {
                $password         = decrypt($node->password);
                $connectionString = "{$protocol}://{$node->username}:{$password}@{$ip}:{$node->port}";
            } else {
                $connectionString = "{$protocol}://{$ip}:{$node->port}";
            }

            $this->guzzle = RpcClient::factory($connectionString, ['rpc_error' => true]);
        }

        try {
            $response = $this->guzzle->send(
                $this->guzzle->request($this->generateRequestId(), $method, $params)
            );
        } catch (RequestException $e) {
            \Log::error("Something went wrong during RPC request: " . $method);
            \Log::error("Params: " . json_encode($params));
            \Log::error($e->getResponse()->getRpcErrorMessage());
            throw $e;
        }

        return $response->getRpcResult();
    }

    /**
     * Utility function to get node.
     *
     * @param int $bankCreditTypeId
     * @return WalletNode
     */
    protected function getNode(int $bankCreditTypeId)
    {
        $creditType = BankCreditType::findOrFail($bankCreditTypeId);

        foreach ($creditType->blockchainNodes as $node) {
            if ($node->is_active) {
                return $node; // Any active node will do if the address is being imported into the nodes
            }
        }

        return;
    }

    /**
     * Generates a random request ID to send for RPC requests.
     *
     * @return string
     * @throws \Exception
     */
    protected function generateRequestId()
    {
        return Keccak::hash(Str::random(20) . now()->toDateTimeString() . nanoSeconds(), 256);
    }

    /**
     * Returns the bank credit type ID, so that the trait knows where to get the node settings from.
     *
     * @return mixed
     */
    abstract protected function getBankCreditTypeId();
}
