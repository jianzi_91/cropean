<?php

namespace Modules\Blockchain\Traits;

use BitWasp\Bitcoin\Address\PayToPubKeyHashAddress;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Crypto\Random\Random;
use BitWasp\Bitcoin\Key\Factory\PrivateKeyFactory;

trait BitcoinAddressGenerator
{
    /**
     * Generate Bitcoin address
     * @return string[]
     * @throws \Exception
     */
    public function generateAddress()
    {
        $network    = Bitcoin::getNetwork(); // Network will be set at BlockchainServiceProvider.php
        $privateKey = resolve(PrivateKeyFactory::class)->generateCompressed(new Random());
        $address    = new PayToPubKeyHashAddress($privateKey->getPublicKey()->getPubKeyHash());

        return [
            'private_key' => $privateKey->toWif($network),
            'address'     => $address->getAddress(),
        ];
    }
}
