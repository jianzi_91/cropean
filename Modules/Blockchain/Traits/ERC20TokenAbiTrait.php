<?php

namespace Modules\Blockchain\Traits;

/**
 * A trait that contains ERC20 ABI functions.
 *
 * @package Modules\Blockchain\Traits
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
trait ERC20TokenAbiTrait
{
    /**
     * Encode ABI parameters.
     *
     * @param string $functionName
     * @param array $params
     * @param bool $hexPrefix
     * @return string
     */
    final protected function encodeAbiParameters(string $functionName, array $params, bool $hexPrefix = true)
    {
        $encodedParamString = $hexPrefix ? '0x' : '';

        switch ($functionName) {
            case 'balanceOf':
                $encodedParamString .= '70a08231';
                break;

            case 'transfer':
                $encodedParamString .= 'a9059cbb';
                break;
        }

        foreach ($params as $param) {
            switch ($param['type']) {
                case 'address':
                    $encodedParamString .= str_pad($param['value'], 64, '0', STR_PAD_LEFT);
                    break;

                case 'uint256':
                    $encodedParamString .= str_pad($param['value'], 64, '0', STR_PAD_LEFT);
                    break;
            }
        }

        return $encodedParamString;
    }

    /**
     * Decode ABI.
     *
     * @param string $abiString
     * @return null|object
     * @throws \Exception
     */
    final protected function decodeAbi(string $abiString)
    {
        // Remove the initial 0x
        if (0 == strpos($abiString, '0x')) {
            $abiString = substr($abiString, 2);
        }

        $erc20FunctionHex = substr($abiString, 0, 8);
        $parameters       = substr($abiString, 8);

        $decodedInfo = null;

        switch ($erc20FunctionHex) {
            case 'a9059cbb': // ERC20 contract function: transfer(address, uint256)
                $recipient = substr($parameters, 0, 64);
                $amount    = substr($parameters, 64);

                $decodedInfo = (object) [
                    'contractFunction' => 'transfer',
                    'to'               => '0x' . substr($recipient, -40),
                    'fractionalAmount' => bigHexToDec(ltrim($amount, '0')),
                ];

                break;

            default:
//                throw new \Exception('The contract function is currently not supported by the ABI decoder');
        }

        return $decodedInfo;
    }
}
