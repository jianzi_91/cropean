<?php

namespace Modules\Blockchain\Traits;

use Elliptic\EC;
use Illuminate\Support\Str;
use kornrunner\Keccak;

trait EthereumAddressGenerator
{
    use EthereumAddressTrait;

    /**
     * Generate ethereum address
     * @return string[]
     * @throws \Exception
     */
    public function generateAddress()
    {
        $privateKey = $this->generatePrivateKey();
        $publicKey  = $this->getPublicKeyFromPrivateKey($privateKey);

        return [
            'private_key' => $privateKey,
            'public_key'  => $publicKey,
            'address'     => $this->getAddressFromPublicKey($publicKey)
        ];
    }

    /**
     * Generate private key
     * @param int $length
     * @return string
     * @throws \Exception
     */
    protected function generatePrivateKey(int $length = 1024)
    {
        $passphrase = Str::random($length) . microtime(true);
        return Keccak::hash($passphrase, 256);
    }

    /**
     * Get public key from private key.
     *
     * @param string $privateKey
     * @return NULL
     */
    protected function getPublicKeyFromPrivateKey(string $privateKey)
    {
        $ec = new EC('secp256k1');
        return $ec->keyFromPrivate($privateKey)->getPublic(false, 'hex');
    }

    /**
     * Get address from public key.
     *
     * @param string $publicKey
     * @return string
     * @throws \Exception
     */
    protected function getAddressFromPublicKey(string $publicKey)
    {
        $address = substr(Keccak::hash(substr(hex2bin($publicKey), 1), 256), 24);
        return $this->convertToChecksumEncoding($address);
    }
}
