<?php

namespace Modules\Blockchain\Traits;

use Graze\GuzzleHttp\JsonRpc\Exception\RequestException;
use kornrunner\Ethereum\Transaction;
use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;

/**
 * Trait which contains Ethereum functions, which can potentially be reused in other places, such as ERC20 tokens.
 *
 * @package Trading\Blockchain\Traits
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
trait EthereumTrait
{
    use EthereumAddressGenerator, JsonRpcTrait;

    /**
     * Nonce generator service.
     *
     * @var NonceGeneratorContract
     */
    protected $nonceGenerator;

    /**
     * @var
     */
    protected $specificNonce;

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::createNewAddress()
     * @throws \Exception
     */
    public function createNewAddress()
    {
        return $this->generateAddress();
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getLatestBlockNumber()
     * @throws \Exception
     */
    public function getLatestBlockNumber()
    {
        return (int) bigHexToDec($this->sendRpcRequest('eth_blockNumber'));
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getBalance()
     * @throws \Exception
     */
    public function getBalance(string $address)
    {
        $weiAmount = bigHexToDec($this->sendRpcRequest('eth_getBalance', [$address, 'latest']));
        return $this->convertToMainUnit($weiAmount);
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\CryptoCurrencyContract::getBlockTransactions()
     * @throws \Exception
     */
    public function getBlockTransactions(int $blockNo)
    {
        $blockInfo = $this->sendRpcRequest('eth_getBlockByNumber', ['0x' . bigDecToHex($blockNo), true]);
        return $blockInfo['transactions'];
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\CryptoCurrencyContract::getTransactionDetails()
     * @throws \Exception
     */
    public function getTransactionDetails(string $transactionHash)
    {
        try {
            return (object) $this->sendRpcRequest('eth_getTransactionByHash', [$transactionHash]);
        } catch (RequestException $e) {
            if (strpos($e->getResponse()->getRpcErrorMessage(), 'Block information is incomplete while ancient block sync is still in progress') !== false) {
                return;
            }
        }
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract::getTransactionReceipt()
     * @throws \Exception
     */
    public function getTransactionReceipt(string $transactionHash)
    {
        try {
            return (object) $this->sendRpcRequest('eth_getTransactionReceipt', [$transactionHash]);
        } catch (RequestException $e) {
            if (strpos($e->getResponse()->getRpcErrorMessage(), 'Block information is incomplete while ancient block sync is still in progress') !== false) {
                return;
            }
        }
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\EthereumContract::signRawTransactionForEtherTransfer()
     * @throws \Exception
     */
    public function signRawTransactionForEtherTransfer(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $fromAddress,
        string $toAddress,
        string $fractionalAmount,
        bool $feeInclusive = false
    ) {
        return $this->signRawTransaction($nonceGenerator, $senderPrivateKey, $fromAddress, $toAddress, $fractionalAmount, '21000', '', $feeInclusive);
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\EthereumContract::signRawTransactionForContractCall()
     * @throws \Exception
     */
    public function signRawTransactionForContractCall(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $contractAddress,
        string $fromAddress,
        string $data
    ) {
        return $this->signRawTransaction($nonceGenerator, $senderPrivateKey, $fromAddress, $contractAddress, '0', '65000', $data);
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\EthereumContract::getGasPrice()
     * @throws \Exception
     */
    public function getGasPrice(string $format = 'hex')
    {
        $gasGweiSetting = setting('blockchain_eth_gas_price_gwei');

        if (0 < $gasGweiSetting) {
            $price = bcmul($gasGweiSetting, '1000000000', 0);
            return 'hex' == $format ? bigDecToHex($price) : $price;
        }

        $priceHex = $this->sendRpcRequest('eth_gasPrice');
        return 'hex' == $format ? $priceHex : bigHexToDec($priceHex);
    }

    /**
     * @inheritdoc
     * @see \Trading\Blockchain\Contracts\EthereumContract::signRawTransaction()
     * @throws \Exception
     */
    public function signRawTransaction(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $fromAddress,
        string $toAddress,
        string $fractionalAmount,
        string $gasLimit,
        string $data = '',
        bool $feeInclusive = false
    ) {
        /*
         * Nonce simply needs to be a sequence of ordered numbers (without gaps in between). So the standard practice is
         * simply to query the blockchain with regards to how many transactions the from address has initiated. That number
         * will become the nonce for the current transaction. This needs to be in hexadecimal.
         */
        $nonce = empty($this->specificNonce)
            ? stripHexPrefix($nonceGenerator->nextNonce($fromAddress))
            : bigDecToHex($this->specificNonce);

        // It is generally safe to simply query the node for the current gas price and use it, instead of deciding on a value ourselves, must be hexadecimal
        $gasPrice        = stripHexPrefix($this->getGasPrice());
        $gasPriceDecimal = bigHexToDec($gasPrice);

        // For simple ether transfers, it will consume EXACTLY 21000 gas, so it's ok to use this number as the limit, must be hexadecimal
        $gasLimitHex = bigDecToHex($gasLimit);

        // Calculate the fee, in case needed
        $totalFee = bcmul($gasPriceDecimal, $gasLimit, 18);

        // This is simply the transfer amount in wei, but it has to be encoded in hexadecimal, and if this is negative, it means transfer entire balance
        if (1 == bccomp('0', $fractionalAmount)) {
            $currentFractionalBalance = $this->convertToFractionalUnit($this->getBalance($fromAddress));
            $transferAmount           = bcsub($currentFractionalBalance, $totalFee, 18);
            $value                    = bigDecToHex($transferAmount);
        } elseif ($feeInclusive) {
            $transferAmount = bcsub($fractionalAmount, $totalFee, 18);
            $value          = bigDecToHex($transferAmount);
        } else {
            $value = bigDecToHex($fractionalAmount);
        }

        $transaction = new Transaction($nonce, $gasPrice, $gasLimitHex, $toAddress, $value, $data);
        return '0x' . $transaction->getRaw($senderPrivateKey);
    }

    /**
     * Get number of confirmations.
     *
     * @param string $transactionHash
     * @return int
     * @throws \Exception
     */
    public function getConfirmations(string $transactionHash)
    {
        $receipt = $this->getTransactionReceipt($transactionHash);

        if (empty($receipt) || empty($receipt->blockNumber)) {
            return 0;
        }

        $latestBlock      = $this->getLatestBlockNumber();
        $transactionBlock = bigHexToDec($receipt->blockNumber);

        return $latestBlock - $transactionBlock + 1;
    }
}
