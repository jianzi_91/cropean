<?php

namespace Modules\Blockchain\Traits;

use Modules\Blockchain\Contracts\Parser\ProcessedBlockContract;

/**
 * A trait that contains useful functions for logging of block data to MySQL.
 *
 * @package Modules\BlockchainParser\Traits
 */
trait LogBlock
{
    /**
     * Log a block into MongoDB.
     *
     * @param string $blockProcessorType
     * @param int $blockNumber
     */
    public function logBlock(string $blockProcessorType, int $blockNumber)
    {
        $processedBlockService = resolve(ProcessedBlockContract::class);

        $processedBlockService->add([
            'block_processor_type' => $blockProcessorType,
            'block_number'         => $blockNumber,
        ]);
    }
}
