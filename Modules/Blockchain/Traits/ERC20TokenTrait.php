<?php

namespace Modules\Blockchain\Traits;

use Graze\GuzzleHttp\JsonRpc\Exception\ServerException;
use Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract;
use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;
use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * A trait that contains ERC20 token functions.
 *
 * @package Trading\Blockchain\Traits
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
trait ERC20TokenTrait
{
    use ERC20TokenAbiTrait, EthereumTrait;

    /**
     * Nonce generator. This should be set by any class that uses this trait.
     *
     * @var NonceGeneratorContract
     */
    protected $nonceGenerator;

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::getBalance()
     * @throws \Exception
     */
    public function getBalance(string $address)
    {
        $encodedParamString = $this->encodeAbiParameters('balanceOf', [
            ['type' => 'address', 'value' => substr($address, 2)] // remove the 0x in front of the address
        ]);

        $balanceHex = $this->sendRpcRequest(
            'eth_call',
            [
                [
                    'from' => $address,
                    'to'   => $this->getContractAddress(),
                    'data' => $encodedParamString
                ],
                'latest'
            ]
        );

        return $this->convertToMainUnit(bigHexToDec($balanceHex));
    }

    /**
     * @inheritdoc
     * @see \Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract::transfer()
     * @throws InsufficientBlockchainFundsException
     * @throws \Exception
     */
    public function transfer(
        string $fromAddress,
        string $toAddress,
        string $amount,
        array $options = []
    ) {
        if (array_key_exists('nonce', $options) && !empty($options['nonce'])) {
            $this->specificNonce = $options['nonce'];
        } else {
            $this->specificNonce = 0;
        }

        // Ensure that the from address has enough money first
        $currentBalance = $this->getBalance($fromAddress);

        if ($amount == CryptoCurrencyContract::ENTIRE_BALANCE) {
            $amount = $currentBalance;
        }

        if (-1 == bccomp($currentBalance, $amount, credit_precision(BankCreditTypeContract::ETH))) {
            throw new InsufficientBlockchainFundsException();
        }

        $encodedAbi = $this->encodeAbiParameters('transfer', [
            ['type' => 'address', 'value' => substr($toAddress, 2)],
            ['type' => 'uint256', 'value' => bigDecToHex($this->convertToFractionalUnit($amount))]
        ]);

        if (array_key_exists('private_key', $options)) {
            $signedTransactionData = $this->signRawTransactionForContractCall(
                $this->nonceGenerator,
                $options['private_key'],
                $this->getContractAddress(),
                $fromAddress,
                $encodedAbi
            );

            // This catches insufficient ETH, not ERC20, because you need ETH to invoke contract functions
            try {
                return $this->sendRpcRequest('eth_sendRawTransaction', [$signedTransactionData]);
            } catch (ServerException $e) {
                if (false !== stripos($e->getMessage(), 'insufficient funds')) {
                    throw new InsufficientBlockchainFundsException();
                }
                throw $e;
            }
        }

        // Same as above, except that this uses password
        try {
            return $this->sendRpcRequest(
                'personal_sendTransaction',
                [
                    [
                        'from' => $fromAddress,
                        'to'   => $this->getContractAddress(),
                        'data' => $encodedAbi,
                        'gas'  => '0x' . bigHexToDec('50000'),
                    ],
                    $options['password']
                ]
            );
        } catch (ServerException $e) {
            if (false !== stripos($e->getMessage(), 'insufficient funds')) {
                throw new InsufficientBlockchainFundsException();
            }
        }
    }

    /**
     * Gets ERC20 token contract address.
     *
     * @return mixed
     */
    abstract public function getContractAddress();

    /**
     * Convert from fractional to main unit.
     *
     * @param string $fractionalAmount
     * @return mixed
     */
    abstract public function convertToMainUnit(string $fractionalAmount);

    /**
     * Convert from main to fractional unit.
     *
     * @param string $mainUnitAmount
     * @return mixed
     */
    abstract public function convertToFractionalUnit(string $mainUnitAmount);
}
