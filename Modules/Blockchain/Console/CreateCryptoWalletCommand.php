<?php

namespace Modules\Blockchain\Console;

use DB;
use Exception;
use Illuminate\Console\Command;
use Modules\Blockchain\Jobs\CreateWallet;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateCryptoWalletCommand extends Command
{
    protected $signature = 'blockchain:create-wallet {userId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create blockchain wallet by commands.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();

        $this->userRepo = $userRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('userId');

        try {
            DB::beginTransaction();
            if ($userId) {
                $user = $this->userRepo->find($userId);

                CreateWallet::dispatchNow($user);

                foreach ($user->blockchainWallets as $wallet) {
                    $this->line('Blockchain Wallet for ' . $wallet->creditType->name . ' is created. Address: ' . $wallet->address);
                }
            } else {
                $users = $this->userRepo->all();

                foreach ($users as $user) {
                    if (count($user->blockchainWallets) == 0) {
                        CreateWallet::dispatchNow($user);

                        foreach ($user->blockchainWallets as $wallet) {
                            $this->line('Blockchain Wallet for ' . $wallet->creditType->name . ' is created. Address: ' . $wallet->address);
                        }
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $this->error($e->getMessage());
        }

        return $this->line('Blockchain wallets creation finished without errors.');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['userId', InputArgument::OPTIONAL, 'userId'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
