<?php

namespace Modules\Blockchain\Console;

use Illuminate\Console\Command;
use Modules\Blockchain\Jobs\MasterWalletRedirectionJob;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;

class MasterWalletRedirectionCommand extends Command
{
    use LogCronOutput;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'blockchain-parser:master-wallet-redirection';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Redirect master wallet for those cant catch up for the first time';

    /**
     * Deposit Repository.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Class constructor
     * .
     * @param BlockchainDepositContract $depositContract
     */
    public function __construct(BlockchainDepositContract $depositContract)
    {
        parent::__construct();
        $this->depositService = $depositContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unredirectedDeposits = $this->depositService->getUnredirectedDeposits();

        foreach ($unredirectedDeposits as $unredirectedDeposit) {
            $this->logMasterWalletRedirection('Master wallet redirection Command: ' . $unredirectedDeposit->user_id . 'Deposit Credit Type ID: ' . $unredirectedDeposit->deposit_credit_type_id);
            if (empty($unredirectedDeposit->is_master_wallet)) {
                dispatch(new MasterWalletRedirectionJob($unredirectedDeposit->user_id, $unredirectedDeposit->deposit_credit_type_id));
            }
        }
    }
}
