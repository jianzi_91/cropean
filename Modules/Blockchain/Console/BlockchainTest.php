<?php

namespace Modules\Blockchain\Console;

use Illuminate\Console\Command;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\Credit\Contracts\BankAccountCreditContract;

class BlockchainTest extends Command
{
    protected $credit;
    protected $eth;
    protected $btc;
    protected $usdt;
    protected $usdtErc20;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockchain:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Blockchain Test';

    public function __construct(
        BankAccountCreditContract $credit,
        EthereumRepository $eth,
        BitcoinContract $btc,
        UsdtRepository $usdt,
        UsdtErc20Repository $usdtErc20
    ) {
        parent::__construct();

        $this->credit    = $credit;
        $this->eth       = $eth;
        $this->btc       = $btc;
        $this->usdt      = $usdt;
        $this->usdtErc20 = $usdtErc20;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->line($this->btc->getLatestBlockNumber());
        // $this->line($this->usdt->getLatestBlockNumber());
        // $this->line($this->eth->getLatestBlockNumber());
        // $this->line($this->usdtErc20->getLatestBlockNumber());

        // $this->line($this->eth->getBalance('0x5E3A6d91AF7D92Ac7C99Bee7Fa4190a4E221A620'));
        // $this->line($this->usdtErc20->getBalance('0x5E3A6d91AF7D92Ac7C99Bee7Fa4190a4E221A620'));

        try {
            $hash = $this->usdtErc20->transfer(
                '0x684902b4851afedafd6c3459589a781ca9a85842',
                '0x102cfA277c9726323179a2BD6a6f28c8f059494B',
                3400,
                ['private_key' => decrypt('eyJpdiI6ImdMZ3FuQ1laQWx4ZERJSmxNR1RmTVE9PSIsInZhbHVlIjoiUUJTVmFXZEI0ekZweVFGam5kTldmeU16ZWJHRlYrNGRDZHk2WjYwNlFtbVhHUUhXRUVMM0tUeEdCa0dudkxvV3dUT29QYjRsSXF6T0xNek9yVVlOU0d5NWhJRGlSam00bDBuR0VSdlwvbVRnPSIsIm1hYyI6IjAxZDk0MTlhMzAxOTI0YjFmZjZjZjk4NmNhNWJmN2FhNGFhM2E4YzAzMjQxMzgzOTRlNTExOTQ1YjU2MWJlNTEifQ==')]
            );
        } catch (InsufficientBlockchainFundsException $e) {
            dd($e);
        }

        // try {
        //     $hash = $this->usdtErc20->transfer(
        //         '0xdea920c329e33e953211bdb51fe3453e04d1210e',
        //         '0x102cfA277c9726323179a2BD6a6f28c8f059494B',
        //         5000,
        //         ['private_key' => decrypt('eyJpdiI6IlFFSUxlUFNoeThlcHJaVzV0XC80ZzhnPT0iLCJ2YWx1ZSI6IlhmWTNDQmZRcFZxOWpYUUkrUVRNTHIwT1hFSEk5bGR2UjJoUUlPOEk2QVJaSVljR2RYSTFwTFNOYndJc2l0dHNOWGh4TVRCcDBNNW1xVEE0eVZtMTloRFVSR05YRmpvMWlQWDF2alwvVmozdz0iLCJtYWMiOiJmYTExODQwZjEzZGUwYzhmODkwOTUxOTBjOWVjNzJiMmY0M2Q5NGE2NjJlNDUyNThmNzJhYTgwNGZmNWVhNWU1In0=')]
        //     );
        // } catch (InsufficientBlockchainFundsException $e) {
        //     dd($e);
        // }

        $this->line($hash);
    }
}
