<?php

namespace Modules\Blockchain\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainFeeContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;

class UpdateFees extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockchain:update-fees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update blockchain fees';

    /**
     * Ethereum repository.
     *
     * @var EthereumRepository
     */
    protected $eth;

    /**
     * Blockchain fee repository.
     *
     * @var BlockchainFeeContract
     */
    protected $feeRepo;

    /**
     * Blockchain deposit repository.
     *
     * @var BlockchainDepositContract
     */
    protected $blockchainDepositRepository;

    /**
     * Class constructor.
     *
     * @param EthereumRepository $eth
     * @param BlockchainFeeContract $feeContract
     */
    public function __construct(
        EthereumRepository $eth,
        BlockchainFeeContract $feeRepo,
        BlockchainDepositContract $blockchainDepositContract
    ) {
        parent::__construct();

        $this->eth                         = $eth;
        $this->feeRepo                     = $feeRepo;
        $this->blockchainDepositRepository = $blockchainDepositContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        DB::beginTransaction();

        try {
            //Update blockchain_fees
            $fees = $this->feeRepo->getEthFees();
            $this->updateBlockchainFee($fees);

            //Update USDT ERC20 deposit fee
            $usdtErc20Fees = $this->blockchainDepositRepository->getUsdtErc20Fees();
            if ($usdtErc20Fees) {
                $this->updateBlockchainFee($usdtErc20Fees);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * @param $fees
     * @throws Exception
     */
    private function updateBlockchainFee($fees)
    {
        foreach ($fees as $fee) {
            $details = $this->eth->getTransactionDetails($fee->transaction_hash);
            $receipt = $this->eth->getTransactionReceipt($fee->transaction_hash);

            if (!empty($receipt) && !empty($receipt->gasUsed)) {
                $gasPrice = bigHexToDec($details->gasPrice);
                $gasUsed  = bigHexToDec($receipt->gasUsed);

                $blockchainFee = bcmul($gasPrice, $gasUsed, 0);
                $blockchainFee = bcdiv($blockchainFee, '1000000000000000000', 18);

                $fee->update(['blockchain_fee' => $blockchainFee]);
            }
        }
    }
}
