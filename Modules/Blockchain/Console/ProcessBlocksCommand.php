<?php

namespace Modules\Blockchain\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Traits\LogBlock;
use Modules\Core\Traits\LogCronOutput;
use Modules\Setting\Contracts\SettingCacheContract;
use Modules\Setting\Contracts\SettingContract;
use Modules\Setting\Models\SettingValue;

/**
 * Handle logic such as deposits by parsing blockchain blocks based on confimrations required.
 *
 * @package Trading\Blockchain\Console
 */
class ProcessBlocksCommand extends Command
{
    use LogBlock, LogCronOutput;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'blockchain-parser:process-blocks {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle logic such as deposits by parsing blockchain blocks based on confirmations required.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type                   = $this->argument('type');
        $transactionProcessors  = config('blockchain.transaction_processors');
        $settingRepository      = resolve(SettingContract::class);
        $settingCacheRepository = resolve(SettingCacheContract::class);

        $processFlag = setting('blockchain_process_' . $type . '_flag');
        if ($processFlag === '1') {
            $this->logBlockProcessor("Other process block is processing.");
            exit;
        }

        try {
            if (isset($transactionProcessors[$type])) {
                $processorClassName = $transactionProcessors[$type];

                $processor = resolve($processorClassName);

                $this->logBlockProcessor('');
                $this->logBlockProcessor('------------------------------');
                $this->logBlockProcessor("Processing credit type {$processor->getCryptoCurrencyService()->getTicker()}");
                $this->logBlockProcessor('------------------------------');

                /*
                 * Get the last block number we processeed. If the number is zero, this means that the cron job is running
                 * for the very first time, due to the project just being deployed. In this case, we don't process anything.
                 * Just insert a bogus record in the collection, so that we know what blocks to start processing on the next
                 * time the cron runs.
                 */
                $lastProcessedBlockNumber      = $processor->getLatestProcessedBlockNumber();
                $latestBlockNumberInBlockchain = $processor->getCryptoCurrencyService()->getLatestBlockNumber();
                $minConfirmationsRequired      = $processor->getMinimumConfirmationsRequired();

                $this->logBlockProcessor("Last Processed Block Number: $lastProcessedBlockNumber");
                $this->logBlockProcessor("Latest BlockNumber In Blockchain: $latestBlockNumberInBlockchain");

                // Must +1 at the end, because when a block is formed, all transactions in that block gets 1 confirmation
                $largestBlockNumberToProcess = $latestBlockNumberInBlockchain - $minConfirmationsRequired + 1;

                // Cron is executing for the first time for this credit type. Just insert a bogus record.
                if (0 == $lastProcessedBlockNumber) {
                    if (0 > $largestBlockNumberToProcess) {
                        $largestBlockNumberToProcess = 0;
                    }

                    $this->logBlockProcessor('Cannot find last processed block number. This is likely the first time the cron runs for this credit type.');
                    $this->logBlockProcessor("Inserting dummy record in MySQL to flag block number $largestBlockNumberToProcess as processed.");
                    $this->logBlock($processor->getCryptoCurrencyService()->getTicker(), $largestBlockNumberToProcess);
                    $this->updateProcessFlag($settingRepository, $settingCacheRepository, $type, 0);
                    exit;
                }

                if (0 > $largestBlockNumberToProcess) {
                    $this->logBlockProcessor('The largest block number that the cron can process in this exeuction is a negative number.');
                    $this->logBlockProcessor("This means that the blockchain currently has less than $minConfirmationsRequired blocks. Skipping.");
                    $this->updateProcessFlag($settingRepository, $settingCacheRepository, $type, 0);
                    exit;
                }

                if ($lastProcessedBlockNumber == $largestBlockNumberToProcess) {
                    $this->logBlockProcessor("The last processed block number is $lastProcessedBlockNumber.");
                    $this->logBlockProcessor("The largest block number that the cron can process in this exeuction is $largestBlockNumberToProcess.");
                    $this->logBlockProcessor('Execution is skipped since the 2 numbers are the same, meaning there are no new blocks in the blockchain');
                    $this->updateProcessFlag($settingRepository, $settingCacheRepository, $type, 0);
                    exit;
                }

                // If execution reaches here, this means we now have to figure out what are the blocks we need to process in this cron execution.
                $blocksToProcess = range($lastProcessedBlockNumber + 1, $largestBlockNumberToProcess);

                foreach ($blocksToProcess as $blockNumber) {
                    try {
                        $this->logBlockProcessor('');
                        $this->logBlockProcessor("Processing block number $blockNumber");
                        $this->logBlockProcessor('--------------------------------------');

                        DB::beginTransaction();
                        $processor->processBlock($blockNumber);
                        DB::commit();
                    } catch (\Exception $e) {
                        \Log::info($e->getMessage());
                        $this->logBlockProcessor('An error has occured. Reverting all transactions that are processed in this block');
                        $this->logBlockProcessor($e->getMessage());
                        $this->logBlockProcessor($e->getTraceAsString());

                        DB::rollback();

                        $this->logBlockProcessor("Changes reverted. Not going to process other blocks.");
                        break;
                    }
                }

                $this->logBlockProcessor("End of processing for credit type {$processor->getCryptoCurrencyService()->getTicker()}");
                $this->logBlockProcessor('-----------------------------------------------------------------------------------------');

                $this->updateProcessFlag($settingRepository, $settingCacheRepository, $type, 0);
            }
        } catch (\Exception $e) {
            $this->logBlockProcessor('An error has occured. Reverting all transactions that are processed in this block', $type);
            $this->logBlockProcessor($e->getMessage(), $type);
            $this->logBlockProcessor($e->getTraceAsString(), $type);

            $this->logBlockProcessor("Changes reverted. Not going to process other blocks.", $type);
            $this->updateProcessFlag($settingRepository, $settingCacheRepository, $type, 0);
            throw $e;
        }
    }

    private function updateProcessFlag($settingRepository, $settingCacheRepository, $type, $value)
    {
        // Update process flag to 0 after finish process
        $settingValue   = $settingRepository->findBySlug('blockchain_process_' . $type . '_flag');
        $setting        = SettingValue::where('setting_id', $settingValue->id)->first();
        $setting->value = $value;
        $setting->save();

        $settingCacheRepository->clear('blockchain_process_' . $type . '_flag');
    }
}
