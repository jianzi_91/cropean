<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for processed_blocks table in blockchain database.
 *
 * @package Modules\Blockchain\Models
 */
class ProcessedBlock extends Model
{
    protected $connection = 'blockchain';

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'blockchain_processed_blocks';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'block_processor_type',
        'block_number',
    ];
}
