<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Models\BankCreditType;

/**
 * Model for erc20_tokens table in database.
 *
 * @package Modules\Blockchain\Models
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
class BlockchainToken extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'blockchain_tokens';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'bank_credit_type_id',
        'ticker',
        'implementation_class',
        'contract_address',
        'property_id',
    ];

    /**
     * Relationship to BankCreditType model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankCreditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }
}
