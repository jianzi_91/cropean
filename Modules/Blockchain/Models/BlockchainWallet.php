<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Models\BankCreditType;
use Modules\User\Models\User;
use Plus65\Base\Models\Relations\UserRelation;

class BlockchainWallet extends Model
{
    use SoftDeletes, UserRelation;

    protected $fillable = [
        'address',
        'user_id',
        'bank_credit_type_id',
        'private_key',
        'qr_code',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'private_key'
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->append(['balance']);
        parent::__construct($attributes);
    }

    /**
     * This model's relation to User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Asset relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * Get balance
     * @return unknown
     */
    public function getBalanceAttribute()
    {
        $credit  = resolve(BankAccountCreditContract::class);
        $balance = $credit->getBalanceByReference($this->user_id, $this->bank_credit_type_id);

        return $balance;
    }

    /**
     * Get deposit min.
     *
     * @return bool|\unknown
     */
    public function getDepositMinAttribute()
    {
        $slug = $this->creditType->rawname;
        return setting("deposit_{$slug}_min");
    }
}
