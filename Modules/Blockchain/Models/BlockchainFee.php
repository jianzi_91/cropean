<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Models\BankCreditType;

class BlockchainFee extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'blockchain_fees';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'bank_credit_type_id',
        'blockchain_fee_transaction_type_id',
        'transaction_hash',
        'blockchain_fee',
        'causer_reference_number',
    ];

    /**
     * Relation to transaction type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactionType()
    {
        return $this->belongsTo(BlockchainFeeTransactionType::class, 'blockchain_fee_transaction_type_id');
    }

    /**
     * Relation to credit type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }
}
