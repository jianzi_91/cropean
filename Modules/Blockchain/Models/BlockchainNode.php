<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Models\BankCreditType;

class BlockchainNode extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'blockchain_nodes';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'host',
        'port',
        'requires_https',
        'username',
        'password',
        'is_active'
    ];

    /**
     * Scope to only query for nodes which are active.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function bankCreditTypes()
    {
        return $this->belongsToMany(BankCreditType::class, 'bank_credit_type_blockchain_nodes');
    }
}
