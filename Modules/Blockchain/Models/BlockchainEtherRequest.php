<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;

class BlockchainEtherRequest extends Model
{
    protected $guarded = [];
    protected $table   = 'blockchain_ether_requests'; 
}
