<?php

namespace Modules\Blockchain\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Plus65\Base\Repositories\Concerns\HasSlug;

class BlockchainFeeTransactionType extends Model
{
    use Translatable, HasSlug, HasActiveScope;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'blockchain_fee_transaction_types';

    /**
     * Translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'is_active',
    ];

    /**
     * Scope to only query for nodes which are active.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }
}
