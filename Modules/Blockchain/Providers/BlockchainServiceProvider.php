<?php

namespace Modules\Blockchain\Providers;

use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Network\NetworkFactory;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Blockchain\Console\BlockchainTest;
use Modules\Blockchain\Console\CreateCryptoWalletCommand;
use Modules\Blockchain\Console\MasterWalletRedirectionCommand;
use Modules\Blockchain\Console\ProcessBlocksCommand;
use Modules\Blockchain\Console\UpdateFees;
use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;

class BlockchainServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /*
     * Command
     */
    protected $commands = [
        ProcessBlocksCommand::class,
        MasterWalletRedirectionCommand::class,
        BlockchainTest::class,
        CreateCryptoWalletCommand::class,
        UpdateFees::class
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->registerBindings();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->app->register(RouteServiceProvider::class);
        $this->setupBitcoinNetwork();
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/blockchain');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/blockchain';
        }, \Config::get('view.paths')), [$sourcePath]), 'blockchain');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/blockchain');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'blockchain');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'blockchain');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * bind interfaces to implementations
     * @return void
     */
    public function registerBindings()
    {
        foreach (config('blockchain.bindings') as $key => $binding) {
            $this->app->bind($key, $binding);
        }

        foreach (config('blockchain.nonce_generators') as $cryptoCurrencyClass => $nonceGenerator) {
            $this->app->when($cryptoCurrencyClass)
                ->needs(NonceGeneratorContract::class)
                ->give(function () use ($nonceGenerator) {
                    return new $nonceGenerator['implementation_class']($nonceGenerator['bank_credit_type']);
                });
        }

        foreach (config('blockchain.data_collectors') as $blockProcessorClassName => $collectorClassNames) {
            $this->app->when($blockProcessorClassName)
                ->needs('$dataCollectorClassNames')
                ->give($collectorClassNames);

            foreach ($collectorClassNames as $collectorClassName) {
                $this->app->when($collectorClassName)
                    ->needs('$dataHandlerClassNames')
                    ->give(config('blockchain.data_handlers')[$collectorClassName]);
            }
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('blockchain.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php',
            'blockchain'
        );
    }

    /**
     * Setup Bitcoin network.
     *
     * @throws \Exception
     */
    protected function setupBitcoinNetwork()
    {
        $network = app()->environment(['production'])
            ? NetworkFactory::bitcoin()
            : NetworkFactory::bitcoinRegtest();

        Bitcoin::setNetwork($network);
    }

    protected function registerCommands()
    {
        $this->commands($this->commands);
    }
}
