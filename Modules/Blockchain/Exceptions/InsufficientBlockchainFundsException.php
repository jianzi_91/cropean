<?php

namespace Modules\Blockchain\Exceptions;

/**
 * This exception is thrown when a transfer request is made in blockchain, but there is insufficient funds.
 *
 * @package Modules\Blockchain\Exceptions
 */
class InsufficientBlockchainFundsException extends \Exception
{
}
