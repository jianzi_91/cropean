<?php

namespace Modules\Blockchain\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainFeeContract;
use Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors\EthereumRedirector;
use Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors\UsdcRedirector;
use Modules\Blockchain\Repositories\Parser\MasterWalletRedirectors\UsdtErc20Redirector;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

class MasterWalletRedirectionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, LogCronOutput;

    /**
     * User ID.
     *
     * @var int
     */
    protected $userId;

    /**
     * Deposit credit type ID.
     *
     * @var int
     */
    protected $depositCreditTypeId;

    /**
     * Class constructor.
     *
     * @param int $userId
     * @param int $depositCreditTypeId
     */
    public function __construct(int $userId, int $depositCreditTypeId)
    {
        $this->userId              = $userId;
        $this->depositCreditTypeId = $depositCreditTypeId;
    }

    /**
     * Send blockchain request.
     *
     * @param BlockchainDepositContract $depositService
     * @param BlockchainWalletContract $walletService
     * @param BlockchainFeeContract $blockchainFeeRepo
     * @param SimpleGenerator $generator
     * @throws \Exception
     */
    public function handle(BlockchainDepositContract $depositService, BlockchainWalletContract $walletService, BlockchainFeeContract $blockchainFeeRepo, SimpleGenerator $generator)
    {
        $this->logMasterWalletRedirection("Wallet redirection Job: master wallet redirection for user id: " . $this->userId . ' and deposit credit type id: ' . $this->depositCreditTypeId);

        DB::beginTransaction();

        $unredirectedDeposits = $depositService->getUnredirectedDepositByUserId($this->userId, $this->depositCreditTypeId);
        $total                = '0';

        $this->logMasterWalletRedirection("Wallet redirection Job: unredirectedDeposits - " . json_encode($unredirectedDeposits));

        foreach ($unredirectedDeposits as $unredirectedDeposit) {
            if (empty($unredirectedDeposit->redirection_transaction_hash)) {
                $total = bcadd($total, $unredirectedDeposit->amount, 8);
            }
        }

        $this->logMasterWalletRedirection("Wallet redirection Job: total amount for redirection is $total");
        if ($total > 0) {
            try {
                $redirector = $this->getRedirector($this->depositCreditTypeId);

                if (empty($redirector)) {
                    DB::rollBack();
                    return;
                }

                $redirectionThreshold = $redirector->getMinimumRedirectionAmount();

                if ($total < $redirectionThreshold) {
                    DB::rollBack();
                    $this->logMasterWalletRedirection('Not going to redirect since the amount is less than ' . $redirectionThreshold . '. ' . '. Current total: ' . $total);

                    return;
                }

                $this->logMasterWalletRedirection('Wallet redirection Job: going to redirect');

                $hash = $redirector->redirect($this->userId, $total);

                $this->logMasterWalletRedirection("Wallet redirection Job: done, hash is $hash");

                if (!empty($hash)) {
                    foreach ($unredirectedDeposits as $unredirectedDeposit) {
                        $this->logMasterWalletRedirection("Wallet redirection Job: updating redirection hash for deposit id - $unredirectedDeposit->id ....");

                        if (empty($unredirectedDeposit->redirection_transaction_hash)) {
                            $this->logMasterWalletRedirection("Wallet redirection Job: updating redirection hash.....");

                            $unredirectedDeposit->update([
                                'redirection_transaction_hash' => $hash,
                            ]);

                            $this->logMasterWalletRedirection("Wallet redirection Job: done updating redirection hash $hash");
                        }
                    }

                    $blockchainFeeCreditTypeId = getBlockchainFeeCreditTypeId($this->depositCreditTypeId);

                    if (!empty($blockchainFeeCreditTypeId)) {
                        $this->logMasterWalletRedirection("Wallet redirection Job: updating fee for credit ID $blockchainFeeCreditTypeId");
                        $blockchainFeeRepo->add([
                            'bank_credit_type_id'                => $blockchainFeeCreditTypeId,
                            'blockchain_fee_transaction_type_id' => blockchainFeeTransactionTypeId(BlockchainFeeTransactionTypeContract::MASTER_WALLET_REDIRECTION),
                            'transaction_hash'                   => $hash,
                            'blockchain_fee'                     => $redirector->getFee(),
                            'causer_reference_number'            => $generator->generate('MWR'),
                        ]);
                        $this->logMasterWalletRedirection("Wallet redirection Job: done updating fee");
                    }
                }
            } catch (\Exception $e) {
                DB::rollBack();

                $this->logMasterWalletRedirection($e->getMessage());

                throw $e;

                // TODO: Send email (Benson to fix)]
            }

            $this->logMasterWalletRedirection("========= END =========");
        } else {
            $this->logMasterWalletRedirection("Wallet redirection Job: No redirection needed");
        }

        DB::commit();
    }

    /**
     * Get redirector.
     *
     * @param int $depositCreditTypeId
     * @return mixed
     */
    protected function getRedirector(int $depositCreditTypeId)
    {
        switch (bank_credit_type_name($depositCreditTypeId)) {
            case BankCreditTypeContract::ETH:
                return resolve(EthereumRedirector::class);

            case BankCreditTypeContract::USDT_ERC20:
                return resolve(UsdtErc20Redirector::class);

            case BankCreditTypeContract::USDC:
                return resolve(UsdcRedirector::class);

            case BankCreditTypeContract::BTC:
                // BTC does not need to be redirected
                return;
            default:
                return;
        }
    }
}
