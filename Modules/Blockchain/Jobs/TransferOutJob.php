<?php

namespace Modules\Blockchain\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainFeeContract;
use Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;

class TransferOutJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LogCronOutput;

    /**
     * Witherawal ID.
     *
     * @var int
     */
    protected $withdrawalId;

    /**
     * Class constructor.
     *
     * @param int $withdrawalId
     */
    public function __construct(int $withdrawalId)
    {
        $this->withdrawalId = $withdrawalId;
    }

    public function handle(BlockchainWithdrawalContract $withdrawalRepo, BlockchainFeeContract $blockchainFeeRepo, SimpleGenerator $generator)
    {
        try {
            DB::beginTransaction();
            $this->logTransferOut('');
            $this->logTransferOut('------------------------------');
            $this->logTransferOut("Processing withdrawal id: $this->withdrawalId");

            $withdrawal = $withdrawalRepo->getModel()->lockForUpdate()->find($this->withdrawalId);

            $this->logTransferOut("Withdrawal: $withdrawal");

            if ($withdrawal && empty($withdrawal->transaction_hash)) {
                $this->logTransferOut("Going to transfer out");

                $info = $this->getCryptoCurrencyInfo($withdrawal);

                $this->logTransferOut($info['master_wallet_address']);

                if (empty($info['master_wallet_private_key'])) {
                    $hash = $info['repo']->transfer(
                        $info['master_wallet_address'],
                        $withdrawal->recipient_address,
                        $withdrawal->amount,
                        ['change_address' => $info['change_address'] ?: null]
                    );
                } else {
                    $hash = $info['repo']->transfer(
                        $info['master_wallet_address'],
                        $withdrawal->recipient_address,
                        $withdrawal->amount,
                        ['private_key' => $info['master_wallet_private_key']]
                    );
                }

                $fee = '0';

                if (is_array($hash)) {
                    $hash = isset($hash['hash'])? $hash['hash']: null;
                    $fee  = isset($hash['fee']) ? $hash['fee'] : 0;
                }

                $this->logTransferOut("Processing external transfer hash: " . $hash);
                $this->logTransferOut("Fee is $fee");

                if (!empty($hash)) {
                    $this->logTransferOut("Updating withdrawal record with hash");
                    $withdrawal->update(['transaction_hash' => $hash]);
                    $this->logTransferOut("Done updating hash");

                    $blockchainFeeCreditTypeId = getBlockchainFeeCreditTypeId($withdrawal->source_credit_type_id);
                    $this->logTransferOut("Fee credit type ID is $blockchainFeeCreditTypeId");

                    if (!empty($blockchainFeeCreditTypeId)) {
                        $this->logTransferOut('Creating new fee record');

                        $blockchainFeeRepo->add([
                            'bank_credit_type_id'                => $blockchainFeeCreditTypeId,
                            'blockchain_fee_transaction_type_id' => blockchainFeeTransactionTypeId(BlockchainFeeTransactionTypeContract::WITHDRAWAL),
                            'transaction_hash'                   => $hash,
                            'blockchain_fee'                     => $fee,
                            'causer_reference_number'            => $withdrawal->reference_number
                        ]);

                        $this->logTransferOut('Done recording new fee');
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->logTransferOut($e->getMessage());
            $this->logTransferOut($e->getTraceAsString());
            throw $e;
        }
    }

    public function getCryptoCurrencyInfo($withdrawal)
    {
        $bankCreditTypeRepo = resolve(BankCreditTypeContract::class);
        $bankCreditTypeSlug = $bankCreditTypeRepo->find($withdrawal->destination_credit_type_id)->rawname;

        $this->logTransferOut("Withdrawal destination type id: $bankCreditTypeSlug");

        switch ($bankCreditTypeSlug) {
            case BankCreditTypeContract::BTC:
                //We need to empty the from wallet address, it will use the sender who have money in unspent list
                return [
                    'master_wallet_address'     => '',
                    'master_wallet_private_key' => '',
                    'repo'                      => resolve(BitcoinContract::class),
                    'change_address'            => setting('master_wallet_btc_address'),
                ];

            case BankCreditTypeContract::USDT_OMNI:
                return [
                    'master_wallet_address'     => setting('master_wallet_btc_address'),
                    'master_wallet_private_key' => decrypt(setting('master_wallet_btc_private_key')),
                    'repo'                      => resolve(UsdtRepository::class)
                ];

            case BankCreditTypeContract::ETH:
                return [
                    'master_wallet_address'     => setting('master_wallet_eth_address'),
                    'master_wallet_private_key' => decrypt(setting('master_wallet_eth_private_key')),
                    'repo'                      => resolve(EthereumRepository::class)
                ];

            case BankCreditTypeContract::USDT_ERC20:
                return [
                    'master_wallet_address'     => setting('master_wallet_eth_address'),
                    'master_wallet_private_key' => decrypt(setting('master_wallet_eth_private_key')),
                    'repo'                      => resolve(UsdtErc20Repository::class)
                ];

            case BankCreditTypeContract::USDC:
                return [
                    'master_wallet_address'     => setting('master_wallet_eth_address'),
                    'master_wallet_private_key' => decrypt(setting('master_wallet_eth_private_key')),
                    'repo'                      => resolve(UsdcRepository::class)
                ];
        }
    }
}
