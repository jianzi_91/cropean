<?php

use Illuminate\Support\Facades\Cache;
use Modules\Blockchain\Contracts\BlockchainFeeTransactionTypeContract;
use Modules\Credit\Contracts\BankCreditTypeContract;

if (!function_exists('bigDecToHex')) {
    /**
     * Converts a big decimal number to hex to prevent integer overflow when number is too large for dechex function.
     *
     * @param string $bigDecimal
     * @return string
     */
    function bigDecToHex(string $bigDecimal)
    {
        $hex = '';

        do {
            $last       = bcmod($bigDecimal, 16);
            $hex        = dechex($last) . $hex;
            $bigDecimal = bcdiv(bcsub($bigDecimal, $last), 16);
        } while ($bigDecimal > 0);

        return $hex;
    }
}

if (!function_exists('bigHexToDec')) {
    /**
     * Converts a big hex number to decimal string.
     *
     * @param string $bigHex
     * @return string
     */
    function bigHexToDec(string $bigHex)
    {
        // If string starts with 0x, remove it
        if (0 === strpos($bigHex, '0x')) {
            $bigHex = substr($bigHex, 2);
        }

        $power         = 0;
        $decimalNumber = '0';
        $characters    = array_reverse(str_split($bigHex));

        foreach ($characters as $char) {
            $digit         = hexdec($char);
            $digitTotal    = bcmul($digit, bcpow(16, $power), 0);
            $decimalNumber = bcadd($decimalNumber, $digitTotal, 0);

            $power++;
        }

        return $decimalNumber;
    }
}

if (!function_exists('stripHexPrefix')) {
    /**
     * Strip the 0x prefix from a hexadecimal string.
     *
     * @param string $hex
     * @return string
     */
    function stripHexPrefix(string $hex)
    {
        // If string starts with 0x, remove it
        if (0 === strpos($hex, '0x')) {
            return substr($hex, 2);
        }

        return $hex;
    }
}

if (!function_exists('pairInvert')) {
    /**
     * Pair invert.
     *
     * @param string $hex
     * @return string
     */
    function pairInvert(string $hex)
    {
        $inverted    = '';
        $stringIndex = strlen($hex) - 2;

        while ($stringIndex >= 0) {
            $inverted .= substr($hex, $stringIndex, 2);
            $stringIndex -= 2;
        }

        return $inverted;
    }
}

if (!function_exists('blockchainFeeTransactionTypeId')) {
    function blockchainFeeTransactionTypeId(string $name)
    {
        return Cache::tags('blockchainFee')
            ->rememberForever("transactionType.$name", function () use ($name) {
                $feeTxTypeRepo = resolve(BlockchainFeeTransactionTypeContract::class);
                return $feeTxTypeRepo->findBySlug($name)->id;
            });
    }
}

if (!function_exists('getBlockchainFeeCreditTypeId')) {
    function getBlockchainFeeCreditTypeId(int $bankCreditTypeId)
    {
        switch (bank_credit_type_name($bankCreditTypeId)) {
            case BankCreditTypeContract::BTC:
            case BankCreditTypeContract::USDT_OMNI:
                return bank_credit_type_id(BankCreditTypeContract::BTC);
            case BankCreditTypeContract::ETH:
            case BankCreditTypeContract::USDC:
            case BankCreditTypeContract::USDT_ERC20:
                return bank_credit_type_id(BankCreditTypeContract::ETH);
        }
    }
}
