<?php

namespace Modules\Blockchain\Contracts\CryptoCurrencies;

use Modules\Blockchain\Exceptions\InsufficientBlockchainFundsException;
use stdClass;

/**
 * Contract for many common functions used in blockchain context.
 *
 * @package Modules\Blockchain\Contracts\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface CryptoCurrencyContract
{
    /**
     * A constant to represent entire balance. This is mainly used for transfer functions.
     */
    const ENTIRE_BALANCE = '-1';

    /**
     * Get ticker.
     *
     * @return string
     */
    public function getTicker();

    /**
     * Get network.
     *
     * @return string
     */
    public function getNetwork();

    /**
     * Get network.
     *
     * @return string
     */
    public function getNodes();

    /**
     * Convert from fractional to main unit.
     *
     * @param string $fractionalUnitAmount
     * @return string
     */
    public function convertToMainUnit(string $fractionalUnitAmount);

    /**
     * Convert from main to fractional unit.
     *
     * @param string $mainUnitAmount
     * @return mixed
     */
    public function convertToFractionalUnit(string $mainUnitAmount);

    /**
     * Returns the number of decimal places supported by the cryptocurrency.
     *
     * @return mixed
     */
    public function getDecimalPlaces();

    /**
     * Generate a new address.
     *
     * @return mixed
     */
    public function createNewAddress();

    /**
     * Get latest block number.
     *
     * @return int
     */
    public function getLatestBlockNumber();

    /**
     * Get all transactions in a particular block number.
     *
     * @param int $blockNo
     * @return mixed
     */
    public function getBlockTransactions(int $blockNo);

    /**
     * Get transaction details
     *
     * @param string $transactionHash
     * @return stdClass
     */
    public function getTransactionDetails(string $transactionHash);

    /**
     * Get balance of address.
     *
     * @param string $address
     * @return int
     */
    public function getBalance(string $address);

    /**
     * Transfer credit between 2 addresses.
     *
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $amount
     * @param array $options
     * @return mixed
     * @throws InsufficientBlockchainFundsException
     * @throws \Exception
     *
     */
    public function transfer(string $fromAddress, string $toAddress, string $amount, array $options = []);

    /**
     * Validate if address format is valid.
     *
     * @param string $address
     * @return bool
     */
    public function validateAddress(string $address);

    /**
     * @param string $transactionHash
     * @return mixed
     */
    public function getConfirmations(string $transactionHash);
}
