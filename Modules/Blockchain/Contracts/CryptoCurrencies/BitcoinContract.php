<?php

namespace Modules\Blockchain\Contracts\CryptoCurrencies;

/**
 * Contract for many common functions used in blockchain context.
 *
 * @package Modules\Blockchain\Contracts\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface BitcoinContract extends CryptoCurrencyContract
{
    /**
     * Gets all the unspent transaction outputs belonging to a particular address.
     *
     * @param $addresses
     * @param int $minConfirmations
     * @param int $maxConfirmations
     * @return mixed
     * @throws \Exception
     */
    public function listUnspent($addresses, int $minConfirmations = 1, int $maxConfirmations = 9999999);

    /**
     * Create raw transaction based on inputs and outputs.
     *
     * @param array $inputs
     * @param array $outputs
     * @return mixed
     * @throws \Exception
     */
    public function createRawTransaction(array $inputs, array $outputs);

    /**
     * Send raw transaction.
     *
     * @param string $hexData
     * @return mixed
     * @throws \Exception
     */
    public function sendRawTransaction(string $hexData);
}
