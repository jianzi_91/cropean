<?php

namespace Modules\Blockchain\Contracts\CryptoCurrencies;

use Modules\Blockchain\Contracts\NonceGenerators\NonceGeneratorContract;

/**
 * Contract for Ethereum-specific functions.
 *
 * @package Modules\Blockchain\Contracts\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface EthereumContract extends CryptoCurrencyContract
{
    /**
     * Get current gas price in node.
     *
     * @param string $format
     * @return mixed|string
     * @throws \Exception
     */
    public function getGasPrice(string $format = 'hex');

    /**
     * Get transaction receipt.
     *
     * @param string $transactionHash
     * @return mixed
     */
    public function getTransactionReceipt(string $transactionHash);

    /**
     * Sign raw transaction for an ether transfer transaction.
     *
     * @param NonceGeneratorContract $nonceGenerator
     * @param string $senderPrivateKey
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $fractionalAmount
     * @param bool $feeInclusive
     * @return string
     */
    public function signRawTransactionForEtherTransfer(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $fromAddress,
        string $toAddress,
        string $fractionalAmount,
        bool $feeInclusive = false
    );

    /**
     * Sign raw transaction for a contract call.
     *
     * @param NonceGeneratorContract $nonceGenerator
     * @param string $senderPrivateKey
     * @param string $contractAddress
     * @param string $fromAddress
     * @param string $data
     * @return string
     * @throws \Exception
     */
    public function signRawTransactionForContractCall(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $contractAddress,
        string $fromAddress,
        string $data
    );

    /**
     * Sign raw transaction using a private key.
     *
     * @param NonceGeneratorContract $nonceGenerator
     * @param string $senderPrivateKey
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $fractionalAmount
     * @param string $gasLimit
     * @param string $data
     * @param bool $feeInclusive
     * @return string
     */
    public function signRawTransaction(
        NonceGeneratorContract $nonceGenerator,
        string $senderPrivateKey,
        string $fromAddress,
        string $toAddress,
        string $fractionalAmount,
        string $gasLimit,
        string $data = '',
        bool $feeInclusive = false
    );
}
