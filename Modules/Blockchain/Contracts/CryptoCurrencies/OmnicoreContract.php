<?php

namespace Modules\Blockchain\Contracts\CryptoCurrencies;

/**
 * Contract for many common functions used in blockchain context.
 *
 * @package Modules\Blockchain\Contracts\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface OmnicoreContract extends CryptoCurrencyContract
{
    /**
     * Get property ID.
     *
     * @return mixed
     */
    public function getPropertyId();

    /**
     * Get total wallet Bitcoin balance in blockchain
     *
     * @return mixed
     */
    public function getWalletBitcoinBalance();

    /**
     * Runs the "omni_listtransactions" command over JSON-RPC.
     *
     * @param string $txId
     * @param int $count
     * @param int $skip
     * @param int $startBlock
     * @param int $endBlock
     * @return mixed
     */
    public function getWalletTransactions(string $txId, int $count, int $skip, int $startBlock, int $endBlock);

    /**
     * Redirect funds to another wallet address.
     *
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $amount
     * @param array $options
     * @return mixed
     */
    public function redirect(string $fromAddress, string $toAddress, string $amount, array $options);
}
