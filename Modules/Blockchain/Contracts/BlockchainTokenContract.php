<?php

namespace Modules\Blockchain\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

/**
 * Contract for blockchain tokens.
 *
 * @package Modules\Blockchain\Contracts\CryptoCurrencies
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface BlockchainTokenContract extends CrudContract
{
    /**
     * Get contract address for a credit type.
     *
     * @param int $bankCreditTypeId
     * @return mixed
     */
    public function getContractAddress(int $bankCreditTypeId);

    /**
     * Get property ID.
     *
     * @param int $bankCreditTypeId
     * @return mixed
     */
    public function getPropertyId(int $bankCreditTypeId);

    /**
     * Get all bank credit type IDs.
     *
     * @return mixed
     */
    public function getErc20CreditTypeIds();

    /**
     * Get ERC20 tokens.
     *
     * @return mixed
     */
    public function getErc20Tokens();
}
