<?php

namespace Modules\Blockchain\Contracts\NonceGenerators;

/**
 * Contract for nonce generator.
 *
 * @package Modules\Blockchain\Contracts\NonceGenerators
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface NonceGeneratorContract
{
    /**
     * Convert from fractional to main unit.
     *
     * @param string $address
     * @param string $format
     * @return string
     */
    public function nextNonce(string $address, string $format = 'hex');
}
