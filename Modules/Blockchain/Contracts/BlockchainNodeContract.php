<?php

namespace Modules\Blockchain\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

/**
 * Contract for blockchain nodes.
 *
 * @package Modules\Blockchain\Contracts
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
interface BlockchainNodeContract extends CrudContract
{
    const BTC_NETWORK = 'BTC';
    const ETH_NETWORK = 'ETH';
}
