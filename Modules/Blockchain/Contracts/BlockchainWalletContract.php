<?php

namespace Modules\Blockchain\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BlockchainWalletContract extends CrudContract, SlugContract
{
    /**
     * Create wallet
     *
     * @param unknown $userId
     * @return Wallet
     */
    public function createWallets($userId);

    /**
     * Get balance by user Id
     * @param int $userId
     * @return double
     */
    public function getBalanceByUser(int $userId);

    /**
     * Get balance by address
     * @param string $address
     * @return double
     */
    public function getBalanceByAddress(string $address);

    /**
     * Get user address
     * @param int $userId
     * @param array $with
     * @param int $bankCreditTypeId
     * @return string
     */
    public function getUserAddress(int $userId, array $with = [], int $bankCreditTypeId = 0);

    /**
     * Transfer VC by address
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $amount
     * @param string $remarks
     * @param bool $isWithdrawal
     * @param bool $feeInclusive
     * @return boolean
     */
    public function transferByAddress(
        string $fromAddress,
        string $toAddress,
        string $amount,
        string $remarks = null,
        bool $isWithdrawal = true,
        bool $feeInclusive = false
    );

    /**
     * Transfer VC by user
     * @param string $fromAddress
     * @param string $toAddress
     * @param string $amount
     * @param string $remarks
     * @return boolean
     */
    public function transferByUser(int $fromUserId, int $toUserId, string $amount, string $remarks = null);

    /**
     * Get qr code
     * @param unknown $walletId
     * return file stream
     */
    public function getQrCode($walletId);

    /**
     * Returns an array of addresses which exists in the database.
     *
     * @param array $addresses
     * @param int $bankCreditTypeId
     * @return array
     */
    public function filterExistingAddresses(array $addresses, int $bankCreditTypeId);

    /**
     * Returns an array of addresses which exists in the database and are ERC20 token addresses.
     *
     * @param array $addresses
     * @return mixed
     */
    public function filterERC20Addresses(array $addresses);

    /**
     * Find record by address,
     *
     * @param string $address
     * @param int $bankCreditTypeId
     * @return mixed
     */
    public function findByAddress(string $address, int $bankCreditTypeId);

    /**
     * Find by user ID and credit type.
     *
     * @param int $userId
     * @param int $bankCreditTypeId
     * @return mixed
     */
    public function findByUserIdAndCreditType(int $userId, int $bankCreditTypeId);

    /**
     * Get all balances.
     *
     * @param int $userId
     * @return mixed
     */
    public function getAllBalances(int $userId);

    /**
     * Generate QR code.
     *
     * @param $text
     * @param int $size
     * @return mixed
     */
    public function generateQrCode($text, $size = 280);
}
