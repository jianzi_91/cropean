<?php

namespace Modules\Blockchain\Contracts;

use stdClass;

/**
 * Contract for ERC20 ether requests.
 *
 * @package Trading\Wallets\Contracts
 */
interface MasterWalletRedirectorContract
{
    /**
     * Redirect address balance to hot wallet.
     *
     * @param string $address
     * @param stdClass|null $miscData
     * @return mixed
     */
    public function redirectToMasterWallet(string $address, stdClass $miscData = null);
}
