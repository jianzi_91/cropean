<?php

namespace Modules\Blockchain\Contracts\Parser;

/**
 * Contract for processed blocks.
 *
 * @package Modules\BlockchainParser\Contracts
 */
interface MasterWalletRedirectorContract
{
    /**
     * Get latest processed block number.
     *
     * @param string $address
     * @param string $amount
     * @return mixed
     */
    public function redirect(int $userId, string $amount);

    /**
     * Get fee.
     *
     * @return mixed
     */
    public function getFee();
}
