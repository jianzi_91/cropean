<?php

namespace Modules\Blockchain\Contracts\Parser;

use Plus65\Base\Repositories\Contracts\CrudContract;

/**
 * Contract for processed blocks.
 *
 * @package Modules\BlockchainParser\Contracts
 */
interface BlockchainEtherRequestContract extends CrudContract
{
    /**
     * Check if address has pending ether request.
     *
     * @param string $address
     * @return mixed
     */
    public function addressHasPendingRequest(string $address);

    /**
     * Return hashes that exists and not yet marked as mined.
     *
     * @param array $hashes
     * @return mixed
     */
    public function filterExistingHashes(array $hashes);

    /**
     * Mark a hash as mined.
     *
     * @param string $hash
     * @return mixed
     */
    public function markHashAsMined(string $hash);

    /**
     * @param string $address
     * @return mixed|void
     */
    public function addressPendingRequest(string $address);
}
