<?php

namespace Modules\Blockchain\Contracts\Parser;

use stdClass;

/**
 * Contract for formatting transactions data in a block.
 *
 * @package Modules\Blockchain\Contracts\Parser
 */
interface BlockDataCollector
{
    /**
     * Collect the data that the collector is interested in.
     *
     * @param array $transactions
     * @param stdClass|null $miscData
     * @return mixed
     */
    public function collectTransactions(array $transactions, stdClass $miscData = null);

    /**
     * Fire data handlers.
     *
     * @param array $collectedData
     * @return mixed
     */
    public function collectHandlerEvents(array $collectedData);
}
