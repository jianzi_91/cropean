<?php

namespace Modules\Blockchain\Contracts\Parser;

use Modules\Blockchain\Contracts\CryptoCurrencies\CryptoCurrencyContract;

/**
 * Contract for processing a block in a cron, where blocks need to be processed in sequence, and also after a number of confirmations.
 *
 * @package Modules\BlockchainParser\Contracts
 */
interface BlockProcessor
{
    /**
     * Get the cryptocurrency service associated with the processor.
     *
     * @return CryptoCurrencyContract
     */
    public function getCryptoCurrencyService();

    /**
     * Get latest block number that is processed by the cron.
     *
     * @return mixed
     */
    public function getLatestProcessedBlockNumber();

    /**
     * Returns the minimum confirmations required for a transaction to be considered valid. When a block is formed,
     * it is counted as 1 confirmation.
     *
     * @return int
     */
    public function getMinimumConfirmationsRequired();

    /**
     * Process the block number.
     *
     * @param int $blockNumber
     * @return mixed
     */
    public function processBlock(int $blockNumber);
}
