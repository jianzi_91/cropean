<?php

namespace Modules\Blockchain\Contracts\Parser;

/**
 * Contract for filtering of data, and processing of data.
 *
 * @package Modules\BlockchainParser\Contracts
 */
interface FilteredDataHandlerContract
{
    /**
     * Filter data.
     *
     * @param array $data
     * @return mixed
     */
    public function filter(array $data);

    /**
     * Process filtered data.
     *
     * @param array $filteredData
     * @return mixed
     */
    public function processFilteredData(array $filteredData);
}
