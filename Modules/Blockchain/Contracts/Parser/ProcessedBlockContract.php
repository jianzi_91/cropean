<?php

namespace Modules\Blockchain\Contracts\Parser;

use Plus65\Base\Repositories\Contracts\CrudContract;

/**
 * Contract for processed blocks.
 *
 * @package Modules\BlockchainParser\Contracts
 */
interface ProcessedBlockContract extends CrudContract
{
    /**
     * Get latest processed block number.
     *
     * @param string $blockProcessorType
     * @return mixed
     */
    public function getLatestBlockNumber(string $blockProcessorType);
}
