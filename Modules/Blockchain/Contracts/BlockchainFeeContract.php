<?php

namespace Modules\Blockchain\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface BlockchainFeeContract extends CrudContract
{
    /**
     * Get Ethereum fees. Default to only return those not updated yet.
     *
     * @param bool $isUpdated
     * @return mixed
     */
    public function getEthFees(bool $isUpdated = false);
}
