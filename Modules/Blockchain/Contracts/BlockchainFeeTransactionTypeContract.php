<?php

namespace Modules\Blockchain\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BlockchainFeeTransactionTypeContract extends CrudContract, SlugContract
{
    const MASTER_WALLET_REDIRECTION = 'master_wallet_redirection';
    const ETHER_REQUEST             = 'ether_request';
    const WITHDRAWAL                = 'withdrawal';
}
