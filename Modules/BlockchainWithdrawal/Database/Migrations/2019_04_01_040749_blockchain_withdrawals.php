<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlockchainWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('source_credit_type_id')->index();
            $table->unsignedInteger('destination_credit_type_id')->index();
            $table->unsignedInteger('refund_credit_type_id')->index();
            $table->string('reference_number')->index();
            $table->unsignedInteger('user_id')->index()->nullable();
            $table->decimal('amount', 40, 20)->default(0.00000000000000000000);
            $table->decimal('admin_fee', 20, 6)->default(0.000000);
            $table->string('recipient_name');
            $table->string('recipient_address');
            $table->unsignedInteger('withdrawal_type_id')->nullable()->index('withdrawal_type_id');
            $table->text('remarks')->nullable();
            $table->unsignedInteger('updated_by')->nullable()->index('updated_by');
            $table->string('transaction_hash')->nullable()->unique();
            $table->boolean('is_master_wallet')->default(0);
            $table->unsignedInteger('confirmations');
            $table->boolean('is_confirmed')->default(false);

            // Constraints
            $table->foreign('withdrawal_type_id')->references('id')->on('withdrawal_types');
            $table->foreign('source_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('destination_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('refund_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_withdrawals');
    }
}
