<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecipientUserIdBlockchainWithdrawalTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blockchain_withdrawals', function (Blueprint $table) {
            $table->unsignedInteger('recipient_user_id')->after('user_id')->nullable();

            $table->foreign('recipient_user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blockchain_withdrawals', function (Blueprint $table) {
            $table->dropColumn('recipient_user_id');
        });
    }
}
