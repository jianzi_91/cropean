<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBlockchainWithdrawalTableAddStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blockchain_withdrawals', function (Blueprint $table) {
            $table->unsignedInteger('blockchain_withdrawal_status_id')->nullable()->index();

            $table->foreign('blockchain_withdrawal_status_id')
                ->references('id')
                ->on('blockchain_withdrawal_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
