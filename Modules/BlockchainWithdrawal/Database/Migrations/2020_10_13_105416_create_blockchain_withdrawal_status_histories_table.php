<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockchainWithdrawalStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_withdrawal_status_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('blockchain_withdrawal_status_id');
            $table->unsignedInteger('blockchain_withdrawal_id');
            $table->unsignedInteger('updated_by')->nullable();
            $table->text('remark')->nullable();
            $table->tinyInteger('is_current')->default(0);

            $table->foreign('blockchain_withdrawal_status_id', 'withdrawal_status_id')
                ->references('id')
                ->on('blockchain_withdrawal_statuses');
            
            $table->foreign('blockchain_withdrawal_id', 'withdrawal_id')
                ->references('id')
                ->on('blockchain_withdrawals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_withdrawal_status_histories');
    }
}
