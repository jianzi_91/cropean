<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlockchainWithdrawalTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_withdrawal_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('amount', 40, 20)->default(0.00000000000000000000);
            $table->string('transaction_code')->index()->unique();
            $table->unsignedInteger('blockchain_withdrawal_id')->index('blockchain_withdrawal_id_index');
            $table->unsignedInteger('bank_credit_type_id')->index()->nullable();
            $table->unsignedInteger('bank_transaction_type_id')->index('bank_transaction_type_id_index');

            // Constraints
            $table->unique(['blockchain_withdrawal_id', 'bank_credit_type_id', 'bank_transaction_type_id'], 'withdrawal_duplicate_constraint');
            $table->foreign('bank_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('bank_transaction_type_id', 'bank_transaction_type_id_foreign')->references('id')->on('bank_transaction_types');
            $table->foreign('blockchain_withdrawal_id', 'blockchain_withdrawal_id_foreign')->references('id')->on('blockchain_withdrawals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_transactions');
    }
}
