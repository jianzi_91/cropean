<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockchainWithdrawalAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_withdrawal_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('bank_credit_type_id');
            $table->string('name');
            $table->string('address');

            $table->unique(['user_id', 'bank_credit_type_id', 'address', 'deleted_at'], 'address_unique');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_withdrawal_addresses');
    }
}
