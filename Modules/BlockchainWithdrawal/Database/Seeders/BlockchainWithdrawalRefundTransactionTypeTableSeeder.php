<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Models\BankTransactionType;

class BlockchainWithdrawalRefundTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $bankTransactionTypes = [
            [
                'name' => 'blockchain_withdrawal_rejected',
                'is_active' => 1,
            ],
            [
                'name' => 'blockchain_withdrawal_cancelled',
                'is_active' => 1,
            ],
        ];

        foreach($bankTransactionTypes as $bankTransactionType){
            (new BankTransactionType($bankTransactionType))->save();
        }

        DB::commit();
    }
}
