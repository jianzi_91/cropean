<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders\Usdc;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdcWithdrawalSettingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain_withdrawal',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        $limits = [
            'withdrawal_usdc_min'                  => 0,
            'withdrawal_usdc_multiple'             => 0,
            'withdrawal_usdc_max'                  => 0,
            'withdrawal_usdc_admin_fee_percentage' => 0,
            'withdrawal_usdc_admin_fee_flat'       => 0,
        ];

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 0;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
