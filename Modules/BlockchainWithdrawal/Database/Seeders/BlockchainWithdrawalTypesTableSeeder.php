<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Withdrawal\Models\WithdrawalType;

class BlockchainWithdrawalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WithdrawalType::create([
            'name'             => 'blockchain',
            'name_translation' => 'blockchain',
            'fee_percentage'   => 0,
            'minimum_amount'   => 0.0001,
            'maximum_amount'   => 0,
            'multiples'        => 1,
            'is_active'        => true,
        ]);
    }
}
