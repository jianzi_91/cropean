<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawal;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;

class UpdateCurrentBlockchainWithdrawalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusRepo = resolve(BlockchainWithdrawalStatusContract::class);

        $withdrawals    = BlockchainWithdrawal::all();
        $approvedStatus = $statusRepo->findBySlug(BlockchainWithdrawalStatusContract::APPROVED);

        DB::beginTransaction();

        try {
            $statusRepo->bulkChangeHistory($withdrawals, $approvedStatus);

            foreach ($withdrawals as $withdrawal) {
                $withdrawal->update([
                    'blockchain_withdrawal_status_id' => $approvedStatus->id,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
        }
    }
}
