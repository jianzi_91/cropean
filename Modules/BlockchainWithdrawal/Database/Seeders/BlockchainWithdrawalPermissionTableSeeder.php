<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class BlockchainWithdrawalPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_blockchain_withdrawal_list',
                        'title' => 'Admin Blockchain Withdrawal List',
                    ],
                    [
                        'name'  => 'admin_blockchain_withdrawal_export',
                        'title' => 'Admin Blockchain Withdrawal Export',
                    ],
                ]
            ],
            'member_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_blockchain_withdrawal_list',
                        'title' => 'Member Blockchain Withdrawal List',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_create',
                        'title' => 'Member Blockchain Withdrawal Create',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_export',
                        'title' => 'Member Blockchain Withdrawal Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_blockchain_withdrawal_list',
                'admin_blockchain_withdrawal_export',
            ],
            Role::ADMIN => [
                'admin_blockchain_withdrawal_list',
                'admin_blockchain_withdrawal_export',
            ],
            Role::MEMBER => [
                'member_blockchain_withdrawal_list',
                'member_blockchain_withdrawal_create',
                'member_blockchain_withdrawal_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
