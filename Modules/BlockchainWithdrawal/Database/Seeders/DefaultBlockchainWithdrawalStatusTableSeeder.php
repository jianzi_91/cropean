<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalStatus;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class DefaultBlockchainWithdrawalStatusTableSeeder extends Seeder
{
    protected $pageRepo;
    protected $groupRepo;

    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_blockchain_withdrawal_statuses',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $statuses = [
            0 => [
                'name'      => 'pending',
                'is_active' => 1,
            ],
            1 => [
                'name'      => 'processing',
                'is_active' => 1,
            ],
            2 => [
                'name'      => 'approved',
                'is_active' => 1,
            ],
            3 => [
                'name'      => 'rejected',
                'is_active' => 1,
            ],
            4 => [
                'name'      => 'cancelled',
                'is_active' => 1,
            ],
        ];

        foreach ($statuses as $status) {
            $result = (new BlockchainWithdrawalStatus($status))->save();
        }
    }
}
