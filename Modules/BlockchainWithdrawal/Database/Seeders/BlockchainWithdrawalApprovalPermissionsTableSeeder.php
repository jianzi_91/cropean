<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class BlockchainWithdrawalApprovalPermissionsTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_blockchain_withdrawal_edit',
                        'title' => 'Admin Blockchain Withdrawal List',
                    ],
                ]
            ],
            'member_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_blockchain_withdrawal_cancel',
                        'title' => 'Member Blockchain Withdrawal Cancel',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_blockchain_withdrawal_edit',
            ],
            Role::ADMIN => [
                'admin_blockchain_withdrawal_edit',
            ],
            Role::MEMBER => [
                'member_blockchain_withdrawal_cancel',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
