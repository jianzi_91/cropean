<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders\UsdtErc20;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Repositories\SettingRepository;

class UsdtErc20WithdrawalSettingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->settings();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function settings()
    {
        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'blockchain_withdrawal',
            'is_active' => 1,
        ]);

        $settingRepository = resolve(SettingRepository::class);

        $limits = [
            'withdrawal_usdt_erc20_min'                  => 0,
            'withdrawal_usdt_erc20_multiple'             => 0,
            'withdrawal_usdt_erc20_max'                  => 0,
            'withdrawal_usdt_erc20_admin_fee_percentage' => 0,
            'withdrawal_usdt_erc20_admin_fee_flat'       => 0,
        ];

        foreach ($limits as $key => $value) {
            $setting                      = new Setting;
            $setting->name                = $key;
            $setting->setting_category_id = $settingCategory->id;
            $setting->is_hidden           = 0;
            $setting->is_active           = 1;
            $setting->save();
            $settingRepository->addValuesBySlug($key, $value);
        }
    }
}
