<?php

namespace Modules\BlockchainWithdrawal\Database\Seeders;

use DB;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class BlockchainWithdrawalAddressesPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::beginTransaction();

        $this->abilities();
        $this->permissions();

        DB::commit();
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilitySections = [
            'admin_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id(AbilityCategoryContract::ADMIN),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_blockchain_withdrawal_addresses_list',
                        'title' => 'Admin Blockchain Withdrawal Address List',
                    ],
                    [
                        'name'  => 'admin_blockchain_withdrawal_addresses_export',
                        'title' => 'Admin Blockchain Withdrawal Address Export',
                    ],

                ]
            ],
            'member_blockchain_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id(AbilityCategoryContract::MEMBER),
                    'name'        => 'Blockchain Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_blockchain_withdrawal_addresses_list',
                        'title' => 'Member Blockchain Withdrawal Address List',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_addresses_export',
                        'title' => 'Member Blockchain Withdrawal Address Export',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_addresses_create',
                        'title' => 'Member Blockchain Withdrawal Address Create',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_addresses_edit',
                        'title' => 'Member Blockchain Withdrawal Address Edit',
                    ],
                    [
                        'name'  => 'member_blockchain_withdrawal_addresses_delete',
                        'title' => 'Member Blockchain Withdrawal Address Delete',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_blockchain_withdrawal_addresses_list',
                'admin_blockchain_withdrawal_addresses_export',
            ],
            Role::ADMIN => [
                'admin_blockchain_withdrawal_addresses_list',
                'admin_blockchain_withdrawal_addresses_export',
            ],
            Role::MEMBER => [
                'member_blockchain_withdrawal_addresses_list',
                'member_blockchain_withdrawal_addresses_export',
                'member_blockchain_withdrawal_addresses_create',
                'member_blockchain_withdrawal_addresses_edit',
                'member_blockchain_withdrawal_addresses_delete'
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
