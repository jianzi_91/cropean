<?php

namespace Modules\BlockchainWithdrawal\Queries\Member;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\BlockchainWithdrawal\Queries\BlockchainWithdrawalAddressQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BlockchainWithdrawalAddressQuery extends BaseQuery implements WithMapping, WithHeadings
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'name' => [
            'filter' => 'like',
            'table'  => 'blockchain_withdrawal_addresses',
            'column' => 'name'
        ],
        'credit_type' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawal_addresses',
            'column' => 'bank_credit_type_id'
        ],
        'address' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawal_addresses',
            'column' => 'address'
        ],
    ];

    /**
     * Set parameters
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $creditTypeRepo   = resolve(BankCreditTypeContract::class);
        $this->parameters = $params;

        $params['credit_type'] = isset($params['credit_type']) ? $creditTypeRepo->findBySlug($params['credit_type'])->id : null;

        $this->filterBroker->setParameters($params);

        return $this;
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('blockchain_withdrawal_addresses.user_id', auth()->user()->id);
    }

    public function map($address): array
    {
        return [
            $address->created_at,
            $address->name,
            $address->credit_type_name,
            $address->address,
        ];
    }

    public function headings(): array
    {
        return [
            __('m_wallet_withdrawal_address.date'),
            __('m_wallet_withdrawal_address.name'),
            __('m_wallet_withdrawal_address.credit type'),
            __('m_wallet_withdrawal_address.wallet address'),
        ];
    }
}
