<?php

namespace Modules\BlockchainWithdrawal\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\BlockchainWithdrawal\Queries\BlockchainWithdrawalQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BlockchainWithdrawalQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'date' => [
            'filter' => 'date_range',
            'table'  => 'blockchain_withdrawals',
            'column' => 'created_at'
        ],
        'transaction_hash' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawals',
            'column' => 'transaction_hash'
        ],
        'reference_number' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawals',
            'column' => 'reference_number'
        ],
        'credit_type' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawals',
            'column' => 'source_credit_type_id'
        ],
        'recipient_address' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawals',
            'column' => 'recipient_address'
        ],
        'status' => [
            'filter' => 'equal',
            'table'  => 'blockchain_withdrawals',
            'column' => 'blockchain_withdrawal_status_id',
        ],
    ];

    /**
     * Set parameters
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $creditTypeRepo   = resolve(BankCreditTypeContract::class);
        $this->parameters = $params;

        $params['credit_type'] = isset($params['credit_type']) ? $creditTypeRepo->findBySlug($params['credit_type'])->id : null;

        $this->filterBroker->setParameters($params);

        return $this;
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('blockchain_withdrawals.user_id', auth()->user()->id);
    }

    public function map($withdrawal): array
    {
        return [
            $withdrawal->created_at,
            __('s_bank_credit_types.' . $withdrawal->source_credit_type_name),
            $withdrawal->recipient_address,
            $withdrawal->transaction_hash,
            amount_format($withdrawal->amount, credit_precision($withdrawal->source_credit_type_name)),
            $withdrawal->status->status->name,
        ];
    }

    public function headings(): array
    {
        return [
            __('m_wallet_withdrawals.date'),
            __('m_wallet_withdrawals.currency type'),
            __('m_wallet_withdrawals.recipient address'),
            __('m_wallet_withdrawals.transaction hash'),
            __('m_wallet_withdrawals.amount'),
            __('m_wallet_withdrawals.status'),
        ];
    }
}
