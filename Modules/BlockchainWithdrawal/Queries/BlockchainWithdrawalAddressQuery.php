<?php

namespace Modules\BlockchainWithdrawal\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalAddress;
use QueryBuilder\QueryBuilder;

class BlockchainWithdrawalAddressQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    protected $paginate = false;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = BlockchainWithdrawalAddress::select('blockchain_withdrawal_addresses.*', 'users.member_id as member_id', 'users.email as email', 'users.username as username', 'bank_credit_types.name_translation as credit_type_name')
            ->join('users', 'users.id', 'blockchain_withdrawal_addresses.user_id')
            ->join('bank_credit_types', 'bank_credit_types.id', '=', 'blockchain_withdrawal_addresses.bank_credit_type_id')
            ->orderBy('blockchain_withdrawal_addresses.created_at', 'desc');

        return $query;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->paginate ? $this->paginate() : $this->get();
    }

    /**
     * Get the value of paginate
     */
    public function getPaginate()
    {
        return $this->paginate;
    }
}
