<?php

namespace Modules\BlockchainWithdrawal\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawal;
use QueryBuilder\QueryBuilder;

class BlockchainWithdrawalQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    protected $paginate = false;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = BlockchainWithdrawal::select('blockchain_withdrawals.*', 'users.email as email', 'users.name as name', 'users.username as username', 'source_credit_type.name_translation as source_credit_type_name', 'users.member_id as member_id', 'blockchain_withdrawal_statuses.name_translation as status_name')
            ->join('users', 'users.id', '=', 'blockchain_withdrawals.user_id')
            ->join('bank_credit_types as source_credit_type', 'source_credit_type.id', '=', 'blockchain_withdrawals.source_credit_type_id')
            ->join('bank_credit_types as destination_credit_type', 'destination_credit_type.id', '=', 'blockchain_withdrawals.destination_credit_type_id')
            ->join('blockchain_withdrawal_statuses', 'blockchain_withdrawal_statuses.id', '=', 'blockchain_withdrawals.blockchain_withdrawal_status_id')
            ->orderBy('blockchain_withdrawals.created_at', 'desc');

        return $query;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->paginate ? $this->paginate() : $this->get();
    }

    /**
     * Get the value of paginate
     */
    public function getPaginate()
    {
        return $this->paginate;
    }
}
