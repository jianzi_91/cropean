<?php

namespace Modules\BlockchainWithdrawal\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\BlockchainWithdrawal\Queries\BlockchainWithdrawalQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BlockchainWithdrawalQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'date' => [
            'filter'    => 'date_range',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'updated_at' => [
            'filter'    => 'date_range',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'updated_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'transaction_hash' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'transaction_hash',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'reference_number' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'reference_number',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'credit_type' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'source_credit_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'recipient_address' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'recipient_address',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'status' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_withdrawals',
            'column'    => 'blockchain_withdrawal_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Set parameters
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $creditTypeRepo   = resolve(BankCreditTypeContract::class);
        $this->parameters = $params;

        $params['credit_type'] = isset($params['credit_type']) ? $creditTypeRepo->findBySlug($params['credit_type'])->id : null;

        $this->filterBroker->setParameters($params);

        return $this;
    }

    public function map($withdrawal): array
    {
        return [
            $withdrawal->created_at,
            $withdrawal->name,
            $withdrawal->member_id,
            $withdrawal->email,
            __('s_bank_credit_types.' . $withdrawal->source_credit_type_name),
            amount_format($withdrawal->amount, credit_precision($withdrawal->source_credit_type_name)),
            $withdrawal->status->status->name,
            $withdrawal->updated_at,
            $withdrawal->recipient_address,
            $withdrawal->transaction_hash,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_wallet_withdrawals.date'),
            __('a_wallet_withdrawals.full name'),
            __('a_wallet_withdrawals.member id'),
            __('a_wallet_withdrawals.email address'),
            __('a_wallet_withdrawals.currency type'),
            __('a_wallet_withdrawals.amount'),
            __('a_wallet_withdrawals.status'),
            __('a_external_transfers.updated at'),
            __('a_wallet_withdrawals.recipient address'),
            __('a_wallet_withdrawals.transaction hash'),
        ];
    }
}
