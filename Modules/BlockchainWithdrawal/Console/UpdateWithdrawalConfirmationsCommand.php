<?php

namespace Modules\BlockchainWithdrawal\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Repositories\CryptoCurrencies\BitcoinRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Handle logic such as withdrawals by parsing blockchain blocks based on confimrations required.
 *
 * @package Modules\BlockchainParser\Console
 */
class UpdateWithdrawalConfirmationsCommand extends Command
{
    use LogCronOutput;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'blockchain-parser:update-withdrawal-confirmations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update confirmations of withdrawals';

    /**
     * Blockchain withdrawal service.
     *
     * @var BlockchainWithdrawalContract
     */
    protected $withdrawalService;

    /**
     * Minimum confirmations required.
     *
     * @var array
     */
    protected $minConfirmations;

    /**
     * Class constructor
     * .
     * @param BlockchainwithdrawalContract $withdrawalService
     */
    public function __construct(BlockchainWithdrawalContract $withdrawalService)
    {
        parent::__construct();
        $this->withdrawalService = $withdrawalService;
    }

    /**
     * Setup variables.
     */
    protected function setup()
    {
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::BTC)]        = setting('withdrawal_btc_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::ETH)]        = setting('withdrawal_eth_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDT_OMNI)]  = setting('withdrawal_usdt_omni_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDT_ERC20)] = setting('withdrawal_usdt_erc20_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDC)]       = setting('withdrawal_usdc_min_confirmation');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->logBlockConfirmation('Start confirmation');

        DB::beginTransaction();

        $this->setup();

        $unprocessedWithdrawals = $this->withdrawalService->getUnprocessedWithdrawals();

        $this->logBlockConfirmation('');
        $this->logBlockConfirmation('------------------------------');
        $this->logBlockConfirmation('Total Unprocessed Withdrawals: ' . $unprocessedWithdrawals->count());
        $this->logBlockConfirmation('------------------------------');

        try {
            foreach ($unprocessedWithdrawals as $withdrawal) {
                $this->logBlockConfirmation('');
                $this->logBlockConfirmation("Processing Withdrawal Hash $withdrawal->transaction_hash");
                $this->logBlockConfirmation('--------------------------------------');

                $confirmations = $this->getCyrptoCurrencyService($withdrawal->destination_credit_type_id)
                        ->getConfirmations($withdrawal->transaction_hash);

                $minConfirmations = $this->minConfirmations[$withdrawal->destination_credit_type_id];
                $this->logBlockConfirmation('   Minimum Confirmation ' . $minConfirmations);

                $this->logBlockConfirmation('       Update DB Confirmation To ' . $confirmations);
                $this->withdrawalService->updateConfirmations($withdrawal->id, $confirmations, $minConfirmations);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->logBlockConfirmation('       Failed to confirmed' . $e->getMessage());
            logger($e->getMessage());
            logger($e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * Get service.
     *
     * @param int $bank_credit_type_id
     * @return mixed
     */
    protected function getCyrptoCurrencyService(int $bank_credit_type_id)
    {
        $className = '';

        switch (bank_credit_type_name($bank_credit_type_id)) {
            case BankCreditTypeContract::BTC:
                $className = BitcoinRepository::class;
                break;

            case BankCreditTypeContract::USDT_OMNI:
                $className = UsdtRepository::class;
                break;

            case BankCreditTypeContract::USDT_ERC20:
            case BankCreditTypeContract::USDC:
                $className = UsdtErc20Repository::class;
                break;

            case BankCreditTypeContract::ETH:
                $className = EthereumRepository::class;
                break;
        }

        return resolve($className);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
