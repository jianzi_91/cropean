<?php

namespace Modules\BlockchainWithdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlockchainWithdrawalStatusHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'blockchain_withdrawal_status_id',
        'blockchain_withdrawal_id',
        'updated_by',
        'remark',
        'is_current',
    ];

    protected $table = 'blockchain_withdrawal_status_histories';

    public function blockchain_withdrawal()
    {
        return $this->belongsTo(BlockchainWithdrawal::class, 'blockchain_withdrawal_id');
    }

    public function status()
    {
        return $this->belongsTo(BlockchainWithdrawalStatus::class, 'blockchain_withdrawal_status_id');
    }

    /**
     * Local scope for getting current status.
     *
     * @param unknown $query
     * @param unknown $current
     *
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where($this->getTable() . '.is_current', $current);
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
