<?php

namespace Modules\BlockchainWithdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Scopes\HasActiveScope;

class BlockchainWithdrawalStatus extends Model implements Stateable
{
    use SoftDeletes, HasActiveScope, Translatable, HasDropDown, HistoryRelation;

    protected $translatableAttributes = ['name'];

    protected $fillable = [
        'name',
        'name_translation',
        'is_active'
    ];

    protected $table = 'blockchain_withdrawal_statuses';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany(BlockchainWithdrawalStatusHistory::class, 'blockchain_withdrawal_status_id');
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return BlockchainWithdrawalStatusHistory::class;
    }

    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'blockchain_withdrawal_status_id';
    }
}
