<?php

namespace Modules\BlockchainWithdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Models\BankTransactionType;

class BlockchainWithdrawalTransaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * This model's relation to bank transaction type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(BankTransactionType::class, 'bank_transaction_type_id');
    }

    /**
     * This model's relation to bank credit type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function credit()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * This model's relation to blockchain withdrawal
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blockchainWithdrawal()
    {
        return $this->belongsTo(BlockchainWithdrawal::class, 'blockchain_withdrawal_id');
    }
}
