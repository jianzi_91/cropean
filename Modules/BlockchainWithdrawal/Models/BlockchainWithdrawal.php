<?php

namespace Modules\BlockchainWithdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\Credit\Models\BankCreditType;
use Modules\User\Models\User;
use Modules\Withdrawal\Models\WithdrawalType;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Relations\UserRelation;

class BlockchainWithdrawal extends Model implements Stateable
{
    use SoftDeletes, UserRelation, HistoryRelation;

    /**
     * Soft delete cascade.
     *
     * @var array
     */
    protected $softCascade = ['transactions'];

    protected $fillable = [
        'source_credit_type_id',
        'destination_credit_type_id',
        'refund_credit_type_id',
        'reference_number',
        'user_id',
        'recipient_user_id',
        'amount',
        'exchange_rate',
        'destination_amount',
        'admin_fee',
        'recipient_name',
        'recipient_address',
        'withdrawal_type_id',
        'remarks',
        'updated_by',
        'transaction_hash',
        'is_master_wallet',
        'confirmations',
        'is_confirmed',
        'blockchain_withdrawal_status_id'
    ];

    /**
     * This model's relation to withdrawal transaction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(BlockchainWithdrawalTransaction::class, 'withdrawal_id');
    }

    public function withdrawBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * This model's relation to user bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(WithdrawalType::class, 'withdrawal_type_id');
    }

    public function sourceCurrency()
    {
        return $this->belongsTo(BankCreditType::class, 'source_credit_type_id');
    }

    public function destinationCurrency()
    {
        return $this->belongsTo(BankCreditType::class, 'destination_credit_type_id');
    }

    public function getEntityId()
    {
        return $this->id;
    }

    public function getForeignKey()
    {
        return 'blockchain_withdrawal_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return BlockchainWithdrawalStatusHistory::class;
    }

    public function statuses()
    {
        return $this->hasMany(BlockchainWithdrawalStatusHistory::class, 'blockchain_withdrawal_id');
    }

    public function status()
    {
        return $this->hasOne(BlockchainWithdrawalStatusHistory::class, 'blockchain_withdrawal_id')->current(1);
    }

    public function getIsRejectableAttribute()
    {
        if (in_array($this->status->status->rawname, [BlockchainWithdrawalStatusContract::APPROVED])) {
            return false;
        }

        return true;
    }

    public function getIsCancellableAttribute()
    {
        if (!in_array($this->status->status->rawname, [BlockchainWithdrawalStatusContract::PENDING])) {
            return false;
        }

        return true;
    }

    public function getIsApprovableAttribute()
    {
        if (in_array($this->status->status->rawname, [BlockchainWithdrawalStatusContract::CANCELLED, BlockchainWithdrawalStatusContract::REJECTED])) {
            return false;
        }

        return true;
    }

    /**
     * Listening to events
     * @throws \Exception
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($withdrawal) {
            $blockchainWithdrawalStatusRepo = resolve(BlockchainWithdrawalStatusContract::class);
            $pendingStatus = $blockchainWithdrawalStatusRepo->findBySlug(BlockchainWithdrawalStatusContract::PENDING);

            if (!$blockchainWithdrawalStatusRepo->changeHistory($withdrawal, $pendingStatus)) {
                throw new \Exception();
            }
        });
    }
}
