<?php

namespace Modules\BlockchainWithdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Credit\Models\BankCreditType;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Relations\UserRelation;

class BlockchainWithdrawalAddress extends Model
{
    use SoftDeletes, UserRelation, HasDropDown;

    protected $table = 'blockchain_withdrawal_addresses';
    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Relation to bank credit type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankCreditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * Display name Mutator
     */
    public function getDisplayNameAttribute()
    {
        return $this->name . ' - ' . $this->address;
    }
}
