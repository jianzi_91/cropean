<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['prefix' => 'blockchain'], function () {
        Route::group(['domain' => config('core.admin_url')], function () {
            Route::put('withdrawals/bulk-update', ['as' => 'admin.blockchain.withdrawals.bulk-update', 'uses' => 'Admin\BlockchainWithdrawalController@bulkUpdate']);
            Route::get('withdrawals/export', ['as' => 'admin.blockchain.withdrawals.export', 'uses' => 'Admin\BlockchainWithdrawalExportController@index']);
            Route::resource('withdrawals', 'Admin\BlockchainWithdrawalController', ['as' => 'admin.blockchain'])->only(['index', 'edit', 'update', 'destroy']);

            Route::get('withdrawal-addresses/qr/image/{bankCreditName}', ['as' => 'admin.blockchain.withdrawal-addresses.qr', 'uses' => 'Admin\BlockchainWithdrawalAddressController@getQrImage']);
            Route::get('withdrawal-addresses/export', ['as' => 'admin.blockchain.withdrawal-addresses.export', 'uses' => 'Admin\BlockchainWithdrawalAddressExportController@index']);
            Route::resource('withdrawal-addresses', 'Admin\BlockchainWithdrawalAddressController', ['as' => 'admin.blockchain'])->only(['index', 'show']);
        });

        Route::group(['domain' => config('core.member_url')], function () {
            Route::get('withdrawals/export', ['as' => 'member.blockchain.withdrawals.export', 'uses' => 'Member\BlockchainWithdrawalExportController@index']);
            Route::post('withdrawals/calculate', 'Member\BlockchainWithdrawalController@calculate')->name('member.blockchain.withdrawals.calculate');
            Route::resource('withdrawals', 'Member\BlockchainWithdrawalController', ['as' => 'member.blockchain'])->only(['index', 'create', 'store', 'destroy']);

            Route::get('withdrawal-addresses/export', ['as' => 'member.blockchain.withdrawal-addresses.export', 'uses' => 'Member\BlockchainWithdrawalAddressExportController@index']);
            Route::get('withdrawal-addresses/list/{creditTypeId}', 'Member\BlockchainWithdrawalAddressController@getWithdrawalAddresses');
            Route::resource('withdrawal-addresses', 'Member\BlockchainWithdrawalAddressController', ['as' => 'member.blockchain'])->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);
        });
    });
});
