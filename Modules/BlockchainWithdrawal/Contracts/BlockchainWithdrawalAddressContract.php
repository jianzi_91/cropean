<?php

namespace Modules\BlockchainWithdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface BlockchainWithdrawalAddressContract extends CrudContract
{
    /**
     * Check if record exists.
     *
     * @param int $userId
     * @param int $bankCreditTypeId
     * @param string $address
     * @param int $ignoreId
     * @return mixed
     */
    public function recordExists(int $userId, int $bankCreditTypeId, string $address, int $ignoreId = 0);
}
