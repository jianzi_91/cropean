<?php

namespace Modules\BlockchainWithdrawal\Contracts;

use Modules\Withdrawal\Contracts\WithdrawalContract;

interface BlockchainWithdrawalContract extends WithdrawalContract
{
    /**
     * Get total pending and approved withdrawal today.
     *
     * @param int $userId
     * @return mixed
     */
    public function getTotalWithdrawalToday(int $userId, string $creditTypeSlug);

    /**
     * Withdraw bank account credits
     *
     * @param $userId
     * @param unknown $amount
     * @param unknown $adminFee
     * @param unknown $referenceNumber
     * @param unknown $sourceCreditTypeId
     * @param unknown $destinationCreditTypeId
     * @param unknown $recipientName
     * @param unknown $recipientAddress
     * @param unknown $withdrawalTypeId
     * @return string
     */
    public function addBlockchainWithdrawal($userId, $amount, $adminFee, $referenceNumber, $sourceCreditTypeId, $destinationCreditTypeId, $recipientName, $recipientAddress, $withdrawalTypeId);

    /**
     * Get total master wallet withdrawal today.
     *
     * @param string $creditTypeSlug
     * @return mixed
     */
    public function getTotalMasterWalletWithdrawalToday(string $creditTypeSlug);

    /**
     * Get unprocessed withdrawals.
     *
     * @return mixed
     */
    public function getUnprocessedWithdrawals();

    /**
     * Update confirmations.
     *
     * @param int $withdrawalId
     * @param int $confirmations
     * @param int $minConfirmations
     * @return mixed
     */
    public function updateConfirmations(int $withdrawalId, int $confirmations, int $minConfirmations);
}
