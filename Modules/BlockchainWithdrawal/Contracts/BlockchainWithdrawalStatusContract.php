<?php

namespace Modules\BlockchainWithdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BlockchainWithdrawalStatusContract extends CrudContract, SlugContract, ActiveContract, HistoryableContract
{
    const PENDING    = 'pending';
    const APPROVED   = 'approved';
    const REJECTED   = 'rejected';
    const CANCELLED  = 'cancelled';
    const PROCESSING = 'processing';
}
