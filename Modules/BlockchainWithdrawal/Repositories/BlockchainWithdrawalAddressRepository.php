<?php

namespace Modules\BlockchainWithdrawal\Repositories;

use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalAddressContract;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalAddress;
use Modules\Credit\Traits\ValidateCredit;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class BlockchainWithdrawalAddressRepository extends Repository implements BlockchainWithdrawalAddressContract
{
    use HasCrud { add as protected parentAdd; }
    use ValidateCredit;

    /**
     * Class constructor.
     *
     * @param BlockchainWithdrawalAddress $model
     */
    public function __construct(BlockchainWithdrawalAddress $model)
    {
        $this->model = $model;
    }

    public function add(array $attributes = [], $forceFill = false)
    {
        if (array_key_exists('bank_credit_type_id', $attributes)) {
            $attributes['bank_credit_type_id'] = $this->validateCreditType($attributes['bank_credit_type_id']);
        }

        return $this->parentAdd($attributes, $forceFill);
    }

    /**
     * @inheritDoc
     * @see \Modules\Withdrawal\Contracts\WithdrawalAddressContract::recordExists()
     */
    public function recordExists(int $userId, int $bankCreditTypeId, string $address, int $ignoreId = 0)
    {
        $count = $this->model
            ->where('user_id', $userId)
            ->where('bank_credit_type_id', $bankCreditTypeId)
            ->where('address', $address)
            ->when(!empty($ignoreId), function ($query) use ($ignoreId) {
                $query->where('id', '<>', $ignoreId);
            })
            ->count();

        return 0 < $count;
    }
}
