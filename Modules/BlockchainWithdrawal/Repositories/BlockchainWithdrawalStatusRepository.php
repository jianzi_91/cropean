<?php

namespace Modules\BlockchainWithdrawal\Repositories;

use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalStatus;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalStatusHistory;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BlockchainWithdrawalStatusRepository extends Repository implements BlockchainWithdrawalStatusContract
{
    use HasCrud, HasActive, HasSlug, HasHistory;

    protected $model;
    protected $historyModel;

    public function __construct(BlockchainWithdrawalStatus $model, BlockchainWithdrawalStatusHistory $historyModel)
    {
        $this->model        = $model;
        $this->historyModel = $historyModel;

        parent::__construct($model);
    }
}
