<?php

namespace Modules\BlockchainWithdrawal\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Jobs\TransferOutJob;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalAddressContract;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawal;
use Modules\BlockchainWithdrawal\Models\BlockchainWithdrawalTransaction;
use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankAccountStatementContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Traits\ValidateCredit;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Models\WithdrawalTransaction;
use Modules\Withdrawal\Repositories\WithdrawalRepository;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;

class BlockchainWithdrawalRepository extends WithdrawalRepository implements BlockchainWithdrawalContract
{
    use ValidateCredit, HasCrud, HasSlug;

    protected $model;

    protected $withdrawalTrasactionModel;

    protected $bankAccountRepository;

    protected $bankAccountCreditRepository;

    protected $bankAccountStatementRepository;

    protected $bankCreditTypeRepository;

    protected $withdrawalAddressRepository;

    protected $withdrawalTypeRepository;

    protected $blockchainWalletRepository;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawal $model
     * @param BlockchainWithdrawalAddressContract
     */
    public function __construct(
        BlockchainWithdrawal $model,
        BlockchainWithdrawalTransaction $withdrawalTransactionModel,
        BankAccountContract $bankAccountRepository,
        BankAccountCreditContract $bankAccountCreditRepository,
        BankAccountStatementContract $bankAccountStatementRepository,
        BlockchainWithdrawalAddressContract $withdrawalAddressRepository,
        BankCreditTypeContract $bankCreditTypeRepository,
        WithdrawalTypeContract $withdrawalTypeRepository,
        BlockchainWalletContract $blockchainWalletRepository
    ) {
        $this->model                          = $model;
        $this->withdrawalTransactionModel     = $withdrawalTransactionModel;
        $this->withdrawalAddressRepository    = $withdrawalAddressRepository;
        $this->bankCreditTypeRepository       = $bankCreditTypeRepository;
        $this->withdrawalTypeRepository       = $withdrawalTypeRepository;
        $this->bankAccountRepository          = $bankAccountRepository;
        $this->bankAccountCreditRepository    = $bankAccountCreditRepository;
        $this->bankAccountStatementRepository = $bankAccountStatementRepository;
        $this->blockchainWalletRepository     = $blockchainWalletRepository;
    }

    /**
     *
     * {@inheritDoc}
     * @throws \Exception
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::request()
     */
    public function request($userId, $withdrawalAddressId, $amount, $creditType, $currencyId = null, $remarks = null)
    {
        // Generate request id
        $config          = config('blockchainwithdrawal.transaction_code');
        $referenceNumber = (new $config['generator'])->generate($config['prefix'], $config['length']);

        $withdrawalType          = $this->withdrawalTypeRepository->findBySlug(WithdrawalTypeContract::BLOCKCHAIN);
        $withdrawalFeePercentage = !empty(setting('withdrawal_' . $creditType . '_admin_fee_percentage')) ? setting('withdrawal_' . $creditType . '_admin_fee_percentage') : $withdrawalType->fee_percentage;
        $withdrawalFeeAmount     = $this->calculateAdminFee($amount, $withdrawalFeePercentage, credit_precision($creditType));
        $finalWithdrawalAmount   = $this->calculateAmount($amount, $withdrawalFeeAmount, credit_precision($creditType));
        $withdrawalAddress       = $this->withdrawalAddressRepository->find($withdrawalAddressId);
        $bankCreditType          = $this->bankCreditTypeRepository->findBySlug($creditType);

        $blockchainWithdrawal = $this->addBlockchainWithdrawal(
            $userId,
            $finalWithdrawalAmount,
            $withdrawalFeeAmount,
            $referenceNumber,
            $bankCreditType->id,
            $withdrawalAddress->bank_credit_type_id,
            $withdrawalAddress->name,
            $withdrawalAddress->address,
            $withdrawalType->id
        );

        if ($blockchainWithdrawal) {
            // we hardcode this to USDT because that is where our credits are
            $creditTypeObject = $this->bankCreditTypeRepository->findBySlug($creditType);
            $deductCreditType = bank_credit_type_name($creditTypeObject->id);

            $withdrawalTxnId = $this->validateTransactionType(BankTransactionTypeContract::BLOCKCHAIN_WITHDRAWAL);
            $creditTypeId    = $this->validateCreditType($deductCreditType);
            $withdrawTxnCode = $this->deductCredit($blockchainWithdrawal, $deductCreditType, $withdrawalTxnId, $remarks);

            $this->addWithdrawalTransaction($blockchainWithdrawal, $creditTypeId, $withdrawTxnCode, $withdrawalTxnId, $withdrawalType->id);

            if ($withdrawalFeeAmount > 0) {
                $this->addFee($blockchainWithdrawal, $withdrawalFeeAmount, $deductCreditType);
            }
        }

        return $blockchainWithdrawal;
    }

    public function approve($withdrawalId)
    {
        $blockchainWithdrawal = $this->find($withdrawalId);

        // This try-catch is neccessary to handle the synchronous job errors
        // When transfer out is error, everything will be reverted
        // Check if recipient is in system first
        $existingAddress = $this->blockchainWalletRepository->findBySlug($blockchainWithdrawal->recipient_address);

        if ($existingAddress) {
            $fromAccount = $this->bankAccountRepository->findByReferenceId($blockchainWithdrawal->user_id);
            $toAccount   = $this->bankAccountRepository->findByReferenceId($existingAddress->user_id);

            $toTxnCode   = $this->bankAccountCreditRepository->add($blockchainWithdrawal->destination_credit_type_id, $blockchainWithdrawal->amount, $toAccount, BankTransactionTypeContract::CREDIT_TRANSFER_FROM, null, auth()->user()->id, null, $fromAccount);
            $toStatement = $this->bankAccountStatementRepository->findBySlug($toTxnCode);
            $this->bankAccountStatementRepository->edit($toStatement->id, ['transfer_account_id' => $fromAccount->id]);
            $blockchainWithdrawal->recipient_user_id = $existingAddress->user_id;
            $blockchainWithdrawal->save();
        } else {
            try {
                TransferOutJob::dispatch($blockchainWithdrawal->id);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                throw $e;
                return;
            }
        }

        return $blockchainWithdrawal;
    }

    /**
     * @param \Modules\Withdrawal\Contracts\unknown $withdrawalId
     * @param null $remarks
     * @param string $transactionType
     * @return bool|void
     * @throws \Modules\Credit\Exceptions\InvalidTransactionType
     */
    public function refund($withdrawalId, $remarks = null, $transactionType = 'blockchain_withdrawal_rejected')
    {
        $withdrawal = $this->model->find($withdrawalId);

        $refundTxnTypeId    = $this->validateTransactionType($transactionType);
        $refundCreditTypeId = $withdrawal->source_credit_type_id;
        $refundAmount       = bcadd($withdrawal->amount, $withdrawal->admin_fee, 4);
        $refundUserId       = $withdrawal->user_id;

        $refundTxnCode   = $this->bankAccountCreditRepository->add($refundCreditTypeId, $refundAmount, $refundUserId, $refundTxnTypeId, $remarks);

        if (!$refundTxnCode) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        $transaction                           = $this->withdrawalTransactionModel->newInstance();
        $transaction->amount                   = $refundAmount;
        $transaction->transaction_code         = $refundTxnCode;
        $transaction->blockchain_withdrawal_id = $withdrawal->id;
        $transaction->bank_credit_type_id      = $refundCreditTypeId;
        $transaction->bank_transaction_type_id = $refundTxnTypeId;
        if (!$transaction->save()) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        return $withdrawal;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::calculateWithdrawalAdminFee()
     */
    public function calculateWithdrawalAdminFee($amount, $creditTypeSlug)
    {
        $percentage = setting("withdrawal_{$creditTypeSlug}_admin_fee_percentage");
        $flatFee    = setting("withdrawal_{$creditTypeSlug}_admin_fee_flat");

        $percentageFee = bcmul($percentage, $amount, credit_precision($creditTypeSlug));

        return (1 == bccomp($flatFee, $percentageFee, credit_precision($creditTypeSlug))) ? $flatFee : $percentageFee;
    }

    public function getTotalWithdrawalToday(int $userId, string $creditTypeSlug)
    {
        $creditTypeId = bank_credit_type_id($creditTypeSlug);

        $sum = $this->model
            ->where('user_id', $userId)
            ->whereBetween('created_at', [now()->startOfDay()->toDateTimeString(), now()->endOfDay()->toDateTimeString()])
            ->where('source_credit_type_id', $creditTypeId)
            ->first(DB::raw('SUM(amount + admin_fee) AS total_withdrawal'));

        return $sum->total_withdrawal ?: 0;
    }

    public function getTotalMasterWalletWithdrawalToday(string $creditTypeSlug)
    {
        $creditTypeId = bank_credit_type_id($creditTypeSlug);

        $sum = $this->model
            ->whereBetween('created_at', [now()->startOfDay()->toDateTimeString(), now()->endOfDay()->toDateTimeString()])
            ->where('source_credit_type_id', $creditTypeId)
            ->where('is_master_wallet', 1)
            ->first(DB::raw('SUM(amount + admin_fee) AS total_withdrawal'));

        return $sum->total_withdrawal ?: 0;
    }

    /**
     * add withdrawal record
     * @param int $userId
     * @param float $amount
     * @param $adminFee
     * @param string $referenceNumber
     * @param $sourceCreditTypeId
     * @param $destinationCreditTypeId
     * @param $recipientName
     * @param $recipientAddress
     * @param $withdrawalTypeId
     * @return BlockchainWithdrawal
     */
    public function addBlockchainWithdrawal($userId, $amount, $adminFee, $referenceNumber, $sourceCreditTypeId, $destinationCreditTypeId, $recipientName, $recipientAddress, $withdrawalTypeId)
    {
        $withdrawal                             = new $this->model;
        $withdrawal->user_id                    = $userId;
        $withdrawal->amount                     = $amount;
        $withdrawal->reference_number           = $referenceNumber;
        $withdrawal->admin_fee                  = $adminFee;
        $withdrawal->recipient_name             = $recipientName;
        $withdrawal->recipient_address          = $recipientAddress;
        $withdrawal->source_credit_type_id      = $sourceCreditTypeId;
        $withdrawal->destination_credit_type_id = $destinationCreditTypeId;
        $withdrawal->refund_credit_type_id      = $destinationCreditTypeId;
        $withdrawal->withdrawal_type_id         = $withdrawalTypeId;

        return $withdrawal->save() ? $withdrawal : null;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::addFee()
     */
    public function addFee($withdrawal, $amount, $creditType, $remarks = null, $transactionType = 'blockchain_withdrawal_fee')
    {
        $feeTxnId     = $this->validateTransactionType($transactionType);
        $creditTypeId = $this->validateCreditType($creditType);
        $feeTxnCode   = $this->bankAccountCreditRepository->deduct($creditType, $amount, $withdrawal->user_id, $feeTxnId, $remarks);
        if (!$feeTxnCode) {
            throw new \Exception('Can\'t deduct withdrawal fee credits');
        }

        $transactionFee                           = new $this->withdrawalTransactionModel;
        $transactionFee->amount                   = $amount;
        $transactionFee->transaction_code         = $feeTxnCode;
        $transactionFee->bank_transaction_type_id = $feeTxnId;
        $transactionFee->blockchain_withdrawal_id = $withdrawal->id;
        $transactionFee->bank_credit_type_id      = $creditTypeId;

        return $transactionFee->save();
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract::getUnprocessedDeposits()
     */
    public function getUnprocessedWithdrawals()
    {
        return BlockchainWithdrawal::where('confirmations', 0)
            ->whereNull('recipient_user_id')
            ->whereNotNull('transaction_hash')
            ->where('is_confirmed', false)
            ->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract::updateConfirmations()
     */
    public function updateConfirmations(int $withdrawalId, int $confirmations, int $minConfirmations)
    {
        $updateData = [
            'confirmations' => $confirmations
        ];

        if ($minConfirmations <= $confirmations) {
            $updateData['is_confirmed'] = true;
        }

        BlockchainWithdrawal::where('id', $withdrawalId)
            ->update($updateData);
    }

    /**
     * Add blockchain withdrawal transaction record
     *
     * @param BlockchainWithdrawal $withdrawal
     * @param int $creditTypeId
     * @param string $withdrawTxnCode
     * @param string $withdrawalTxnId
     * @return WithdrawalTransaction
     */
    protected function addWithdrawalTransaction($withdrawal, $creditTypeId, $withdrawTxnCode, $withdrawalTxnId)
    {
        $transaction                           = new $this->withdrawalTransactionModel;
        $transaction->amount                   = $withdrawal->amount;
        $transaction->transaction_code         = $withdrawTxnCode;
        $transaction->blockchain_withdrawal_id = $withdrawal->id;
        $transaction->bank_credit_type_id      = $creditTypeId;
        $transaction->bank_transaction_type_id = $withdrawalTxnId;

        if (!$transaction->save()) {
            throw new \Exception('Failed saving withdrawal transaction');
        }

        return $transaction;
    }

    /**
     * Deduct withdrawal amount from user credit
     *
     * @param  BlockchainWithdrawal $withdrawal
     * @param  int $creditType
     * @param  string $withdrawalTxnId
     * @param  string $remarks
     * @return string
     */
    protected function deductCredit($withdrawal, $creditType, $withdrawalTxnId, $remarks)
    {
        $withdrawTxnCode = $this->bankAccountCreditRepository->deduct($creditType, $withdrawal->amount, $withdrawal->user_id, $withdrawalTxnId, $remarks);

        if (!$withdrawTxnCode) {
            throw new \Exception('Failed deducting user credits');
        }

        return $withdrawTxnCode;
    }
}
