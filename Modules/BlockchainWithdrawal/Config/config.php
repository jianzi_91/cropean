<?php

return [
    'bindings' => [
        Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract::class        => Modules\BlockchainWithdrawal\Repositories\BlockchainWithdrawalRepository::class,
        Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract::class  => Modules\BlockchainWithdrawal\Repositories\BlockchainWithdrawalStatusRepository::class,
        Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalAddressContract::class => Modules\BlockchainWithdrawal\Repositories\BlockchainWithdrawalAddressRepository::class,
    ],

    /*
     |--------------------------------------------------------------------------
     | Blockchain Withdrawal Code Generator
     |--------------------------------------------------------------------------
     |
     | Here you can configure the setting when generating withdrawal transaction code
     |
     */
    'transaction_code' => [
        'generator' => 'Modules\Withdrawal\SimpleGenerator',
        'length'    => 18,
        'prefix'    => 'BCW', // BlockChain Withdrawal
    ],
];
