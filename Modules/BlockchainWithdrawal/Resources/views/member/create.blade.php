@extends('templates.member.master')
@section('title', __('m_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.external wallet transfer'), 'route' => route('member.blockchain.withdrawals.index')],
        ['name'=>__('s_breadcrumb.new external transfer')]
    ],
    'header'=>__('m_wallet_withdrawals_create.new external transfer'),
    'backRoute'=> route('member.blockchain.withdrawals.index'),
])

<div class="row">
    <div class="col-lg-8 col-md-7 col-xs-12 order-2 order-md-1">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-9 col-xs-12">
                        {{ Form::open(['method'=>'post', 'route'=>'member.blockchain.withdrawals.store', 'id'=>'create-withdrawal', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                        {{ Form::formSelect('credit_type', withdrawable_blockchain_credit_types_dropwdown(), old('credit_type'), __('m_wallet_withdrawals_create.credit type')) }}
                        
                        {{ Form::formSelect('user_withdrawal_address_id', [], old('user_withdrawal_address_id'), __('m_wallet_withdrawals_create.withdrawal wallet address'), ['required'=>true]) }}

                        {{ Form::formNumber('amount', old('amount'), __('m_wallet_withdrawals_create.transfer amount'), ['required'=>true]) }}

                        {{ Form::formNumber('admin_fee', old('admin_fee') ?? 0, __('m_wallet_withdrawals_create.admin fee'), ['readonly']) }}
                        
                        {{ Form::formNumber('total_amount', old('total_amount') ?? 0, __('m_wallet_withdrawals_create.total amount'), ['readonly']) }}
                        
                        {{ Form::formPassword('secondary_password', '', __('m_wallet_withdrawals_create.secondary password'), ['required'=>true]) }}

                        <div class="row d-flex justify-content-end pr-1">
                            <button type="submit" name="btn_submit" class="btn btn-primary btn-cro-primary">{{ __('m_wallet_withdrawals_create.submit') }}</button>
                        </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-5 col-xs-12 order-1 order-md-2">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_wallet_withdrawals_create.usdt erc20 credit balance'),
            'value'=>amount_format(auth()->user()->usdt_erc20_credit, credit_precision('usdt_erc20')),
            'backgroundClass'=>'widget-usdt-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_wallet_withdrawals_create.usdc credit balance'),
            'value'=>amount_format(auth()->user()->usdc_credit, credit_precision('usdc')),
            'backgroundClass'=>'widget-usdc-image',
        ])
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#create-withdrawal', {
        amount: {
            presence: { message: __.validation.field_required },
            numericality: {
                greaterThanOrEqualTo: parseFloat('{{ amount_format($withdrawalType->minimum_amount, 2) }}'),
                message: '{{ __("m_wallet_withdrawals_create.amount must be greater or equal to the minimum value") }}'
            }
        }
    })

    function calculate(creditType, amount) {
        axios.post('/blockchain/withdrawals/calculate', {
            creditType: creditType,
            amount: amount,
        })
        .then(function (response) {
            $('#admin_fee').val(response.data.admin_fee);
            $('#total_amount').val(response.data.total_amount);
        });
    }

    $('#credit_type').change(function() {
        let creditType = $(this).val();
        $('#user_withdrawal_address_id').empty();

        axios.get("/blockchain/withdrawal-addresses/list/" + creditType)
            .then(function (response) {
                $.each(response.data, function(key, value) {
                    var option = new Option(value, key);
                    $(option).html(value);
                    $('#user_withdrawal_address_id').append(option);
                });
            });

        if ($('#amount').val() != "") {
            calculate(creditType, $('#amount').val());
        } 
    });

    $('#amount').on("keyup", function() {
        let creditType = $('#credit_type').val();
        let amount = $(this).val();

        calculate(creditType, amount);
    });
})
</script>
@endpush