@extends('templates.member.master')
@section('title', __('m_page_title.my wallet addresses'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
    ['name'=>__('s_breadcrumb.my wallet addresses'), 'route' => route('member.blockchain.withdrawal-addresses.index')],
    ['name'=>__('s_breadcrumb.edit wallet address')]
]])

<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'withdrawal-addresses'])
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-7 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-lg-9">
                            <h4 class="card-title p-0">{{ __('m_edit_wallet_address.edit wallet address') }}</h4>

                            {{ Form::open(['method'=>'put', 'route'=> ['member.blockchain.withdrawal-addresses.update', 'withdrawal_address' => $address->id ], 'id'=>'update-withdrawal-address', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                            {{ Form::formHide('bank_credit_type_slug', $credit_type) }}
                            {{ Form::formText('bank_credit_type_slug_display', blockchain_credit_types_dropdown()[bank_credit_type_name($address->bank_credit_type_id)],__('m_create_withdrawal_address.credit_type'), ['disabled' => 'disabled']) }}

                            {{ Form::formText('name', $address->name, __('m_edit_wallet_address.name'), ['required'=>true]) }}

                            {{ Form::formText('address', $address->address, __('m_edit_wallet_address.address')) }}

                            {{ Form::formPassword('secondary_password', '', __('m_edit_wallet_address.secondary_password'), ['required'=>true]) }}
                            
                            <div class="d-flex justify-content-end">
                                <button type="submit" name="btn_submit" class="btn btn-primary">{{ __('m_edit_wallet_address.proceed') }}</button>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#update-withdrawal-address', {
        name: { presence: { message: __.validation.field_required } },
        address: { presence: { message: __.validation.field_required } },
        secondary_password: { presence: { message: __.validation.field_required } }
    })
})
</script>
@endpush