@extends('templates.member.master')
@section('title', __('m_page_title.my wallet addresses'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
    ['name'=>__('s_breadcrumb.my wallet addresses')]
]])

<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'withdrawal-addresses'])
        </div>
    </div>

    @can('member_blockchain_withdrawal_addresses_create')
    <div class="row">
        <div class="col">
            <a class="btn btn-primary" href="{{ route('member.blockchain.withdrawal-addresses.create') }}">{{ __('m_wallet_withdrawals_addresses.add wallet') }}</a>
        </div>
    </div>
    <br />
    @endcan

    @component('templates.__fragments.components.filter')
        <div class="col-12 col-sm-6 col-md-4 col-xl-3">
            {{ Form::formSelect('credit_type',  blockchain_credit_types_dropdown(), request('credit_type'), __('m_wallet_withdrawals_addresses.currency type'), [], false) }}
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-xl-3">
            {{ Form::formText('address', request('address'),__('m_wallet_withdrawals_addresses.address'), [], false) }}
        </div>
    @endcomponent

    <div class="card">
        <div class="card-content">
            <div class="card-body d-flex justify-content-between align-items-center p-2">
                <h4 class="card-title filter-title p-0 m-0">{{__('m_wallet_withdrawals_addresses.my wallet addresses')}}</h4>
                @can('member_blockchain_withdrawal_addresses_export')
                    <a class="btn btn-secondary" href="{{ route('member.blockchain.withdrawal-addresses.export', array_merge(request()->except('page'))) }}">{{ __('a_external_transfers.export table') }}</a>
                @endcan
            </div>
            @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('m_wallet_withdrawals_addresses.wallet name') }}</th>
                    <th>{{ __('m_wallet_withdrawals_addresses.wallet type') }}</th>
                    <th>{{ __('m_wallet_withdrawals_addresses.wallet address') }}</th>
                    <th>{{ __('m_wallet_withdrawals_addresses.action') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($addresses as $address)
                <tr>
                    <td>{{$address->name}}</td>
                    <td>{{$address->credit_type_name}}</td>
                    <td>{{$address->address}}</td>
                    <td class="action">
                        <div class="d-flex flex-row">
                            @can('member_blockchain_withdrawal_addresses_edit')
                            <a href="{{ route('member.blockchain.withdrawal-addresses.edit', $address->id) }}" title="{{ __('m_wallet_withdrawals_addresses.edit') }}">
                                <i class="bx bx-pencil"></i>
                            </a>
                            @endcan
                            @can('member_blockchain_withdrawal_addresses_delete')
                                {{ Form::open(['method'=>'post', 'route'=>['member.blockchain.withdrawal-addresses.destroy', $address->id], 'id'=>'form']) }}
                                {{ csrf_field() }}
                                @method('DELETE')
                                <a href="#" class="js-delete" title="{{ __('m_wallet_withdrawals_addresses.delete') }}">
                                    <i class="bx bx-trash"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </div>
                    </td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 4,
                'text' => __('m_wallet_withdrawals_addresses.no records') ])
                @endforelse
            </tbody>
            @endcomponent
        </div>
    </div>
{!! $addresses->render() !!}
</section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('.js-delete').on('click', function(e) {
        e.preventDefault();
        var el = this;

        swal.fire({
            title: "{{ ucfirst(__('m_wallet_withdrawals_addresses.are you sure you want to delete this wallet address?')) }}",
            showCancelButton: true,
            cancelButtonText: "{{ __('m_wallet_withdrawals_addresses.no') }}",
            confirmButtonText: "{{ __('m_wallet_withdrawals_addresses.yes') }}",
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-secondary'
            },
            preConfirm: function (res) {
                $(el).closest('form').submit();
            }
        })
    })
})

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endpush