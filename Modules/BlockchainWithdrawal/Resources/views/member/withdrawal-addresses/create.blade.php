@extends('templates.member.master')
@section('title', __('m_page_title.my wallet addresses'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
    ['name'=>__('s_breadcrumb.my wallet addresses'), 'route' => route('member.blockchain.withdrawal-addresses.index')],
    ['name'=>__('s_breadcrumb.new wallet')]
]])

<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'withdrawal-addresses'])
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-7 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xs-12 col-lg-9">
                            <h4 class="card-title p-0">{{ __('m_create_withdrawal_address.new wallet address') }}</h4>

                            {{ Form::open(['method'=>'post', 'route'=>'member.blockchain.withdrawal-addresses.store', 'id'=>'create-withdrawal-address', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                            {{ Form::formSelect('bank_credit_type_slug', blockchain_credit_types_dropdown(), old('bank_credit_type_slug'),__('m_create_withdrawal_address.credit_type')) }}

                            {{ Form::formText('name', old('name'), __('m_create_withdrawal_address.wallet name'),['required'=>true]) }}

                            {{ Form::formText('address', old('address'), __('m_create_withdrawal_address.wallet address') ) }}

                            {{ Form::formPassword('secondary_password', '', __('m_create_withdrawal_address.secondary password'),['required'=>true]) }}

                            <div class="d-flex justify-content-end">
                                <button type="submit" name="btn_submit" class="btn btn-primary">{{ __('m_create_withdrawal_address.proceed') }}</button>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#create-withdrawal-address', {
        name: { presence: { message: __.validation.field_required } },
        address: { presence: { message: __.validation.field_required } },
        secondary_password: { presence: { message: __.validation.field_required } }
    })
})
</script>
@endpush