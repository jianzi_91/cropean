@extends('templates.member.master')
@section('title', __('m_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.external wallet transfer')]
    ],
    'header'=>__('m_external_transfer.external wallet transfer')
])

@can('member_blockchain_withdrawal_create')
<div class="row">
    <div class="col">
        <a class="btn btn-primary" href="{{ route('member.blockchain.withdrawals.create') }}">{{ __('m_wallet_withdrawals.new external transfer') }}</a>
    </div>
</div>
<br />
@endcan

@component('templates.__fragments.components.filter')        
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('date', request('date'),__('m_external_transfer.date'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('transaction_hash', request('transaction_hash'),__('m_external_transfer.transaction hash'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('credit_type',  blockchain_credit_types_dropdown(), request('credit_type'), __('m_external_transfer.currency type'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('recipient_address', request('recipient_address'),__('m_external_transfer.recipient address'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('status',  blockchain_withdrawal_statuses_dropdown(), request('status'), __('m_external_transfer.status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{__('m_external_transfer.external wallet transfer records')}}</h4>
                @can('member_blockchain_withdrawal_export')
                    {{ Form::formTabSecondary(__('m_external_transfer.export excel'), route('member.blockchain.withdrawals.export', array_merge(request()->except('page')))) }}
                @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_external_transfer.date') }}</th>
                <th>{{ __('m_external_transfer.currency type') }}</th>
                <th>{{ __('m_external_transfer.recipient address') }}</th>
                <th>{{ __('m_external_transfer.transaction hash') }}</th>
                <th>{{ __('m_external_transfer.amount') }}</th>
                <th>{{ __('m_external_transfer.status') }}</th>
                @can('member_blockchain_withdrawal_cancel')
                <th>{{ __('m_external_transfer.action') }}</th>
                @endcan
            </tr>
        </thead>
        <tbody>
            @forelse ($withdrawals as $withdrawal)
            <tr>
                <td>{{ $withdrawal->created_at }}</td>
                <td>{{ __('s_bank_credit_types.'. $withdrawal->source_credit_type_name) }}</td>
                <td>{{ $withdrawal->recipient_address }}</td>
                <td>{{ $withdrawal->transaction_hash ?: '-' }}</td>
                <td>{{ amount_format($withdrawal->amount, 2) }}</td>
                <td>{{ $withdrawal->status_name }}</td>
                @can('member_blockchain_withdrawal_cancel')
                <td class="action">
                    @if ($withdrawal->status->status->rawname == 'pending')
                    <div>
                        <input type="hidden" value={{$withdrawal->id}} name="withdrawal_id" id="withdrawal_id" />
                        <form action="{{ route('member.blockchain.withdrawals.destroy', $withdrawal->id) }}" method="POST">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <a role="button" class="js-delete" href="#">
                                <i class="bx bx-trash"></i>
                            </a>
                        </form>
                    </div>
                    @endif
                </td>
                @endcan
            </tr>
            @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 7,
                'text' => __('m_wallet_withdrawals.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $withdrawals->render() !!}
@endsection

@push('scripts')
<script>
    $(function() {
        $('#btn_create').on('click', function(e) {
            e.preventDefault();
            window.location.href = "{{ route('member.blockchain.withdrawals.index') }}"
        })
    });

    $('.js-delete').on('click',function(e){
        e.preventDefault();
        var el = this;

        swal.fire({
            title: '{{ ucfirst(__("m_external_transfer.do you want to cancel this transfer?")) }}',
            showCancelButton: true,
            cancelButtonText: '{{ __("m_external_transfer.no") }}',
            confirmButtonText: '{{ __("m_external_transfer.yes") }}',
            customClass: {
                confirmButton: 'btn btn-primary cro-btn-primary',
                cancelButton: 'btn btn-secondary cro-btn-secondary',
            },
            preConfirm: function(e){
                $(el).closest('form').submit();
            }
        })
    })
</script>
@endpush