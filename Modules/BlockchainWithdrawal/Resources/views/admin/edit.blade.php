@extends('templates.admin.master')
@section('title', __('a_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.external wallet transfer'), 'route' => route('admin.blockchain.withdrawals.index')],
        ['name'=>__('s_breadcrumb.new external transfer')]
    ],
    'header'=>__('a_external_transfer.edit external transfer'),
    'backRoute'=> route('admin.blockchain.withdrawals.index'),
])
@if($errors->any())
{{ implode('', $errors->all('<div>:message</div>')) }}
@endif
<div class="row">
    <div class="col-lg-8 col-md-7 col-xs-12 order-2 order-md-1">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title">{{ __('a_external_transfer.status') }}</h4>
                    <div class="card-title kyc_status">{{ $withdrawal->status->status->name_translation }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-9 col-xs-12">
                        {{ Form::open(['method'=>'put', 'route'=> ['admin.blockchain.withdrawals.update', 'withdrawal' => $withdrawal->id], 'id'=>'edit-withdrawal', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                        {{ Form::formText('created_at', $withdrawal->created_at, __('a_external_transfer.requested date'), ['disabled']) }}

                        {{ Form::formText('name', $withdrawal->user->name, __('a_external_transfer.name'), ['disabled']) }}

                        {{ Form::formText('member_id', $withdrawal->user->member_id, __('a_external_transfer.member_id'), ['disabled']) }}

                        {{ Form::formText('email', $withdrawal->user->email, __('a_external_transfer.email'), ['disabled']) }}

                        {{ Form::formSelect('credit_type', withdrawable_blockchain_credit_types_dropwdown(), $withdrawal->sourceCurrency->rawname, __('a_external_transfer.credit type'), ['disabled' => 'disabled']) }}
                        
                        {{ Form::formText('recipient_address', $withdrawal->recipient_address, __('a_external_transfer.recipient address'), ['readonly']) }}

                        {{ Form::formNumber('amount', amount_format($withdrawal->amount, 2), __('a_external_transfer.transfer amount'), ['disabled']) }}

                        {{ Form::formNumber('admin_fee', amount_format($withdrawal->admin_fee, 2), __('a_external_transfer.admin fee'), ['readonly']) }}

                        @if ($withdrawal->is_approvable && $withdrawal->is_rejectable )
                        {{ Form::formSelect('status', blockchain_withdrawal_statuses_dropdown(['cancelled']), $withdrawal->status->status->id, __('a_external_transfer.status')) }}
                        
                        {{ Form::formTextArea('remarks', old('remarks'), __('a_external_transfer.remarks')) }}

                        <div class="row d-flex justify-content-end pr-1">
                            <button type="submit" name="btn_submit" class="btn btn-primary btn-cro-primary">{{ __('a_external_transfer.submit') }}</button>
                        </div>
                        @endif
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
@endpush