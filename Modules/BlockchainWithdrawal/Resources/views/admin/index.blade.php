@extends('templates.admin.master')
@section('title', __('a_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.external transfers')]
    ],
    'header'=>__('a_external_transfers.external transfers')
])

@component('templates.__fragments.components.filter')        
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('date', request('date'),__('a_external_transfers.date'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('credit_type', blockchain_credit_types_dropdown(), request('credit_type'), __('a_external_transfers.currency type'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('name', request('name'),__('a_external_transfers.full name'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('email', request('email'),__('a_external_transfers.email'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('member_id', request('member_id'),__('a_external_transfers.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_conversion_list.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('transaction_hash', request('transaction_hash'),__('a_external_transfers.transaction hash'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('recipient_address', request('recipient_address'),__('a_external_transfers.receiver_wallet_address'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('status',  blockchain_withdrawal_statuses_dropdown(), request('status'), __('a_external_transfers.status'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('updated_at', request('updated_at'),__('a_external_transfers.updated at'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_external_transfers.external transfers')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_external_transfers.total results:') }} {{ $withdrawals->total() }}</div>
                @if (in_array(request()->status, [1,2]))
                    {{ Form::formButtonPrimary('btn-bulk-update', __('a_withdrawal_list.update'), 'button', []) }}
                @endif
                &ensp;&ensp;&ensp;
                @can('admin_blockchain_withdrawal_export')
                    <a class="btn btn-secondary" href="{{ route('admin.blockchain.withdrawals.export', array_merge(request()->except('page'))) }}">{{ __('a_external_transfers.export table') }}</a>
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                @if (in_array(request()->status, [1,2]))
                    <td class="text-center">{{ Form::formCheckbox(null, 'select-all') }}</td>
                @endif
                @can('admin_blockchain_withdrawal_edit')
                <th>{{ __('a_external_transfers.action') }}</th>
                @endcan
                <th>{{ __('a_external_transfers.date') }}</th>
                <th>{{ __('a_external_transfers.full name') }}</th>
                <th>{{ __('a_external_transfers.member id') }}</th>
                <th>{{ __('a_external_transfers.email address') }}</th>
                <th>{{ __('a_external_transfers.currency type') }}</th>
                <th>{{ __('a_external_transfers.transferred amount') }}</th>
                <th>{{ __('a_external_transfers.status') }}</th>
                <th>{{ __('a_external_transfers.updated at') }}</th>
                <th>{{ __('a_external_transfers.recipient address') }}</th>
                <th>{{ __('a_external_transfers.transaction hash') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($withdrawals as $withdrawal)
            <tr>
                @if (in_array(request()->status, [1,2]))
                    <td class="text-center">{{ Form::formCheckbox('cbx-withdrawal', 'cbx-withdrawal-' . $withdrawal->id, null, null, ['class' => 'cbx-withdrawal', 'data-id' => $withdrawal->id]) }}</td>
                @endif
                @can('admin_blockchain_withdrawal_edit')
                <td class="action text-center">
                    @if (!$withdrawal->is_cancelled)
                        <a href="{{ route('admin.blockchain.withdrawals.edit', $withdrawal->id) }}">
                            @if($withdrawal->blockchain_withdrawal_status_id == 1 || $withdrawal->blockchain_withdrawal_status_id == 2)
                                <i class="bx bx-pencil"></i>
                            @else
                                <i class="bx bx-show-alt"></i>
                            @endif
                        </a>
                    @endif
                </td>
                @endcan
                <td>{{ $withdrawal->created_at }}</td>
                <td>{{ $withdrawal->name }}</td>
                <td>{{ $withdrawal->member_id }}</td>
                <td>{{ $withdrawal->email }}</td>
                <td>{{ __('s_bank_credit_types.'. $withdrawal->source_credit_type_name) }}</td>
                <td>{{ amount_format($withdrawal->amount) }}</td>
                <td>{{ $withdrawal->status->status->name }}</td>
                <td>{{ $withdrawal->updated_at }}</td>
                <td>{{ $withdrawal->recipient_address }}</td>
                <td>{{ $withdrawal->transaction_hash }}</td>
            </tr>
            @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('a_external_transfers.no records') ])
            @endforelse
        </tbody>
        @endcomponent
        {{ Form::open(['method'=>'put', 'route' => 'admin.blockchain.withdrawals.bulk-update', 'id'=>'fm-bulk-update']) }}
        {{ Form::formHide('withdrawal_ids') }}
        {{ Form::formHide('remarks') }}
        {{ Form::formHide('withdrawal_status_id') }}
        {{ Form::close() }}
    </div>
</div>
{!! $withdrawals->render() !!}
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        
        let updatableStatuses = '{!! json_encode(blockchain_action_status_dropdown()) !!}';
        console.log(updatableStatuses);
        let updatableStatusOptions = $.parseJSON(updatableStatuses);

        $("#select-all").click(function() {
            if ($(this).prop("checked")) {
                $('.cbx-withdrawal').attr('checked', 'checked');
            } else {
                $('.cbx-withdrawal').removeAttr('checked');
            }
            
        });

        $("#btn-bulk-update").click(function() {
            let withdrawal_ids = [];

            $.each(($('.cbx-withdrawal:checked')), function() {
                withdrawal_ids.push($(this).attr('data-id'));
            });

            if (withdrawal_ids.length === 0) {
                swal.fire({
                    type: 'error',
                    text: 'No withdrawal selected',
                })
                return;
            }

            $('#withdrawal_ids').val(withdrawal_ids);
            swal.fire({
                title: "{{ __('a_withdrawal_list.bulk update') }}",
                input: 'select',
                inputOptions: updatableStatusOptions,
                html: '<div class="form-group"><textarea class="form-control " id="sw-remarks" cols="50" rows="10" placeholder="Remarks" style="resize: none;"></textarea></div>',
                inputPlaceholder: "{{ __('a_withdrawal_list.please select a status') }}",
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                            let remarks = $('#sw-remarks').val();
                        if (value) {
                            if (value == 4 && remarks == "") {
                                resolve("{{ __('a_withdrawal_list.remarks is required for rejecting withdrawals') }}");
                                return;
                            }

                            $('#btn-bulk-update').attr('disabled','disabled');
                            
                            $('#remarks').val(remarks);
                            $('#withdrawal_status_id').val(value);

                            resolve();
                            $('#fm-bulk-update').submit();
                        } else {
                            resolve("{{ __('a_withdrawal_list.please select a status') }}")
                        }
                    })
                }
            })
        });
    });
</script>

@if(!empty($errors))
<script>
    @foreach($errors->all() as $error)
        CreateNoty({
            type: "error",
            text: "{{ $error }}",
        })
    @endforeach
</script>
@endif
@endpush