@extends('templates.admin.master')
@section('title', __('a_member_management_withdrawal_addresses.withdrawal wallet addresses'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_withdrawal_addresses.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_withdrawal_addresses.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_withdrawal_addresses.withdrawal wallet addresses')]
    ],
    'header'=>__('a_member_management_withdrawal_addresses.withdrawal wallet addresses'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'wallet addresses', 'uid' => $userId])

<br />

@component('templates.__fragments.components.filter')
    <div class="col-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('credit_type',  blockchain_credit_types_dropdown(), request('credit_type'), __('a_wallet_withdrawals_addresses.currency type'), [], false) }}
    </div>
    <div class="col-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('address', request('address'),__('a_wallet_withdrawals_addresses.address'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_member_management_withdrawal_addresses.wallet addresses')}}</h4>
            @can('admin_blockchain_withdrawal_addresses_export')
                <a class="btn btn-secondary" href="{{ route('admin.blockchain.withdrawal-addresses.export', array_merge(request()->except('page'))) }}">{{ __('a_external_transfers.export table') }}</a>
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_member_management_withdrawal_addresses.date') }}</th>
                    <th>{{ __('a_member_management_withdrawal_addresses.wallet name') }}</th>
                    <th>{{ __('a_member_management_withdrawal_addresses.wallet type') }}</th>
                    <th>{{ __('a_member_management_withdrawal_addresses.wallet address') }}</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($addresses as $address)
                <tr>
                    <td>{{$address->created_at}}</td>
                    <td>{{$address->name}}</td>
                    <td>{{$address->credit_type_name}}</td>
                    <td>{{$address->address}}</td>
                </tr>
            @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 4,
                'text' => __('a_member_management_withdrawal_addresses.no records') ])
            @endforelse
            </tbody>
        @endcomponent
    </div>
</div>

@endsection
