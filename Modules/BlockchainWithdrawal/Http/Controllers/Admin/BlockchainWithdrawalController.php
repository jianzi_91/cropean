<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\BlockchainWithdrawal\Queries\Admin\BlockchainWithdrawalQuery;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\BlockchainWithdrawal\Http\Requests\Admin\Update;
use Modules\BlockchainWithdrawal\Http\Requests\Admin\BulkUpdateWithdrawalStatus;
use Plus65\Utility\Exceptions\ApiException;
use Plus65\Utility\Exceptions\WebException;

class BlockchainWithdrawalController extends Controller
{
    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawalQuery $withdrawalQuery
     */
    public function __construct(
        BlockchainWithdrawalQuery $withdrawalQuery,
        BlockchainWithdrawalContract $blockchainWithdrawalContract,
        BlockchainWithdrawalStatusContract $blockchainWithdrawalStatusContract
    ) {
        $this->middleware('permission:admin_blockchain_withdrawal_list')->only('index');
        $this->middleware('permission:admin_blockchain_withdrawal_edit')->only(['edit','update']);

        $this->withdrawalQuery                = $withdrawalQuery;
        $this->blockchainWithdrawalRepo       = $blockchainWithdrawalContract;
        $this->blockchainWithdrawalStatusRepo = $blockchainWithdrawalStatusContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $withdrawals = $this->withdrawalQuery
            ->setParameters($request->all())
            ->paginate();

        return view('blockchainwithdrawal::admin.index')->with(compact('withdrawals'));
    }

    public function edit($id)
    {
        $withdrawal = $this->blockchainWithdrawalRepo->findOrFail($id);

        return view('blockchainwithdrawal::admin.edit', compact('withdrawal'));
    }

    public function update(Update $request, $id)
    {
        $withdrawal = $this->blockchainWithdrawalRepo->findOrFail($id);
        $status     = $this->blockchainWithdrawalStatusRepo->find($request->status);

        if ($status->rawname == BlockchainWithdrawalStatusContract::APPROVED) {
            if (!$withdrawal->is_approvable) {
                return back()->with('error', __('a_withdrawals.failed to update withdrawal'));
            }
        }

        if ($status->rawname == BlockchainWithdrawalStatusContract::REJECTED) {
            if (!$withdrawal->is_rejectable) {
                return back()->with('error', __('a_withdrawals.failed to update withdrawal'));
            }
        }

        try {
            DB::beginTransaction();

            if ($status->rawname == BlockchainWithdrawalStatusContract::APPROVED) {
                if ($withdrawal->is_approvable) {
                    $this->blockchainWithdrawalRepo->approve($withdrawal->id);    
                }
            }

            if ($status->rawname == BlockchainWithdrawalStatusContract::REJECTED) {
                if ($withdrawal->is_rejectable) {
                    $this->blockchainWithdrawalRepo->refund($withdrawal->id, $request->remarks, BankTransactionTypeContract::BLOCKCHAIN_WITHDRAWAL_REJECTED);
                }
            }

            $this->blockchainWithdrawalStatusRepo->changeHistory($withdrawal, $status);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            return back()->with('error', __('a_withdrawals.failed to update withdrawal'));
        }

        return redirect(route('admin.blockchain.withdrawals.index'))->with('success', __('a_withdrawals.status updated successfully'));
    }

    public function bulkUpdate(BulkUpdateWithdrawalStatus $request)
    {
        $withdrawalIds = explode(',', $request->withdrawal_ids);

        $lockForUpdateStatuses = [
            $this->blockchainWithdrawalStatusRepo->findBySlug(BlockchainWithdrawalStatusContract::APPROVED)->id,
            $this->blockchainWithdrawalStatusRepo->findBySlug(BlockchainWithdrawalStatusContract::REJECTED)->id,
            $this->blockchainWithdrawalStatusRepo->findBySlug(BlockchainWithdrawalStatusContract::CANCELLED)->id,
        ];

        $skipForUpdateStatuses = [

        ];

        DB::beginTransaction();

        try {
            ini_set('max_execution_time', -1);
            $blockchainStatus = $this->blockchainWithdrawalStatusRepo->find($request->withdrawal_status_id);

            foreach ($withdrawalIds as $withdrawalId) {
                $blockchainWithdrawal = $this->blockchainWithdrawalRepo->getModel()->where('id', $withdrawalId)->lockForUpdate()->first();

                if (!in_array($blockchainWithdrawal->blockchain_withdrawal_status_id, $lockForUpdateStatuses)) {
                    $this->blockchainWithdrawalRepo->edit($withdrawalId, $request->only(['blockchain_withdrawal_status_id', 'remarks']));

                    if ($blockchainStatus->rawname === BlockchainWithdrawalStatusContract::APPROVED) {
                        # do transfer out
                        $this->blockchainWithdrawalRepo->approve($withdrawalId);
                    } 
                    
                    if ($blockchainStatus->rawname === BlockchainWithdrawalStatusContract::REJECTED) {
                        # refund
                        $this->blockchainWithdrawalRepo->refund($withdrawalId, $request->remarks);
                    }
                }

                $this->blockchainWithdrawalStatusRepo->changeHistory($blockchainWithdrawal, $blockchainStatus, ['updated_by' => auth()->id()]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            throw new WebException($e, __('a_withdrawal_request.failed to update bulk withdrawal requests status'));
        }

        return redirect(route('admin.blockchain.withdrawals.index'))->with('success', __('a_withdrawals.status updated successfully'));
    }
}
