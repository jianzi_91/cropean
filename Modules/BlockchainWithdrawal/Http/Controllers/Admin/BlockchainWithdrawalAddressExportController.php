<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\BlockchainWithdrawal\Queries\Admin\BlockchainWithdrawalAddressQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainWithdrawalAddressExportController extends Controller
{
    use CanExportTrait;

    /**
     * Withdrawal address repository.
     *
     * @var BlockchainWithdrawalAddressQuery
     */
    protected $withdrawalAddressQuery;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
     */
    public function __construct(
        BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
    ) {
        $this->middleware('permission:admin_blockchain_withdrawal_addresses_export')->only('index');

        $this->withdrawalAddressQuery = $withdrawalAddressQuery;
    }

    /**
     * Show a resource.
     *
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->withdrawalAddressQuery->setParameters($request->all()), 'admin_withdrawal_addresses_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
