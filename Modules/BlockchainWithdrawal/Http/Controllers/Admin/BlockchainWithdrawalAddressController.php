<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\BlockchainWithdrawal\Queries\Admin\BlockchainWithdrawalAddressQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\User\Models\User;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainWithdrawalAddressController extends Controller
{
    use CanExportTrait;

    /**
     * Bank credit type repository.
     *
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepo;

    /**
     * Withdrawal address repository.
     *
     * @var BlockchainWithdrawalAddressQuery
     */
    protected $withdrawalAddressQuery;

    /**
     * Class constructor
     *
     * @param BankCreditTypeContract $bankCreditTypeRepo
     * @param BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
     */
    public function __construct(
        BankCreditTypeContract $bankCreditTypeRepo,
        BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
    ) {
        $this->middleware('permission:admin_blockchain_withdrawal_addresses_list')->only('index', 'show');

        $this->bankCreditTypeRepo     = $bankCreditTypeRepo;
        $this->withdrawalAddressQuery = $withdrawalAddressQuery;
    }

    /**
     * Show a resource.
     *
     */
    public function index(Request $request)
    {
        $addresses = $this->withdrawalAddressQuery
            ->setParameters($request->all())
            ->paginate();

        $blockchainCredits = $this->bankCreditTypeRepo->getBlockchainCredits();
        $blockchainCredits = $blockchainCredits->pluck('name', 'rawname');

        return view('blockchainwithdrawal::admin.withdrawal-addresses.index', compact('addresses', 'blockchainCredits'));
    }

    /**
     * Show a resource.
     *
     */
    public function show(Request $request, $userId)
    {
        if (isset($userId)) {
            $user = User::find($userId);
            $request->merge([
                'member_id' => $user->member_id,
            ]);
        }

        $addresses = $this->withdrawalAddressQuery
            ->setParameters($request->all())
            ->paginate();

        $blockchainCredits = $this->bankCreditTypeRepo->getBlockchainCredits();
        $blockchainCredits = $blockchainCredits->pluck('name', 'rawname');

        return view('blockchainwithdrawal::admin.withdrawal-addresses.index', compact('addresses', 'blockchainCredits', 'userId'));
    }

    public function getQrImage($bankCreditName)
    {
        return response()->file(storage_path('wallet/qr_code/') . basename(setting('master_wallet_' . $bankCreditName . '_qr_code')), ['Content-Type' => 'image/svg+xml']);
    }

    /*
        * @param Request $request
        * @param $type
    */
    public function export(Request $request, $userId)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->withdrawalAddressQuery->setParameters($request->all()), 'member_withdrawal_addresses_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
