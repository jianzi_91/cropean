<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\BlockchainWithdrawal\Queries\Admin\BlockchainWithdrawalQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainWithdrawalExportController extends Controller
{
    use CanExportTrait;

    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawalQuery $withdrawalQuery
     */
    public function __construct(BlockchainWithdrawalQuery $withdrawalQuery)
    {
        $this->middleware('permission:admin_blockchain_withdrawal_export')->only('index');

        $this->withdrawalQuery = $withdrawalQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->withdrawalQuery->setParameters($request->all()), 'admin_withdrawals_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
