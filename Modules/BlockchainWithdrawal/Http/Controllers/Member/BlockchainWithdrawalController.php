<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\BlockchainWithdrawal\Http\Requests\Member\Store;
use Modules\BlockchainWithdrawal\Queries\Member\BlockchainWithdrawalAddressQuery;
use Modules\BlockchainWithdrawal\Queries\Member\BlockchainWithdrawalQuery;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\User\Contracts\UserContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Plus65\Utility\Exceptions\WebException;

class BlockchainWithdrawalController extends Controller
{
    /**
     * The withdrawal repository
     *
     * @var unknown
     */
    protected $withdrawalRepository;

    /**
     * The withdrawal type repository
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * The withdrawal address repository
     *
     * @var unknown
     */
    protected $withdrawalAddressRepository;

    protected $userRepository;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawalContract $blockchainWithdrawalContract
     * @param WithdrawalTypeContract $withdrawalTypeContract
     * @param BlockchainWithdrawalQuery $withdrawalQuery
     * @param BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
     * @param UserContract $userRepository
     */
    public function __construct(
        BlockchainWithdrawalContract $blockchainWithdrawalContract,
        BlockchainWithdrawalStatusContract $blockchainWithdrawalStatusContract,
        WithdrawalTypeContract $withdrawalTypeContract,
        BlockchainWithdrawalQuery $withdrawalQuery,
        BlockchainWithdrawalAddressQuery $withdrawalAddressQuery,
        UserContract $userRepository
    ) {
        $this->middleware('permission:member_blockchain_withdrawal_list')->only('index');
        $this->middleware('permission:member_blockchain_withdrawal_create')->only('create', 'store');
        $this->middleware('double_click_prevention')->only('store');

        // Construct contracts
        $this->blockchainWithdrawalRepository       = $blockchainWithdrawalContract;
        $this->blockchainWithdrawalStatusRepository = $blockchainWithdrawalStatusContract;
        $this->withdrawalTypeRepository             = $withdrawalTypeContract;

        // Construct queries
        $this->withdrawalQuery        = $withdrawalQuery;
        $this->withdrawalAddressQuery = $withdrawalAddressQuery;

        $this->userRepository = $userRepository;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->document_verification_status_id != document_status_approve_id()) {
            return redirect(route('member.documents.index'));
        }

        $withdrawals = $this->withdrawalQuery
            ->setParameters($request->all())
            ->paginate();

        return view('blockchainwithdrawal::member.index')->with(compact('withdrawals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        if (auth()->user()->document_verification_status_id != document_status_approve_id()) {
            return redirect(route('member.documents.index'));
        }

        $withdrawalType      = $this->withdrawalTypeRepository->findBySlug($this->withdrawalTypeRepository::BLOCKCHAIN);
        $withdrawalAddresses = $this->withdrawalAddressQuery
            ->setParameters($request->all())
            ->get();

        $addresses = [];
        foreach ($withdrawalAddresses as $withdrawalAddress) {
            $addresses[$withdrawalAddress->id] = $withdrawalAddress->display_name;
        }

        return view('blockchainwithdrawal::member.create')->with(compact('withdrawalType', 'addresses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();
        try {
            $user   = $this->userRepository->getModel()->where('id', auth()->user()->id)->lockForUpdate()->first();
            $userId = $user->id;
            if (!$this->blockchainWithdrawalRepository->request($userId, $request->user_withdrawal_address_id, $request->amount, $request->credit_type, $request->remarks)) {
                return redirect(route('member.blockchain.withdrawals.index'))->with('error', __('m_withdrawals.withdrawal failed'));
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.blockchain.withdrawals.index'))->withMessage(__('m_withdrawals.failed to request withdrawal'));
        }

        return redirect(route('member.blockchain.withdrawals.index'))->with('success', __('m_withdrawals.withdraw requested'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $withdrawal      = $this->blockchainWithdrawalRepository->findOrFail($id);
        $cancelledStatus = $this->blockchainWithdrawalStatusRepository->findBySlug(BlockchainWithdrawalStatusContract::CANCELLED);

        if (!$withdrawal->is_cancellable) {
            return back()->with('error', __('m_withdrawals.failed to cancel withdrawal'));
        }

        DB::beginTransaction();
        try {
            $withdrawal = $this->blockchainWithdrawalRepository->refund($withdrawal->id, null, BankTransactionTypeContract::BLOCKCHAIN_WITHDRAWAL_CANCELLED);
            $this->blockchainWithdrawalStatusRepository->changeHistory($withdrawal, $cancelledStatus);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.blockchain.withdrawals.index'))->withMessage(__('m_withdrawals.failed to cancel withdrawal'));
        }

        return redirect(route('member.blockchain.withdrawals.index'))->with('success', __('m_withdrawals.withdraw cancel'));
    }

    public function calculate()
    {
        $data        = request()->only('creditType', 'amount');
        $percentage  = setting('withdrawal_' . $data['creditType'] . '_admin_fee_percentage');
        $adminFee    = $this->blockchainWithdrawalRepository->calculateAdminFee($data['amount'], $percentage, 2);
        $totalAmount = $this->blockchainWithdrawalRepository->calculateAmount($data['amount'], $adminFee, 2);

        return response()->json([
            'admin_fee'    => $adminFee,
            'total_amount' => $totalAmount,
        ]);
    }
}
