<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalAddressContract;
use Modules\BlockchainWithdrawal\Http\Requests\Member\AddressStore;
use Modules\BlockchainWithdrawal\Http\Requests\Member\AddressUpdate;
use Modules\BlockchainWithdrawal\Queries\Member\BlockchainWithdrawalAddressQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Plus65\Utility\Exceptions\WebException;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainWithdrawalAddressController extends Controller
{
    use CanExportTrait;

    /**
     * Withdrawal address repository.
     *
     * @var BlockchainWithdrawalAddressContract
     */
    protected $withdrawalAddressRepo;

    /**
     * Class constructor
     *
     * @param BankCreditTypeContract $bankCreditTypeRepo
     * @param BlockchainWithdrawalAddressContract $withdrawalAddressRepo
     * @param BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
     */
    public function __construct(
        BankCreditTypeContract $bankCreditTypeRepo,
        BlockchainWithdrawalAddressContract $withdrawalAddressRepo,
        BlockchainWithdrawalAddressQuery $withdrawalAddressQuery
    ) {
        $this->middleware('permission:member_blockchain_withdrawal_addresses_list')->only('index');
        $this->middleware('permission:member_blockchain_withdrawal_addresses_create')->only('create', 'store');
        $this->middleware('permission:member_blockchain_withdrawal_addresses_edit')->only('edit', 'update');
        $this->middleware('permission:member_blockchain_withdrawal_addresses_delete')->only('destroy');

        $this->bankCreditTypeRepo     = $bankCreditTypeRepo;
        $this->withdrawalAddressRepo  = $withdrawalAddressRepo;
        $this->withdrawalAddressQuery = $withdrawalAddressQuery;
    }

    /**
     * Show a resource.
     *
     */
    public function index(Request $request)
    {
        $addresses = $this->withdrawalAddressQuery
            ->setParameters($request->all())
            ->paginate();

        return view('blockchainwithdrawal::member.withdrawal-addresses.index', compact('addresses'));
    }

    /**
     * Create a resource.
     */
    public function create(Request $request)
    {
        return view('blockchainwithdrawal::member.withdrawal-addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AddressStore $request)
    {
        DB::beginTransaction();

        try {
            $this->withdrawalAddressRepo->add([
                'user_id'             => $request->user()->id,
                'bank_credit_type_id' => $request->bank_credit_type_slug,
                'name'                => $request->name,
                'address'             => $request->address,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw (new WebException($e))->redirectTo(route('member.blockchain.withdrawal-addresses.index'))->withMessage(__('m_wallet_address.failed to store withdrawal address'));
        }

        return redirect(route('member.blockchain.withdrawal-addresses.index'))->with('success', __('m_wallet_address.withdrawal address stored successfully'));
    }

    /**
     * Edit a resource.
     *
     * @param Request $request
     * @param int $id
     */
    public function edit($id)
    {
        $address     = $this->withdrawalAddressRepo->find($id);
        $credit_type = $this->bankCreditTypeRepo->find($address->bank_credit_type_id)->rawname;

        if ($address->user_id != auth()->user()->id) {
            abort(404);
        }

        return view('blockchainwithdrawal::member.withdrawal-addresses.edit', compact('address', 'credit_type'));
    }

    public function update(AddressUpdate $request, $id)
    {
        DB::beginTransaction();

        try {
            $this->withdrawalAddressRepo->edit($id, [
                'name'    => $request->name,
                'address' => $request->address,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw (new WebException($e))->redirectTo(route('member.blockchain.withdrawal-addresses.index'))->withMessage(__('m_wallet_address.failed to update withdrawal address'));
        }

        return redirect(route('member.blockchain.withdrawal-addresses.index'))->with('success', __('m_wallet_address.withdrawal address updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $address = $this->withdrawalAddressRepo->find($id);

            if ($address->user_id != auth()->user()->id) {
                throw new WebException(__('m_wallet_address.address does not belongs to the member'));
            }

            $this->withdrawalAddressRepo->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.blockchain.withdrawal-addresses.index'))->withMessage(__('m_wallet_address.failed to delete withdrawal address'));
        }

        return  redirect(route('member.blockchain.withdrawal-addresses.index'))->with('success', __('m_wallet_address.withdrawal address deleted successfully'));
    }

    public function getWithdrawalAddresses($creditType)
    {
        $addresses = $this->withdrawalAddressRepo->getModel()
            ->where('user_id', auth()->user()->id)
            ->where('bank_credit_type_id', bank_credit_type_id($creditType))
            ->get();

        $list = [];
        foreach ($addresses as $address) {
            $list[$address->id] = $address->name . ' - ' . $address->address;
        }

        return response()->json($list);
    }
}
