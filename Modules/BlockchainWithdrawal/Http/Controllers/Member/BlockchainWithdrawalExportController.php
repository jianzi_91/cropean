<?php

namespace Modules\BlockchainWithdrawal\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\BlockchainWithdrawal\Queries\Member\BlockchainWithdrawalQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainWithdrawalExportController extends Controller
{
    use CanExportTrait;
    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * Class constructor
     *
     * @param BlockchainWithdrawalQuery $withdrawalQuery
     */
    public function __construct(
        BlockchainWithdrawalQuery $withdrawalQuery
    ) {
        $this->middleware('permission:member_blockchain_withdrawal_export')->only('index');

        // Construct queries
        $this->withdrawalQuery = $withdrawalQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->withdrawalQuery->setParameters($request->all()), 'member_withdrawals_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
