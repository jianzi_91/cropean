<?php

namespace Modules\BlockchainWithdrawal\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;

class IsExternalAddress implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $walletRepo = resolve(BlockchainWithdrawalContract::class);
        $count      = $walletRepo->getModel()->where('address', $value)->get()->count();

        if ($count > 0) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.address exists in system');
    }
}
