<?php

namespace Modules\BlockchainWithdrawal\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Blockchain\Contracts\BlockchainWalletContract;

class IsExternalBlockchainAddress implements Rule
{
    /**
     * Bank credit type slug.
     *
     * @var string
     */
    protected $bankCreditTypeSlug;

    /**
     * Class constructor.
     *
     * @param string $bankCreditTypeSlug
     */
    public function __construct(string $bankCreditTypeSlug)
    {
        $this->bankCreditTypeSlug = $bankCreditTypeSlug;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $blockchainWalletContract = resolve(BlockchainWalletContract::class);
        $internalAddress          = $blockchainWalletContract->findByAddress($value, get_bank_credit_type($this->bankCreditTypeSlug)->id);

        if (!$internalAddress) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.invalid address');
    }
}
