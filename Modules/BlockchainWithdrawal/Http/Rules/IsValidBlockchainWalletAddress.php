<?php

namespace Modules\BlockchainWithdrawal\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Blockchain\Contracts\CryptoCurrencies\BitcoinContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Credit\Contracts\BankCreditTypeContract;

class IsValidBlockchainWalletAddress implements Rule
{
    /**
     * Bank credit type slug.
     *
     * @var string
     */
    protected $bankCreditTypeSlug;

    /**
     * Class constructor.
     *
     * @param string $bankCreditTypeSlug
     */
    public function __construct(string $bankCreditTypeSlug)
    {
        $this->bankCreditTypeSlug = $bankCreditTypeSlug;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->getCryptoCurrencyRepo($this->bankCreditTypeSlug)->validateAddress($value);
    }

    public function getCryptoCurrencyRepo(string $bankCreditTypeSlug)
    {
        switch ($bankCreditTypeSlug) {
            case BankCreditTypeContract::USDT_ERC20:
            case BankCreditTypeContract::USDC:
            case BankCreditTypeContract::ETH:
                return resolve(EthereumContract::class);

            case BankCreditTypeContract::BTC:
            case BankCreditTypeContract::USDT_OMNI:
                return resolve(BitcoinContract::class);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.invalid address format');
    }
}
