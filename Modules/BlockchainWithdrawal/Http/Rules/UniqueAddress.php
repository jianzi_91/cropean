<?php

namespace Modules\BlockchainWithdrawal\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalAddressContract;
use Modules\Credit\Traits\ValidateCredit;

class UniqueAddress implements Rule
{
    use ValidateCredit;

    /**
     * User ID.
     *
     * @var int
     */
    protected $userId;

    /**
     * The withdrawal type id
     *
     * @var unknown
     */
    protected $bankCreditTypeSlug;

    /**
     * Ignore ID.
     *
     * @var int
     */
    protected $ignoreId;

    /**
     * The message
     *
     * @var unknown
     */
    protected $message;

    /**
     * Withdrawal address repository.
     *
     * @var mixed
     */
    protected $withdrawalAddressRepo;

    /**
     * Create a new rule instance.
     *
     * @param int $userId
     * @param int $bankCreditTypeSlug
     * @param int $ignoreId
     */
    public function __construct(int $userId, string $bankCreditTypeSlug, int $ignoreId = 0)
    {
        $this->userId                = $userId;
        $this->bankCreditTypeSlug    = $bankCreditTypeSlug;
        $this->ignoreId              = $ignoreId;
        $this->withdrawalAddressRepo = resolve(BlockchainWithdrawalAddressContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws \Modules\Credit\Exceptions\InvalidCreditType
     */
    public function passes($attribute, $value)
    {
        return !$this->withdrawalAddressRepo
            ->recordExists($this->userId, $this->validateCreditType($this->bankCreditTypeSlug), $value, $this->ignoreId);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('m_withdrawals.address exists for the selected type');
    }
}
