<?php

namespace Modules\BlockchainWithdrawal\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\IsWithdrawable;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Rules\ValidWithdrawalAmount;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $withdrawalTypeRepo = resolve(WithdrawalTypeContract::class);

        return [
            'credit_type' => [
                'bail',
                'required',
                'exists:bank_credit_types,name',
                new IsWithdrawable(),
            ],
            'user_withdrawal_address_id' => [
                'bail',
                'required',
                'exists:blockchain_withdrawal_addresses,id'
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'max:' . config('withdrawal.withdrawal.max'),
                'gt:0',
                new NotScientificNotation(),
                new ValidWithdrawalAmount($this->credit_type, $withdrawalTypeRepo->findBySlug($this->withdrawal_type)->id),
                new EnoughCredit($this->credit_type, auth()->user()->id),
                new MaxDecimalPlaces(credit_precision($this->credit_type))
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    public function validationData()
    {
        $this->merge([
            'withdrawal_type' => WithdrawalTypeContract::BLOCKCHAIN,
        ]);

        return parent::validationData();
    }

    /*
     * Set custom attributes name
     */
    public function attributes()
    {
        return [
            'credit_type'                => __('m_create_withdrawal.credit type'),
            'user_withdrawal_address_id' => __('m_create_withdrawal.user withdrawal address'),
            'amount'                     => __('m_create_withdrawal.amount'),
            'secondary_password'         => __('m_create_withdrawal.secondary password')
        ];
    }
}
