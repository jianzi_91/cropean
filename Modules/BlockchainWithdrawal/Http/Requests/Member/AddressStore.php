<?php

namespace Modules\BlockchainWithdrawal\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\BlockchainWithdrawal\Http\Rules\IsExternalBlockchainAddress;
use Modules\BlockchainWithdrawal\Http\Rules\IsValidBlockchainWalletAddress;
use Modules\BlockchainWithdrawal\Http\Rules\UniqueAddress;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Withdrawal\Http\Rules\CanWithdraw;

class AddressStore extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_credit_type_slug' => [
                'bail',
                'required',
                new CanWithdraw(),
            ],
            'name' => [
                'bail',
                'required',
            ],
            'address' => [
                'bail',
                'required',
                isset($this->bank_credit_type_slug) ? new UniqueAddress(auth()->id(), $this->bank_credit_type_slug) : '',
                isset($this->bank_credit_type_slug) ? new IsValidBlockchainWalletAddress($this->bank_credit_type_slug) : '',
                isset($this->bank_credit_type_slug) ? new IsExternalBlockchainAddress($this->bank_credit_type_slug) : '',
            ],
            'secondary_password' => [
                'bail',
                'required',
                new SecondaryPasswordMatch()
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'bank_credit_type_slug' => __('m_create_withdrawal_address.bank credit type slug'),
            'name'                  => __('m_create_withdrawal_address.name'),
            'address'               => __('m_create_withdrawal_address.address'),
            'secondary_password'    => __('m_create_withdrawal_address.secondary password'),
        ];
    }
}
