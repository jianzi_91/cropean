<?php

namespace Modules\BlockchainWithdrawal\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BulkUpdateWithdrawalStatus extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'withdrawal_ids'       => 'required',
            'withdrawal_status_id' => [
                'bail',
                'required',
                'integer',
                Rule::exists('blockchain_withdrawal_statuses', 'id')->where(function ($query) {
                    $query->whereIn('name', [BlockchainWithdrawalStatusContract::APPROVED, BlockchainWithdrawalStatusContract::REJECTED, BlockchainWithdrawalStatusContract::PROCESSING]);
                }),
            ],
        ];

        /** @var WithdrawalStatusRepository $withdrawalStatusContract */
        $withdrawalStatusContract = resolve(BlockchainWithdrawalStatusContract::class);
        $rejectedStatus           = $withdrawalStatusContract->findBySlug($withdrawalStatusContract::REJECTED);
        if ($rejectedStatus->id == $this->withdrawal_status_id) {
            $rules['remarks'] = 'required|max:255';
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'withdrawal_ids'       => __('a_wallet_withdrawals_request.withdrawal requests'),
            'withdrawal_status_id' => __('a_wallet_withdrawals_request.blockchain withdrawal status'),
            'remarks'              => __('a_wallet_withdrawals_request.remarks')
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $withdrawalStatusRepo = resolve(BlockchainWithdrawalStatusContract::class);
            $approvedStatus = $withdrawalStatusRepo->findBySlug(BlockchainWithdrawalStatusContract::APPROVED);
            if ($this->withdrawal_status_id == $approvedStatus->id) {
                if (!empty($this->withdrawal_ids)) {
                    $withdrawalRepo = resolve(BlockchainWithdrawalContract::class);
                    $usdtErc20Repo  = resolve(UsdtErc20Repository::class);
                    $usdcRepo       = resolve(UsdcRepository::class);
    
                    $withdrawalIds = explode(',', $this->withdrawal_ids);
                    $withdrawals = $withdrawalRepo->getModel()
                        ->whereIn('id', $withdrawalIds)
                        ->get();
    
                    $totalUsdtWithdrawal   = $withdrawals->where('source_credit_type_id', bank_credit_type_id(BankCreditTypeContract::USDT_ERC20))->sum('amount');
                    $totalMasterWalletUsdt = $usdtErc20Repo->getBalance(setting('master_wallet_usdt_erc20_address'));
                    $totalUsdcWithdrawal   = $withdrawals->where('source_credit_type_id', bank_credit_type_id(BankCreditTypeContract::USDC))->sum('amount');
                    $totalMasterWalletUsdc = $usdcRepo->getBalance(setting('master_wallet_usdc_address'));
    
                    if ($totalUsdtWithdrawal > $totalMasterWalletUsdt) {
                        $validator->errors()->add('insufficient_usdt', __('a_wallet_withdrawals_request.total usdt erc20 withdrawal exceeded master wallet amount'));
                    }
    
                    if ($totalUsdcWithdrawal > $totalMasterWalletUsdc) {
                        $validator->errors()->add('insufficient_usdc', __('a_wallet_withdrawals_request.total usdc withdrawal exceeded master wallet amount'));
                    }
                }
            }
        });
    }
}
