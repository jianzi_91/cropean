<?php

namespace Modules\BlockchainWithdrawal\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $statusRepo = resolve(BlockchainWithdrawalStatusContract::class);
        $rejectedStatus = $statusRepo->findBySlug(BlockchainWithdrawalStatusContract::REJECTED);

        return [
            'status' => [
                'bail',
                'required',
            ],
            'remarks' => [
                'required_if:status,' .  $rejectedStatus->id,
                'max:255',
            ]
        ];
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'remarks.required_if' => __('a_wallet_withdrawals_edit.remarks is required when status is rejected'), 
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'status' => __('a_wallet_withdrawals_edit.status'),
        ];
    }
}
