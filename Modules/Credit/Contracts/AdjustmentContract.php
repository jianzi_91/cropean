<?php

namespace Modules\Credit\Contracts;

use Carbon\Carbon;

interface AdjustmentContract
{
    /**
     * Add credit to target
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $transactionType
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return string|boolean
     */
    public function add($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null);

    /**
     * Deduct credit to target
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $transactionType
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return return string|boolean
     */
    public function deduct($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null);
}
