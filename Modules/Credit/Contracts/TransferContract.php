<?php

namespace Modules\Credit\Contracts;

use Carbon\Carbon;

interface TransferContract
{
    /**
     * Transfer credit from source to target
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $source
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return array|null
     */
    public function transfer($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null);
}
