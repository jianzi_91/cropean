<?php

namespace Modules\Credit\Contracts;

interface TransactionCodeContract
{
    /**
     * Generate transaction code
     * @param string $prefix
     * @param number $length
     * @return string
     */
    public function generate($prefix = 'TXN', $length = 0);
}
