<?php

namespace Modules\Credit\Contracts;

use Carbon\Carbon;
use Modules\User\Contracts\UserStatusContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BankAccountStatementContract extends CrudContract, SlugContract
{
    /**
     * Get all Bank Account Statements
     * @param Carbon $from
     * @param Carbon $to
     * @param string $orderBy
     * @param unknown $transactionType
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function get(Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*']);

    /**
     * Find a Bank Account Statements by account Id
     * @param unknown $id
     * @param Carbon $from
     * @param Carbon $to
     * @param string $orderBy
     * @param unknown $transactionType
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function findByAccountId($id, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*']);

    /**
     * Find a Bank Account Statements by Account Number
     * @param unknown $accountNum
     * @param Carbon $from
     * @param Carbon $to
     * @param string $orderBy
     * @param unknown $transactionType
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function findByAccountNum($accountNum, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*']);

    /**
     * Find a Bank Account Statements by Reference Id
     * @param unknown $referenceId
     * @param Carbon $from
     * @param Carbon $to
     * @param string $orderBy
     * @param unknown $transactionType
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function findByReferenceId($referenceId, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*']);

    /**
     * Delete Bank Account Statements by account Ids
     * @param unknown $ids
     * @return integer
     */
    public function deleteByAccountId($ids);

    /**
     * Delete Bank Account Statements by Account Number
     * @param unknown $ids
     * @return integer
     */
    public function deleteByAccountNum($ids);

    /**
     * Delete Bank Account Statements by Reference Ids
     * @param unknown $ids
     */
    public function deleteByReferenceId($ids);

    public function updateToCreditTypeIdUsingTxnCode($txnCode, $toCreditTypeId);

    //get all downline total balances
    public function getDownlineTotalBalance($userId, $creditTypes, $includedSelf = false, $statuses = [UserStatusContract::ACTIVE, UserStatusContract::HOLD, UserStatusContract::SUSPENDED]);
}
