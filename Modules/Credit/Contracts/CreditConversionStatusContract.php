<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface CreditConversionStatusContract extends CrudContract, SlugContract, HistoryableContract
{
    const PENDING   = 'pending';
    const APPROVED  = 'approved';
    const REJECTED  = 'rejected';
    const CANCELLED = 'cancelled';
    const COMPLETED = 'completed';
}
