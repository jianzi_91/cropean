<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SoftDeleteable;

interface BankAccountContract extends CrudContract, SoftDeleteable
{
    /**
     * Find Bank Account by Account Number
     * @param unknown $accountNum
     * @param array $with
     * @param array $select
     * @return Illuminate\Database\Eloquent\Model
     */
    public function findByAccountNum($accountNum, array $with = [], $select = ['*']);

    /**
     * Find Bank Account by Reference Id
     * @param unknown $referenceId
     * @param array $select
     * @return Illuminate\Database\Eloquent\Model
     */
    public function findByReferenceId($referenceId, array $with = [], $select = ['*']);

    /**
     * Edit a Bank Account by Account Number
     * @param unknown $accountNum
     * @param array $with
     * @param array $attributes
     * @return boolean
     */
    public function editByAccountNum($accountNum, array $attributes = []);

    /**
     * Edit a Bank Account by Reference Id
     * @param unknown $referenceId
     * @param array $attributes
     * @return boolean
     */
    public function editByReferenceId($referenceId, array $attributes = []);

    /**
     * Delete Bank Account by Account Numbers
     * @param unknown $ids
     * @return integer
     */
    public function deleteByAccountNum($ids);

    /**
     * Delete Bank Account by Reference Ids
     * @param unknown $ids
     * @return integer
     */
    public function deleteByReferenceId($ids);
}
