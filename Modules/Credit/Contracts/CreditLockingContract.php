<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface CreditLockingContract extends CrudContract
{
    public function updateCreditLock($sourceAmount, $userId);

    public function getAvailableLocks($todayDate, $userId);
}
