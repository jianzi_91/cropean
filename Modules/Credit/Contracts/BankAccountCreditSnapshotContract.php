<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BankAccountCreditSnapshotContract extends CrudContract
{
    public function getTotalCredits($snapshotDate, $creditTypes);
}
