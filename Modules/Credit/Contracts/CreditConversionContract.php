<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface CreditConversionContract extends CrudContract
{
    /**
     * Calculate and get normal admin fee
     * @param string $sourceCreditType
     * @param unknown $sourceAmount
     * @return $fee
     */
    public function calculateAdminFee($sourceCreditType, $sourceAmount);

    /**
     * Calculate and get converted amount
     * @param unknown $amount
     * @param unknown $rate
     * @return $amount
     */
    public function calculateAmount($amount, $rate);

    /**
     * Calculate and get convert amount
     * @param unknown $amount
     * @param unknown $adminFee
     * @param unknown $lockedInAdminFee
     * @return $amount
     */
    public function convertAmount($amount, $adminFee, $lockedInAdminFee);

    public function getConvertCreditDetails($request);

    public function calculate($request);

    public function getConvertibleCreditTypes($creditConvertFrom);
}
