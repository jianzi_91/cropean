<?php

namespace Modules\Credit\Contracts;

use Carbon\Carbon;

interface BankAccountCreditContract extends AdjustmentContract
{
    /**
     * Add credit to target by Account Number
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $transactionType
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return string|boolean
     */
    public function addByAccountNum($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null);

    /**
     * Deduct credit to target by Account Number
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $transactionType
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return string|boolean
     */
    public function deductByAccountNum($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null);

    /**
     * Transfer credit from source to target by Reference Id
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $source
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return array
     */
    public function transfer($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null);

    /**
     * Transfer credit from source to target by Account Number
     * @param unknown $creditType
     * @param unknown $amount
     * @param unknown $target
     * @param unknown $source
     * @param unknown $remarks
     * @param unknown $doneBy
     * @param Carbon $transactionDate
     * @return array|null
     */
    public function transferByAccountNum($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null);

    /**
     * Get Bank Account's outstanding balance by Reference Number
     * @param unknown $bankId
     * @param array $creditTypes
     * @return array|null
     */
    public function getBalanceByReference($referenceId, $creditTypes = []);

    /**
     * Get Bank Account's outstanding balance by Account Number
     * @param unknown $accountNumber
     * @param array $creditTypes
     * @return array|null
     */
    public function getBalanceByAccountNum($accountNumber, $creditTypes = []);

    /**
     * Get Bank Account's outstanding balance by Id
     * @param unknown $id
     * @param array $creditTypes
     * @return array|null
     */
    public function getBalance($id, $creditTypes = []);

    /**
     * Get System bank account balance
     * @param unknow $creditTypeId
     * @return array|null
     */
    public function getSystemBalance($creditTypeId);

    /**
     * Get System bank account balance
     * @param array $creditTypeId
     * @return array|null
     */
    public function getSystemAllBalance($creditTypeIds);
}
