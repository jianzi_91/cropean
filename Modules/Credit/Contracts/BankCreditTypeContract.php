<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BankCreditTypeContract extends CrudContract, SlugContract, ActiveContract
{
    //System Credit
    const USD_CREDIT       = 'usd_credit';
    const CAPITAL_CREDIT   = 'capital_credit';
    const ROLL_OVER_CREDIT = 'roll_over_credit';
    const CASH_CREDIT      = 'cash_credit';
    const PROMOTION_CREDIT = 'promotion_credit';

    // Blockchain
//    const USDT       = 'usdt';
    const USDT_OMNI  = 'usdt_omni';
    const USDT_ERC20 = 'usdt_erc20';
    const BTC        = 'btc';
    const ETH        = 'eth';
    const USDC       = 'usdc';

    /**
     * Blockchain credit networks
     */
    const BTC_NETWORK = 'BTC';
    const ETH_NETWORK = 'ETH';

    /*
     * For exchange rate, not a credit type
     */
    const EXCHANGE_RATE = 'exchange_rate';
}
