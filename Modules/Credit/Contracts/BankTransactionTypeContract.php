<?php

namespace Modules\Credit\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface BankTransactionTypeContract extends CrudContract, SlugContract, ActiveContract
{
    const SPONSOR_BONUS          = 'sponsor_bonus';
    const GOLDMINE_BONUS         = 'goldmine_bonus';
    const SPECIAL_BONUS          = 'special_bonus';
    const LEADER_BONUS           = 'leader_bonus';
    const LEADER_BONUS_SAME_RANK = 'leader_bonus_same_rank';
    const REFERRAL_BONUS         = 'referral_bonus';
    const REBATE                 = 'rebate';

    const CREDIT_CONVERSION_TO                    = 'credit_conversion_to';
    const CREDIT_CONVERSION_FROM                  = 'credit_conversion_from';
    const CREDIT_CONVERSION_FEE                   = 'credit_conversion_fee';
    const CREDIT_CONVERSION_PENALTY_FEE           = 'credit_conversion_penalty_fee';
    const CREDIT_CONVERSION_CANCELLED             = 'credit_conversion_cancelled';
    const CREDIT_CONVERSION_FEE_CANCELLED         = 'credit_conversion_fee_cancelled';
    const CREDIT_CONVERSION_PENALTY_FEE_CANCELLED = 'credit_conversion_penalty_fee_cancelled';
    const CREDIT_CONVERSION_REJECTED              = 'credit_conversion_rejected';
    const CREDIT_CONVERSION_FEE_REJECTED          = 'credit_conversion_fee_rejected';
    const CREDIT_CONVERSION_PENALTY_FEE_REJECTED  = 'credit_conversion_penalty_fee_rejected';
    const CREDIT_ADJUSTMENT                       = 'credit_adjustment';

    const WITHDRAWAL_REQUEST = 'withdrawal_request';
    const WITHDRAWAL_FEE     = 'withdrawal_fee';

    const WITHDRAWAL_REFUND     = 'withdrawal_refund';
    const WITHDRAWAL_FEE_REFUND = 'withdrawal_fee_refund';

    const WITHDRAWAL_CANCEL     = 'withdrawal_cancel';
    const WITHDRAWAL_FEE_CANCEL = 'withdrawal_fee_cancel';

    const WITHDRAWAL_REJECTED     = 'withdrawal_rejected';
    const WITHDRAWAL_FEE_REJECTED = 'withdrawal_fee_rejected';

    const CREDIT_TRANSFER_FROM = 'credit_transfer_from';
    const CREDIT_TRANSFER_TO   = 'credit_transfer_to';

    const DEPOSIT = 'deposit';

    const BLOCKCHAIN_DEPOSIT              = 'blockchain_deposit';
    const BLOCKCHAIN_WITHDRAWAL           = 'blockchain_withdrawal';
    const BLOCKCHAIN_WITHDRAWAL_FEE       = 'blockchain_withdrawal_fee';
    const BLOCKCHAIN_WITHDRAWAL_REJECTED  = 'blockchain_withdrawal_rejected';
    const BLOCKCHAIN_WITHDRAWAL_CANCELLED = 'blockchain_withdrawal_cancelled';

    const PROMOTION            = 'promotion';
    const PROMOTION_PENALTY    = 'promotion_penalty';
    const PROMOTION_CLEAERANCE = 'promotion_clearance';

    const CAMPAIGN_PARTICIPATION = 'campaign_participation';
    const CAMPAIGN_PAYOUT        = 'campaign_payout';
    const CAMPAIGN_REFUND        = 'campaign_refund';
}
