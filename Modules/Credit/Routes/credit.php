<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        // Member Management
        Route::get('members/{id}/credits/adjustment', ['as' => 'admin.management.members.credits.adjustment.edit', 'uses' => 'Admin\Management\Adjustment\Member\CreditController@edit']);
        Route::put('members/{id}/credits/adjustment', ['as' => 'admin.management.members.credits.adjustment.update', 'uses' => 'Admin\Management\Adjustment\Member\CreditController@update']);
//        Route::get('members/{id}/credits/transfer', ['as' => 'admin.management.members.credits.transfer.edit', 'uses' => 'Admin\Management\Transfer\Member\CreditController@edit']);
//        Route::put('members/{id}/credits/transfer', ['as' => 'admin.management.members.credits.transfer.update', 'uses' => 'Admin\Management\Transfer\Member\CreditController@update']);

        // Admin Management
        Route::get('admins/{id}/credits/adjustment', ['as' => 'admin.management.admins.credits.adjustment.edit', 'uses' => 'Admin\Management\Adjustment\Admin\CreditController@edit']);
        Route::put('admins/{id}/credits/adjustment', ['as' => 'admin.management.admins.credits.adjustment.update', 'uses' => 'Admin\Management\Adjustment\Admin\CreditController@update']);
//        Route::get('admins/{id}/credits/transfer', ['as' => 'admin.management.admins.credits.transfer.edit', 'uses' => 'Admin\Management\Transfer\Admin\CreditController@edit']);
//        Route::put('admins/{id}/credits/transfer', ['as' => 'admin.management.admins.credits.transfer.update', 'uses' => 'Admin\Management\Transfer\Admin\CreditController@update']);

        //Admin System
        Route::group(['prefix' => 'credits'], function () {
            Route::group(['prefix' => 'system'], function () {
                Route::get('/adjustments/members', ['as' => 'admin.management.members.credits.adjustment.index', 'uses' => 'Admin\Management\Adjustment\Member\CreditController@index']);
                Route::get('/adjustments/members/export', ['as' => 'admin.management.members.credits.adjustment.export', 'uses' => 'Admin\Management\Adjustment\Member\CreditController@export']);

                Route::get('/adjustments/admins', ['as' => 'admin.management.admins.credits.adjustment.index', 'uses' => 'Admin\Management\Adjustment\Admin\CreditController@index']);
                Route::get('/adjustments/admins/export', ['as' => 'admin.management.admins.credits.adjustment.export', 'uses' => 'Admin\Management\Adjustment\Admin\CreditController@export']);

                Route::resource('capital-credit/export', 'Admin\System\CapitalCredit\CreditExportController', ['as' => 'admin.credits.system.capital-credit'])->only(['index']);
                Route::resource('capital-credit', 'Admin\System\CapitalCredit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);
                Route::get('capital-credit/breakdown', 'Admin\System\CapitalCredit\CreditController@breakdown')->name('admin.credits.statements.capital-credit.breakdown');

                Route::resource('rollover-credit/export', 'Admin\System\RollOverCredit\CreditExportController', ['as' => 'admin.credits.system.rollover-credit'])->only(['index']);
                Route::resource('rollover-credit', 'Admin\System\RollOverCredit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);

                Route::resource('cash-credit/export', 'Admin\System\CashCredit\CreditExportController', ['as' => 'admin.credits.system.cash-credit'])->only(['index']);
                Route::resource('cash-credit', 'Admin\System\CashCredit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);

                Route::resource('usdt-erc20-credit/export', 'Admin\System\UsdtErc20Credit\CreditExportController', ['as' => 'admin.credits.system.usdt-erc20-credit'])->only(['index']);
                Route::resource('usdt-erc20-credit', 'Admin\System\UsdtErc20Credit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);

                Route::resource('usdc-credit/export', 'Admin\System\UsdcCredit\CreditExportController', ['as' => 'admin.credits.system.usdc-credit'])->only(['index']);
                Route::resource('usdc-credit', 'Admin\System\UsdcCredit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);

                Route::resource('promotion-credit/export', 'Admin\System\PromotionCredit\CreditExportController', ['as' => 'admin.credits.system.promotion-credit'])->only(['index']);
                Route::resource('promotion-credit', 'Admin\System\PromotionCredit\CreditController', ['as' => 'admin.credits.system'])->only(['index']);

                Route::resource('transfer', 'Admin\System\MemberCreditTransferController', ['as' => 'admin.credits.system'])->only(['create', 'store']);
            });

            //Admin Statement
            Route::group(['prefix' => 'statement'], function () {
                Route::resource('capital-credit/export', 'Admin\Statement\CapitalCredit\CreditExportController', ['as' => 'admin.credits.statements.capital-credit'])->only(['index']);
                Route::resource('capital-credit', 'Admin\Statement\CapitalCredit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);

                Route::resource('rollover-credit/export', 'Admin\Statement\RollOverCredit\CreditExportController', ['as' => 'admin.credits.statements.rollover-credit'])->only(['index']);
                Route::resource('rollover-credit', 'Admin\Statement\RollOverCredit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);

                Route::resource('cash-credit/export', 'Admin\Statement\CashCredit\CreditExportController', ['as' => 'admin.credits.statements.cash-credit'])->only(['index']);
                Route::resource('cash-credit', 'Admin\Statement\CashCredit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);

                Route::resource('usdt-erc20-credit/export', 'Admin\Statement\UsdtErc20Credit\CreditExportController', ['as' => 'admin.credits.statements.usdt-erc20-credit'])->only(['index']);
                Route::resource('usdt-erc20-credit', 'Admin\Statement\UsdtErc20Credit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);

                Route::resource('usdc-credit/export', 'Admin\Statement\UsdcCredit\CreditExportController', ['as' => 'admin.credits.statements.usdc-credit'])->only(['index']);
                Route::resource('usdc-credit', 'Admin\Statement\UsdcCredit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);

                Route::resource('promotion-credit/export', 'Admin\Statement\PromotionCredit\CreditExportController', ['as' => 'admin.credits.statements.promotion-credit'])->only(['index']);
                Route::resource('promotion-credit', 'Admin\Statement\PromotionCredit\CreditController', ['as' => 'admin.credits.statements'])->only(['index']);
            });

            Route::group(['prefix' => 'report'], function () {
                Route::get('balance/export', ['as' => 'admin.report.balance.export', 'uses' => 'Admin\Report\BalanceExportController@index']);
                Route::resource('balance', 'Admin\Report\BalanceController', ['as' => 'admin.report'])->only(['index']);
            });

            Route::get('conversions/settings', ['as' => 'admin.conversions.settings.index', 'uses' => 'Admin\Management\Conversion\SettingController@index']);
            Route::put('conversions/settings/{type}', ['as' => 'admin.conversions.settings.update', 'uses' => 'Admin\Management\Conversion\SettingController@update'])->where('type', 'limit|penalty|service|admin_fee');
            Route::post('/conversions/bulk-update', ['as' => 'admin.conversions.bulk.update', 'uses' => 'Admin\Management\Conversion\ConversionController@bulkUpdate']);
            Route::get('conversion/export', ['as' => 'admin.credits.conversion.export', 'uses' => 'Admin\Management\Conversion\ConversionController@export']);
            Route::resource('conversions/invoices', 'Admin\CreditConversionInvoiceController', ['as' => 'admin.credits.conversions'])->only(['show']);
            Route::resource('conversion', 'Admin\Management\Conversion\ConversionController', ['as' => 'admin.credits'])->only(['index', 'show']);
        });
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::group(['prefix' => 'credits'], function () {
            Route::resource('transfer', 'Member\TransferController', ['as' => 'member.credits'])->only(['create', 'store']);

            Route::resource('capital-credit/export', 'Member\CapitalCredit\CreditExportController', ['as' => 'member.credits.statements.capital-credit'])->only(['index']);
            Route::resource('capital-credit', 'Member\CapitalCredit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('rollover-credit/export', 'Member\RollOverCredit\CreditExportController', ['as' => 'member.credits.statements.rollover-credit'])->only(['index']);
            Route::resource('rollover-credit', 'Member\RollOverCredit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('cash-credit/export', 'Member\CashCredit\CreditExportController', ['as' => 'member.credits.statements.cash-credit'])->only(['index']);
            Route::resource('cash-credit', 'Member\CashCredit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('usdt-erc20-credit/export', 'Member\UsdtErc20Credit\CreditExportController', ['as' => 'member.credits.statements.usdt-erc20-credit'])->only(['index']);
            Route::resource('usdt-erc20-credit', 'Member\UsdtErc20Credit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('usdc-credit/export', 'Member\UsdcCredit\CreditExportController', ['as' => 'member.credits.statements.usdc-credit'])->only(['index']);
            Route::resource('usdc-credit', 'Member\UsdcCredit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('promotion-credit/export', 'Member\PromotionCredit\CreditExportController', ['as' => 'member.credits.statements.promotion-credit'])->only(['index']);
            Route::resource('promotion-credit', 'Member\PromotionCredit\CreditController', ['as' => 'member.credits.statements'])->only(['index']);

            Route::resource('conversion', 'Member\ConversionController', ['as' => 'member.credits'])->only(['index', 'update', 'create', 'store']);
            Route::get('conversion/convertible-credit-types', ['as' => 'member.credits.conversion.convertible-credit-types', 'uses' => 'Member\ConversionController@getConvertibleCreditTypes']);
            Route::get('conversion/convert-credit-details', ['as' => 'member.credits.conversion.convert-credit-details', 'uses' => 'Member\ConversionController@getConvertCreditDetails']);
            Route::get('conversion/calculates', ['as' => 'member.credits.conversion.calculates', 'uses' => 'Member\ConversionController@calculate']);
            Route::get('conversion/export', ['as' => 'member.credits.conversion.export', 'uses' => 'Member\ConversionController@export']);
            Route::resource('conversions/invoices', 'Member\CreditConversionInvoiceController', ['as' => 'member.credits.conversions'])->only(['show']);
        });
    });
});
