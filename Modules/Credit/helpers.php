<?php

use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;

if (!function_exists('credit_precision')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function credit_precision($creditType = null)
    {
        switch ($creditType) {
            case BankCreditTypeContract::EXCHANGE_RATE:
                return config('credit.precision.exchange_rate');
            case BankCreditTypeContract::USDT_ERC20:
            case BankCreditTypeContract::USDC:
                return 2;
            case BankCreditTypeContract::USD_CREDIT:
            default:
                return config('credit.precision.default');
        }
    }
}

if (!function_exists('is_blockchain_credit')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function is_blockchain_credit($creditType)
    {
        switch ($creditType) {
            case BankCreditTypeContract::BTC:
            case BankCreditTypeContract::ETH:
            case BankCreditTypeContract::USDT_OMNI:
            case BankCreditTypeContract::USDT_ERC20:
            case BankCreditTypeContract::SCGC:
                return true;
            default:
                return false;
        }

        return false;
    }
}

if (!function_exists('trim_precision')) {
    /**
     * Trim a number to display precision set in the credits module config, or shorter if the remaining digits are zeroes.
     *
     * @param string $number
     * @return string
     */
    function trim_precision($number)
    {
        if (is_null($number)) {
            return $number;
        }
        $number = (string) $number;

        return trim_zero(bcmul($number, '1.' . str_repeat('0', credit_precision()), credit_precision()));
    }
}

if (!function_exists('trim_zero')) {
    /**
     * Utility function to remove extra zeroes from the right of a number.
     *
     * @param string $number
     * @param bool $decimalPoint
     * @return bool|string
     */
    function trim_zero(string $number, bool $decimalPoint = false)
    {
        if (false === strpos($number, '.')) {
            return $number;
        }

        $number = rtrim($number, '0');

        if ('.' == substr($number, -1)) {
            $number = $decimalPoint ? $number . '0' : substr($number, 0, strlen($number) - 1);
        }

        return $number;
    }
}

if (!function_exists('gt_zero')) {
    /**
     * Check if number is greater than zero.
     *
     * @param string $operand
     * @return bool
     */
    function gt_zero(string $operand)
    {
        return 1 == bccomp($operand, '0', credit_precision());
    }
}

if (!function_exists('is_zero')) {
    /**
     * Check if number is zero.
     *
     * @param string $operand
     * @return bool
     */
    function is_zero(string $operand)
    {
        return 0 == bccomp($operand, '0', credit_precision());
    }
}

if (!function_exists('display_transaction')) {
    /**
     * Replace transaction placeholders
     * @param unknown $statement
     */
    function display_transaction($statement)
    {
        $rolesRepository = resolve(\Modules\Rbac\Contracts\AssignRoleContract::class);
        $role            = is_null($statement->transfer)?"":$rolesRepository->findBySlug($statement->transfer->user->id)->role->name;
        if (in_array($statement->transaction->rawname, ['credit_transfer_from', 'credit_transfer_to'])) {
            $name = false === strpos($role, 'admin')? $statement->transfer->name : __('s_transfer.system');
            //is_numeric($statement->transfer->name) &&
            $transaction = __($statement->transaction->name, ['member_id' => $name, 'credit_type' => $statement->creditType->name]);
        } elseif (in_array($statement->transaction->rawname, ['credit_adjustment'])) {
            // @TODO: find a better way to do this that's not expensive
            $name        = is_numeric($statement->doneBy->name) && false === strpos($role, 'admin') ? $statement->doneBy->name : __('s_transfer.system');
            $transaction = __($statement->transaction->name, ['member_id' => $name]);
        } elseif (in_array($statement->transaction->rawname, ['referral_fee'])) {
            // @TODO: find a better way to do this that's not expensive
            $name        = ($statement->doneBy) ? $statement->doneBy->name : '';
            $transaction = __($statement->transaction->name, ['member_id' => $name]);
        } else {
            $transaction = $statement->transaction->name;
        }

        return $transaction;
    }
}

if (!function_exists('display_admin_transaction')) {
    /**
     * Replace transaction placeholders
     * @param unknown $statement
     */
    function display_admin_transaction($statement)
    {
        if (in_array($statement->transaction->rawname, ['credit_transfer_from', 'credit_transfer_to'])) {
            $name        = false === strpos($statement->transfer->name, 'sysadmin')? $statement->transfer->name : __('s_transfer.system');
            $transaction = __($statement->transaction->name, ['member_id' => $name, 'credit_type' => $statement->creditType->name]);
        } elseif (in_array($statement->transaction->rawname, ['credit_adjustment'])) {
            // @TODO: find a better way to do this that's not expensive
            $name        = false === strpos($statement->doneBy->name, 'sysadmin') ? $statement->doneBy->name : __('s_transfer.system');
            $transaction = __($statement->transaction->name, ['member_id' => $name]);
        } else {
            $transaction = $statement->transaction->name;
        }

        return $transaction;
    }
}

if (!function_exists('bank_credit_type_id')) {
    function bank_credit_type_id(string $name)
    {
        return Cache::tags('bank')
            ->rememberForever("creditType.$name", function () use ($name) {
                $bankCreditTypeRepo = resolve(BankCreditTypeContract::class);
                if ($bankCreditTypeRepo->findBySlug($name)) {
                    return $bankCreditTypeRepo->findBySlug($name)->id;
                }
                return;
            });
    }
}

if (!function_exists('bank_credit_type_name')) {
    /**
     * Get bank credit type name, given ID.
     *
     * @param int $bankCreditTypeId
     * @return mixed
     */
    function bank_credit_type_name(int $bankCreditTypeId)
    {
        return Cache::tags('bank')
            ->rememberForever("creditTypeId.$bankCreditTypeId", function () use ($bankCreditTypeId) {
                $bankCreditTypeService = resolve(BankCreditTypeContract::class);
                return $bankCreditTypeService->find($bankCreditTypeId)->getRawAttribute('name');
            });
    }
}

if (!function_exists('bank_transaction_type_id')) {
    function bank_transaction_type_id(string $name)
    {
        return Cache::tags('bank')
            ->rememberForever("transactionType.$name", function () use ($name) {
                $bankTransactionTypeRepo = resolve(BankTransactionTypeContract::class);
                return $bankTransactionTypeRepo->findBySlug($name)->id;
            });
    }
}

if (!function_exists('get_total_equity')) {
    function get_total_equity($exchangeRates, $capitalCredit, $rollOverCredit)
    {
        $convertedCapitalRate  = bcdiv($capitalCredit ?? 0, $exchangeRates[\Modules\Credit\Contracts\BankCreditTypeContract::CAPITAL_CREDIT], credit_precision());
        $convertedRolloverRate = bcdiv($rollOverCredit ?? 0, $exchangeRates[\Modules\Credit\Contracts\BankCreditTypeContract::ROLL_OVER_CREDIT], credit_precision());
        $totalEquity           = bcadd($convertedCapitalRate, $convertedRolloverRate, credit_precision());
        return $totalEquity ?? 0;
    }
}

if (!function_exists('convertable_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function convertable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        $bankCreditType = $bankCreditTypeRepository->getModel()->where('can_convert', 1)->get()->dropDown('name', 'id');

        if (!auth()->user()->can('capital_credit_convert') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::CAPITAL_CREDIT)->id]);
        }
        if (!auth()->user()->can('rollover_credit_convert') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::ROLL_OVER_CREDIT)->id]);
        }
        if (!auth()->user()->can('cash_credit_convert') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::CASH_CREDIT)->id]);
        }
        if (!auth()->user()->can('usdt_erc20_convert') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDT_ERC20)->id]);
        }
        if (!auth()->user()->can('usdc_convert') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDC)->id]);
        }

        return $bankCreditType;
    }
}

if (!function_exists('conversion_statuses_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function conversion_statuses_dropdown($exclusion = [])
    {
        $conversionStatusRepo = resolve(CreditConversionStatusContract::class);

        $conversionStatuses = $conversionStatusRepo->getModel()
            ->where('is_active', 1)
            ->whereNotIn('name', $exclusion)
            ->get()
            ->dropDown('name', 'id');

        return $conversionStatuses;
    }
}

if (!function_exists('transferable_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function transferable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        $bankCreditType = $bankCreditTypeRepository->getModel()->where('can_transfer', 1)->get()->dropDown('name', 'id');

        if (!auth()->user()->can('member_transfer_usdt_erc20') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDT_ERC20)->id]);
        }

        if (!auth()->user()->can('member_transfer_usdc') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDC)->id]);
        }

        return $bankCreditType;
    }
}

if (!function_exists('blockchain_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function blockchain_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);
        $ethCreditType            = $bankCreditTypeRepository->findBySlug('eth');

        $bankCreditType = $bankCreditTypeRepository->getModel()
            ->whereNotNull('network')
            ->whereNotIn('id', [$ethCreditType->id])
            ->get()
            ->dropDown('name', 'rawname');

        return $bankCreditType;
    }
}

if (!function_exists('get_system_credit')) {
    function get_system_credit($creditTypeId)
    {
        $bankAccountCredit = resolve(\Modules\Credit\Contracts\BankAccountCreditContract::class);
        return $bankAccountCredit->getSystemBalance($creditTypeId);
    }
}

if (!function_exists('get_system_all_credits')) {
    function get_system_all_credits($creditTypeIds)
    {
        $bankAccountCredit = resolve(\Modules\Credit\Contracts\BankAccountCreditContract::class);
        return $bankAccountCredit->getSystemAllBalance($creditTypeIds);
    }
}

if (!function_exists('penalty_duration_dropdown')) {
    function penalty_duration_dropdown()
    {
        return [
            0  => __('a_penalty_dropdown.no penalty'),
            30 => __('a_penalty_dropdown.30 days penalty'),
            90 => __('a_penalty_dropdown.90 days penalty'),
        ];
    }
}

if (!function_exists('withdrawable_blockchain_credit_types_dropwdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function withdrawable_blockchain_credit_types_dropwdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        $bankCreditTypes = $bankCreditTypeRepository->getModel()
            ->where('can_withdraw', 1)
            ->whereNotNull('network')
            ->get();

        $blockchainCreditType    = [];
        $blockchainCreditType[0] = __('m_wallet_withdrawals_create.select credit type');
        foreach ($bankCreditTypes as $bankCreditType) {
            $blockchainCreditType[$bankCreditType->rawname] = $bankCreditType->name;
        }

        return $blockchainCreditType;
    }
}
