<?php

namespace Modules\Credit\Exceptions;

class NotEnoughCredit extends \Exception
{
    /**
     * Current credit
     * @var unknown
     */
    protected $currentCredit;

    /**
     * The amount to deduct
     * @var unknown
     */
    protected $deduction;

    public function __construct($message = null, $currentCredit = 0, $deduction = 0, $code = null, $previous = null)
    {
        $this->currentCredit = $currentCredit;
        $this->deduction     = $deduction;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get current credit
     * @return \SCM\Credit\Exceptions\unknown
     */
    public function getCurrentCredit()
    {
        return $this->currentCredit;
    }

    /**
     * Get deduction
     * @return \SCM\Credit\Exceptions\unknown
     */
    public function getDeduction()
    {
        return $this->deduction;
    }
}
