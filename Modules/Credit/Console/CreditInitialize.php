<?php

namespace Modules\Credit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Translation\Contracts\TranslatorTranslationContract;

class CreditInitialize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credits:init';

    /**
     * Bank Credit Type Instance
     * @var unknown
     */
    protected $creditTypeRepository;

    /**
     * Bank Transaction Type Instance
     * @var unknown
     */
    protected $transactionTypeRepository;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize credit system default settings.';

    /**
     * The translation repository
     * @var unknown
     */
    protected $translationRepository;

    /**
     * Create a new command instance.
     *
     * @param BankCreditTypeContract $creditTypeContract
     * @param BankTransactionTypeContract $transactionTypeContract
     * @param TranslatorTranslationContract $translationContract
     * @return void
     */
    public function __construct(
        BankCreditTypeContract $creditTypeContract,
        BankTransactionTypeContract $transactionTypeContract,
        TranslatorTranslationContract $translationContract
    ) {
        $this->creditTypeRepository      = $creditTypeContract;
        $this->transactionTypeRepository = $transactionTypeContract;
        $this->translationRepository     = $translationContract;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        try {
            // set default credit types
            $defaultCredits        = config('credit.credit_types');
            $depositTypes          = config('credit.deposit_credit_types');
            $withdrawalTypes       = config('credit.withdrawal_credit_types');
            $creditNetworks        = config('credit.credit_network');
            $locale                = config('app.locale');
            $creditTypePageId      = get_translator_page('s_bank_credit_types')->id;
            $transactionTypePageId = get_translator_page('s_bank_transaction_types')->id;

            if ($defaultCredits) {
                foreach ($defaultCredits as $slug => $name) {
                    $row = $this->creditTypeRepository->add(['name' => $slug, 'is_active' => 1]);

                    if (array_key_exists($slug, $depositTypes)) {
                        $this->creditTypeRepository->editBySlug($slug, ['can_deposit' => true, 'deposit_credit_type_id' => $this->creditTypeRepository->findBySlug($depositTypes[$slug])->id]);
                    }

                    if (array_key_exists($slug, $withdrawalTypes)) {
                        $this->creditTypeRepository->editBySlug($slug, ['can_withdraw' => true, 'withdrawal_credit_type_id' => $this->creditTypeRepository->findBySlug($withdrawalTypes[$slug])->id]);
                    }

                    if (array_key_exists($slug, $creditNetworks)) {
                        $this->creditTypeRepository->editBySlug($slug, ['network' => $creditNetworks[$slug]]);
                    }

                    $trans = $this->translationRepository->findByKey($locale, $creditTypePageId, $row->name_translation);
                    if ($trans) {
                        $data                = $trans->toArray();
                        $data['language_id'] = $data['translator_language_id'];
                        $data['value']       = ucwords($name);
                        $this->translationRepository->updateTranslation($data);
                    }
                }
            }

            // set default transaction types
            $defaultTransactions = config('credit.transaction_types');
            if ($defaultTransactions) {
                foreach ($defaultTransactions as $slug => $name) {
                    $row   = $this->transactionTypeRepository->add(['name' => $slug, 'is_active' => 1]);
                    $trans = $this->translationRepository->findByKey($locale, $transactionTypePageId, $row->name_translation);
                    if ($trans) {
                        $data                = $trans->toArray();
                        $data['language_id'] = $data['translator_language_id'];
                        $data['value']       = ucwords($name);
                        $this->translationRepository->updateTranslation($data);
                    }
                }
            }

            DB::commit();

            $this->info('Credits initialized.');
        } catch (\Exception $e) {
            \Log::info($e);
            $this->error($e->getMessage());
            DB::rollback();
        }
    }
}
