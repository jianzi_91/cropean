<?php

namespace Modules\Credit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;

class BulkConversionApprovalCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'conversions:bulk-approve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Approve pending conversions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $conversionRepo = resolve(CreditConversionContract::class);
        $statusRepo     = resolve(CreditConversionStatusContract::class);

        #[STEP 1] Get All pending conversions
        $pendingStatus  = $statusRepo->findBySlug(CreditConversionStatusContract::PENDING);
        $approvedStatus = $statusRepo->findBySlug(CreditConversionStatusContract::APPROVED);
        $conversions    = $conversionRepo->getModel()
            ->where('credit_conversion_status_id', $pendingStatus->id)
            ->get();

        DB::beginTransaction();

        try {
            #[STEP 2] Approve all requests
            foreach ($conversions as $conversion) {
                $conversion = $conversionRepo->approve($conversion->id);
                $statusRepo->changeHistory($conversion, $approvedStatus);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e);
        }
    }
}
