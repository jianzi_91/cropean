<?php

namespace Modules\Credit\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Credit\Models\BankAccountCredit;
use Modules\Credit\Models\BankAccountCreditSnapshot;

class BankAccountCreditSnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'snapshot:bank_account_credits {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the user credit amount.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $now = new Carbon($this->argument('date'));

        $date = $now->copy()->subDay()->endOfDay()->toDateString();

        if (BankAccountCreditSnapshot::where('run_date', $date)->exists()) {
            $this->error('Bank account credit snapshot already exist');
            exit;
        }

        $history = BankAccountCredit::get()->toArray();

        foreach ($history as $record) {
            $snapshot = array_merge($record, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'run_date' => $date]);
            BankAccountCreditSnapshot::insert($snapshot);
        }
    }
}
