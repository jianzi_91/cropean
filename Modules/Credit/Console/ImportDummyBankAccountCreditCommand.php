<?php

namespace Modules\Credit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;

class ImportDummyBankAccountCreditCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'import:credits {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import dummy bank account credit.';

    /**
     * Execute the console command.
     * @param BankAccountCreditContract $bankAccountCreditContract
     * @return mixed
     */
    public function handle(BankAccountCreditContract $bankAccountCreditContract)
    {
        if (app()->environment() == 'production') {
            $this->line('This command unable to process in production environment');
            exit;
        }

        $file = fopen(storage_path('test') . '/' . $this->argument('file'), 'r');

        $firstLine = true;
        $users     = [];
        while (!feof($file)) {
            $data = fgetcsv($file);

            if ($firstLine) {
                $firstLine = false;
            } else {
                $users[] = (object) [
                    'id'              => $data[0],
                    'capital_credit'  => empty($data[1]) ? 0 :$data[1],
                    'rollover_credit' => empty($data[2]) ? 0 :$data[2],
                    'cash_credit'     => empty($data[3]) ? 0 :$data[3],
                ];
            }
        }

        fclose($file);

        DB::beginTransaction();

        try {
            foreach ($users as $user) {
                if (isset($user->capital_credit) && $user->capital_credit > 0) {
                    $this->line("inserted user id {$user->id} capital credit : {$user->capital_credit}");
                    $bankAccountCreditContract->add(BankCreditTypeContract::CAPITAL_CREDIT, $user->capital_credit, $user->id, BankTransactionTypeContract::DEPOSIT, null, $user->id);
                }
                if (isset($user->rollover_credit) && $user->rollover_credit > 0) {
                    $this->line("inserted user id {$user->id} rollover credit : {$user->rollover_credit}");
                    $bankAccountCreditContract->add(BankCreditTypeContract::ROLL_OVER_CREDIT, $user->rollover_credit, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                }
                if (isset($user->cash_credit) && $user->cash_credit > 0) {
                    $this->line("inserted user id {$user->id} cash credit : {$user->cash_credit}");
                    $bankAccountCreditContract->add(BankCreditTypeContract::CASH_CREDIT, $user->cash_credit, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('end upload bank account credit');
    }
}
