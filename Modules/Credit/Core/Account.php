<?php

namespace Modules\Credit\Core;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Contracts\AdjustmentContract;
use Modules\Credit\Contracts\TransferContract;
use Modules\Credit\Exceptions\NotEnoughCredit;
use Modules\Credit\Models\BankAccountCredit;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Traits\Statement;
use Modules\Credit\Traits\ValidateAccount;
use Modules\Credit\Traits\ValidateCredit;

class Account implements AdjustmentContract, TransferContract
{
    use Statement, ValidateAccount, ValidateCredit;

    /**
     * Generator instance
     * @var unknown
     */
    protected $generator;

    /**
     * Bank Transaction Type instance
     * @var unknown
     */
    protected $statement;

    /**
     * Bank account credit model.
     *
     * @var BankAccountCredit
     */
    protected $credit;

    /**
     * Class constructor
     * @param BankAccountStatement $statement
     */
    public function __construct(BankAccountStatement $statement, BankAccountCredit $credit)
    {
        $this->statement = $statement;
        $this->credit    = $credit;
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\AdjustmentContract::add()
     */
    public function add($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $accountIdTo = null)
    {
        $creditTypeId      = $this->validateCreditType($creditType);
        $accountId         = $this->validateAccount($target);
        $transactionTypeId = $this->validateTransactionType($transactionType);
        if ($accountIdTo) {
            $accountIdTo = $this->validateAccount($accountIdTo);
        }

        if ($doneBy) {
            $doneBy = $this->validateAccount($doneBy);
        }

        return $this->log($creditTypeId, $amount, 0, $accountId, $accountIdTo, $transactionTypeId, $remarks, null, $doneBy, null, $transactionDate);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\AdjustmentContract::deduct()
     */
    public function deduct($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $accountIdTo = null)
    {
        $creditTypeId      = $this->validateCreditType($creditType);
        $accountId         = $this->validateAccount($target);
        $transactionTypeId = $this->validateTransactionType($transactionType);
        $this->validateBalance($creditTypeId, $accountId, $amount);

        if ($accountIdTo) {
            $accountIdTo = $this->validateAccount($accountIdTo);
        }

        if ($doneBy) {
            $doneBy = $this->validateAccount($doneBy);
        }

        return $this->log($creditTypeId, 0, $amount, $accountId, $accountIdTo, $transactionTypeId, $remarks, null, $doneBy, null, $transactionDate);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\TransferContract::transfer()
     */
    public function transfer($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null)
    {
        $txnTypeFrom = $this->getTransactionTransfer();
        $txnTypeTo   = $this->getTransactionTransfer(false);

        $txnTypeFromId = $this->validateTransactionType($txnTypeFrom);
        $txnTypeToId   = $this->validateTransactionType($txnTypeTo);

        $creditTypeId  = $this->validateCreditType($creditType);
        $accountIdFrom = $this->validateAccount($source);
        $accountIdTo   = $this->validateAccount($target);

        if ($doneBy) {
            $doneBy = $this->validateAccount($doneBy);
        }

        $this->validateBalance($creditTypeId, $accountIdFrom, $amount);

        $fromTxnCode = $this->log($creditTypeId, 0, $amount, $accountIdFrom, $accountIdTo, $txnTypeFromId, $remarks, null, $doneBy, null, $transactionDate);
        $toTxnCode   = $this->log($creditTypeId, $amount, 0, $accountIdTo, $accountIdFrom, $txnTypeToId, $remarks, null, $doneBy, null, $transactionDate);

        return [$fromTxnCode, $toTxnCode];
    }

    /**
     * Get Bank credit statement table
     * @return statement
     */
    public function getModel()
    {
        return $this->statement->newInstance();
    }

    /**
     * Get bank account credit model instance.
     * @return Model
     */
    public function getBankAccountCreditModel()
    {
        return $this->credit->newInstance();
    }

    /**
     * Get generator instance
     * @return Object
     */
    public function getGenerator()
    {
        if (!$this->generator) {
            $class           = config('credit.transaction_code.generator', SimpleGenerator::class);
            $this->generator = $class::make();
        }
        return $this->generator;
    }

    /**
     * Transfer transaction types
     * @param string $fromTo
     * @return mixed|\Illuminate\Foundation\Application
     */
    public function getTransactionTransfer($fromTo = true)
    {
        return $fromTo ? config('credit.transfer.from', 'credit_transfer_from') : config('credit.transfer.to', 'credit_transfer_to');
    }

    /**
     * Validate credit
     * @param unknown $creditTypeId
     * @param unknown $source
     * @param unknown $amount
     * @throws InvalidBank
     * @throws InvalidBankCredit
     * @throws NotEnoughCredit
     * @return boolean
     */
    protected function validateBalance($creditTypeId, $accountId, $amount)
    {
        $balance = $this->getFinalCredit($creditTypeId, $accountId);
        if ($balance < $amount) {
            throw new NotEnoughCredit('Not enough credit', $balance, $amount);
        }

        return true;
    }
}
