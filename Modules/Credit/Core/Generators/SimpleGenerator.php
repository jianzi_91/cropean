<?php

namespace Modules\Credit\Core\Generators;

use Modules\Credit\Contracts\TransactionCodeContract;
use Modules\Credit\Traits\MakeInstance;

class SimpleGenerator implements TransactionCodeContract
{
    use MakeInstance;

    /**
     * Generate transaction code
     * @param string $prefix
     */
    public function generate($prefix = 'TXN', $length = 20)
    {
        $code      = $prefix . str_replace('.', '', microtime(true));
        $strLength = strlen($code);
        if ($strLength < $length) {
            $start = str_pad(1, ($length - $strLength), 1);
            $limit = str_pad(9, ($length - $strLength), 9);

            if ($limit > mt_getrandmax()) {
                $start = 1;
                $limit = mt_getrandmax();
            }

            $code .= mt_rand(intval($start), intval($limit));
        }

        return substr($code, 0, $length);
    }
}
