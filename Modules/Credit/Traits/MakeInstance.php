<?php

namespace Modules\Credit\Traits;

trait MakeInstance
{
    /**
     * Create instance of self
     * @return self
     */
    public static function make()
    {
        return new static();
    }
}
