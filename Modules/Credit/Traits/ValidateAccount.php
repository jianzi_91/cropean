<?php

namespace Modules\Credit\Traits;

use Illuminate\Support\Facades\Redis;
use Modules\Credit\Exceptions\DuplicateBankAccount;
use Modules\Credit\Exceptions\InvalidBankAccount;
use Modules\Credit\Models\BankAccount;

trait ValidateAccount
{
    /**
     * Validates Bank Account if exists
     * @param unknown $reference
     * @throws InvalidCreditHolder
     * @return boolean
     */
    public function validateAccount($reference, $cached = true)
    {
        if ($reference instanceof BankAccount) {
            return $reference->id;
        }

        if ($this->isCached($reference) && $cached) {
            $account = $this->getFromCache($reference);
        } else {
            $account = BankAccount::where('reference_id', $reference)->select(['id', 'reference_type', 'reference_id', 'account_number', 'name'])->first();
            $this->putIntoCache($reference, $account);
        }

        if (!$account) {
            throw new InvalidBankAccount('Invalid Bank Account (' . $reference . ')');
        }

        return $account->id;
    }

    /**
     * Validates Bank Account by Account Number if exists
     * @param unknown $accountNumber
     * @param string $reference
     * @throws InvalidBankAccount
     * @return unknown
     */
    public function validateAccountByAccountNumber($accountNumber, $reference = false)
    {
        if ($accountNumber instanceof BankAccount) {
            return $accountNumber->id;
        }

        $account = BankAccount::where('account_number', $accountNumber)->lockForUpdate()->first();
        if (!$account) {
            throw new InvalidBankAccount('Invalid Bank Account (' . $reference . ')');
        }

        return $reference? $account->reference_id : $account->id;
    }

    /**
     * Validate duplicate Bank Account
     * @param unknown $holder
     * @throws DuplicateHolder
     * @return boolean
     */
    public function validateDuplicate($referenceId)
    {
        if (BankAccount::where('reference_id', $referenceId)->exists()) {
            throw new DuplicateBankAccount('Duplicate Bank Account (' . $referenceId . ')');
        }

        return true;
    }

    /**
     * Put data to cache
     * @param unknown $data
     */
    public function putIntoCache($reference, $data)
    {
        Redis::hdel('bank_account', $reference);
        Redis::hset('bank_account', $reference, $data);
    }

    /**
     * Get cached data
     */
    public function getFromCache($reference)
    {
        return json_decode(Redis::hget('bank_account', $reference));
    }

    /**
     * Check whether the navigations is already cached
     * @return boolean
     */
    public function isCached($reference)
    {
        return Redis::hexists('bank_account', $reference);
    }
}
