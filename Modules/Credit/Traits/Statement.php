<?php

namespace Modules\Credit\Traits;

use Carbon\Carbon;
use Modules\Credit\Contracts\BankCreditTypeContract;

trait Statement
{
    /**
     * Log Credit Statement
     * @param unknown $creditId
     * @param unknown $credit
     * @param unknown $debit
     * @param unknown $source
     * @param unknown $transfer
     * @param unknown $transactionTypeId
     * @param unknown $remarks
     * @param unknown $txnCode
     * @param unknown $doneBy
     * @param unknown $bankId
     * @param Carbon $date
     * @throws \Exception
     * @return string
     */
    public function log($creditId, $credit, $debit, $source, $transfer, $transactionTypeId, $remarks = null, $txnCode = null, $doneBy = null, $bankId = null, Carbon $date = null)
    {
        $config    = config('credit.transaction_code');
        $statement = $this->getModel();

        if (!$txnCode) {
            $generator = $this->getGenerator();
            $txnCode   = $generator->generate($config['prefix'], $config['length']);
        }

        $statement->bank_credit_type_id = $creditId;
        if ($bankId) {
            $statement->bank_id         = $bankId;
            $statement->bank_account_id = $source;
        } else {
            $statement->bank_account_id     = $source;
            $statement->transfer_account_id = $transfer;
            $statement->done_by             = $doneBy ? $doneBy : $source;
        }

        $statement->transaction_date = new Carbon();
        if ($date) {
            $statement->transaction_date = $date;
        }

        $statement->bank_transaction_type_id = $transactionTypeId;
        $statement->remarks                  = $remarks;
        $statement->transaction_code         = $txnCode;

        $bankCreditType = resolve(BankCreditTypeContract::class);
        $creditType     = $bankCreditType->find($creditId);
        $precision      = credit_precision($creditType->rawname);

        // Its important to set the decimal precision for the signature
        $statement->credit = bcadd($credit, 0, $precision);
        $statement->debit  = bcadd($debit, 0, $precision);

        if ($date) {
            $statement->transaction_date = $date;
            $statement->created_at       = $date;
            $statement->updated_at       = $date;
        }

        //dd($statement);
        if (!$statement->save()) {
            throw new \Exception('Unable to save statement to database');
        }

        return $txnCode;
    }

    /**
     * Get model instance
     */
    abstract public function getModel();

    /**
     * Get bank account credit model.
     * @return mixed
     */
    abstract public function getBankAccountCreditModel();

    /**
     * Get generator instance
     */
    abstract public function getGenerator();

    /**
     * Get Bank Account final credit
     * @param unknown $creditTypeId
     * @param unknown $bankId
     * @param string $order
     * @return unknown
     */
    public function getFinalCredit($creditTypeId, $accountId)
    {
        $statement = $this->getBankAccountCreditModel()
                    ->where('bank_credit_type_id', $creditTypeId)
                    ->where('bank_account_id', $accountId)
                    ->first(['balance']);

        return $statement && !is_null($statement->balance) ? $statement->balance : 0;
    }
}
