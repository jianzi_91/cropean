<?php

namespace Modules\Credit\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redis;
use Modules\Credit\Exceptions\InvalidBankCredit;
use Modules\Credit\Exceptions\InvalidCreditType;
use Modules\Credit\Exceptions\InvalidTransactionType;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Models\BankTransactionType;

trait ValidateCredit
{
    /**
     * Validate credit type
     * @param unknown $creditType
     * @throws InvalidCreditType
     * @return integer
     */
    public function validateCreditType($creditType)
    {
        $type = $this->validate(BankCreditType::query(), $creditType);
        if (!$type) {
            throw new InvalidCreditType('Invalid Credit Type (' . $creditType . ')');
        }

        return $type->id;
    }

    /**
     * Validate Bank Credit
     * @param unknown $credit
     * @throws InvalidBankCredit
     * @return integer
     */
    public function validateBankCredit($credit)
    {
        $credit = $this->validate(BankCredit::query(), $credit);
        if (!$credit) {
            throw new InvalidBankCredit('Bank credit not found');
        }

        return $credit->id;
    }

    /**
     * Validate transaction type
     * @param unknown $type
     * @throws InvalidTransactionType
     * @return integer
     */
    public function validateTransactionType($type)
    {
        $txnType = $this->validate(BankTransactionType::query(), $type);
        if (!$txnType) {
            throw new InvalidTransactionType('Invalid transaction type (' . $type . ')');
        }

        return $txnType->id;
    }

    /**
     * Put data to cache
     * @param unknown $data
     */
    public function put($name, $reference, $data)
    {
        Redis::hdel($name, $reference);
        Redis::hset($name, $reference, $data);
    }

    /**
     * Get cached data
     */
    public function get($name, $reference)
    {
        return json_decode(Redis::hget($name, $reference));
    }

    /**
     * Check whether the navigations is already cached
     * @return boolean
     */
    public function cached($name, $reference)
    {
        return Redis::hexists($name, $reference);
    }

    /**
     * Generic validator
     * @param Builder $model
     * @param unknown $value
     * @param string $col
     * @return Model
     */
    protected function validate(Builder $builder, $value, $col = 'name')
    {
        $cacheName = $builder->getModel()->getTable();

        if (!is_numeric($value)) {
            if ($this->cached($cacheName, $value)) {
                return  $this->get($builder->getModel()->getTable(), $value);
            }
            $data = $builder->where($col, $value)->first();
            $this->put($cacheName, $value, $data);
            return $data;
        }
        // This must be a PK id
        return $builder->find($value);
    }
}
