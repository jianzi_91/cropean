<?php

namespace Modules\Credit\Http\Requests\Admin\System\Transfer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Credit\Http\Rules\ValidMemberTransferCreditAmount;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\Tree\Http\Rules\SponsorRelation;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Rules\UserIsActive;
use Modules\User\Http\Rules\UserIsMember;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $transferFrom = resolve(UserContract::class)->findByIdentifier($this->transfer_from);

        $transferTo = resolve(UserContract::class)->findByIdentifier($this->transfer_to);

        $credits = transferable_credit_types_dropdown();

        unset($credits['']);

        if (!auth()->user()->can('admin_system_transfer_usdt_credit')) {
            unset($credits[BankCreditTypeContract::USDT]);
        }

        $rules = [
            'transfer_from' => [
                'bail',
                'required',
                new UserIsMember(),
                new SponsorExist(),
                new UserIsActive()
            ],
            'transfer_to' => [
                'bail',
                'required',
                new UserIsMember(),
                new SponsorExist(),
                new UserIsActive()
            ],
            'credit_type' => [
                'required',
                'exists:bank_credit_types,name',
                Rule::in(array_keys($credits))
            ],
            'remarks' => [
                'nullable',
                'max:65535'
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                new ValidMemberTransferCreditAmount($this->credit_type),
                new NotScientificNotation(),
                new MaxDecimalPlaces(credit_precision($this->credit_type)),
            ],
            'secondary_password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                new SecondaryPasswordMatch()
            ],
        ];

        if ($transferFrom && $transferTo) {
            $rules['transfer_from'] = array_merge($rules['transfer_from'], [new SponsorRelation($transferTo->member_id)]);
            $rules['transfer_to']   = array_merge($rules['transfer_to'], [new SponsorRelation($transferFrom->member_id), Rule::notIn([$transferFrom->member_id, $transferFrom->username])]);
            $rules['amount']        = array_merge($rules['amount'], [new EnoughCredit($this->credit_type, $transferFrom->id)]);
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'transfer_from'      => __('a_member_transfer_credit.transfer from'),
            'transfer_to'        => __('a_member_transfer_credit.transfer to'),
            'credit_type'        => __('a_member_transfer_credit.credit type'),
            'amount'             => __('a_member_transfer_credit.amount'),
            'remarks'            => __('a_member_transfer_credit.remarks'),
            'secondary_password' => __('a_member_transfer_credit.secondary password'),
        ];
    }
}
