<?php

namespace Modules\Credit\Http\Requests\Admin\System\RewardCredit\Transfer;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Tree\Http\Rules\SponsorRelation;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Rules\UserIsActive;

class Store extends FormRequest
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $UserContract $userContract
     */
    public function __construct()
    {
        $this->userRepository = resolve(UserContract::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fromUser = $this->userRepository->findByIdentifier($this->from_member_id);

        $rules = [
            'from_member_id' => [
                'bail',
                'required',
                new SponsorRelation($this->to_member_id),
                new UserIsActive()
            ],
            'to_member_id' => [
                'bail',
                'required',
                new SponsorRelation($this->from_member_id),
                new UserIsActive()
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                'max:' . config('credit.transfers.max'),
                new NotScientificNotation(),
                new MaxDecimalPlaces(),

            ],
            'remarks' => [
                'max:255'
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];

        if ($fromUser) {
            $rules['amount'][] = new EnoughCredit(BankCreditTypeContract::USD_CREDIT, $fromUser->id);
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('a_credit_management_credit_transfer.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'from_member_id'     => __('a_credit_management_credit_transfer.from username/member id'),
            'to_member_id'       => __('a_credit_management_credit_transfer.to username/member id'),
            'amount'             => __('a_credit_management_credit_transfer.amount'),
            'remarks'            => __('a_credit_management_credit_transfer.credit remarks'),
            'secondary_password' => __('a_credit_management_credit_transfer.secondary password')
        ];
    }
}
