<?php

namespace Modules\Credit\Http\Requests\Admin\Statement\RewardCredit\Transfer;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Tree\Http\Rules\SponsorRelation;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Rules\UserIsActive;
use Modules\User\Http\Rules\UserIsMember;

class Store extends FormRequest
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $UserContract $userContract
     */
    public function __construct()
    {
        $this->userRepository = resolve(UserContract::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$fromUser = $this->userRepository->findByIdentifier($this->from_member_id);
        $user = auth()->user();

        $rules = [
            /*'from_member_id' => [
                'bail',
                'required',
                new SponsorRelation($this->to_member_id, false),
                new UserIsActive()
            ],*/
            'to_member_id' => [
                'bail',
                'required',
                //new SponsorRelation($this->from_member_id, false),
                new UserIsActive(),
                new UserIsMember()
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                'max:' . config('credit.transfers.max'),
                new NotScientificNotation(),
                new MaxDecimalPlaces(),
                new EnoughCredit(BankCreditTypeContract::USD_CREDIT, $user->id),

            ],
            'remarks' => [
                'max:255'
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('a_credit_statement_credit_transfer.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'from_member_id'     => __('a_credit_statement_credit_transfer.from username/member id'),
            'to_member_id'       => __('a_credit_statement_credit_transfer.to username/member id'),
            'amount'             => __('a_credit_statement_credit_transfer.amount'),
            'remarks'            => __('a_credit_statement_credit_transfer.credit remarks'),
            'secondary_password' => __('a_credit_statement_credit_transfer.secondary password')
        ];
    }
}
