<?php

namespace Modules\Credit\Http\Requests\Admin\Management\Conversion\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Contracts\CreditConversionStatusContract;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $statusRepo = resolve(CreditConversionStatusContract::class);
        $rejectedStatus = $statusRepo->findBySlug(CreditConversionStatusContract::REJECTED);

        return [
            'status_id' => 'bail|required|exists:credit_conversion_statuses,id',
            'remarks' => 'bail|required_if:status_id,' . $rejectedStatus->id,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'status_id' => __('a_conversion_details.status'),
            'remarks'   => __('a_conversion_details.remarks'),
        ];
    }

    public function messages()
    {
        return [
            'remarks.required_if' => __('a_conversion_details.Remarks if required when rejecting conversion request'),
        ];
    }
}
