<?php

namespace Modules\Credit\Http\Requests\Admin\Management\Conversion\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Plus65\Base\Rules\CheckOwnership;

class UpdateSetting extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->type) {
            case "limit":
                return [
                    'minimum.*.*' => [
                        'bail',
                        'required',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                    'multiple.*.*' => [
                        'bail',
                        'required',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                    'admin_fee.*.*' => [
                        'bail',
                        'required',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                ];
                break;
            case "penalty":
                return [
                    'penalty_percentage_30' => [
                        'required',
                        'numeric',
                        'gte: 0',
                        new MaxDecimalPlaces(2),
                    ],
                    'penalty_percentage_90' => [
                        'required',
                        'numeric',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                ];
                break;
            case "service":
                return [
                    'credit_conversion_roll_over_credit_admin_fee_percentage' => [
                        'required',
                        'numeric',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                ];
                break;
            case "admin_fee":
                return [
                    'credit_conversion_usdt_erc20_admin_fee_percentage' => [
                        'required',
                        'numeric',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                    'credit_conversion_usdc_admin_fee_percentage' => [
                        'required',
                        'numeric',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                    'credit_conversion_cash_credit_admin_fee_percentage' => [
                        'required',
                        'numeric',
                        'gte:0',
                        new MaxDecimalPlaces(2),
                    ],
                ];
                break;
            default:
                break;
        }
        return [];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return  [
            'minimum.*.*'                                        => __('a_credit_convert_settings.minimum amount'),
            'multiple.*.*'                                   => __('a_credit_convert_settings.multiple'),
            'admin_fee.*.*'                                   => __('a_credit_convert_settings.admin fee percentage'),
            'penalty_percentage_30'                                   => __('a_credit_convert_settings.penalty fee percentage'),
            'penalty_percentage_90'                                   => __('a_credit_convert_settings.penalty fee percentage'),
            'credit_conversion_roll_over_credit_admin_fee_percentage' => __('a_credit_convert_settings.service fee percentage'),
        ];
    }
}
