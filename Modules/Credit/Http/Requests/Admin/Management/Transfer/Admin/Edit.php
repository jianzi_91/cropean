<?php

namespace Modules\Credit\Http\Requests\Admin\Management\Transfer\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Rules\CheckOwnership;

class Edit extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->userColumn = 'id';
        $user             = resolve(UserContract::class)->find($this->id);

        return !$this->isOwner($user, $this->user(), true) && $user->is_admin;
    }
}
