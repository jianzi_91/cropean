<?php

namespace Modules\Credit\Http\Requests\Admin\Management\Transfer\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\EnoughCreditFromPersonToTransfer;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Credit\Http\Rules\NotZero;
use Modules\Credit\Http\Rules\ValidAdminTransferCreditAmount;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Rules\CheckOwnership;

class Transfer extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        return [
            'credit_type' => [
                'required',
                'exists:bank_credit_types,name'
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                new NotZero(),
                new NotScientificNotation(),
                new MaxDecimalPlaces(credit_precision($this->credit_type)),
                new EnoughCredit($this->credit_type, $user->id),
                new EnoughCreditFromPersonToTransfer($this->credit_type, $this->id),
                new ValidAdminTransferCreditAmount($this->credit_type),
            ],
            'remarks' => [
                'required',
                'max:' . config('credit.remarks.max')
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->userColumn = 'id';
        $user             = resolve(UserContract::class)->find($this->id);

        return !$this->isOwner($user, $this->user(), true) && $user->is_admin;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('a_admin_management_adjust_credit.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'credit_type'        => __('a_admin_management_transfer_credit.credit type'),
            'amount'             => __('a_admin_management_transfer_credit.credit amount'),
            'remarks'            => __('a_admin_management_transfer_credit.credit remarks'),
            'secondary_password' => __('a_admin_management_transfer_credit.secondary password')
        ];
    }
}
