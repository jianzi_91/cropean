<?php

namespace Modules\Credit\Http\Requests\Member\Transfer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Credit\Http\Rules\ValidMemberTransferCreditAmount;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\Tree\Http\Rules\SponsorRelation;
use Modules\User\Http\Rules\UserIsActive;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        return [
            'credit_type' => [
                'required',
                'exists:bank_credit_types,name'
            ],
            'to_member_id' => [
                'bail',
                'required',
                Rule::notIn([Auth::user()->member_id, Auth::user()->username]),
                // new SponsorExist(),
                // new SponsorRelation($user->username),
                new UserIsActive()
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                new ValidMemberTransferCreditAmount($this->credit_type),
                new NotScientificNotation(),
                new MaxDecimalPlaces(credit_precision($this->credit_type)),
                new EnoughCredit($this->credit_type, $user->id),
            ],
            'remarks' => [
                'max:255'
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('m_credit_transfer_create.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'to_member_id'       => __('m_credit_transfer_create.transfer id'),
            'amount'             => __('m_credit_transfer_create.amount'),
            'remarks'            => __('m_credit_transfer_create.credit remarks'),
            'secondary_password' => __('m_credit_transfer_create.secondary password')
        ];
    }
}
