<?php

namespace Modules\Credit\Http\Requests\Member\RewardCredit\Transfer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\Tree\Http\Rules\SponsorRelation;
use Modules\User\Http\Rules\UserIsActive;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        return [
            'to_member_id' => [
                'bail',
                'required',
                Rule::notIn([Auth::user()->member_id, Auth::user()->username]),
                new SponsorExist(),
                new SponsorRelation($user->username),
                new UserIsActive()
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                'max:' . config('credit.transfers.max'),
                new NotScientificNotation(),
                new MaxDecimalPlaces(),
                new EnoughCredit(BankCreditTypeContract::USD_CREDIT, $user->id),
            ],
            'remarks' => [
                'max:255'
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('m_usd_credit_transfer.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'to_member_id'       => __('m_usd_credit_transfer.transfer id'),
            'amount'             => __('m_usd_credit_transfer.amount'),
            'remarks'            => __('m_usd_credit_transfer.credit remarks'),
            'secondary_password' => __('m_usd_credit_transfer.secondary password')
        ];
    }
}
