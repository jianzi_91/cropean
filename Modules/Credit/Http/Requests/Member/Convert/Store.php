<?php

namespace Modules\Credit\Http\Requests\Member\Convert;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Credit\Http\Rules\ValidConvertCreditAmount;
use Modules\Credit\Http\Rules\ValidConvertCreditType;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bankCreditTypeContract = resolve(BankCreditTypeContract::class);
        $creditType             = $bankCreditTypeContract->find($this->credit_type_convert_from)->rawname ?? null;

        return [
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                new NotScientificNotation(), // To not allow scientific notation
                new MaxDecimalPlaces(credit_precision()),
                new EnoughCredit($creditType, auth()->user()->id),
                new ValidConvertCreditAmount($this->credit_type_convert_from, $this->credit_type_convert_to),
            ],
            'credit_type_convert_from' => [
                'bail',
                'required',
                'exists:bank_credit_types,id',
            ],
            'credit_type_convert_to' => [
                'bail',
                'required',
                'exists:bank_credit_types,id',
                new ValidConvertCreditType($this->credit_type_convert_from)
            ],
            'secondary_password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'amount'                   => __('m_convert_credits.amount to convert'),
            'credit_type_convert_from' => __('m_convert_credits.convert from'),
            'credit_type_to_convert'   => __('m_convert_credits.convert to'),
            'secondary_password'       => __('m_convert_credits.secondary password')
        ];
    }
}
