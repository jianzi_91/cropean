<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Models\ConvertibleCreditType;

class ValidConvertCreditAmount implements Rule
{
    /**
     * The message
     *
     * @var unknown
     */
    protected $message;

    /**
     * Create a new rule instance.
     *
     */
    public function __construct($fromCreditTypeId, $toCreditTypeId)
    {
        $this->fromCreditTypeId = $fromCreditTypeId;
        $this->toCreditTypeId   = $toCreditTypeId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Return true only if from and to credit type id is empty, let their own validation works for this
        if (empty($this->fromCreditTypeId) || empty($this->toCreditTypeId)) {
            return true;
        }

        $setting = ConvertibleCreditType::where('from_credit_type_id', $this->fromCreditTypeId)
            ->where('to_credit_type_id', $this->toCreditTypeId)
            ->first();

        $minimumAmount = $setting->minimum;
        $maximumAmount = $setting->maximum;
        $multiples     = $setting->multiple;

        if ($minimumAmount > 0) {
            if ($value < $minimumAmount) {
                $this->message = __('s_validation.minimum amount is :minimum_amount', ['minimum_amount' => amount_format($minimumAmount)]);
                return false;
            }
        }

        if ($maximumAmount > 0) {
            if ($value > $maximumAmount) {
                $this->message = __('s_validation.maximum amount is :maximum_amount', ['maximum_amount' => amount_format($maximumAmount)]);
                return false;
            }
        }

        if ($multiples > 0) {
            if (0 != bcmod($value, $multiples, credit_precision())) {
                $this->message = __('s_validation.amount must be a multiples of :multiples', ['multiples' => amount_format($multiples)]);
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
