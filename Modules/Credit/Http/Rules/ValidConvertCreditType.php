<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\CreditConversionContract;

class ValidConvertCreditType implements Rule
{
    protected $creditId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($creditId)
    {
        $this->creditId = $creditId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $creditConversionRepository = resolve(CreditConversionContract::class);

        $creditConvertTo = $creditConversionRepository->getConvertibleCreditTypes($this->creditId);

        if (in_array($value, $creditConvertTo->pluck('id')->toArray())) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.cannot convert to this credit type');
    }
}
