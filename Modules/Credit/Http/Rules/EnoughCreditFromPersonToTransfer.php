<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;

class EnoughCreditFromPersonToTransfer implements Rule
{
    /**
     * The credit repository.
     *
     * @return unknown
     */
    protected $creditRepository;

    /**
     * The credit type repository
     *
     * @return unknown
     */
    protected $creditTypeRepository;

    /**
     * The credit type id
     *
     * @return unknown
     */
    protected $creditType;

    /**
     * The user id
     *
     * @return unknown
     */
    protected $userId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($creditType, $userId)
    {
        $this->creditRepository     = resolve(BankAccountCreditContract::class);
        $this->creditTypeRepository = resolve(BankCreditTypeContract::class);
        $this->creditType           = $creditType;
        $this->userId               = $userId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //Only validate if credit type is passing in the request
        if ($this->creditType) {
            $creditType = $this->creditTypeRepository->findBySlug($this->creditType);
            $balance    = $this->creditRepository->getBalanceByReference($this->userId, $creditType->id);

            if ($value >= 0) {
                return true;
            }
            $remainingBalance = bcsub($balance, abs($value), 6);

            return $remainingBalance >= 0;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.insufficient credits');
    }
}
