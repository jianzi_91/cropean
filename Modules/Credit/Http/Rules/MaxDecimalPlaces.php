<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class MaxDecimalPlaces implements Rule
{
    /**
     * The max decimal places
     * @var integer
     */
    protected $max = 0;

    /**
     * Class constructor
     * @param number $max
     */
    public function __construct($max = 2)
    {
        $this->max = $max;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (false === strpos($value, '.')) {
            return true;
        }

        [$number, $decimal] = explode('.', $value);

        return strlen($decimal) <= $this->max;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $message = __('s_validation.decimal places shouldnt be greater than');

        return $message . ' ' . $this->max;
    }
}
