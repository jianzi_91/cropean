<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotZero implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return bcmul($value, 1, credit_precision()) != 0 && !empty($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.must not be zero');
    }
}
