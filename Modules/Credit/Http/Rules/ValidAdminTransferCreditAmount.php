<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidAdminTransferCreditAmount implements Rule
{
    /**
     * The message
     *
     * @var unknown
     */
    protected $message;

    /**
     * Credit type slug
     *
     * @var unknown
     */
    protected $creditType;

    /**
     * Create a new rule instance.
     *
     */
    public function __construct($creditType)
    {
        $this->creditType = $creditType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $minimumAmount = config('credit.admin_transfer.min');
        $maximumAmount = config('credit.admin_transfer.max');
        $multiples     = config('credit.admin_transfer.multiple');

        if ($minimumAmount > 0) {
            if ($value < $minimumAmount) {
                $this->message = __('s_validation.minimum amount is :minimum_amount', ['minimum_amount' => amount_format($minimumAmount)]);
                return false;
            }
        }

        if ($maximumAmount > 0) {
            if ($value > $maximumAmount) {
                $this->message = __('s_validation.maximum amount is :maximum_amount', ['maximum_amount' => amount_format($maximumAmount)]);
                return false;
            }
        }

        if ($multiples > 0) {
            if (0 != bcmod($value, $multiples, credit_precision())) {
                $this->message = __('s_validation.amount must be a multiples of :multiples', ['multiples' => amount_format($multiples)]);
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
