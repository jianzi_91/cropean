<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\BankCreditTypeContract;

class IsTransferable implements Rule
{
    /**
     * The credit repository.
     *
     * @return unknown
     */
    protected $creditRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->creditRepository = resolve(BankCreditTypeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $creditType = $this->creditRepository->findBySlug($value);

        return $creditType->is_transferable ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.is not transferable');
    }
}
