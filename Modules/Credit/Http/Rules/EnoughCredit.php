<?php

namespace Modules\Credit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\BankAccountCreditContract;

class EnoughCredit implements Rule
{
    /**
     * The credit repository.
     *
     * @return unknown
     */
    protected $creditRepository;

    /**
     * The credit type id
     *
     * @return unknown
     */
    protected $creditType;

    /**
     * The user id
     *
     * @return unknown
     */
    protected $userId;

    /**
     * The adjust flag
     *
     * @return unknown
     */
    protected $adjust;

    /**
     * The offset
     *
     * @return unknown
     */
    protected $offset;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($creditType, $userId, $adjust = false, $offset = 0)
    {
        $this->creditRepository = resolve(BankAccountCreditContract::class);

        $this->creditType = $creditType;
        $this->userId     = $userId;
        $this->adjust     = $adjust;
        $this->offset     = $offset;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //Only validate if credit type is passing in the request
        if ($this->creditType) {
            $balance = $this->creditRepository->getBalanceByReference($this->userId, $this->creditType);

            if (!$this->adjust) {
                $value += $this->offset;
                return $balance >= $value;
            }

            if ($value >= 0) {
                return true;
            }

            $remainingBalance = bcsub($balance, abs($value), 6);

            return $remainingBalance >= 0;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.insufficient credits');
    }
}
