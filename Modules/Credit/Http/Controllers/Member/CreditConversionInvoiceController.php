<?php

namespace Modules\Credit\Http\Controllers\Member;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\CreditConversionInvoiceContract;

class CreditConversionInvoiceController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct(
        CreditConversionInvoiceContract $invoiceContract,
        PDF $pdfService
    ) {
        $this->invoiceRepo = $invoiceContract;
        $this->pdfService  = $pdfService;
    }

    public function show(Request $request, $id)
    {
        $invoice = $this->invoiceRepo->findBySlug($id);

        if ($invoice->member_id != auth()->user()->member_id) {
            abort(404);
        }

        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . 'credit_conversions' . DIRECTORY_SEPARATOR . $invoice->reference_number . '.pdf';
        return response()->file($path, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition: attachment; filename="' . $invoice->reference_number . '"'
        ]);
    }
}
