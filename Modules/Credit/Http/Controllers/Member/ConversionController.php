<?php

namespace Modules\Credit\Http\Controllers\Member;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Http\Requests\Member\Convert\Store;
use Modules\Credit\Queries\Member\CreditConversionQuery;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;
use QueryBuilder\Concerns\CanExportTrait;

class ConversionController extends Controller
{
    use CanExportTrait;

    protected $creditConversionRepository;
    protected $exchangeRateRepository;

    public function __construct(
        CreditConversionQuery $conversionQuery,
        CreditConversionContract $creditConversionContract,
        CreditConversionStatusContract $creditConversionStatusContract,
        ExchangeRateContract $exchangeRateContract,
        BankCreditTypeContract $bankCreditTypeContract
    ) {
        $this->middleware('permission:member_conversion_list')->only('index');
        $this->middleware('permission:member_conversion_create')->only('create', 'store');
        $this->middleware('permission:member_conversion_edit')->only('update');
        $this->middleware('permission:member_conversion_export')->only('export');
        $this->middleware('double_click_prevention')->only('store');

        $this->conversionQuery                  = $conversionQuery;
        $this->creditConversionRepository       = $creditConversionContract;
        $this->creditConversionStatusRepository = $creditConversionStatusContract;
        $this->exchangeRateRepository           = $exchangeRateContract;
        $this->bankCreditTypeRepository         = $bankCreditTypeContract;
    }

    public function index(Request $request)
    {
        $conversions = $this->conversionQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::member.conversion.index', compact('conversions'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $user = auth()->user();

        if (!$user->can('capital_credit_convert') && !$user->can('rollover_credit_convert') && !$user->can('cash_credit_convert') && !$user->can('usdt_erc20_convert') && !$user->can('usdc_convert')) {
            abort('403');
        }

        $exchangeRate = $this->exchangeRateRepository->findBySlug(ExchangeRateContract::CNY)->buy_rate;

        return view('credit::member.conversion.create', compact('exchangeRate'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $user = auth()->user();

        if (!$user->can('capital_credit_convert') && !$user->can('rollover_credit_convert') && !$user->can('cash_credit_convert') && !$user->can('usdt_erc20_convert') && !$user->can('usdc_convert')) {
            abort('403');
        }

        $blockchainCreditTypes = $this->bankCreditTypeRepository->getModel()
            ->whereNotNull('network')
            ->pluck('id')
            ->toArray();

        DB::beginTransaction();

        try {
            $userId = $user->id;
            User::lockForUpdate()->find($userId);

            $data            = $request->only('amount', 'credit_type_convert_from', 'credit_type_convert_to');
            $data['user_id'] = $userId;

            $conversion = $this->creditConversionRepository->request($data);

            // Edited out on 12/10/2020 on client request
            // if (in_array($data['credit_type_convert_to'], $blockchainCreditTypes)) {
            //     $pendingStatus = $this->creditConversionStatusRepository->findBySlug(CreditConversionStatusContract::PENDING);

            //     $this->creditConversionStatusRepository->changeHistory($conversion, $pendingStatus);

            //     DB::commit();

            //     return redirect()->back()->with('success', __('m_credit_conversion.conversion request is submitted'));
            // }

            $conversion      = $this->creditConversionRepository->approve($conversion->id);
            $completedStatus = $this->creditConversionStatusRepository->findBySlug(CreditConversionStatusContract::COMPLETED);

            $this->creditConversionStatusRepository->changeHistory($conversion, $completedStatus);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->withMessage(__('m_credit_conversion.failed to convert'));

            return redirect()->back()->withInput($request->all());
        }

        return redirect()->back()->with('success', __('m_credit_conversion.convert successfully'));
    }

    public function update(Request $request, $id)
    {
        $conversion = $this->creditConversionRepository->findOrFail($id);

        if ($conversion->status->status->rawname != CreditConversionStatusContract::PENDING) {
            return redirect(route('member.credits.conversion.index'))->with('error', __('m_credit_conversion.failed to update status'));
        }

        DB::beginTransaction();

        try {
            $conversion      = $this->creditConversionRepository->cancel($conversion->id);
            $cancelledStatus = $this->creditConversionStatusRepository->findBySlug(CreditConversionStatusContract::CANCELLED);
            $this->creditConversionStatusRepository->changeHistory($conversion, $cancelledStatus);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('m_conversion_list.status updated successfully'));
    }

    public function getConvertibleCreditTypes(Request $request)
    {
        $creditType = $request->bank_credit_type;
        $creditType = $this->bankCreditTypeRepository->find($creditType);
        // $convertableCreditTypes = [];
        $results = [
            "0" => "--",
        ];

        // switch ($creditType) {
        //     case BankCreditTypeContract::CAPITAL_CREDIT:
        //         $convertableCreditTypes = [BankCreditTypeContract::CASH_CREDIT];
        //         break;
        //     case BankCreditTypeContract::ROLL_OVER_CREDIT:
        //         $convertableCreditTypes = [BankCreditTypeContract::CASH_CREDIT];
        //         break;
        //     case BankCreditTypeContract::CASH_CREDIT:
        //         $convertableCreditTypes = [BankCreditTypeContract::ROLL_OVER_CREDIT, BankCreditTypeContract::USDT_ERC20, BankCreditTypeContract::USDC];
        //         break;
        //     case BankCreditTypeContract::USDT_ERC20:
        //     case BankCreditTypeContract::USDC:
        //         $convertableCreditTypes = [BankCreditTypeContract::CAPITAL_CREDIT];
        //         break;
        //     default:
        //         $convertableCreditTypes = [];
        //         break;
        // }

        $convertableCreditTypes = $creditType->canConvertToCredits;

        foreach ($convertableCreditTypes as $creditType) {
            $results[$creditType->id] = $creditType->name;
        }

        return $results;
    }

    public function getConvertCreditDetails(Request $request)
    {
        if (!empty($request['credit_type_convert_from']) && !empty($request['credit_type_convert_to'])) {
            $data = $this->creditConversionRepository->getConvertCreditDetails($request);

            return response()->json([
                'credit_type_convert_to'  => $data['id'],
                'currency_from'           => $data['currency_from'],
                'currency_from_sell_rate' => amount_format($data['currency_from_sell_rate'], credit_precision()),
                'currency_to'             => $data['currency_to'],
                'currency_to_buy_rate'    => amount_format($data['currency_to_buy_rate'], credit_precision()),
                'base_currency'           => $data['base_currency'],
                'base_currency_rate'      => amount_format($data['base_currency_rate'], credit_precision()),
            ]);
        }
        return response()->json([
                'credit_type_convert_to'  => 0,
                'currency_from'           => 0,
                'currency_from_sell_rate' => 0,
                'currency_to'             => 0,
                'currency_to_buy_rate'    => 0,
                'base_currency'           => 0,
                'base_currency_rate'      => 0,
            ]);
    }

    public function calculate(Request $request)
    {
        $request->merge([
            'user_id' => auth()->user()->id,
            'amount'  => $request->amount,
        ]);

        if (!empty($request['credit_type_convert_from']) && !empty($request['credit_type_convert_to'])) {
            $fee = $this->creditConversionRepository->calculate($request);

            return response()->json([
                'exchange_rate'    => $fee['exchangeRate'],
                'penalty_fee'      => $fee['penaltyAmount'],
                'service_fee'      => $fee['adminFee'],
                'converted_amount' => $fee['issueAmount'],
            ]);
        }

        return response()->json([
            'exchange_rate'    => 0,
            'penalty_fee'      => 0,
            'service_fee'      => 0,
            'converted_amount' => 0,
        ]);
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->conversionQuery->setParameters($request->all()), __('m_credit_conversion.conversion report') . now() . '.xlsx', auth()->user(), __('m_credit_conversion.conversion report') . Carbon::now()->toDateString());
    }
}
