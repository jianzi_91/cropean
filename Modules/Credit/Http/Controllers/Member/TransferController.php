<?php

namespace Modules\Credit\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Requests\Member\Transfer\Store;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;

class TransferController extends Controller
{
    protected $creditConversionRepository;
    protected $exchangeRateRepository;

    public function __construct(
        BankAccountCreditContract $creditContract,
        UserContract $userContract,
        BankCreditTypeContract $bankCreditTypeContract
    ) {
        $this->middleware('permission:member_credit_transfer_create')->only('create', 'store');

        $this->creditRepository         = $creditContract;
        $this->userRepository           = $userContract;
        $this->bankCreditTypeRepository = $bankCreditTypeContract;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('credit::member.transfer.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $fromUser = auth()->user();
        $toUser   = $this->userRepository->findByIdentifier($request->to_member_id);

        if (!$toUser) {
            return back()->withInput($request->all())->with('error', __('a_transfer_credit.user not found'));
        }

        DB::beginTransaction();

        User::lockForUpdate()->find($fromUser->id);

        try {
            $remarks = $request->remarks;
            $this->creditRepository->transfer($request->credit_type, abs($request->amount), $toUser->id, $fromUser->id, $remarks);
            // $this->creditRepository->add($request->credit_type, abs($request->amount), $toUser->id, 'credit_transfer_from', $remarks);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->withMessage(__('m_credit_transfer_create.failed to transfer credit'));
        }

        return back()->with('success', __('m_credit_transfer_create.credit transferred successfully'));
    }
}
