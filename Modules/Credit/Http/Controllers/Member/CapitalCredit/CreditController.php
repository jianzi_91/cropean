<?php

namespace Modules\Credit\Http\Controllers\Member\CapitalCredit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Credit\Queries\Member\BankAccountStatementQuery;
use Modules\Credit\Queries\Member\CreditLockingQuery;

class CreditController extends Controller
{
    protected $bankAccountStatementQuery;

    protected $bankCreditTypeContract;

    /**
     * Class constructor
     *
     * @param BankAccountStatementQuery $bankAccountStatementQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     */
    public function __construct(
        BankAccountStatementQuery $bankAccountStatementQuery, 
        CreditLockingQuery $creditLockingQuery, 
        BankCreditTypeContract $bankCreditTypeContract,
        CreditLockingContract $creditLockingContract
    ){
        $this->middleware('permission:member_credit_statement_capital_credit_list')->only('index');

        $this->bankAccountStatementQuery = $bankAccountStatementQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
        $this->creditLockingContract     = $creditLockingContract;
        $this->creditLockingQuery        = $creditLockingQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge([
            'bank_credit_type_id' => get_bank_credit_type($this->bankCreditTypeContract::CAPITAL_CREDIT)->id
        ]);

        $statements = $this->bankAccountStatementQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::member.capital_credit.index', compact('statements'));
    }
}
