<?php

namespace Modules\Credit\Http\Controllers\Member\CapitalCredit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Member\BankAccountStatementQuery;
use QueryBuilder\Concerns\CanExportTrait;

class CreditExportController extends Controller
{
    use CanExportTrait;

    protected $bankAccountStatementQuery;

    protected $bankCreditTypeContract;

    /**
     * Class constructor
     *
     * @param BankAccountStatementQuery $bankAccountStatementQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     */
    public function __construct(BankAccountStatementQuery $bankAccountStatementQuery, BankCreditTypeContract $bankCreditTypeContract)
    {
        $this->middleware('permission:member_credit_statement_capital_credit_export')->only(['index']);

        $this->bankAccountStatementQuery = $bankAccountStatementQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge([
            'bank_credit_type_id' => get_bank_credit_type($this->bankCreditTypeContract::CAPITAL_CREDIT)->id,
        ]);

        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->bankAccountStatementQuery->setParameters($request->all()), 'capital_statement_' . $now . '.xlsx', auth()->user(), 'Export Capital Credit Statement on ' . $now);
    }
}
