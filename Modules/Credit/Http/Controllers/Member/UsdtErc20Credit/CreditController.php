<?php

namespace Modules\Credit\Http\Controllers\Member\UsdtErc20Credit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Member\BankAccountStatementQuery;

class CreditController extends Controller
{
    protected $bankAccountStatementQuery;

    protected $bankCreditTypeContract;

    /**
     * Class constructor
     *
     * @param BankAccountStatementQuery $bankAccountStatementQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     */
    public function __construct(BankAccountStatementQuery $bankAccountStatementQuery, BankCreditTypeContract $bankCreditTypeContract)
    {
        // $this->middleware('permission:member_credit_statement_cash_credit_list')->only('index');

        $this->bankAccountStatementQuery = $bankAccountStatementQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge([
            'bank_credit_type_id' => get_bank_credit_type($this->bankCreditTypeContract::USDT_ERC20)->id
        ]);

        $statements = $this->bankAccountStatementQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::member.usdt_erc20_credit.index', compact('statements'));
    }
}
