<?php

namespace Modules\Credit\Http\Controllers\Admin\Statement\CashCredit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BankAccountStatementQuery;

class CreditController extends Controller
{
    protected $bankAccountStatementQuery;

    protected $bankCreditTypeContract;

    /**
     * Class constructor
     *
     * @param BankAccountStatementQuery $bankAccountStatementQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     */
    public function __construct(BankAccountStatementQuery $bankAccountStatementQuery, BankCreditTypeContract $bankCreditTypeContract)
    {
        $this->middleware('permission:admin_credit_statement_cash_credit_list')->only(['index']);

        $this->bankAccountStatementQuery = $bankAccountStatementQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge([
            'bank_credit_type_id' => get_bank_credit_type($this->bankCreditTypeContract::CASH_CREDIT)->id
        ]);

        $statements = $this->bankAccountStatementQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.statement.cash_credit.index', compact('statements'));
    }
}
