<?php

namespace Modules\Credit\Http\Controllers\Admin\System;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Requests\Admin\System\Transfer\Store;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;

class MemberCreditTransferController extends Controller
{
    protected $bankCreditTypeRepository;

    /**
     * Class constructor
     *
     */
    public function __construct(BankAccountCreditContract $creditContract, UserContract $userContract, BankCreditTypeContract $bankCreditTypeContract)
    {
        $this->middleware('permission:admin_system_transfer_credit')->only('create', 'store');

        $this->creditRepository         = $creditContract;
        $this->userRepository           = $userContract;
        $this->bankCreditTypeRepository = $bankCreditTypeContract;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $credits = transferable_credit_types_dropdown();

        if (!auth()->user()->can('admin_system_transfer_usdt_credit')) {
            unset($credits[$this->bankCreditTypeRepository::USDT]);
        }

        return view('credit::admin.system.credit_transfer.index', compact('credits'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $fromUser = $this->userRepository->findByIdentifier($request->transfer_from);
        $toUser   = $this->userRepository->findByIdentifier($request->transfer_to);

        if (!$toUser) {
            return back()->withInput($request->all())->with('error', __('a_transfer_credit.user not found'));
        }

        DB::beginTransaction();

        User::lockForUpdate()->find($fromUser->id);

        try {
            $adminId = Auth::id();
            $remarks = $request->remarks;
            $this->creditRepository->deduct($request->credit_type, abs($request->amount), $fromUser->id, 'credit_transfer_to', $remarks, $adminId, null, $adminId);
            $this->creditRepository->add($request->credit_type, abs($request->amount), $toUser->id, 'credit_transfer_from', $remarks, $adminId, null, $adminId);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->withMessage(__('a_transfer_credit.failed to transfer credit'));
        }

        return back()->with('success', __('a_transfer_credit.credit transferred successfully'));
    }
}
