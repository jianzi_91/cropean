<?php

namespace Modules\Credit\Http\Controllers\Admin\System\CapitalCredit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BankAccountSystemQuery;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Credit\Queries\Admin\CreditLockingQuery;

class CreditController extends Controller
{
    protected $bankAccountSystemQuery;

    protected $bankCreditTypeContract;

    protected $bankAccountCreditContract;

    /**
     * Class constructor
     *
     * @param BankAccountSystemQuery $bankAccountSystemQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BankAccountCreditContract $bankAccountCreditContract
     */
    public function __construct(
        BankAccountSystemQuery $bankAccountSystemQuery,
        BankCreditTypeContract $bankCreditTypeContract,
        BankAccountCreditContract $bankAccountCreditContract,
        CreditLockingQuery $creditLockingQuery,
        CreditLockingContract $creditLockingContract
    ) {
        $this->middleware('permission:admin_credit_system_capital_credit_list')->only(['index']);

        $this->bankAccountSystemQuery    = $bankAccountSystemQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
        $this->bankAccountCreditContract = $bankAccountCreditContract;
        $this->creditLockingContract     = $creditLockingContract;
        $this->creditLockingQuery        = $creditLockingQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $bankCreditTypeId = get_bank_credit_type($this->bankCreditTypeContract::CAPITAL_CREDIT)->id;
        $request->merge([
            'bank_credit_type_id' => $bankCreditTypeId,
        ]);

        $systemBalance = $this->bankAccountCreditContract->getSystemBalance($bankCreditTypeId);

        $creditLocking = $this->creditLockingContract->getModel()
            ->select([
                DB::raw('DATE(end_date) AS unlocking_date'),
                DB::raw('SUM(current_amount) AS unlocking_amount'),
            ])
            ->whereDate('end_date', '>', now()->toDateString())
            ->where('current_amount', '!=', 0)
            ->where('penalty_days', '!=', 0)
            ->groupBy('unlocking_date')
            ->orderBy('end_date')
            ->first();

        $statements = $this->bankAccountSystemQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.system.capital_credit.index', compact('statements', 'systemBalance', 'creditLocking'));
    }

    public function breakdown(Request $request)
    {
        $creditLocks = $this->creditLockingQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.system.capital_credit.breakdown', compact('creditLocks'));
    }
}
