<?php

namespace Modules\Credit\Http\Controllers\Admin\System\UsdtErc20Credit;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BankAccountSystemQuery;
use QueryBuilder\Concerns\CanExportTrait;

class CreditExportController extends Controller
{
    use CanExportTrait;

    protected $bankAccountSystemQuery;

    protected $bankCreditTypeContract;

    /**
     * Class constructor
     *
     * @param BankAccountSystemQuery $bankAccountSystemQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     */
    public function __construct(BankAccountSystemQuery $bankAccountSystemQuery, BankCreditTypeContract $bankCreditTypeContract)
    {
        $this->middleware('permission:admin_credit_system_usdt_erc20_credit_export')->only(['index']);

        $this->bankAccountSystemQuery = $bankAccountSystemQuery;
        $this->bankCreditTypeContract = $bankCreditTypeContract;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $request->merge([
            'bank_credit_type_id' => get_bank_credit_type($this->bankCreditTypeContract::USDT_ERC20)->id,
        ]);

        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->bankAccountSystemQuery->setParameters($request->all()), 'usdt_erc20_system_statement_' . $now . '.xlsx', auth()->user(), 'Export Usdt Erc20 Statement on ' . $now);
    }
}
