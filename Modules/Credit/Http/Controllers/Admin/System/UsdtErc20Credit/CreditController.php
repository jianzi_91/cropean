<?php

namespace Modules\Credit\Http\Controllers\Admin\System\UsdtErc20Credit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BankAccountSystemQuery;

class CreditController extends Controller
{
    protected $bankAccountSystemQuery;

    protected $bankCreditTypeContract;

    protected $bankAccountCreditContract;

    /**
     * Class constructor
     *
     * @param BankAccountSystemQuery $bankAccountSystemQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BankAccountCreditContract $bankAccountCreditContract
     */
    public function __construct(
        BankAccountSystemQuery $bankAccountSystemQuery,
        BankCreditTypeContract $bankCreditTypeContract,
        BankAccountCreditContract $bankAccountCreditContract
    ) {
        $this->middleware('permission:admin_credit_system_usdt_erc20_credit_list')->only(['index']);

        $this->bankAccountSystemQuery    = $bankAccountSystemQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
        $this->bankAccountCreditContract = $bankAccountCreditContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $bankCreditTypeId = get_bank_credit_type($this->bankCreditTypeContract::USDT_ERC20)->id;
        $request->merge([
            'bank_credit_type_id' => $bankCreditTypeId,
        ]);

        $systemBalance = $this->bankAccountCreditContract->getSystemBalance($bankCreditTypeId);

        $statements = $this->bankAccountSystemQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.system.usdt_erc20_credit.index', compact('statements', 'systemBalance'));
    }
}
