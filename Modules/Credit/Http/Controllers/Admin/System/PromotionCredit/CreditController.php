<?php

namespace Modules\Credit\Http\Controllers\Admin\System\PromotionCredit;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BankAccountSystemQuery;

class CreditController extends Controller
{
    protected $bankAccountSystemQuery;

    protected $bankCreditTypeContract;

    protected $bankAccountCreditContract;

    /**
     * Class constructor
     *
     * @param BankAccountSystemQuery $bankAccountSystemQuery
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BankAccountCreditContract $bankAccountCreditContract
     */
    public function __construct(
        BankAccountSystemQuery $bankAccountSystemQuery,
        BankCreditTypeContract $bankCreditTypeContract,
        BankAccountCreditContract $bankAccountCreditContract
    ) {
        $this->middleware('permission:admin_credit_system_promotion_credit_list')->only(['index']);

        $this->bankAccountSystemQuery    = $bankAccountSystemQuery;
        $this->bankCreditTypeContract    = $bankCreditTypeContract;
        $this->bankAccountCreditContract = $bankAccountCreditContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $bankCreditTypeId = get_bank_credit_type($this->bankCreditTypeContract::PROMOTION_CREDIT)->id;
        $request->merge([
            'bank_credit_type_id' => $bankCreditTypeId,
        ]);

        $systemBalance = $this->bankAccountCreditContract->getSystemBalance($bankCreditTypeId);

        $statements = $this->bankAccountSystemQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.system.promotion_credit.index', compact('statements', 'systemBalance'));
    }
}
