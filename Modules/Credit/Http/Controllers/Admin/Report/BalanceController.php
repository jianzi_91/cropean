<?php

namespace Modules\Credit\Http\Controllers\Admin\Report;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\Admin\BalanceQuery;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class BalanceController extends Controller
{
    protected $query;
    protected $exchangeRateRepo;

    /**
     * SpecialBonusController constructor.
     * @param BalanceQuery $query
     * @param ExchangeRateContract $exchangeRateRepo
     */
    public function __construct(BalanceQuery $query, ExchangeRateContract $exchangeRateRepo)
    {
        $this->query            = $query;
        $this->exchangeRateRepo = $exchangeRateRepo;
        $this->middleware('permission:admin_report_balance_statement_list')->only('index');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $statements = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.report.balance', compact('statements'));
    }
}
