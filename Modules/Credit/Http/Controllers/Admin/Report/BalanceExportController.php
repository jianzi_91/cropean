<?php

namespace Modules\Credit\Http\Controllers\Admin\Report;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Queries\Admin\BalanceQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BalanceExportController extends Controller
{
    use CanExportTrait;

    /**
     * The leader bonus query
     *
     * @var unknown
     */
    protected $query;

    /**
     * Class constructor
     *
     * @param BalanceQuery $query
     */
    public function __construct(BalanceQuery $query)
    {
        $this->middleware('permission:admin_report_balance_statement_export')->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('a_report_balance.member balance') . now() . '.xlsx', auth()->user(), __('a_report_balance.member balance') . Carbon::now()->toDateString());
    }
}
