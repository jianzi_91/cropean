<?php

namespace Modules\Credit\Http\Controllers\Admin\Management\Conversion;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Http\Requests\Admin\Management\Conversion\Admin\Update;
use Modules\Credit\Queries\Admin\CreditConversionQuery;
use QueryBuilder\Concerns\CanExportTrait;

class ConversionController extends Controller
{
    use CanExportTrait;

    /**
     * Class constructor.
     *
     * @param CreditConversionQuery $conversionQuery
     */
    public function __construct(
        CreditConversionQuery $conversionQuery,
        CreditConversionContract $creditConversionRepo,
        CreditConversionStatusContract $creditConversionStatusRepo
    ) {
        $this->middleware('permission:admin_conversion_list')->only('index');
        $this->middleware('permission:admin_conversion_edit')->only('edit', 'update');
        $this->middleware('permission:admin_conversion_view')->only('show');
        $this->middleware('permission:admin_conversion_export')->only('export');
        $this->middleware('double_click_prevention')->only('update');

        $this->conversionQuery      = $conversionQuery;
        $this->conversionRepo       = $creditConversionRepo;
        $this->conversionStatusRepo = $creditConversionStatusRepo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $conversions = $this->conversionQuery->setParameters($request->all())
            ->paginate();

        return view('credit::admin.conversion.index', compact('conversions'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $conversion = $this->conversionRepo->findOrFail($id);

        return view('credit::admin.conversion.show', compact('conversion'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $conversion = $this->conversionRepo->find($id);

        return view('credit::admin.conversion.edit', compact('conversion'));
    }

    /**
     * Update the specified resource.
     * @param int $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $data       = $request->only('status_id', 'remarks');
        $conversion = $this->conversionRepo->find($id);
        $status     = $this->conversionStatusRepo->find($data['status_id']);

        if ($conversion->status->status->rawname != CreditConversionStatusContract::PENDING) {
            return redirect(route('admin.credits.conversion.index'))->with('error', __('a_conversion_list.failed to update status'));
        }
        DB::beginTransaction();

        try {
            if ($status->rawname == CreditConversionStatusContract::APPROVED) {
                $conversion = $this->conversionRepo->approve($conversion->id);
            }

            if ($status->rawname == CreditConversionStatusContract::REJECTED) {
                $conversion = $this->conversionRepo->reject($conversion->id);
            }

            $this->conversionStatusRepo->changeHistory($conversion, $status, ['remarks' => $data['remarks']]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('errors', $e);
        }

        return redirect()->route('admin.credits.conversion.index')->with('success', __('a_conversion_list.status updated successfully'));
    }

    public function bulkUpdate(Request $request)
    {
        $conversionIds = explode(',', $request->conversion_ids);
        
        switch ($request->action) {
            case 'approve':
                $status = $this->conversionStatusRepo->findBySlug(CreditConversionStatusContract::APPROVED);
            break;
            case 'reject': 
                $status = $this->conversionStatusRepo->findBySlug(CreditConversionStatusContract::REJECTED);
            break;
            default:
            return back()->with('error', __('a_conversion_list.invalid status'));
            break;
        }

        try {
            DB::beginTransaction();
            ini_set('max_execution_time', -1);
            foreach ($conversionIds as $conversionId) {
                $conversion = $this->conversionRepo->find($conversionId);

                if ($conversion) {
                    if ($conversion->status->status->rawname != CreditConversionStatusContract::PENDING || $conversion->status->status->rawname == CreditConversionStatusContract::CANCELLED) {
                        continue;
                    }

                    if ($request->action == 'approve') {
                        $conversion = $this->conversionRepo->approve($conversion->id);
                    }

                    if ($request->action == 'reject') {
                        $conversion = $this->conversionRepo->reject($conversion->id);
                    }

                    $this->conversionStatusRepo->changeHistory($conversion, $status, ['remarks' => $request->remarks]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('a_conversion_list.status updated successfully'));
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->conversionQuery->setParameters($request->all()), __('a_conversion_list.conversion report') . now() . '.xlsx', auth()->user(), __('a_conversion_list.conversion report') . Carbon::now()->toDateString());
    }
}
