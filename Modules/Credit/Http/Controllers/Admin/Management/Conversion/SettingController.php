<?php

namespace Modules\Credit\Http\Controllers\Admin\Management\Conversion;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Http\Requests\Admin\Management\Conversion\Admin\UpdateSetting;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Models\ConvertibleCreditType;
use Modules\Setting\Contracts\SettingCacheContract;
use Modules\Setting\Contracts\SettingContract;

class SettingController extends Controller
{
    /**
     * Class constructor
     *
     */
    public function __construct(
        SettingContract $settingContract,
        SettingCacheContract $settingCacheContract
    ) {
        $this->middleware('permission:admin_conversion_setting_view')->only('index');
        $this->middleware('permission:admin_conversion_setting_update')->only('update');

        $this->settingRepo      = $settingContract;
        $this->settingCacheRepo = $settingCacheContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $penaltyRate            = $this->settingRepo->findBySlug('penalty_percentage');
        $penaltyRate            = \json_decode($penaltyRate->value->value, true);
        $convertibleCreditTypes = BankCreditType::where('can_convert', true)->get();

        return view('credit::admin.management.conversion.settings.index', compact('penaltyRate', 'convertibleCreditTypes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdateSetting $request, $method)
    {
        $data = $request->only(
            'minimum',
            'multiple',
            'admin_fee',
            'penalty_percentage_30',
            'penalty_percentage_90',
            'credit_conversion_roll_over_credit_admin_fee_percentage',
            'credit_conversion_usdt_erc20_admin_fee_percentage',
            'credit_conversion_usdc_admin_fee_percentage',
            'credit_conversion_cash_credit_admin_fee_percentage'
        );

        if ($method == 'limit') {
            $minimums  = $data['minimum'];
            $multiples = $data['multiple'];
            $adminFees = $data['admin_fee'];

            $convertibleCreditTypes = ConvertibleCreditType::all();
            foreach ($convertibleCreditTypes as $setting) {
                $setting->update([
                    'minimum'              => $minimums[$setting->from_credit_type_id][$setting->to_credit_type_id],
                    'multiple'             => $multiples[$setting->from_credit_type_id][$setting->to_credit_type_id],
                    'admin_fee_percentage' => bcdiv($adminFees[$setting->from_credit_type_id][$setting->to_credit_type_id], 100, 4),
                ]);
            }

            return back()->with('success', __('a_setting_others.setting updated successfully'));
        }

        if (isset($data['penalty_percentage_30']) && isset($data['penalty_percentage_90'])) {
            $penaltyRate = [
                '30' => bcdiv($data['penalty_percentage_30'], 100, 4),
                '90' => bcdiv($data['penalty_percentage_90'], 100, 4),
            ];
            $data['penalty_percentage'] = \json_encode($penaltyRate, JSON_NUMERIC_CHECK);
        }

        if (isset($data['credit_conversion_roll_over_credit_admin_fee_percentage'])) {
            $data['credit_conversion_roll_over_credit_admin_fee_percentage'] = bcdiv($data['credit_conversion_roll_over_credit_admin_fee_percentage'], 100, 4);
        }

        if (isset($data['credit_conversion_usdt_erc20_admin_fee_percentage'])) {
            $data['credit_conversion_usdt_erc20_admin_fee_percentage'] = bcdiv($data['credit_conversion_usdt_erc20_admin_fee_percentage'], 100, 4);
        }

        if (isset($data['credit_conversion_usdc_admin_fee_percentage'])) {
            $data['credit_conversion_usdc_admin_fee_percentage'] = bcdiv($data['credit_conversion_usdc_admin_fee_percentage'], 100, 4);
        }

        if (isset($data['credit_conversion_cash_credit_admin_fee_percentage'])) {
            $data['credit_conversion_cash_credit_admin_fee_percentage'] = bcdiv($data['credit_conversion_cash_credit_admin_fee_percentage'], 100, 4);
        }

        $settings = $this->settingRepo->getModel()
            ->whereIn('name', array_keys($data))
            ->where('is_active', 1)
            ->get();

        foreach ($settings as $setting) {
            $this->settingRepo->deleteValuesBySlug($setting->rawname);
            $value = $data[$setting->rawname];
            $this->settingRepo->addValuesBySlug($setting->rawname, $value);
            $this->settingCacheRepo->clear($setting->rawname);
        }

        return back()->with('success', __('a_setting_others.setting updated successfully'));
    }
}
