<?php

namespace Modules\Credit\Http\Controllers\Admin\Management\Transfer\Member;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Http\Requests\Admin\Management\Transfer\Member\Transfer;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class CreditController extends Controller
{
    /**
     * The credit repository
     *
     * @var unknown
     */
    protected $creditRepository;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param BankAccountCreditContract $creditContract
     * @param UserContract $UserContract $userContract
     */
    public function __construct(BankAccountCreditContract $creditContract, UserContract $userContract)
    {
        $this->middleware('permission:admin_user_member_management_transfer_credit')->only('edit', 'update');

        $this->creditRepository = $creditContract;
        $this->userRepository   = $userContract;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_transfer_credit.user not found'));
        }

        return view('credit::admin.management.transfer.member.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function update(Transfer $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_transfer_credit.user not found'));
        }

        DB::beginTransaction();
        try {
            $remarks = $request->remarks;
            $adminId = auth()->user()->id;

            if ($request->amount < 0) {
                $this->creditRepository->deduct($request->credit_type, abs($request->amount), $id, 'credit_transfer_to', $remarks, $adminId, null, $adminId);
                $this->creditRepository->add($request->credit_type, abs($request->amount), $adminId, 'credit_transfer_from', $remarks, $adminId, null, $id);
            } else {
                $this->creditRepository->transfer($request->credit_type, $request->amount, $user->id, $request->user()->id, $request->remarks);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.credits.transfer.edit', $id))->withMessage(__('a_member_management_transfer_credit.failed to transfer credit'));
        }

        return back()->with('success', __('a_member_management_transfer_credit.credit transferred successfully'));
    }
}
