<?php

namespace Modules\Credit\Http\Controllers\Admin\Management\Adjustment\Member;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Credit\Http\Requests\Admin\Management\Adjustment\Member\Adjust;
use Modules\Credit\Queries\Admin\MemberCreditAdjustmentQuery;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;
use QueryBuilder\Concerns\CanExportTrait;

class CreditController extends Controller
{
    use CanExportTrait;

    /**
     * The credit repository
     *
     * @var unknown
     */
    protected $creditRepository;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepository;

    /**
     * @var CreditLockingContract
     */
    protected $creditLockingRepo;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     * @param BankAccountCreditContract $creditContract
     */
    public function __construct(
        UserContract $userContract,
        BankAccountCreditContract $creditContract,
        BankCreditTypeContract $bankCreditTypeContract,
        CreditLockingContract $creditLockingContract,
        MemberCreditAdjustmentQuery $creditAdjustmentQuery
    ) {
        $this->middleware('permission:admin_user_member_management_adjust_credit_list')->only('index');
        $this->middleware('permission:admin_user_member_management_adjust_credit')->only('edit', 'update');
        $this->middleware('permission:admin_user_member_management_adjust_credit_export')->only('export');

        $this->creditRepository         = $creditContract;
        $this->userRepository           = $userContract;
        $this->creditLockingRepo        = $creditLockingContract;
        $this->bankCreditTypeRepository = $bankCreditTypeContract;
        $this->creditAdjustmentQuery    = $creditAdjustmentQuery;
    }

    /**
     * Get the credit adjustment history
     */
    public function index(Request $request)
    {
        $adjustments = $this->creditAdjustmentQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.management.adjustment.member.index')->with(compact('adjustments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_adjust_credit.user not found'));
        }

        return view('credit::admin.management.adjustment.member.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Adjust $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_adjust_credit.user not found'));
        }

        DB::beginTransaction();
        try {
            $remarks = $request->remarks;
            $adminId = auth()->user()->id;

            $capitalCreditName = $this->bankCreditTypeRepository::CAPITAL_CREDIT;

            if ($request->amount < 0) {
                $txnCode = $this->creditRepository->deduct($request->credit_type, abs($request->amount), $id, 'credit_adjustment', $remarks, $adminId);

                if ($request->credit_type === $capitalCreditName) {
                    $this->creditLockingRepo->updateCreditLock(abs($request->amount), $user->id, true);
                }
            } else {
                $txnCode = $this->creditRepository->add($request->credit_type, $request->amount, $id, 'credit_adjustment', $remarks, $adminId);

                if ($request->credit_type === $capitalCreditName) {
                    $penaltyPercentage = (setting('penalty_percentage'));

                    $this->creditLockingRepo->add([
                        'user_id'                 => $user->id,
                        'transaction_code'        => $txnCode,
                        'penalty_days'            => $request->penalty_days,
                        'penalty_fee_percentages' => $penaltyPercentage,
                        'start_date'              => Carbon::now(),
                        'end_date'                => Carbon::now()->addDay($request->penalty_days - 1),
                        'original_amount'         => $request->amount,
                        'current_amount'          => $request->amount,
                    ]);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.credits.adjustment.edit', $id))->withMessage(__('a_member_management_adjust_credit.failed to adjust user credit'));
        }

        return back()->with('success', __('a_member_management_adjust_credit.user credit adjusted successfully'));
    }

    /**
     * Get the credit adjustment history
     */
    public function export(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->creditAdjustmentQuery->setParameters($request->all()), 'member_credit_adjustments_' . $now . '.xlsx', auth()->user(), 'Export Cash Credit Statement on ' . $now);
    }
}
