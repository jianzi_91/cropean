<?php

namespace Modules\Credit\Http\Controllers\Admin\Management\Adjustment\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Http\Requests\Admin\Management\Adjustment\Admin\Adjust;
use Modules\Credit\Http\Requests\Admin\Management\Adjustment\Admin\Edit;
use Modules\Credit\Queries\Admin\AdminCreditAdjustmentQuery;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;
use QueryBuilder\Concerns\CanExportTrait;

class CreditController extends Controller
{
    use CanExportTrait;

    /**
     * The credit repository
     *
     * @var unknown
     */
    protected $creditRepository;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     * @param BankAccountCreditContract $creditContract
     */
    public function __construct(
        UserContract $userContract,
        BankAccountCreditContract $creditContract,
        AdminCreditAdjustmentQuery $creditAdjustmentQuery
    ) {
        $this->middleware('permission:admin_user_admin_management_adjust_credit_list')->only('index');
        $this->middleware('permission:admin_user_admin_management_adjust_credit')->only('edit', 'update');
        $this->middleware('permission:admin_user_admin_management_adjust_credit_export')->only('export');

        $this->creditRepository      = $creditContract;
        $this->userRepository        = $userContract;
        $this->creditAdjustmentQuery = $creditAdjustmentQuery;
    }

    /**
     * Get the credit adjustment history
     */
    public function index(Request $request)
    {
        $adjustments = $this->creditAdjustmentQuery
            ->setParameters($request->all())
            ->paginate();

        return view('credit::admin.management.adjustment.admin.index')->with(compact('adjustments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management_adjust_credit.user not found'));
        }

        return view('credit::admin.management.adjustment.admin.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Adjust $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management_adjust_credit.user not found'));
        }

        DB::beginTransaction();
        try {
            $remarks = $request->remarks;
            $adminId = auth()->user()->id;

            if ($request->amount < 0) {
                $txnCode = $this->creditRepository->deduct($request->credit_type, abs($request->amount), $id, 'credit_adjustment', $remarks, $adminId);
            } else {
                $txnCode = $this->creditRepository->add($request->credit_type, $request->amount, $id, 'credit_adjustment', $remarks, $adminId);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.credits.adjustment.edit', $id))->withMessage(__('a_admin_management_adjust_credit.failed to adjust user credit'));
        }

        return back()->with('success', __('a_admin_management_adjust_credit.user credit adjusted successfully'));
    }

    /**
     * Get the credit adjustment history
     */
    public function export(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->creditAdjustmentQuery->setParameters($request->all()), 'admin_credit_adjustments_' . $now . '.xlsx', auth()->user(), 'Export Cash Credit Statement on ' . $now);
    }
}
