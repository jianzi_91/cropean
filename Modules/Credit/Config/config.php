<?php

return [
    'name' => 'Credit',

    /*
     |--------------------------------------------------------------------------
     | Repository and Contract bindings
     |
     |--------------------------------------------------------------------------
     */
    'bindings' => [

        // Credits
        'Modules\Credit\Contracts\BankAccountContract'               => 'Modules\Credit\Repositories\BankAccountRepository',
        'Modules\Credit\Contracts\BankAccountCreditContract'         => 'Modules\Credit\Repositories\BankAccountCreditRepository',
        'Modules\Credit\Contracts\BankAccountStatementContract'      => 'Modules\Credit\Repositories\BankAccountStatementRepository',
        'Modules\Credit\Contracts\BankCreditTypeContract'            => 'Modules\Credit\Repositories\BankCreditTypeRepository',
        'Modules\Credit\Contracts\BankTransactionTypeContract'       => 'Modules\Credit\Repositories\BankTransactionTypeRepository',
        'Modules\Credit\Contracts\BankAccountCreditSnapshotContract' => 'Modules\Credit\Repositories\BankAccountCreditSnapshotRepository',
        'Modules\Credit\Contracts\CreditConversionContract'          => 'Modules\Credit\Repositories\CreditConversionRepository',
        'Modules\Credit\Contracts\CreditConversionStatusContract'    => 'Modules\Credit\Repositories\CreditConversionStatusRepository',
        'Modules\Credit\Contracts\CreditConversionInvoiceContract'   => 'Modules\Credit\Repositories\CreditConversionInvoiceRepository',
        'Modules\Credit\Contracts\CreditLockingContract'             => 'Modules\Credit\Repositories\CreditLockingRepository',
        'Modules\Credit\Contracts\CreditLockingLogContract'          => 'Modules\Credit\Repositories\CreditLockingLogRepository',
    ],

    /*
    |--------------------------------------------------------------------------
    | Credit Types
    |--------------------------------------------------------------------------
    |
    | This option controls the defaut credit types on the system.
    | They can be changed to any values as required in your application.
    | Format : [ slug => name ]
    |
    */
    'credit_types' => [
        'capital_credit'   => 'capital credit',
        'roll_over_credit' => 'roll over credit',
        'cash_credit'      => 'cash credit'
    ],

    /*
    |--------------------------------------------------------------------------
    | Deposit Credit Types
    |--------------------------------------------------------------------------
    |
    | This option controls the credit type which will be credited when users want to
    | deposit.
    |
    | Format : [ source type => credited type ]
    |
    */
    'deposit_credit_types' => [
        'capital_credit' => 'capital_credit',
    ],

    /*
    |--------------------------------------------------------------------------
    | Withdrawal Credit Types
    |--------------------------------------------------------------------------
    |
    | This option controls the credit type which will be debited when users want to
    | withdraw.
    |
    | Format : [ source type => deducted type ]
    |
    */
    'withdrawal_credit_types' => [
        'cash_credit' => 'cash_credit'
    ],

    /*
    |--------------------------------------------------------------------------
    | Convertible Credit Types
    |--------------------------------------------------------------------------
    |
    | This option controls the credit type which will be convertible when users want to
    | convert.
    |
    */
    'convertible_credit_types' => [
        'capital_credit'   => [
            'cash_credit',
        ],
        'roll_over_credit' => [
            'cash_credit',
        ],
        'cash_credit'      => [
            'roll_over_credit',
            'usdt_erc20',
            'usdc',
        ],
        'usdt_erc20' => [
            'capital_credit',
        ],
        'usdc' => [
            'capital_credit',
        ],
    ],

    /*
   |--------------------------------------------------------------------------
   | Credit Types Network
   |--------------------------------------------------------------------------
   |
   | This option states the credit type network
   |
   */
    'credit_network' => [
    ],
    /*
    |--------------------------------------------------------------------------
    | Transaction Types
    |--------------------------------------------------------------------------
    |
    | These are the default transaction types on the system
    | They can be changed to any values as required in your application.
    | Format : [ slug => name ]
    |
    */
    'transaction_types' => [
        //Credits module
        'credit_adjustment'       => 'Credit Adjustment by :member_id',
        'credit_transfer_from'    => 'User received :credit_type from :member_id',
        'credit_transfer_to'      => 'User transferred :credit_type to :member_id',
        'deposit'                 => 'Deposit',
        'deposit_fee'             => 'Deposit Fee',
        'withdrawal_request'      => 'Withdrawal Request',
        'withdrawal_fee'          => 'Withdrawal Fee',
        'withdrawal_refund'       => 'Withdrawal Refund',
        'withdrawal_fee_refund'   => 'Withdrawal Fee Refund',
        'withdrawal_cancel'       => 'Withdrawal Cancelled',
        'withdrawal_fee_cancel'   => 'Withdrawal Fee Cancelled',
        'withdrawal_rejected'     => 'Withdrawal Rejected',
        'withdrawal_fee_rejected' => 'Withdrawal Fee Rejected',
        'rebate'                  => 'Rebate Payout',
        'goldmine_bonus'          => 'Goldmine Bonus Payout',
        'sponsor_bonus'           => 'Sponsor Bonus Payout',
        'special_bonus'           => 'Special Bonus Payout',
        'leader_bonus'            => 'Leader Bonus Payout',
    ],

    /*
    |--------------------------------------------------------------------------
    | Transaction code
    |--------------------------------------------------------------------------
    |
    | Here you can configure the transaction code settings
    |
    */
    'transaction_code' => [
        'prefix'    => 'TXN',
        'generator' => 'Modules\Credit\Core\Generators\SimpleGenerator',
        'length'    => 20,
    ],

    /*
    |--------------------------------------------------------------------------
    | Account number
    |--------------------------------------------------------------------------
    |
    | Here you can configure the account number settings
    |
    */
    'account_number' => [
        'prefix'    => 'ACNT',
        'generator' => 'Modules\Credit\Core\Generators\SimpleGenerator',
        'length'    => 20,
    ],

    'precision' => [
        'default'            => env('CREDIT_DEFAULT_PRECISION', 2),
        'blockchain'         => env('CREDIT_BLOCKCHAIN_PRECISION', 8),
        'exchange_rate'      => env('CREDIT_EXCHANGE_RATE_PRECISION', 4),
        'percentage_decimal' => env('CREDIT_PERCENTAGE_PRECISION', 4),
        'calculation'        => env('CREDIT_CALCULATION_PRECISION', 20)
    ],

    /*
    |--------------------------------------------------------------------------
    | Transaction Types for Transfer
    |--------------------------------------------------------------------------
    |
    | These settings is used when transfering of credits
    |
    */
    'transfer' => [
        'from' => 'credit_transfer_to',
        'to'   => 'credit_transfer_from'
    ],

    /*
    |--------------------------------------------------------------------------
    | Adjustments
    |--------------------------------------------------------------------------
    |
    | Here you can configure the adjustment settings
    |
    */
    'adjustments' => [
        'min' => -999999999999,
        'max' => 999999999999,
    ],

    /*
    |--------------------------------------------------------------------------
    | Transfers
    |--------------------------------------------------------------------------
    |
    | Here you can configure the transfer settings
    |
    */
    'member_transfer' => [
        'min'      => env('CREDIT_MEMBER_TRANSFER_MIN', 0),
        'multiple' => env('CREDIT_MEMBER_TRANSFER_MULTIPLE', 0),
        'max'      => env('CREDIT_MEMBER_TRANSFER_MAX', 999999999999),
    ],
    'admin_transfer' => [
        'min'      => env('CREDIT_ADMIN_TRANSFER_MIN', 0),
        'multiple' => env('CREDIT_ADMIN_TRANSFER_MULTIPLE', 0),
        'max'      => env('CREDIT_ADMIN_TRANSFER_MAX', 999999999999),
    ],
    /*
    |--------------------------------------------------------------------------
    | Remarks
    |--------------------------------------------------------------------------
    |
    | Here you can configure the remarks settings
    |
    */
    'remarks' => [
        'max' => 5000,
    ],

    'invoice_storage' => 'credit_conversions',
];
