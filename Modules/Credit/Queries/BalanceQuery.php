<?php

namespace Modules\Credit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankAccountCredit;
use Modules\Rbac\Models\Role;
use QueryBuilder\QueryBuilder;

class BalanceQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $capitalCreditId   = get_bank_credit_type(BankCreditTypeContract::CAPITAL_CREDIT)->id;
        $rolloverCreditId  = get_bank_credit_type(BankCreditTypeContract::ROLL_OVER_CREDIT)->id;
        $cashCreditId      = get_bank_credit_type(BankCreditTypeContract::CASH_CREDIT)->id;
        $promotionCreditId = get_bank_credit_type(BankCreditTypeContract::PROMOTION_CREDIT)->id;
        $usdtCreditId      = get_bank_credit_type(BankCreditTypeContract::USDT_ERC20)->id;
        $usdcCreditId      = get_bank_credit_type(BankCreditTypeContract::USDC)->id;
        $adminRoles        = Role::admins()->get()->pluck('id')->toArray();
        $sysadminRole      = Role::where('name', Role::SYSADMIN)->first()->id;
        array_push($adminRoles, $sysadminRole);

        $query = BankAccountCredit::join('bank_accounts', 'bank_accounts.id', 'bank_account_credits.bank_account_id')
            ->join('users', 'users.id', 'bank_accounts.reference_id')
            ->join('assigned_roles', function ($join) use ($adminRoles) {
                $join->on('assigned_roles.entity_id', '=', 'users.id')
                    ->where('assigned_roles.entity_type', 'Modules\User\Models\User')
                    ->whereNotIn('assigned_roles.role_id', $adminRoles);
            })
            ->join('bank_credit_types', 'bank_credit_types.id', 'bank_account_credits.bank_credit_type_id')
            ->leftJoin('exchange_rates', 'exchange_rates.currency', 'bank_credit_types.name')
            ->where('users.is_system_generated', false)
            ->selectRaw("users.member_id, users.email, users.name as user_name,
            TRUNCATE(sum(if(bank_account_credits.bank_credit_type_id = {$capitalCreditId}, TRUNCATE(bank_account_credits.balance / exchange_rates.sell_rate, 2) , 0)) +
            sum(if(bank_account_credits.bank_credit_type_id = {$rolloverCreditId}, TRUNCATE(bank_account_credits.balance / exchange_rates.sell_rate, 2), 0)) + 
            sum(if(bank_account_credits.bank_credit_type_id = {$promotionCreditId}, TRUNCATE(bank_account_credits.balance, 2), 0)), 2) as total_equity,
            sum(if(bank_account_credits.bank_credit_type_id = {$capitalCreditId}, bank_account_credits.balance, 0)) as capital_credit,
            sum(if(bank_account_credits.bank_credit_type_id = {$rolloverCreditId}, bank_account_credits.balance, 0)) as rollover_credit,
            sum(if(bank_account_credits.bank_credit_type_id = {$cashCreditId}, bank_account_credits.balance, 0)) as cash_credit,
            sum(if(bank_account_credits.bank_credit_type_id = {$promotionCreditId}, bank_account_credits.balance, 0)) as promotion_credit,
            sum(if(bank_account_credits.bank_credit_type_id = {$usdtCreditId}, bank_account_credits.balance, 0)) as usdt_credit,
            sum(if(bank_account_credits.bank_credit_type_id = {$usdcCreditId}, bank_account_credits.balance, 0)) as usdc_credit")
            ->groupBy('users.id')
            ->orderBy('users.id', 'desc');

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
