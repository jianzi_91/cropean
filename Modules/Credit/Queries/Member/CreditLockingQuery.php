<?php

namespace Modules\Credit\Queries\Member;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Credit\Queries\CreditLockingQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class CreditLockingQuery extends BaseQuery implements WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'start_date' => [
            'filter'    => 'date_range',
            'table'     => 'credit_lockings',
            'column'    => 'start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'end_date' => [
            'filter'    => 'date_range',
            'table'     => 'credit_lockings',
            'column'    => 'end_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', auth()->user()->id);
    }

    public function map($conversion): array
    {
        return [
        ];
    }

    public function headings(): array
    {
        return [
        ];
    }
}
