<?php

namespace Modules\Credit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Credit\Models\BankAccountStatement;
use QueryBuilder\QueryBuilder;

class BankAccountStatementQuery extends QueryBuilder implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return BankAccountStatement::join('bank_accounts as from_bank_account', 'bank_account_statements.bank_account_id', '=', 'from_bank_account.id')
            ->with(['creditType', 'transaction', 'source', 'transfer', 'doneBy'])
            ->join('users', 'users.id', '=', 'from_bank_account.reference_id')
            ->selectRaw('bank_account_statements.*, users.name, users.member_id, users.email')
            ->orderBy('bank_account_statements.created_at', 'desc')
            ->orderBy('bank_account_statements.id', 'desc');
    }

    public function collection()
    {
        return $this->get();
    }
}
