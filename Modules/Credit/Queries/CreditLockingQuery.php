<?php

namespace Modules\Credit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Credit\Models\CreditLocking;
use QueryBuilder\QueryBuilder;

class CreditLockingQuery extends QueryBuilder implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return CreditLocking::join('users', 'users.id', 'credit_lockings.user_id')
            ->where('current_amount', '>', '0');
    }

    public function collection()
    {
        return $this->get();
    }
}
