<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Credit\Queries\CreditAdjustmentQuery as BaseQuery;
use Modules\Rbac\Models\Role;
use QueryBuilder\FilterBroker;

class MemberCreditAdjustmentQuery extends BaseQuery implements WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'bank_credit_type_id' => [
            'filter' => 'equal',
            'table'  => 'bank_account_statements',
            'column' => 'bank_credit_type_id'
        ],
        'bank_transaction_type_id' => [
            'filter' => 'equal',
            'table'  => 'bank_account_statements',
            'column' => 'bank_transaction_type_id'
        ],
        'email' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'email'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'member_id'
        ],
        'transaction_date' => [
            'filter' => 'date_range',
            'table'  => 'bank_account_statements',
            'column' => 'transaction_date'
        ],
        'transaction_code' => [
            'filter' => 'text',
            'table'  => 'bank_account_statements',
            'column' => 'transaction_code'
        ],
        'penalty_days' => [
            'filter' => 'equal',
            'table'  => 'credit_lockings',
            'column' => 'penalty_days'
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function beforeBuild()
    {
        return $this->builder->whereIn('roles.name', Role::members()->get()->pluck('name'))
            ->where('users.is_system_generated', 0);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function afterBuild()
    {   
        switch (request()->adjustment_type) {
            case 'credit':
                return $this->builder->where('bank_account_statements.credit', '!=', 0);
            case 'debit':
                return $this->builder->where('bank_account_statements.debit', '!=', 0);
            default:
                return $this->builder;
        }
    }

    public function map($statement): array
    {
        return [
            $statement->transaction_date,
            $statement->transaction_code,
            $statement->member_id,
            $statement->email,
            $statement->creditType->name,
            amount_format($statement->amount, credit_precision($statement->creditType->rawname)),
            $statement->debit != 0 ? __('a_admin_management_adjust_credit.adjust out') : __('a_admin_management_adjust_credit.adjust in'),
            isset($statement->penalty_days) ? $statement->penalty_days : '-',
            $statement->remarks,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_admin_management_adjust_credit.date'),
            __('a_admin_management_adjust_credit.transaction number'),
            __('a_admin_management_adjust_credit.member id'),
            __('a_admin_management_adjust_credit.email'),
            __('a_admin_management_adjust_credit.credit type'),
            __('a_admin_management_adjust_credit.amount'),
            __('a_admin_management_adjust_credit.adjustment type'),
            __('a_admin_management_adjust_credit.penalty days'),
            __('a_admin_management_adjust_credit.remarks'),
        ];
    }
}
