<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Queries\BalanceQuery as BaseQuery;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use QueryBuilder\FilterBroker;

class BalanceQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'name' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
    ];

    protected $exchangeRates;

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }

    public function map($statement): array
    {
        return [
            $statement->user_name,
            $statement->member_id,
            $statement->email,
            ' ' . amount_format($statement->total_equity ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($statement->capital_credit ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($statement->rollover_credit ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($statement->cash_credit ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($statement->usdt_credit ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($statement->usdc_credit ?? 0, credit_precision()) . ' ',
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_balance.name'),
            __('a_report_balance.member id'),
            __('a_report_balance.email'),
            __('a_report_balance.equity amount'),
            __('a_report_balance.current capital credit'),
            __('a_report_balance.current rollover credit'),
            __('a_report_balance.current cash credit'),
            __('a_report_balance.current usdt credit'),
            __('a_report_balance.current usdc credit'),
        ];
    }
}
