<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Credit\Queries\BankAccountStatementQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class BankAccountStatementQuery extends BaseQuery implements WithHeadings, WithMapping
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'bank_credit_type_id' => [
            'filter' => 'equal',
            'table'  => 'bank_account_statements',
            'column' => 'bank_credit_type_id'
        ],
        'bank_transaction_type_id' => [
            'filter' => 'equal',
            'table'  => 'bank_account_statements',
            'column' => 'bank_transaction_type_id'
        ],
        'username' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'username'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'member_id'
        ],
        'transaction_date' => [
            'filter' => 'date_range',
            'table'  => 'bank_account_statements',
            'column' => 'transaction_date'
        ],
        'transaction_code' => [
            'filter' => 'text',
            'table'  => 'bank_account_statements',
            'column' => 'transaction_code'
        ]
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function beforeBuild()
    {
        return $this->builder->where('bank_account_statements.bank_account_id', auth()->user()->bankAccount->id);
    }

    public function map($statement): array
    {
        return [
            $statement->transaction_date,
            $statement->transaction_code,
            display_transaction($statement),
            amount_format($statement->credit, credit_precision($statement->creditType->rawname)),
            amount_format($statement->debit, credit_precision($statement->creditType->rawname)),
            $statement->remarks,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_credits_statement.date'),
            __('a_credits_statement.transaction number'),
            __('a_credits_statement.transaction type'),
            __('a_credits_statement.credit'),
            __('a_credits_statement.debit'),
            __('a_credits_statement.remarks'),
        ];
    }
}
