<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Credit\Queries\CreditLockingQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class CreditLockingQuery extends BaseQuery implements WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'start_date' => [
            'filter'    => 'date_range',
            'table'     => 'credit_lockings',
            'column'    => 'start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'end_date' => [
            'filter'    => 'date_range',
            'table'     => 'credit_lockings',
            'column'    => 'end_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'name' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->whereDate('end_date', '>', now());
    }


    public function map($conversion): array
    {
        return [
        ];
    }

    public function headings(): array
    {
        return [
        ];
    }
}
