<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Credit\Queries\CreditConversionQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class CreditConversionQuery extends BaseQuery implements WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'credit_conversions',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'credit_conversion_status_id' => [
            'filter'    => 'select',
            'table'     => 'credit_conversions',
            'column'    => 'credit_conversion_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'source_credit_type_id' => [
            'filter'    => 'select',
            'table'     => 'credit_conversions',
            'column'    => 'source_credit_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'destination_credit_type_id' => [
            'filter'    => 'select',
            'table'     => 'credit_conversions',
            'column'    => 'destination_credit_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function afterBuild()
    {
        if (auth()->user()->cannot('admin_blockchain_transactions_view')) {
            return $this->builder->whereNotIn('source_credit_type_id', [bank_credit_type_id('usdt_erc20'), \bank_credit_type_id('usdc')])
                ->whereNotIn('destination_credit_type_id', [\bank_credit_type_id('usdt_erc20'), \bank_credit_type_id('usdc')]);
        }

        return $this->builder;
    }

    public function map($conversion): array
    {
        return [
            $conversion->created_at,
            $conversion->name,
            $conversion->member_id,
            $conversion->email,
            $conversion->sourceCreditType->name,
            $conversion->destinationCreditType->name,
            amount_format($conversion->source_amount, credit_precision()),
            amount_format($conversion->admin_fee, credit_precision()),
            amount_format($conversion->penalty_amount, credit_precision()),
            amount_format($conversion->destination_amount, credit_precision()),
        ];
    }

    public function headings(): array
    {
        return [
            __('a_conversion_list.request date'),
            __('a_conversion_list.full name'),
            __('a_conversion_list.member id'),
            __('a_conversion_list.email'),
            __('a_conversion_list.source credit type'),
            __('a_conversion_list.destination credit type'),
            __('a_conversion_list.source amount'),
            __('a_conversion_list.admin fee'),
            __('a_conversion_list.penalty fee'),
            __('a_conversion_list.destination amount'),
        ];
    }
}
