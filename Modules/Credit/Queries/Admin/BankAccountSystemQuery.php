<?php

namespace Modules\Credit\Queries\Admin;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Credit\Queries\BankAccountStatementQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class BankAccountSystemQuery extends BaseQuery implements WithHeadings, WithMapping
{
    use Exportable;
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'bank_credit_type_id' => [
            'filter'    => 'equal',
            'table'     => 'bank_account_statements',
            'column'    => 'bank_credit_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'bank_transaction_type_id' => [
            'filter'    => 'equal',
            'table'     => 'bank_account_statements',
            'column'    => 'bank_transaction_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'member_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'transaction_date' => [
            'filter'    => 'date_range',
            'table'     => 'bank_account_statements',
            'column'    => 'transaction_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'transaction_code' => [
            'filter'    => 'text',
            'table'     => 'bank_account_statements',
            'column'    => 'transaction_code',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function beforeBuild()
    {
        return $this->builder->where('users.is_system_generated', 0);
    }

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function afterBuild()
    {
        if (auth()->user()->cannot('admin_blockchain_transactions_view')) {
            return $this->builder->whereNotIn('bank_credit_type_id', [\bank_credit_type_id('usdt_erc20'), \bank_credit_type_id('usdc')])
                ->where(function ($query) {
                    $query->whereNotIn('to_bank_credit_type_id', [\bank_credit_type_id('usdt_erc20'), \bank_credit_type_id('usdc')])
                        ->orWhereNull('to_bank_credit_type_id');
                });
        }

        return $this->builder;
    }

    public function map($statement): array
    {
        return [
            $statement->transaction_date,
            $statement->transaction_code,
            $statement->name,
            $statement->email,
            $statement->member_id,
            display_transaction($statement),
            amount_format($statement->credit, credit_precision($statement->creditType->rawname)),
            amount_format($statement->debit, credit_precision($statement->creditType->rawname)),
            $statement->remarks,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_credits_system.date'),
            __('a_credits_system.transaction number'),
            __('a_credits_system.full name'),
            __('a_credits_system.email'),
            __('a_credits_system.member id'),
            __('a_credits_system.transaction type'),
            __('a_credits_system.credit'),
            __('a_credits_system.debit'),
            __('a_credits_system.remarks'),
        ];
    }
}
