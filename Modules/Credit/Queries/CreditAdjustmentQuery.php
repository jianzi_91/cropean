<?php

namespace Modules\Credit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Models\BankTransactionType;
use QueryBuilder\QueryBuilder;

class CreditAdjustmentQuery extends QueryBuilder implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $creditAdjustmentTransactionType = BankTransactionType::where('name', 'credit_adjustment')->first();

        return BankAccountStatement::join('bank_accounts', 'bank_account_statements.bank_account_id', '=', 'bank_accounts.id')
            ->join('users', 'users.id', '=', 'bank_accounts.reference_id')
            ->join('assigned_roles', function ($join) {
                $join->on('assigned_roles.entity_id', '=', 'users.id')
                    ->where('assigned_roles.entity_type', 'Modules\User\Models\User');
            })
            ->join('roles', function ($join) {
                $join->on('roles.id', '=', 'assigned_roles.role_id');
            })
            ->leftJoin('credit_lockings', 'credit_lockings.transaction_code', '=', 'bank_account_statements.transaction_code')
            ->where('bank_transaction_type_id', $creditAdjustmentTransactionType->id)
            ->selectRaw('bank_account_statements.*, users.name, users.member_id, users.email, credit - debit as amount, credit_lockings.penalty_days')
            ->orderBy('bank_account_statements.created_at', 'desc')
            ->orderBy('bank_account_statements.id', 'desc');
    }

    public function collection()
    {
        return $this->get();
    }
}
