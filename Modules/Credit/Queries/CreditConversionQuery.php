<?php

namespace Modules\Credit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Credit\Models\CreditConversion;
use QueryBuilder\QueryBuilder;

class CreditConversionQuery extends QueryBuilder implements FromCollection, ShouldAutoSize
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return CreditConversion::join('credit_conversion_status_histories', function ($join) {
            $join->on('credit_conversion_status_histories.credit_conversion_id', '=', 'credit_conversions.id')
                ->where('credit_conversion_status_histories.is_current', 1)
                ->whereNull('credit_conversion_status_histories.deleted_at');
        })
        ->join('credit_conversion_statuses', 'credit_conversion_statuses.id', '=', 'credit_conversion_status_histories.credit_conversion_status_id')
        ->with('status.status')
        ->join('users', 'users.id', '=', 'credit_conversions.user_id')
        ->orderBy('credit_conversions.created_at', 'DESC')
        ->select([
            'credit_conversions.*',
            'credit_conversion_statuses.name_translation AS status_name',
            'credit_conversion_statuses.id AS status_id',
            'users.name',
            'users.member_id',
            'users.email',
        ]);
    }

    public function collection()
    {
        return $this->get();
    }
}
