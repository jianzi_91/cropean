<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;

class CreditConversion extends Model implements Stateable
{
    use SoftDeletes, HistoryRelation;

    protected $fillable = [
        'user_id',
        'source_amount',
        'penalty_amount',
        'admin_fee',
        'admin_fee_percentage',
        'destination_amount',
        'source_credit_type_id',
        'destination_credit_type_id',
        'credit_conversion_status_id',
    ];

    /**
     * This model's relation to status histories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(CreditConversionStatusHistory::class, 'credit_conversion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status()
    {
        return $this->hasOne(CreditConversionStatusHistory::class, 'credit_conversion_id')->current(1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sourceCreditType()
    {
        return $this->hasOne(BankCreditType::class, 'id', 'source_credit_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function destinationCreditType()
    {
        return $this->hasOne(BankCreditType::class, 'id', 'destination_credit_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Relation to invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice()
    {
        return $this->hasOne(CreditConversionInvoice::class, 'credit_conversion_id');
    }

    /**
     * Get the state id.
     *
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'credit_conversion_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return CreditConversionStatusHistory::class;
    }

    /**
     * Approved status scope
     */
    public function scopeApproved($query)
    {
        $approvedStatus = CreditConversionStatus::where('name', 'approved')->first();

        return $query->where('credit_conversion_status_id', $approvedStatus->id);
    }
}
