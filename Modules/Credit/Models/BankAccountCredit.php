<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccountCredit extends Model
{
    public $timestamps = false;
    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
        'bank_account_id',
        'bank_credit_type_id',
        'balance',
    ];

    /**
     * This model's relation to credit type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * This model's relation to Bank Account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }
}
