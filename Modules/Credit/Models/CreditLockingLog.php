<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;

class CreditLockingLog extends Model
{
    protected $fillable = [
        'credit_locking_id',
        'amount',
        'penalty_fee_percentages',
        'transaction_code'
    ];

    protected $table   = "credit_lockings_logs";
}
