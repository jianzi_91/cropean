<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ConvertibleCreditType extends Pivot
{
    protected $guarded = [];
    protected $table   = "convertible_credit_types";
}
