<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;

    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
            'reference_id',
            'reference_type',
            'name',
            'account_number',
    ];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['statements'];

    /**
     * This model's relation to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'reference_id');
    }

    /**
     * This models' relation to Bank Account Credit Statements
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statements()
    {
        return $this->hasMany(BankAccountStatement::class, 'bank_account_id');
    }

    /**
     * This model's relation to Bank Account Credits
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->belongsToMany(BankCreditType::class, 'bank_account_credits', 'bank_account_id', 'bank_credit_type_id');
    }

    /**
     * This model's relation to bank account credits
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accountCredits()
    {
        return $this->hasMany(BankAccountCredit::class, 'bank_account_id');
    }
}
