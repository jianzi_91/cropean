<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class BankTransactionType extends Model
{
    use SoftDeletes, HasActiveScope, Translatable, HasDropDown;

    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'is_active',
    ];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['accountStatements'];

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * This model's relation to Credit Holder Statements
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accountStatements()
    {
        return $this->hasMany(BankAccountStatement::class, 'bank_transaction_type_id');
    }
}
