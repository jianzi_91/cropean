<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccountStatement extends Model
{
    use SoftDeletes;

    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
        'transaction_date',
        'transaction_code',
        'bank_transaction_type_id',
        'bank_credit_type_id',
        'bank_account_id',
        'transfer_account_id',
        'done_by',
        'remarks',
        'credit',
        'debit',
    ];

    /**
     * This model's relation to credit type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * This model's relation to transaction type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction()
    {
        return $this->belongsTo(BankTransactionType::class, 'bank_transaction_type_id');
    }

    /**
     * This model's relation to credit source bank account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function source()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

    /**
     * This model's relation to done by bank account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function doneBy()
    {
        return $this->belongsTo(BankAccount::class, 'done_by');
    }

    /**
     * This model's relation to credit transfer bank account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transfer()
    {
        return $this->belongsTo(BankAccount::class, 'transfer_account_id');
    }

    /**
     * This model's relation to credit transfer bank account
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditLocking()
    {
        return $this->hasOne(CreditLocking::class, 'transaction_code', 'transaction_code');
    }
}
