<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccountCreditSnapshot extends Model
{
    use SoftDeletes;

    protected $table = 'bank_account_credit_snapshots';

    protected $fillable = [
        'bank_account_id',
        'bank_credit_type_id',
        'balance',
        'run_date',
    ];

    /**
     * This model's relation to bank account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

    /**
     * This model's relation to bank credit type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bankCreditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }
}
