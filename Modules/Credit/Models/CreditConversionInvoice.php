<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;

class CreditConversionInvoice extends Model
{
    protected $table   = "credit_conversion_invoices";
    protected $guarded = [];
}
