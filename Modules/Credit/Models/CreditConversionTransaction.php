<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditConversionTransaction extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $table   = 'credit_conversion_transactions';
}
