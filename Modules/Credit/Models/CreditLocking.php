<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditLocking extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'transaction_code',
        'penalty_days',
        'penalty_fee_percentages',
        'start_date',
        'end_date',
        'original_amount',
        'current_amount'
    ];

    protected $table   = "credit_lockings";
}
