<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class CreditConversionStatusHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'credit_conversion_status_id',
        'credit_conversion_id',
        'remarks',
        'is_current',
        'updated_by',
    ];

    /**
     * This model's relation to user status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(CreditConversionStatus::class, 'credit_conversion_status_id');
    }

    /**
     * Local scope for getting current status.
     *
     * @param unknown $query
     * @param unknown $current
     *
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where($this->getTable() . '.is_current', $current);
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
