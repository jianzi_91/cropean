<?php

namespace Modules\Credit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Blockchain\Models\BlockchainNode;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class BankCreditType extends Model
{
    use SoftDeletes, HasActiveScope, Translatable, HasDropDown;

    /**
     * Fillable columns
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'is_active',
    ];

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['accountStatements'];

    /**
     * This model's relation to Bank Account Statements
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function canWithdraw()
    {
        return $this->can_withdraw;
    }

    /**
     * This model's relation to Bank Account Statements
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accountStatements()
    {
        return $this->hasMany(BankAccountStatement::class, 'bank_credit_type_id');
    }

    /**
     * This model's relation to Blockchain Nodes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blockchainNodes()
    {
        return $this->belongsToMany(BlockchainNode::class, 'bank_credit_type_blockchain_nodes')->wherePivot('is_active', true);
    }

    /**
     * Filter credit type under btc blockchain network
     *
     * return $query
     */
    public function scopeBtcNetwork($query)
    {
        return $query->where('network', 'BTC');
    }

    /**
     * Filter credit type under eth blockchain network
     *
     * return $query
     */
    public function scopeEthNetwork($query)
    {
        return $query->where('network', 'ETH');
    }

    /**
     * This model's relation to itself through ConvertibleCreditType as pivot
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function canConvertFromCredits()
    {
        return $this->belongsToMany(BankCreditType::class, ConvertibleCreditType::class, 'to_credit_type_id', 'from_credit_type_id')->wherePivot('is_active', true)->withPivot('minimum', 'multiple', 'admin_fee_percentage');
    }

    /**
     * This model's relation to itself through ConvertibleCreditType as pivot
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function canConvertToCredits()
    {
        return $this->belongsToMany(BankCreditType::class, ConvertibleCreditType::class, 'from_credit_type_id', 'to_credit_type_id')->wherePivot('is_active', true)->withPivot('minimum', 'multiple', 'admin_fee_percentage');
    }
}
