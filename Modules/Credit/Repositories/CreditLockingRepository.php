<?php

namespace Modules\Credit\Repositories;

use Carbon\Carbon;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Credit\Contracts\CreditLockingLogContract;
use Modules\Credit\Models\CreditLocking;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class CreditLockingRepository extends Repository implements CreditLockingContract
{
    use HasCrud;

    /**
     * @var CreditLockingLogContract
     */
    protected $creditLockingLogRepository;

    public function __construct(
        CreditLocking $model,
        CreditLockingLogContract $creditLockingLogContract
    ) {
        $this->creditLockingLogRepository = $creditLockingLogContract;

        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see Modules\Credit\Contracts\CreditConversionContract::calculateAdminFee()
     */
    public function updateCreditLock($sourceAmount, $userId, $update = false)
    {
        $totalPenaltyAmount = 0;
        $todayDate          = Carbon::now();
        $penaltyPercentage  = 0;

        $creditLocks = $this->getAvailableLocks($todayDate, $userId);

        foreach ($creditLocks as $creditLock) {
            $penalties = json_decode($creditLock->penalty_fee_percentages, true);
            ksort($penalties);
            $startDate = Carbon::parse($creditLock->start_date)->startOfDay();
            $endDate   = Carbon::parse($creditLock->end_date)->endOfDay();
            $diffDay   = $startDate->diffInDays($todayDate->startOfDay());

            if ($creditLock->current_amount < $sourceAmount) {
                if ($update) {
                    $this->edit($creditLock->id, ['current_amount' => 0]);
                }
                $amount       = $creditLock->current_amount;
                $sourceAmount = bcsub($sourceAmount, $creditLock->current_amount, credit_precision());
            } else {
                if ($update) {
                    $this->edit($creditLock->id, ['current_amount' => bcsub($creditLock->current_amount, $sourceAmount, credit_precision())]);
                }
                $amount       = $sourceAmount;
                $sourceAmount = 0;
            }

            foreach ($penalties as $key => $value) {
                if ($key > $diffDay) {
                    $penaltyPercentage = $value;
                    break;
                }
            }

            if ($endDate->lt($todayDate)) {
                $penaltyPercentage = 0;
            }

            if ($update) {
                $this->creditLockingLogRepository->add([
                    'credit_locking_id'       => $creditLock->id,
                    'amount'                  => $amount,
                    'penalty_fee_percentages' => $penaltyPercentage,
                    'transaction_code'        => $creditLock->transaction_code,
                ]);
            }

            $totalPenaltyAmount = bcadd($totalPenaltyAmount, bcmul($penaltyPercentage, $amount, credit_precision()), credit_precision());

            if ($sourceAmount == 0) {
                break;
            }
        }

        return $totalPenaltyAmount;
    }

    public function getAvailableLocks($todayDate, $userId)
    {
        return $this->model
            ->where('user_id', '=', $userId)
            ->where('current_amount', '<>', 0)
            ->orderBy('start_date', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->orderBy('id', 'ASC')
            ->get();
    }
}
