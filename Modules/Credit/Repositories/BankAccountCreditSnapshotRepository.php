<?php

namespace Modules\Credit\Repositories;

use Modules\Credit\Contracts\BankAccountCreditSnapshotContract;
use Modules\Credit\Models\BankAccountCreditSnapshot;
use Modules\Credit\Models\BankCreditType;
use Modules\Rbac\Models\Role;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class BankAccountCreditSnapshotRepository extends Repository implements BankAccountCreditSnapshotContract
{
    use HasCrud;

    /**
     * Class Constructor
     * @param BankCreditType $model
     */
    public function __construct(BankAccountCreditSnapshot $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function getTotalCredits($snapshotDate, $creditTypes = [])
    {
        $roleIds = Role::members()->pluck('id')->toArray();
        $query   = [];
        foreach ($creditTypes as $creditTypeId => $creditTypeName) {
            $query[] = "sum(if(bank_account_credit_snapshots.bank_credit_type_id = $creditTypeId, bank_account_credit_snapshots.balance, 0)) as $creditTypeName";
        }

        return $this->model->join('bank_accounts', 'bank_accounts.id', 'bank_account_credit_snapshots.bank_account_id')
            ->join('users', 'users.id', 'bank_accounts.reference_id')
            ->join('assigned_roles', 'assigned_roles.entity_id', 'users.id')
            ->join('bank_credit_types', 'bank_credit_types.id', 'bank_account_credit_snapshots.bank_credit_type_id')
            ->whereIn('assigned_roles.role_id', $roleIds)
            ->where('bank_account_credit_snapshots.run_date', $snapshotDate)
            ->whereIn('bank_account_credit_snapshots.bank_credit_type_id', array_keys($creditTypes))
            ->selectRaw("users.id as user_id, " . implode(', ', $query))
            ->groupBy('bank_accounts.id')
            ->get()
            ->keyBy('user_id')
            ->toArray();
    }
}
