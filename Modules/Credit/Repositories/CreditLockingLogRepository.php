<?php

namespace Modules\Credit\Repositories;

use Modules\Credit\Contracts\CreditLockingLogContract;
use Modules\Credit\Models\CreditLockingLog;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class CreditLockingLogRepository extends Repository implements CreditLockingLogContract
{
    use HasCrud;

    public function __construct(CreditLockingLog $model)
    {
        parent::__construct($model);
    }
}
