<?php

namespace Modules\Credit\Repositories;

use Carbon\Carbon;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankAccountStatementContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionInvoiceContract;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Credit\Contracts\CreditLockingLogContract;
use Modules\Credit\Models\ConvertibleCreditType;
use Modules\Credit\Models\CreditConversion;
use Modules\Credit\Models\CreditConversionTransaction;
use Modules\Credit\Traits\ValidateCredit;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Repository;

class CreditConversionRepository extends Repository implements CreditConversionContract
{
    use HasCrud, ValidateCredit, HasHistory;

    /**
     * Credit Conversion Transaction Model
     *
     */
    protected $creditConversionTransactionModel;

    /**
     * Bank Credit Type Repository
     *
     */
    protected $bankCreditTypeRepository;

    /**
     * Exchange Rate Repository
     *
     */
    protected $exchangeRateRepository;

    /**
     * The bank account credit repository
     *
     * @var unknown
     */
    protected $bankAccountCreditRepository;

    protected $bankAccountStatementRepository;

    protected $creditLockingRepository;

    /**
     * @var BankTransactionTypeContract
     */
    protected $bankTransactionTypeContract;

    /**
     * Class constructor
     * @param CreditConversion $model
     * @param CreditConversionTransaction $creditConversionTransactionModel
     * @param CreditLockingContract $creditLockingContract
     * @param CreditLockingLogContract $creditLockingLogContract
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param BankTransactionTypeContract $bankTransactionTypeContract
     * @param ExchangeRateContract $exchangeRateContract
     * @param BankAccountCreditContract $bankAccountCreditContract
     * @param BankAccountStatementContract $bankAccountStatementContract
     */
    public function __construct(
        CreditConversion $model,
        CreditConversionTransaction $creditConversionTransactionModel,
        CreditLockingContract $creditLockingContract,
        BankCreditTypeContract $bankCreditTypeContract,
        BankTransactionTypeContract $bankTransactionTypeContract,
        ExchangeRateContract $exchangeRateContract,
        BankAccountCreditContract $bankAccountCreditContract,
        BankAccountStatementContract $bankAccountStatementContract,
        CreditConversionInvoiceContract $invoiceContract
    ) {
        $this->creditConversionTransactionModel = $creditConversionTransactionModel;
        $this->bankTransactionTypeContract      = $bankTransactionTypeContract;
        $this->creditLockingRepository          = $creditLockingContract;
        $this->bankCreditTypeRepository         = $bankCreditTypeContract;
        $this->exchangeRateRepository           = $exchangeRateContract;
        $this->bankAccountCreditRepository      = $bankAccountCreditContract;
        $this->bankAccountStatementRepository   = $bankAccountStatementContract;
        $this->invoiceRepo                      = $invoiceContract;

        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::calculateAdminFee()
     */
    public function calculateAdminFee($sourceCreditType, $sourceAmount, $setting = false)
    {
        $percentage = setting("credit_conversion_{$sourceCreditType}_admin_fee_percentage");

        $percentageFee = bcmul($percentage, $sourceAmount, credit_precision());

        if ($setting) {
            return $percentage;
        }

        return $percentageFee;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::calculateAmount()
     */
    public function calculateAmount($amount, $rate, $precision = 2)
    {
        //Fee inclusive
        return bcmul($amount, $rate, $precision);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::calculateConvertAmount()
     */
    public function calculateConvertAmount($amount, $rate, $precision = 2)
    {
        //Fee inclusive
        return bcdiv($amount, $rate, $precision);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::convertAmount()
     */
    public function convertAmount($amount, $adminFee, $precision = 2)
    {
        return bcsub($amount, $adminFee, $precision);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::request()
     */
    public function request($request, $remarks = null)
    {
        $data = $this->calculate($request);

        $conversion = $this->addConversion(
            $data['userId'],
            $data['sourceAmount'],
            $data['penaltyAmount'],
            $data['adminFee'],
            $data['adminFeeSetting'],
            $data['issueAmount'],
            $data['fromCreditType']['id'],
            $data['toCreditType']['id']
        );

        if ($conversion) {
            if ($data['adminFee'] > 0) {
                $this->addFee($conversion, $data['adminFee'], $data['fromCreditType']['rawname'], $data['exchangeRate']);
            }
            if ($data['penaltyAmount'] > 0) {
                $this->addFee($conversion, $data['penaltyAmount'], $data['fromCreditType']['rawname'], $data['exchangeRate'], '', 'credit_conversion_penalty_fee');
            }

            $deductedFromAmount    = bcsub($data['sourceAmount'], bcadd($data['adminFee'], $data['penaltyAmount'], credit_precision($data['fromCreditType']['rawname'])), credit_precision($data['fromCreditType']['rawname']));
            $conversionFromTxnCode = $this->bankAccountCreditRepository->deduct($data['fromCreditType']['rawname'], $deductedFromAmount, $data['userId'], $this->bankTransactionTypeContract::CREDIT_CONVERSION_FROM, $remarks);
            $this->addCreditConversionTransaction(
                $conversion,
                -$data['convertAmount'],
                $conversionFromTxnCode,
                $this->bankTransactionTypeContract::CREDIT_CONVERSION_FROM,
                $data['exchangeRate'],
                $data['fromCreditType']['rawname']
            );

            // Update destination for from credit type.
            $this->bankAccountStatementRepository->updateToCreditTypeIdUsingTxnCode($conversionFromTxnCode, $data['toCreditType']['id']);
        }

        return $conversion;
    }

    public function approve($conversionId)
    {
        $conversion = $this->find($conversionId);
        $request    = [
            'user_id'                  => $conversion->user_id,
            'amount'                   => $conversion->source_amount,
            'credit_type_convert_from' => $conversion->source_credit_type_id,
            'credit_type_convert_to'   => $conversion->destination_credit_type_id,
        ];
        $data = $this->calculate($request);

        $conversionToTxnCode = $this->bankAccountCreditRepository->add($data['toCreditType']['rawname'], $data['issueAmount'], $data['userId'], $this->bankTransactionTypeContract::CREDIT_CONVERSION_TO);
        $this->addCreditConversionTransaction(
            $conversion,
            $data['convertedAmount'],
            $conversionToTxnCode,
            $this->bankTransactionTypeContract::CREDIT_CONVERSION_TO,
            $data['issueExchangeRate'],
            $data['toCreditType']['rawname']
        );

        // Lock credit if the credit being converted to capital credit
        $capitalCreditId = $this->bankCreditTypeRepository->findBySlug($this->bankCreditTypeRepository::CAPITAL_CREDIT)->id;

        //only if credit type = capital credits penalty fee apply and update
        if ($data['fromCreditType']['id'] === $capitalCreditId) {
            $this->creditLockingRepository->updateCreditLock($data['sourceAmount'], $data['userId'], true);
        }

        if ($conversion->destination_credit_type_id === $capitalCreditId) {
            $penaltyDuration   = (setting('penalty_duration'));
            $penaltyPercentage = (setting('penalty_percentage'));

            $this->creditLockingRepository->add([
                'user_id'                 => $conversion->user_id,
                'transaction_code'        => $conversionToTxnCode,
                'penalty_days'            => $penaltyDuration,
                'penalty_fee_percentages' => $penaltyPercentage,
                'start_date'              => Carbon::now(),
                'end_date'                => Carbon::now()->addDay($penaltyDuration - 1),
                'original_amount'         => $conversion->destination_amount,
                'current_amount'          => $conversion->destination_amount,
            ]);
        }

        // Update destination for to credit type.
        $this->bankAccountStatementRepository->updateToCreditTypeIdUsingTxnCode($conversionToTxnCode, $data['fromCreditType']['id']);

        if ($conversion->destination_credit_type_id == $capitalCreditId) {
            $this->invoiceRepo->generate($conversion);
        }

        return $conversion;
    }

    public function reject($conversionId)
    {
        $conversion            = $this->find($conversionId);
        $conversionTransaction = CreditConversionTransaction::where('credit_conversion_id', $conversionId)
            ->where('bank_transaction_type_id', bank_transaction_type_id(BankTransactionTypeContract::CREDIT_CONVERSION_FROM))
            ->first();

        $refundTxnCode = $this->bankAccountCreditRepository->add(\bank_credit_type_name($conversion->source_credit_type_id), $conversion->destination_amount, $conversion->user_id, BankTransactionTypeContract::CREDIT_CONVERSION_REJECTED);
        $this->addCreditConversionTransaction(
            $conversion,
            $conversion->destination_amount,
            $refundTxnCode,
            BankTransactionTypeContract::CREDIT_CONVERSION_REJECTED,
            $conversionTransaction->exchange_rate,
            bank_credit_type_name($conversion->source_credit_type_id)
        );

        if ($conversion->admin_fee != 0) {
            $conversionFeeTransaction = CreditConversionTransaction::where('credit_conversion_id', $conversionId)
                ->where('bank_transaction_type_id', \bank_transaction_type_id(BankTransactionTypeContract::CREDIT_CONVERSION_FEE))
                ->first();

            $feeRefundTxnCode = $this->bankAccountCreditRepository->add(\bank_credit_type_name($conversion->source_credit_type_id), $conversion->admin_fee, $conversion->user_id, BankTransactionTypeContract::CREDIT_CONVERSION_FEE_REJECTED);
            $this->addCreditConversionTransaction(
                $conversion,
                $conversion->admin_fee,
                $feeRefundTxnCode,
                BankTransactionTypeContract::CREDIT_CONVERSION_FEE_REJECTED,
                $conversionFeeTransaction->exchange_rate,
                bank_credit_type_name($conversion->source_credit_type_id)
            );
        }

        return $conversion;
    }

    public function cancel($conversionId)
    {
        $conversion            = $this->find($conversionId);
        $conversionTransaction = CreditConversionTransaction::where('credit_conversion_id', $conversionId)
            ->where('bank_transaction_type_id', bank_transaction_type_id(BankTransactionTypeContract::CREDIT_CONVERSION_FROM))
            ->first();

        $refundTxnCode = $this->bankAccountCreditRepository->add(\bank_credit_type_name($conversion->source_credit_type_id), $conversion->source_amount, $conversion->user_id, BankTransactionTypeContract::CREDIT_CONVERSION_CANCELLED);
        $this->addCreditConversionTransaction(
            $conversion,
            $conversion->source_amount,
            $refundTxnCode,
            BankTransactionTypeContract::CREDIT_CONVERSION_CANCELLED,
            $conversionTransaction->exchange_rate,
            bank_credit_type_name($conversion->source_credit_type_id)
        );

        if ($conversion->admin_fee != 0) {
            $conversionFeeTransaction = CreditConversionTransaction::where('credit_conversion_id', $conversionId)
                ->where('bank_transaction_type_id', \bank_transaction_type_id(BankTransactionTypeContract::CREDIT_CONVERSION_FEE))
                ->first();

            $feeRefundTxnCode = $this->bankAccountCreditRepository->add(\bank_credit_type_name($conversion->source_credit_type_id), $conversion->admin_fee, $conversion->user_id, BankTransactionTypeContract::CREDIT_CONVERSION_FEE_CANCELLED);
            $this->addCreditConversionTransaction(
                $conversion,
                $conversion->admin_fee,
                $feeRefundTxnCode,
                BankTransactionTypeContract::CREDIT_CONVERSION_FEE_CANCELLED,
                $conversionFeeTransaction->exchange_rate,
                bank_credit_type_name($conversion->source_credit_type_id)
            );
        }

        return $conversion;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\CreditConversionContract::addFee()
     */
    public function addFee($conversion, $amount, $creditType, $exchangeRate, $remarks = null, $transactionType = 'credit_conversion_fee')
    {
        $feeTxnCode = $this->bankAccountCreditRepository->deduct($creditType, $amount, $conversion->user_id, $transactionType, $remarks);
        if (!$feeTxnCode) {
            throw new \Exception('Can\'t deduct credit conversion fee credits');
        }

        $transactionFee                           = new $this->creditConversionTransactionModel;
        $transactionFee->amount                   = $amount;
        $transactionFee->transaction_code         = $feeTxnCode;
        $transactionFee->bank_transaction_type_id = bank_transaction_type_id($transactionType);
        $transactionFee->credit_conversion_id     = $conversion->id;
        $transactionFee->bank_credit_type_id      = bank_credit_type_id($creditType);
        $transactionFee->exchange_rate            = $exchangeRate;

        return $transactionFee->save();
    }

    public function getConvertCreditDetails($request)
    {
        $creditFrom = $this->bankCreditTypeRepository->find($request['credit_type_convert_from'])->rawname;
        $creditTo   = $this->bankCreditTypeRepository->find($request['credit_type_convert_to']);

        $creditBaseCurrency = $this->exchangeRateRepository->findBySlug($this->exchangeRateRepository::USD_CREDIT);
        $creditCurrencyFrom = $this->exchangeRateRepository->findBySlug($creditFrom);
        $creditCurrencyTo   = $this->exchangeRateRepository->findBySlug($creditTo->rawname);

        if (isset($creditFrom) && isset($creditTo)) {
            $data = [
                'id'                      => $creditTo->id,
                'currency_from'           => $creditCurrencyFrom->currency,
                'currency_from_sell_rate' => $creditCurrencyFrom->sell_rate,
                'currency_to'             => $creditCurrencyTo->currency,
                'currency_to_buy_rate'    => $creditCurrencyTo->buy_rate,
                'base_currency'           => $creditBaseCurrency->currency,
                'base_currency_rate'      => '1.00',
            ];
        } else {
            $data = [
                'id'                      => '',
                'currency_from'           => '',
                'currency_from_sell_rate' => '',
                'currency_to'             => '',
                'currency_to_buy_rate'    => '',
                'base_currency'           => '',
                'base_currency_rate'      => '',
            ];
        }

        return $data;
    }

    public function calculate($request)
    {
        $userId       = $request['user_id'];
        $sourceAmount = $request['amount'];

        $fromCreditType = $this->bankCreditTypeRepository->find($request['credit_type_convert_from']);
        $toCreditType   = $this->bankCreditTypeRepository->find($request['credit_type_convert_to']);

        $exchangeRate = $this->exchangeRateRepository->findBySlug($fromCreditType->rawname)->sell_rate;

        $issueExchangeRate = $this->exchangeRateRepository->findBySlug($toCreditType->rawname)->buy_rate;

        $penaltyAmount   = 0;
        $adminFee        = 0;
        $adminFeeSetting = 0;

        $capitalCreditId = $this->bankCreditTypeRepository->findBySlug($this->bankCreditTypeRepository::CAPITAL_CREDIT)->id;

        //only if credit type = capital credits penalty fee apply here will not update
        if ($fromCreditType->id === $capitalCreditId) {
            $penaltyAmount = $this->creditLockingRepository->updateCreditLock($sourceAmount, $userId);
        }

        $conversionSetting = ConvertibleCreditType::where('from_credit_type_id', $fromCreditType->id)
            ->where('to_credit_type_id', $toCreditType->id)
            ->first();

        $adminFeeSetting = $conversionSetting->admin_fee_percentage;
        $adminFee        = bcmul($sourceAmount, $adminFeeSetting, 2);

        //convertAmount is subtract by adminFee and penaltyAmount
        $convertAmount = $this->convertAmount($sourceAmount, $adminFee, credit_precision());
        $convertAmount = $this->convertAmount($convertAmount, $penaltyAmount, credit_precision());

        $convertedAmount = $this->calculateConvertAmount($convertAmount, $exchangeRate, credit_precision());

        $issueAmount = $this->calculateAmount($convertedAmount, $issueExchangeRate, credit_precision());

        $data = [
            'userId'            => $userId,
            'sourceAmount'      => $sourceAmount,
            'penaltyAmount'     => $penaltyAmount,
            'adminFee'          => $adminFee,
            'adminFeeSetting'   => $adminFeeSetting,
            'convertAmount'     => $convertAmount,
            'convertedAmount'   => $convertedAmount,
            'issueAmount'       => $issueAmount,
            'exchangeRate'      => $exchangeRate,
            'issueExchangeRate' => $issueExchangeRate,
            'fromCreditType'    => $fromCreditType,
            'toCreditType'      => $toCreditType,
        ];

        return $data;
    }

    public function getConvertibleCreditTypes($fromCreditType)
    {
        $creditType             = $this->bankCreditTypeRepository->find($fromCreditType)->rawname;
        $convertableCreditTypes = [];

        switch ($creditType) {
            case BankCreditTypeContract::CAPITAL_CREDIT:
                $convertableCreditTypes = [BankCreditTypeContract::CASH_CREDIT];
                break;
            case BankCreditTypeContract::ROLL_OVER_CREDIT:
                $convertableCreditTypes = [BankCreditTypeContract::CASH_CREDIT];
                break;
            case BankCreditTypeContract::CASH_CREDIT:
                $convertableCreditTypes = [BankCreditTypeContract::ROLL_OVER_CREDIT, BankCreditTypeContract::USDT_ERC20, BankCreditTypeContract::USDC];
                break;
            case BankCreditTypeContract::USDT_ERC20:
            case BankCreditTypeContract::USDC:
                $convertableCreditTypes = [BankCreditTypeContract::CAPITAL_CREDIT];
                break;
            default:
                $convertableCreditTypes = [];
                break;
        }

        return $this->bankCreditTypeRepository->getModel()
            ->whereIn('name', $convertableCreditTypes)
            ->get();
    }

    /**
     * add credit conversion record
     * @param int $userId
     * @param float $rate
     * @param float $convertAmount
     * @param float $convertedAmount
     * @param float $adminFee
     * @param $adminFeeSetting
     * @param int $fromCreditTypeId
     * @param int $toCreditTypeId
     * @return CreditConversion
     */
    protected function addConversion($userId, $sourceAmount, $penaltyAmount, $adminFee, $adminFeeSetting, $issueAmount, $fromCreditTypeId, $toCreditTypeId)
    {
        $conversion                             = new $this->model;
        $conversion->user_id                    = $userId;
        $conversion->source_amount              = $sourceAmount;
        $conversion->penalty_amount             = $penaltyAmount;
        $conversion->admin_fee                  = $adminFee;
        $conversion->admin_fee_percentage       = $adminFeeSetting;
        $conversion->destination_amount         = $issueAmount;
        $conversion->source_credit_type_id      = $fromCreditTypeId;
        $conversion->destination_credit_type_id = $toCreditTypeId;

        return $conversion->save() ? $conversion : null;
    }

    /**
     * Add credit conversion transaction record
     *
     * @param CreditConversion $creditConversionId
     * @param int $creditTypeId
     * @param string $conversionTxnCode
     * @param string $conversionTxnId
     * @return CreditConversionTransaction
     * @throws \Exception
     */
    protected function addCreditConversionTransaction($conversion, $amount, $txnCode, $transactionType, $exchangeRate, $creditType)
    {
        $transaction                           = new $this->creditConversionTransactionModel;
        $transaction->credit_conversion_id     = $conversion->id;
        $transaction->amount                   = $amount;
        $transaction->transaction_code         = $txnCode;
        $transaction->bank_transaction_type_id = get_bank_transaction_type($transactionType)->id;
        $transaction->exchange_rate            = $exchangeRate;
        $transaction->bank_credit_type_id      = get_bank_credit_type($creditType)->id;

        if (!$transaction->save()) {
            throw new \Exception('Failed saving credit conersion transaction');
        }

        return $transaction;
    }
}
