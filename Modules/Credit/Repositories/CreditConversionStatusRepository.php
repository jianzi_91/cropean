<?php

namespace Modules\Credit\Repositories;

use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Models\CreditConversionStatus;
use Modules\Credit\Models\CreditConversionStatusHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class CreditConversionStatusRepository extends Repository implements CreditConversionStatusContract
{
    use HasCrud, HasSlug, HasHistory;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param CreditConversionStatus  $model
     * @param CreditConversionStatusHistory  $model
     */
    public function __construct(CreditConversionStatus $model, CreditConversionStatusHistory $history)
    {
        $this->model        = $model;
        $this->historyModel = $history;

        parent::__construct($model);
    }
}
