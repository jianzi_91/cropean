<?php

namespace Modules\Credit\Repositories;

use Illuminate\Support\Facades\Storage;
use Modules\Credit\Contracts\CreditConversionInvoiceContract;
use Modules\Credit\Models\CreditConversion;
use Modules\Credit\Models\CreditConversionInvoice;
use PDF;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class CreditConversionInvoiceRepository extends Repository implements CreditConversionInvoiceContract
{
    use HasCrud, HasSlug;
    // implement functions here

    /**
     * Class constructor.
     */
    public function __construct(
        CreditConversionInvoice $model
    ) {
        $this->model = $model;
        $this->slug  = "reference_number";

        parent::__construct($model);
    }

    public function generate(CreditConversion $conversion)
    {
        $data = [
            'credit_conversion_id' => $conversion->id,
            'name'                 => $conversion->user->name,
            'member_id'            => $conversion->user->member_id,
            'reference_number'     => generate_ref('DEP'),
            'amount'               => amount_format($conversion->destination_amount),
        ];

        $invoice = $this->add($data);

        $data['transaction_date'] = now()->format('d/m/Y');

        $view = "deposit::invoice_" . $conversion->user->locale;
        $pdf  = PDF::setOptions(['isRemoteEnabled' => true,  'isHtml5ParserEnabled' => true, 'defaultFont' => 'simhei', 'fontDir' => storage_path('fonts/')])
                ->loadView($view, compact('data'))
                ->setPaper('A4', 'portrait')
                ->download();

        $path     = config('credit.invoice_storage');
        $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $path;

        if (config('filesystems.cloud_enable')) {
            Storage::cloud()->put($path . DIRECTORY_SEPARATOR . $data['reference_number'] . '.pdf', $pdf);
        }

        Storage::put($path . DIRECTORY_SEPARATOR . $data['reference_number'] . '.pdf', $pdf);

        return $invoice;
    }
}
