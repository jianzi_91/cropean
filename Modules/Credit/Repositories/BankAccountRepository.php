<?php

namespace Modules\Credit\Repositories;

use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Models\BankAccount;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Traits\ValidateAccount;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class BankAccountRepository extends Repository implements BankAccountContract
{
    use ValidateAccount, HasCrud;

    /**
     * Generator instance
     * @var unknown
     */
    protected $generator;

    /**
     * Credit Type Instance
     * @var unknown
     */
    protected $creditType;

    /**
     * Class constructor
     * @param BankAccount $model
     * @param BankCreditType $creditType
     */
    public function __construct(BankAccount $model, BankCreditType $creditType)
    {
        $this->creditType = $creditType;
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::findByAccountNum()
     */
    public function findByAccountNum($accountNum, array $with = [], $select = ['*'])
    {
        return $this->model->where('account_number', $accountNum)->with($with)->first($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::findByReferenceId()
     */
    public function findByReferenceId($referenceId, array $with = [], $select = ['*'])
    {
        return $this->model->where('reference_id', $referenceId)->with($with)->first($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::add()
     */
    public function add(array $attributes = [], $forceFill = false)
    {
        $this->validateDuplicate($attributes['reference_id']);

        $account                 = $this->model->newInstance($attributes);
        $account->account_number = isset($attributes['account_number'])
            ? $attributes['account_number']
            : $this->generateAccountNumber();

        if ($account->save()) {

            // create account credits
            $creditTypes = $this->creditType->active()->get(['id']);
            if ($creditTypes->isEmpty()) {
                throw new \Exception('No Credit Types available');
            }
            $ids = $creditTypes->pluck('id');
            $account->credits()->attach($ids);

            return $account->account_number;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::editByAccountNum()
     */
    public function editByAccountNum($accountNum, array $attributes = [])
    {
        $bankAccount = $this->model->where('account_number', $accountNum)->firstOrFail();

        return $bankAccount->fill($attributes)->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::editByReferenceId()
     */
    public function editByReferenceId($referenceId, array $attributes = [])
    {
        $bankAccount = $this->model->where('reference_id', $referenceId)->firstOrFail();

        return $bankAccount->fill($attributes)->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::deleteByAccountNum()
     */
    public function deleteByAccountNum($ids)
    {
        return $this->model->where('account_number', $ids)->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountContract::deleteByReferenceId()
     */
    public function deleteByReferenceId($ids)
    {
        return $this->model->where('reference_id', $ids)->delete();
    }

    /**
     * Generate Account Number
     * @return unknown
     */
    private function generateAccountNumber()
    {
        $config = config('credit.account_number');
        if (!$this->generator) {
            $class           = $config['generator'] ? $config['generator'] : SimpleGenerator::class;
            $this->generator = $class::make();
        }

        return $this->generator->generate($config['prefix'], $config['length']);
    }
}
