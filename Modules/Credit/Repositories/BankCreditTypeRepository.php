<?php

namespace Modules\Credit\Repositories;

use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BankCreditTypeRepository extends Repository implements BankCreditTypeContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class Constructor
     * @param BankCreditType $model
     */
    public function __construct(BankCreditType $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    /**
     * Get credits type by network
     *
     * @param $network
     * @return BankCreditType
     */
    public function getCreditsByNetwork($network)
    {
        return BankCreditType::where('network', $network)->get();
    }

    /**
     * Get all crypto credits
     *
     * @param $network
     * @return BankCreditType
     */
    public function getBlockchainCredits()
    {
        return BankCreditType::where('network', '!=', null)->get();
    }
}
