<?php

namespace Modules\Credit\Repositories;

use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Models\BankTransactionType;

class BankTransactionTypeRepository extends Repository implements BankTransactionTypeContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor
     * @param BankTransactionType $model
     */
    public function __construct(BankTransactionType $model)
    {
        parent::__construct($model);
    }
}
