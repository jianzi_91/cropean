<?php

namespace Modules\Credit\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Core\Account;
use Modules\Credit\Core\Generators\SimpleGenerator;
use Modules\Credit\Models\BankAccount;
use Modules\Credit\Models\BankAccountCredit;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Models\BankCreditType;
use Modules\Credit\Traits\Statement;
use Modules\Credit\Traits\ValidateAccount;
use Modules\Credit\Traits\ValidateCredit;
use Plus65\Base\Repositories\Repository;

class BankAccountCreditRepository extends Repository implements BankAccountCreditContract
{
    use Statement, ValidateAccount, ValidateCredit;

    /**
     * Account instance
     * @var unknown
     */
    protected $account;

    /**
     * Credit Type instance
     * @var unknown
     */
    protected $creditType;

    /**
     * The statment model
     * @var unknown
     */
    protected $statement;

    /**
     * Bank account credit model.
     *
     * @var BankAccountCredit
     */
    protected $credit;

    /**
     * Class constructor
     *
     * @param BankAccount $model
     * @param BankCreditType $creditType
     * @param BankAccountStatement $statement
     * @param BankAccountCredit $credit
     * @param Account $account
     */
    public function __construct(
        BankAccount $model,
        BankCreditType $creditType,
        BankAccountStatement $statement,
        BankAccountCredit $credit,
        Account $account
    ) {
        $this->account    = $account;
        $this->creditType = $creditType;
        $this->statement  = $statement;
        $this->credit     = $credit;

        parent::__construct($model);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\AdjustmentContract::add()
     */
    public function add($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $transfer = null)
    {
        return $this->account->add($creditType, $amount, $target, $transactionType, $remarks, $doneBy, $transactionDate, $transfer);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::addByAccountNum()
     */
    public function addByAccountNum($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $transfer = null)
    {
        $referenceId = $this->validateAccountByAccountNumber($target, true);
        return $this->account->add($creditType, $amount, $referenceId, $transactionType, $remarks, $doneBy, $transactionDate, $transfer);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\AdjustmentContract::deduct()
     */
    public function deduct($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $transfer = null)
    {
        return $this->account->deduct($creditType, $amount, $target, $transactionType, $remarks, $doneBy, $transactionDate, $transfer);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::deductByAccountNum()
     */
    public function deductByAccountNum($creditType, $amount, $target, $transactionType, $remarks = null, $doneBy = null, Carbon $transactionDate = null, $transfer = null)
    {
        $referenceId = $this->validateAccountByAccountNumber($target, true);
        return $this->account->deduct($creditType, $amount, $referenceId, $transactionType, $remarks, $doneBy, $transactionDate, $transfer);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::transfer()
     */
    public function transfer($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null)
    {
        return $this->account->transfer($creditType, $amount, $target, $source, $remarks, $doneBy, $transactionDate);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::transferDifferentType()
     */
    public function transferDifferentType($fromCreditType, $toCreditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null)
    {
        return $this->account->transferDifferentType($fromCreditType, $toCreditType, $amount, $target, $source, $remarks, $doneBy, $transactionDate);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::transferByAccountNum()
     */
    public function transferByAccountNum($creditType, $amount, $target, $source, $remarks = null, $doneBy = null, Carbon $transactionDate = null)
    {
        $referenceIdFrom = $this->validateAccountByAccountNumber($target);
        $referenceIdTo   = $this->validateAccountByAccountNumber($source);
        if ($doneBy) {
            $doneBy = $this->validateAccountByAccountNumber($doneBy);
        }
        return $this->transfer($creditType, $amount, $referenceIdTo, $referenceIdFrom, $remarks, $doneBy, $transactionDate);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::getBalanceByReference()
     */
    public function getBalanceByReference($referenceId, $creditTypes = [])
    {
        $accountId = $this->validateAccount($referenceId);
        return $this->getBalance($accountId, $creditTypes);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::getSystemBalance()
     */
    public function getSystemBalance($creditTypeId)
    {
        return $this->getBankAccountCreditModel()
            ->where('bank_credit_type_id', $creditTypeId)
            ->sum('balance');
    }

    public function getSystemAllBalance($creditTypeIds)
    {
        return $this->credit
            ->whereIn('bank_credit_type_id', array_keys($creditTypeIds))
            ->groupBy('bank_credit_type_id')
            ->selectRaw('bank_credit_type_id, sum(balance) as balance')
            ->pluck('balance', 'bank_credit_type_id')
            ->toArray();
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::getBalanceByAccountNum()
     */
    public function getBalanceByAccountNum($accountNumber, $creditTypes = [])
    {
        $accountId = $this->validateAccountByAccountNumber($accountNumber);
        return $this->getBalance($accountId, $creditTypes);
    }

    /**
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountCreditContract::getBalance()
     */
    public function getBalance($id, $creditTypes = [])
    {
        if (!is_array($creditTypes) && !empty($creditTypes)) {
            $creditTypeId = $this->validateCreditType($creditTypes);
            $balance      = $this->getFinalCredit($creditTypeId, $id);
            return $balance;
        }
        if (empty($creditTypes)) {
            $types = $this->creditType->active()->get(['name']);
            if (count($types)) {
                $creditTypes = $types->pluck('name');
            }
        }

        $creditBalance = [];
        foreach ($creditTypes as $type) {
            $creditTypeId         = $this->validateCreditType($type);
            $balance              = $this->getFinalCredit($creditTypeId, $id);
            $creditBalance[$type] = $balance;
        }

        return $creditBalance;
    }

    /**
     * Get model instance
     * @return Model
     */
    public function getModel()
    {
        return $this->statement->newInstance();
    }

    /**
     * Get bank account credit model instance.
     * @return Model
     */
    public function getBankAccountCreditModel()
    {
        return $this->credit->newInstance();
    }

    /**
     * Get generator instance
     * @return object
     */
    public function getGenerator()
    {
        if (!$this->generator) {
            $class           = config('credit.transaction_code.generator', SimpleGenerator::class);
            $this->generator = $class::make();
        }
        return $this->generator;
    }
}
