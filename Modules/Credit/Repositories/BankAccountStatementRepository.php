<?php

namespace Modules\Credit\Repositories;

use Carbon\Carbon;
use Modules\Credit\Contracts\BankAccountStatementContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Traits\ValidateCredit;
use Modules\Rbac\Models\Role;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BankAccountStatementRepository extends Repository implements BankAccountStatementContract
{
    use ValidateCredit, HasCrud, HasSlug;

    /**
     * Class constructor
     * @param BankAccountStatement $model
     */
    public function __construct(BankAccountStatement $model)
    {
        $this->slug = 'transaction_code';
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::get()
     */
    public function get(Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->select($select);
        if ($from && $to) {
            $prepare->whereBetween('transaction_date', [$from, $to]);
        }

        if ($transactionType) {
            $txnTypeId = $this->validateTransactionType($transactionType);
            $prepare->where('bank_transaction_type_id', $txnTypeId);
        }

        $prepare->orderBy('transaction_date', $orderBy)->orderBy('id', $orderBy);

        return $prepare->with($with)->get();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::findByAccountId()
     */
    public function findByAccountId($id, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->select($select)->where('bank_account_id', $id);

        if ($from && $to) {
            $prepare->whereBetween('transaction_date', [$from, $to]);
        }

        if ($transactionType) {
            $txnTypeId = $this->validateTransactionType($transactionType);
            $prepare->where('bank_transaction_type_id', $txnTypeId);
        }

        $prepare->orderBy('transaction_date', $orderBy)->orderBy('id', $orderBy);

        return $prepare->with($with)->get();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::findByAccountNum()
     */
    public function findByAccountNum($accountNum, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*'])
    {
        //@TODO: Check wherehas performance
        $prepare = $this->model->select($select)
                        ->whereHas(
                            'source',
                            function ($query) use ($accountNum) {
                                $query->where('account_number', $accountNum);
                            }
                        );

        if ($from && $to) {
            $prepare->whereBetween('transaction_date', [$from, $to]);
        }

        if ($transactionType) {
            $txnTypeId = $this->validateTransactionType($transactionType);
            $prepare->where('bank_transaction_type_id', $txnTypeId);
        }

        $prepare->orderBy('transaction_date', $orderBy)->orderBy('id', $orderBy);

        return $prepare->with($with)->get();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::findByReferenceId()
     */
    public function findByReferenceId($referenceId, Carbon $from = null, Carbon $to = null, $orderBy = 'desc', $transactionType = null, array $with = [], $select = ['*'])
    {
        //@TODO: Check wherehas performance
        $prepare = $this->model->select($select)
                        ->whereHas(
                            'source',
                            function ($query) use ($referenceId) {
                                $query->where('reference_id', $referenceId);
                            }
                        );

        if ($from && $to) {
            $prepare->whereBetween('transaction_date', [$from, $to]);
        }

        if ($transactionType) {
            $txnTypeId = $this->validateTransactionType($transactionType);
            $prepare->where('bank_transaction_type_id', $txnTypeId);
        }

        $prepare->orderBy('transaction_date', $orderBy)
                 ->orderBy('id', $orderBy);

        return $prepare->with($with)->get();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::deleteByAccountId()
     */
    public function deleteByAccountId($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        return $this->model->whereIn('bank_account_id', $ids)->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::deleteByAccountNum()
     */
    public function deleteByAccountNum($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        //@TODO: check wherehas performance
        $prepare = $this->model->whereHas(
            'source',
            function ($query) use ($ids) {
                $query->whereIn('account_number', $ids);
            }
        );

        return $prepare->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Credit\Contracts\BankAccountStatementContract::deleteByReferenceId()
     */
    public function deleteByReferenceId($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        //@TODO: check wherehas performance
        $prepare = $this->model->whereHas(
            'source',
            function ($query) use ($ids) {
                $query->whereIn('reference_id', $ids);
            }
        );

        return $prepare->delete();
    }

    public function getDownlineTotalBalance(
        $userId,
        $creditTypes,
        $includedSelf = false,
        $statuses = [UserStatusContract::ACTIVE, UserStatusContract::HOLD, UserStatusContract::SUSPENDED]
    ) {
        $qry   = [];
        foreach ($creditTypes as $creditTypeId => $creditTypeName) {
            $qry[] = "sum(if(bank_account_credits.bank_credit_type_id = $creditTypeId, bank_account_credits.balance, 0)) as {$creditTypeName}_key";
        }

        $query = User::join('sponsor_trees', 'sponsor_trees.user_id', '=', 'users.id')
            ->join('sponsor_trees as downline', function ($query) use ($includedSelf) {
                if ($includedSelf) {
                    $query->on('downline.lft', '>=', 'sponsor_trees.lft')
                        ->on('downline.rgt', '<=', 'sponsor_trees.rgt');
                } else {
                    $query->on('downline.lft', '>', 'sponsor_trees.lft')
                        ->on('downline.rgt', '<', 'sponsor_trees.rgt');
                }
            })
            ->join('users as downline_users', 'downline_users.id', '=', 'downline.user_id')
            ->join('assigned_roles', 'assigned_roles.entity_id', 'users.id')
            ->join('bank_accounts', 'bank_accounts.reference_id', '=', 'downline_users.id')
            ->join('bank_account_credits', 'bank_account_credits.bank_account_id', '=', 'bank_accounts.id')
            ->join('user_statuses', 'user_statuses.id', '=', 'downline_users.user_status_id')
            ->whereIn('user_statuses.name', $statuses)
            ->whereIn('bank_account_credits.bank_credit_type_id', array_keys($creditTypes))
            ->selectRaw(implode(', ', $qry))
            ->where('users.id', $userId)
            ->first();

        return $query;
    }

    public function updateToCreditTypeIdUsingTxnCode($txnCode, $toCreditTypeId)
    {
        return $this->model->where('transaction_code', $txnCode)->update(['to_bank_credit_type_id' => $toCreditTypeId]);
    }
}
