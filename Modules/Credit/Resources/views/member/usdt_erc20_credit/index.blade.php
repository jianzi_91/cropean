@extends('templates.member.master')
@section('title', __('m_page_title.usdt erc20 credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_usdt_erc20_credit.usdt credits')],
    ],
    'header'=>__('m_usdt_erc20_credit.usdt credits')
])

<div class="row">
    <div class="col-12 col-xs-6 col-lg-6 col-xl-6">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_usdt_erc20_credit.usdt credits balance'),
            'value'=>amount_format(auth()->user()->usdt_erc20_credit, credit_precision('usdt_erc20')),
            'backgroundClass'=>'widget-usdt-image',
        ])
    </div>
</div>

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('transaction_date', request('transaction_date'), __('m_usdt_erc20_credit.date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('transaction_code', request('transaction_code'), __('m_usdt_erc20_credit.transaction number'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('bank_transaction_type_id',  credit_transaction_types_dropdown(), request('bank_transaction_type_id'), __('m_usdt_erc20_credit.transaction type'), [], false) }}
</div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{__('m_usdt_erc20_credit.transaction history')}}</h4>
            <div class="d-flex flex-row">
                @can('member_blockchain_withdrawal_create')
                 {{-- {{ Form::formButtonPrimary('external_transfer', __('m_usdt_erc20_credit.external transfer')) }} --}}
                &nbsp;&nbsp;&nbsp;
                @endcan
                @can('member_credit_transfer_create')
                    @can('member_transfer_usdt_erc20')
                        {{ Form::formTabSecondary(__('m_usdt_erc20_credit.internal transfer'), route('member.credits.transfer.create')) }}
                    @endcan
                &nbsp;&nbsp;&nbsp;
                @endcan
                @can('member_conversion_create')
                    @can('usdt_erc20_convert')
                    {{-- {{ Form::formButtonPrimary('btn_convert', __('m_usdt_erc20_credit.convert')) }} --}}
                    @endcan
                &nbsp;&nbsp;&nbsp;
                @endcan
                @can('member_credit_statement_usdt_erc20_credit_export')
                {{ Form::formTabSecondary(__('m_usdt_erc20_credit.export excel'), route('member.credits.statements.usdt-erc20-credit.export.index',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('m_usdt_erc20_credit.date') }}</th>
                    <th>{{ __('m_usdt_erc20_credit.transaction number') }}</th>
                    <th>{{ __('m_usdt_erc20_credit.transaction type') }}</th>
                    <th>{{ __('m_usdt_erc20_credit.credit') }}</th>
                    <th>{{ __('m_usdt_erc20_credit.debit') }}</th>
                    <th>{{ __('m_usdt_erc20_credit.remarks') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($statements as $statement)
                <tr>
                    <td>{{$statement->transaction_date }}</td>
                    <td>{{$statement->transaction_code}}</td>
                    <td>{{display_transaction($statement)}}</td>
                    <td>{{amount_format($statement->credit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{amount_format($statement->debit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{$statement->remarks}}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 6,
                'text' => __('m_usdt_erc20_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $statements->render() !!}
@endsection


@push('scripts')
<script>
    $(function() {
        $('#external_transfer').on('click', function(e){
            e.preventDefault()
            window.location.href = '{{ route("member.blockchain.withdrawals.index") }}'
        })

        $('#btn_convert').on('click', function(e){
            e.preventDefault()
            window.location.href = '{{ route("member.credits.conversion.create") }}'
        })
    });
</script>
@endpush
