@extends('templates.member.master')
@section('title', __('m_page_title.transfer credits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.transfer credits')]
    ],
    'header'=>__('m_credit_transfer.transfer credits')
])

<div class="row">
    <div class="col-xs-12 col-lg-8 order-lg-1 order-2">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['route' => 'member.credits.transfer.store', 'method'=>'post', 'id' => 'transfer-credits', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formSelect('credit_type', transferable_credit_types_dropdown(), request('credit_type'), __('m_credit_transfer.credit type'), [], true) }}
                    {{ Form::formNumber('to_member_id', request('to_member_id'), __('m_credit_transfer.transfer to')) }}
                    {{ Form::formNumber('amount', request('amount'), __('m_credit_transfer.amount to transfer')) }}
                    {{ Form::formPassword('secondary_password', '', __('m_credit_transfer.secondary password'), ['autocomplete'=>'off'], true, true) }}
                    <div class="row d-flex justify-content-end m-0 p-0">
                        <button id="btn_submit" class="btn btn-primary cro-btn-primary js-submit" type="submit">{{ __('m_credit_transfer.submit') }}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-4 order-lg-2 order-1">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_transfer.usdt credits balance'),
            'value'=>amount_format(auth()->user()->usdt_erc20_credit, credit_precision('usdt_erc20')),
            'backgroundClass'=>'widget-capital-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_transfer.usdc credits balance'),
            'value'=>amount_format(auth()->user()->usdc_credit, credit_precision('usdc')),
            'backgroundClass'=>'widget-rollover-image',
        ])
    </div>
</div>

@endsection
