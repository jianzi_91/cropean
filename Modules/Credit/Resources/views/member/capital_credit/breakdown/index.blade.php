@extends('templates.member.master')
@section('title', __('m_page_title.capital credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_capital_credit.capital credits')],
    ],
    'header'=>__('m_capital_credit.capital credits breakdown'),
    'backRoute' => route('member.credits.statements.capital-credit.index'),
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('start_date', request('start_date'), __('m_capital_credit.lock date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('end_date', request('end_date'), __('m_capital_credit.unlock date'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('m_capital_credit.lock date') }}</th>
                    <th>{{ __('m_capital_credit.unlock date') }}</th>
                    <th>{{ __('m_capital_credit.amount') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($creditLocks as $creditLock)
                <tr>
                    <td>{{ $creditLock->start_date }}</td>
                    <td>{{ $creditLock->end_date }}</td>
                    <td>{{ amount_format($creditLock->current_amount) }}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 6,
                'text' => __('m_capital_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $creditLocks->render() !!}
@endsection


@push('scripts')
<script>
    $(function() {
        $('#btn_convert').on('click', function(e){
            e.preventDefault()
            window.location.href = '{{ route("member.credits.conversion.create") }}'
        })
    });
</script>
@endpush
