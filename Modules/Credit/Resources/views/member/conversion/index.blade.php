@extends('templates.member.master')
@section('title', __('m_page_title.conversions'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
    ],
    'header'=>__('m_conversion_list.conversions')
])

@can('member_conversion_create')
<div class="pb-2">
    {{ Form::formButtonPrimary('btn_create', __('m_conversion_list.new converssion request'), 'button') }}
</div>
@endcan

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('created_at', request('created_at'), __('m_conversion_list.request date'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('source_credit_type_id', convertable_credit_types_dropdown(), request('source_credit_type_id'), __('m_conversion_list.source credit type'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('destination_credit_type_id', convertable_credit_types_dropdown(), request('destination_credit_type_id'), __('m_conversion_list.destination credit type'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('credit_conversion_status_id', conversion_statuses_dropdown(), request('credit_conversion_status_id'), __('m_conversion_list.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_conversion_list.conversion requests')}}</h4>
            <div class="d-flex flex-row align-items-center">

                <div class="table-total-results mr-2">{{ __('m_conversion_list.total results:') }} {{ $conversions->total() }}</div>
                @can('member_conversion_export')
                {{ Form::formTabSecondary(__('m_conversion_list.export table'), route('member.credits.conversion.export', array_merge(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_conversion_list.request date') }}</th>
                <th>{{ __('m_conversion_list.source credit type') }}</th>
                <th>{{ __('m_conversion_list.destination credit type') }}</th>
                <th>{{ __('m_conversion_list.source amount') }}</th>
                <th>{{ __('m_conversion_list.admin fee') }}</th>
                <th>{{ __('m_conversion_list.penalty') }}</th>
                <th>{{ __('m_conversion_list.destination amount') }}</th>
                <th>{{ __('m_conversion_list.invoice') }}</th>
                @can('member_conversion_edit')
                @endcan
            </tr>
        </thead>
        <tbody>
        @forelse ($conversions as $conversion)
            <tr>
                <td>{{ $conversion->created_at }}</td>
                <td>{{ $conversion->sourceCreditType->name }}</td>
                <td>{{ $conversion->destinationCreditType->name }}</td>
                <td>{{ amount_format($conversion->source_amount, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->admin_fee, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->penalty_amount, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->destination_amount, credit_precision()) }}</td>
                <td>
                    @if($conversion->invoice)
                    <a href="{{ route('member.credits.conversions.invoices.show', ['invoice' => $conversion->invoice->reference_number]) }}" data-toggle="tooltip" data-placement="top" title="{{ __('m_conversion_list.invoice')}}" target="__blank"><i class="bx bx-receipt"></i></a>
                    @else
                    -
                    @endif
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('m_conversion_list.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $conversions->render() !!}
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $('#btn_create').on('click', function(e){
            e.preventDefault();
            window.location.href = "{{ route('member.credits.conversion.create') }}"
        })

        $('.js-delete').on('click',function(e){
            e.preventDefault();
            var el = this;

            swal.fire({
                title: '{{ ucfirst(__("m_conversion_list.do you want to cancel this conversion")) }}',
                showCancelButton: true,
                cancelButtonText: '{{ __("m_conversion_list.no") }}',
                confirmButtonText: '{{ __("m_conversion_list.yes") }}',
                customClass: {
                    confirmButton: 'btn btn-primary cro-btn-primary',
                    cancelButton: 'btn btn-secondary cro-btn-secondary',
                },
                preConfirm: function(e){
                    $(el).closest('form').submit();
                }
            })
        })
    });
</script>
@endpush