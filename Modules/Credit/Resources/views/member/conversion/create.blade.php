@extends('templates.member.master')
@section('title', __('m_page_title.convert credits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.convert credits')]
    ],
    'header'=>__('m_credit_convert.convert credits'),
    'backRoute'=> route('member.credits.conversion.index')
])

<div class="row">
    <div class="col-xs-12 col-lg-8 order-lg-1 order-2">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['route' => 'member.credits.conversion.store', 'method'=>'post', 'id' => 'convert-credits', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formSelect('credit_type_convert_from', convertable_credit_types_dropdown(), old('credit_type_convert_from'), __('m_credit_convert.convert from'), [], true) }}
                    {{ Form::formSelect('credit_type_convert_to', [], old('credit_type_convert_to'), __('m_credit_convert.convert to'), [], true) }}
                    {{ Form::formNumber('amount', request('amount'), __('m_credit_convert.amount to convert')) }}

                    <div class="div_penalty_fee">
                        {{ Form::formText('penalty_fee', '', __('m_credit_convert.penalty fee'), ['readonly'], true) }}
                    </div>
                    <div class="div_service_fee">
                        {{ Form::formText('service_fee', '', __('m_credit_convert.service fee'), ['readonly'], true) }}
                    </div>

                    {{ Form::formText('converted_amount', '', __('m_credit_convert.converted amount'), ['readonly'], true) }}
                    {{ Form::formPassword('secondary_password', '', __('m_credit_convert.secondary password'), ['autocomplete'=>'off'], true, true) }}
                    <div class="row d-flex justify-content-end m-0 p-0">
                        <button id="btn_submit" class="btn btn-primary cro-btn-primary js-submit" type="submit">{{ __('m_credit_convert.submit') }}</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-4 order-lg-2 order-1">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_convert.capital credits balance'),
            'value'=>amount_format(auth()->user()->capital_credit),
            'backgroundClass'=>'widget-capital-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_convert.roll-over credits balance'),
            'value'=>amount_format(auth()->user()->rollover_credit),
            'backgroundClass'=>'widget-rollover-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_convert.cash credits balance'),
            'value'=>amount_format(auth()->user()->cash_credit),
            'backgroundClass'=>'widget-cash-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_convert.usdt credits balance'),
            'value'=>amount_format(auth()->user()->usdt_erc20_credit),
            'backgroundClass'=>'widget-cash-image',
        ])
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('m_credit_convert.usdc credits balance'),
            'value'=>amount_format(auth()->user()->usdc_credit),
            'backgroundClass'=>'widget-cash-image',
        ])
        <div class="card">
            <div>
                <div class="card-body">
                    <div class="widget-label" id="exchange-rate-label">{{__('m_credit_convert.exchange rate placeholder')}}</div>
                    <div class="widget-value" id="exchange-rate-value"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
 <script type="text/javascript">
    $(document).ready(function() {
        $('.div_penalty_fee').hide();
        $('.div_service_fee').hide();

        function getConvertibleCreditTypes() {
            var lang = '{{ app()->getLocale() }}';
            var bank_credit_type = $('#credit_type_convert_from').val();
            $('#credit_type_convert_to').empty();

            axios.get('{{ route("member.credits.conversion.convertible-credit-types") }}/?bank_credit_type=' + bank_credit_type)
                .then((response) => {
                    $.each(response.data, function(key, value) {
                        var option = new Option(value, key);
                        $(option).html(value);
                        $('#credit_type_convert_to').append(option);
                    });

                    var convert_to = $('#credit_type_convert_to option:first').val();
                    $("#credit_type_convert_to").val(convert_to);
                    
                    if (convert_to) {
                        getCreditTypeConvertTo(convert_to);
                    }
                });
        }

        function getCreditTypeConvertTo(convertTo) {
            var lang = '{{ app()->getLocale() }}',
                credit_type_convert_from = $('#credit_type_convert_from').val(),
                // credit_type_convert_to = $('#credit_type_convert_to').val(),
                credit_type_convert_to = convertTo,
                amount = $('#amount').val()

            if(credit_type_convert_from){
                axios.get('{{ route("member.credits.conversion.convert-credit-details") }}/?credit_type_convert_from=' + credit_type_convert_from + '&credit_type_convert_to=' + credit_type_convert_to)
                    .then((response) => {
                        const {
                            currency_from,
                            currency_from_sell_rate,
                            currency_to,
                            currency_to_buy_rate,
                            base_currency,
                            base_currency_rate,
                        } = response.data;

                        const exchangeWidgetLabel = `${currency_from} : ${base_currency} : ${currency_to}`;
                        const exchangeWidgetValue = `${currency_from_sell_rate}:${base_currency_rate}:${currency_to_buy_rate}`;

                        $('#exchange-rate-label').text(exchangeWidgetLabel);
                        $('#exchange-rate-value').text(exchangeWidgetValue);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }

            if (amount != "") {
                getFee();
            }
        }

        function getFee() {
            var lang = '{{ app()->getLocale() }}',
                credit_type_convert_from = $('#credit_type_convert_from').val(),
                credit_type_convert_to = $('#credit_type_convert_to').val(),
                amount = $('#amount').val()

            if(credit_type_convert_from != "" && credit_type_convert_to != ""){
                axios.get('{{ route("member.credits.conversion.calculates") }}/?credit_type_convert_from=' + credit_type_convert_from + '&credit_type_convert_to=' + credit_type_convert_to + '&amount=' + amount)
                    .then((response) => {
                        $('#penalty_fee').val(response.data.penalty_fee);
                        $('#service_fee').val(response.data.service_fee);
                        $('#converted_amount').val(response.data.converted_amount);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        }

        $('#credit_type_convert_from').on('change', function() {
            switch(this.value){
                //capital_credit
                case "1":
                case 1:
                    $('.div_penalty_fee').show();
                    $('.div_service_fee').hide();
                    break;
                //roll_over_credit
                case "2":
                case "6":
                case "7":
                case 2:
                    $('.div_penalty_fee').hide();
                    $('.div_service_fee').show();
                    break;
                //cash_credit
                case "3":
                    $('.div_penalty_fee').hide();
                    $('.div_service_fee').show();
                    break;
                default:
                    $('.div_penalty_fee').hide();
                    $('.div_service_fee').hide();
                    $('#credit_type_convert_to').empty();
            }

            if (this.value != "") {
                getConvertibleCreditTypes();
            } 
        })

        $('#credit_type_convert_to').on('change', function() {
            if (this.value != "" && this.value != null) {
                getCreditTypeConvertTo(this.value);
                getFee();
            }
        });

        $('#amount').keyup(function() {
            getFee();
        })
    });
</script>
@endpush
