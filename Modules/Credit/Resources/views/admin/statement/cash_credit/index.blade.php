@extends('templates.admin.master')
@section('title', __('a_page_title.cash credit statement'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_credits_statement_cash_credit.cash credits')],
    ],
    'header'=>__('a_credits_statement_cash_credit.cash credits'),
])

<div class="row">
    <div class="col-12 col-xs-6 col-lg-6 col-xl-6">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('a_credits_statement_cash_credit.cash credits balance'),
            'value'=>amount_format(auth()->user()->cash_credit),
            'backgroundClass'=>'widget-cash-image',
        ])
    </div>
</div>

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('transaction_date', request('transaction_date'), __('a_credits_statement_cash_credit.date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('transaction_code', request('transaction_code'), __('a_credits_statement_cash_credit.transaction number'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('bank_transaction_type_id',  credit_transaction_types_dropdown(), request('bank_transaction_type_id'), __('a_credits_statement_cash_credit.transaction type'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_credits_statement_cash_credit.transactions')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_credits_statement_cash_credit.total results:') }} {{ $statements->total() }}</div>
                @can('admin_credit_statement_cash_credit_export')
                {{ Form::formTabSecondary(__('a_credit_statement_cash_credit.export excel'), route('admin.credits.statements.cash-credit.export.index',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_credits_statement_cash_credit.date') }}</th>
                    <th>{{ __('a_credits_statement_cash_credit.transaction number') }}</th>
                    <th>{{ __('a_credits_statement_cash_credit.transaction type') }}</th>
                    <th>{{ __('a_credits_statement_cash_credit.credit') }}</th>
                    <th>{{ __('a_credits_statement_cash_credit.debit') }}</th>
                    <th>{{ __('a_credits_statement_cash_credit.remarks') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($statements as $statement)
                <tr>
                    <td>{{$statement->transaction_date }}</td>
                    <td>{{$statement->transaction_code}}</td>
                    <td>{{display_transaction($statement)}}</td>
                    <td>{{amount_format($statement->credit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{amount_format($statement->debit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{$statement->remarks}}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [
                'span' => 6, 'text' => __('a_credits_statement_cash_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
        {!! $statements->render() !!}
    </div>
</div>
@endsection


@push('scripts')
<script>
    $(function() {

        });
</script>
@endpush
