@extends('templates.admin.master')
@section('title', __('a_page_title.conversions'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
    ],
    'header'=>__('a_conversion_list.conversions')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('created_at', request('created_at'), __('a_conversion_list.request date'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('a_conversion_list.name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('member_id', request('member_id'), __('a_conversion_list.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3 mt-2">
        {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_conversion_list.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('email', request('email'), __('a_conversion_list.email'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('source_credit_type_id', convertable_credit_types_dropdown(), request('source_credit_type_id'), __('a_conversion_list.source'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('destination_credit_type_id', convertable_credit_types_dropdown(), request('destination_credit_type_id'), __('a_conversion_list.destination'), [], false) }}
    </div>
    {{-- <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('credit_conversion_status_id', conversion_statuses_dropdown(), request('credit_conversion_status_id'), __('a_conversion_list.status'), [], false) }}
    </div> --}}
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_conversion_list.conversion requests')}}</h4>
            <div class="d-flex flex-row align-items-center">

                <div class="table-total-results mr-2">{{ __('a_conversion_list.total results:') }} {{ $conversions->total() }}</div>
                {{-- @if (request()->credit_conversion_status_id == 1)
                    {{ Form::formButtonPrimary('btn-bulk-update', __('a_conversion_list.update'), 'button', []) }}
                @endif
                &nbsp;&nbsp;&nbsp;  --}}
                @can('admin_conversion_export')
                {{ Form::formTabSecondary(__('a_conversion_list.export table'), route('admin.credits.conversion.export', array_merge(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                {{-- @if (request()->credit_conversion_status_id == 1)
                <td class="text-center">{{ Form::formCheckbox(null, 'select-all') }}</td>
                @endif --}}
                {{-- @canany(['admin_conversion_view', 'admin_conversion_edit'])
                <th class="text-center">{{ __('a_conversion_list.action') }}</th>
                @endcanany --}}
                <th>{{ __('a_conversion_list.request date') }}</th>
                <th>{{ __('a_conversion_list.full name') }}</th>
                <th>{{ __('a_conversion_list.member id') }}</th>
                <th>{{ __('a_conversion_list.email') }}</th>
                <th>{{ __('a_conversion_list.source credit type') }}</th>
                <th>{{ __('a_conversion_list.destination credit type') }}</th>
                <th>{{ __('a_conversion_list.source amount') }}</th>
                <th>{{ __('a_conversion_list.admin fee') }}</th>
                <th>{{ __('a_conversion_list.penalty fee') }}</th>
                <th>{{ __('a_conversion_list.destination amount') }}</th>
                {{-- <th>{{ __('a_conversion_list.status') }}</th> --}}
                <th>{{ __('a_conversion_list.invoice') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($conversions as $conversion)
            <tr>
                {{-- @if (request()->credit_conversion_status_id == 1)
                    <td class="text-center">{{ Form::formCheckbox('cbx-conversion', 'cbx-conversion-' . $conversion->id, null, null, ['class' => 'cbx-conversion', 'data-id' => $conversion->id]) }}</td>
                @endif --}}
                {{-- @canany(['admin_conversion_view', 'admin_conversion_edit'])
                <td class="action text-center">
                    @if (!$conversion->is_cancelled)
                        @can('admin_conversion_view')
                        <a href="{{ route('admin.credits.conversion.show', $conversion->id) }}">
                            <i class="bx bx-show-alt"></i>
                        </a>
                        @endcan
                    @endif
                </td>
                @endcanany --}}
                <td>{{ $conversion->created_at }}</td>
                <td>{{ $conversion->name }}</td>
                <td>{{ $conversion->member_id }}</td>
                <td>{{ $conversion->email }}</td>
                <td>{{ $conversion->sourceCreditType->name }}</td>
                <td>{{ $conversion->destinationCreditType->name }}</td>
                <td>{{ amount_format($conversion->source_amount, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->admin_fee, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->penalty_amount, credit_precision()) }}</td>
                <td>{{ amount_format($conversion->destination_amount, credit_precision()) }}</td>
                {{-- <td>{{ __('s_credit_conversion_statuses.' . $conversion->status_name) }}</td> --}}
                <td>
                    @if($conversion->invoice)
                    <a href="{{ route('admin.credits.conversions.invoices.show', ['invoice' => $conversion->invoice->reference_number]) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_conversion_list.invoice')}}" target="__blank"><i class="bx bx-receipt"></i></a>
                    @else
                    -
                    @endif
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('a_conversion_list.no records') ])
        @endforelse
        </tbody>
        {{-- {{ Form::open(['method'=>'post', 'route' => 'admin.conversions.bulk.update', 'id'=>'fm-bulk-update']) }}
        {{ Form::formHide('conversion_ids') }}
        {{ Form::formHide('remarks') }}
        {{ Form::formHide('action') }}
        {{ Form::close() }} --}}
        @endcomponent
    </div>
</div>
{!! $conversions->render() !!}
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        // $("#select-all").click(function() {
        //     if ($(this).prop("checked")) {
        //         $('.cbx-conversion').attr('checked', 'checked');
        //     } else {
        //         $('.cbx-conversion').removeAttr('checked');
        //     }
            
        // });

        // $("#btn-bulk-update").click(function() {
        //     let conversion_ids = [];

        //     $.each(($('.cbx-conversion:checked')), function() {
        //         conversion_ids.push($(this).attr('data-id'));
        //     });

        //     if (conversion_ids.length === 0) {
        //         swal.fire({
        //             type: 'error',
        //             text: 'No conversion selected',
        //         })
        //         return;
        //     }

        //     $('#conversion_ids').val(conversion_ids);
        //     swal.fire({
        //         title: "{{ __('a_conversion_list.bulk update') }}",
        //         input: 'select',
        //         inputOptions: {
        //             approve: "{{ __('a_conversion_list.approve') }}",
        //             reject: "{{ __('a_conversion_list.reject') }}",
        //         },
        //         html: '<div class="form-group"><textarea class="form-control " id="sw-remarks" cols="50" rows="10" placeholder="Remarks" style="resize: none;"></textarea></div>',
        //         inputPlaceholder: "{{ __('a_conversion_list.please select a status') }}",
        //         showCancelButton: true,
        //         inputValidator: (value) => {
        //             return new Promise((resolve) => {
                        
        //                 let remarks = $('#sw-remarks').val();
                        
        //                 if (value) {
        //                     if (value == 'reject' && remarks == "") {
        //                         resolve("{{ __('a_conversion_list.remarks is required for rejecting conversions') }}");
        //                         return;
        //                     }

        //                     $('#btn-bulk-update').attr('disabled','disabled');
                            
        //                     $('#remarks').val(remarks);
        //                     $('#action').val(value);

        //                     resolve();
        //                     $('#fm-bulk-update').submit();
        //                 } else {
        //                     resolve("{{ __('a_conversion_list.please select a status') }}")
        //                 }
        //             })
        //         }
        //     })
        // });
    });
</script>
@endpush