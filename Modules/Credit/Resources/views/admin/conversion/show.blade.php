@extends('templates.admin.master')
@section('title', __('a_page_title.conversion details'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('a_conversion_details.conversions'), 'route'=>route('admin.credits.conversion.index')],
        ['name'=>__('a_conversion_details.conversion request details')]
    ],
    'header'=>__('a_conversion_details.conversion request details'),
    'backRoute'=> route('admin.credits.conversion.index')
])

<div class="row">
    <div class="col-lg-8 col-xs-12">
        {{-- <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title">{{ __('a_conversion_details.status') }}</h4>
                    <div class="card-title kyc_status">{{ $conversion->status->status->name_translation }}</div>
                </div>
            </div>
        </div> --}}
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::formText('created_at', $conversion->created_at, __('a_conversion_details.request date'), ['readonly']) }}
                    {{ Form::formText('name', $conversion->user->name, __('a_conversion_details.name'), ['readonly']) }}
                    {{ Form::formText('member_id', $conversion->user->member_id, __('a_conversion_details.member id'), ['readonly']) }}
                    {{ Form::formText('email', $conversion->user->email, __('a_conversion_details.email'), ['readonly']) }}
                    {{ Form::formText('convert_from', $conversion->sourceCreditType->name, __('a_conversion_details.convert from'), ['readonly']) }}
                    {{ Form::formText('convert_to', $conversion->destinationCreditType->name, __('a_conversion_details.convert to'), ['readonly']) }}
                    {{ Form::formText('source_amount', amount_format($conversion->source_amount, credit_precision()), __('a_conversion_details.requested conversion amount'), ['readonly']) }}
                    {{ Form::formText('admin_fee', amount_format($conversion->admin_fee, credit_precision()), __('a_conversion_details.admin fee'), ['readonly']) }}
                    {{ Form::formText('destination_amount', amount_format($conversion->destination_amount,credit_precision()), __('a_conversion_details.destination amount'), ['readonly']) }}
                    {{ Form::formText('destination_amount', amount_format($conversion->destination_amount,credit_precision()), __('a_conversion_details.destination amount'), ['readonly']) }}
                    {{ Form::formTextArea('remarks', $conversion->status->remarks, __('a_conversion_details.remarks'), ['disabled' => 'disabled']) }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
