@extends('templates.admin.master')
@section('title', __('a_page_title.member balance report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.member balance report')]
    ],
    'header'=>__('a_report_balance.member balance report')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('name', request('name'), __('a_report_balance.name'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('email', request('email'), __('a_report_balance.email'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('member_id', request('member_id'), __('a_report_balance.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3 mt-2">
        {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_balance.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
        <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_balance.member balance report') }}</h4>
        <div class="d-flex flex-row align-items-center">
        <div class="table-total-results mr-2">{{ __('a_report_balance.total results:') }} {{ $statements->total() }}</div>
        @can('admin_report_balance_statement_export')
            {{ Form::formTabSecondary(__('a_report_balance_.export table'), route('admin.report.balance.export', array_merge(request()->except('page'))))}}
        @endcan
        </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_balance.name') }}</th>
                <th>{{ __('a_report_balance.member id') }}</th>
                <th>{{ __('a_report_balance.email') }}</th>
                <th>{{ __('a_report_balance.equity amount') }}</th>
                <th>{{ __('a_report_balance.current capital credit') }}</th>
                <th>{{ __('a_report_balance.current rollover credit') }}</th>
                <th>{{ __('a_report_balance.current cash credit') }}</th>
                <th>{{ __('a_report_balance.current promotion credit') }}</th>
                <th>{{ __('a_report_balance.current usdt credit') }}</th>
                <th>{{ __('a_report_balance.current usdc credit') }}</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($statements as $statement)
                <tr>
                    <td>{{ $statement->user_name }}</td>
                    <td>{{ $statement->member_id }}</td>
                    <td>{{ $statement->email }}</td>
                    <td>{{ amount_format($statement->total_equity, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->capital_credit, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->rollover_credit, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->cash_credit, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->promotion_credit, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->usdt_credit, credit_precision()) ?? '0' }}</td>
                    <td>{{ amount_format($statement->usdc_credit, credit_precision()) ?? '0' }}</td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 6, 'text' => __('a_report_balance.no records') ])
            @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $statements->render() !!}
@endsection