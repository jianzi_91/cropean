@extends('templates.admin.master')
@section('title', __('a_member_management_change_password.transfer credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_transfer_credit.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_transfer_credit.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_transfer_credit.transfer credit')]
    ],
    'header'=>__('a_member_management_transfer_credit.transfer credit'),
    'showBack'=>true,
    'backRoute'=>route('admin.management.members.index'),
])

<div class="row flex-column-reverse flex-lg-row">
    <div class="col-lg-12">
        <div class="card">
            @include('templates.admin.includes._mm-nav', ['page' => 'transfer credit', 'uid' => $user->id ])
            <div class="card-body">
                  <div class="tab-container">
                    <div class="tab-content mt-20">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card" style="border:0;box-shadow:none;">
                                    <div class="card-body">
                                    {{ Form::open(['method'=>'put', 'id'=>'transfer-credit', 'class'=>'form-vertical -with-label']) }}
                                        <div class="row">
                                            <div class="col-12 jv-label-color">
                                                {{ Form::formSelect('credit_type', transferable_credit_types_dropdown(), old('credit_type'),__('a_member_management_transfer_credit.credit type')) }}
                                            </div>
                                            <div class="col-12 jv-label-color">
                                                {{ Form::formText('amount', old('amount'), __('a_member_management_transfer_credit.credit amount').' <span>*</span>') }}
                                            </div>
                                            <div class="col-12 jv-label-color">
                                                {{ Form::formTextArea('remarks', old('remarks'), __('a_member_management_transfer_credit.credit remarks').' <span>*</span>') }}
                                            </div>
                                            <div class="col-12 jv-label-color">
                                                {{ Form::formPassword('secondary_password', old('secondary_password'), __('a_member_management_transfer_credit.secondary password').' <span>*</span>') }}
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col ar-right-title-btn">
                                                <button type="submit" class="btn waves-effect waves-light btn-lg btn-primary">{{ __('a_member_management_transfer_credit.submit') }}</button>
                                            </div>
                                        </div>
                                    {{ Form::close() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
