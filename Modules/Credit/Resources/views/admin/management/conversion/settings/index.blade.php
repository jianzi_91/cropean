@extends('templates.admin.master')
@section('title', __('a_page_title.credit conversion settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_credit_convert_settings.credit conversion settings')],
    ],
    'header'=>__('a_credit_convert_settings.credit conversion settings'),
])

<div class="row">
  <div class="col-12 col-lg-12">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <h4 class="card-title">{{ __('a_credit_convert_settings.credit conversion limit') }}</h4>
          {{ Form::open(['method'=>'put', 'route' => ['admin.conversions.settings.update', 'limit']]) }}
          <div class="table-responsive-sm table-responsive-md">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th scope="col" >{{ __('a_credit_convert_settings.from') }}</th>
                <th scope="col">{{ __('a_credit_convert_settings.to') }}</th>
                <th scope="col">{{ __('a_credit_convert_settings.minimum') }}</th>
                <th scope="col">{{ __('a_credit_convert_settings.multiple') }}</th>
                <th scope="col">{{ __('a_credit_convert_settings.admin fee percentage') }}</th>
              </tr>
              @foreach($convertibleCreditTypes as $convertibleCreditType)
                @foreach($convertibleCreditType->canConvertToCredits as $creditType)
                <tr>
                  @if($loop->first)
                  <td rowspan="{{ $loop->count }}">{{ $convertibleCreditType->name }}</td>
                  @endif
                  <td>{{ $creditType->name }}</td>
                  <td>{{ Form::formNumber('minimum[' . $convertibleCreditType->id . '][' . $creditType->id . ']', amount_format($creditType->pivot->minimum), '', ['onClick' => 'this.select()'], false) }}</td>
                  <td>{{ Form::formNumber('multiple[' . $convertibleCreditType->id . '][' . $creditType->id . ']', amount_format($creditType->pivot->multiple), '', ['onClick' => 'this.select()'], false) }}</td>
                  <td>{{ Form::formNumber('admin_fee[' . $convertibleCreditType->id . '][' . $creditType->id . ']', bcmul($creditType->pivot->admin_fee_percentage, 100, 2), '', ['onClick' => 'this.select()'], false) }}</td>
                </tr>
                @endforeach
                
              @endforeach
            </thead>
          </table>
          </div>
          <div class="d-flex justify-content-end mr-2">
            {{ Form::formButtonPrimary('btn_submit1', __('a_credit_convert_settings.save')) }}
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
</div>

<!-- section 2 -->
<div class="row">
  <div class="col-12 col-lg-6">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <h4 class="card-title">{{ __('a_credit_convert_settings.penalty fee % capital credits to cash credits') }}</h4>
          {{ Form::open(['method'=>'put', 'route' => ['admin.conversions.settings.update', 'penalty']]) }}
          <div class="row d-flex justify-content-between p-0 m-0">
            <div class="col-6 p-0 m-0">
              <div class="credit-conversion-settings-label">{{ __('a_credit_convert_settings.1st - 30th day') }}</div>
            </div>
            <div class="col-6 p-0 m-0 d-flex justify-content-end">
              <div class="mr-2">
                {{ Form::formNumber('penalty_percentage_30', bcmul($penaltyRate[30], 100, 2), '', [], false) }}
              </div>
            </div>  
          </div>

          <div class="row d-flex justify-content-between p-0 m-0">
            <div class="col-6 p-0 m-0">
              <div class="credit-conversion-settings-label">{{ __('a_credit_convert_settings.31st - 90th day') }}</div>
            </div>
            <div class="col-6 p-0 m-0 d-flex justify-content-end">
              <div class="mr-2">
                {{ Form::formNumber('penalty_percentage_90', bcmul($penaltyRate[90], 100, 2), '', [], false) }}
              </div>
            </div>  
          </div>
          
          <div class="d-flex justify-content-end mr-2">
            {{ Form::formButtonPrimary('btn_submit2', __('a_credit_convert_settings.save')) }}
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>

<!-- end section 2 -->

<!-- section 3 -->
  {{-- <div class="col-12 col-lg-6">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <h4 class="card-title">{{ __('a_credit_convert_settings.service fee % rollover credits to cash credits') }}</h4>
          {{ Form::open(['method'=>'put', 'route' => ['admin.conversions.settings.update', 'service']]) }}
          <div class="row d-flex justify-content-between p-0 m-0">
            <div class="col-6 p-0 m-0">
              <div class="credit-conversion-settings-label">{{ __('a_credit_convert_settings.fee %') }}</div>
            </div>
            <div class="col-6 p-0 m-0 d-flex justify-content-end">
              <div class="mr-2">
                {{ Form::formNumber('credit_conversion_roll_over_credit_admin_fee_percentage', bcmul(setting('credit_conversion_roll_over_credit_admin_fee_percentage'), 100, 2), '', [], false) }}
              </div>
            </div>  
          </div>
          
          <div class="d-flex justify-content-end mr-2">
            {{ Form::formButtonPrimary('btn_submit3', __('a_credit_convert_settings.save')) }}
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
</div> --}}
<!-- end section 3 -->
@endsection
