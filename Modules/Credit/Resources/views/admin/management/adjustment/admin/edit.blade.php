@extends('templates.admin.master')
@section('title', __('a_admin_management_adjust_credit.adjust credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_admin_management_adjust_credit.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_admin_management_adjust_credit.admin management'), 'route' => route('admin.management.admins.index')],
        ['name' => __('a_admin_management_adjust_credit.adjust credit')]
    ],
    'header'=>__('a_admin_management_adjust_credit.adjust credit'),
    'showBack'=>true,
    'backRoute'=>route('admin.management.admins.index'),
])

@include('templates.admin.includes._am-nav', ['page' => 'adjust credit', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['method'=>'put', 'id'=>'adjust-credit', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formSelect('credit_type', adjustable_credit_types_dropdown(), old('credit_type'),__('a_admin_management_adjust_credit.credit type')) }}
                    {{ Form::formText('amount', old('amount'), __('a_admin_management_adjust_credit.credit amount').' (- 1 / + 1) <span>*</span>') }}
                    {{ Form::formTextArea('remarks', old('remarks'), __('a_admin_management_adjust_credit.credit remarks').' <span>*</span>') }}
                    {{ Form::formPassword('secondary_password', '', __('a_admin_management_adjust_credit.secondary password'), ["required"=>true]) }}
                    <div class="row d-flex justify-content-end m-0">
                        {{ Form::formButtonPrimary('btn_submit', __('a_admin_management_adjust_credit.submit')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>              
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function(){
    // Validation
    CreateValidation('form#adjust-credit', {
        credit_type: {
            presence: { message: __.validation.field_required },
        },
        amount: {
            presence: { message: __.validation.field_required },
        },
        remarks: {
            presence: { message: __.validation.field_required },
        },
        secondary_password: {
            presence: { message: __.validation.field_required },
        }
    });
});
</script>
@endpush
