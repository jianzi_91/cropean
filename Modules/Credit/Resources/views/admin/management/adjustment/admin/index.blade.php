@extends('templates.admin.master')
@section('title', __('a_page_title.adjustments history'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('a_admin_management_adjust_credit.dashboard'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_admin_management_adjust_credit.capital credits'), 'route'=>route('admin.credits.system.capital-credit.index')],
        ['name'=>__('a_admin_management_adjust_credit.adjustments history')],
    ],
    'header'=>__('a_admin_management_adjust_credit.adjustments history'),
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('transaction_date', request('transaction_date'), __('a_admin_management_adjust_credit.date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('member_id', request('member_id'), __('a_admin_management_adjust_credit.member id'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('email', request('email'), __('a_admin_management_adjust_credit.member id'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('transaction_code', request('transaction_code'), __('a_admin_management_adjust_credit.reference number'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('adjustment_type',  adjustment_types_dropdown(), request('adjustment_type'), __('a_admin_management_adjust_credit.adjustment type'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('penalty_days',  penalty_days_dropdown(), request('penalty_days'), __('a_admin_management_adjust_credit.penalty days'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{__('a_admin_management_adjust_credit.transactions')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_admin_management_adjust_credit.total results:') }} {{ $adjustments->total() }}</div>
                @can('admin_user_member_management_adjust_credit_export')
                {{ Form::formTabSecondary(__('a_admin_management_adjust_credit.export'), route('admin.management.members.credits.adjustment.export',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_admin_management_adjust_credit.date') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.reference number') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.member id') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.email address') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.credit type') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.amount') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.adjustment type') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.penalty duration') }}</th>
                    <th>{{ __('a_admin_management_adjust_credit.remarks') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($adjustments as $adjustment)
                <tr>
                    <td>{{ $adjustment->transaction_date }}</td>
                    <td>{{ $adjustment->transaction_code }}</td>
                    <td>{{ $adjustment->member_id }}</td>
                    <td>{{ $adjustment->email }}</td>
                    <td>{{ $adjustment->creditType->name }}</td>
                    <td>{{ amount_format($adjustment->amount, credit_precision($adjustment->creditType->rawname)) }}</td>
                    <td>{{ $adjustment->debit != 0 ? __('a_admin_management_adjust_credit.adjust out') : __('a_admin_management_adjust_credit.adjust in') }}</td>
                    <td>{{ isset($adjustment->penalty_days) ? $adjustment->penalty_days : '-' }}</td>
                    <td>{{ $adjustment->remarks }}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [
                'span' => 9, 'text' => __('a_admin_management_adjust_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $adjustments->render() !!}
@endsection