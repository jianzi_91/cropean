@extends('templates.admin.master')
@section('title', __('a_member_management_adjust_credit.adjust credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_adjust_credit.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_adjust_credit.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_adjust_credit.adjust credit')]
    ],
    'header'=>__('a_member_management_adjust_credit.adjust credit'),
    'showBack'=>true,
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'adjust credit', 'uid' => $user->id ])

<br/>

<div class="row">
    <div class="col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['method'=>'put', 'id'=>'adjust-credit', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formSelect('credit_type', adjustable_credit_types_dropdown(), old('credit_type'),__('a_member_management_adjust_credit.credit type')) }}
                    {{ Form::formText('amount', old('amount'), __('a_member_management_adjust_credit.credit amount').' (- 1 / + 1) <span>*</span>') }}
                    {{ Form::formSelect('penalty_days', penalty_duration_dropdown(), old('penalty_days'),__('a_member_management_adjust_credit.penalty duration')) }}
                    {{ Form::formTextArea('remarks', old('remarks'), __('a_member_management_adjust_credit.credit remarks').' <span>*</span>') }}
                    {{ Form::formPassword('secondary_password', null, __('a_member_management_adjust_credit.secondary password').' <span>*</span>') }}
                    <div class="row d-flex justify-content-end m-0">
                        {{ Form::formButtonPrimary('btn_submit', __('a_member_management_adjust_credit.submit')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function() {
          var capitalCredit = '{{ \Modules\Credit\Contracts\BankCreditTypeContract::CAPITAL_CREDIT }}';

          $('#amount').on('change', penaltyDropdownDisplayToggle);
          $('#credit_type').on('change', penaltyDropdownDisplayToggle);

          function penaltyDropdownDisplayToggle() {
            var creditType = $('#credit_type').val();
            var amount = $('#amount').val();

            if (creditType === capitalCredit) {
              if (amount.trim() === '' || isNaN(amount) || amount < 0) {
                $('#penalty_days').closest('.form-group').hide();
              } else {
                $('#penalty_days').closest('.form-group').show();
              }
            } else {
              $('#penalty_days').closest('.form-group').hide();
            }
          }

          // Just trigger either amount or credit_type will do
          $('#amount').change();
        });
    </script>
@endpush
