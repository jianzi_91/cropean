@extends('templates.admin.master')
@section('title', __('a_page_title.capital credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_credits_system_capital_credit.capital credits')],
    ],
    'header'=>__('a_credits_system_capital_credit.capital credits breakdown'),
    'backRoute' => route('admin.credits.system.capital-credit.index'),
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('start_date', request('start_date'), __('a_credits_system_capital_credit.lock date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('end_date', request('end_date'), __('a_credits_system_capital_credit.unlock date'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('member_id', request('member_id'), __('a_credits_system_capital_credit.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_credits_system_capital_credit.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('name', request('name'), __('a_credits_system_capital_credit.name'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('email', request('email'), __('a_credits_system_capital_credit.email'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_credits_system_capital_credit.lock date') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.unlock date') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.member id') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.name') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.email') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.amount') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($creditLocks as $creditLock)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($creditLock->start_date)->toDateString() }}</td>
                    <td>{{ \Carbon\Carbon::parse($creditLock->end_date)->addDay()->toDateString() }}</td>
                    <td>{{ $creditLock->member_id }}</td>
                    <td>{{ $creditLock->name }}</td>
                    <td>{{ $creditLock->email }}</td>
                    <td>{{ amount_format($creditLock->current_amount) }}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 6,
                'text' => __('a_credits_system_capital_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $creditLocks->render() !!}
@endsection


@push('scripts')
<script>
    $(function() {
        $('#btn_convert').on('click', function(e){
            e.preventDefault()
            window.location.href = '{{ route("member.credits.conversion.create") }}'
        })
    });
</script>
@endpush
