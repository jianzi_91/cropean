@extends('templates.admin.master')
@section('title', __('a_page_title.capital credit'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_credits_system_capital_credit.capital credits')],
    ],
    'header'=>__('a_credits_system_capital_credit.capital credits'),
])

<div class="row">
    <div class="col-12 col-xs-6 col-lg-6 col-xl-6">
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('a_credits_system_capital_credit.capital credits balance'),
            'value'=>amount_format($systemBalance),
            'backgroundClass'=>'widget-capital-image',
        ])
    </div>
    @if (!empty($creditLocking))
    <div class="col-12 col-xs-6 col-lg-6 col-xl-6">
        <a href="{{ route('admin.credits.statements.capital-credit.breakdown') }}">
        @include('templates.__fragments.components.credit-widget-sublabel', [
            'label'=> __('m_capital_credit.locked amount') . ' (' . __('m_capital_credit.unlock after :end_date', ['end_date' => \Carbon\Carbon::parse($creditLocking->unlocking_date)->addDay()->toDateString()]) . ')',
            'value'=> amount_format($creditLocking->unlocking_amount),
            'backgroundClass'=> '',
        ])
        </a>
    </div>
    @endif
</div>

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formDateRange('transaction_date', request('transaction_date'), __('a_credits_system_capital_credit.date')) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('transaction_code', request('transaction_code'), __('a_credits_system_capital_credit.transaction number'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formSelect('bank_transaction_type_id', credit_transaction_types_dropdown(), request('bank_transaction_type_id'), __('a_credits_system_capital_credit.transaction type'),  [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_credits_system_capital_credit.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_credits_system_capital_credit.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_roi.include downlines'), ['isChecked' => request('member_id_downlines', false)]) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_credits_system_capital_credit.transactions')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_credits_system_capital_credit.total results:') }} {{ $statements->total() }}</div>
                @can('admin_user_member_management_adjust_credit_list')
                {{ Form::formTabSecondary(__('a_credits_system_capital_credit.credit adjustment transaction history'), route('admin.management.members.credits.adjustment.index')) }}
                @endcan
                @can('admin_credit_system_capital_credit_export')
                &nbsp;&nbsp;&nbsp;
                    {{ Form::formTabSecondary(__('a_credits_system_capital_credit.export excel'), route('admin.credits.system.capital-credit.export.index',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_credits_system_capital_credit.date') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.transaction number') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.full name') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.email') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.member id') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.transaction type') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.credit') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.debit') }}</th>
                    <th>{{ __('a_credits_system_capital_credit.remarks') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($statements as $statement)
                <tr>
                    <td>{{$statement->transaction_date }}</td>
                    <td>{{$statement->transaction_code}}</td>
                    <td>{{$statement->name}}</td>
                    <td>{{$statement->email}}</td>
                    <td>{{$statement->member_id}}</td>
                    <td>{{display_transaction($statement)}}</td>
                    <td>{{amount_format($statement->credit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{amount_format($statement->debit, credit_precision($statement->creditType->rawname))}}</td>
                    <td>{{$statement->remarks}}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 9,
                'text' => __('a_credits_system_capital_credit.no records') ])
                @endforelse
            </tbody>
        @endcomponent
        {!! $statements->render() !!}
    </div>
</div>
@endsection


@push('scripts')
<script>
    $(function() {

        });
</script>
@endpush
