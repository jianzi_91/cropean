@can('admin_system_transfer_credit')
<h1 class="main">{{ __('a_member_transfer_credit.member transfer') }}</h1>
{{ Form::open(['route' => 'admin.credits.system.transfer.store', 'method'=>'post', 'id'=>'admin-transfer']) }}
<div class="row">
    <div class="col-md-4 col-12">
        {{ Form::formText('transfer_from', request('transfer_from'), __('a_member_transfer_credit.transfer from'), [], true, true )}}

        {{ Form::formText('transfer_to', request('transfer_to'), __('a_member_transfer_credit.transfer to'), [], true, true) }}

        {{ Form::formSelect('credit_type', $credits, request('credit_type'), __('a_member_transfer_credit.credit type'), array('class' => '') ) }}

        {{ Form::formText('amount', request('amount'), __('a_member_transfer_credit.amount'), [], true, true) }}

        {{ Form::formTextArea('remarks', request('remarks'), __('a_member_transfer_credit.remarks'), [], true, true) }}

        {{ Form::formPassword('secondary_password', '', __('a_member_transfer_credit.secondary password'), [], true, true) }}

        <div class="flex">
            <button class="btn-dark js-transfer"
                type="submit">{{ __('a_member_transfer_credit.submit') }}</button>
            <a class="btn-light margin-0-l-button"
                href="{{ url()->previous() }}">{{ __('a_member_transfer_credit.cancel') }}</a>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endcan
