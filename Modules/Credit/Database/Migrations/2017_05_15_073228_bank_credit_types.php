<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankCreditTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_credit_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->unique();
            $table->string('name_translation')->nullable()->index();
            $table->boolean('is_adjustable')->default(0);
            $table->boolean('is_transferable')->default(0);
            $table->boolean('can_deposit')->default(0);
            $table->boolean('can_withdraw')->default(0);
            $table->boolean('can_convert')->default(0);
            $table->boolean('is_admin_only')->default(0);
            $table->enum('network', ['BTC', 'ETH'])->nullable();
            $table->boolean('is_active')->default(0);
            $table->unsignedInteger('deposit_credit_type_id')->nullable();
            $table->unsignedInteger('withdrawal_credit_type_id')->nullable();

            $table->foreign('deposit_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('withdrawal_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_credit_types');
    }
}
