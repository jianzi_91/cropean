<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvertibleCreditTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convertible_credit_types', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unsignedInteger('from_credit_type_id');
            $table->unsignedInteger('to_credit_type_id');
            $table->unsignedDecimal('minimum', 40, 20);
            $table->unsignedDecimal('maximum', 40, 20);
            $table->unsignedDecimal('multiple', 40, 20);
            $table->unsignedDecimal('admin_fee_percentage', 40, 20);
            $table->unsignedSmallInteger('is_active');
            $table->unsignedSmallInteger('is_hidden');

            $table->foreign('from_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('to_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convertible_credit_types');
    }
}
