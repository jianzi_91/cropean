<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToBankCreditTypeAtBankStatement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_statements', function (Blueprint $table) {
            $table->unsignedInteger('to_bank_credit_type_id')->after('bank_credit_type_id')->nullable()->index();
            $table->foreign('to_bank_credit_type_id')->references('id')->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_statements', function (Blueprint $table) {
            $table->dropForeign(['to_bank_credit_type_id']);
            $table->dropColumn('to_bank_credit_type_id');
        });
    }
}
