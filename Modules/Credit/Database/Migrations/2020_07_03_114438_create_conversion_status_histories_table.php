<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_conversion_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
            $table->unsignedInteger('credit_conversion_status_id')->index('ccs_id_index');
            $table->unsignedInteger('credit_conversion_id')->index();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('credit_conversion_status_id', 'ccs_id_foreign')
                ->references('id')
                ->on('credit_conversion_statuses');

            $table->foreign('credit_conversion_id')
                ->references('id')
                ->on('credit_conversions');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_conversion_status_histories');
    }
}
