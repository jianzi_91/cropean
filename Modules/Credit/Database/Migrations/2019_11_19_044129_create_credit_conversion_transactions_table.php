<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditConversionTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_conversion_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('amount', 40, 20)->default(0);
            $table->string('transaction_code')->index()->unique();
            $table->unsignedInteger('credit_conversion_id')->index();
            $table->unsignedInteger('bank_transaction_type_id')->index();
            $table->unsignedDecimal('exchange_rate', 40, 20)->default(0);
            $table->unsignedInteger('bank_credit_type_id')->index();

            $table->foreign('credit_conversion_id')
                ->references('id')
                ->on('credit_conversions');

            $table->foreign('transaction_code')
                ->references('transaction_code')
                ->on('bank_account_statements');

            $table->foreign('bank_transaction_type_id')
                ->references('id')
                ->on('bank_transaction_types');

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_conversion_transactions');
    }
}
