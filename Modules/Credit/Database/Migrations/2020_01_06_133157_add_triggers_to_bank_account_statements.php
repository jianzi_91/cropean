<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTriggersToBankAccountStatements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER update_bank_account_credits_after_insert
            AFTER INSERT ON bank_account_statements
            FOR EACH ROW 
            BEGIN
                DECLARE currentBalance DECIMAL(40, 20) UNSIGNED;
                
                IF NEW.deleted_at IS NULL THEN
                    # Must lock row to ensure there is no data corruption
                    SELECT balance INTO currentBalance FROM bank_account_credits WHERE bank_account_id = NEW.bank_account_id AND bank_credit_type_id = NEW.bank_credit_type_id FOR UPDATE;
                    UPDATE bank_account_credits SET balance = currentBalance + NEW.credit - NEW.debit WHERE bank_account_id = NEW.bank_account_id AND bank_credit_type_id = NEW.bank_credit_type_id;
                END IF;
            END
        ');

        DB::unprepared('
            CREATE TRIGGER update_bank_account_credits_after_update
            AFTER UPDATE ON bank_account_statements
            FOR EACH ROW 
            BEGIN
                DECLARE currentBalance DECIMAL(40, 20) UNSIGNED;
                SELECT balance INTO currentBalance FROM bank_account_credits WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id FOR UPDATE;
                
                IF old.deleted_at IS NULL AND new.deleted_at IS NOT NULL THEN
                    UPDATE bank_account_credits SET balance = currentBalance + OLD.debit - OLD.credit WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id;
                ELSEIF old.deleted_at IS NOT NULL AND new.deleted_at IS NULL THEN
                    UPDATE bank_account_credits SET balance = currentBalance - OLD.debit + OLD.credit WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id;
                ELSEIF old.deleted_at IS NULL AND new.deleted_at IS NULL THEN
                    UPDATE bank_account_credits SET balance = currentBalance + OLD.debit - OLD.credit + NEW.credit - NEW.debit WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id;
                END IF;
            END
        ');

        DB::unprepared('
            CREATE TRIGGER update_bank_account_credits_after_delete
            AFTER DELETE ON bank_account_statements
            FOR EACH ROW 
            BEGIN
                DECLARE currentBalance DECIMAL(40, 20) UNSIGNED;
                
                IF OLD.deleted_at IS NULL THEN
                    # Must lock row to ensure there is no data corruption
                    SELECT balance INTO currentBalance FROM bank_account_credits WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id FOR UPDATE;
                    UPDATE bank_account_credits SET balance = currentBalance + OLD.debit - OLD.credit WHERE bank_account_id = OLD.bank_account_id AND bank_credit_type_id = OLD.bank_credit_type_id;
                END IF;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER update_bank_account_credits_after_insert');
        DB::unprepared('DROP TRIGGER update_bank_account_credits_after_update');
        DB::unprepared('DROP TRIGGER update_bank_account_credits_after_delete');
    }
}
