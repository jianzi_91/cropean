<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankAccountStatements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_statements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->dateTime('transaction_date')->index();
            $table->string('transaction_code')->unique();
            $table->unsignedInteger('bank_transaction_type_id')->index();
            $table->unsignedInteger('bank_credit_type_id')->index();
            $table->unsignedInteger('bank_account_id')->index();
            $table->unsignedInteger('transfer_account_id')->index()->nullable();
            $table->unsignedInteger('done_by')->index();
            $table->text('remarks')->nullable();
            $table->decimal('credit', 40, 20)->default(0.00000000000000000000);
            $table->decimal('debit', 40, 20)->default(0.00000000000000000000);

            //constraints
            $table->foreign('bank_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('transfer_account_id')->references('id')->on('bank_accounts');
            $table->foreign('done_by')->references('id')->on('bank_accounts');
            $table->foreign('bank_transaction_type_id')->references('id')->on('bank_transaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_statements', function (Blueprint $table) {
            $table->dropForeign(['bank_credit_type_id']);
            $table->dropForeign(['bank_account_id']);
            $table->dropForeign(['transfer_account_id']);
            $table->dropForeign(['bank_transaction_type_id']);
        });
        Schema::drop('bank_account_statements');
    }
}
