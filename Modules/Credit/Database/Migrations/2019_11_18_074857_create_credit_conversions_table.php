<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_conversions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedDecimal('source_amount', 40, 20)->default(0);
            $table->unsignedDecimal('penalty_amount', 40, 20)->default(0);
            $table->unsignedDecimal('admin_fee', 40, 20)->default(0);
            $table->unsignedDecimal('admin_fee_percentage', 40, 20)->default(0);
            $table->unsignedDecimal('destination_amount', 40, 20)->default(0);
            $table->unsignedInteger('source_credit_type_id')->index();
            $table->unsignedInteger('destination_credit_type_id')->index();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('source_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('destination_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_conversions');
    }
}
