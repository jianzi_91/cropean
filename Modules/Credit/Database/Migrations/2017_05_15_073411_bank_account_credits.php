<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankAccountCredits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bank_account_id')->index();
            $table->unsignedInteger('bank_credit_type_id')->index();
            $table->unsignedDecimal('balance', 40, 20)->default(0);

            // constraints
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('bank_credit_type_id')->references('id')->on('bank_credit_types');
            $table->unique(['bank_account_id', 'bank_credit_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_credits', function (Blueprint $table) {
            $table->dropForeign(['bank_account_id']);
            $table->dropForeign(['bank_credit_type_id']);
        });
        Schema::drop('bank_account_credits');
    }
}
