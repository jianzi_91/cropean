<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountCreditSnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_credit_snapshots', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('bank_account_id')->index();
            $table->unsignedInteger('bank_credit_type_id')->index();
            $table->unsignedDecimal('balance', 40, 20)->default(0);
            $table->date('run_date')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account_credit_snapshots');
    }
}
