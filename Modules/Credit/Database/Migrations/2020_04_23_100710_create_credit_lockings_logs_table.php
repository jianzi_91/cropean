<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditLockingsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_lockings_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('credit_locking_id')->index();
            $table->decimal('amount', 40, 20)->default(0);
            $table->decimal('penalty_fee_percentages', 40, 20)->default(0);
            $table->string('transaction_code');

            $table->foreign('credit_locking_id')
                ->references('id')
                ->on('credit_lockings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_lockings_logs');
    }
}
