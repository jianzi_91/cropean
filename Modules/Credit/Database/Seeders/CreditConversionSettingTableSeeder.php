<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class CreditConversionSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'credit_conversion',
            'is_active' => 1,
        ];

        if (!SettingCategory::where('name', 'credit_conversion')->exists()) {
            (new SettingCategory($category))->save();
        }

        $settings = [
            [
                'name'                => 'capital_credit_conversion_min',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'capital_credit_conversion_max',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'capital_credit_conversion_multiple',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roll_over_credit_conversion_min',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roll_over_credit_conversion_max',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roll_over_credit_conversion_multiple',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'cash_credit_conversion_min',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'cash_credit_conversion_max',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'cash_credit_conversion_multiple',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'capital_credit_conversion_min')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'capital_credit_conversion_max')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'capital_credit_conversion_multiple')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'roll_over_credit_conversion_min')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'roll_over_credit_conversion_max')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'roll_over_credit_conversion_multiple')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'cash_credit_conversion_min')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'cash_credit_conversion_max')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'cash_credit_conversion_multiple')->first()->id,
                'value'      => '1',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
