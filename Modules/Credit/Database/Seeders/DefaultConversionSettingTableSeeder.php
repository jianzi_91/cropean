<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Models\ConvertibleCreditType;
use Modules\Credit\Contracts\BankCreditTypeContract;

class DefaultConversionSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $creditTypeRepo = resolve(BankCreditTypeContract::class);

        $convertibleCredits = [
            'capital_credit' => [
                'cash_credit' => [
                    'minimum'              => 100,
                    'multiple'             => 100,
                    'admin_fee_percentage' => 0,
                ],
            ],
            'roll_over_credit' => [
                'cash_credit' => [
                    'minimum'              => 10,
                    'multiple'             => 10,
                    'admin_fee_percentage' => 0.2,
                ],
            ],
            'cash_credit' => [
                'roll_over_credit' => [
                    'minimum'              => 10,
                    'multiple'             => 10,
                    'admin_fee_percentage' => 0,
                ],
                'usdt_erc20' => [
                    'minimum'              => 100,
                    'multiple'             => 10,
                    'admin_fee_percentage' => 0.03,
                ],
                'usdc' => [
                    'minimum'              => 100,
                    'multiple'             => 10,
                    'admin_fee_percentage' => 0.03,
                ],
            ],
            'usdt_erc20' => [
                'capital_credit' => [
                    'minimum'              => 500,
                    'multiple'             => 100,
                    'admin_fee_percentage' => 0,
                ],
            ],
            'usdc' => [
                'capital_credit' => [
                    'minimum'              => 500,
                    'multiple'             => 100,
                    'admin_fee_percentage' => 0,
                ],
            ],
        ];

        foreach ($convertibleCredits as $fromCreditType => $toCreditTypes) {
            $fromCreditType = $creditTypeRepo->findBySlug($fromCreditType);

            foreach ($toCreditTypes as $toCreditType => $settings) {
                $toCreditType = $creditTypeRepo->findBySlug($toCreditType);

                ConvertibleCreditType::where('from_credit_type_id', $fromCreditType->id)
                    ->where('to_credit_type_id', $toCreditType->id)
                    ->update($settings);
            }
        }
    }
}
