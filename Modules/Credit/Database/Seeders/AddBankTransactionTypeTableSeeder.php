<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Models\BankTransactionType;

class AddBankTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();

        $bankTransactionTypes = [
            [
                'name' => 'leader_bonus_same_rank',
                'is_active' => 1,
            ],
        ];

        foreach($bankTransactionTypes as $bankTransactionType){
            (new BankTransactionType($bankTransactionType))->save();
        }

        DB::commit();
    }
}
