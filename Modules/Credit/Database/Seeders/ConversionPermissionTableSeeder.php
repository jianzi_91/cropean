<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ConversionPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_credit_conversion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit Conversion',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_conversion_list',
                        'title' => 'Admin Conversion List',
                    ],
                    [
                        'name'  => 'admin_conversion_export',
                        'title' => 'Admin Conversion Export',
                    ],
                    [
                        'name'  => 'admin_conversion_edit',
                        'title' => 'Admin Conversion Edit',
                    ],
                    [
                        'name'  => 'admin_conversion_view',
                        'title' => 'Admin Conversion View',
                    ],
                ]
            ],
            'member_credit_convert' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Member Credit Convert',
                ],
                'abilities' => [
                    [
                        'name'  => 'usdt_erc20_convert',
                        'title' => 'Usdt Erc20 Convert',
                    ],
                    [
                        'name'  => 'usdc_convert',
                        'title' => 'Usdc Convert',
                    ],
                    [
                        'name'  => 'member_conversion_list',
                        'title' => 'Member Conversion List',
                    ],
                    [
                        'name'  => 'member_conversion_export',
                        'title' => 'Member Conversion Export',
                    ],
                    [
                        'name'  => 'member_conversion_create',
                        'title' => 'Member Conversion Create',
                    ],
                    [
                        'name'  => 'member_conversion_edit',
                        'title' => 'Member Conversion Edit',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_conversion_list',
                'admin_conversion_export',
                'admin_conversion_edit',
                'admin_conversion_view',
            ],
            Role::ADMIN => [
                'admin_conversion_list',
                'admin_conversion_export',
                'admin_conversion_edit',
                'admin_conversion_view',
            ],
            Role::MEMBER => [
                'usdt_erc20_convert',
                'usdc_convert',
                'member_conversion_list',
                'member_conversion_create',
                'member_conversion_export',
                'member_conversion_edit',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
