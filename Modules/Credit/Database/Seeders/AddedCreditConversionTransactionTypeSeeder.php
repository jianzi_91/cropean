<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\BankTransactionTypeContract;

class AddedCreditConversionTransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bankTransactionTypeRepository = resolve(BankTransactionTypeContract::class);
        $creditConversionFrom          = $bankTransactionTypeRepository->findBySlug(BankTransactionTypeContract::CREDIT_CONVERSION_FROM);
        $creditConversionTo            = $bankTransactionTypeRepository->findBySlug(BankTransactionTypeContract::CREDIT_CONVERSION_TO);
        $creditConversionFee           = $bankTransactionTypeRepository->findBySlug(BankTransactionTypeContract::CREDIT_CONVERSION_FEE);
        $creditConversionPenaltyFee    = $bankTransactionTypeRepository->findBySlug(BankTransactionTypeContract::CREDIT_CONVERSION_PENALTY_FEE);

        if (!$creditConversionFrom) {
            $bankTransactionTypeRepository->add(['name' => 'credit_conversion_from', 'name_translation' => 'credit conversion from', 'is_active' => 1]);
        }

        if (!$creditConversionTo) {
            $bankTransactionTypeRepository->add(['name' => 'credit_conversion_to', 'name_translation' => 'credit conversion to', 'is_active' => 1]);
        }

        if (!$creditConversionFee) {
            $bankTransactionTypeRepository->add(['name' => 'credit_conversion_fee', 'name_translation' => 'credit conversion fee', 'is_active' => 1]);
        }

        if (!$creditConversionPenaltyFee) {
            $bankTransactionTypeRepository->add(['name' => 'credit_conversion_penalty_fee', 'name_translation' => 'credit conversion penalty fee', 'is_active' => 1]);
        }
    }
}
