<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class CreditConversionAdminFeeSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'credit_conversion',
            'is_active' => 1,
        ];

        if (!SettingCategory::where('name', 'credit_conversion')->exists()) {
            (new SettingCategory($category))->save();
        }

        $settings = [
            [
                'name'                => 'credit_conversion_capital_credit_admin_fee_percentage',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
            [
                'name'                => 'credit_conversion_roll_over_credit_admin_fee_percentage',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
            [
                'name'                => 'credit_conversion_cash_credit_admin_fee_percentage',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'credit_conversion_capital_credit_admin_fee_percentage')->first()->id,
                'value'      => 0,
            ],
            [
                'setting_id' => Setting::where('name', 'credit_conversion_roll_over_credit_admin_fee_percentage')->first()->id,
                'value'      => 0,
            ],
            [
                'setting_id' => Setting::where('name', 'credit_conversion_cash_credit_admin_fee_percentage')->first()->id,
                'value'      => 0,
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
