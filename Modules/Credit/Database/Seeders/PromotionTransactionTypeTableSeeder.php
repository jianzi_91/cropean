<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Models\BankTransactionType;

class PromotionTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bankTransactionTypes = [
            [
                'name'      => 'promotion',
                'is_active' => 1,
            ],
            [
                'name'      => 'promotion_penalty',
                'is_active' => 1,
            ],
            [
                'name'      => 'promotion_clearance',
                'is_active' => 1,
            ],
        ];

        foreach ($bankTransactionTypes as $bankTransactionType) {
            (new BankTransactionType($bankTransactionType))->save();
        }
    }
}
