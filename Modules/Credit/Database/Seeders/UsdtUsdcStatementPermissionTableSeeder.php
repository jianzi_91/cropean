<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class UsdtUsdcStatementPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_credit_system' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit System',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_credit_system_usdt_erc20_credit_list',
                        'title' => 'Admin Credit System Usdt Erc20 Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_system_usdt_erc20_credit_export',
                        'title' => 'Admin Credit System Usdt Erc20 Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_system_usdc_credit_list',
                        'title' => 'Admin Credit System Usdc Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_system_usdc_credit_export',
                        'title' => 'Admin Credit System Usdc Credit Export',
                    ],
                ]
            ],
            'admin_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_credit_statement_usdt_erc20_credit_list',
                        'title' => 'Admin Credit Statement Usdt Erc20 Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_statement_usdt_erc20_credit_export',
                        'title' => 'Admin Credit Statement Usdt Erc20 Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_statement_usdc_credit_list',
                        'title' => 'Admin Credit Statement Usdc Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_statement_usdc_credit_export',
                        'title' => 'Admin Credit Statement Usdc Credit Export',
                    ],
                ]
            ],
            'member_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_credit_statement_usdt_erc20_credit_list',
                        'title' => 'Member Credit Statement Usdt Erc20 Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_usdt_erc20_credit_export',
                        'title' => 'Member Credit Statement Usdt Erc20 Credit Export',
                    ],
                    [
                        'name'  => 'member_credit_statement_usdc_credit_list',
                        'title' => 'Member Credit Statement Usdc Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_usdc_credit_export',
                        'title' => 'Member Credit Statement Usdc Credit Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_credit_system_usdt_erc20_credit_list',
                'admin_credit_system_usdt_erc20_credit_export',
                'admin_credit_system_usdc_credit_list',
                'admin_credit_system_usdc_credit_export',
                'admin_credit_statement_usdt_erc20_credit_list',
                'admin_credit_statement_usdt_erc20_credit_export',
                'admin_credit_statement_usdc_credit_list',
                'admin_credit_statement_usdc_credit_export',
            ],
            Role::ADMIN => [
                'admin_credit_system_usdt_erc20_credit_list',
                'admin_credit_system_usdt_erc20_credit_export',
                'admin_credit_system_usdc_credit_list',
                'admin_credit_system_usdc_credit_export',
                'admin_credit_statement_usdt_erc20_credit_list',
                'admin_credit_statement_usdt_erc20_credit_export',
                'admin_credit_statement_usdc_credit_list',
                'admin_credit_statement_usdc_credit_export',
            ],
            Role::MEMBER => [
                'member_credit_statement_usdt_erc20_credit_list',
                'member_credit_statement_usdt_erc20_credit_export',
                'member_credit_statement_usdc_credit_list',
                'member_credit_statement_usdc_credit_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
