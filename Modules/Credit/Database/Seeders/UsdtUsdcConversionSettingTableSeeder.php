<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class UsdtUsdcConversionSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'credit_conversion',
            'is_active' => 1,
        ];

        if (!SettingCategory::where('name', 'credit_conversion')->exists()) {
            (new SettingCategory($category))->save();
        }

        $settings = [
            [
                'name'                => 'usdt_erc20_conversion_min',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'usdt_erc20_conversion_max',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'usdt_erc20_conversion_multiple',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'credit_conversion_usdt_erc20_admin_fee_percentage',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'usdc_conversion_min',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'usdc_conversion_max',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'usdc_conversion_multiple',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'credit_conversion_usdc_admin_fee_percentage',
                'setting_category_id' => SettingCategory::where('name', 'credit_conversion')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'usdt_erc20_conversion_min')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'usdt_erc20_conversion_max')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'usdt_erc20_conversion_multiple')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'credit_conversion_usdt_erc20_admin_fee_percentage')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'usdc_conversion_min')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'usdc_conversion_max')->first()->id,
                'value'      => '0',
            ],
            [
                'setting_id' => Setting::where('name', 'usdc_conversion_multiple')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'credit_conversion_usdc_admin_fee_percentage')->first()->id,
                'value'      => '0',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
