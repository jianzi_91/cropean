<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;

class AdjustAndTransferCreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();

        BankCreditType::whereIn('name', [
            BankCreditTypeContract::USDT,
        ])->update([
            'is_adjustable'   => 1,
            'is_transferable' => 1
        ]);

        BankCreditType::whereIn('name', [
            BankCreditTypeContract::USDT_OMNI,
            BankCreditTypeContract::BTC,
        ])->update([
            'is_active' => 0,
        ]);

        DB::commit();
    }
}
