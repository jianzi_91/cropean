<?php

namespace Modules\Credit\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Modules\Credit\Models\BankAccount;
use Modules\Credit\Models\BankCreditType;

class PromotionCreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $promotionCredit = BankCreditType::create([
                'created_at'       => now(),
                'updated_at'       => now(),
                'name'             => 'promotion_credit',
                'name_translation' => 'promotion credit',
                'is_adjustable'    => 0,
                'is_transferable'  => 0,
                'can_deposit'      => 0,
                'can_withdraw'     => 0,
                'can_convert'      => 0,
                'is_admin_only'    => 0,
                'is_active'        => 1,
            ]);

            $bankAccounts = BankAccount::all();

            foreach ($bankAccounts as $bankAccount) {
                $bankAccount->accountCredits()->create([
                    'bank_credit_type_id' => $promotionCredit->id
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e);
        }
    }
}
