<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CreditTransferPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'member_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Credit Transfer',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_credit_transfer_create',
                        'title' => 'Member Credit Transfer Create',
                    ],
                    [
                        'name'  => 'member_transfer_usdt_erc20',
                        'title' => 'Member Transfer Usdt Erc20',
                    ],
                    [
                        'name'  => 'member_transfer_usdc',
                        'title' => 'Member Transfer Usdc',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::MEMBER => [
                'member_credit_transfer_create',
                'member_transfer_usdt_erc20',
                'member_transfer_usdc',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
