<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;

class AlterCurrentConversionsStatusTableSeeder extends Seeder
{
    /**
     * Class constructor
     */
    public function __construct(
        CreditConversionContract $creditConversionRepo,
        CreditConversionStatusContract $creditConversionStatusRepo
    ) {
        $this->creditConversionRepo       = $creditConversionRepo;
        $this->creditConversionStatusRepo = $creditConversionStatusRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conversions     = $this->creditConversionRepo->getModel()->all();
        $completedStatus = $this->creditConversionStatusRepo->findBySlug(CreditConversionStatusContract::COMPLETED);

        $this->creditConversionStatusRepo->bulkChangeHistory($conversions, $completedStatus);
        foreach ($conversions as $conversion) {
            $conversion->update([
                'credit_conversion_status_id' => $completedStatus->id,
            ]);
        }
    }
}
