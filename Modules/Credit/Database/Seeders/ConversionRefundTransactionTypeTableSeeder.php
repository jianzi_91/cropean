<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Models\BankTransactionType;

class ConversionRefundTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $bankTransactionTypes = [
            [
                'name'      => 'credit_conversion_cancelled',
                'is_active' => 1,
            ],
            [
                'name'      => 'credit_conversion_fee_cancelled',
                'is_active' => 1,
            ],
            [
                'name'      => 'credit_conversion_penalty_fee_cancelled',
                'is_active' => 1,
            ],
            [
                'name'      => 'credit_conversion_rejected',
                'is_active' => 1,
            ],
            [
                'name'      => 'credit_conversion_fee_rejected',
                'is_active' => 1,
            ],
            [
                'name'      => 'credit_conversion_penalty_fee_rejected',
                'is_active' => 1,
            ],
        ];

        foreach ($bankTransactionTypes as $bankTransactionType) {
            (new BankTransactionType($bankTransactionType))->save();
        }

        DB::commit();
    }
}
