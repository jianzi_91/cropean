<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CreditConversionPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_credit_conversion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit Conversion',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_conversion_setting_view',
                        'title' => 'Admin View Conversion Setting ',
                    ],
                    [
                        'name'  => 'admin_conversion_setting_update',
                        'title' => 'Admin Update Conversion Setting',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_conversion_setting_view',
                'admin_conversion_setting_update',
            ],
            Role::ADMIN => [
                'admin_conversion_setting_view',
                'admin_conversion_setting_update',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
