<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CreditPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_credit_system' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit System',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_credit_system_capital_credit_list',
                        'title' => 'Admin Credit System Capital Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_system_capital_credit_export',
                        'title' => 'Admin Credit System Capital Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_system_roll_over_credit_list',
                        'title' => 'Admin Credit System Roll Over Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_system_roll_over_credit_export',
                        'title' => 'Admin Credit System Roll Over Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_system_cash_credit_list',
                        'title' => 'Admin Credit System Cash Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_system_cash_credit_export',
                        'title' => 'Admin Credit System Cash Credit Export',
                    ],
                ]
            ],
            'admin_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_credit_statement_capital_credit_list',
                        'title' => 'Admin Credit Statement Capital Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_statement_capital_credit_export',
                        'title' => 'Admin Credit Statement Capital Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_statement_roll_over_credit_list',
                        'title' => 'Admin Credit Statement Roll Over Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_statement_roll_over_credit_export',
                        'title' => 'Admin Credit Statement Roll Over Credit Export',
                    ],
                    [
                        'name'  => 'admin_credit_statement_cash_credit_list',
                        'title' => 'Admin Credit Statement Cash Credit List',
                    ],
                    [
                        'name'  => 'admin_credit_statement_cash_credit_export',
                        'title' => 'Admin Credit Statement Cash Credit Export',
                    ],
                ]
            ],
            'member_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_credit_statement_capital_credit_list',
                        'title' => 'Member Credit Statement Capital Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_capital_credit_export',
                        'title' => 'Member Credit Statement Capital Credit Export',
                    ],
                    [
                        'name'  => 'member_credit_statement_roll_over_credit_list',
                        'title' => 'Member Credit Statement Roll Over Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_roll_over_credit_export',
                        'title' => 'Member Credit Statement Roll Over Credit Export',
                    ],
                    [
                        'name'  => 'member_credit_statement_cash_credit_list',
                        'title' => 'Member Credit Statement Cash Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_cash_credit_export',
                        'title' => 'Member Credit Statement Cash Credit Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_credit_system_capital_credit_list',
                'admin_credit_system_capital_credit_export',
                'admin_credit_system_roll_over_credit_list',
                'admin_credit_system_roll_over_credit_export',
                'admin_credit_system_cash_credit_list',
                'admin_credit_system_cash_credit_export',
                'admin_credit_statement_capital_credit_list',
                'admin_credit_statement_capital_credit_export',
                'admin_credit_statement_roll_over_credit_list',
                'admin_credit_statement_roll_over_credit_export',
                'admin_credit_statement_cash_credit_list',
                'admin_credit_statement_cash_credit_export',
            ],
            Role::ADMIN => [
                'admin_credit_system_capital_credit_list',
                'admin_credit_system_capital_credit_export',
                'admin_credit_system_roll_over_credit_list',
                'admin_credit_system_roll_over_credit_export',
                'admin_credit_system_cash_credit_list',
                'admin_credit_system_cash_credit_export',
                'admin_credit_statement_capital_credit_list',
                'admin_credit_statement_capital_credit_export',
                'admin_credit_statement_roll_over_credit_list',
                'admin_credit_statement_roll_over_credit_export',
                'admin_credit_statement_cash_credit_list',
                'admin_credit_statement_cash_credit_export',
            ],
            Role::MEMBER => [
                'member_credit_statement_capital_credit_list',
                'member_credit_statement_capital_credit_export',
                'member_credit_statement_roll_over_credit_list',
                'member_credit_statement_roll_over_credit_export',
                'member_credit_statement_cash_credit_list',
                'member_credit_statement_cash_credit_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
