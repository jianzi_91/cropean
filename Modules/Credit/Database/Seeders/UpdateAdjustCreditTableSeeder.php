<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;

class UpdateAdjustCreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();

        BankCreditType::whereIn('name', [
            BankCreditTypeContract::CAPITAL_CREDIT,
            BankCreditTypeContract::ROLL_OVER_CREDIT,
            BankCreditTypeContract::CASH_CREDIT,
        ])->update([
            'is_adjustable'   => 1
        ]);

        DB::commit();
    }
}
