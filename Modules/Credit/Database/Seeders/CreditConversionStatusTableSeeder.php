<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Models\CreditConversionStatus;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class CreditConversionStatusTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_credit_conversions',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $statuses = [
            0 => [
                'name'      => 'pending',
                'is_active' => 1,
            ],
            1 => [
                'name'      => 'approved',
                'is_active' => 1,
            ],
            2 => [
                'name'      => 'rejected',
                'is_active' => 1,
            ],
            3 => [
                'name'      => 'cancelled',
                'is_active' => 1,
            ],
            4 => [
                'name'      => 'completed',
                'is_active' => 1,
            ],
        ];

        foreach ($statuses as $status) {
            $result = (new CreditConversionStatus($status))->save();
        }
    }
}
