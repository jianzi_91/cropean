<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\ConvertibleCreditType;

class ConvertibleCreditTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $convertableCreditTypes = config('credit.convertible_credit_types');
        $creditRepo             = resolve(BankCreditTypeContract::class);

        foreach ($convertableCreditTypes as $fromCreditType => $toCreditTypes) {
            $convertFrom = $creditRepo->findBySlug($fromCreditType);

            foreach ($toCreditTypes as $toCreditType) {
                $convertTo = $creditRepo->findBySlug($toCreditType);

                ConvertibleCreditType::create([
                    'from_credit_type_id' => $convertFrom->id,
                    'to_credit_type_id'   => $convertTo->id,
                    'minimum'             => 0,
                    'maximum'             => 0,
                    'multiple'            => 0,
                    'is_hidden'           => false,
                    'is_active'           => true,
                ]);
            }
        }
    }
}
