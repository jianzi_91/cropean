<?php

namespace Modules\Credit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CreditAdjustmentListingPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_adjust_credit_list',
                        'title' => 'Admin User Member Management Adjust Credit List',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_adjust_credit_list',
                        'title' => 'Admin User Admin Management Adjust Credit List',
                    ],
                    [
                        'name'  => 'admin_user_member_management_adjust_credit_export',
                        'title' => 'Admin User Member Management Adjust Credit Export',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_adjust_credit_export',
                        'title' => 'Admin User Admin Management Adjust Credit Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_user_member_management_adjust_credit_list',
                'admin_user_admin_management_adjust_credit_list',
                'admin_user_member_management_adjust_credit_export',
                'admin_user_admin_management_adjust_credit_export',
            ],
            Role::ADMIN => [
                'admin_user_member_management_adjust_credit_list',
                'admin_user_admin_management_adjust_credit_list',
                'admin_user_member_management_adjust_credit_export',
                'admin_user_admin_management_adjust_credit_export',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
