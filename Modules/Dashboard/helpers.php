<?php

if (!function_exists('current_user_leader_bonus_rank')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function current_user_leader_bonus_rank($userId)
    {
        $userLeaderBonusRankHistory = resolve(\Modules\Rank\Contracts\UserRankHistoryContract::class);

        $userRank = $userLeaderBonusRankHistory->getModel()
            ->where('user_id', $userId)
            ->current()
            ->first();

        if ($userRank) {
            if ($userRank->is_locked) {
                return $userRank->rank->name ?? null;
            }

            if ($userRank->rank_id > $userRank->qualified_rank_id) {
                return $userRank->leaderBonusRank->name ?? null;
            }

            return $userRank->leaderBonusQualifiedRank->name ?? null;
        }

        $user = resolve(\Modules\User\Contracts\UserContract::class)
            ->getModel()
            ->where('id', $userId)
            ->first();

        return $user ? $user->leader_bonus_rank_name : null;
    }
}

if (!function_exists('current_user_goldmine_rank')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function current_user_goldmine_rank($userId)
    {
        $userGoldmineRankHistory = resolve(\Modules\Rank\Contracts\UserGoldmineRankHistoryContract::class);

        $userGoldmineRank = $userGoldmineRankHistory->getModel()
            ->where('user_id', $userId)
            ->current()
            ->first();

        if ($userGoldmineRank) {
            if ($userGoldmineRank->is_locked) {
                return $userGoldmineRank->goldmineRank->name ?? null;
            }

            if ($userGoldmineRank->goldmine_rank_id > $userGoldmineRank->qualified_goldmine_rank_id) {
                return $userGoldmineRank->goldmineRank->name ?? null;
            }

            return $userGoldmineRank->goldmineQualifiedRank->name ?? null;
        }

        $user = resolve(\Modules\User\Contracts\UserContract::class)
            ->getModel()
            ->where('id', $userId)
            ->first();
        return $user ? $user->goldmine_rank_name : null;
    }
}
