@extends('templates.admin.master')
@section('title', __('a_page_title.dashboard'))

@section('main')
<div class="row">
    @can('admin_dashboard_master_wallet_usdt_erc20_balance')
    <div class="col-12">
        @component('templates.__fragments.components.crypto-dashboard-widget', ['cardId' => 'usdt-balance', 'id'=>'usdt_master_balance', 'address' => setting('master_wallet_usdt_erc20_address'), 'title' => __('a_dashboard.usdt master wallet balance'), 'copyBtn' => __('a_dashboard.copy to clipboard'), 'balance' => 'n/a'])
        @endcomponent
    </div>
    @endcan
</div>
<div class="row">
    @can('admin_dashboard_master_wallet_usdc_balance')
    <div class="col-xs-12 col-lg-6">
        @component('templates.__fragments.components.crypto-dashboard-widget', ['cardId' => 'usdc-balance', 'id'=>'usdc_master_balance', 'address' => setting('master_wallet_usdc_address'), 'title' => __('a_dashboard.usdc master wallet balance'), 'copyBtn' => __('a_dashboard.copy to clipboard'), 'balance' => 'n/a'])
        @endcomponent
    </div>
    @endcan
    @can('admin_dashboard_master_wallet_eth_balance')
    <div class="col-xs-12 col-lg-6">
        @component('templates.__fragments.components.crypto-dashboard-widget', ['cardId' => 'eth-balance', 'id'=>'eth_master_balance', 'address' => setting('master_wallet_eth_address'), 'title' => __('a_dashboard.eth master wallet balance'), 'copyBtn' => __('a_dashboard.copy to clipboard'), 'balance' => 'n/a'])
        @endcomponent
    </div>
    @endcan
</div>
<div class="row">
    @if(auth()->user()->can('admin_dashboard_system_cash_balance')
    || auth()->user()->can('admin_dashboard_system_capital_balance')
    || auth()->user()->can('admin_dashboard_system_rollover_balance')
    || auth()->user()->can('admin_dashboard_system_total_equity'))
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-title">{{ __('a_dashboard.system credits') }}</div>
                    @can('admin_dashboard_system_total_equity')
                    {{ Form::formWidgetInfo('equity_amount', __('a_dashboard.equity amount'), amount_format(bcadd(get_total_equity($exchangeRates, $systemAllCreditBalance[$creditTypes['capital_credit']] ?? 0, $systemAllCreditBalance[$creditTypes['roll_over_credit']] ?? 0), $systemAllCreditBalance[$creditTypes['promotion_credit']], 2) ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_system_capital_balance')
                    <div class="widget-divider extra"></div>
                    {{ Form::formWidgetInfo('capital_credits', __('a_dashboard.capital credits balance'), amount_format($systemAllCreditBalance[$creditTypes['capital_credit']] ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_system_rollover_balance')
                    <div class="widget-divider extra"></div>
                    {{ Form::formWidgetInfo('rollover_credits', __('a_dashboard.rollover credits balance'), amount_format($systemAllCreditBalance[$creditTypes['roll_over_credit']] ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_system_cash_balance')
                    <div class="widget-divider extra"></div>
                    {{ Form::formWidgetInfo('cash_credits', __('a_dashboard.cash credits balance'), amount_format($systemAllCreditBalance[$creditTypes['cash_credit']] ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                    @endcan
                    <div class="widget-divider extra"></div>
                    {{ Form::formWidgetInfo('usdt_credits', __('a_dashboard.usdt credits balance'), amount_format($systemAllCreditBalance[$creditTypes['usdt_erc20']] ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                    <div class="widget-divider extra"></div>
                    {{ Form::formWidgetInfo('usdc_credits', __('a_dashboard.usdc credits balance'), amount_format($systemAllCreditBalance[$creditTypes['usdc']] ?? 0, credit_precision()), 'col-6', 'col-6', true) }}
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-title">{{ __('a_dashboard.accounts') }}</div>
                    @can('admin_dashboard_total_registered_accounts')
                    {{ Form::formWidgetInfo('total_registered', __('a_dashboard.total registered'), $membersCount, 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_total_active_accounts')
                    <div class="widget-divider"></div>
                    {{ Form::formWidgetInfo('total_active', __('a_dashboard.total active status'), $activeMembers, 'col-6', 'col-6', true) }}
                    @endcan
                </div>
            </div>
        </div>
        @canany(['admin_dashboard_kyc_request_status','admin_dashboard_kyc_pending_status'])
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-title">{{ __('a_dashboard.kyc status') }}</div>
                    @can('admin_dashboard_kyc_request_status')
                    {{ Form::formWidgetInfo('approved', __('a_dashboard.approved'), get_kyc_count(\Modules\DocumentVerification\Contracts\UserDocumentStatusContract::APPROVED), 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_kyc_pending_status')
                    <div class="widget-divider"></div>
                    {{ Form::formWidgetInfo('pending', __('a_dashboard.pending'), get_kyc_count(\Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REQUESTED), 'col-6', 'col-6', true) }}
                    @endcan
                </div>
            </div>
        </div>
        @endcanany
    </div>
</div>
<div class="row">
    @canany(['admin_dashboard_daily_cash_deposits','admin_dashboard_monthly_cash_deposits','admin_dashboard_daily_usdt_erc20_deposits','admin_dashboard_monthly_usdt_erc20_deposits','admin_dashboard_daily_usdc_deposits','admin_dashboard_monthly_usdc_deposits','admin_dashboard_daily_approved_deposits','admin_dashboard_monthly_approved_deposits'])
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-title">{{ __('a_dashboard.current deposit amount usd') }}</div>
                    @can('admin_dashboard_daily_cash_deposits')
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.cash approved for the day'), $depositsData['daily']['cash'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_usdt_erc20_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.usdt approved for the day'), $depositsData['daily']['usdt'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_usdc_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.usdc approved for the day'), $depositsData['daily']['usdc'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_approved_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.approved for the day'), $depositsData['daily']['total'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_cash_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.cash approved for the month'), $depositsData['monthly']['cash'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_usdt_erc20_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.usdt approved for the month'), $depositsData['monthly']['usdt'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_usdc_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_day', __('a_dashboard.usdc approved for the month'), $depositsData['monthly']['usdc'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_approved_deposits')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved_month', __('a_dashboard.approved for the month'), $depositsData['monthly']['total'], 'col-6', 'col-6', true) }}
                    @endcan
                </div>
            </div>
        </div>
        </div>
    @endcanany
    @canany(['admin_dashboard_daily_cash_withdrawals','admin_dashboard_monthly_cash_withdrawals','admin_dashboard_daily_usdt_erc20_withdrawals','admin_dashboard_monthly_usdt_erc20_withdrawals','admin_dashboard_daily_usdc_withdrawals','admin_dashboard_monthly_usdc_withdrawals','admin_dashboard_daily_approved_withdrawals','admin_dashboard_monthly_approved_withdrawals'])
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-title">{{ __('a_dashboard.current withdrawal amount usd') }}</div>
                    @can('admin_dashboard_daily_cash_withdrawals')
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.cash approved for the day'), $withdrawalsData['daily']['cash'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_usdt_erc20_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.usdt approved for the day'), $withdrawalsData['daily']['usdt'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_usdc_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.usdc approved for the day'), $withdrawalsData['daily']['usdc'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_daily_approved_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('requested', __('a_dashboard.approved for the day'), $withdrawalsData['daily']['total'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_cash_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.cash approved for the month'), $withdrawalsData['monthly']['cash'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_usdt_erc20_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.usdt approved for the month'), $withdrawalsData['monthly']['usdt'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_usdc_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.usdc approved for the month'), $withdrawalsData['monthly']['usdc'], 'col-6', 'col-6', true) }}
                    @endcan
                    @can('admin_dashboard_monthly_approved_withdrawals')
                    <div class="widget-divider"></div>
                        {{ Form::formWidgetInfo('approved', __('a_dashboard.approved for the month'), $withdrawalsData['monthly']['total'], 'col-6', 'col-6', true) }}
                    @endcan
                </div>
            </div>
        </div>
    </div>
    @endcanany
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $('.kyc-pending-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.documents.index') }}";
    })

    $('.kyc-request-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.documents.index', http_build_query(['status_id' => $documentStatuses[ucfirst(\Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REQUESTED)] ?? ''])) }}";
    })

    $('.capital-statement-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.statements.capital-credit.index') }}";
    })

    $('.rollover-statement-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.statements.rollover-credit.index') }}";
    })

    $('.cash-statement-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.statements.cash-credit.index') }}";
    })

    $('.capital-system-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.system.capital-credit.index') }}";
    })

    $('.rollover-system-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.system.rollover-credit.index') }}";
    })

    $('.cash-system-widget').on('click', function(e) {
        e.preventDefault();
        window.location.href = "{{ route('admin.credits.system.cash-credit.index') }}";
    })

    $(document).ready(function() {
        function getValue(creditType) {
            axios.get('/dashboard/master_wallet_balance/' + creditType)
                .then(function (response) {
                    // console.log($('#eth-balance .widget-value'));
                    if (creditType == "eth") {
                        $('.eth-balance .widget-value').text(response.data.balance);
                    }

                    if (creditType == "usdt_erc20") {
                        $('.usdt-balance .widget-value').text(response.data.balance);
                    }

                    if (creditType == "usdc") {
                        $('.usdc-balance .widget-value').text(response.data.balance);
                    }
                });
        };

        getValue('eth');
        getValue('usdt_erc20');
        getValue('usdc');
    });
</script>
@endpush

