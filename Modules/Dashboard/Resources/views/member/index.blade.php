@extends('templates.member.master')
@section('title', __('m_page_title.dashboard'))

@section('main')
<div class="row">
    @can('member_dashboard_current_total_equity')
    @if(!empty(auth()->user()->julyPromotionPayout) || !empty(auth()->user()->augustPromotionPayout))
    <div class="col-lg-8"> 
        <!-- promo widget -->
        <div class="card d-flex flex-row">
            <div class="col-lg-6" style="border-radius:20px">
                <div class="card-body widget-card-small widget-equity-image">
                    <div class="d-flex flex-column">
                        <div class="widget-label">{{ __('m_dashboard.current total equity') }}</div>
                    </div>
                    <div class="widget-value">{{ amount_format(bcadd(get_total_equity($exchangeRates, auth()->user()->capital_credit ?? 0, auth()->user()->rollover_credit ?? 0), auth()->user()->promotion_credit_balance, 2) ?? 0, credit_precision()) }}</div>
                </div>
            </div> 
            <div class="col-lg-6">
                <div class="card-body widget-card-small widget-promo-credit">
                    <div class="widget-label">{{ __('m_dashboard.promo credits') }}</div>
                    {{-- @if (!empty(auth()->user()->promotionPayout) && !empty(auth()->user()->promotionPayout->amount))
                    {{ Form::formWidgetInfo('june_promo', __('m_dashboard.june promo'), amount_format(auth()->user()->promotionPayout->amount), 'col-6', 'col-6', false, false) }}
                    @endif --}}
                    @if (!empty(auth()->user()->julyPromotionPayout) && !empty(auth()->user()->julyPromotionPayout->amount))
                    {{ Form::formWidgetInfo('july_promo', __('m_dashboard.july promo'), amount_format(auth()->user()->julyPromotionPayout->amount), 'col-6', 'col-6', false, false) }}
                    @endif
                    @if (!empty(auth()->user()->augustPromotionPayout) && !empty(auth()->user()->augustPromotionPayout->amount))
                    {{ Form::formWidgetInfo('august_promo', __('m_dashboard.august promo'), amount_format(auth()->user()->augustPromotionPayout->amount), 'col-6', 'col-6', false, false) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-lg-4">
        @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current total equity'), 'value' => amount_format(get_total_equity($exchangeRates, auth()->user()->capital_credit ?? 0, auth()->user()->rollover_credit ?? 0) ?? 0, credit_precision()), 'backgroundClass' => 'widget-equity-image'])
        @endcomponent
    </div>
    @endif
    @endcan
    @can('member_dashboard_current_capital_balance')
    <div class="col-lg-4">
        @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current capital credit'), 'value' => amount_format(auth()->user()->capital_credit ?? 0, credit_precision()), 'backgroundClass' => 'widget-capital-image'])
        @endcomponent
    </div>
    @endcan
    @if(auth()->user()->can('member_dashboard_current_goldmine_bonus_rank')
    && $userGoldmineRank)
    <div class="col-lg-4"> 
        <div class="card">
            <div class="card-content">
                <div class="card-body d-flex justify-content-between">
                    <div>
                        <div class="widget-label">{{ __('m_dashboard.goldmine rank') }}</div>
                        <div class="widget-value">{{ $userGoldmineRank }}</div>
                    </div>
                    <div>
                        <img src="{{ asset('images/goldmine.png') }}" style="max-height:96px"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
</div>

<div class="row">
    @can('member_dashboard_current_rollover_balance')
        <div class="col-lg-4">
            @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current rollover credit'), 'value' => amount_format(auth()->user()->rollover_credit ?? 0, credit_precision()), 'backgroundClass' => 'widget-rollover-image'])
            @endcomponent
        </div>
    @endcan
    @can('member_dashboard_current_cash_balance')
        <div class="col-lg-4">
            @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current cash credit'), 'value' => amount_format(auth()->user()->cash_credit ?? 0, credit_precision()), 'backgroundClass' => 'widget-cash-image'])
            @endcomponent
        </div>
    @endcan
    @if(auth()->user()->can('member_dashboard_current_leader_bonus_rank')
    && $userRank)
    <div class="col-lg-4">
        <div class="card">
            <div class="card-content">
                <div class="card-body d-flex justify-content-between">
                    <div>
                        <div class="widget-label">{{ __('m_dashboard.leader bonus rank') }}</div>
                        <div class="widget-value">{{ $userRank }}</div>
                    </div>
                    <div>
                        <img src="{{ asset('images/leader_bonus.png') }}" style="max-height:96px"/>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    @endif
</div>

<div class="row">
    @can('member_dashboard_current_usdt_erc20_balance')
        <div class="col-lg-4">
            @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current usdt credit'), 'value' => amount_format(auth()->user()->usdt_erc20_credit ?? 0, credit_precision()), 'backgroundClass' => 'widget-usdt-image'])
            @endcomponent
        </div>
    @endcan
    @can('member_dashboard_current_usdc_balance')
        <div class="col-lg-4">
            @component('templates.__fragments.components.credit-widget-bg-image', ['label' => __('m_dashboard.current usdc credit'), 'value' => amount_format(auth()->user()->usdc_credit ?? 0, credit_precision()), 'backgroundClass' => 'widget-usdc-image'])
            @endcomponent
        </div>
    @endcan
</div>

@can('member_dashboard_announcement')
<div class="row col-12">
    <div class="card">
        <div class="card-content">
            <div class="card-body d-flex justify-content-between align-items-center p-2">
                <h4 class="card-title filter-title p-0 m-0">{{ __('m_dashboard.announcements') }}</h4>
                <a class="hyperlink" href="{{ route('member.announcements.index') }}">
                    {{ __('m_announcements.view more') }}
                </a>
            </div>
            @component('templates.__fragments.components.tables')
                <thead class="text-capitalize">
                    <tr>
                        <th>{{ __('m_announcements.date') }}</th>
                        <th>{{ __('m_announcements.title') }}</th>
                        <th>{{ __('m_announcements.message') }}</th>
                        <th>{{ __('m_announcements.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($announcements as $announcement)
                    <tr>
                        <td>{{ $announcement->created_at }}</td>
                        <td>{{ $announcement->title }}</td>
                        <td>{!! nl2br($announcement->body) !!}</td>
                        @can('member_announcement_show')
                            <td class="action">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" onclick="setValueToModal({{$announcement}})" data-toggle="modal" data-target="#announcementModal"><i class="bx bx-show-alt"></i></a>
                                    &nbsp;&nbsp;
                                    @if($announcement->announcement)
                                        @if ($attachment = $announcement->announcement->attachments->where('locale', app()->getLocale())->first())
                                            <a href="{{ route('member.announcements.attachments.show', $attachment->id) }}">
                                                <i class="bx bx-download"></i>
                                            </a>
                                        @endif
                                    @endif
                                </div>
                            </td>
                        @endcan
                    </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 5, 'text' => __('m_announcements.no records') ])
                @endforelse
                </tbody>
            @endcomponent
        </div>
    </div>
</div>
@endcan

  <!-- Modal -->
<div class="modal fade" id="announcementModal" tabindex="-1" role="dialog" aria-labelledby="announcement"
aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title"></h5>
                    <div class="created-at"></div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            @can('member_announcement_show')
                <div class="modal-footer">
                    <div class="attachment"></div>
                </div>
            @endcan
        </div>
    </div>
</div>

@if (!$hasWatched)
<div class="modal fade" id="welcomeMessageModal" tabindex="-1" role="dialog" aria-labelledby="welcome"
aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <video id="welcome-msg" controls autoplay>
                <source id="welcome-video" src="{{ route('member.videos.show', ['video' => 1]) }}" type="video/mp4">
            </video>
        </div>
    </div>
</div>
@endif
@endsection

@push('scripts')
    <script type="text/javascript">
        function setValueToModal(announcementData) {
            $('.modal-title').text(announcementData.title);
            $('.modal-body').html(announcementData.body);
            $('.modal-header .created-at').text(announcementData.created_at);

            var attachments = announcementData.announcement.attachments;

            if (attachments.length > 0) {
                var currentLocale = '{{ app()->getLocale() }}';
                var attachmentId = 0;
                var attachmentName = '';

                Object.entries(attachments).forEach(function (entry) {
                    let value = entry[1];
                    if (value.locale == currentLocale) {
                        attachmentId = value.id;
                        attachmentName = value.original_filename;
                    }
                });

                var attachmentTxt = '{{ __("m_announcement.attachment") }}';

                if (attachmentId) {
                    link = '{{ route("member.announcements.attachments.show", ":id") }}';
                    link = link.replace(':id', attachmentId);
                    $('.modal-footer').show().find('.attachment').html(
                        attachmentTxt + ' : <a href="' + link + '" target="_blank">' + attachmentName +
                        '</a>'
                    );
                }
            } else {
                $('.modal-footer').hide().find('attachment').html('');
            }

            $('#announcementModal').modal()
        }
    </script>
    @if (!$hasWatched)
    <script type="text/javascript">
        $(document).ready(function() {
            function updateVideoView() {
                axios.put('/videos/1');
            }

            $('#welcomeMessageModal').modal({
                backdrop: 'static',
                keyboard: false,
            });

            $('#welcome-msg').bind('ended', function() {
            updateVideoView();
            $('#welcomeMessageModal').modal('hide');
            });
        })
    </script>
    @endif
@endpush