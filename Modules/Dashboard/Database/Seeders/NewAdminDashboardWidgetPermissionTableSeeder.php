<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class NewAdminDashboardWidgetPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_dashboard' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Dashboard',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_dashboard_total_registered_accounts',
                        'title' => 'Admin Dashboard Total Registered Accounts',
                    ],
                    [
                        'name'  => 'admin_dashboard_total_active_accounts',
                        'title' => 'Admin Dashboard Total Active Accounts',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_approved_deposits',
                        'title' => 'Admin Dashboard Daily Approved Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_approved_deposits',
                        'title' => 'Admin Dashboard Monthly Approved Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_requested_withdrawals',
                        'title' => 'Admin Dashboard Monthly Requested Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_approved_withdrawals',
                        'title' => 'Admin Dashboard Monthly Approved Withdrawals',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_dashboard_total_registered_accounts',
                'admin_dashboard_total_active_accounts',
                'admin_dashboard_daily_approved_deposits',
                'admin_dashboard_monthly_approved_deposits',
                'admin_dashboard_monthly_requested_withdrawals',
                'admin_dashboard_monthly_approved_withdrawals',
            ],
            Role::ADMIN => [
                'admin_dashboard_total_registered_accounts',
                'admin_dashboard_total_active_accounts',
                'admin_dashboard_daily_approved_deposits',
                'admin_dashboard_monthly_approved_deposits',
                'admin_dashboard_monthly_requested_withdrawals',
                'admin_dashboard_monthly_approved_withdrawals',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
