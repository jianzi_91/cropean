<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CreateDashboardPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_dashboard' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Dashboard',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_dashboard_current_capital_balance',
                        'title' => 'Admin Dashboard Current Capital Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_current_rollover_balance',
                        'title' => 'Admin Dashboard Current Rollover Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_current_cash_balance',
                        'title' => 'Admin Dashboard Current Cash Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_system_total_equity',
                        'title' => 'Admin Dashboard System Total Equity',
                    ],
                    [
                        'name'  => 'admin_dashboard_system_capital_balance',
                        'title' => 'Admin Dashboard System Capital Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_system_rollover_balance',
                        'title' => 'Admin Dashboard System Rollover Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_system_cash_balance',
                        'title' => 'Admin Dashboard System Cash Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_kyc_pending_status',
                        'title' => 'Admin Dashboard KYC Pending Status',
                    ],
                    [
                        'name'  => 'admin_dashboard_announcement',
                        'title' => 'Admin Dashboard Announcement',
                    ],
                ]
            ],
            'member_dashboard' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Member Dashboard',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_dashboard_current_goldmine_bonus_rank',
                        'title' => 'Member Dashboard Current Goldmine Bonus Rank',
                    ],
                    [
                        'name'  => 'member_dashboard_current_leader_bonus_rank',
                        'title' => 'Member Dashboard Current Leader Bonus Rank',
                    ],
                    [
                        'name'  => 'member_dashboard_current_total_equity',
                        'title' => 'Member Dashboard Current Total Equity',
                    ],
                    [
                        'name'  => 'member_dashboard_current_capital_balance',
                        'title' => 'Member Dashboard Current Capital Balance',
                    ],
                    [
                        'name'  => 'member_dashboard_current_rollover_balance',
                        'title' => 'Member Dashboard Current Rollover Balance',
                    ],
                    [
                        'name'  => 'member_dashboard_current_cash_balance',
                        'title' => 'Member Dashboard Current Cash Balance',
                    ],
                    [
                        'name'  => 'member_dashboard_announcement',
                        'title' => 'Member Dashboard Announcement',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_dashboard_current_capital_balance',
                'admin_dashboard_current_rollover_balance',
                'admin_dashboard_current_cash_balance',
                'admin_dashboard_system_total_equity',
                'admin_dashboard_system_capital_balance',
                'admin_dashboard_system_rollover_balance',
                'admin_dashboard_system_cash_balance',
                'admin_dashboard_kyc_pending_status',
                'admin_dashboard_announcement'
            ],
            Role::ADMIN => [
                'admin_dashboard_current_capital_balance',
                'admin_dashboard_current_rollover_balance',
                'admin_dashboard_current_cash_balance',
                'admin_dashboard_system_total_equity',
                'admin_dashboard_system_capital_balance',
                'admin_dashboard_system_rollover_balance',
                'admin_dashboard_system_cash_balance',
                'admin_dashboard_kyc_pending_status',
                'admin_dashboard_announcement'
            ],
            Role::MEMBER => [
                'member_dashboard_current_goldmine_bonus_rank',
                'member_dashboard_current_leader_bonus_rank',
                'member_dashboard_current_total_equity',
                'member_dashboard_current_capital_balance',
                'member_dashboard_current_rollover_balance',
                'member_dashboard_current_cash_balance',
                'member_dashboard_announcement'
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
