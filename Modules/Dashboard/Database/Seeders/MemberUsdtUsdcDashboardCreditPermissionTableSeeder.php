<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class MemberUsdtUsdcDashboardCreditPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_dashboard' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Dashboard',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_dashboard_master_wallet_eth_balance',
                        'title' => 'Admin Dashboard Master Wallet Eth Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_master_wallet_usdt_erc20_balance',
                        'title' => 'Admin Dashboard Master Wallet Usdt Erc20 Balance',
                    ],
                    [
                        'name'  => 'admin_dashboard_master_wallet_usdc_balance',
                        'title' => 'Admin Dashboard Master Wallet Usdc Balance',
                    ],
                ]
            ],
            'member_dashboard' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Member Dashboard',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_dashboard_current_usdt_erc20_balance',
                        'title' => 'Member Dashboard Current Usdt Erc20 Balance',
                    ],
                    [
                        'name'  => 'member_dashboard_current_usdc_balance',
                        'title' => 'Member Dashboard Current Usdc Balance',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_dashboard_master_wallet_eth_balance',
                'admin_dashboard_master_wallet_usdt_erc20_balance',
                'admin_dashboard_master_wallet_usdc_balance',
            ],
            Role::ADMIN => [
                'admin_dashboard_master_wallet_eth_balance',
                'admin_dashboard_master_wallet_usdt_erc20_balance',
                'admin_dashboard_master_wallet_usdc_balance',
            ],
            Role::MEMBER => [
                'member_dashboard_current_usdt_erc20_balance',
                'member_dashboard_current_usdc_balance',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
