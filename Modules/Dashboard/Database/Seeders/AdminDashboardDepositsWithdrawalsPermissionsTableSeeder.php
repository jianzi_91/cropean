<?php

namespace Modules\Dashboard\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class AdminDashboardDepositsWithdrawalsPermissionsTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_dashboard_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Dashboard Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_dashboard_daily_cash_deposits',
                        'title' => 'Admin Dashboard Daily Cash Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_cash_deposits',
                        'title' => 'Admin Dashboard Monthly Cash Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_usdt_erc20_deposits',
                        'title' => 'Admin Dashboard Daily USDT Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_usdt_erc20_deposits',
                        'title' => 'Admin Dashboard Monthly USDT Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_usdc_deposits',
                        'title' => 'Admin Dashboard Daily USDC Deposits',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_usdc_deposits',
                        'title' => 'Admin Dashboard Monthly USDC Deposits',
                    ],
                ],
            ],
            'admin_dashboard_withdarawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Dashboard Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_dashboard_daily_approved_withdrawals',
                        'title' => 'Admin Dashboard Daily Approved Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_cash_withdrawals',
                        'title' => 'Admin Dashboard Daily Cash Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_cash_withdrawals',
                        'title' => 'Admin Dashboard Monthly Cash Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_usdt_erc20_withdrawals',
                        'title' => 'Admin Dashboard Daily USDT Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_usdt_erc20_withdrawals',
                        'title' => 'Admin Dashboard Monthly USDT Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_daily_usdc_withdrawals',
                        'title' => 'Admin Dashboard Daily USDT Withdrawals',
                    ],
                    [
                        'name'  => 'admin_dashboard_monthly_usdc_withdrawals',
                        'title' => 'Admin Dashboard Monthly USDT Withdrawals',
                    ],
                ],
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_dashboard_daily_cash_deposits',
                'admin_dashboard_monthly_cash_deposits',
                'admin_dashboard_daily_usdt_erc20_deposits',
                'admin_dashboard_monthly_usdt_erc20_deposits',
                'admin_dashboard_daily_usdc_deposits',
                'admin_dashboard_monthly_usdc_deposits',
                'admin_dashboard_daily_cash_withdrawals',
                'admin_dashboard_monthly_cash_withdrawals',
                'admin_dashboard_daily_usdt_erc20_withdrawals',
                'admin_dashboard_monthly_usdt_erc20_withdrawals',
                'admin_dashboard_daily_usdc_withdrawals',
                'admin_dashboard_monthly_usdc_withdrawals',
                'admin_dashboard_daily_approved_withdrawals',
            ],
            Role::ADMIN => [
                'admin_dashboard_daily_cash_deposits',
                'admin_dashboard_monthly_cash_deposits',
                'admin_dashboard_daily_usdt_erc20_deposits',
                'admin_dashboard_monthly_usdt_erc20_deposits',
                'admin_dashboard_daily_usdc_deposits',
                'admin_dashboard_monthly_usdc_deposits',
                'admin_dashboard_daily_cash_withdrawals',
                'admin_dashboard_monthly_cash_withdrawals',
                'admin_dashboard_daily_usdt_erc20_withdrawals',
                'admin_dashboard_monthly_usdt_erc20_withdrawals',
                'admin_dashboard_daily_usdc_withdrawals',
                'admin_dashboard_monthly_usdc_withdrawals',
                'admin_dashboard_daily_approved_withdrawals',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
