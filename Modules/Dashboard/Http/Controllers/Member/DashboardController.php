<?php

namespace Modules\Dashboard\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Announcement\Queries\Member\AnnouncementI18nQuery;
use Modules\Announcement\Models\Video;
use Modules\Announcement\Models\VideoView;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Queries\Member\SupportTicketQuery;
use Plus65\Base\Paginations\CollectionPagination;

class DashboardController extends Controller
{
    use CollectionPagination;

    /*
     * Support Ticket
     */
    protected $supportTicketQuery;

    /*
     * Support Ticket status
     */
    protected $supportTicketStatus;

    protected $announcementQuery;

    protected $exchangeRateRepo;

    public function __construct(
        SupportTicketQuery $supportTicketQuery,
        SupportTicketStatusContract $supportTicketStatusContract,
        AnnouncementI18nQuery $announcementQuery,
        ExchangeRateContract $exchangeRateRepo
    ) {
        $this->supportTicketQuery  = $supportTicketQuery;
        $this->supportTicketStatus = $supportTicketStatusContract;
        $this->announcementQuery   = $announcementQuery;
        $this->exchangeRateRepo    = $exchangeRateRepo;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $supportTicketStatus = $this->supportTicketStatus->findBySlug($this->supportTicketStatus::OPEN);
        if ($supportTicketStatus) {
            $supportTicketCount = $this->supportTicketQuery->setParameters(['support_ticket_status_id' => $supportTicketStatus->id])->get()->count();
        }

        $videoView = VideoView::where('video_id', 1)
            ->where('user_id', auth()->user()->id)
            ->first();

        $hasWatched = empty($videoView) ? false : true;

        // If this session is admin login as, no need to show video
        if ($request->session()->has('is_admin_login_as')) {
            $hasWatched = true;
        }

        $announcements = $this->announcementQuery
            ->setLocale(session()->get('locale'))
            ->setParameters($request->all())
            ->paginate()
            ->take(config('dashboard.announcement.max', 5));

        $exchangeRates = $this->exchangeRateRepo->getModel()
            ->whereIn('currency', [
                BankCreditTypeContract::CAPITAL_CREDIT,
                BankCreditTypeContract::ROLL_OVER_CREDIT
            ])
            ->pluck('sell_rate', 'currency')
            ->toArray();

        $userGoldmineRank = current_user_goldmine_rank(auth()->user()->id);
        $userRank         = current_user_leader_bonus_rank(auth()->user()->id);
        return view('dashboard::member.index', compact('supportTicketCount', 'announcements', 'exchangeRates', 'userGoldmineRank', 'userRank', 'hasWatched'));
    }
}
