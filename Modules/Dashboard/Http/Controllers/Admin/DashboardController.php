<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Deposit\Contracts\DepositContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Queries\Admin\SupportTicketQuery;
use Modules\User\Models\User;
use Modules\Withdrawal\Contracts\WithdrawalContract;
use Plus65\Base\Paginations\CollectionPagination;

class DashboardController extends Controller
{
    use CollectionPagination;

    /*
     * Support Ticket
     */
    protected $supportTicketQuery;

    /*
     * Support Ticket status
     */
    protected $supportTicketStatus;

    protected $exchangeRateRepo;
    protected $bankCreditTypeRepo;

    public function __construct(
        SupportTicketQuery $supportTicketQuery,
        SupportTicketStatusContract $supportTicketStatusContract,
        ExchangeRateContract $exchangeRateRepo,
        BankCreditTypeContract $bankCreditTypeRepo,
        DepositContract $depositRepo,
        CreditConversionContract $conversionRepo,
        WithdrawalContract $withdrawalRepo,
        BlockchainDepositContract $blockchainDepositRepo,
        BlockchainWithdrawalContract $blockchainWithdrawalRepo
    ) {
        $this->supportTicketQuery       = $supportTicketQuery;
        $this->supportTicketStatus      = $supportTicketStatusContract;
        $this->exchangeRateRepo         = $exchangeRateRepo;
        $this->bankCreditTypeRepo       = $bankCreditTypeRepo;
        $this->depositRepo              = $depositRepo;
        $this->conversionRepo           = $conversionRepo;
        $this->withdrawalRepo           = $withdrawalRepo;
        $this->blockchainDepositRepo    = $blockchainDepositRepo;
        $this->blockchainWithdrawalRepo = $blockchainWithdrawalRepo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $supportTicketStatus = $this->supportTicketStatus->findBySlug($this->supportTicketStatus::OPEN);
        if ($supportTicketStatus) {
            $supportTicketCount = $this->supportTicketQuery->setParameters(['support_ticket_status_id' => $supportTicketStatus->id])->get()->count();
        }

        $exchangeRates = $this->exchangeRateRepo->getModel()
            ->whereIn('currency', [
                BankCreditTypeContract::CAPITAL_CREDIT,
                BankCreditTypeContract::ROLL_OVER_CREDIT
            ])
            ->pluck('sell_rate', 'currency')
            ->toArray();

        $creditTypes = $this->bankCreditTypeRepo->getModel()->whereIn('name', [
            BankCreditTypeContract::CAPITAL_CREDIT,
            BankCreditTypeContract::ROLL_OVER_CREDIT,
            BankCreditTypeContract::CASH_CREDIT,
            BankCreditTypeContract::PROMOTION_CREDIT,
            BankCreditTypeContract::USDT_ERC20,
            BankCreditTypeContract::USDC,
        ])->pluck('name', 'id')->toArray();
        $systemAllCreditBalance = get_system_all_credits($creditTypes);

        $creditTypes      = array_flip($creditTypes);
        $documentStatuses = array_flip(kyc_status_dropdown());

        $membersCount  = User::members()->count();
        $activeMembers = User::members()->active()->count();

        $depositsData    = $this->getDashboardDepositData();
        $withdrawalsData = $this->getDashboardWithdrawalData();

        return view('dashboard::admin.index', compact('supportTicketCount', 'exchangeRates', 'systemAllCreditBalance', 'creditTypes', 'documentStatuses', 'membersCount', 'activeMembers', 'depositsData', 'withdrawalsData'));
    }

    public function getMasterWalletBalance($creditType)
    {
        $masterWallet = setting('master_wallet_' . $creditType . '_address');
        if (!$masterWallet) {
            return 0;
        }

        switch ($creditType) {
            case BankCreditTypeContract::ETH:
                $ethRepo = resolve(EthereumRepository::class);
                $balance = $ethRepo->getBalance($masterWallet);
            break;
            case BankCreditTypeContract::USDT_ERC20:
                $usdtErc20Repo = resolve(UsdtErc20Repository::class);
                $balance       = $usdtErc20Repo->getBalance($masterWallet);
            break;
            case BankCreditTypeContract::USDC:
                $usdcRepo = resolve(UsdcRepository::class);
                $balance  = $usdcRepo->getBalance($masterWallet);
            break;
            default:
                $balance = 0;
            break;
        }

        return response()->json([
            'balance' => amount_format($balance, 2),
        ]);
    }

    protected function getDashboardDepositData()
    {
        // From Bank Deposits
        $dailyCashDeposits   = $this->depositRepo->getModel()->approved()->whereDate('updated_at', now())->sum(DB::raw('amount + admin_fee'));
        $monthlyCashDeposits = $this->depositRepo->getModel()->approved()->whereMonth('updated_at', now())->sum(DB::raw('amount + admin_fee'));

        $usdtErc20Credit = $this->bankCreditTypeRepo->findBySlug(BankCreditTypeContract::USDT_ERC20);
        $usdcCredit      = $this->bankCreditTypeRepo->findBySlug(BankCreditTypeContract::USDC);

        // From Credit Conversion
        // Usdt deposits
        $dailyUsdtDeposits   = $this->blockchainDepositRepo->getModel()
            ->where('deposit_credit_type_id', $usdtErc20Credit->id)
            ->where('is_master_wallet', true)
            ->whereDate('created_at', now())
            ->select(DB::raw("SUM(TRUNCATE(amount, 2)) AS daily_deposits"))
            ->first()
            ->daily_deposits;

        $monthlyUsdtDeposits = $this->blockchainDepositRepo->getModel()
            ->where('deposit_credit_type_id', $usdtErc20Credit->id)
            ->where('is_master_wallet', true)
            ->whereMonth('created_at', now())
            ->select(DB::raw("SUM(TRUNCATE(amount, 2)) AS monthly_deposits"))
            ->first()
            ->monthly_deposits;

        // Usdc deposits
        $dailyUsdcDeposits   = $this->blockchainDepositRepo->getModel()
            ->where('deposit_credit_type_id', $usdcCredit->id)
            ->where('is_master_wallet', true)
            ->whereDate('created_at', now())
            ->select(DB::raw("SUM(TRUNCATE(amount, 2)) AS daily_deposits"))
            ->first()
            ->daily_deposits;

        $monthlyUsdcDeposits = $this->blockchainDepositRepo->getModel()
            ->where('deposit_credit_type_id', $usdcCredit->id)
            ->where('is_master_wallet', true)
            ->whereMonth('created_at', now())
            ->select(DB::raw("SUM(TRUNCATE(amount, 2)) AS monthly_deposits"))
            ->first()
            ->monthly_deposits;

        return [
            'daily' => [
                'cash'  => amount_format($dailyCashDeposits, 2),
                'usdt'  => amount_format($dailyUsdtDeposits, 2),
                'usdc'  => amount_format($dailyUsdcDeposits, 2),
                'total' => amount_format(bcadd(bcadd($dailyCashDeposits, $dailyUsdtDeposits, 2), $dailyUsdcDeposits, 2), 2),
            ],
            'monthly' => [
                'cash'  => amount_format($monthlyCashDeposits, 2),
                'usdt'  => amount_format($monthlyUsdtDeposits, 2),
                'usdc'  => amount_format($monthlyUsdcDeposits, 2),
                'total' => amount_format(bcadd(bcadd($monthlyCashDeposits, $monthlyUsdtDeposits, 2), $monthlyUsdcDeposits, 2), 2),
            ],
        ];
    }

    protected function getDashboardWithdrawalData()
    {
        // From Bank Deposits
        $dailyCashWithdrawals   = $this->withdrawalRepo->getModel()->approved()->whereDate('updated_at', now())->sum('converted_base_amount');
        $monthlyCashWithdrawals = $this->withdrawalRepo->getModel()->approved()->whereMonth('updated_at', now())->sum('converted_base_amount');

        $usdtErc20Credit = $this->bankCreditTypeRepo->findBySlug(BankCreditTypeContract::USDT_ERC20);
        $usdcCredit      = $this->bankCreditTypeRepo->findBySlug(BankCreditTypeContract::USDC);

        // From Credit Conversion
        // Usdt withdrawals
        $dailyUsdtWithdrawals   = $this->blockchainWithdrawalRepo->getModel()
            ->where('destination_credit_type_id', $usdtErc20Credit->id)
            ->whereNotNull('transaction_hash')
            ->whereDate('updated_at', now())
            ->sum('amount');

        $monthlyUsdtWithdrawals = $this->blockchainWithdrawalRepo->getModel()
            ->where('destination_credit_type_id', $usdtErc20Credit->id)
            ->whereNotNull('transaction_hash')
            ->whereMonth('updated_at', now())
            ->sum('amount');

        // Usdc withdrawals
        $dailyUsdcWithdrawals   = $this->blockchainWithdrawalRepo->getModel()
            ->where('destination_credit_type_id', $usdcCredit->id)
            ->whereNotNull('transaction_hash')
            ->whereDate('updated_at', now())
            ->sum('amount');

        $monthlyUsdcWithdrawals = $this->blockchainWithdrawalRepo->getModel()
            ->where('destination_credit_type_id', $usdcCredit->id)
            ->whereNotNull('transaction_hash')
            ->whereMonth('updated_at', now())
            ->sum('amount');

        return [
            'daily' => [
                'cash'  => amount_format($dailyCashWithdrawals, 2),
                'usdt'  => amount_format($dailyUsdtWithdrawals, 2),
                'usdc'  => amount_format($dailyUsdcWithdrawals, 2),
                'total' => amount_format(bcadd(bcadd($dailyCashWithdrawals, $dailyUsdtWithdrawals, 2), $dailyUsdcWithdrawals, 2), 2),
            ],
            'monthly' => [
                'cash'  => amount_format($monthlyCashWithdrawals, 2),
                'usdt'  => amount_format($monthlyUsdtWithdrawals, 2),
                'usdc'  => amount_format($monthlyUsdcWithdrawals, 2),
                'total' => amount_format(bcadd(bcadd($monthlyCashWithdrawals, $monthlyUsdtWithdrawals, 2), $monthlyUsdcWithdrawals, 2), 2),
            ],
        ];
    }
}
