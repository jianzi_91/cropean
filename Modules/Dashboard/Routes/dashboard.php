<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\DashboardController@index']);
        Route::get('dashboard/master_wallet_balance/{creditType}', ['as' => 'admin.dashboard.master_wallet.balance', 'uses' => 'Admin\DashboardController@getMasterWalletBalance']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('dashboard', ['as' => 'member.dashboard', 'uses' => 'Member\DashboardController@index']);
    });
});
