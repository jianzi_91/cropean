<?php

namespace Modules\Rbac\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Rbac\Rules\RoleHasNoUser;
use Modules\Rbac\Rules\RoleIsNotSystem;

class Delete extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:roles,id',
                new RoleIsNotSystem(),
                new RoleHasNoUser(),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function validationData()
    {
        $this->merge([
            'id' => $this->role,
        ]);

        return parent::validationData();
    }
}
