<?php

namespace Modules\Rbac\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.*' => [
                'required',
                'max:255',
                'unique:roles,name,' . $this->role,
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validations.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
    }
}
