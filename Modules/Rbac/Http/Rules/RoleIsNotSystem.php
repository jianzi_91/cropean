<?php

namespace Modules\Rbac\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Rbac\Contracts\RoleContract;

class RoleIsNotSystem implements Rule
{
    /**
     * The role repository
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->roleRepository = resolve(RoleContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $role = $this->roleRepository->find($value);

        if (!$role) {
            return false;
        }

        return !$role->is_system;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.role is system defined');
    }
}
