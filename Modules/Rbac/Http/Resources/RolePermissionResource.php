<?php

namespace Modules\Rbac\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RolePermissionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
