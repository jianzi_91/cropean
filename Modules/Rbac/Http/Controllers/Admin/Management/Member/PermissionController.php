<?php

namespace Modules\Rbac\Http\Controllers\Admin\Management\Member;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Rbac\Contracts\PermissionContract;
use Modules\Rbac\Http\Requests\Admin\Management\Member\Edit;
use Modules\Rbac\Models\Permission;
use Modules\Rbac\Queries\Admin\PermissionQuery;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Response\ApiResponse;
use Plus65\Utility\Exceptions\ApiException;

class PermissionController extends Controller
{
    use ApiResponse;
    /**
     * The permission repository
     *
     * @var unknown
     */
    protected $permissionRepository;

    /**
     * The permission query
     *
     * @var unknown
     */
    protected $permissionQuery;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param PermissionContract $permissionContract

     * @param PermissionQuery $permissionQuery
     * @param UserContract $userContract
     */
    public function __construct(
        PermissionContract $permissionContract,
        PermissionQuery $permissionQuery,
        UserContract $userContract
    ) {
        $this->middleware('permission:admin_user_member_management_update_permissions')->only('edit', 'update');

        $this->permissionRepository = $permissionContract;
        $this->permissionQuery      = $permissionQuery;
        $this->userRepository       = $userContract;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param unknown $id
     * @return Response
     */
    public function edit(Edit $request, $id)
    {
        if (!$user = $this->userRepository->find($id)) {
            return back()->with('error', __('a_member_permissions.user not found'));
        }

        $request->merge([
            'ability_category_id' => $user->role->ability_category->id
        ]);

        $categoryPermissions = $this->permissionQuery
            ->setParameters($request->all())
            ->get([
                'abilities.id',
                'abilities.name',
                'abilities.name_translation as ability',
                'ability_ability_section.ability_section_id',
                'ability_sections.name_translation as ability_section'
            ]);

        $forbiddenAbilities = $user->getForbiddenAbilities()->keyBy('id');
        $userAbilities      = Permission::where('entity_id', $user->id)
            ->where('entity_type', 'Modules\User\Models\User')
            ->where('forbidden', 0)
            ->pluck('ability_id', 'ability_id');

        $userRoleCategoryPermissions = [];

        foreach ($categoryPermissions as $record) {
            $record->forbidden                                       = isset($forbiddenAbilities[$record->id])? 1:0;
            $record->allow                                           = isset($userAbilities[$record->id])? 1:0;
            $record->role                                            = (isset($forbiddenAbilities[$record->id]) || isset($userAbilities[$record->id]))? 0:1;
            $userRoleCategoryPermissions[$record->ability_section][] = $record;
        }

        return view('rbac::admin.management.member.edit')->with(compact('user', 'userRoleCategoryPermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  unknown $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            switch ($request->permission) {
                case 'role':
                    $this->permissionRepository->defaultToRole($id, $request->ability_id);
                    $message = __('a_member_permissions.permission turn to follow role');
                    break;
                case 'allow':
                    $this->permissionRepository->allowUserTo($id, $request->ability_id);
                    $message = __('a_member_permissions.permission assigned to user');
                    break;
                case 'forbidden':
                    $this->permissionRepository->disallowUserTo($id, $request->ability_id);
                    $message = __('a_member_permissions.permission revoked from user');
                    break;
            }

            DB::commit();

            return $this->respondSuccess($message);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e);
        }
    }
}
