<?php

namespace Modules\Rbac\Http\Controllers\Admin\Role;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Rbac\Contracts\RoleContract;
use Modules\Rbac\Contracts\RoleI18nContract;
use Modules\Rbac\Http\Requests\Admin\Role\Delete;
use Modules\Rbac\Http\Requests\Admin\Role\Edit;
use Modules\Rbac\Http\Requests\Admin\Role\Store;
use Modules\Rbac\Http\Requests\Admin\Role\Update;
use Modules\Rbac\Models\Role;
use Modules\Rbac\Queries\Admin\RoleI18nQuery;
use Modules\Rbac\Queries\Admin\RoleQuery;

class RoleController extends Controller
{
    /**
     * The role service.
     *
     * @var RoleService
     */
    protected $roleRepository;

    /**
     * The role query.
     *
     * @var role query
     */
    protected $roleQuery;

    protected $roleI18nQuery;

    protected $roleI18nRepo;

    /**
     * Create a new controller instance.
     *
     * @param RoleContract $roleRepository
     * @param RoleQuery $roleQuery
     * @param RoleI18nQuery $i18nQuery
     * @param RoleI18nContract $i18nContract
     */
    public function __construct(RoleContract $roleRepository, RoleQuery $roleQuery, RoleI18nQuery $i18nQuery, RoleI18nContract $i18nContract)
    {
        $this->middleware('permission:admin_role_list')->only(['index']);
        $this->middleware('permission:admin_role_create')->only(['create', 'store']);
        $this->middleware('permission:admin_role_edit')->only(['edit', 'update']);
        $this->middleware('permission:admin_role_delete')->only(['destroy']);

        $this->roleRepository = $roleRepository;
        $this->roleQuery      = $roleQuery;
        $this->roleI18nQuery  = $i18nQuery;
        $this->roleI18nRepo   = $i18nContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param unknown $id
     * @return Response
     */
    public function index(Request $request)
    {
        $roles = $this->roleI18nQuery
            ->setLocale(session()->get('locale'))
            ->setParameters($request->all())
            ->paginate();

        return view('rbac::admin.role.index')->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('rbac::admin.role.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();

        try {
            $defaultLanguage = app()->getLocale();
            $data            = $request->only('name');

            $roleId = DB::table('roles')->insertGetId([
                'created_at' => now(),
                'updated_at' => now(),
                'name'       => $data['name'][$defaultLanguage],
            ]);
            if ($roleId) {
                foreach ($data['name'] as $code => $name) {
                    $this->roleI18nRepo->add([
                        'role_id'                => $roleId,
                        'translator_language_id' => get_translator_language_id($code),
                        'name'                   => $name
                    ]);
                }
            }

            DB::commit();

            return redirect()->route('admin.roles.index')->with('success', __('a_roles.role successfully added'));
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(Edit $request, $id)
    {
        if (!$role = $this->roleRepository->find($id, ['translations'])) {
            return back()->with('error', __('a_roles.role not found'));
        }

        return view('rbac::admin.role.edit')->with(compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Edit $request
     * @param unknown $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        DB::beginTransaction();

        try {
            $data = $request->only('name');

            $this->roleI18nRepo->edit($id, $data['name']);
            DB::commit();

            return redirect()->route('admin.roles.index')->with('success', __('a_roles.role successfully updated'));
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Delete $request
     * @param unknown $id
     * @return Response
    */
    public function destroy(Delete $request, $id)
    {
        DB::beginTransaction();

        try {
            $this->roleRepository->delete($id);

            $this->roleI18nRepo->delete($id);
            DB::commit();

            return redirect()->route('admin.roles.index')->with('success', __('a_roles.role successfully deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', $e->getMessage())->withInput();
        }
    }
}
