<?php

namespace Modules\Rbac\Http\Controllers\Admin\Role;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Rbac\Contracts\PermissionContract;
use Modules\Rbac\Contracts\RoleContract;
use Modules\Rbac\Http\Requests\Admin\Role\BulkUpdatePermission;
use Modules\Rbac\Http\Resources\RolePermissionResource;
use Modules\Rbac\Queries\Admin\PermissionQuery;
use Plus65\Base\Response\ApiResponse;
use Plus65\Utility\Exceptions\ApiException;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class PermissionController extends Controller
{
    use ApiResponse;

    /**
     * The permission repository
     *
     * @var unknown
     */
    protected $permissionRepository;

    /**
     * The permission query
     *
     * @var unknown
     */
    protected $permissionQuery;

    /**
     * The role repository
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * Class constructor
     *
     * @param PermissionContract $permissionContract
     * @param PermissionQuery $permissionQuery
     * @param RoleContract $roleContract
     */
    public function __construct(
        PermissionContract $permissionContract,
        PermissionQuery $permissionQuery,
        RoleContract $roleContract
    ) {
        $this->middleware('permission:admin_role_edit')->only('index', 'update');

        $this->permissionRepository = $permissionContract;
        $this->permissionQuery      = $permissionQuery;
        $this->roleRepository       = $roleContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param unknown $id
     * @return Response
     */
    public function index(Request $request, $id)
    {
        if (!$role = $this->roleRepository->find($id)) {
            return back()->with('error', __('a_role_permissions.role not found'));
        }

        $request->merge([
            'ability_category_id' => $role->ability_category->id
        ]);

        $categoryPermissions = $this->permissionQuery
            ->setParameters($request->all())
            ->get([
                'abilities.id',
                'abilities.name_translation as ability',
                'ability_ability_section.ability_section_id',
                'ability_sections.name_translation as ability_section'
            ]);

        $rolePermissions         = $role->abilities->keyBy('id');
        $roleCategoryPermissions = [];

        foreach ($categoryPermissions as $record) {
            $record->forbidden                                   = !isset($rolePermissions[$record->id]);
            $roleCategoryPermissions[$record->ability_section][] = $record;
        }

        return view('rbac::admin.role.permission')->with(compact('roleCategoryPermissions', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  unknown $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            if ($request->allow) {
                $this->permissionRepository->allowRoleTo($id, $request->permission);
                $message = __('a_role_permissions.permission assigned to role');
            } else {
                $this->permissionRepository->disallowRoleTo($id, $request->permission);
                $message = __('a_role_permissions.permission revoked from role');
            }

            DB::commit();

            return response()->json(['message' => $message], HttpResponse::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the specified resource in storage.
     *
     * @param  Request $request
     * @param  unknown $id
     * @return Response
     */
    public function show(Request $request, $roleId, $permissionId)
    {
        //get current role
        $permission = $this->permissionRepository->getModel()::where('ability_id', $permissionId)
            ->where('entity_id', $roleId)
            ->where('entity_type', 'roles')
            ->first();

        if ($permission) {
            //show the user with specific permission
            $userPermissions = $this->permissionRepository->getModel()::join('users', 'users.id', '=', 'permissions.entity_id')
                ->where('ability_id', $permissionId)
                ->where('forbidden', $permission->forbidden? 0:1)
                ->where('entity_type', 'Modules\User\Models\User')
                ->select([
                    'users.id',
                    'users.name',
                    'users.email',
                ])
                ->get();
        } else {
            $userPermissions = $this->permissionRepository->getModel()::join('users', 'users.id', '=', 'permissions.entity_id')
                ->where('ability_id', $permissionId)
                ->where('forbidden', 0)
                ->where('entity_type', 'Modules\User\Models\User')
                ->select([
                    'users.id',
                    'users.name',
                    'users.email',
                ])
                ->get();
        }
        return RolePermissionResource::collection($userPermissions);
    }

    /**
     * Bulk Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  unknown $id
     * @return Response
     */
    public function bulkUpdate(BulkUpdatePermission $request, $roleId, $permissionId)
    {
        DB::beginTransaction();
        try {
            $this->permissionRepository->bulkDefaultToRole($request->user_ids, $permissionId);

            DB::commit();

            return $this->respondSuccess(__('a_role_permissions.permission update successfully for default settings'));
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e);
        }
    }
}
