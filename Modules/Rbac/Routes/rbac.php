<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        // Member Management
        Route::get('members/{id}/permissions', ['as' => 'admin.management.members.permissions.edit', 'uses' => 'Admin\Management\Member\PermissionController@edit']);
        Route::put('members/{id}/permissions', ['as' => 'admin.management.members.permissions.update', 'uses' => 'Admin\Management\Member\PermissionController@update']);

        // Admin Management
        Route::get('admins/{id}/permissions', ['as' => 'admin.management.admins.permissions.edit', 'uses' => 'Admin\Management\Admin\PermissionController@edit']);
        Route::put('admins/{id}/permissions', ['as' => 'admin.management.admins.permissions.update', 'uses' => 'Admin\Management\Admin\PermissionController@update']);

        // Role Management
        Route::put('roles/{id}/permissions/bulk/{permission}', ['as' => 'admin.roles.permissions.bulk.update', 'uses' => 'Admin\Role\PermissionController@bulkUpdate']);
        Route::resource('roles/{id}/permissions', 'Admin\Role\PermissionController', ['as' => 'admin.roles'])->only(['index', 'update', 'show']);
        Route::resource('roles', 'Admin\Role\RoleController', ['as' => 'admin'])->except(['show']);
    });
});
