<?php

namespace Modules\Rbac\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface RoleI18nContract extends CrudContract
{
}
