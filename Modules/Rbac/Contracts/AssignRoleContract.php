<?php

namespace Modules\Rbac\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface AssignRoleContract extends CrudContract, SlugContract
{
    // add functions here
}
