<?php

namespace Modules\Rbac\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;
use Plus65\Base\Repositories\Contracts\SoftDeleteable;

interface PermissionContract extends CrudContract, SlugContract, SoftDeleteable
{
    /**
     * Assign role an ability
     *
     * @param int $roleId
     * @param int $abilityId
     * @return void
     */
    public function allowRoleTo($roleId, $abilityId);

    /**
     * Revoke role an ability
     *
     * @param int $roleId
     * @param int $abilityId
     * @return void
     */
    public function disallowRoleTo($roleId, $abilityId);

    /**
     * Assign user an ability
     *
     * @param int $userId
     * @param int $abilityId
     * @return void
     */
    public function allowUserTo($userId, $abilityId);

    /**
     * Revoke user an ability
     *
     * @param int $userId
     * @param int $abilityId
     * @return void
     */
    public function disallowUserTo($userId, $abilityId);
}
