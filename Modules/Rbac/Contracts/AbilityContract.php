<?php

namespace Modules\Rbac\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;
use Plus65\Base\Repositories\Contracts\SoftDeleteable;

interface AbilityContract extends CrudContract, SlugContract, SoftDeleteable
{
}
