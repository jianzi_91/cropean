<?php

namespace Modules\Rbac\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;
use Plus65\Base\Repositories\Contracts\SoftDeleteable;
use Modules\User\Models\User;
use Modules\User\Models\UserStatus;

interface RoleContract extends CrudContract, SlugContract, SoftDeleteable
{
    /**
     * Update member role based on change of member status.
     *
     * @param string $prefix
     * @return string The new member ID
     * @param int $length
     */
    public function updateMemberRole(User $user, UserStatus $status);
}
