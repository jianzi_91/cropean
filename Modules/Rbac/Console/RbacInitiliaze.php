<?php

namespace Modules\Rbac\Console;

use Illuminate\Console\Command;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RbacInitiliaze extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rbac:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize RBAC.';

    /**
     * The ability category repository
     *
     * @var unknown
     */
    protected $abilityCategoriesRepository;

    /**
     * Class constructor
     *
     * @param AbilityCategoryContract $abilityCategoryContract
     */
    public function __construct(AbilityCategoryContract $abilityCategoryContract)
    {
        $this->abilityCategoriesRepository = $abilityCategoryContract;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $abilityCategories = config('rbac.ability_categories');
        if ($abilityCategories) {
            foreach ($abilityCategories as $slug => $category) {
                $row = $this->abilityCategoriesRepository->add(['name' => $slug]);
            }
        }

        $this->info('Rbac initalized...');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
