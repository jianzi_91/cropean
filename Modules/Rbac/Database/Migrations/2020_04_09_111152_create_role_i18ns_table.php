<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleI18nsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_i18ns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('role_id')->unsigned();
            $table->integer('translator_language_id')->unsigned();
            $table->string('name', 255)->collation('utf8mb4_unicode_ci');

            // foreign key
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('translator_language_id')->references('id')->on('translator_languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_i18ns');
    }
}
