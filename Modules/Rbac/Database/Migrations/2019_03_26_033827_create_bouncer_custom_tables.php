<?php

use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Silber\Bouncer\Database\Models;

class CreateBouncerCustomTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ability_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name', 150)->index()->unique();
            $table->string('name_translation');
        });

        Schema::create('ability_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->index();
            $table->string('name_translation');
            $table->unsignedInteger('ability_category_id')->nullable();
        });

        Schema::create('ability_ability_section', function (Blueprint $table) {
            $table->unsignedInteger('ability_section_id')->index();
            $table->unsignedInteger('ability_id')->index();

            $table->foreign('ability_section_id')
                ->references('id')->on('ability_sections');

            $table->foreign('ability_id')
                ->references('id')->on('abilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Models::table('ability_ability_section'));
        Schema::drop(Models::table('ability_sections'));
        Schema::drop(Models::table('ability_categories'));
    }
}
