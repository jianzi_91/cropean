<?php

namespace Modules\Rbac\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class RolesTableSeeder extends Seeder
{
    protected $pageRepo;
    protected $groupRepo;

    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $translatorLanguageContract = resolve(TranslatorLanguageContract::class);

        $translatorLanguages = $translatorLanguageContract->all();

        Model::unguard();

        $this->pageRepo->add([
            'name'                => 's_roles',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $data = [
            [
                'name'      => \Modules\Rbac\Models\Role::SYSADMIN,
                'is_system' => 1,
            ],
            [
                'name'      => \Modules\Rbac\Models\Role::ADMIN,
                'is_system' => 1,
            ],
            [
                'name'      => \Modules\Rbac\Models\Role::MEMBER,
                'is_system' => 1,
            ],
            [
                'name'      => \Modules\Rbac\Models\Role::MEMBER_ON_HOLD,
                'is_system' => 1,
            ],
            [
                'name'      => \Modules\Rbac\Models\Role::MEMBER_SUSPENDED,
                'is_system' => 1,
            ],
            [
                'name'      => \Modules\Rbac\Models\Role::MEMBER_TERMINATED,
                'is_system' => 1,
            ],
        ];

        foreach ($data as $role) {
            $newRoleId = DB::table('roles')->insertGetId(['name' => $role['name'], 'is_system' => 1, 'created_at' => now(), 'updated_at' => now()]);
            foreach ($translatorLanguages as $translatorLanguage) {
                DB::table('role_i18ns')->insert([
                    'role_id'                => $newRoleId,
                    'translator_language_id' => $translatorLanguage->id,
                    'name'                   => ucwords(preg_replace('/_+/', ' ', trim($role['name']))),
                    'created_at'             => now(),
                    'updated_at'             => now()
                ]);
            }
        }
    }
}
