<?php

namespace Modules\Rbac\Database\Seeders;

use Artisan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class RbacDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('rbac:init');

        $this->call(RolesTableSeeder::class);
    }
}
