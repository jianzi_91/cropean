<?php

namespace Modules\Rbac\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class RbacPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);
        $abilitySections         = [
            'admin_role' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Role',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_role_list',
                        'title' => 'Admin Role List',
                    ],
                    [
                        'name'  => 'admin_role_create',
                        'title' => 'Admin Create Role',
                    ],
                    [
                        'name'  => 'admin_role_edit',
                        'title' => 'Admin Edit Role',
                    ],
                    [
                        'name'  => 'admin_role_delete',
                        'title' => 'Admin Delete Role',
                    ]
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_role_list',
                'admin_role_create',
                'admin_role_edit',
                'admin_role_delete',
            ],
            Role::ADMIN => [
                'admin_role_list',
                'admin_role_create',
                'admin_role_edit',
                'admin_role_delete',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
