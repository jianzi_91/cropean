<?php

namespace Modules\Rbac\Providers;

use Bouncer;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Rbac\Console\RbacInitiliaze;
use Modules\Rbac\Models\Ability;
use Modules\Rbac\Models\Role;

class RbacServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * The commands
     *
     * @var array
     */
    protected $commands = [
        RbacInitiliaze::class
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        Bouncer::useRoleModel(Role::class);
        Bouncer::useAbilityModel(Ability::class);

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerBindings();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->commands($this->commands);
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/rbac');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/rbac';
        }, \Config::get('view.paths')), [$sourcePath]), 'rbac');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/rbac');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'rbac');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'rbac');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * Bind interfaces to implementations
     *
     * @return void
     */
    public function registerBindings()
    {
        foreach (config('rbac.bindings') as $key => $binding) {
            $this->app->bind($key, $binding);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('rbac.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php',
            'rbac'
        );
    }
}
