<?php

return [
    'name'     => 'Rbac',
    'bindings' => [
        Modules\Rbac\Contracts\AbilityCategoryContract::class => Modules\Rbac\Repositories\AbilityCategoryRepository::class,
        Modules\Rbac\Contracts\AbilitySectionContract::class  => Modules\Rbac\Repositories\AbilitySectionRepository::class,
        Modules\Rbac\Contracts\AbilityContract::class         => Modules\Rbac\Repositories\AbilityRepository::class,
        Modules\Rbac\Contracts\RoleContract::class            => Modules\Rbac\Repositories\RoleRepository::class,
        Modules\Rbac\Contracts\PermissionContract::class      => Modules\Rbac\Repositories\PermissionRepository::class,
        Modules\Rbac\Contracts\RoleI18nContract::class        => Modules\Rbac\Repositories\RoleI18nRepository::class,
        Modules\Rbac\Contracts\AssignRoleContract::class      => Modules\Rbac\Repositories\AssignRoleRepository::class,
    ],
    'ability_categories' => [
        'admin'  => 'Admin',
        'member' => 'Member'
    ],
];
