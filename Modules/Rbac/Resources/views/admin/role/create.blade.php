@extends('templates.admin.master')
@section('title', __('a_create_roles.roles'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_create_roles.roles'), 'route'=>route('admin.roles.index')],
        ['name'=>__('a_create_roles.create role')],
    ],
    'header'=>__('a_create_roles.new role'),
    'backRoute'=>route('admin.roles.index'),
]
)

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                <h4 class="card-title ar-right-title-btn">{{__('a_create_roles.create new role')}}</h4>
                    {{ Form::open(['route' => 'admin.roles.store', 'method'=>'post', 'id'=>'create-role', 'enctype'=>'multipart/form-data']) }}
                    @foreach(languages() as $code => $language)
                    {{ Form::formText("name[{$code}]", old("name[{$code}]"), __('a_create_roles.name'). ' ('. $code . ')', ['errorkey'=>"title.{$code}", 'required'=>true]) }}
                    @endforeach
                    <div style="width:100%;text-align:right;">
                    {{ Form::formButtonPrimary('btn_submit', __('a_create_roles.submit')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    // Frontend to validate
// $(function(){
//     CreateValidation('form#create-role', {
//         'name[]': {
//             presence: { message: __.validation.field_required },
//         },
//     });
// });
</script>
@endpush
