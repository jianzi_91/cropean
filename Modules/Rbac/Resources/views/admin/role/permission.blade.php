@extends('templates.admin.master')
@section('title'){{ __('a_role_permissions.roles') }}@endsection

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
  ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
  ['name'=>__('a_role_permissions.roles'), 'route' => route('admin.roles.index')],
  ['name'=>__('a_role_permissions.permissions')]
],
    'header'=>__('a_role_permissions.permissions'),
    'showBack'=>true,
    'backRoute'=>route('admin.roles.index'),
])

<div class="col-lg-6 col-xs-12">
    @foreach ($roleCategoryPermissions as $category => $permissions)
    <div class="card">
        <div class="card-content">
            <div class="card-body border-bottom">
                <div class="card-title p-0 m-0">{{ __('s_ability_sections.'.$category) }}</div>
            </div>
            @for ($i = 0; $i < count($permissions); $i +=1)
            <div class="d-flex justify-content-between m-1 pl-1">
                {{ Form::formCheckbox($permissions[$i]->id, $permissions[$i]->id, $permissions[$i]->id, __('s_abilities.'.$permissions[$i]->ability), ['isChecked'=>!$permissions[$i]->forbidden, 'class'=>'js-checkbox', 'id'=>'permission_'.$permissions[$i]->id]) }}
                <div id="js-list-data-{{ $permissions[$i]->id }}" data-id="{{ $permissions[$i]->id }}" data-forbidden="{{ $permissions[$i]->forbidden ? 1 : 0 }}" style="display: none;"></div>
                {{ Form::formButtonPrimary("btn_".$permissions[$i]->id, __('a_role_permissions.list')) }}
            </div>
            @endfor
        </div>
    </div>
    <br/>
    @endforeach
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('.btn-primary').on('click', function() {
        var id = $(this).attr('id').replace('btn_', '');
        var divData = $('#js-list-data-' + id);
        var isForbidden = divData.data('forbidden');
        var onOff = isForbidden === 1 ? "{{ __('a_roles_permission.on') }}" : "{{ __('a_roles_permission.off') }}";
        var descString = "{{ __('a_roles_permission.users with :onOff setting') }}";
        descString = descString.replace(':onOff', onOff);

        var route = '{{ route("admin.roles.permissions.show", [$role->id, 1]) }}';
        var routeArr = route.split('/');
        routeArr.pop();
        route = routeArr.join('/') + '/' + id;

        axios.get(route)
          .then(function(resp) {
              var data = resp.data.data;
              var body = '';

              if (data.length > 0) {
                  body += '<div class="modal_cb_list p-1">';
                  _.forEach(data, function(val) {
                    body += '<div class="custom-control custom-checkbox"><input class="js-user-cb custom-control-input" id="user_' + val.id + '" name="user_' + val.id + '" type="checkbox" value="' + val.id + '"><label class="custom-control-label" for="user_' + val.id + '">' + val.email + '</label></div>';
                  });
                  body += '</div>';
              } else {
                  body = '<div class="modal_no_data">{{ __("a_roles_permission.no users") }}</div>'
              }

              $('.modal_body').html(body);
          })
          .catch(function(err) {
            var error = err.response.data.errors;
            var errorKey = Object.keys(error);
            CreateNoty({ type: 'error', text:error[errorKey[0]]});
          });

        swal.fire({
            title: descString,
            showCancelButton: true,
            cancelButtonText: '{{ __("a_roles_permission.cancel") }}',
            confirmButtonText: '{{ __("a_roles_permission.apply") }}',
            showLoaderOnConfirm: true,
            html: '<div><div class="modal_header">{{ __("a_roles_permission.select and apply current settings for these users") }}</div><div class="modal_body"></div></div>',
            preConfirm: function(e) {
                var updateRoute = '{{ route("admin.roles.permissions.bulk.update", [$role->id, 1]) }}';
                var routeArr = updateRoute.split('/');
                routeArr.pop();
                updateRoute = routeArr.join('/') + '/' + id;

                const selectedIds = _.map($('.js-user-cb:checked'), function(val) {
                    return val.id.replace('user_', '');
                });

                if (selectedIds.length > 0) {
                  axios.put(updateRoute, { user_ids: selectedIds })
                    .then(function(resp) {
                      CreateNoty({ type: 'success', text:'{{ __("a_roles_permission.user permission successfully updated")  }}'});
                    })
                    .catch(function(err) {
                      CreateNoty({ type: 'error', text:'{{ __("a_roles_permission.you cannot update this users permissions")  }}'});
                    });
                } else {
                  CreateNoty({ type: 'error', text:'{{ __("a_roles_permission.no users selected")  }}'});
                }
            }
        })
    });

    $(".js-checkbox").change(function () {
        var rawRoute = '{{ route("admin.roles.permissions.update", [$role->id, 1]) }}';
        var route = rawRoute.slice(0, -1) + this.value;

        axios.put(route, { allow: this.checked })
            .then(function (response) {
                CreateNoty({ type: 'success', text:'{{ __('a_roles_permission.permission successfully updated')  }}'});
            })
            .catch(function (error) {
                CreateNoty({ type: 'error', text:'{{ __('a_roles_permission.you cannot update permissions')  }}'});
            })
    });

});
</script>
@endpush