@extends('templates.admin.master')
@section('title', __('a_roles.roles'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_roles.roles')],
    ],
    'header'=>__('a_roles.roles'),
])

<div class="mb-2">
    @can('admin_role_create')
    {{ Form::formButtonPrimary('btn_create', __('a_admin_management.create new role')) }}
    @endcan
</div>

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body d-flex justify-content-between align-items-center p-2">
                    <h4 class="card-title filter-title p-0 m-0">{{__('a_roles.role list')}}</h4>
                </div>
                @component('templates.__fragments.components.tables')
                <thead class="text-capitalize">
                    <tr>
                        <th>{{ __('a_roles.role') }}</th>
                        <th>{{ __('a_roles.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($roles as $role)
                    <tr>
                        <td>{{ $role->name }}</td>
                        <td class="action">
                            @can('admin_role_edit')
                                <a href="{{ route('admin.roles.edit', $role->role_id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_roles.edit') }}">
                                    <i class="bx bx-pencil"></i>
                                </a>
                                <a href="{{ route('admin.roles.permissions.index', $role->role_id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_roles.permissions') }}">
                                    <i class="bx bx-shield"></i>
                                </a>
                            @endcan
                            @can('admin_role_delete')
                                @if(!$role->is_system)
                                <a class="js-action_delete jv-table-icons" data-id="{{ $role->role_id }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_roles.delete') }}">
                                    <i class="bx bx-trash-alt"></i>
                                </a>
                                @endif
                            @endcan
                        </td>
                    </tr>
                    @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 2,
                    'text' => __('a_roles.no records') ])
                    @endforelse
                </tbody>
                @endcomponent
            </div>
        </div>
    </div>
</div>
{!! $roles->render() !!}
@endsection

@push('scripts')
<script type="text/javascript">
$(function(){
    $('.js-action_delete').click(function (ev) {
        var id = $(this).data('id');
        confirm(id, $(this))
    })

    $('#btn_create').click(function (e) {
        e.preventDefault();
        window.location.href = '{{ route("admin.roles.create") }}'
    })

    function confirm(id, self) {
        var url = '{{ route("admin.roles.destroy", ":id") }}';
        url = url.replace(':id', id);
        swal.fire({
            title: "{{ ucfirst(__('a_roles.confirm delete')) }}",
            html: "<p>{{ ucfirst(__('a_roles.are you sure you want to delete this role?')) }}</p>",
            showCancelButton: true,
            cancelButtonText: "{{ __('a_roles.no') }}",
            confirmButtonText: "{{ __('a_roles.yes') }}",
            preConfirm: function (res) {
                return axios({method: 'post', url: url , data: { _method: 'delete' } })
                .then(function (response) {
                    setTimeout(function () {
                        location.reload()
                    }, 300)
                })
                .catch(function (error) {
                    AutoPopulateError(error.response.data);
                });
            }
        })
    }
});

function goToRoles() {
    window.location.href = "{{ route('admin.roles.create') }}";
}

</script>
@endpush
