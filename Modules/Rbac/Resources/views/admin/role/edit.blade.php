@extends('templates.admin.master')
@section('title', __('a_edit_roles.roles'))
@section('main')

@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
    ['name'=>__('a_edit_roles.roles'), 'route' => route('admin.roles.index')],
    ['name'=>__('a_edit_roles.edit')]
],
    'header'=>__('a_edit_roles.edit role'),
    'backRoute'=>route('admin.roles.index'),
])

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                <h4 class="card-title ar-right-title-btn">{{__('a_edit_roles.edit role')}}</h4>
                    {{ Form::open(['route' => ['admin.roles.update', $role->id], 'method'=>'put', 'id'=>'edit-role', 'enctype'=>'multipart/form-data']) }}
                    @foreach($role->translations as $key => $translate)
                    {{ Form::formText("name[{$translate->language->code}]", $translate->name , __('a_edit_roles.name'). ' ('. $translate->language->code .')', ['required'=>true]) }}
                    @endforeach
                    <div style="width:100%;text-align:right;">
                    {{ Form::formButtonPrimary('btn_submit', __('a_edit_roles.submit')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    // Frontend to validate
// $(function(){
//     CreateValidation('form#edit-role', {
//         'name': {
//             presence: { message: __.validation.field_required },
//         },
//     });
// });
</script>
@endpush
