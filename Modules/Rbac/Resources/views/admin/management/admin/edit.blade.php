@extends('templates.admin.master')
@section('title', __('a_admin_management_permission.permissions'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_admin_management_permission.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_admin_management_permission.admin management'), 'route' => route('admin.management.admins.index')],
        ['name' => __('a_admin_management_permission.permissions')]
    ],
    'header'=>__('a_admin_management_permission.permissions'),
    'showBack'=>true,
    'backRoute'=>route('admin.management.admins.index'),
])


@include('templates.admin.includes._am-nav', ['page' => 'permissions', 'uid' => $user->id ])
<br />

<div class="col-lg-8 col-xl-12 col-xs-12" style="min-width:750px">
    @foreach ($userRoleCategoryPermissions as $category => $permissions)
    <div class="card">
        <div class="card-content roles_cont">
            <div class=" card-body border-bottom">
                <div class="card-title p-0 m-0">{{ __('s_ability_sections.'.$category) }}</div>
            </div>
            @for ($i = 0; $i < count($permissions); $i +=1)
            <div class="d-flex justify-content-between m-1 pl-1">
                <span class="label">{{ __('s_abilities.'.$permissions[$i]->ability) }}</span>
                <div class="permission_tabs" id="{{ $permissions[$i]->id }}">
                    <span name="role" class="{{ $permissions[$i]->role ? 'active':'' }}" onclick="setPermissionType('role', {{ $permissions[$i]->id }})">{{ __('a_admin_management_permission.roles') }}</span>
                    <span name="allow" class="{{ $permissions[$i]->allow? 'active':'' }}" onclick="setPermissionType('allow', {{ $permissions[$i]->id }})">{{ __('a_admin_management_permission.on') }}</span>
                    <span name="forbidden" class="{{ $permissions[$i]->forbidden? 'active':'' }}" onclick="setPermissionType('forbidden', {{ $permissions[$i]->id }})">{{ __('a_admin_management_permission.off') }}</span>
                </div>
            </div>
            @endfor
            </div>
        </div>
        <br />
    @endforeach
</div>
@endsection


@push('scripts')
<script type="text/javascript">
    $(function () {
    //     $(".permissions :checkbox").change(function () {
    //         var route = '{{ route("admin.management.members.permissions.update", $user->id)}}';

    //         axios.put(route, { permission: 'forbidden', ability_id: this.value })
    //             .then(function (response) {
    //                 CreateNoty({ "success", text:"{{ __('a_admin_management_permission.permission successfully updated')}}"});
    //             })
    //             .catch(function (error) {
    //                 CreateNoty({ "error", text:"{{ __('a_admin_management_permission.you cannot update permissions')}}"});
    //             })
    //     });
    });

    function setPermissionType(type, id) {
        var route = '{{ route("admin.management.admins.permissions.update", $user->id)}}';
        axios.put(route, {
                permission: type,
                ability_id: id
            })
            .then(function (response) {
                CreateNoty({
                    type: "success",
                    text: "{{ __('a_admin_management_permission.permission successfully updated')  }}"
                });
                $("#" + id + " span").removeClass('active');
                $("#" + id + " span[name='" + type + "']").addClass('active');
            })
            .catch(function (error) {
                CreateNoty({
                    type: "error",
                    text: "{{ __('a_admin_management_permission.you cannot update permissions')}}"
                });
                FormPopulateError(error.response.data);
            });
    }
</script>
@endpush
