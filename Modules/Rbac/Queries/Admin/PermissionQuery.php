<?php

namespace Modules\Rbac\Queries\Admin;

use Modules\Rbac\Queries\PermissionQuery as BaseQuery;

class PermissionQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'ability_category_id' => [
            'filter' => 'equal',
            'table'  => 'ability_categories',
            'column' => 'id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }
}
