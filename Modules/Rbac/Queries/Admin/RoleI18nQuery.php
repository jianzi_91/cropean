<?php

namespace Modules\Rbac\Queries\Admin;

use Modules\Rbac\Models\Role;
use Modules\Rbac\Queries\RoleI18nQuery as BaseQuery;

class RoleI18nQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'roles',
            'column' => 'created_at'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do not show sysadmin records in rbac page for admin other than sysadmin
        return $this->builder->join('roles', function ($query) {
            $query->on('roles.id', 'role_i18ns.role_id')
                ->whereNull('roles.deleted_at');
        })
            ->where('roles.name', '<>', Role::SYSADMIN)
            ->select(['role_i18ns.*', 'roles.is_system']);
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }
}
