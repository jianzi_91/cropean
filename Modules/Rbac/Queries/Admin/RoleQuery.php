<?php

namespace Modules\Rbac\Queries\Admin;

use Modules\Rbac\Models\Role;
use Modules\Rbac\Queries\RoleQuery as BaseQuery;

class RoleQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
        return $this->builder->where('name', '<>', Role::SYSADMIN);
    }
}
