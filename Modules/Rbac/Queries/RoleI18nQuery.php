<?php

namespace Modules\Rbac\Queries;

use Modules\Rbac\Models\RoleI18n;
use QueryBuilder\QueryBuilder;

class RoleI18nQuery extends QueryBuilder
{
    protected $locale = 'cn';

    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        $languageId = get_translator_language($this->locale)->id;
        return RoleI18n::where('translator_language_id', $languageId)
            ->whereNull('role_i18ns.deleted_at')
            ->orderBy('role_i18ns.created_at', 'desc');
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->builder = $this->query();
        return $this;
    }
}
