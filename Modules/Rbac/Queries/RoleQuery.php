<?php

namespace Modules\Rbac\Queries;

use QueryBuilder\QueryBuilder;
use Modules\Rbac\Models\Role;

class RoleQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = Role::query();

        return $query;
    }
}
