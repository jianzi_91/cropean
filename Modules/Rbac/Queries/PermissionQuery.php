<?php

namespace Modules\Rbac\Queries;

use QueryBuilder\QueryBuilder;
use Modules\Rbac\Models\Ability;

class PermissionQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return Ability::join('ability_ability_section', 'ability_ability_section.ability_id', '=', 'abilities.id')
            ->join('ability_sections', 'ability_sections.id', '=', 'ability_ability_section.ability_section_id')
            ->join('ability_categories', 'ability_categories.id', '=', 'ability_sections.ability_category_id')
            //->where('is_hidden', 0)
            ->groupBy(
                'abilities.id',
                'abilities.title_translation',
                'ability_ability_section.ability_section_id',
                'ability_sections.name_translation'
            );
    }
}
