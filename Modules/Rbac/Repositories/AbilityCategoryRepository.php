<?php

namespace Modules\Rbac\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\AbilityCategory;

class AbilityCategoryRepository extends Repository implements AbilityCategoryContract
{
    use HasCrud, HasSlug;

    /**
     * Constructor
     * @param Menu $model
     */
    public function __construct(AbilityCategory $model)
    {
        $this->slug = 'name';
        parent::__construct($model);
    }
}
