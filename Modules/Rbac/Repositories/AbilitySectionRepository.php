<?php

namespace Modules\Rbac\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Rbac\Contracts\AbilitySectionContract;
use Modules\Rbac\Models\AbilitySection;

class AbilitySectionRepository extends Repository implements AbilitySectionContract
{
    use HasCrud, HasSlug;

    /**
     * Constructor
     * @param Menu $model
     */
    public function __construct(AbilitySection $model)
    {
        $this->slug = 'name';
        parent::__construct($model);
    }
}
