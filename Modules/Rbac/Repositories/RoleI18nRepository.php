<?php

namespace Modules\Rbac\Repositories;

use Modules\Rbac\Contracts\RoleI18nContract;
use Modules\Rbac\Models\RoleI18n;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class RoleI18nRepository extends Repository implements RoleI18nContract
{
    use HasCrud;
    protected $model;

    /**
     * RoleI18nRepository constructor.
     * @param RoleI18n $model
     */
    public function __construct(RoleI18n $model)
    {
        $this->slug                       = 'name';
        $this->model                      = $model;

        parent::__construct($model);
    }

    /**
     * @param \Plus65\Base\Repositories\Contracts\unknown $id
     * @param array $attributes
     * @param bool $forceFill
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model|void
     */
    public function edit($id, array $attributes = [], $forceFill = false)
    {
        foreach ($attributes as $locale => $name) {
            $model               = $this->model->where('role_id', $id)->where('translator_language_id', get_translator_language_id($locale))->first();
            $translateAttributes = [
                'name' => $attributes[$locale],
            ];
            $model->fill($translateAttributes)->save();
        }
    }

    /**
     * @param \Plus65\Base\Repositories\Contracts\unknown $ids
     * @return int|void
     */
    public function delete($ids)
    {
        $this->model->where('role_id', $ids)->delete();
    }
}
