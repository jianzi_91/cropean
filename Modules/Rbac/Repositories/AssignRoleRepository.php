<?php

namespace Modules\Rbac\Repositories;

use Modules\Rbac\Contracts\AssignRoleContract;
use Modules\Rbac\Models\AssignedRole;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class AssignRoleRepository extends Repository implements AssignRoleContract
{
    use HasCrud, HasSlug;

    /**
     * Constructor
     *
     * @param Menu $model
     */
    public function __construct(AssignedRole $model)
    {
        $this->slug = 'entity_id';
        parent::__construct($model);
    }
}
