<?php

namespace Modules\Rbac\Repositories;

use Bouncer;
use Modules\Rbac\Contracts\PermissionContract;
use Modules\Rbac\Models\Ability;
use Modules\Rbac\Models\Permission;
use Modules\Rbac\Models\Role;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class PermissionRepository extends Repository implements PermissionContract
{
    use HasCrud, HasSlug;

    /**
     * Constructor
     *
     * @param Menu $model
     */
    public function __construct(Permission $model)
    {
        $this->slug = 'name';
        parent::__construct($model);
    }

    /**
     * Assign role an ability
     *
     * @param int $roleId
     * @param int $abilityId
     * @return void
     */
    public function allowRoleTo($roleId, $abilityId)
    {
        $role    = Role::findOrFail($roleId);
        $ability = Ability::findOrFail($abilityId);

        Bouncer::allow($role)->to($ability);
    }

    /**
     * Revoke role an ability
     *
     * @param int $roleId
     * @param int $abilityId
     * @return void
     */
    public function disallowRoleTo($roleId, $abilityId)
    {
        $role    = Role::findOrFail($roleId);
        $ability = Ability::findOrFail($abilityId);

        Bouncer::disallow($role)->to($ability);
    }

    /**
     * Assign user an ability
     *
     * @param int $userId
     * @param int $abilityId
     * @return void
     */
    public function allowUserTo($userId, $abilityId)
    {
        $user    = User::findOrFail($userId);
        $ability = Ability::findOrFail($abilityId);

        $forbiddenAbilities = $user->getForbiddenAbilities()->keyBy('id');

        if (isset($forbiddenAbilities[$ability->id])) {
            $user->unforbid($ability);
        }

        $user->allow($ability);

        return true;
    }

    /**
     * Revoke user an ability
     *
     * @param int $userId
     * @param int $abilityId
     * @return void
     */
    public function disallowUserTo($userId, $abilityId)
    {
        $user    = User::findOrFail($userId);
        $ability = Ability::findOrFail($abilityId);

        $userAbilities = $user->getAbilities()->keyBy('id');

        $user->forbid($ability);

        $user->disallow($ability);

        return true;
    }

    /**
     * Default user ability
     *
     * @param int $userId
     * @param int $abilityId
     * @return void
     */
    public function defaultToRole($userId, $abilityId)
    {
        return $this->model::where('entity_id', $userId)
            ->where('entity_type', 'Modules\User\Models\User')
            ->where('ability_id', $abilityId)
            ->delete();
    }

    /**
     * bulk update user ability to role
     *
     * @param array $userIds
     * @param int $abilityId
     * @return void
     */
    public function bulkDefaultToRole($userIds, $abilityId)
    {
        foreach (array_chunk($userIds, 100) as $userId) {
            $this->model::whereIn('entity_id', $userId)
                ->where('entity_type', 'Modules\User\Models\User')
                ->where('ability_id', $abilityId)
                ->delete();
        }

        return true;
    }
}
