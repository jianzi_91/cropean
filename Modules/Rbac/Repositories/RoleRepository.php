<?php

namespace Modules\Rbac\Repositories;

use Modules\Rbac\Contracts\RoleContract;
use Modules\Rbac\Models\Role;
use Modules\User\Contracts\UserStatusContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class RoleRepository extends Repository implements RoleContract
{
    use HasCrud, HasSlug;

    protected $userStatusRepository;

    /**
     * Constructor
     * @param Menu $model
     */
    public function __construct(Role $model, UserStatusContract $userStatusContract)
    {
        $this->slug = 'name';

        $this->userStatusRepository = $userStatusContract;
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Rbac\Contracts\RoleContract::updateMemberRole()
     * return $role_id
     */
    public function updateMemberRole($user, $status)
    {
        if ($user->is_member) {
            $user->retract($user->role);
            switch ($status->rawname) {
                case $this->userStatusRepository::ACTIVE:
                    $role = $this->findBySlug(Role::MEMBER);
                    $user->assign($role);
                    return true;
                break;
                case $this->userStatusRepository::HOLD:
                    $role = $this->findBySlug(Role::MEMBER_ON_HOLD);
                    $user->abilities()->detach();
                    $user->assign($role);
                    return true;
                break;
                case $this->userStatusRepository::SUSPENDED:
                    $role = $this->findBySlug(Role::MEMBER_SUSPENDED);
                    $user->abilities()->detach();
                    $user->assign($role);
                    return true;
                break;
                case $this->userStatusRepository::TERMINATED:
                    $role = $this->findBySlug(Role::MEMBER_TERMINATED);
                    $user->abilities()->detach();
                    $user->assign($role);
                    return true;
                break;
                default:
                break;
            }
        }

        return false;
    }
}
