<?php

namespace Modules\Rbac\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Rbac\Contracts\AbilityContract;
use Modules\Rbac\Models\Ability;

class AbilityRepository extends Repository implements AbilityContract
{
    use HasCrud, HasSlug;

    /**
     * Constructor
     * @param Menu $model
     */
    public function __construct(Ability $model)
    {
        $this->slug = 'name';
        parent::__construct($model);
    }
}
