<?php

if (!function_exists('admin_roles_dropdown')) {

    /**
     * Get admin roles list.
     *
     * @return unknown
     */
    function admin_roles_dropdown()
    {
        /** @var \Modules\Rbac\Repositories\RoleRepository $roleRepository */
        $roleRepository = resolve(Modules\Rbac\Contracts\RoleContract::class);

        return $roleRepository->getModel()->admins()
            ->join('role_i18ns', 'role_i18ns.role_id', 'roles.id')
            ->where('translator_language_id', get_translator_language_id(session()->get('locale')))
            ->get([
                'role_i18ns.name',
                'roles.id'
            ])
            ->dropDown('name', 'id');
    }
}
