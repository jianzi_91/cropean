<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\User\Models\User;
use Plus65\Base\Models\Concerns\HasDropDown;
use Silber\Bouncer\Database\Concerns\HasAbilities;
use Silber\Bouncer\Database\Role as BaseRole;

class Role extends BaseRole
{
    use SoftDeletes, HasAbilities, HasDropDown;

    const SYSADMIN          = 'sysadmin';
    const ADMIN             = 'admin';
    const MEMBER            = 'member';
    const MEMBER_SUSPENDED  = 'member_suspended';
    const MEMBER_TERMINATED = 'member_terminated';
    const MEMBER_ON_HOLD    = 'member_on_hold';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'level',
        'scope',
        'is_system'
    ];

    /**
     * A Role is assigned to many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function users()
    {
        return $this->morphedByMany(User::class, 'entity', 'assigned_roles');
    }

    /**
     * Local scope for admins
     *
     * @param unknown $query
     * @return unknown
     */
    public function scopeAdmins($query)
    {
        return $query->whereNotIn('roles.name', array_merge($this->getMemberRoles(), [self::SYSADMIN]));
    }

    /**
     * Local scope for members
     *
     * @param unknown $query
     * @return unknown
     */
    public function scopeMembers($query)
    {
        return $query->whereIn('roles.name', $this->getMemberRoles());
    }

    /**
     * Custom attribute for ability category of the role
     *
     * @return string
     */
    public function getAbilityCategoryAttribute()
    {
        $abilityCategoryRepository = resolve(AbilityCategoryContract::class);

        if (in_array($this->name, $this->getMemberRoles())) {
            return $abilityCategoryRepository->findBySlug($abilityCategoryRepository::MEMBER);
        }

        return $abilityCategoryRepository->findBySlug($abilityCategoryRepository::ADMIN);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(RoleI18n::class, 'role_id');
    }

    /**
     * Get the list of member roles
     *
     * @return array
     */
    protected function getMemberRoles()
    {
        return [
            self::MEMBER,
            self::MEMBER_SUSPENDED,
            self::MEMBER_TERMINATED,
            self::MEMBER_ON_HOLD
        ];
    }
}
