<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Rbac\Models\AbilitySection;
use Modules\Translation\Traits\Translatable;
use Silber\Bouncer\Database\Ability as BaseAbility;

class Ability extends BaseAbility
{
    use SoftDeletes, Translatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'title',
        'title_translation',
        'entity_id',
        'entity_type',
        'only_owned',
        'options',
        'scope',
        'permission_category_id',
        'permission_section_id',
        'is_active'
    ];

    /**
     * Translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name', 'title'];

    /**
     * The sections that belong to the ability.
     */
    public function sections()
    {
        return $this->belongsToMany(AbilitySection::class);
    }
}
