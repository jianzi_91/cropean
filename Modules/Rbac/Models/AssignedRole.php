<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\Model;

class AssignedRole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'role_id',
            'entity_id',
            'entity_type',
            'restricted_to_id',
            'restricted_to_type',
            'scope'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}
