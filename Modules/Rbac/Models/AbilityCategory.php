<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Translation\Traits\Translatable;

class AbilityCategory extends Model
{
    use Translatable;

    /**
     * Translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * The sections that belong to the category.
     */
    public function sections()
    {
        return $this->belongsToMany(AbilitySection::class);
    }
}
