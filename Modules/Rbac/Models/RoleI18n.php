<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Models\TranslatorLanguage;

class RoleI18n extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'role_id',
        'translator_language_id',
        'name',
    ];

    protected $table = 'role_i18ns';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(TranslatorLanguage::class, 'translator_language_id');
    }
}
