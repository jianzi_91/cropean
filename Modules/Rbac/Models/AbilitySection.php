<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Translation\Traits\Translatable;

class AbilitySection extends Model
{
    use Translatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'ability_category_id'];

    /**
     * Translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * The category the section belongs to.
     */
    public function category()
    {
        return $this->hasOne(AbilityCategory::class);
    }

    /**
     * The abilities that belong to the section.
     */
    public function abilities()
    {
        return $this->belongsToMany(Ability::class);
    }
}
