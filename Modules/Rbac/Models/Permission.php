<?php

namespace Modules\Rbac\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
            'ability_id',
            'entity_id',
            'entity_type',
            'forbidden',
            'scope',
    ];
}
