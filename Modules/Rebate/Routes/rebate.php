<?php

Route::group(['middleware' => 'web'], function () {
    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('roi/export', ['as' => 'member.report.roi.export', 'uses' => 'Member\RoiEquity\RoiExportController@index']);
        Route::resource('roi', 'Member\RoiEquity\RoiController', ['as' => 'member.report'])->only(['index']);
    });
    Route::group(['domain' => config('core.admin_url')], function () {
        #admin manage roi setting
        Route::get('roi/setting/edit', ['as' => 'admin.roi.setting.edit', 'uses' => 'Admin\Management\Roi\RoiController@edit']);
        Route::put('roi/setting/', ['as' => 'admin.roi.setting.update', 'uses' => 'Admin\Management\Roi\RoiController@update']);
        Route::group(['prefix' => 'reports'], function (){
            Route::get('roi/export', ['as' => 'admin.report.roi.export', 'uses' => 'Admin\RoiEquity\RoiExportController@index']);
            Route::resource('roi', 'Admin\RoiEquity\RoiController', ['as' => 'admin.report'])->only(['index']);
        });
    });
});
