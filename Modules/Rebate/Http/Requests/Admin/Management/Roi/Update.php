<?php

namespace Modules\Rebate\Http\Requests\Admin\Management\Roi;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Rebate\Contracts\RoiTierContract;
use Modules\Rebate\Http\Rules\RoiPayoutRatio;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $roiTiers = resolve(RoiTierContract::class)->getModel()->pluck('id')->toArray();
        $rules    = [];

        foreach ($roiTiers as $roiTierId) {
            $rules['payout_percentage_' . $roiTierId] = [
                'required',
                'numeric',
                'max:' . bcmul(setting('roi_tier_max_percentage_' . $roiTierId), 100, credit_precision()),
                'min:' . bcmul(setting('roi_tier_min_percentage_' . $roiTierId), 100, credit_precision()),
                'gt:0',
                'bail',
                new MaxDecimalPlaces(2),
            ];

            $rules['rollover_credit_' . $roiTierId] = [
                'required',
                'numeric',
                'gt:0',
                'bail',
                new MaxDecimalPlaces(2),
            ];
            $rules['cash_credit_' . $roiTierId] = [
                'required',
                'numeric',
                'gt:0',
                'bail',
                new MaxDecimalPlaces(2),
            ];

            if (!empty(request()->{'rollover_credit_' . $roiTierId}) && !empty(request()->{'cash_credit_' . $roiTierId})) {
                $ratio = bcdiv(request()->{'rollover_credit_' . $roiTierId}, 100, 4) + bcdiv(request()->{'cash_credit_' . $roiTierId}, 100, 4);

                if ($ratio > config('commission.roi.ratio') || $ratio < config('commission.roi.ratio')) {
                    $rules['cash_credit_' . $roiTierId][]     = new RoiPayoutRatio();
                    $rules['rollover_credit_' . $roiTierId][] = new RoiPayoutRatio();
                }
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'payout_percentage_1' => __('a_roi_setting.tier 1 payout percentage'),
            'payout_percentage_2' => __('a_roi_setting.tier 2 payout percentage'),
            'payout_percentage_3' => __('a_roi_setting.tier 3 payout percentage'),
            'payout_percentage_4' => __('a_roi_setting.tier 4 payout percentage'),
            // 'payout_percentage_.*' => __('a_roi_setting.payout percentage'),
            'rollover_credit_.*' => __('a_roi_setting.rollover credit'),
            'cash_credit_.*'     => __('a_roi_setting.cash credit'),
        ];
    }
}
