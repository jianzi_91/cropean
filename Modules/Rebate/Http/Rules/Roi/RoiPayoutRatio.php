<?php

namespace Modules\Rebate\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class RoiPayoutRatio implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.credit payout ratio and cash credit payout ratio add up must be 100 percent');
    }
}
