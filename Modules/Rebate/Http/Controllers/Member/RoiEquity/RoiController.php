<?php

namespace Modules\Rebate\Http\Controllers\Member\RoiEquity;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Rebate\Queries\Member\RoiEquityQuery;

class RoiController extends Controller
{
    protected $query;

    /**
     * v constructor.
     * @param
     */
    public function __construct(RoiEquityQuery $query)
    {
        $this->query = $query;
        $this->middleware('permission:member_commission_roi_equity_list')->only('index');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('rebate::member.roi_equity.index', compact('payouts'));
    }
}
