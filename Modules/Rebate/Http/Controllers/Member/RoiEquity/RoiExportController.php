<?php

namespace Modules\Rebate\Http\Controllers\Member\RoiEquity;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Rebate\Queries\Member\RoiEquityQuery;
use QueryBuilder\Concerns\CanExportTrait;

class RoiExportController extends Controller
{
    use CanExportTrait;

    /**
     * The leader bonus query
     *
     * @var unknown
     */
    protected $query;

    /**
     * Class constructor
     *
     * @param RoiEquityQuery $query
     */
    public function __construct(RoiEquityQuery $query)
    {
        $this->middleware('permission:member_commission_roi_equity_export')->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('m_report_roi.roi equity') . now() . '.xlsx', auth()->user(), __('m_report_roi.roi equity') . Carbon::now()->toDateString());
    }
}
