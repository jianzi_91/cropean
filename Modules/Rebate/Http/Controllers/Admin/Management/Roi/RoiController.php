<?php

namespace Modules\Rebate\Http\Controllers\Admin\Management\Roi;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Rebate\Contracts\RoiTierContract;
use Modules\Rebate\Contracts\RoiTierHistoryContract;
use Modules\Rebate\Http\Requests\Admin\Management\Roi\Update;
use Plus65\Utility\Exceptions\WebException;

class RoiController extends Controller
{
    protected $roiTierRepo;
    protected $roiTierHistoryRepo;

    public function __construct(
        RoiTierContract $roiTierRepo,
        RoiTierHistoryContract $roiTierHistoryRepo
    ) {
        $this->middleware('permission:admin_roi_tier_setting')->only('edit', 'update');

        $this->roiTierRepo        = $roiTierRepo;
        $this->roiTierHistoryRepo = $roiTierHistoryRepo;
    }

    public function edit()
    {
        $roiTiers = $this->roiTierRepo->all();
        return view('commission::admin.management.roi.edit', compact('roiTiers'));
    }

    public function update(Update $request)
    {
        $updatedBy = auth()->user()->id;
        $roiTiers  = $this->roiTierRepo->all();

        //save history first
        DB::beginTransaction();
        try {
            $this->roiTierHistoryRepo->getModel()
                ->where('is_current', 1)
                ->update(['is_current' => 0]);

            foreach ($roiTiers as $roiTier) {
                $data = [];

                $rolloverCredit = json_decode($roiTier->payout_credits)->roll_over_credit ?? 0.5;
                $cashCredit     = json_decode($roiTier->payout_credits)->cash_credit ?? 0.5;
                if ($request->has('payout_percentage_' . $roiTier->id)) {
                    $data['payout_percentage'] = bcdiv($request->{'payout_percentage_' . $roiTier->id}, 100, 10);
                }

                if ($request->has('rollover_credit_' . $roiTier->id)) {
                    $rolloverCredit = bcdiv($request->{'rollover_credit_' . $roiTier->id}, 100, 4);
                }

                if ($request->has('cash_credit_' . $roiTier->id)) {
                    $cashCredit = bcdiv($request->{'cash_credit_' . $roiTier->id}, 100, 4);
                }

                $data['payout_credits'] = json_encode([
                    BankCreditTypeContract::ROLL_OVER_CREDIT => $rolloverCredit,
                    BankCreditTypeContract::CASH_CREDIT      => $cashCredit,
                ]);

                if (!$this->roiTierRepo->edit($roiTier->id, $data)) {
                    DB::rollBack();
                    return back()->withErrors(__('a_roi_setting.failed to update roi tier :id', $roiTier->id));
                }

                $data['updated_by']  = $updatedBy;
                $data['is_current']  = 1;
                $data['roi_tier_id'] = $roiTier->id;
                $this->roiTierHistoryRepo->add($data);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw (new WebException($e))->redirectTo(route('admin.roi.setting.edit'))->withMessage(__('a_roi_setting.failed to update roi tier'));
        }

        return back()->with('success', __('a_roi_setting.roi tier updated successfully'));
    }
}
