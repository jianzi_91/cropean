<?php

namespace Modules\Rebate\Http\Controllers\Admin\RoiEquity;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Rebate\Queries\Admin\RoiEquityQuery;
use QueryBuilder\Concerns\CanExportTrait;

class RoiExportController extends Controller
{
    use CanExportTrait;

    /**
     * The leader bonus query
     *
     * @var unknown
     */
    protected $query;

    /**
     * Class constructor
     *
     * @param RoiEquityQuery $query
     */
    public function __construct(RoiEquityQuery $query)
    {
        $this->middleware('permission:admin_commission_roi_equity_export')->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('a_report_roi.roi equity') . now() . '.xlsx', auth()->user(), __('a_report_roi.roi equity') . Carbon::now()->toDateString());
    }
}
