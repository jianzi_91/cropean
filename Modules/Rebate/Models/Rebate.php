<?php

namespace Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class Rebate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'amount',
        'credit_type_id',
        'transaction_code',
        'created_at',
        'updated_at'
    ];

    /**
     * This model's relation to product purchase detail.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
