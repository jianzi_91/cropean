<?php

namespace Modules\Rebate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class RoiPayout extends Model
{
    use SoftDeletes, UserRelation;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'roi_payouts';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'payout_date',
        'user_id',
        'total_equity',
        'amount',
        'percentage',
        'computed_amount',
        'roi_tier_id'
    ];

    /**
     * Relation to credits.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany(RoiPayoutCredit::class, 'roi_payout_id');
    }

    /**
     * Relation to roi tier.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roiTier()
    {
        return $this->hasOne(RoiTier::class, 'roi_tier_id');
    }

    /**
     * Get percentage view
     *
     * @param  string  $value
     * @return string
     */
    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }
}
