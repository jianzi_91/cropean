<?php

namespace Modules\Rebate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Models\BankCreditType;

class RoiPayoutCredit extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'roi_payout_credits';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'roi_payout_id',
        'bank_credit_type_id',
        'percentage',
        'amount',
        'transaction_code',
        'exchange_rate',
        'issued_amount'
    ];

    /**
     * Relation to payout.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payout()
    {
        return $this->belongsTo(RoiPayout::class, 'roi_payout_id');
    }

    /**
     * Relation to credit type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * Relation to statement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statement()
    {
        return $this->belongsTo(BankAccountStatement::class, 'transaction_code', 'transaction_code');
    }
}
