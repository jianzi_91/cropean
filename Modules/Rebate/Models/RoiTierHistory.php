<?php

namespace Modules\Rebate\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class RoiTierHistory extends Model
{
    protected $fillable = [
        'roi_tier_id',
        'payout_percentage',
        'payout_credits',
        'is_current',
        'updated_by'
    ];

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function roiTier()
    {
        return $this->belongsTo(RoiTier::class, 'roi_tier_id');
    }
}
