<?php

namespace Modules\Rebate\Models;

use Illuminate\Database\Eloquent\Model;

class RoiTier extends Model
{
    protected $fillable = [
        'equity_min',
        'equity_max',
        'payout_percentage',
        'payout_credits'
    ];

    public function history()
    {
        return $this->hasMany(RoiTierHistory::class, 'roi_tier_id');
    }
}
