<?php

namespace Modules\Rebate\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Rebate\Models\RoiPayoutCredit;
use QueryBuilder\QueryBuilder;

class RoiEquityQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $rollOverCreditId = get_bank_credit_type(BankCreditTypeContract::ROLL_OVER_CREDIT)->id;
        $cashCreditId     = get_bank_credit_type(BankCreditTypeContract::CASH_CREDIT)->id;

        $query = RoiPayoutCredit::join('roi_payouts', 'roi_payouts.id', 'roi_payout_credits.roi_payout_id')
            ->join('users', 'users.id', 'roi_payouts.user_id')
            ->selectRaw("roi_payouts.*, users.member_id, users.email, users.name,
            sum(if(roi_payout_credits.bank_credit_type_id = {$rollOverCreditId}, roi_payout_credits.issued_amount, 0)) as rollover_payout,
            sum(if(roi_payout_credits.bank_credit_type_id = {$cashCreditId}, roi_payout_credits.issued_amount, 0)) as cash_payout")
            ->groupBy('roi_payouts.id')
            ->orderBy('roi_payouts.created_at', 'desc');

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
