<?php

namespace Modules\Rebate\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Rebate\Queries\RoiEquityQuery as BaseQuery;

class RoiEquityQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'roi_payouts',
            'column'    => 'payout_date',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', auth()->user()->id);
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            ' ' . amount_format($payout->total_equity ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($payout->rollover_payout ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($payout->cash_payout ?? 0, credit_precision()) . ' ',
        ];
    }

    public function headings(): array
    {
        return [
            __('m_report_roi.date'),
            __('m_report_roi.equity amount'),
            __('m_report_roi.rollover credit payout'),
            __('m_report_roi.cash credit payout'),
        ];
    }
}
