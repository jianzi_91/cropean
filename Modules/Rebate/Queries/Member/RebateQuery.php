<?php

namespace Modules\Rebate\Queries\Member;

use Modules\Rebate\Queries\RebateQuery as BaseQuery;

class RebateQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'rebates',
            'column' => 'created_at'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', auth()->user()->id);
    }
}
