<?php

namespace Modules\Rebate\Queries\Admin;

use Modules\Rebate\Queries\RebateQuery as BaseQuery;

class RebateQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'rebates',
            'column' => 'created_at'
        ],
        'username' => [
            'filter' => 'like',
            'table'  => 'users',
            'column' => 'username',
        ]
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }
}
