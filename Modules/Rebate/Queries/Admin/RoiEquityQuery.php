<?php

namespace Modules\Rebate\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Rebate\Queries\RoiEquityQuery as BaseQuery;

class RoiEquityQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'roi_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->name,
            $payout->member_id,
            $payout->email,
            ' ' . amount_format($payout->total_equity ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($payout->rollover_payout ?? 0, credit_precision()) . ' ',
            ' ' . amount_format($payout->cash_payout ?? 0, credit_precision()) . ' ',
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_roi.date'),
            __('a_report_roi.name'),
            __('a_report_roi.member id'),
            __('a_report_roi.email'),
            __('a_report_roi.equity amount'),
            __('a_report_roi.rollover credit payout'),
            __('a_report_roi.cash credit payout'),
        ];
    }
}
