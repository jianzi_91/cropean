<?php

namespace Modules\Rebate\Queries;

use QueryBuilder\QueryBuilder;
use Modules\Product\Models\Rebate;

class RebateQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = Rebate::select('rebates.*')
            ->join('users', 'users.id', 'rebates.user_id')
            ->with('productPurchaseDetail.product')
            ->orderBy('rebates.created_at', 'desc');

        return $query;
    }
}
