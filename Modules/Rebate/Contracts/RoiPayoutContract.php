<?php

namespace Modules\Rebate\Contracts;

use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface RoiPayoutContract extends CrudContract, BulkActionContract
{
}
