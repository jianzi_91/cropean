<?php

namespace Modules\Rebate\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditSnapshotContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Rebate\Contracts\RoiPayoutContract;
use Modules\Rebate\Contracts\RoiTierContract;
use Modules\Rebate\Repositories\RoiRepository;
use Modules\Tree\Contracts\SponsorTreeContract;
use Plus65\Utility\Contracts\SnapshotContract;

class RoiPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     *
     * @var string
     */
    protected $signature = 'rebate:payout {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Roi commission payout';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param RoiPayoutContract $payoutRepo
     * @param BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo
     * @param ExchangeRateContract $exchangeRateRepo
     * @param RoiTierContract $roiTierRepo
     * @param BankCreditTypeContract $bankCreditTypeRepo
     * @return mixed
     * @throws \Exception
     */
    public function handle(
        RoiPayoutContract $payoutRepo,
        BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo,
        ExchangeRateContract $exchangeRateRepo,
        RoiTierContract $roiTierRepo,
        BankCreditTypeContract $bankCreditTypeRepo
    ) {
        $payoutDate = new Carbon($this->argument('date'));

        $dateFrom = $payoutDate->copy()->startOfDay()->toDateTimeString();
        $dateTo   = $payoutDate->copy()->endOfDay()->toDateTimeString();

        if ($payoutRepo->getModel()->whereBetween('created_at', [$dateFrom, $dateTo])->exists()) {
            $this->error("Roi payout already processed for {$payoutDate->toDateString()}");
            return;
        }

        $startTime = $this->start();
        $this->line("Roi started at " . $startTime->toDateTimeString());

        $this->line("[Step 1] Get previous day before capital and roll over credit");

        $snapshotDate = $payoutDate->copy()->subDay()->toDateString();
        $creditTypes  = $bankCreditTypeRepo->getModel()->whereIn('name', [
            $bankCreditTypeRepo::CAPITAL_CREDIT,
            $bankCreditTypeRepo::ROLL_OVER_CREDIT,
            $bankCreditTypeRepo::PROMOTION_CREDIT
        ])->pluck('name', 'id')->toArray();

        $equities = $bankAccountCreditSnapshotRepo->getTotalCredits($snapshotDate, $creditTypes);

        $this->line("[Step 2] Get roi tier and exchange rate");
        $roiTiers      = $roiTierRepo->getModel()::orderBy('id', 'desc')->get();
        $exchangeRates = $exchangeRateRepo->getExchangeRates();

        DB::beginTransaction();
        try {
            $roiRepo = new RoiRepository($equities, $roiTiers);
            $this->line("[Step 3] Set payout date");
            $roiRepo->setPayoutDate($payoutDate);

            $this->line("[Step 4] Set non eligible users");
            $roiRepo->setNonEligibleUsers();

            $this->line("[Step 5] Set min equity");
            $roiRepo->setMinTotalEquity();

            $this->line("[Step 6] Set exchange rate");
            $roiRepo->setExchangeRate($exchangeRates);

            $this->line("[Step 7] Calculation");
            $roiRepo->calculate(credit_precision());

            $this->line("[Step 8] Payout");
            $roiRepo->payoutCommission();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
