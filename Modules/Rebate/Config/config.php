<?php

return [
    'name'     => 'Rebate',
    'bindings' => [
        \Modules\Rebate\Contracts\RebateContract::class        => \Modules\Rebate\Repositories\RebateRepository::class,
        Modules\Rebate\Contracts\RoiPayoutContract::class      => Modules\Rebate\Repositories\RoiPayoutRepository::class,
        Modules\Rebate\Contracts\RoiTierContract::class        => Modules\Rebate\Repositories\RoiTierRepository::class,
        Modules\Rebate\Contracts\RoiTierHistoryContract::class => Modules\Rebate\Repositories\RoiTierHistoryRepository::class,

    ],
];
