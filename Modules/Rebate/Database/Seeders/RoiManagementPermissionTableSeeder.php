<?php

namespace Modules\Rebate\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class RoiManagementPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_roi_setting' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Roi Setting',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_roi_tier_setting',
                        'title' => 'Admin Roi Tier Setting',
                    ],
                ]
            ],
            'admin_roi_equity_report' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Roi Equity Report',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_commission_roi_equity_list',
                        'title' => 'Admin Commission Roi Equity List',
                    ],
                    [
                        'name'  => 'admin_commission_roi_equity_export',
                        'title' => 'Admin Commission Roi Equity Export',
                    ],
                ]
            ],
            'member_roi_equity_report' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Roi Equity Report',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_commission_roi_equity_list',
                        'title' => 'Member Commission Roi Equity List',
                    ],
                    [
                        'name'  => 'member_commission_roi_equity_export',
                        'title' => 'Member Commission Roi Equity Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_roi_tier_setting',
                'admin_commission_roi_equity_list',
                'admin_commission_roi_equity_export',
            ],
            Role::ADMIN => [
                'admin_roi_tier_setting',
                'admin_commission_roi_equity_list',
                'admin_commission_roi_equity_export',
            ],
            Role::MEMBER => [
                'member_commission_roi_equity_list',
                'member_commission_roi_equity_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
