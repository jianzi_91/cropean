<?php

namespace Modules\Rebate\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class RebatePermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_rebate' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Rebate',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_rebate_list',
                        'title' => 'Admin Rebate List',
                    ],
                    [
                        'name'  => 'admin_rebate_export',
                        'title' => 'Admin Rebate Export',
                    ],
                ]
            ],
            'member_rebate' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'rebate',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_rebate_list',
                        'title' => 'Member Rebate List',
                    ],
                    [
                        'name'  => 'member_rebate_export',
                        'title' => 'Member Rebate Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_rebate_list',
                'admin_rebate_export'
            ],
            Role::ADMIN => [
                'admin_rebate_list',
                'admin_rebate_export'
            ],
            Role::MEMBER => [
                'member_rebate_list',
                'member_rebate_export'
            ],
            Role::MEMBER_SUSPENDED => [
                'member_rebate_list',
                'member_rebate_export'
            ],
            Role::MEMBER_TERMINATED => [
            ],
            Role::MEMBER_ON_HOLD => [
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
