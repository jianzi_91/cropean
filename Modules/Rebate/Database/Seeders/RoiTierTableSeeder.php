<?php

namespace Modules\Rebate\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rebate\Models\RoiTier;

class RoiTierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $ranks = [
            [
                'equity_min'        => 500,
                'equity_max'          => 2999,
                'payout_percentage' => 0.0015,
                'payout_credits'    => '{"roll_over_credit":0.5, "cash_credit":0.5}'
            ],
            [
                'equity_min'        => 3000,
                'equity_max'          => 9999,
                'payout_percentage' => 0.002,
                'payout_credits'    => '{"roll_over_credit":0.5, "cash_credit":0.5}'
            ],
            [
                'equity_min'        => 10000,
                'equity_max'          => 49999,
                'payout_percentage' => 0.0025,
                'payout_credits'    => '{"roll_over_credit":0.5, "cash_credit":0.5}'
            ],
            [
                'equity_min'        => 50000,
                'equity_max'          => 50000,
                'payout_percentage' => 0.003,
                'payout_credits'    => '{"roll_over_credit":0.5, "cash_credit":0.5}'
            ],
        ];

        foreach ($ranks as $rank) {
            (new RoiTier($rank))->save();
        }

        DB::commit();
    }
}
