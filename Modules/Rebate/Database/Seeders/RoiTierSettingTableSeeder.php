<?php

namespace Modules\Rebate\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class RoiTierSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'roi_tier_setting',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $setting_cat = SettingCategory::where('name', 'roi_tier_setting')->first()->id;

        $settings = [
            [
                'name'                => 'roi_tier_max_percentage_1',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_max_percentage_2',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_max_percentage_3',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_max_percentage_4',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_min_percentage_1',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_min_percentage_2',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_min_percentage_3',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_tier_min_percentage_4',
                'setting_category_id' => $setting_cat,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'roi_tier_max_percentage_1')->first()->id,
                'value'      => '0.0019',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_max_percentage_2')->first()->id,
                'value'      => '0.0024',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_max_percentage_3')->first()->id,
                'value'      => '0.0029',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_max_percentage_4')->first()->id,
                'value'      => '0.0035',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_min_percentage_1')->first()->id,
                'value'      => '0.0001',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_min_percentage_2')->first()->id,
                'value'      => '0.0020',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_min_percentage_3')->first()->id,
                'value'      => '0.0025',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_tier_min_percentage_4')->first()->id,
                'value'      => '0.0030',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
