<?php

namespace Modules\Rebate\Repositories;

use Modules\Rebate\Contracts\RoiTierHistoryContract;
use Modules\Rebate\Models\RoiTierHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class RoiTierHistoryRepository extends Repository implements RoiTierHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(RoiTierHistory $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}