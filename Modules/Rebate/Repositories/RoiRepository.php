<?php

namespace Modules\Rebate\Repositories;

use Carbon\Carbon;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Rebate\Models\RoiPayout;
use Modules\Rebate\Models\RoiPayoutCredit;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class RoiRepository extends Repository
{
    use CanPayoutCommission;

    protected $creditTransactionType        = BankTransactionTypeContract::REBATE;
    protected $bonus_payout_id              = 'roi_payout_id';
    protected $data                         = [];
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $payouts                      = [];
    protected $payoutDate;
    protected $amounts;
    protected $roiTiers;
    protected $convertTotalEquityExchangeRates;
    protected $exchangeRates;
    protected $commissions = [];
    protected $roiPayouts;
    protected $minEquity;

    /**
     * Class constructor.
     *
     * @param array $amounts
     * @param $roiTiers
     * @param $exchangeRate
     */
    public function __construct(array $amounts = [], $roiTiers)
    {
        parent::__construct();
        $this->amounts  = $amounts;
        $this->roiTiers = $roiTiers;
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setMinTotalEquity()
    {
        $this->minEquity = $this->roiTiers->min('equity_min');
    }

    public function setExchangeRate($exchangeRates)
    {
        $capitalCredit  = BankCreditTypeContract::CAPITAL_CREDIT;
        $rolloverCredit = BankCreditTypeContract::ROLL_OVER_CREDIT;
        $cashCredit     = BankCreditTypeContract::CASH_CREDIT;

        $this->convertTotalEquityExchangeRates = [
            $capitalCredit  => $exchangeRates[$capitalCredit]['sell_rate'] ?? 1,
            $rolloverCredit => $exchangeRates[$rolloverCredit]['sell_rate'] ?? 1
        ];

        $this->exchangeRates = [
            $rolloverCredit => $exchangeRates[$rolloverCredit]['buy_rate'] ?? 1,
            $cashCredit     => $exchangeRates[$cashCredit]['buy_rate'] ?? 1,
        ];
    }

    public function calculate($precision = 2)
    {
        $this->commissions = [];
        $this->roiPayouts  = [];

        if ($this->amounts) {
            $dateTime = Carbon::parse($this->payoutDate)->toDateTimeString();
            foreach ($this->amounts as $userId => $userBalances) {
                if (!in_array($userId, $this->nonEligibleReceiveUsers)) {
                    $capitalCredit   = $userBalances['capital_credit'] ?? 0;
                    $rollOverCredit  = $userBalances['roll_over_credit'] ?? 0;
                    $promotionCredit = $userBalances['promotion_credit'] ?? 0;
                    $totalEquity     = bcadd(get_total_equity($this->convertTotalEquityExchangeRates, $capitalCredit, $rollOverCredit), $promotionCredit, $precision);

                    #check is equity min
                    if ($totalEquity >= $this->minEquity) {
                        $roiTier = $this->getRoiTier($totalEquity);

                        if ($roiTier) {
                            $percentage = $roiTier->payout_percentage;
                            $amount     = bcmul($totalEquity, $percentage, $precision);

                            $this->roiPayouts[$userId] = [
                                'user_id'         => $userId,
                                'total_equity'    => $totalEquity,
                                'percentage'      => $percentage,
                                'amount'          => $amount,
                                'computed_amount' => $amount,
                                'created_at'      => $dateTime,
                                'updated_at'      => $dateTime,
                                'payout_date'     => $this->payoutDate,
                                'roi_tier_id'     => $roiTier->id
                            ];
                        } #end if roi tier
                    } #end check equity
                } #end if check eligible receive or not
            } #enf foreach
        } #end if check amount

        $this->insertRoiPayout();
    }

    public function payoutCommission()
    {
        $roiTiers = $this->roiTiers->pluck('payout_credits', 'id')->toArray();
        if ($this->commissions) {
            foreach ($this->commissions as $tier => $commission) {
                $this->payouts = [];
                if (isset($roiTiers[$tier])) {
                    $this->payouts = $commission;
                    $this->initPayoutCommission(json_decode($roiTiers[$tier], true), $this->exchangeRates);
                    if (!$this->payouts->isEmpty()) {
                        foreach ($this->payouts as $payout) {
                            $calculatedAmount = $payout->amount;
                            $payoutCredits    = $this->calculateAllCommCredits($payout->user_id, $calculatedAmount, $this->creditTransactionTypeId, $this->payoutDate);

                            foreach ($payoutCredits as $bankAccountStatement) {
                                $this->data[] = [
                                    'created_at'           => Carbon::parse($this->payoutDate),
                                    'updated_at'           => Carbon::parse($this->payoutDate),
                                    $this->bonus_payout_id => $payout->id,
                                    'transaction_code'     => $bankAccountStatement['transaction_code'],
                                    'percentage'           => $bankAccountStatement['percentage'],
                                    'amount'               => $bankAccountStatement['amount'],
                                    'issued_amount'        => $bankAccountStatement['issued_amount'],
                                    'bank_credit_type_id'  => $bankAccountStatement['bank_credit_type_id'],
                                    'exchange_rate'        => $bankAccountStatement['exchange_rate'],
                                ];
                            } #end foreach
                        } #end foreach commissions
                    } #end if commissions
                }
            }

            #payout in bank account statement
            $this->saveCommData();

            #payout credit
            $this->saveCommissionCredits();
        }
    }

    private function insertRoiPayout()
    {
        foreach (array_chunk($this->roiPayouts, 1000) as $record) {
            RoiPayout::insert($record);
        }

        $this->commissions = RoiPayout::whereBetween('payout_date', [$this->payoutDate->copy()->startOfDay(), $this->payoutDate->copy()->endOfDay()])
            ->get()
            ->mapToGroups(function ($item, $key) {
                return [$item->roi_tier_id => $item];
            });
    }

    private function getRoiTier($amount)
    {
        foreach ($this->roiTiers as $tier) {
            if ($amount >= $tier->equity_max) {
                return $tier;
            }
            if ($amount >= $tier->equity_min && $amount <= $tier->equity_max) {
                return $tier;
            }
        }
        return;
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            RoiPayoutCredit::insert($record);
        }
    }
}
