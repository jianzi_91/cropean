<?php

namespace Modules\Rebate\Repositories;

use Modules\Rebate\Contracts\RoiPayoutContract;
use Modules\Rebate\Models\RoiPayout;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class RoiPayoutRepository extends Repository implements RoiPayoutContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param RoiPayout $model
     */
    public function __construct(RoiPayout $model)
    {
        parent::__construct($model);
    }
}
