<?php

namespace Modules\Rebate\Repositories;

use Modules\Rebate\Contracts\RoiTierContract;
use Modules\Rebate\Models\RoiTier;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class RoiTierRepository extends Repository implements RoiTierContract
{
    use HasCrud;

    protected $model;

    public function __construct(RoiTier $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}