@extends('templates.member.master')
@section('title', __('m_page_title.reports'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.my equity')]
    ],
    'header'=>__('m_report_roi.my equity')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-4">
        {{ Form::formDateRange('payout_date', request('payout_date'), __('m_report_roi.payout date'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_roi.my equity report') }}</h4>
            @can('member_commission_roi_equity_export')
                {{ Form::formTabSecondary(__('m_report_roi.export table'), route('member.report.roi.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_roi.date') }}</th>
                <th>{{ __('m_report_roi.equity amount') }}</th>
                <th>{{ __('m_report_roi.rollover credit payout') }}</th>
                <th>{{ __('m_report_roi.cash credit payout') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->payout_date }}</td>
                <td>{{ amount_format($payout->total_equity, credit_precision()) }}</td>
                <td>{{ amount_format($payout->rollover_payout, credit_precision()) }}</td>
                <td>{{ amount_format($payout->cash_payout, credit_precision()) }}</td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', ['span' => 4, 'text' => __('m_report_roi.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection