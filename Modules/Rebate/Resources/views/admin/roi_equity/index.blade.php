@extends('templates.admin.master')
@section('title', __('a_page_title.reports'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.equity')]
    ],
    'header'=>__('a_report_roi.equity report')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-3">
        {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_roi.payout date'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('email', request('email'), __('a_report_roi.email'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('member_id', request('member_id'), __('a_report_roi.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3 mt-2">
        {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_roi.include downlines'), ['isChecked' => request('member_id_downlines', false)]) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('name', request('name'), __('a_report_roi.name'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_roi.equity report') }}</h4>
            <div class="d-flex flex-row align-items-center">
            <div class="table-total-results mr-2">{{ __('a_report_roi.total results:') }} {{ $payouts->total() }}</div>
            @can('admin_commission_roi_equity_export')
                {{ Form::formTabSecondary(__('a_report_roi.export table'), route('admin.report.roi.export', array_merge(request()->except('page'))))}}
            @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_roi.date') }}</th>
                <th>{{ __('a_report_roi.name') }}</th>
                <th>{{ __('a_report_roi.member id') }}</th>
                <th>{{ __('a_report_roi.email') }}</th>
                <th>{{ __('a_report_roi.equity amount') }}</th>
                <th>{{ __('a_report_roi.rollover credit payout') }}</th>
                <th>{{ __('a_report_roi.cash credit payout') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
                <tr>
                    <td>{{ $payout->payout_date }}</td>
                    <td>{{ $payout->name }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->email }}</td>
                    <td>{{ amount_format($payout->total_equity, credit_precision()) }}</td>
                    <td>{{ amount_format($payout->rollover_payout, credit_precision()) }}</td>
                    <td>{{ amount_format($payout->cash_payout, credit_precision()) }}</td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 6, 'text' => __('a_report_roi.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection