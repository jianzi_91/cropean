<?php

namespace Modules\SupportTicket\Console;

use Illuminate\Console\Command;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Contracts\SupportTicketTypeContract;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SupportInitiliaze extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'support:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize Support Tickets.';

    /**
     * The status repository
     * @var unknown
     */
    protected $status;

    /**
     * The type repository
     * @var unknown
     */
    protected $type;

    /**
     * The translation repository
     * @var unknown
     */
    protected $translation;

    /**
     * Class constructor
     * @param SupportTicketStatusContract $status
     * @param SupportTicketTypeContract $type
     */
    public function __construct(SupportTicketStatusContract $status, SupportTicketTypeContract $type)
    {
        $this->status = $status;
        $this->type   = $type;
        //$this->translation = $trans;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses = config('supportticket.statuses');
        if ($statuses) {
            foreach ($statuses as $slug => $status) {
                $row = $this->status->add(['name' => $slug, 'is_active' => 1]);
                //$this->translation->updateDefaultByCode($row->name_translation, $status);
            }
        }

        $types = config('supportticket.types');
        if ($types) {
            foreach ($types as $slug => $type) {
                $row = $this->type->add(['name' => $type, 'is_active' => 1]);
                //$this->translation->updateDefaultByCode($row->name_translation, $type);
            }
        }

        $this->info('Stickets initalized...');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
