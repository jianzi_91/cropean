<?php

namespace Modules\SupportTicket\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class SupportTicketPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_support_ticket' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Support Ticket',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_support_ticket_list',
                        'title' => 'Admin Support Ticket List',
                    ],
                    [
                        'name'  => 'admin_support_ticket_create',
                        'title' => 'Admin Create Support Ticket',
                    ],
                    [
                        'name'  => 'admin_support_ticket_edit',
                        'title' => 'Admin Edit Support Ticket',
                    ],
                    [
                        'name'  => 'admin_support_ticket_delete',
                        'title' => 'Admin Delete Support Ticket',
                    ]
                ]
            ],
            'member_support_ticket' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Support Ticket',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_support_ticket_list',
                        'title' => 'Member Support Ticket List',
                    ],
                    [
                        'name'  => 'member_support_ticket_create',
                        'title' => 'Member Create Support Ticket',
                    ],
                    [
                        'name'  => 'member_support_ticket_edit',
                        'title' => 'Member Edit Support Ticket',
                    ],
                    [
                        'name'  => 'member_support_ticket_delete',
                        'title' => 'Member Delete Support Ticket',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_support_ticket_list',
                'admin_support_ticket_create',
                'admin_support_ticket_edit',
                'admin_support_ticket_delete',
            ],
            Role::ADMIN => [
                'admin_support_ticket_list',
                'admin_support_ticket_create',
                'admin_support_ticket_edit',
                'admin_support_ticket_delete',
            ],
            Role::MEMBER => [
                'member_support_ticket_list',
                'member_support_ticket_create',
                'member_support_ticket_edit',
                'member_support_ticket_delete',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
