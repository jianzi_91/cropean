<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SupportTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('subject');
            $table->string('ticket_number')->unique();
            $table->unsignedInteger('support_ticket_type_id')->index();
            $table->unsignedInteger('support_ticket_status_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('updated_by')->index()->nullable();
            $table->text('remarks')->nullable();

            //constraints
            $table->foreign('support_ticket_type_id')->references('id')->on('support_ticket_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('support_ticket_status_id')->references('id')->on('support_ticket_statuses');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
