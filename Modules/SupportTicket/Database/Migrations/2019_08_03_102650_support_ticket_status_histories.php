<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SupportTicketStatusHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_ticket_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('support_ticket_id')->index();
            $table->unsignedInteger('support_ticket_status_id')->index();
            $table->unsignedInteger('updated_by')->index()->nullable();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);

            //constraints
            $table->foreign('support_ticket_id')->references('id')->on('support_tickets');
            $table->foreign('support_ticket_status_id')->references('id')->on('support_ticket_statuses');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_ticket_status_histories');
    }
}
