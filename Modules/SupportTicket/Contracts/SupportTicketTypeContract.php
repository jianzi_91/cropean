<?php

namespace Modules\SupportTicket\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface SupportTicketTypeContract extends CrudContract, SlugContract, ActiveContract
{
}
