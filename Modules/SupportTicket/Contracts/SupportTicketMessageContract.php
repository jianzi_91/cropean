<?php

namespace Modules\SupportTicket\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface SupportTicketMessageContract extends CrudContract
{
    /**
     * Find by ticket num
     * @param unknown $ticketNum
     * @param $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function findByTicketNum($ticketNum, array $with = [], $select = ['*']);

    /**
     * Find by ticket id
     * @param unknown $id
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function findByTicketId($id, array $with = [], $select = ['*']);

    /**
     * Delete by ticket num
     * @param unknown $ticketNum
     * @return integer
     */
    public function deleteByTicketNum($ticketNum);

    /**
     * Delete by ticket id
     * @param unknown $id
     * @return integer
     */
    public function deleteByTicketId($id);
}
