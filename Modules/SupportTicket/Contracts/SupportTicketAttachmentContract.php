<?php

namespace Modules\SupportTicket\Contracts;

use Illuminate\Http\UploadedFile;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface SupportTicketAttachmentContract extends CrudContract
{
    /**
     * Attach file
     * @param unknown $messageId
     * @param unknown $filename
     * @param unknown $path
     * @param string $move
     * @return boolean
     */
    public function attach($messageId, $filename, $path, $move = false);

    /**
     * Attach file using byte contents
     * @param unknown $messageId
     * @param unknown $filename
     * @param unknown $mime
     * @param unknown $contents
     * @return boolean
     */
    public function attachContents($messageId, $filename, $mime, $contents);

    /**
     * Attache file using UploadedFile
     * @param unknown $messageId
     * @param UploadedFile $file
     * @param string $move
     * @return boolean
     */
    public function attachUploadedFile($messageId, UploadedFile $file, $move = false);

    /**
     * Get attachment as byte contents
     * @param unknown $attachmentId
     * @param string $download
     * @return string bytes
     */
    public function get($attachmentId, $download = false);

    /**
     * Get attachments
     * @param unknown $messageId
     * @param array $with
     * @param array $select
     * @return \Illuminate\Support\Collection
     */
    public function getAttachments($messageId, array $with = [], $select = ['*']);

    /**
     * Delete attachment
     * @param unknown $attachmentId
     * @return integer
     */
    public function delete($attachmentId);

    /**
     * Delete message attachments
     * @param unknown $messageId
     * @return integer
     */
    public function deleteMessageAttachments($messageId);

    /**
     * Delete all attachments
     * @return integer
     */
    public function deleteAll();
}
