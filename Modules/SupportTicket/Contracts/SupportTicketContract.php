<?php

namespace Modules\SupportTicket\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;
use Plus65\Base\Repositories\Contracts\SoftDeleteable;

interface SupportTicketContract extends CrudContract, SlugContract, SoftDeleteable
{
}
