<?php

namespace Modules\SupportTicket\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface SupportTicketStatusContract extends CrudContract, SlugContract, HistoryableContract, ActiveContract
{
    const OPEN  = 'open';
    const CLOSE = 'close';
}
