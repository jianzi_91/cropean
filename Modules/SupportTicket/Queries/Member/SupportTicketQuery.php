<?php

namespace Modules\SupportTicket\Queries\Member;

use Illuminate\Support\Facades\Auth;
use Modules\SupportTicket\Queries\SupportTicketQuery as BaseQuery;

class SupportTicketQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'ticket_number' => [
            'filter' => 'text',
            'table'  => 'support_tickets',
        ],
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'support_tickets',
           ],
        'support_ticket_type_id' => [
            'filter' => 'select',
            'table'  => 'support_tickets',
        ],
        'support_ticket_status_id' => [
            'filter' => 'select',
            'table'  => 'support_ticket_status_histories',
        ],
    ];

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function beforeBuild()
    {
        $this->builder->where('support_tickets.user_id', Auth::id());
    }
}
