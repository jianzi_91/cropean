<?php

namespace Modules\SupportTicket\Queries\Admin;

use Modules\SupportTicket\Queries\SupportTicketQuery as BaseQuery;

class SupportTicketQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'ticket_number' => [
            'filter' => 'text',
            'table'  => 'support_tickets',
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
        ],
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'support_tickets',
           ],
        'support_ticket_type_id' => [
            'filter' => 'select',
            'table'  => 'support_tickets',
        ],
        'support_ticket_status_id' => [
            'filter' => 'select',
            'table'  => 'support_ticket_status_histories',
        ],
    ];
}
