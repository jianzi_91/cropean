<?php

namespace Modules\SupportTicket\Queries;

use QueryBuilder\QueryBuilder;
use Modules\SupportTicket\Models\SupportTicket;

class SupportTicketQuery extends QueryBuilder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return SupportTicket::with([
                        'user',
                        'status.status',
                        'type',
                        'messages',
                        'messages.attachments'
                    ])
                    ->join('users', 'users.id', '=', 'support_tickets.user_id')
                    ->join('support_ticket_status_histories', function ($join) {
                        $join->on('support_ticket_status_histories.support_ticket_id', '=', 'support_tickets.id');
                        $join->where('support_ticket_status_histories.is_current', 1);
                        $join->whereNull('support_ticket_status_histories.deleted_at');
                    })
                    ->select('support_tickets.*')
                    ->orderBy('support_tickets.created_at', 'desc');
    }
}
