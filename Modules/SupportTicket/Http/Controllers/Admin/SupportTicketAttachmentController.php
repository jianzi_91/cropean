<?php

namespace Modules\SupportTicket\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;
use Modules\SupportTicket\Contracts\SupportTicketAttachmentContract;

class SupportTicketAttachmentController extends Controller
{
    /**
     * The announcement attachment repository
     *
     * @var unknown
     */
    protected $attachmentRepository;

    /**
     * Class constructor
     *
     * @param AnnouncementAttachmentContract $attachment
     */
    public function __construct(SupportTicketAttachmentContract $attachmentContract)
    {
        $this->middleware('permission:admin_support_attachment_show')->only('show');

        $this->attachmentRepository = $attachmentContract;
    }

    /**
     * Show the specified resource.
     *
     * @param unknown $id
     * @return Response
     */
    public function show($id)
    {
        $attachment = $this->attachmentRepository->find($id);

        if (!$attachment) {
            return;
        }

        return $this->attachmentRepository->get($id, false);
    }
}
