<?php

namespace Modules\SupportTicket\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Core\Generators\SimpleGenerator;
use Modules\SupportTicket\Contracts\SupportTicketAttachmentContract;
use Modules\SupportTicket\Contracts\SupportTicketContract;
use Modules\SupportTicket\Contracts\SupportTicketMessageContract;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Http\Requests\Member\Destroy;
use Modules\SupportTicket\Http\Requests\Member\Show;
use Modules\SupportTicket\Http\Requests\Member\Store;
use Modules\SupportTicket\Http\Requests\Member\Update;
use Modules\SupportTicket\Queries\Member\SupportTicketQuery;
use Plus65\Utility\Exceptions\WebException;

class SupportTicketController extends Controller
{
    /**
     * The support repository
     * @var unknown
     */
    protected $support;

    /**
     * The support ticket status repository
     * @var unknown
     */
    protected $status;

    /**
     * Support ticket message repository
     * @var unknown
     */
    protected $message;

    /**
     * The query object
     * @var unknown
     */
    protected $query;

    /**
    * Support ticket attachment repository
    * @var unknown
    */
    protected $attachment;

    /**
     * Class constructor
     * @param SupportTicketContract $support
     */
    public function __construct(
        SupportTicketContract $support,
        SupportTicketStatusContract $status,
        SupportTicketMessageContract $message,
        SupportTicketQuery $query,
        SupportTicketAttachmentContract $attachment
    ) {
        $this->middleware('permission:member_support_ticket_list')->only('index', 'show');
        $this->middleware('permission:member_support_ticket_create')->only('create', 'store');
        $this->middleware('permission:member_support_ticket_edit')->only('edit', 'update');
        $this->middleware('permission:member_support_ticket_delete')->only('destroy');

        $this->support    = $support;
        $this->status     = $status;
        $this->message    = $message;
        $this->query      = $query;
        $this->attachment = $attachment;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $tickets = $this->query
                        ->setParameters($request->all())
                        ->paginate();
        return view('supportticket::member.index')->with(compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('supportticket::member.create');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Show $request, $id)
    {
        $ticket = $this->support->find($id, [
                'user',
                'messages' => function ($query) {
                    $query->orderBy('created_at', 'asc');
                },
                'messages.user',
                'messages.attachments',
                'type',
                'status.status'
        ]);

        return view('supportticket::member.show')->with(compact('ticket'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();
        try {
            $details                  = $request->all();
            $details['ticket_number'] = (new SimpleGenerator())->generate('SCT');
            $details['user_id']       = $request->user()->id;
            if ($support = $this->support->add($details)) {
                $message = [
                        'message'           => $request->message,
                        'user_id'           => $request->user()->id,
                        'support_ticket_id' => $support->id
                ];
                if ($message = $this->message->add($message)) {
                    $attachments = $request->file('attachments');
                    if ($attachments) {
                        foreach ($attachments as $attachment) {
                            $this->attachment->attachUploadedFile($message->id, $attachment);
                        }
                    }
                }
            } else {
                throw new \Exception('ticket not created');
            }

            DB::commit();
        } catch (\Exception $e) {
            throw $e;
        }

        return redirect(route('member.support.tickets.index'))
                    ->with('success', __('m_create_support_ticket.ticket created successfully'));
    }

    /**
     * Update the specified resource in storage.
     * @param Update $request
     * @param unknown $id
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            if ($message = $this->message->add([
                    'support_ticket_id' => $id,
                    'message' => $request->message,
                    'user_id' => $request->user()->id
            ])) {
                $attachments = $request->file('attachments');
                if ($attachments) {
                    foreach ($attachments as $attachment) {
                        $this->attachment->attachUploadedFile($message->id, $attachment);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();

            if(is_null($request->message) && $request->file('attachments'))
                $errorMsg = __('m_view_support_ticket.message field is required');
            else
                $errorMsg = __('m_view_support_ticket.failed to send message');

            throw (new WebException($e))->redirectTo(route('member.support.tickets.show', $id))->withMessage($errorMsg);
        }
        return back()->with('success', __('m_view_support_ticket.message sent successfuly'));
    }

    /**
     * Remove the specified resource from storage.
     * @param Destroy $request
     * @param unknown $id
     */
    public function destroy(Destroy $request, $id)
    {
        DB::beginTransaction();
        try {
            $closeStatus = $this->status->findBySlug('closed');
            $ticket      = $this->support->find($id);
            $this->status->changeHistory($ticket, $closeStatus, ['updated_by' => $request->user()->id]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw (new WebException($e))->redirectTo(route('member.support.tickets.index'))->withMessage(__('m_view_support_ticket.failed to close ticket'));
        }

        return back()->with('success', __('m_view_support_ticket.ticket closed successfully'));
    }
}
