<?php

namespace Modules\SupportTicket\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\SupportTicket\Contracts\SupportTicketContract;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Plus65\Base\Rules\CheckOwnership;

class Update extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'       => 'required|max:' . config('supportticket.message.max'),
            'attachments.*' => 'mimetypes:' . implode(',', config('supportticket.attachments.mimes')) . '|max:' . config('supportticket.attachments.max_size'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ticket = resolve(SupportTicketContract::class)->find($this->ticket);
        if (!$ticket) {
            return false;
        }

        $ticketStatus = resolve(SupportTicketStatusContract::class);
        $openStatus   = $ticketStatus->findBySlug($ticketStatus::OPEN);

        if ($ticket->support_ticket_status_id != $openStatus->id) {
            return false;
        }

        return $this->isOwner($ticket, $this->user());
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'message'       => __('m_view_support_ticket.message'),
            'attachments.*' => __('m_view_support_ticket.attachments'),
        ];
    }
}
