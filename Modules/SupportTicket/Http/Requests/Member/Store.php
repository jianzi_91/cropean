<?php

namespace Modules\SupportTicket\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'support_ticket_type_id' => 'required|numeric|max:255',
            'subject'                => 'required|max:' . config('supportticket.subject.max'),
            'message'                => 'required|max:' . config('supportticket.message.max'),
            'attachments.*'          => 'mimetypes:' . implode(',', config('supportticket.attachments.mimes')) . '|max:' . config('supportticket.attachments.max_size'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'support_ticket_type_id' => __('m_create_support_ticket.category'),
            'subject'                => __('m_create_support_ticket.subject'),
            'message'                => __('m_create_support_ticket.message'),
            'attachments.*'          => __('m_create_support_ticket.attachments')
        ];
    }
}
