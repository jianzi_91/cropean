<?php

namespace Modules\SupportTicket\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Plus65\Base\Rules\CheckOwnership;
use Modules\SupportTicket\Contracts\SupportTicketContract;

class Show extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ticket = resolve(SupportTicketContract::class)->find($this->ticket);
        if (!$ticket) {
            return false;
        }

        return $this->isOwner($ticket, $this->user());
    }
}
