<?php

namespace Modules\SupportTicket\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Plus65\Base\Rules\CheckOwnership;
use Modules\SupportTicket\Contracts\SupportTicketContract;

class Update extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|max:' . config('supportticket.message.max'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ticket = resolve(SupportTicketContract::class)->find($this->ticket);
        if (!$ticket) {
            return false;
        }

        return true;

        //return $this->isOwner($ticket, $this->user());
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'message' => __('a_view_support_ticket.message'),
        ];
    }
}
