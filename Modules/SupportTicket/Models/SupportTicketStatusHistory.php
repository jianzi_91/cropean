<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SupportTicketStatusHistory extends Model
{
    use SoftDeletes, UserRelation;

    protected $fillable = [
            'support_ticket_id',
            'support_ticket_status_id',
            'updated_by',
            'remarks',
            'is_current',
    ];

    /**
     * This model's relation to support ticket
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(SupportTicket::class, 'support_ticket_id');
    }

    /**
     * This model's relation to support ticket status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(SupportTicketStatus::class, 'support_ticket_status_id');
    }

    /**
     * Local scope for getting current status
     * @param unknown $query
     * @param unknown $current
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where('is_current', $current);
    }
}
