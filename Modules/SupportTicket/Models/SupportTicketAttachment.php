<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SupportTicketAttachment extends Model
{
    use SoftDeletes, UserRelation;

    protected $fillable = [
            'filename',
            'original_filename',
            'mime_type',
            'path',
            'support_ticket_message_id'
    ];

    /**
     * This model's relation to support ticket message
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(SupportTicketMessage::class, 'support_ticket_message_id');
    }
}
