<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SupportTicketMessage extends Model
{
    use SoftDeletes, UserRelation;

    protected $fillable = [
            'message',
            'support_ticket_id',
            'user_id'
    ];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['attachments'];

    /**
     * This model's relation to support ticket
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(SupportTicket::class, 'support_ticket_id');
    }

    /**
     * This model's relation to attachments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(SupportTicketAttachment::class, 'support_ticket_message_id');
    }
}
