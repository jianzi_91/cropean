<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Scopes\HasActiveScope;

class SupportTicketStatus extends Model implements Stateable
{
    use SoftDeletes, Translatable, HasActiveScope, HasDropDown, HistoryRelation;

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    protected $fillable = [
            'name',
            'name_translation',
            'is_active'
    ];

    /**
     * This model's relation to support ticket histories
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany(SupportTicketStatusHistory::class, 'support_ticket_status_id');
    }

    /**
     * Get the state id
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'support_ticket_status_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return SupportTicketStatusHistory::class;
    }
}
