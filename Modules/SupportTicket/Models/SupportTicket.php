<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Relations\UserRelation;

class SupportTicket extends Model implements Stateable
{
    use SoftDeletes, UserRelation, HistoryRelation;

    protected $fillable = [
             'subject',
             'ticket_number',
             'support_ticket_type_id',
             'user_id',
             'support_ticket_status_id',
             'updated_by',
             'remarks'
    ];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['messages', 'statuses'];

    /**
     * This model's relation to support ticket messages
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(SupportTicketMessage::class, 'support_ticket_id');
    }

    /**
     * This model's relation to support ticket type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(SupportTicketType::class, 'support_ticket_type_id');
    }

    /**
     * This model's relation to support ticket status histories
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(SupportTicketStatusHistory::class, 'support_ticket_id');
    }

    /**
     * Getting model's current status
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status()
    {
        return $this->hasOne(SupportTicketStatusHistory::class, 'support_ticket_id')->current(1);
    }

    /**
     * Get the state id
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'support_ticket_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return SupportTicketStatusHistory::class;
    }
}
