<?php

namespace Modules\SupportTicket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Modules\Translation\Traits\Translatable;

class SupportTicketType extends Model
{
    use SoftDeletes, Translatable, HasActiveScope, HasDropDown;

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    protected $fillable = [
                'name',
                'name_translation',
                'is_active'
    ];

    /**
     * This model's relation to support tickets
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany(SupportTicket::class, 'support_ticket_type_id');
    }
}
