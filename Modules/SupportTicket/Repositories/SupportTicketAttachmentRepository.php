<?php

namespace Modules\SupportTicket\Repositories;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Core\Exceptions\MaxFileSize;
use Modules\Core\Exceptions\UnsupportedMimeType;
use Modules\SupportTicket\Contracts\SupportTicketAttachmentContract;
use Modules\SupportTicket\Models\SupportTicketAttachment;
use Modules\SupportTicket\Models\SupportTicketMessage;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SupportTicketAttachmentRepository extends Repository implements SupportTicketAttachmentContract
{
    use HasCrud;

    /**
     * The message model
     * @var unknown
     */
    protected $messageModel;

    /**
     * The root directory
     * @var unknown
     */
    protected $rootDir;

    /**
     * The storage path location
     * @var unknown
     */
    protected $storagePath;

    /**
     * Class constructor
     * @param SupportTicketAttachment $model
     * @param SupportTicketMessage $message
     */
    public function __construct(SupportTicketAttachment $model, SupportTicketMessage $message)
    {
        $this->messageModel = $message;
        $this->rootDir      = config('filesystems.disks.local.root');
        $this->storagePath  = config('supportticket.storage');
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::find()
     */
    public function find($id, array $with = [], $select = ['*'])
    {
        return $this->model->with($with)->find($id, $select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::attach()
     */
    public function attach($messageId, $filename, $path, $move = false)
    {
        $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
        $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
        return $this->attachUploadedFile($messageId, $upload);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::attachContents()
     */
    public function attachContents($messageId, $filename, $mime, $contents)
    {
        $this->validateByteString($contents, $mime);

        $message = $this->messageModel->findOrFail($messageId);

        $attachment                            = new $this->model;
        $randomFilename                        = $this->randomFilename() . '.' . $mime;
        $attachment->filename                  = $randomFilename;
        $attachment->original_filename         = $filename;
        $attachment->path                      = $this->storagePath;
        $attachment->mime_type                 = $mime;
        $attachment->support_ticket_message_id = $messageId;
        if ($attachment->save()) {
            $dirPath = $this->rootDir . DIRECTORY_SEPARATOR . $this->storagePath;
            $this->checkDirectory($dirPath);

            $relativePath = $this->storagePath . DIRECTORY_SEPARATOR . $randomFilename;
            Storage::put($relativePath, $contents);
            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::attachFileSystem()
     */
    public function attachUploadedFile($messageId, UploadedFile $file, $move = false)
    {
        $this->validateFile($file);

        $attachment                            = new $this->model;
        $randomFilename                        = $this->randomFilename() . '.' . $file->extension();
        $attachment->filename                  = $randomFilename;
        $attachment->original_filename         = $file->getClientOriginalName();
        $attachment->path                      = $this->storagePath;
        $attachment->mime_type                 = $file->getMimeType();
        $attachment->support_ticket_message_id = $messageId;
        if ($attachment->save()) {
            if (config('filesystems.cloud_enable')) {
                Storage::cloud()->putFileAs($this->storagePath, $file, $randomFilename, 'private');
            } else {
                $dirPath = $this->rootDir . DIRECTORY_SEPARATOR . $this->storagePath;
                $this->checkDirectory($dirPath);
                Storage::putFileAs($this->storagePath, $file, $randomFilename);
            }
            $dirPath = $this->rootDir . DIRECTORY_SEPARATOR . $this->storagePath;
            $this->checkDirectory($dirPath);
            Storage::putFileAs($this->storagePath, $file, $randomFilename);
            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::get()
     */
    public function get($attachmentId, $download = false)
    {
        $attachment = $this->model->findOrFail($attachmentId);
        $path       = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
        if (config('filesystems.cloud_enable')) {
            return response(Storage::cloud()->get($path))->header(
                'Content-Type',
                $attachment->mime_type
            );
        }
        if (Storage::exists($path)) {
            $contents = Storage::get($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::getAttachments()
     */
    public function getAttachments($messageId, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->where('support_ticket_message_id', $messageId);

        return $prepare->with($with)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::delete()
     */
    public function delete($attachmentId)
    {
        $attachment = $this->model->find($attachmentId);
        if ($attachment) {
            $path = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
            if (file_exists($path)) {
                $this->fileSystem->delete($path);
            }

            return $attachment->delete();
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::deleteMessageAttachments()
     */
    public function deleteMessageAttachments($messageId)
    {
        $attachments = $this->model->where('support_ticket_message_id', $messageId)->get();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketAttachmentContract::deleteAll()
     */
    public function deleteAll()
    {
        $attachments = $this->all();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     * Generate random file name
     * @return unknown
     */
    protected function randomFilename()
    {
        return md5(str_replace('.', '', Str::random(5) . microtime(true)));
    }

    /**
     * Validate file
     * @param unknown $filename
     * @param unknown $path
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateFile($filename, $path = null)
    {
        $config = config('supportticket.attachments');
        if ($filename instanceof UploadedFile) {
            $mime = $filename->getClientMimeType();
            $size = $filename->getSize();
        } else {
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
            $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
            $mime     = $upload->getClientMimeType();
            $size     = $upload->getSize();
        }

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Validate byte strings
     * @param unknown $contents
     * @param unknown $mime
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateByteString($contents, $mime)
    {
        $config = config('supportticket.attachments');
        $size   = strlen($contents);

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Check directory
     * @param unknown $path
     * @return boolean
     */
    protected function checkDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path, 0777, true);
        }

        return false;
    }
}
