<?php

namespace Modules\SupportTicket\Repositories;

use Modules\SupportTicket\Contracts\SupportTicketContract;
use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Models\SupportTicket;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class SupportTicketRepository extends Repository implements SupportTicketContract
{
    use HasCrud {
        add as protected crudAdd;
    }

    use HasSlug;

    /**
     * The support ticket status repository
     * @var unknown
     */
    protected $statusRepository;

    /**
     * Class constructor
     * @param SupportTicket $model
     * @param SupportTicketStatusContract $status
     */
    public function __construct(SupportTicket $model, SupportTicketStatusContract $status)
    {
        $this->statusRepository = $status;
        $this->slug             = 'ticket_number';
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Plus65\Base\Repositories\Contracts\CrudContract::add()
     */
    public function add(array $attributes = [], $forceFill = false)
    {
        $statuses                              = config('supportticket.statuses');
        $status                                = $this->statusRepository->findBySlug(array_shift($statuses));
        $attributes[ $status->getForeignKey()] = $status->id;
        if ($ticket = $this->crudAdd($attributes, $forceFill)) {
            if ($this->statusRepository->changeHistory($ticket, $status)) {
                return $ticket;
            }
        }

        return false;
    }
}
