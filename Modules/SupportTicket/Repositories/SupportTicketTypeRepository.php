<?php

namespace Modules\SupportTicket\Repositories;

use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\SupportTicket\Contracts\SupportTicketTypeContract;
use Modules\SupportTicket\Models\SupportTicketType;

class SupportTicketTypeRepository extends Repository implements SupportTicketTypeContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor
     * @param SupportTicketType $model
     */
    public function __construct(SupportTicketType $model)
    {
        parent::__construct($model);
    }
}
