<?php

namespace Modules\SupportTicket\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;
use Modules\SupportTicket\Contracts\SupportTicketMessageContract;
use Modules\SupportTicket\Models\SupportTicketMessage;

class SupportTicketMessageRepository extends Repository implements SupportTicketMessageContract
{
    use HasCrud;

    /**
     * Class constructor
     * @param SupportTicketMessage $model
     */
    public function __construct(SupportTicketMessage $model)
    {
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketMessageContract::findByTicketNum()
     */
    public function findByTicketNum($ticketNum, array $with = [], $select = ['*'])
    {
        return $this->model->whereHas(
            'ticket',
                        function ($query) use ($ticketNum) {
                            $query->where('ticket_number', $ticketNum);
                        }
        )->with($with)->first($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketMessageContract::findByTicketId()
     */
    public function findByTicketId($id, array $with = [], $select = ['*'])
    {
        return $this->model->where('support_ticket_id', $id)->with($with)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketMessageContract::deleteByTicketNum()
     */
    public function deleteByTicketNum($ticketNum)
    {
        return $this->model->whereHas(
            'ticket',
                    function ($query) use ($ticketNum) {
                        $query->where('ticket_number', $ticketNum);
                    }
        )->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\SupportTicket\Contracts\SupportTicketMessageContract::deleteByTicketId()
     */
    public function deleteByTicketId($id)
    {
        return $this->model->where('support_ticket_id', $id)->delete();
    }
}
