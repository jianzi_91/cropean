<?php

namespace Modules\SupportTicket\Repositories;

use Modules\SupportTicket\Contracts\SupportTicketStatusContract;
use Modules\SupportTicket\Models\SupportTicketStatus;
use Modules\SupportTicket\Models\SupportTicketStatusHistory;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class SupportTicketStatusRepository extends Repository implements SupportTicketStatusContract
{
    use HasCrud, HasSlug, HasHistory, HasActive;

    /**
     * Class constructor
     * @param SupportTicketStatus $model
     * @param SupportTicketStatusHistory $history
     */
    public function __construct(SupportTicketStatus $model, SupportTicketStatusHistory $history)
    {
        $this->historyModel = $history;
        parent::__construct($model);
    }
}
