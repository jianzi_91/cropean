<?php

return [
    'name' => 'SupportTicket',

    /*
     |--------------------------------------------------------------------------
     | Repository and contract bindings
     |
     |--------------------------------------------------------------------------
     */
    'bindings' => [
        'Modules\SupportTicket\Contracts\SupportTicketAttachmentContract' => 'Modules\SupportTicket\Repositories\SupportTicketAttachmentRepository',
        'Modules\SupportTicket\Contracts\SupportTicketContract'           => 'Modules\SupportTicket\Repositories\SupportTicketRepository',
        'Modules\SupportTicket\Contracts\SupportTicketMessageContract'    => 'Modules\SupportTicket\Repositories\SupportTicketMessageRepository',
        'Modules\SupportTicket\Contracts\SupportTicketStatusContract'     => 'Modules\SupportTicket\Repositories\SupportTicketStatusRepository',
        'Modules\SupportTicket\Contracts\SupportTicketTypeContract'       => 'Modules\SupportTicket\Repositories\SupportTicketTypeRepository',
     ],

    /*
     |--------------------------------------------------------------------------
     | Attachment settings
     |--------------------------------------------------------------------------
     |
     | Here you can configure the attachments
     |
     */
    'attachments' => [

        'mimes' => [
            '.gif' => 'image/gif',
            '.ico' => 'image/x-icon',
            '.jpg' => 'image/jpeg',
            '.png' => 'image/png',
        ],

        'max_size' => 20000 // in kilo bytes
    ],

    /*
    |--------------------------------------------------------------------------
    | Default statuses
    |--------------------------------------------------------------------------
    |
    | Here you can configure the default status.
    | The first index will be the default
    |
    */
    'statuses' => [
        'open'   => 'Open',
        'closed' => 'Closed'
    ],

    /*
    |--------------------------------------------------------------------------
    | Default support ticket types
    |--------------------------------------------------------------------------
    |
    | Here you can configure the default support ticket types
    |
    */
    'types' => [
        'general'   => 'General',
        'sales'     => 'Sales',
        'questions' => 'Questions',
        'others'    => 'Others'
    ],

    /*
    |--------------------------------------------------------------------------
    | Attachment storage location
    |--------------------------------------------------------------------------
    |
    | Here you can configure where to store the attachments
    |
    */
    'storage' => 'support',

    /*
    |--------------------------------------------------------------------------
    | Subject
    |--------------------------------------------------------------------------
    |
    | Here you can configure the subject
    |
    */
    'subject' => [
        'max' => 100,
    ],

    /*
    |--------------------------------------------------------------------------
    | Message
    |--------------------------------------------------------------------------
    |
    | Here you can configure the message
    |
    */
    'message' => [
        'max' => 65535,
    ],
];
