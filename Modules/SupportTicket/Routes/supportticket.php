<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'supports'], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('/support/attachments/{id}', ['uses' => 'Admin\SupportTicketAttachmentController@show'])->name('admin.support.attachments.show');
        Route::resource('tickets', 'Admin\SupportTicketController', ['as' => 'admin.support'])->except(['create', 'edit', 'store']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('/support/attachments/{id}', ['uses' => 'Member\SupportTicketAttachmentController@show'])->name('member.support.attachments.show');
        Route::resource('tickets', 'Member\SupportTicketController', ['as' => 'member.support'])->except(['edit', 'destroy']);
    });
});
