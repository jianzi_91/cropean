@extends('templates.member.master')
@section('title', __('m_page_title.support'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.support'), 'route' => route('member.support.tickets.index')],
        ['name'=>__('s_breadcrumb.view support ticket')]
    ],
    'header'=>__('m_view_support_ticket.view support ticket'),
    'backRoute'=>route('member.support.tickets.index')
])

<div class="row">
    <div class="col-xs-12 col-lg-4">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h5 class="mb-2 card-title">{{ __('m_view_support_ticket.support ticket details') }}</h5>
                    {{ Form::formLabel(__('m_view_support_ticket.ticket subject'), $ticket->subject) }}
                    {{ Form::formLabel(__('m_view_support_ticket.ticket number'), $ticket->ticket_number) }}
                    {{ Form::formLabel(__('m_view_support_ticket.ticket category'), $ticket->type->name) }}
                    {{ Form::formLabel(__('m_view_support_ticket.ticket status'), $ticket->status->status->name) }}
                    {{ Form::formLabel(__('m_view_support_ticket.ticket created at'), $ticket->created_at) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-8">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="support-ticket-msg-cont">
                        <div class="msg-box">
                            @foreach ($ticket->messages as $message)
                                <div class="chat-cont {{Auth::user()->id == $message->user->id ? 'right' : ''}}">
                                    <!-- Image -->
                                    @can('member_support_attachment_show')
                                    @foreach($message->attachments as $attachment)
                                    @isset ($attachment)
                                    <div class="chat-msg {{Auth::user()->id == $message->user->id ? 'right' : 'left'}}" style="margin-bottom:10px;">
                                        <img style="max-width:100%;" src="{{ route('member.support.attachments.show', $attachment->id) }}" alt="">
                                    </div>
                                    @endisset
                                    @endforeach
                                    @endcan
                                    <div class="chat-msg {{Auth::user()->id == $message->user->id ? 'right' : 'left'}}">{{$message->message}}</div>
                                    <div class="chat-details">{{$message->user->name}}, {{$message->created_at}}</div>
                                </div>
                            @endforeach
                        </div>
                        @can('member_support_ticket_edit')
                        @if ($ticket->status->status->rawname != 'closed')
                        {{ Form::open(['method'=>'put', 'id'=>'create-support-ticket', 'route'=>['member.support.tickets.update', $ticket->id] ,'class'=>'form-vertical -with-label', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        <div class="row input-cont">
                            <div class="col-9 text-cont">
                                {{Form::formTextArea('message', '', '', [])}}
                            </div>
                            <div class="col-3 icon-cont">
                                <i class="bx bx-paperclip" id="attachment"></i>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                {{ Form::formButtonPrimary('btn_submit', __('m_view_support_ticket.submit'), 'submit') }}
                                <input type="file" name="attachments[]" style="display:none;" id="file-input" accept="image/*" />
                            </div>
                        </div>
                        <div class="attachment-cont">
                            {{ __('m_view_support_ticket.attachment') }}:
                            <span id="filename"></span>
                            &nbsp;&nbsp;&nbsp;
                            <span class="remove">{{ __('m_view_support_ticket.remove') }}</span>
                        </div>
                        {{ Form::close() }}
                        @endif
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('.msg-box').scrollTop($('.msg-box')[0].scrollHeight)

    $('#attachment').on('click', function() {
        $('#file-input').click()
    })

    $('#file-input').on('change', function() {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0]
            $('#filename').html(file.name)
            $('.attachment-cont').show()
        } else $('.attachment-cont').hide()
    })

    $('.remove').on('click', function() {
        $('#file-input').val('')
        $('#filename').html('')
        $('.attachment-cont').hide()
    })
})
</script>
@endpush
