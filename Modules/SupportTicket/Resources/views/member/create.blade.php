@extends('templates.member.master')
@section('title', __('m_page_title.support'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.support'), 'route' => route('member.support.tickets.index')],
        ['name'=>__('s_breadcrumb.new support ticket')]
    ],
    'header'=>__('m_create_support_ticket.new support ticket'),
    'backRoute'=> route('member.support.tickets.index'),
])

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['method'=>'post', 'route'=>'member.support.tickets.store', 'id'=>'create-support-ticket', 'class'=>'form-vertical -with-label','files'=>true, 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                            {{ Form::formSelect('support_ticket_type_id', ticket_type_dropdown(), '', __('m_create_support_ticket.category'), [], false) }}
                            {{ Form::formText('subject', old('subject'), __('m_create_support_ticket.subject'), [], false) }}
                            {{ Form::formTextArea('message', old('message'), __('m_create_support_ticket.message'), ["id"=>"support_message"], false) }}
                            {{ Form::formFileUpload('attachments', '', __('m_create_support_ticket.attachments'), __('m_create_support_ticket.choose file'), ['name'=>'attachments[]','id'=>'attachments', 'multiple', 'accept'=>"image/png, image/jpeg, image/gif", 'info'=>__('m_create_support_ticket.2mb file limit, jpg/png/gif types, more than 1 file can be uploaded')], true) }}

                    <div class="row mt-3">
                        <div class="col btn-container">
                            {{ Form::formButtonPrimary('btn_submit', __('m_create_support_ticket.create'), 'submit') }}
                            &nbsp;&nbsp;
                            {{ Form::formButtonSecondary('btn_cancel', __('m_create_support_ticket.cancel'), 'button') }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#btn_cancel').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("member.support.tickets.index") }}'
    })
})
</script>
@endpush