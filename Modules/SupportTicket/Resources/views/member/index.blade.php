@extends('templates.member.master')
@section('title', __('m_page_title.support'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.support')]
    ],
    'header'=>__('m_support_tickets.customer support')
])

<div class="pb-2">
    @can('member_support_ticket_create')
    {{ Form::formButtonPrimary('btn_new', __('m_support_tickets.new support ticket')) }}
    @endcan
</div>

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('m_support_tickets.date')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('ticket_number', request('ticket_number'),__('m_support_tickets.ticket number'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('support_ticket_type_id', ticket_type_dropdown(), request('support_ticket_type_id'), __('m_support_tickets.category'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('support_ticket_status_id', ticket_status_dropdown(), request('support_ticket_status_id') ,__('m_support_tickets.status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('m_support_tickets.support tickets list') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_support_tickets.date') }}</th>
                <th>{{ __('m_support_tickets.ticket number') }}</th>
                <th>{{ __('m_support_tickets.category') }}</th>
                <th>{{ __('m_support_tickets.subject') }}</th>
                <th>{{ __('m_support_tickets.status') }}</th>
                <th>{{ __('m_support_tickets.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @if(count($tickets))
            @foreach($tickets as $ticket)
            <tr>
                <td>{{$ticket->created_at}}</td>
                <td>{{$ticket->ticket_number}}</td>
                <td>{{$ticket->type->name}}</td>
                <td>{{$ticket->subject}}</td>
                <td>{{$ticket->status->status->name}}</td>
                <td class="action">
                    @can('member_support_ticket_list')
                        <a href="{{route('member.support.tickets.show',$ticket->id)}}"><i class="bx bx-show-alt"></i></a>
                    @endcan
                </td>
            </tr>
            @endforeach
        @else
            @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('m_support_tickets.no records') ])
        @endif
        </tbody>
        @endcomponent
    </div>
</div>
{!! $tickets->render() !!}
@endsection


@push('scripts')
<script>
$(function(){
    $('#btn_new').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("member.support.tickets.create") }}'
    })
})
</script>
@endpush