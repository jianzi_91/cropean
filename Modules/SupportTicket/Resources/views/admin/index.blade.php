@extends('templates.admin.master')
@section('title', __('a_page_title.support'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.customer service')]
    ],
    'header'=>__('a_support_tickets.customer service')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_support_tickets.date')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('member_id', request('member_id'),__('a_support_tickets.member id'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('ticket_number', request('ticket_number'),__('a_support_tickets.ticket number'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('support_ticket_type_id', ticket_type_dropdown(), old('support_ticket_type_id', request('support_ticket_type_id')), __('a_support_tickets.category'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('support_ticket_status_id', ticket_status_dropdown(), old('support_ticket_status_id', request('support_ticket_status_id')), __('a_support_tickets.status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_support_tickets.support tickets list') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_support_tickets.date') }}</th>
                <th>{{ __('a_support_tickets.ticket number') }}</th>
                <th>{{ __('a_support_tickets.member id') }}</th>
                <th>{{ __('a_support_tickets.category') }}</th>
                <th>{{ __('a_support_tickets.subject') }}</th>
                <th>{{ __('a_support_tickets.status') }}</th>
                <th>{{ __('a_support_tickets.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @if(count($tickets))
            @foreach($tickets as $ticket)
            <tr>
                <td>{{$ticket->created_at}}</td>
                <td>{{$ticket->ticket_number}}</td>
                <td>{{$ticket->user->member_id}}</td>
                <td>{{$ticket->type->name}}</td>
                <td>{{$ticket->subject}}</td>
                <td>{{$ticket->status->status->name}}</td>
                <td class="action">
                    @can('admin_support_ticket_list')
                        <a class="btn btn-gold btn-sm btn-round mb-1" href="{{route('admin.support.tickets.show',$ticket->id)}}"><i class="bx bx-show-alt"></i></a>
                    @endcan
                </td>
            </tr>
            @endforeach
        @else
            @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_support_tickets.no records') ])
        @endif
        </tbody>
        @endcomponent
    </div>
</div>
{!! $tickets->render() !!}
@endsection