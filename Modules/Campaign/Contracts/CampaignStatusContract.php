<?php

namespace Modules\Campaign\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface CampaignStatusContract extends CrudContract, SlugContract, HistoryableContract
{
	const DRAFT     = 'draft';
    const OPEN      = 'open';
    const CLOSED    = 'closed';
	const VESTING   = 'vesting';
	const COMPLETED = 'completed';
	const PAID      = 'paid';
	const REFUNDED  = 'refunded';
	const SCHEDULED = 'scheduled';
}