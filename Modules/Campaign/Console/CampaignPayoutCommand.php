<?php

namespace Modules\Campaign\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignParticipant;
use Modules\Campaign\Models\CampaignPayout;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;

class CampaignPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'campaign:payout {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Campaign payout command.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon($this->argument('date'));

        if (CampaignPayout::where('payout_date', $date->toDateString())->exists()) {
            $this->error("Campaign payout already processed for $date");
            exit;
        }

        $campaigns = Campaign::where('payout_date', $date->toDateString())->get();

        $startTime = $this->start();
        $this->line("Campaign ROI Payout started at " . $startTime->toDateTimeString());

        if (!empty($campaigns)) {
            DB::beginTransaction();
        
            $creditRepo          = resolve(BankAccountCreditContract::class);
            $campaignStatusRepo  = resolve(CampaignStatusContract::class);
            $usdToCashCreditRate = ExchangeRate::where('currency', BankCreditTypeContract::CASH_CREDIT)->first();

            try {
                foreach ($campaigns as $campaign) {
                    $payoutRate   = $campaign->actual_roi_rate;
                    $participants = CampaignParticipant::where('campaign_id', $campaign->id)
                        ->where('is_admin', false)
                        ->groupBy('user_id')
                        ->select([
                            'campaign_participants.*',
                            DB::raw('SUM(amount) AS total_participations')
                        ]);

                    foreach ($participants->cursor() as $participant) {
                        $roiInUsd  = $participant->total_participations * $payoutRate;
                        $roiInCash = bcmul($roiInUsd, $usdToCashCreditRate->buy_rate, 2);

                        $txnCode = $creditRepo->add(BankCreditTypeContract::CASH_CREDIT, $roiInCash, $participant->user_id, BankTransactionTypeContract::CAMPAIGN_PAYOUT);

                        $payout = CampaignPayout::create([
                            'campaign_id'      => $campaign->id,
                            'user_id'          => $participant->user_id,
                            'amount_usd'       => $roiInUsd,
                            'exchange_rate'    => $usdToCashCreditRate->buy_rate,
                            'amount'           => $roiInCash, 
                            'payout_rate'      => $payoutRate,
                            'transaction_code' => $txnCode,
                            'payout_date'      => $date->toDateString(),
                        ]);

                        CampaignParticipant::where('campaign_id', $campaign->id)
                            ->where('user_id', $participant->user_id)
                            ->update([
                                'payout_id' => $payout->id,
                            ]);
                    }

                    $paidStatus = $campaignStatusRepo->findBySlug(CampaignStatusContract::PAID);
                    $campaignStatusRepo->changeHistory($campaign, $paidStatus);
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                $this->line($e->getMessage());
            }
        }
        

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
