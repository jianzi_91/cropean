<?php

namespace Modules\Campaign\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignStatus;

class CampaignStatusUpdateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'campaign:update-status {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Campaign status update command.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon($this->argument('date'));
        $yesterday = $date->copy()->subDay();

        $updatableStatuses = CampaignStatus::whereIn('name', [CampaignStatusContract::OPEN,CampaignStatusContract::CLOSED,CampaignStatusContract::VESTING, CampaignStatusContract::SCHEDULED])
            ->get()
            ->pluck('id')
            ->toArray();

        $campaigns = Campaign::whereIn('campaign_status_id', $updatableStatuses)->get();

        $this->start();

        if (!empty($campaigns)) {
            DB::beginTransaction();
        
            $campaignStatusRepo  = resolve(CampaignStatusContract::class);

            try {
                foreach ($campaigns as $campaign) {

                    if ($campaign->status->status->rawname == CampaignStatusContract::OPEN) {
                        $this->line("$campaign->name is open, cutoff_date is $campaign->cutoff_date");
                        if ($campaign->cutoff_date == $yesterday->toDateString()) {
                            $status = $campaignStatusRepo->findBySlug(CampaignStatusContract::CLOSED);

                            $campaignStatusRepo->changeHistory($campaign, $status);

                            $this->line("$campaign->name status changed to closed");
                        }
                    }

                    if ($campaign->status->status->rawname == CampaignStatusContract::CLOSED) {
                        $this->line("$campaign->name is closed, vesting_start is $campaign->vesting_start_date");
                        if ($campaign->vesting_start_date == $date->toDateString()) {
                            $status = $campaignStatusRepo->findBySlug(CampaignStatusContract::VESTING);

                            $campaignStatusRepo->changeHistory($campaign, $status);

                            $this->line("$campaign->name status changed to vesting");
                        }
                    }

                    if ($campaign->status->status->rawname == CampaignStatusContract::VESTING) {
                        $this->line("$campaign->name is vesting, vesting_end is $campaign->vesting_end_date");

                        if ($campaign->vesting_end_date <= $yesterday->toDateString() && isset($campaign->actual_roi_rate)) {
                            $status = $campaignStatusRepo->findBySlug(CampaignStatusContract::COMPLETED);
                            $campaignStatusRepo->changeHistory($campaign, $status);

                            $campaign->update([
                                'payout_date' => $date->toDateString(),
                            ]);

                            $this->line("$campaign->name status changed to completed");
                        }
                    }

                    if ($campaign->status->status->rawname == CampaignStatusContract::SCHEDULED) {
                        $this->line("$campaign->name is scheduled, scheduled publishing date is $campaign->published_at");
                        if (Carbon::parse($campaign->published_at)->toDateString() == $date->toDateString()) {
                            $status = $campaignStatusRepo->findBySlug(CampaignStatusContract::OPEN);

                            $campaignStatusRepo->changeHistory($campaign, $status);

                            $this->line("$campaign->name status changed to open");
                        }
                    }
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                $this->line($e->getMessage());
            }
        }
        

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
