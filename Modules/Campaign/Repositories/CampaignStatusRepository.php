<?php

namespace Modules\Campaign\Repositories;

use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Models\CampaignStatus;
use Modules\Campaign\Models\CampaignStatusHistory;
use Plus65\Base\Repositories\Repository;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;

class CampaignStatusRepository extends Repository implements CampaignStatusContract
{
    use HasCrud, HasSlug, HasHistory;
    // implement functions here

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param UserStatus  $model
     */
    public function __construct(CampaignStatus $model, CampaignStatusHistory $history)
    {
        $this->model = $model;
        $this->historyModel = $history;

        parent::__construct($model);
    }
}
