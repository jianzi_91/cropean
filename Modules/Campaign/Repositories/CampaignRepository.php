<?php

namespace Modules\Campaign\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignParticipant;
use Modules\Campaign\Models\CampaignRefund;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Repositories\Repository;
use Plus65\Base\Repositories\Concerns\HasCrud;

class CampaignRepository extends Repository implements CampaignContract
{
    use HasCrud;       
    
    /**
     * Class constructor
     */
    public function __construct(
        Campaign $model,
        CampaignStatusContract $campaignStatusRepo,
        UserContract $userRepo,
        BankAccountCreditContract $creditRepo
    ) {
        $this->model              = $model;
        $this->campaignStatusRepo = $campaignStatusRepo;
        $this->userRepo           = $userRepo;
        $this->creditRepo         = $creditRepo;
    }

    /**
     * Add participant
     */
    public function participate($campaignId, $userId, $amount, $isAdmin = false)
    {
        $rate = ExchangeRate::where('currency', BankCreditTypeContract::USDT_ERC20)->first();

        return CampaignParticipant::create([
            'campaign_id'   => $campaignId,
            'user_id'       => $userId,
            'raw_amount'    => $amount,
            'exchange_rate' => $isAdmin == false ? $rate->sell_rate : 1,
            'amount'        => $isAdmin == false ? bcdiv($amount, $rate->sell_rate, 2) : $amount,
            'is_admin'      => $isAdmin,
        ]);
    }

    /**
     * Close campaign
     */
    public function close($campaignId)
    {
        $campaign    = $this->find($campaignId);
        $closeStatus = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::CLOSED);

        $this->campaignStatusRepo->changeHistory($campaign, $closeStatus);
    }

    /**
     * Refund campaign
     */
    public function refund($campaignId) {
        $campaign = Campaign::findOrFail($campaignId);

        $participations = CampaignParticipant::where('campaign_id', $campaignId)
            ->where('is_admin', false)
            ->groupBy('user_id')
            ->select([
                'user_id',
                DB::raw('SUM(amount) as total_amount'),
            ])
            ->pluck('total_amount', 'user_id')
            ->toArray();

        foreach ($participations as $userId => $amount) {
            
            $txnCode = $this->creditRepo->add(BankCreditTypeContract::USDT_ERC20, $amount, $userId, BankTransactionTypeContract::CAMPAIGN_REFUND);

            $refund = CampaignRefund::create([
                'campaign_id'      => $campaignId,
                'user_id'          => $userId,
                'transaction_code' => $txnCode,
                'amount'           => $amount,
                'refund_date'      => now()->toDateString(),
            ]);

            $refundedParticipations = CampaignParticipant::where('campaign_id', $campaignId)
                ->where('user_id', $userId)
                ->update([
                    'refund_id' => $refund->id,
                ]);
        }

        $campaign->update([
            'refund_date' => now()->toDateString(),
        ]);
    }
}
