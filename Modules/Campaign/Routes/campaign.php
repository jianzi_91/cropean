<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web','auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('campaign/export', 'Admin\CampaignExportController@export')->name('admin.campaign.export');
        Route::get('campaign/export-participants/{campaignId}', 'Admin\CampaignExportController@exportParticipants')->name('admin.campaign.export-participants');
        Route::post('campaign/topup/{campaignId}', 'Admin\CampaignController@topUp')->name('admin.campaign.topup');
        Route::post('campaign/actual-roi/{campaignId}', 'Admin\CampaignController@updateActualRoi')->name('admin.campaign.actual-roi');
        Route::get('campaign/participants/{campaignId}', 'Admin\CampaignController@getParticipants')->name('admin.campaign.participants');
        Route::put('campaign/{campaignId}/update-status', 'Admin\CampaignController@updateStatus')->name('admin.campaign.update-status');
        Route::get('reports/campaign', 'Admin\CampaignReportController@index')->name('admin.campaign.report.index');
        Route::get('reports/campaign/export', 'Admin\CampaignReportController@export')->name('admin.campaign.report.export');
        Route::resource('campaign', 'Admin\CampaignController', ['as' => 'admin']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('campaign/export', 'Member\CampaignExportController@export')->name('member.campaign.export');
        Route::get('campaign/portfolio/export', 'Member\CampaignExportController@exportPortfolio')->name('member.campaign.export-portfolios');
        Route::get('campaign/portfolio', 'Member\CampaignController@portfolio')->name('member.campaign.portfolio');
        Route::get('reports/campaign', 'Member\CampaignReportController@index')->name('member.campaign.report.index');
        Route::get('reports/campaign/export', 'Member\CampaignReportController@export')->name('member.campaign.report.export');
        Route::resource('campaign', 'Member\CampaignController', ['as' => 'member'])->only(['index','show']);
        Route::post('campaign/{campaign_id}', 'Member\CampaignController@participate')->name('member.campaign.participate');
    });
});
