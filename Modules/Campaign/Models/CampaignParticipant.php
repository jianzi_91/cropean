<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class CampaignParticipant extends Model
{
    protected $table   = "campaign_participants";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function payout()
    {
        return $this->hasOne(CampaignPayout::class, 'payout_id');
    }

    public function getTotalEquityInUsdAttribute()
    {
        $rateRepo = resolve(ExchangeRateContract::class);
        
        $capitalInBase   = $rateRepo->toBaseCurrency(BankCreditTypeContract::CAPITAL_CREDIT, $this->user->capital_credit);
        $rolloverInBase  = $rateRepo->toBaseCurrency(BankCreditTypeContract::ROLL_OVER_CREDIT, $this->user->roll_over_credit);
        $promotionInBase = $this->user->promotion_credit; // So far promotion hasnt been set with an exchange rate

        return bcsum([$capitalInBase, $rolloverInBase, $promotionInBase]);
    }
}
