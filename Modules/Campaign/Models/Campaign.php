<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Relations\UserRelation;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\User\Models\User;

class Campaign extends Model implements Stateable
{
    use SoftDeletes, UserRelation, HistoryRelation;

    protected $table    = 'campaigns';
    protected $fillable = [
        'name',
        'fund_size',
        'published_at',
        'cutoff_date',
        'vesting_start_date',
        'vesting_end_date',
        'payout_date',
        'refund_date',
        'estimate_roi_rate_min',
        'estimate_roi_rate_max',
        'actual_roi_rate',
        'participate_amount_minimum',
        'participate_amount_maximum',
        'participate_amount_multiple',
        'campaign_status_id',
        'created_by',
    ];

    public function participants()
    {
        return $this->hasMany(CampaignParticipant::class, 'campaign_id');
    }

    public function participant($userId)
    {
        return $this->hasMany(CampaignParticipant::class, 'campaign_id')->where('user_id', $userId)->get();
    }

    public function getCurrentFundAttribute()
    {
        return $this->participants->sum('amount');
    }

    /**
     * This model's relation to user status histories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(CampaignStatusHistory::class, 'campaign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status()
    {
        return $this->hasOne(CampaignStatusHistory::class, 'campaign_id')->current(1);
    }

    /**
     * Get the state id.
     *
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'campaign_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return CampaignStatusHistory::class;
    }
}
