<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignRefund extends Model
{
    protected $table   = "campaign_refunds";
    protected $guarded = [];
}
