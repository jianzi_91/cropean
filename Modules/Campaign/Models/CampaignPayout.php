<?php

namespace Modules\Campaign\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignPayout extends Model
{
    protected $table   = "campaign_payouts";
    protected $guarded = [];
}
