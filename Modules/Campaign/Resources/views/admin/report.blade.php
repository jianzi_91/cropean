@extends('templates.admin.master')
@section('title', __('a_page_title.campaigns report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_campaign_report.campaigns report')],
    ],
    'header'=>__('a_campaign_report.campaigns report')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('payout_date', request('payout_date'), __('a_campaign_report.payout date'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('member_id', request('member_id'), __('a_campaign_report.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('a_campaign_report.member name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('email', request('email'), __('a_campaign_report.member email'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('campaign', request('campaign'), __('a_campaign_report.campaign name'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_campaign_report.campaigns')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_campaign_report.total results:') }}  {{ $payouts->count() }}</div>
                @can('admin_campaign_report_export')
                {{ Form::formTabSecondary(__('a_campaign_report.export excel'), route('admin.campaign.report.export', http_build_query(request()->except('page')))) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
              <tr>
                    <th>{{ __('a_campaign_report.payout date') }}</th>
                    <th>{{ __('a_campaign_report.name') }}</th>
                    <th>{{ __('a_campaign_report.member id') }}</th>
                    <th>{{ __('a_campaign_report.email') }}</th>
                    <th>{{ __('a_campaign_report.campaign') }}</th>
                    <th>{{ __('a_campaign_report.payout rate') }}</th>
                    <th>{{ __('a_campaign_report.payout amount') }}</th>
              </tr>
            </thead>
            <tbody>
                @forelse($payouts as $payout)
                <tr>
                    <td>{{ $payout->payout_date }}</td>
                    <td>{{ $payout->member_name }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->member_email }}</td>
                    <td>{{ $payout->campaign_name }}</td>
                    <td>{{ bcmul($payout->payout_rate, 100, 2) }}</td>
                    <td>{{ amount_format($payout->amount, 2) }}</td>
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 7, 'text' => __('a_campaign_report.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
    {!! $payouts->render() !!}
</div>
@endsection