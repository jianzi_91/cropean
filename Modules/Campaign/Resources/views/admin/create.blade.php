@extends('templates.admin.master')
@section('title', __('a_page_title.create campaign'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_create_campaign.all campaigns'), 'route'=>route('admin.campaign.index')],
        ['name'=>__('a_create_campaign.create campaign')]
    ],
    'header'=>__('a_create_campaign.create campaign'),
    'backRoute'=>route('admin.campaign.index'),
])

<div class="col-xs-12 col-lg-8">
  <div class="card">
      <div class="card-content">
        <div class="card-body">
          {{ Form::open(['method'=>'post', 'route' => 'admin.campaign.store', 'id'=>'fm-campaign', 'enctype'=>'multipart/form-data']) }}
            {{ Form::formText('name', '', __('a_create_campaign.campaign name')) }}
            {{ Form::formNumber('fund_size', old('fund_size') ?? 0, __('a_create_campaign.campaign fund size usd')) }}
            {{ Form::formDate('cutoff_date', old('cutoff_date'), __('a_create_campaign.campaign cutoff date')) }}
            {{ Form::formDateRange('vesting_period', old('vesting_period'), __('a_create_campaign.vesting period'), array(), true) }}
            {{ Form::formText('estimate_roi_rate_min', old('estimate_roi_rate_min') ?? 0, __('a_create_campaign.estimated roi min %')) }}
            {{ Form::formText('estimate_roi_rate_max', old('estimate_roi_rate_max') ?? 0, __('a_create_campaign.estimated roi max %')) }}
            {{ Form::formNumber('participate_amount_minimum', old('participate_amount_minimum') ?? 0, __('a_create_campaign.minimum participation amount usd')) }}
            {{ Form::formNumber('participate_amount_multiple', old('participate_amount_minimum') ?? 0, __('a_create_campaign.multiple amount to participate usd')) }}
            {{ Form::formNumber('participate_amount_maximum', old('participate_amount_maximum') ?? 0, __('a_create_campaign.maximum amount in % equity to participate')) }}
            {{ Form::formDate('published_at', old('published_at'), __('a_create_campaign.schedule publishing date')) }}
            {{ Form::formHide('is_draft', 'false') }}
            <div class="row d-flex flex-row justify-content-end m-0">
              {{ Form::formButtonSecondary('btn_submit_draft', __('a_create_campaign.save as draft'), 'button')}}
              &nbsp;&nbsp;&nbsp;
              {{ Form::formButtonPrimary('btn_submit', __('a_create_campaign.publish'), 'button')}}
            </div>
          {{ Form::close() }}
        </div>
      </div>
  </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('#fm-campaign', {
        estimate_roi_rate_min: { 
          presence: { message: __.validation.field_required },
          format: {
            pattern: /^(100(\.00?)?|[1-9]?\d(\.\d\d?)?)$/,
            message: "{{__('a_create_campaign.must be a valid percentage value')}}",
          }, 
        },
        estimate_roi_rate_max: { 
          presence: { message: __.validation.field_required },
          format: {
            pattern: /^(100(\.00?)?|[1-9]?\d(\.\d\d?)?)$/,
            message: "{{__('a_create_campaign.must be a valid percentage value')}}",
          }, 
        },
        name: { presence: { message: __.validation.field_required } },
        fund_size: { presence: { message: __.validation.field_required } },
        cutoff_date: { presence: { message: __.validation.field_required } },
        vesting_period: { presence: { message: __.validation.field_required } },
    });

    $('#btn_submit_draft').click(function() {
        $('#is_draft').val("true");
        $('#fm-campaign').submit();
    });

    $('#btn_submit').click(function() {
        $('#is_draft').val("false");
        $('#fm-campaign').submit();
    })
})
</script>
@endpush