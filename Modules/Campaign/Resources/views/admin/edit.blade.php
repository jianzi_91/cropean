@extends('templates.admin.master')
@section('title', __('a_page_title.edit campaign'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_edit_campaign.all campaigns'), 'route'=>route('admin.campaign.index')],
        ['name'=>__('a_edit_campaign.edit :campaign', ['campaign'=> $campaign->name])]
    ],
    'header'=>__('a_edit_campaign.edit :campaign', ['campaign'=> $campaign->name]),
    'backRoute' => route('admin.campaign.index'),
])

<div class="row">
  <div class="col-xs-12 col-lg-8">
    <div class="card">
        <div class="card-content">
          <div class="card-body">
            {{ Form::open(['method'=>'put', 'route' => ['admin.campaign.update', 'campaign' => $campaign->id], 'id'=>'fm-campaign']) }}
              {{ Form::formText('name', old('name') ?? $campaign->name, __('a_edit_campaign.campaign name')) }}
              {{-- {{ Form::formText('current_fund', number_format($campaign->current_fund, 2, '.', ','), __('a_edit_campaign.current fund size usd'), ['disabled']) }} --}}
              {{ Form::formText('fund_size', old('fund_size') ?? number_format($campaign->fund_size, 2, '.', ''), __('a_edit_campaign.campaign fund size usd')) }}
              {{ Form::formText('available_amount', number_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2, '.', ','), __('a_edit_campaign.available amount usd'), ['disabled']) }}
              {{ Form::formDate('cutoff_date', old('cutoff_date') ?? $campaign->cutoff_date, __('a_edit_campaign.campaign cutoff date'), in_array($campaign->status->status->rawname, ['closed', 'vesting']) ? ['disabled'] : []) }}
              {{ Form::formDateRange('vesting_period', old('vesting_period') ?? $campaign->vesting_start_date . ' to ' . $campaign->vesting_end_date, __('a_edit_campaign.vesting period'), array(), true) }}
              {{ Form::formText('estimate_roi_rate_min', old('estimate_roi_rate_min') ?? bcmul($campaign->estimate_roi_rate_min, 100, 2), __('a_edit_campaign.estimated roi min %')) }}
              {{ Form::formText('estimate_roi_rate_max', old('estimate_roi_rate_max') ?? bcmul($campaign->estimate_roi_rate_max, 100, 2), __('a_edit_campaign.estimated roi max %')) }}
              {{ Form::formNumber('participate_amount_minimum', old('participate_amount_minimum') ?? number_format($campaign->participate_amount_minimum, 2, '.', ''), __('a_edit_campaign.minimum participation amount usd')) }}
              {{ Form::formNumber('participate_amount_multiple', old('participate_amount_multiple') ?? number_format($campaign->participate_amount_multiple, 2, '.', ''), __('a_edit_campaign.multiple amount to participate usd')) }}
              {{ Form::formNumber('participate_amount_maximum', old('participate_amount_maximum') ?? bcmul($campaign->participate_amount_maximum, 100, 2), __('a_edit_campaign.maximum amount in % equity to participate')) }}
              @if($campaign->status->status->rawname == 'scheduled')
              {{ Form::formDate('published_at', $campaign->published_at, __('a_edit_campaign.schedule publishing date')) }}
              @endif
              @if($campaign->status->status->rawname == 'draft')
              {{ Form::formDate('published_at', $campaign->published_at, __('a_edit_campaign.schedule publishing date')) }}
              {{ Form::formHide('is_draft', 'true') }}
              <div class="row d-flex flex-row justify-content-end m-0">
                {{ Form::formButtonSecondary('btn_save', __('a_edit_campaign.save'), 'button')}}
                &ensp;&ensp;&ensp;
                {{ Form::formButtonPrimary('btn_publish', __('a_edit_campaign.publish'), 'button')}}
              </div>
              @else
              {{ Form::formHide('is_draft', 'false') }}
              <div class="row d-flex flex-row justify-content-end m-0">
                {{ Form::formButtonPrimary('btn_submit', __('a_edit_campaign.save'), 'submit')}}
              </div>
              @endif
            {{ Form::close() }}
          </div>
        </div>
    </div>
  </div>
  <div class="col-xs-12 col-lg-4">
    <div class="row">
        <div class="col-xs-12 col-lg-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="kyc_label">{{ __('a_campaign_participants.status') }}</div>
                        <div class="kyc_status">{{ $campaign->status->status->name }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="kyc_label">{{ __('a_campaign_participants.system usdt balance') }}</div>
                        <div class="kyc_status">{{ amount_format(get_system_credit(bank_credit_type_id('usdt_erc20')), 2) }}</div>
                    </div>
                </div>
            </div>
        </div>
      {{--  TOP UP --}}
      @can('admin_campaign_topup')
      @if ($campaign->status->status->rawname == "open")
      <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-content">
              <div class="card-body">
                <div class="card-title">{{ __('a_edit_campaign.fund top up') }}</div>
                {{ Form::open(['method'=>'post', 'route' => ['admin.campaign.topup', 'campaignId' => $campaign->id], 'id'=>'fm-topup', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{bcmul(bcdiv($campaign->current_fund, $campaign->fund_size, 2), 100, 2)}}%" aria-valuenow="{{ bcmul(bcdiv($campaign->current_fund, $campaign->fund_size, 2), 100, 2) }}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <br>
                  {{ Form::formNumber('topup_amount', request('topup_amount') ?? 0, __('a_edit_campaign.company topup usd')) }}
                  <div class="row d-flex flex-row justify-content-end m-0">
                    {{ Form::formButtonPrimary('btn_submit', __('a_edit_campaign.top up'), 'submit')}}
                  </div>
                {{ Form::close() }}
              </div>
            </div>
        </div>
      </div>
      @endif
      @endcan
      {{--  END TOP UP --}}
      {{--  ACTUAL ROI --}}
      @can('admin_campaign_update_roi')
      @if ($campaign->status->status->rawname == "vesting")
      <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-content">
              <div class="card-body">
                <div class="card-title">{{ __('a_edit_campaign.actual roi') }}</div>
                {{ Form::open(['method'=>'post', 'route' => ['admin.campaign.actual-roi', 'campaignId' => $campaign->id], 'id'=>'fm-actual-roi', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                <br>
                  {{ Form::formNumber('actual_roi_rate', bcmul($campaign->actual_roi_rate, 100, 2), __('a_edit_campaign.actual roi')) }}
                  <div class="row d-flex flex-row justify-content-end m-0">
                    {{ Form::formButtonPrimary('btn_set', __('a_edit_campaign.set'), 'submit')}}
                  </div>
                {{ Form::close() }}
              </div>
            </div>
        </div>
      </div>
      @endif
      @endcan
      {{--  END ACTUAL ROI --}}
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#fm-campaign', {
        estimate_roi_rate_min: { 
          presence: { message: __.validation.field_required },
          format: {
            pattern: /^(100(\.00?)?|[1-9]?\d(\.\d\d?)?)$/,
            message: "{{__('a_edit_campaign.must be a valid percentage value')}}",
          }, 
        },
        estimate_roi_rate_max: { 
          presence: { message: __.validation.field_required },
          format: {
            pattern: /^(100(\.00?)?|[1-9]?\d(\.\d\d?)?)$/,
            message: "{{__('a_edit_campaign.must be a valid percentage value')}}",
          }, 
        },
        name: { presence: { message: __.validation.field_required } },
        fund_size: { presence: { message: __.validation.field_required } },
        cutoff_date: { presence: { message: __.validation.field_required } },
        vesting_period: { presence: { message: __.validation.field_required } },
    });

    $('#btn_save').click(function() {
      $('#is_draft').val("true");
      $('#fm-campaign').submit();
    });

    $('#btn_publish').click(function() {
      $('#is_draft').val("false");
      $('#fm-campaign').submit();
    });

    // $('#btn_set').click(function() {
    //   swal.fire({
    //     title: "{{ __('a_edit_campaign.set actual roi') }}",
    //     text: "{{ __('a_edit_campaign.you will not be able to edit this campaign anymore') }}",
    //     showCancelButton: true,
    //     confirmButtonText: "{{ __('a_edit_campaign.confirm') }}",
    //   }).then((result) => {
    //     if (result.value) {
    //       var actual_roi_form = document.getElementById("fm-actual-roi");
          
    //       actual_roi_form.submit();
    //     }
    //   })
    // });
})
</script>
@endpush