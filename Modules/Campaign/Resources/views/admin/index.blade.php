@extends('templates.admin.master')
@section('title', __('a_page_title.all campaigns'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_campaigns.all campaigns')],
    ],
    'header'=>__('a_campaigns.all campaigns')
])
@can('admin_campaign_create')
<div class="pb-2">
  {{ Form::formButtonPrimary('btn_create', __('a_campaigns.new campaign'), 'button') }}
</div>
@endcan

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('published_at', request('published_at'), __('a_campaigns.date published'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('a_campaigns.campaign name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_start', request('vesting_period_start'), __('a_campaigns.vesting period start'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_end', request('vesting_period_end'), __('a_campaigns.vesting period end'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('cutoff_date', request('cutoff_date'), __('a_campaigns.cut off date'), [], false)}}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('campaign_status_id', campaign_statuses_dropdown(), request('campaign_status_id'), __('a_campaigns.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_campaigns.campaigns')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_campaigns.total results:') }}  {{ $campaigns->count() }}</div>
                @can('admin_campaign_export')
                {{ Form::formTabSecondary(__('a_campaigns.export excel'), route('admin.campaign.export', http_build_query(request()->except('page')))) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
              <tr>
                @canany(['admin_campaign_edit', 'admin_campaign_delete'])  
                <th>{{ __('a_campaigns.actions') }}</th>
                @endcanany
                <th>{{ __('a_campaigns.status') }}</th>
                <th>{{ __('a_campaigns.date published') }}</th>
                <th>{{ __('a_campaigns.name') }}</th>
                <th>{{ __('a_campaigns.fund size USD') }}</th>
                <th>{{ __('a_campaigns.available amount') }}</th>
                <th>{{ __('a_campaigns.cutoff date') }}</th>
                <th>{{ __('a_campaigns.vesting period') }}</th>
                <th>{{ __('a_campaigns.% of equity for amount participation') }}</th>
                <th>{{ __('a_campaigns.estimated roi %') }}</th>
                <th>{{ __('a_campaigns.actual roi %') }}</th>
                @can('admin_campaign_participant_list')
                <th>{{ __('a_campaigns.participating members') }}</th>
                @endcan
              </tr>
            </thead>
            <tbody>
                @forelse($campaigns as $campaign)
                <tr>
                    @canany(['admin_campaign_edit', 'admin_campaign_delete'])
                    <td class="action">
                        <div class="d-flex">
                            @can('admin_campaign_edit')
                            <a href="{{ route('admin.campaign.edit', $campaign->id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_campaigns.edit')}}">
                            <i class="bx bx-pencil"></i>
                            </a>
                            @endcan
                            @can('admin_campaign_delete')
                            @if($campaign->status->status->rawname == 'draft')
                                <div>
                                    <form action="{{ route('admin.campaign.destroy', $campaign->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <a role="button" class="js-delete" href="#">
                                            <i class="bx bx-trash"></i>
                                        </a>
                                    </form>
                                </div>
                            @endif
                            @endcan
                        </div>
                    </td>
                    @endcanany
                    <td>{{ $campaign->status->status->name }}</td>
                    <td>{{ \Carbon\Carbon::parse($campaign->published_at)->toDateString() }}</td>
                    <td>{{ $campaign->name }}</td>
                    <td>{{ amount_format($campaign->fund_size, 2) }}</td>
                    <td>{{ amount_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2) }}</td>
                    <td>{{ $campaign->cutoff_date }}</td>
                    <td>{{ $campaign->vesting_start_date }} - {{ $campaign->vesting_end_date }}</td>
                    <td>{{ bcmul($campaign->participate_amount_maximum, 100, 2) }}</td>
                    <td>{{ bcmul($campaign->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($campaign->estimate_roi_rate_max, 100, 2) }}</td>
                    <td>{{ !empty($campaign->actual_roi_rate) ? bcmul($campaign->actual_roi_rate, 100, 2) : '-' }}</td>
                    @can('admin_campaign_participant_list')
                    <td>
                    <a href="{{ route('admin.campaign.participants', $campaign->id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_campaigns.view participants')}}">
                      <i class="bx bx-show"></i>
                    </a>
                    </td>
                    @endcan
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 13, 'text' => __('a_campaigns.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $campaigns->render() !!}
@endsection

@push('scripts')
<script>
    $(function() {
        $('#btn_create').on('click', function(e) {
            e.preventDefault();
            window.location.href = "{{ route('admin.campaign.create') }}"
        })
    });

    $(function () {
    $('.js-delete').on('click',function(e){
        e.preventDefault();
        var el = this;

        swal.fire({
            title: '{{ ucfirst(__("a_campaigns.do you want to delete this campaign")) }}',
            showCancelButton: true,
            cancelButtonText: '{{ __("a_campaigns.no") }}',
            confirmButtonText: '{{ __("a_campaigns.yes") }}',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-primary'
            },
            preConfirm: function(e){
                $(el).closest('form').submit();
            }
        })
    })
})
</script>
@endpush