@extends('templates.admin.master')
@section('title', __('a_page_title.edit campaign'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_edit_campaign.all campaigns'), 'route'=>route('admin.campaign.index')],
        ['name'=>__('a_edit_campaign.edit :campaign', ['campaign'=> $campaign->name])]
    ],
    'header'=>__('a_edit_campaign.edit :campaign', ['campaign'=> $campaign->name]),
    'backRoute' => route('admin.campaign.index'),
])

<div class="row">
  <div class="col-xs-12 col-lg-8">
    <div class="card">
        <div class="card-content">
          <div class="card-body">
              {{ Form::formText('name', $campaign->name, __('a_edit_campaign.campaign name'), ['disabled']) }}
              {{ Form::formText('current_fund', number_format($campaign->current_fund, 2, '.', ','), __('a_edit_campaign.current fund size usd'), ['disabled']) }}
              {{ Form::formText('fund_size', number_format($campaign->fund_size, 2, '.', ''), __('a_edit_campaign.campaign fund size usd'), ['disabled']) }}
              {{ Form::formDate('cutoff_date', $campaign->cutoff_date, __('a_edit_campaign.campaign cutoff date'), ['disabled']) }}
              {{ Form::formDateRange('vesting_period', $campaign->vesting_start_date . ' to ' . $campaign->vesting_end_date, __('a_edit_campaign.vesting period'), array('disabled'), true) }}
              {{ Form::formText('estimate_roi_rate_min', bcmul($campaign->estimate_roi_rate_min, 100, 2), __('a_edit_campaign.estimated roi min %'), ['disabled']) }}
              {{ Form::formText('estimate_roi_rate_max', bcmul($campaign->estimate_roi_rate_max, 100, 2), __('a_edit_campaign.estimated roi max %'), ['disabled']) }}
              {{ Form::formNumber('participate_amount_minimum', number_format($campaign->participate_amount_minimum, 2, '.', ''), __('a_edit_campaign.minimum participation amount usd'), ['disabled']) }}
              {{ Form::formNumber('participate_amount_multiple', number_format($campaign->participate_amount_multiple, 2, '.', ''), __('a_edit_campaign.multiple amount to participate usd'), ['disabled']) }}
              {{ Form::formNumber('participate_amount_maximum', bcmul($campaign->participate_amount_maximum, 100, 2), __('a_edit_campaign.maximum amount in % equity to participate'), ['disabled']) }}
              @can('admin_campaign_refund')
              @if ($campaign->status->status->rawname == "paid")
              {{ Form::open(['method'=>'put', 'route' => ['admin.campaign.update-status', 'campaignId' => $campaign->id], 'id'=>'fm-refund', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                <div class="row d-flex flex-row justify-content-end m-0">
                  {{ Form::formHide('status', 'refunded') }}
                  {{ Form::formButtonPrimary('btn_refund', __('a_edit_campaign.refund'), 'button')}}
                </div>
              {{ Form::close() }}
              @endif
              @endcan
          </div>
        </div>
    </div>
  </div>
  <div class="col-xs-12 col-lg-4">
    <div class="row">
      <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="kyc_label">{{ __('a_campaign_participants.status') }}</div>
                    <div class="kyc_status">{{ $campaign->status->status->name }}</div>
                </div>
            </div>
        </div>
      </div>
      <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-content">
              <div class="card-body">
                <div class="card-title">{{ __('a_edit_campaign.actual roi') }}</div>
                  {{ Form::formNumber('actual_roi_rate', bcmul($campaign->actual_roi_rate, 100, 2), __('a_edit_campaign.actual roi'), ['disabled']) }}
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('#btn_refund').click(function() {
      swal.fire({
        title: "{{ __('a_edit_campaign.refund') }}",
        text: "{{ __('a_edit_campaign.usdt will be refunded to participants are you sure to continue') }}",
        showCancelButton: true,
        confirmButtonText: "{{ __('a_edit_campaign.confirm') }}",
      }).then((result) => {
        if (result.value) {
          var refund_fm = document.getElementById("fm-refund");
          
          refund_fm.submit();
        }
      })
    });
})
</script>
@endpush