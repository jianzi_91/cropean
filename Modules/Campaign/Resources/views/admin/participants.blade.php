@extends('templates.admin.master')
@section('title', __('a_page_title.campaign participants'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_campaign_participants.all campaigns'), 'route'=>route('admin.campaign.index')],
        ['name'=>__('a_campaign_participants.:campaign participants', ['campaign'=> $campaign->name])]
    ],
    'header'=>__('a_campaign_participants.:campaign participants', ['campaign'=> $campaign->name]),
    'backRoute'=>route('admin.campaign.index'),
])

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="kyc_label">{{ __('a_campaign_participants.status') }}</div>
                    <div class="kyc_status">{{ $campaign->status->status->name }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="kyc_label">{{ __('a_campaign_participants.total member participation amount') }}</div>
                    <div class="kyc_status">{{ amount_format($totalMemberParticipatedMember, 2) }}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('member_id', request('member_id'), __('a_campaign_participants.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('a_campaign_participants.full name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('email', request('email'), __('a_campaign_participants.email address'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_campaign_participants.participants')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_campaign_participants.total results:') }}  {{ $participants->count() }}</div>
                @can('admin_campaign_participant_export')
                {{ Form::formTabSecondary(__('a_campaign_participants.export excel'), route('admin.campaign.export-participants', ['campaignId' => $campaign->id, http_build_query(request()->except('page'))])) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>Created at</th>
                    <th>{{ __('a_campaign_participants.full name') }}</th>
                    <th>{{ __('a_campaign_participants.member id') }}</th>
                    <th>{{ __('a_campaign_participants.email') }}</th>
                    <th>{{ __('a_campaign_participants.max participation amount allowed usd') }}</th>
                    <th>{{ __('a_campaign_participants.participated amount usd') }}</th>
                    <th>{{ __('a_campaign_participants.campaign roi amount earned cash credits') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse($participants as $participant)
                <tr>
                    <td>{{ $participant->participated_at }}</td>
                    <td>{{ $participant->member_name }}</td>
                    <td>{{ $participant->member_id }}</td>
                    <td>{{ $participant->email }}</td>
                    <td>{{ amount_format(bcmul($participant->total_equity_in_usd, $campaign->participate_amount_maximum, 2)) }}</td>
                    <td>{{ amount_format($participant->total_participated) }}</td>
                    @if(in_array($campaign->status->status->rawname, ['paid', 'refunded']) && $participant->is_admin == false)
                    <td>{{ bcmul($participant->amount, $campaign->actual_roi_rate, 2) }}</td>
                    @else 
                    <td>-</td>
                    @endif
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_campaign_participants.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $participants->render() !!}
@endsection