@extends('templates.member.master')
@section('title', __('a_page_title.my portfolio'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_portfolio.my portfolio')],
    ],
    'header'=>__('m_portfolio.my portfolio'),
])


@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('published_at', request('published_at'), __('m_portfolio.date published'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('m_portfolio.campaign name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_start', request('vesting_period_start'), __('m_campaigns.vesting period start'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_end', request('vesting_period_end'), __('m_campaigns.vesting period end'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('cutoff_date', request('cutoff_date'), __('m_portfolio.cut off date'), [], false)}}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('campaign_status_id', campaign_statuses_dropdown(['draft']), request('campaign_status_id'), __('m_portfolio.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_portfolio.portfolios')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                @can('member_campaign_portfolio_export')
                {{ Form::formTabSecondary(__('m_portfolio.export excel'), route('member.campaign.export-portfolios', http_build_query(request()->except('page')))) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
              <tr>
                  <th>{{ __('m_portfolio.status') }}</th>
                  <th>{{ __('m_portfolio.date published') }}</th>
                  <th>{{ __('m_portfolio.campaign name') }}</th>
                  <th>{{ __('m_portfolio.campaign fund size USD') }}</th>
                  <th>{{ __('m_portfolio.available amount') }}</th>
                  <th>{{ __('m_portfolio.cutoff date') }}</th>
                  <th>{{ __('m_portfolio.vesting period') }}</th>
                  <th>{{ __('m_portfolio.% of equity for amount participation') }}</th>
                  <th>{{ __('m_portfolio.estimated roi %') }}</th>
                  <th>{{ __('m_portfolio.actual roi %') }}</th>
                  <th>{{ __('m_portfolio.participation amount self usd') }}</th>
                  <th>{{ __('m_portfolio.campaign roi amount earned self cash credits') }}</th>
              </tr>
            </thead>
            <tbody>
                @forelse($participations as $participation)
                <tr>
                    <td>{{ $participation->campaign->status->status->name }}</td>
                    <td>{{ \Carbon\Carbon::parse($participation->published_at)->toDateString() }}</td>
                    <td>{{ $participation->name }}</td>
                    <td>{{ amount_format($participation->fund_size, 2) }}</td>
                    <td>{{ amount_format(bcsub($participation->fund_size, $participation->campaign->current_fund, 2), 2) }}</td>
                    <td>{{ $participation->cutoff_date }}</td>
                    <td>{{ $participation->vesting_start_date }} - {{ $participation->vesting_end_date }}</td>
                    <td>{{ bcmul($participation->campaign->participate_amount_maximum, 100, 2) }}</td>
                    <td>{{ bcmul($participation->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($participation->estimate_roi_rate_max, 100, 2) }}</td>
                    <td>{{ in_array($participation->campaign->status->status->rawname, ['completed', 'paid', 'refunded']) ? bcmul($participation->actual_roi_rate, 100, 2) : '-' }}</td>
                    <td>{{ amount_format($participation->total, 2) }}</td>
                    <td>{{ !empty($participation->payout_amount) ? amount_format($participation->payout_amount, 2) : '-' }}</td>
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', [ 'span' => 11, 'text' => __('m_campaigns.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $participations->render() !!}
@endsection