@extends('templates.member.master')
@section('title', __('m_page_title.all campaigns'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_campaigns.all campaigns')],
    ],
    'header'=>__('m_campaigns.all campaigns')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('published_at', request('published_at'), __('m_campaigns.date published'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('name', request('name'), __('m_campaigns.campaign name'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_start', request('vesting_period_start'), __('m_campaigns.vesting period start'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('vesting_period_end', request('vesting_period_end'), __('m_campaigns.vesting period end'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDate('cutoff_date', request('cutoff_date'), __('m_campaigns.cut off date'), [], false)}}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('campaign_status_id', campaign_statuses_dropdown(['draft', 'scheduled']), request('campaign_status_id'), __('m_campaigns.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_campaigns.campaigns')}}</h4>
            <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center">
                @can('member_campaign_export')
                {{ Form::formTabSecondary(__('m_campaigns.export excel'), route('member.campaign.export', http_build_query(request()->except('page')))) }}
                @endcan
            </div>
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
          <tr>
            <th>{{ __('m_campaigns.status') }}</th>
            <th>{{ __('m_campaigns.date published') }}</th>
            <th>{{ __('m_campaigns.campaign name') }}</th>
            <th>{{ __('m_campaigns.campaign fund size usd') }}</th>
            <th>{{ __('m_campaigns.available amount usd') }}</th>
            <th>{{ __('m_campaigns.cutoff date for participation') }}</th>
            <th>{{ __('m_campaigns.vesting period') }}</th>
            <th>{{ __('m_campaigns.% of equity for amount participation') }}</th>
            <th>{{ __('m_campaigns.estimated roi %') }}</th>
            <th>{{ __('m_campaigns.actual roi %') }}</th>
            @can('member_campaign_participate')
            <th>{{ __('m_campaigns.action')}}</th>
            @endcan
          </tr>
        </thead>
        <tbody>
            @forelse($campaigns as $campaign)
            <tr>
                <td>{{ $campaign->status->status->name }}</td>
                <td>{{ \Carbon\Carbon::parse($campaign->published_at)->toDateString() }}</td>
                <td>{{ $campaign->name }}</td>
                <td>{{ amount_format($campaign->fund_size, 2) }}</td>
                <td>{{ amount_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2) }}</td>
                <td>{{ $campaign->cutoff_date }}</td>
                <td>{{ $campaign->vesting_start_date }} - {{ $campaign->vesting_end_date }}</td>
                <td>{{ bcmul($campaign->participate_amount_maximum, 100, 2) }}</td>
                <td>{{ bcmul($campaign->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($campaign->estimate_roi_rate_max, 100, 2) }}</td>
                <td>{{ in_array($campaign->status->status->rawname, ['completed', 'paid', 'refunded']) ? bcmul($campaign->actual_roi_rate, 100, 2) : '-' }}</td>
                @can('member_campaign_participate')
                @if ($campaign->status->status->rawname == 'open')
                <td>
                    <a href="{{ route('member.campaign.show', $campaign->id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('m_campaigns.participate')}}">
                      <i class="bx bx-show"></i>
                    </a>
                </td>
                @else
                <td></td>
                @endif
                @endcan
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 12, 'text' => __('m_campaigns.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $campaigns->render() !!}
@endsection