@extends('templates.member.master')
@section('title', __('m_page_title.all campaigns'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_campaign_participate.all campaigns'), 'route'=>route('member.campaign.index')],
        ['name'=>__('m_campaign_participate.participate')]
    ],
    'header'=>__('m_campaign_participate.participate'),
    'backRoute'=>route('member.campaign.index'),
])

<div class="row">
    <div class="col-lg-8 col-xs-12 order-lg-1 order-2">
      <div class="card">
          <div class="card-content">
              <div class="card-body pb-2 pt-1">
                  <div class="kyc_label">{{ __('m_campaign_participate.status') }}</div>
                  <div class="d-flex flex-row">
                    <div class="kyc_status">{{ $campaign->status->status->name }}</div>
                  </div>
              </div>
          </div>
      </div>
      <div class="card">
        <div class="card-content">
          <div class="card-body">
            <div class="col-lg-10 col-xs-12">
            {{ Form::open(['method'=>'post', 'route' => ['member.campaign.participate', $campaign->id] , 'id'=>'participate', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
              {{ Form::formText('campaign_name', $campaign->name, __('m_campaign_participate.campaign name'), ['readonly']) }}
              {{ Form::formText('fund_size', amount_format($campaign->fund_size, 2), __('m_campaign_participate.campaign fund size usd'), ['readonly']) }}
              {{ Form::formText('available_amount', amount_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2), __('m_campaign_participate.available_amount usd'), ['readonly']) }}
              {{ Form::formText('cutoff_date', $campaign->cutoff_date, __('m_campaign_participate.cutoff participation date'), ['readonly']) }}
              {{ Form::formText('vesting_period', $campaign->vesting_start_date . ' - ' . $campaign->vesting_end_date, __('m_campaign_participate.vesting period'), ['readonly']) }}
              {{ Form::formText('estimated_roi', bcmul($campaign->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($campaign->estimate_roi_rate_max, 100, 2), __('m_campaign_participate.estimated roi %'), ['readonly']) }}
              {{ Form::formText('min_participation_amt', amount_format($campaign->participate_amount_minimum,2), __('m_campaign_participate.minimum participation amount usd'), ['readonly']) }}
              {{ Form::formText('multiple_amt', amount_format($campaign->participate_amount_multiple,2), __('m_campaign_participate.multiple amount to participate usd'), ['readonly']) }}
              {{ Form::formText('max_amt_in_equity_participate', bcsub(bcmul(auth()->user()->total_equity, $campaign->participate_amount_maximum, 2), $campaign->participant(auth()->user()->id)->sum('amount'), 2), __('m_campaign_participate.maximum amount allowed'), ['readonly']) }}
              @if($campaign->status->status->rawname == 'open')
              {{ Form::formNumber('raw_amount', old('raw_amount'), __('m_campaign_participate.participation amount usdt')) }}
              {{ Form::formPassword('secondary_password', old('secondary_password'), __('m_campaign_participate.secondary password'), ['required'=>true]) }}
              <div class="row d-flex flex-row justify-content-end m-0">
                {{ Form::formButtonPrimary('btn_submit', __('m_campaign_participate.participate'), 'submit')}}
              </div>
              @endif
            {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-xs-12 order-lg-2 order-1">
      @include('templates.__fragments.components.countdown-timer', [
          'label'=>__('m_campaign_participate.campaign closing in'),
          'datetime'=> $campaign->cutoff_date .' 23:59:59',
      ])
      @include('templates.__fragments.components.credit-widget-bg-image', [
          'label'=>__('m_campaign_participate.usdt erc20 credit balance'),
          'value'=>amount_format(auth()->user()->usdt_erc20_credit, credit_precision('usdt_erc20')),
          'backgroundClass'=>'widget-usdt-image',
      ])
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#campaign', {
        participation_amt: { presence: { message: __.validation.field_required } },
        secondary_password: { presence: { message: __.validation.field_required } },
    });
})
</script>
@endpush