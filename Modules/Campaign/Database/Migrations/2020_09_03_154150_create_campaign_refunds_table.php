<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_refunds', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('user_id');
            $table->string('transaction_code')->unique();
            $table->unsignedDecimal('amount', 40, 20);
            $table->date('refund_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_refunds');
    }
}
