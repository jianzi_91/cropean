<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unsignedInteger('campaign_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedDecimal('raw_amount', 40, 20);
            $table->unsignedDecimal('exchange_rate', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);
            $table->unsignedTinyInteger('is_admin')->default(0);
            $table->unsignedInteger('payout_id')->nullable();
            $table->unsignedInteger('refund_id')->nullable();

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_participants');
    }
}
