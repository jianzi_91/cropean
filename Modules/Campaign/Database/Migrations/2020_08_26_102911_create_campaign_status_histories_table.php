<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
            $table->unsignedInteger('campaign_status_id')->index();
            $table->unsignedInteger('campaign_id')->index();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('campaign_status_id')
                ->references('id')
                ->on('campaign_statuses');

            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_status_histories');
    }
}
