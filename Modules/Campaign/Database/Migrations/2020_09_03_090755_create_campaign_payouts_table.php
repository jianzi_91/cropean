<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_payouts', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unsignedInteger('campaign_id');
            $table->unsignedInteger('user_id');
            $table->string('transaction_code')->unique();
            $table->unsignedDecimal('amount_usd', 40, 20);
            $table->unsignedDecimal('exchange_rate',5, 4);
            $table->unsignedDecimal('amount', 40, 20);
            $table->unsignedDecimal('payout_rate', 5, 4);
            $table->date('payout_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_payouts');
    }
}
