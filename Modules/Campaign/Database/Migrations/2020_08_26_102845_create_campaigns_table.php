<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
            $table->string('name');
            $table->unsignedDecimal('fund_size', 40, 20)->comment('in USD');
            $table->timestamp('published_at')->nullable();
            $table->date('cutoff_date');
            $table->date('vesting_start_date');
            $table->date('vesting_end_date');
            $table->date('payout_date')->nullable();
            $table->date('refund_date')->nullable();
            $table->unsignedDecimal('estimate_roi_rate_min', 5, 4);
            $table->unsignedDecimal('estimate_roi_rate_max', 5, 4);
            $table->unsignedDecimal('actual_roi_rate', 5, 4)->nullable();
            $table->unsignedDecimal('participate_amount_minimum', 40, 20)->comment('in USD');
            $table->unsignedDecimal('participate_amount_maximum', 5, 4)->comment('in %');
            $table->unsignedDecimal('participate_amount_multiple', 40, 20)->comment('in USD');
            $table->unsignedInteger('campaign_status_id')->nullable()->index();
            $table->unsignedInteger('created_by');

            $table->foreign('created_by')
                ->references('id')
                ->on('users');

            $table->foreign('campaign_status_id')
                ->references('id')
                ->on('campaign_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
