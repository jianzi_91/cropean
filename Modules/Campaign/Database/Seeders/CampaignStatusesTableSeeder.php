<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Contracts\CampaignStatusContract;

class CampaignStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusRepo = resolve(CampaignStatusContract::class);

        $statuses = [
            CampaignStatusContract::DRAFT,
            CampaignStatusContract::OPEN,
            CampaignStatusContract::CLOSED,
            CampaignStatusContract::VESTING,
            CampaignStatusContract::COMPLETED,
            CampaignStatusContract::PAID,
            CampaignStatusContract::REFUNDED,
        ];

        foreach ($statuses as $status) {
            $statusRepo->add([
                'name'      => $status,
                'is_active' => true,
            ]);
        }
    }
}
