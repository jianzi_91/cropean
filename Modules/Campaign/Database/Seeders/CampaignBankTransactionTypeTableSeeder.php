<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Models\BankTransactionType;

class CampaignBankTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $bankTransactionTypes = [
            [
                'name' => 'campaign_participation',
                'is_active' => 1,
            ],
            [
                'name' => 'campaign_payout',
                'is_active' => 1,
            ],
            [
                'name' => 'campaign_refund',
                'is_active' => 1,
            ],
        ];

        foreach($bankTransactionTypes as $bankTransactionType){
            (new BankTransactionType($bankTransactionType))->save();
        }

        DB::commit();
    }
}
