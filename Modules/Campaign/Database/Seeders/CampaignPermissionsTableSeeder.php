<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CampaignPermissionsTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_campaign' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Campaigns',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_campaign_list',
                        'title' => 'Admin Campaign List',
                    ],
                    [
                        'name'  => 'admin_campaign_create',
                        'title' => 'Admin Campaign Create',
                    ],
                    [
                        'name'  => 'admin_campaign_edit',
                        'title' => 'Admin Campaign Edit',
                    ],
                    [
                        'name'  => 'admin_campaign_topup',
                        'title' => 'Admin Campaign Top Up',
                    ],
                    [
                        'name'  => 'admin_campaign_update_roi',
                        'title' => 'Admin Campaign Update ROI',
                    ],
                    [
                        'name'  => 'admin_campaign_refund',
                        'title' => 'Admin Campaign Refund',
                    ],
                    [
                        'name'  => 'admin_campaign_delete',
                        'title' => 'Admin Campaign Delete',
                    ],
                    [
                        'name'  => 'admin_campaign_export',
                        'title' => 'Admin Campaign Export',
                    ],
                    [
                        'name'  => 'admin_campaign_participant_list',
                        'title' => 'Admin Campaign Participant List',
                    ],
                    [
                        'name'  => 'admin_campaign_participant_export',
                        'title' => 'Admin Campaign Participant Export',
                    ],
                    [
                        'name'  => 'admin_campaign_report_list',
                        'title' => 'Admin Campaign Report List',
                    ],
                    [
                        'name'  => 'admin_campaign_report_export',
                        'title' => 'Admin Campaign Report Export',
                    ],
                ]
            ],
            'member_campaign' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Campaigns',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_campaign_list',
                        'title' => 'Member Campaign List',
                    ],
                    [
                        'name'  => 'member_campaign_participate',
                        'title' => 'Member Campaign Participate',
                    ],
                    [
                        'name'  => 'member_campaign_export',
                        'title' => 'Member Campaign Export',
                    ],
                    [
                        'name'  => 'member_campaign_portfolio_list',
                        'title' => 'Member Campaign Portfolio List',
                    ],
                    [
                        'name'  => 'member_campaign_portfolio_export',
                        'title' => 'Member Campaign Portfolio Export',
                    ],
                    [
                        'name'  => 'member_campaign_report_list',
                        'title' => 'Member Campaign Report List',
                    ],
                    [
                        'name'  => 'member_campaign_report_export',
                        'title' => 'Member Campaign Report Export',
                    ],
                ],
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_campaign_list',
                'admin_campaign_create',
                'admin_campaign_edit',
                'admin_campaign_topup',
                'admin_campaign_update_roi',
                'admin_campaign_refund',
                'admin_campaign_delete',
                'admin_campaign_export',
                'admin_campaign_participant_list',
                'admin_campaign_participant_export',
                'admin_campaign_report_list',
                'admin_campaign_report_export',
            ],
            Role::ADMIN => [
                'admin_campaign_list',
                'admin_campaign_create',
                'admin_campaign_edit',
                'admin_campaign_topup',
                'admin_campaign_update_roi',
                'admin_campaign_refund',
                'admin_campaign_delete',
                'admin_campaign_export',
                'admin_campaign_participant_list',
                'admin_campaign_participant_export',
                'admin_campaign_report_list',
                'admin_campaign_report_export',
            ],
            Role::MEMBER => [
                'member_campaign_list',
                'member_campaign_participate',
                'member_campaign_export',
                'member_campaign_portfolio_list',
                'member_campaign_portfolio_export',
                'member_campaign_report_list',
                'member_campaign_report_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
