<?php

namespace Modules\Campaign\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Campaign\Contracts\CampaignStatusContract;

class AddScheduledStatusToCampaignStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusRepo = resolve(CampaignStatusContract::class);

        $statuses = [
            CampaignStatusContract::SCHEDULED,
        ];

        foreach ($statuses as $status) {
            $statusRepo->add([
                'name'      => $status,
                'is_active' => true,
            ]);
        }
    }
}
