<?php

namespace Modules\Campaign\Queries\Admin;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Queries\CampaignPayoutQuery as BaseQuery;

class CampaignPayoutQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'campaign_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'member_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'campaign' => [
            'filter'    => 'equal',
            'table'     => 'campaigns',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_campaign_report.payout date'),
            __('a_campaign_report.name'),
            __('a_campaign_report.member id'),
            __('a_campaign_report.email'),
            __('a_campaign_report.campaign'),
            __('a_campaign_report.payout rate'),
            __('a_campaign_report.payout amount'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->member_name,
            $payout->member_id,
            $payout->member_email,
            $payout->campaign_name,
            bcmul($payout->payout_rate, 100, 2),
            amount_format($payout->amount, 2),
        ];
    }
}
