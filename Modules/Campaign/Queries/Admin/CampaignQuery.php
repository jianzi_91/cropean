<?php

namespace Modules\Campaign\Queries\Admin;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Campaign\Queries\CampaignQuery as BaseQuery;

class CampaignQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'published_at' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'published_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'like',
            'table'     => 'campaigns',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_start' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_end' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_end_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'cutoff_date' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'cutoff_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'campaign_status_id' => [
            'filter'    => 'select',
            'table'     => 'campaigns',
            'column'    => 'campaign_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_campaigns.status'),
            __('a_campaigns.date published'),
            __('a_campaigns.name'),
            __('a_campaigns.fund size USD'),
            __('a_campaigns.available amount'),
            __('a_campaigns.cutoff date'),
            __('a_campaigns.vesting period'),
            __('a_campaigns.estimated roi %'),
            __('a_campaigns.% of equity for amount participation'),
            __('a_campaigns.actual roi %'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($campaign): array
    {
        return [
            $campaign->status_name,
            Carbon::parse($campaign->published_at)->toDateString(),
            $campaign->name,
            amount_format($campaign->fund_size, 2),
            amount_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2),
            $campaign->cutoff_date,
            $campaign->vesting_start_date . ' - ' . $campaign->vesting_end_date,
            bcmul($campaign->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($campaign->estimate_roi_rate_max, 100, 2),
            bcmul($campaign->participate_amount_maximum, 100, 2),
            !empty($campaign->actual_roi_rate) ? bcmul($campaign->actual_roi_rate, 100, 2) : '-',
        ];
    }
}
