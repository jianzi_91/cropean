<?php

namespace Modules\Campaign\Queries\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Queries\ParticipantQuery as BaseQuery;

class ParticipantQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'name' => [
            'filter'    => 'like',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'member_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    public function beforeBuild()
    {
        return $this->builder->where('campaign_id', request()->campaignId);
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_campaign_participants.full name'),
            __('a_campaign_participants.member id'),
            __('a_campaign_participants.email'),
            __('a_campaign_participants.max participation amount allowed usd'),
            __('a_campaign_participants.participated amount usd'),
            __('a_campaign_participants.campaign roi amount earned cash credits'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($participant): array
    {
        $campaign = Campaign::find(request()->campaignId);

        return [
            $participant->member_name,
            $participant->member_id,
            $participant->email,
            amount_format(bcmul($participant->total_equity_in_usd, $campaign->participate_amount_maximum, 2)),
            amount_format($participant->total_participated),
            in_array($campaign->status->status->rawname, ['paid', 'refunded']) ? bcmul($participant->amount, $campaign->actual_roi_rate, 2) : '-',
        ];
    }
}
