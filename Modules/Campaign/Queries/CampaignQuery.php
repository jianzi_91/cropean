<?php

namespace Modules\Campaign\Queries;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\Campaign;
use QueryBuilder\QueryBuilder;

class CampaignQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = Campaign::select('campaigns.*', 'campaign_statuses.name_translation AS status_name')
            ->join('campaign_status_histories', function ($join) {
                $join->on('campaign_status_histories.campaign_id', '=', 'campaigns.id')
                    ->where('campaign_status_histories.is_current', 1)
                    ->whereNull('campaign_status_histories.deleted_at');
            })
            ->join('campaign_statuses', 'campaign_statuses.id', '=', 'campaign_status_histories.campaign_status_id')
                ->with('status.status')
            ->orderBy('campaigns.created_at', 'desc');

        return $query;
    }
}
