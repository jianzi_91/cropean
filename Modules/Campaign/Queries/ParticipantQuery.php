<?php

namespace Modules\Campaign\Queries;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\Campaign;
use Modules\Campaign\Models\CampaignParticipant;
use QueryBuilder\QueryBuilder;

class ParticipantQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = CampaignParticipant::select([
                DB::raw("MAX(campaign_participants.created_at) AS participated_at"),
                'campaign_participants.user_id',
                'campaign_participants.campaign_id',
                'campaign_participants.amount',
                'campaigns.name AS campaign_name',
                'users.member_id AS member_id',
                'users.email AS email',
                'users.name AS member_name',
                DB::raw("SUM(amount) AS total_participated"),
            ])
            ->join('campaigns', 'campaigns.id', 'campaign_participants.campaign_id')
            ->join('users', 'users.id', 'campaign_participants.user_id')
            ->groupBy('user_id')
            ->orderBy('participated_at', 'DESC');

        return $query;
    }
}
