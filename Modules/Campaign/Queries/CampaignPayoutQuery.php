<?php

namespace Modules\Campaign\Queries;

use Modules\Campaign\Models\CampaignPayout;
use QueryBuilder\QueryBuilder;

class CampaignPayoutQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = CampaignPayout::select([
                'campaign_payouts.*',
                'users.member_id AS member_id',
                'users.name AS member_name',
                'users.email AS member_email',
                'campaigns.name AS campaign_name',
                'campaigns.actual_roi_rate AS campaign_actual_roi',
            ])
            ->join('users', 'users.id', 'campaign_payouts.user_id')
            ->join('campaigns', 'campaigns.id', 'campaign_payouts.campaign_id')
            ->orderBy('payout_date', 'DESC');

        return $query;
    }
}
