<?php

namespace Modules\Campaign\Queries\Member;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Queries\CampaignQuery as BaseQuery;

class CampaignQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'published_at' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'published_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'like',
            'table'     => 'campaigns',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_start' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_end' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_end_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'cutoff_date' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'cutoff_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'campaign_status_id' => [
            'filter'    => 'select',
            'table'     => 'campaigns',
            'column'    => 'campaign_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    public function beforeBuild()
    {
        $statusRepo      = resolve(CampaignStatusContract::class);
        $draftStatus     = $statusRepo->findBySlug(CampaignStatusContract::DRAFT);
        $scheduledStatus = $statusRepo->findBySlug(CampaignStatusContract::SCHEDULED);

        return $this->builder->whereNotIn('campaigns.campaign_status_id', [$draftStatus->id, $scheduledStatus->id]);
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('m_campaigns.status'),
            __('m_campaigns.date published'),
            __('m_campaigns.campaign name'),
            __('m_campaigns.campaign fund size usd'),
            __('m_campaigns.available amount usd'),
            __('m_campaigns.cutoff date for participation'),
            __('m_campaigns.vesting period'),
            __('m_campaigns.% of equity for amount participation'),
            __('m_campaigns.estimated roi %'),
            __('m_campaigns.actual roi %'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($campaign): array
    {
        return [
            $campaign->status_name,
            Carbon::parse($campaign->published_at)->toDateString(),
            $campaign->name,
            amount_format($campaign->fund_size, 2),
            amount_format(bcsub($campaign->fund_size, $campaign->current_fund, 2), 2),
            $campaign->cutoff_date,
            $campaign->vesting_start_date . ' - ' . $campaign->vesting_end_date,
            bcmul($campaign->participate_amount_maximum, 100, 2),
            bcmul($campaign->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($campaign->estimate_roi_rate_max, 100, 2),
            in_array($campaign->status->status->rawname, ['completed', 'paid', 'refunded']) ? bcmul($campaign->actual_roi_rate, 100, 2) : '-',
        ];
    }
}
