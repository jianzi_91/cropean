<?php

namespace Modules\Campaign\Queries\Member;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Queries\PortfolioQuery as BaseQuery;

class PortfolioQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'published_at' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'published_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_start' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'vesting_period_end' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'vesting_end_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'like',
            'table'     => 'campaigns',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'cutoff_date' => [
            'filter'    => 'date_range',
            'table'     => 'campaigns',
            'column'    => 'cutoff_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'campaign_status_id' => [
            'filter'    => 'select',
            'table'     => 'campaigns',
            'column'    => 'campaign_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    public function beforeBuild()
    {
        return $this->builder->where('campaign_participants.user_id', auth()->user()->id);
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('m_portfolio.status'),
            __('m_portfolio.date published'),
            __('m_portfolio.campaign name'),
            __('m_portfolio.campaign fund size USD'),
            __('m_portfolio.available amount'),
            __('m_portfolio.cutoff date'),
            __('m_portfolio.vesting period'),
            __('m_portfolio.% of equity for amount participation'),
            __('m_portfolio.estimated roi %'),
            __('m_portfolio.actual roi %'),
            __('m_portfolio.participation amount self usd'),
            __('m_portfolio.campaign roi amount earned self cash credits'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($participation): array
    {
        return [
            $participation->campaign->status->status->name,
            Carbon::parse($participation->published_at)->toDateString(),
            $participation->name,
            amount_format($participation->fund_size, 2),
            amount_format(bcsub($participation->fund_size, $participation->campaign->current_fund, 2), 2),
            $participation->cutoff_date,
            $participation->vesting_start_date . ' - ' . $participation->vesting_end_date,
            bcmul($participation->campaign->participate_amount_maximum, 100, 2),
            bcmul($participation->estimate_roi_rate_min, 100, 2) . ' - ' . bcmul($participation->estimate_roi_rate_max, 100, 2),
            in_array($participation->campaign->status->status->rawname, ['completed', 'paid', 'refunded']) ? bcmul($participation->actual_roi_rate, 100, 2) : '-',
            amount_format($participation->total, 2),
            !empty($participation->payout_amount) ? amount_format($participation->payout_amount, 2) : '-',
        ];
    }
}
