<?php

namespace Modules\Campaign\Queries\Member;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Campaign\Queries\CampaignPayoutQuery as BaseQuery;

class CampaignPayoutQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'campaign_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'campaign' => [
            'filter'    => 'equal',
            'table'     => 'campaigns',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    public function beforeBuild()
    {
        return $this->builder->where('campaign_payouts.user_id', auth()->user()->id);
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_campaign_report.payout date'),
            __('a_campaign_report.campaign'),
            __('a_campaign_report.payout rate'),
            __('a_campaign_report.payout amount'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->campaign_name,
            bcmul($payout->payout_rate, 100, 2),
            amount_format($payout->amount, 2),
        ];
    }
}
