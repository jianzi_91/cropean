<?php

namespace Modules\Campaign\Queries;

use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignParticipant;
use QueryBuilder\QueryBuilder;

class PortfolioQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = CampaignParticipant::join('campaigns', 'campaigns.id', 'campaign_participants.campaign_id')
            ->leftJoin('campaign_payouts', 'campaign_payouts.id', 'campaign_participants.payout_id')
            ->select([
                'campaigns.*',
                'campaign_payouts.amount AS payout_amount',
                'campaign_participants.*',
                DB::raw('SUM(campaign_participants.amount) AS total')
            ])
            ->groupBy('campaign_participants.campaign_id')
            ->orderBy('campaigns.published_at', 'desc');

        return $query;
    }
}
