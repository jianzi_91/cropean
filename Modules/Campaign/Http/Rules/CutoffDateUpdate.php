<?php

namespace Modules\Campaign\Http\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Campaign\Contracts\CampaignStatusContract;

class CutoffDateUpdate implements Rule
{
    protected $campaignId;

    /**
     * Class constructor
     *
     */
    public function __construct(
        $campaignId
    ) {
        $this->campaignRepo = resolve(CampaignContract::class);
        $this->campaignId   = $campaignId;
        $this->message      = "";
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $campaign   = $this->campaignRepo->find($this->campaignId);

        if (!isset($value)) {
            $this->message = __('s_validation.the :attribute field is required', ['attribute' => __('a_create_campaign.campaign cutoff date')]);
            return false;
        }

        if (in_array($campaign->status->status->rawname, [CampaignStatusContract::DRAFT, CampaignStatusContract::OPEN])) {
            if ($value <= now()) {
                $this->message = __('s_validation.cutoff date cannot be earlier than today');
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
