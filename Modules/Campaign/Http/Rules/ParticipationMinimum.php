<?php

namespace Modules\Campaign\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class ParticipationMinimum implements Rule
{
    protected $campaignId;

    /**
     * Class constructor
     *
     */
    public function __construct(
        $campaignId
    ) {
        $this->campaignRepo = resolve(CampaignContract::class);
        $this->campaignId   = $campaignId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $campaign = $this->campaignRepo->find($this->campaignId);
        
        if ($value < $campaign->participate_amount_minimum) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.amount must be higher than campaign mimimum');
    }
}
