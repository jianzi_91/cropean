<?php

namespace Modules\Campaign\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class FundSizeLimit implements Rule
{
    protected $campaignId;

    /**
     * Class constructor
     *
     */
    public function __construct(
        $campaignId
    ) {
        $this->rateRepo     = resolve(ExchangeRateContract::class);
        $this->campaignRepo = resolve(CampaignContract::class);
        $this->campaignId   = $campaignId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $campaign    = $this->campaignRepo->find($this->campaignId);
        $currentFund = $campaign->current_fund;

        if ($value < $currentFund) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the value is less than current fund size');
    }
}
