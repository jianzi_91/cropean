<?php

namespace Modules\Campaign\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankCreditType;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class EquityLimit implements Rule
{
    protected $campaignId;

    /**
     * Class constructor
     *
     */
    public function __construct(
        $campaignId
    ) {
        $this->rateRepo     = resolve(ExchangeRateContract::class);
        $this->campaignRepo = resolve(CampaignContract::class);
        $this->campaignId   = $campaignId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user            = auth()->user();
        $capitalInBase   = $this->rateRepo->toBaseCurrency(BankCreditTypeContract::CAPITAL_CREDIT, $user->capital_credit);
        $rolloverInBase  = $this->rateRepo->toBaseCurrency(BankCreditTypeContract::ROLL_OVER_CREDIT, $user->roll_over_credit);
        $promotionInBase = $user->promotion_credit; // So far promotion hasnt been set with an exchange rate
        $currentEquityInBase = bcsum([$capitalInBase, $rolloverInBase, $promotionInBase]);

        $campaign = $this->campaignRepo->find($this->campaignId);
        $currentParticipation = $campaign->participant(auth()->user()->id)->sum('amount');

        $equityLimit       = bcmul($currentEquityInBase, $campaign->participate_amount_maximum, 2);
        $totalParticipation = bcadd($value, $currentParticipation, 2);

        if ($totalParticipation > $equityLimit) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.exceeded equity limit allowed');
    }
}
