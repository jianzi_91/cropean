<?php

namespace Modules\Campaign\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaign\Queries\Member\CampaignQuery;
use Modules\Campaign\Queries\Member\PortfolioQuery;
use QueryBuilder\Concerns\CanExportTrait;

class CampaignExportController extends Controller
{
    use CanExportTrait;

    /**
     * Class constructor
     */
    public function __construct(
        CampaignQuery $campaignQuery,
        PortfolioQuery $portfolioQuery
    ) {
        $this->middleware('permission:member_campaign_export')->only(['export']);
        $this->middleware('permission:member_campaign_portfolio_export')->only(['exportPortfolio']);

        $this->campaignQuery      = $campaignQuery;
        $this->portfolioQuery   = $portfolioQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->campaignQuery->setParameters($request->all()), 'campaigns_' . now() . '.xlsx', auth()->user(), 'Export Campaigns on ' . now());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function exportPortfolio(Request $request)
    {
        return $this->exportReport($this->portfolioQuery->setParameters($request->all()), 'portfolios_' . now() . '.xlsx', auth()->user(), 'Export Portfolio on ' . now());
    }
}
