<?php

namespace Modules\Campaign\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Http\Requests\Member\Participate;
use Modules\Campaign\Queries\Member\CampaignQuery;
use Modules\Campaign\Queries\Member\PortfolioQuery;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;

class CampaignController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct(
        CampaignContract $campaignContract,
        CampaignStatusContract $campaignStatusContract,
        CampaignQuery $campaignQuery,
        PortfolioQuery $portfolioQuery,
        BankAccountCreditContract $creditContract
    ) {
        $this->middleware('permission:member_campaign_list')->only('index');
        $this->middleware('permission:member_campaign_participate')->only(['show', 'participate']);
        $this->middleware('permission:member_campaign_portfolio_list')->only(['portfolio']);

        $this->campaignRepo       = $campaignContract;
        $this->campaignStatusRepo = $campaignStatusContract;
        $this->campaignQuery      = $campaignQuery;
        $this->portfolioQuery     = $portfolioQuery;
        $this->creditRepo         = $creditContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $campaigns = $this->campaignQuery
            ->setParameters($request->all())
            ->paginate();

        return view('campaign::member.index', compact('campaigns'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $campaign = $this->campaignRepo->findOrFail($id);

        return view('campaign::member.show', compact('campaign'));
    }

    /**
     * Participate in a campaign
     */
    public function participate(Participate $request, $id)
    {
        $data     = request()->only('raw_amount');
        $campaign = $this->campaignRepo->findOrFail($id);
        $user     = auth()->user();

        try {
            DB::beginTransaction();

            $details = $this->campaignRepo->participate($campaign->id, $user->id, $data['raw_amount']);
            $this->creditRepo->deduct(BankCreditTypeContract::USDT_ERC20, $details->raw_amount, $user->id, BankTransactionTypeContract::CAMPAIGN_PARTICIPATION);

            if ($campaign->current_fund >= $campaign->fund_size) {
                $this->campaignRepo->close($campaign->id);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e);
        }

        return redirect()->route('member.campaign.index')->with('success', __('m_campaign.participated successfully'));
    }

    public function portfolio(Request $request)
    {
        $participations = $this->portfolioQuery
            ->setParameters($request->all())
            ->paginate();

        return view('campaign::member.portfolio', compact('participations'));
    }
}
