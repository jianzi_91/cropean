<?php

namespace Modules\Campaign\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaign\Queries\Member\CampaignPayoutQuery;
use QueryBuilder\Concerns\CanExportTrait;

class CampaignReportController extends Controller
{
    use CanExportTrait;

    /**
     * Class constructor
     */
    public function __construct(
        CampaignPayoutQuery $payoutQuery
    ) {
        $this->middleware('permission:member_campaign_report_list')->only(['index']);
        $this->middleware('permission:member_campaign_report_export')->only(['export']);

        $this->payoutQuery = $payoutQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutQuery
            ->setParameters($request->all())
            ->paginate();

        return view('campaign::member.report', compact('payouts'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->payoutQuery->setParameters($request->all()), 'campaign_report_' . now() . '.xlsx', auth()->user(), 'Export Campaigns Report on ' . now());
    }
}
