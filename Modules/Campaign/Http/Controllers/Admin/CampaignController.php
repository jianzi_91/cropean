<?php

namespace Modules\Campaign\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Campaign\Http\Requests\Admin\Store;
use Modules\Campaign\Http\Requests\Admin\TopUp;
use Modules\Campaign\Http\Requests\Admin\Update;
use Modules\Campaign\Http\Requests\Admin\UpdateActualRoi;
use Modules\Campaign\Queries\Admin\CampaignQuery;
use Modules\Campaign\Queries\Admin\ParticipantQuery;

class CampaignController extends Controller
{

    /**
     * Class constructor
     */
    public function __construct(
        CampaignContract $campaignContract,
        CampaignStatusContract $campaignStatusContract,
        CampaignQuery $campaignQuery,
        ParticipantQuery $participantQuery
    ) {
        $this->middleware('permission:admin_campaign_list')->only('index');
        $this->middleware('permission:admin_campaign_create')->only(['create', 'store']);
        $this->middleware('permission:admin_campaign_edit')->only(['edit', 'update']);
        $this->middleware('permission:admin_campaign_delete')->only(['destroy']);
        $this->middleware('permission:admin_campaign_topup')->only(['topUp']);
        $this->middleware('permission:admin_campaign_update_roi')->only(['updateActualRoi']);
        $this->middleware('permission:admin_campaign_refund')->only(['updateStatus']);
        $this->middleware('permission:admin_campaign_participant_list')->only(['show', 'getParticipants']);

        $this->campaignRepo       = $campaignContract;
        $this->campaignStatusRepo = $campaignStatusContract;
        $this->campaignQuery      = $campaignQuery;
        $this->participantQuery   = $participantQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $campaigns = $this->campaignQuery
            ->setParameters($request->all())
            ->paginate();

        return view('campaign::admin.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('campaign::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $data = $request->only([
            'name',
            'fund_size',
            'cutoff_date',
            'vesting_period',
            'estimate_roi_rate_min',
            'estimate_roi_rate_max',
            'participate_amount_minimum',
            'participate_amount_multiple',
            'participate_amount_maximum',
            'published_at',
            'is_draft',
        ]);

        $vestingPeriod                      = date_range_extract($data['vesting_period']);
        $data['vesting_start_date']         = $vestingPeriod[0]->toDateString(0);
        $data['vesting_end_date']           = empty($vestingPeriod[1]) ? $vestingPeriod[0]->toDateString() : $vestingPeriod[1]->toDateString();
        $data['estimate_roi_rate_min']      = percentage($data['estimate_roi_rate_min'], 4);
        $data['estimate_roi_rate_max']      = percentage($data['estimate_roi_rate_max'], 4);
        $data['participate_amount_maximum'] = percentage($data['participate_amount_maximum'], 4);
        $data['created_by']                 = auth()->user()->id;

        $campaign = $this->campaignRepo->add($data);

        if ($campaign) {

            if ($data['is_draft'] == "true") {
                $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::DRAFT);
                $this->campaignStatusRepo->changeHistory($campaign, $status);
                return redirect()->route('admin.campaign.index')->with('success', __('a_create_campaign.campaign is saved as draft'));
            }

            if (!empty($data['published_at'])) {
                $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::SCHEDULED);
                $this->campaignStatusRepo->changeHistory($campaign, $status);
                $campaign->update([
                    'published_at' => $data['published_at'],
                ]);
            } else {
                $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::OPEN);
                $this->campaignStatusRepo->changeHistory($campaign, $status);
                $campaign->update([
                    'published_at' => now(),
                ]);
            }

            return redirect()->route('admin.campaign.index')->with('success', __('a_create_campaign.campaign is published successfully'));
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $campaign     = $this->campaignRepo->findOrFail($id);
        $participants = $this->participantQuery
            ->setParameters($request->all())
            ->paginate();

        return view('campaign::admin.show', compact('campaign', 'participants'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $campaign = $this->campaignRepo->findOrFail($id);

        if (in_array($campaign->status->status->rawname, [CampaignStatusContract::COMPLETED, CampaignStatusContract::PAID, CampaignStatusContract::REFUNDED])) {
            return view('campaign::admin.show', compact('campaign'));
        }

        return view('campaign::admin.edit', compact('campaign'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $data = $request->only([
            'name',
            'fund_size',
            'cutoff_date',
            'vesting_period',
            'estimate_roi_rate_min',
            'estimate_roi_rate_max',
            'participate_amount_minimum',
            'participate_amount_multiple',
            'participate_amount_maximum',
            'published_at',
            'is_draft',
        ]);

        $vestingPeriod                      = date_range_extract($data['vesting_period']);
        $data['vesting_start_date']         = $vestingPeriod[0]->toDateString(0);
        $data['vesting_end_date']           = empty($vestingPeriod[1]) ? $vestingPeriod[0]->toDateString() : $vestingPeriod[1]->toDateString();
        $data['estimate_roi_rate_min']      = percentage($data['estimate_roi_rate_min'], 4);
        $data['estimate_roi_rate_max']      = percentage($data['estimate_roi_rate_max'], 4);
        $data['participate_amount_maximum'] = percentage($data['participate_amount_maximum'], 4);
        $data['created_by']                 = auth()->user()->id;

        $campaign = $this->campaignRepo->findOrFail($id);

        DB::beginTransaction();

        try {
            $campaign = $this->campaignRepo->edit($campaign->id, $data);

            // Close the campaign if fund_size is being updated to match current fund
            if ($campaign->current_fund >= $campaign->fund_size) {
                $this->campaignRepo->close($campaign->id);
            }

            if ($campaign->status->status->rawname == CampaignStatusContract::DRAFT) {
                if ($data['is_draft'] != "true") {
                    if (empty($data['published_at'])) {
                        $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::OPEN);
                        $this->campaignStatusRepo->changeHistory($campaign, $status);
                        $campaign->update([
                            'published_at' => now(),
                        ]);
                    } else {
                        $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::SCHEDULED);
                        $this->campaignStatusRepo->changeHistory($campaign, $status);
                        $campaign->update([
                            'published_at' => $data['published_at'],
                        ]);
                    }
                }
            }

            if (empty($data['published_at']) && $campaign->status->status->rawname == CampaignStatusContract::SCHEDULED) {
                $status = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::OPEN);
                $this->campaignStatusRepo->changeHistory($campaign, $status);
                $campaign->update([
                    'published_at' => now(),
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('admin.campaign.index')->with('success', __('a_create_campaign.campaign is edited successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $campaign = $this->campaignRepo->findOrFail($id);

        if ($campaign->status->status->rawname == CampaignStatusContract::DRAFT) {
            DB::beginTransaction();

            try {
                $this->campaignRepo->delete($id);

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                return  back()->with('error', $e->getMessage());
            }

            return back()->with('success', __('a_campaigns.campaign is deleted successfully'));
        } else {
            return back()->with('error', __('a_campaigns.campaign is not in drafting state'));
        }
    }

    /**
     * Update status
     */
    public function updateStatus(Request $request, $id)
    {
        $data = $request->only('status');

        $campaign = $this->campaignRepo->findOrFail($id);
        $status   = $this->campaignStatusRepo->findBySlug($data['status']);

        DB::beginTransaction();

            if ($status->rawname == CampaignStatusContract::REFUNDED) {
                
                if ($campaign->status->status->rawname != CampaignStatusContract::PAID) {
                    throw new \Exception(__('a_campaigns.campaign payout is not done yet'));
                }

                $this->campaignRepo->refund($campaign->id);
            }

            $this->campaignStatusRepo->changeHistory($campaign, $status);

            DB::commit();
        try {

        } catch(\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('a_campaigns.status updated successfully'));
    }

    /**
     * Top up the campaign
     */
    public function topUp(TopUp $request, $id)
    {
        $data = $request->only('topup_amount');

        DB::beginTransaction();

        $admin    = auth()->user();
        $campaign = $this->campaignRepo->findOrFail($id);

        try {
            $this->campaignRepo->participate($id, $admin->id, $data['topup_amount'], true);

            if ($campaign->current_fund >= $campaign->fund_size) {
                $this->campaignRepo->close($campaign->id);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
        
        return back()->with('success', __('a_campaigns.top up successfully'));
    }

    /**
     * Set the actual roi
     */
    public function updateActualRoi(UpdateActualRoi $request, $id)
    {
        $data = $request->only('actual_roi_rate');

        $campaign = $this->campaignRepo->findOrFail($id);

        DB::beginTransaction();

        try {
            $campaign->update([
                'actual_roi_rate' => percentage($data['actual_roi_rate'], 4),
                // 'payout_date'     => now()->addDay()->toDateString(),
            ]);

            // $completedStatus = $this->campaignStatusRepo->findBySlug(CampaignStatusContract::COMPLETED);
            // $this->campaignStatusRepo->changeHistory($campaign, $completedStatus);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('admin.campaign.edit', ['campaign' => $id])->with('success', __('a_campaigns.campaign is completed'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function getParticipants(Request $request, $id)
    {
        $campaign     = $this->campaignRepo->findOrFail($id);
        $participants = $this->participantQuery
            ->setParameters($request->all())
            ->paginate();

        $totalMemberParticipatedMember = $campaign->participants->where('is_admin', false)->sum('amount');

        return view('campaign::admin.participants', compact('campaign', 'participants', 'totalMemberParticipatedMember'));
    }
}
