<?php

namespace Modules\Campaign\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaign\Queries\Admin\CampaignQuery;
use Modules\Campaign\Queries\Admin\ParticipantQuery;
use QueryBuilder\Concerns\CanExportTrait;

class CampaignExportController extends Controller
{
    use CanExportTrait;

    /**
     * Class constructor
     */
    public function __construct(
        CampaignQuery $campaignQuery,
        ParticipantQuery $participantQuery
    ) {
        $this->middleware('permission:admin_campaign_export')->only('export');
        $this->middleware('permission:admin_campaign_participant_export')->only('exportParticipants');

        $this->campaignQuery      = $campaignQuery;
        $this->participantQuery   = $participantQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->campaignQuery->setParameters($request->all()), 'campaigns_' . now() . '.xlsx', auth()->user(), 'Export Campaigns on ' . now());
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function exportParticipants(Request $request, $id)
    {
        return $this->exportReport($this->participantQuery->setParameters($request->all()), 'participants_' . now() . '.xlsx', auth()->user(), 'Export Participants on ' . now());
    }
}
