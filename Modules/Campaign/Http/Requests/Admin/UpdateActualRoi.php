<?php

namespace Modules\Campaign\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;

class UpdateActualRoi extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'actual_roi_rate' => [
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'actual_roi_rate' => __('a_edit_campaign.actual roi'),
        ];
    }
}
