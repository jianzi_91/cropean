<?php

namespace Modules\Campaign\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;

class TopUp extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'topup_amount' => [
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $campaignRepo = resolve(CampaignContract::class);
        $campaign  = $campaignRepo->find(request()->campaignId);
        
        $validator->after(function ($validator) use ($campaign) {
            $topupable = bcsub($campaign->fund_size, $campaign->current_fund, 2);

            if ($this->topup_amount > $topupable) {
                $validator->errors()->add('topup_amount', __('a_campaigns.the top up amount exceeded the fund size'));
            }
        });
    }
}
