<?php

namespace Modules\Campaign\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Campaign\Http\Rules\CutoffDateUpdate;
use Modules\Campaign\Http\Rules\FundSizeLimit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required',
            'fund_size'      => [
                'required',
                'gt:0',
                'gte:participate_amount_minimum',
                'numeric',
                new FundSizeLimit(request()->campaign),
            ],
            'cutoff_date'    => [
                new CutoffDateUpdate(request()->campaign),
            ],
            'vesting_period' => 'required',
            'estimate_roi_rate_min' => [
                'bail',
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
            'estimate_roi_rate_max' => [
                'bail',
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
            'participate_amount_minimum' => [
                'bail',
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
            'participate_amount_multiple' => [
                'bail',
                'required',
                new MaxDecimalPlaces(2),
            ],
            'participate_amount_maximum' => [
                'bail',
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
            ],
            'published_at' => [
                'nullable',
                'before:cutoff_date',
                'after:today',
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'                        => __('a_create_campaign.campaign name'),
            'fund_size'                   => __('a_create_campaign.campaign fund size usd'),
            'cutoff_date'                 => __('a_create_campaign.campaign cutoff date'),
            'vesting_period'              => __('a_create_campaign.vesting period'),
            'estimate_roi_rate_min'       => __('a_create_campaign.estimated roi min %'),
            'estimate_roi_rate_max'       => __('a_create_campaign.estimated roi max %'),
            'participate_amount_minimum'  => __('a_create_campaign.minimum participation amount usd'),
            'participate_amount_multiple' => __('a_create_campaign.multiple amount to participate usd'),
            'participate_amount_maximum'  => __('a_create_campaign.maximum amount in % equity to participate'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            if (!empty($this->vesting_period)) {
                $vestingPeriod    = date_range_extract($this->vesting_period);
                $vestingStartDate = $vestingPeriod[0]->toDateString();
                $vestingEndDate   = empty($vestingPeriod[1]) ? $vestingPeriod[0]->toDateString() : $vestingPeriod[1]->toDateString();

                if ($vestingEndDate < $vestingStartDate) {
                    $validator->errors()->add('vesting_period', __('a_create_campaign.vesting end date should be later than start date'));
                }

                if ($vestingEndDate == $vestingStartDate) {
                    $validator->errors()->add('vesting_period', __('a_create_campaign.vesting period should be at least a day apart'));
                }

                if ($vestingStartDate <= $this->cutoff_date) {
                    $validator->errors()->add('cutoff_date', __('a_create_campaign.cutoff date cannot be later than vesting start date'));
                }

                if ($vestingStartDate <= now()) {
                    $validator->errors()->add('vesting_period', __('a_create_campaign.vesting start date should be later than today'));
                }
            }
        });
    }
}
