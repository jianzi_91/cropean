<?php

namespace Modules\Campaign\Http\Requests\Member;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Campaign\Contracts\CampaignContract;
use Modules\Campaign\Http\Rules\EquityLimit;
use Modules\Campaign\Http\Rules\ParticipationMinimum;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Participate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'raw_amount' => [
                'required',
                'gt:0',
                new MaxDecimalPlaces(2),
                new EnoughCredit(BankCreditTypeContract::USDT_ERC20, auth()->user()->id),
                new ParticipationMinimum(request()->campaign_id),
                new EquityLimit(request()->campaign_id),
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch(),
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $campaignRepo     = resolve(CampaignContract::class);
        $exchangeRateRepo = resolve(ExchangeRateContract::class);

        $campaign = $campaignRepo->find(request()->campaign_id);
        $base     = $exchangeRateRepo->toBaseCurrency(BankCreditTypeContract::USDT_ERC20, $this->raw_amount);

        $validator->after(function ($validator) use ($campaign, $base) {

            if ($campaign->participate_amount_multiple != 0) {
                $modulo = bcmod($base, $campaign->participate_amount_multiple, 2);
                if ($modulo != 0) {
                    $validator->errors()->add('raw_amount', __('m_campaigns.the amount is not multiple of :multiple', ['multiple' => amount_format($campaign->participate_amount_multiple, 2)]));
                }
            }

            $currentCampaignAmount = $campaign->participants->sum('amount');
            $total                 = bcadd($base, $currentCampaignAmount, 2);
            if ($total > $campaign->fund_size) {
                $validator->errors()->add('raw_amount', __('m_campaigns.amount exceeded campaign fund size'));
            }
        });
    }
}
