<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'name'     => 'Campaign',
    'bindings' => [
        Modules\Campaign\Contracts\CampaignContract::class       => Modules\Campaign\Repositories\CampaignRepository::class,
        Modules\Campaign\Contracts\CampaignStatusContract::class => Modules\Campaign\Repositories\CampaignStatusRepository::class,
    ],
];
