<?php

namespace Modules\Tree\Queries;

use Modules\Tree\Models\SponsorTree;
use QueryBuilder\QueryBuilder;

class SponsorTreeQuery extends QueryBuilder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return SponsorTree::join('users', 'users.id', '=', 'sponsor_trees.user_id')
            ->where('is_system_generated', 0);
    }
}
