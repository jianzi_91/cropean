<?php

namespace Modules\Tree\Queries\Member;

use Illuminate\Support\Facades\DB;
use Modules\Tree\Queries\SponsorTreeQuery as BaseQuery;

class SponsorTreeDownlineQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'level' => [
            'filter' => 'less_than_equal',
            'table'  => 'sponsor_trees'
        ],
        'lft' => [
            'filter' => 'greater_than_equal',
            'table'  => 'sponsor_trees'
        ],
        'rgt' => [
            'filter' => 'less_than_equal',
            'table'  => 'sponsor_trees'
        ]
    ];

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function beforeBuild()
    {
        $this->builder
            ->select(
                [
                'sponsor_trees.*',
                'users.member_id',
                'users.username',
                'users.name',
                'users.email',
                'users.rank_id',
                'users.goldmine_rank_id',
                DB::raw('(sponsor_trees.rgt-sponsor_trees.lft-1)/2 as children_count')

            ]
            );
    }

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function afterBuild()
    {
        $this->builder->orderBy('lft', 'asc')
            ->orderBy('level', 'asc');
    }
}
