<?php

namespace Modules\Tree\Queries\Member;

use Illuminate\Support\Facades\DB;
use Modules\Tree\Queries\PlacementTreeQuery as BaseQuery;

class PlacementTreeDownlineQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'level' => [
            'filter' => 'less_than_equal',
            'table'  => 'placement_trees'
        ],
        'lft' => [
            'filter' => 'greater_than_equal',
            'table'  => 'placement_trees'
        ],
        'rgt' => [
            'filter' => 'less_than_equal',
            'table'  => 'placement_trees'
        ]
    ];

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function beforeBuild()
    {
        $this->builder
            ->select(
            [
                'placement_trees.*',
                'users.member_id',
                'users.username',
                DB::raw('(placement_trees.rgt-placement_trees.lft-1)/2 as children_count')

            ]
        );
    }

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function afterBuild()
    {
        $this->builder->orderBy('lft', 'asc')
            ->orderBy('level', 'asc');
    }
}
