<?php

namespace Modules\Tree\Queries;

use Modules\Tree\Models\PlacementTree;
use QueryBuilder\QueryBuilder;

class PlacementTreeQuery extends QueryBuilder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return PlacementTree::join('users', 'users.id', '=', 'placement_trees.user_id')
            ->where('is_system_generated', 0);
    }
}
