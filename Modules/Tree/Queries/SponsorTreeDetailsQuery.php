<?php

namespace Modules\Tree\Queries;

use Affiliate\Commission\Concerns\HasNestedSet;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountStatementContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Rank\Models\GoldmineRank;
use Modules\Rank\Models\Rank;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\User\Models\User;

class SponsorTreeDetailsQuery
{
    use HasNestedSet;

    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query($userId)
    {
        $creditTypes = resolve(BankCreditTypeContract::class)->getModel()->whereIn('name', [
            BankCreditTypeContract::CAPITAL_CREDIT,
            BankCreditTypeContract::ROLL_OVER_CREDIT
        ])->pluck('name', 'id')->toArray();

        $query   = [];
        foreach ($creditTypes as $creditTypeId => $creditTypeName) {
            $query[] = DB::raw("sum(if(bank_account_credits.bank_credit_type_id = $creditTypeId, bank_account_credits.balance, 0)) as {$creditTypeName}_key");
        }

        $user = User::join('sponsor_trees', 'users.id', '=', 'sponsor_trees.user_id')
            ->crossJoin('bank_credit_types')
            ->join('bank_accounts', 'bank_accounts.reference_id', '=', 'users.id')
            ->join('bank_account_credits', function ($join) {
                $join->on('bank_credit_types.id', '=', 'bank_account_credits.bank_credit_type_id');
                $join->on('bank_accounts.id', '=', 'bank_account_credits.bank_account_id');
            })
            ->whereIn('bank_credit_types.id', array_keys($creditTypes))
            ->selectRaw('sponsor_trees.*,
                    users.username,
                    users.email,
                    users.member_id,
                    users.name,
                    users.rank_id,
                    users.goldmine_rank_id,
                    users.user_status_id,
                   (rgt-lft-1)/2 as children_count,' . implode(', ', $query)
            )
            ->where('users.id', $userId)
            ->groupby('sponsor_trees.id', 'sponsor_trees.user_id', 'sponsor_trees.level', 'sponsor_trees.lft', 'sponsor_trees.rgt')
            ->first();

        $bankCreditStatementRepository = resolve(BankAccountStatementContract::class);

        $exchangeRates = resolve(ExchangeRateContract::class)->getModel()
            ->whereIn('currency', [
                BankCreditTypeContract::CAPITAL_CREDIT,
                BankCreditTypeContract::ROLL_OVER_CREDIT
            ])
            ->pluck('sell_rate', 'currency')
            ->toArray();

        $downline = $bankCreditStatementRepository->getDownlineTotalBalance($user->user_id, $creditTypes, false);

        $ranks = $this->downlineRankCount($userId)->toArray();

        return [
            'user_id'           => $user->user_id,
            'email'             => $user->email,
            'username'          => $user->username,
            'name'              => $user->name,
            'member_id'         => $user->member_id,
            'account_status'    => $user->accountStatus ? $user->accountStatus->name : "-",
            'leader_bonus_rank' => $user->rank ? $user->rank->name : "-",
            'goldmine_rank'     => $user->goldmineRank ? $user->goldmineRank->name : "-",
            'rank_id'           => $user->rank_id ?? 0,
            'goldmine_rank_id'  => $user->goldmine_rank_id ?? 0,
            'children_count'    => ($user->rgt - $user->lft - 1) / 2,
            'personal_equity'   => amount_format(get_total_equity($exchangeRates, $user->capital_credit_key ?? 0, $user->roll_over_credit_key ?? 0)),
            'group_equity'      => amount_format(get_total_equity($exchangeRates, $downline->capital_credit_key ?? 0, $downline->roll_over_credit_key ?? 0)),
            'goldmine_ranks'     => 'A1 - ' . $ranks['A1'] . ', A2 - ' . $ranks['A2'] . ', A3 - ' . $ranks['A3'] . ', A4 - ' . $ranks['A4'],
            'leader_ranks'       => 'V1 - ' . $ranks['V1'] . ', V2 - ' . $ranks['V2'] . ', V3 - ' . $ranks['V3'] . ', V4 - ' . $ranks['V4'],
        ];
    }

    public function downlineRankCount($userId)
    {
        $goldmineRanks = GoldmineRank::all()->pluck('name', 'id');

        foreach ($goldmineRanks as $rankId => $rankName) {
            $selection[] = "COALESCE(SUM(IF(downline_users.goldmine_rank_id = $rankId, 1, 0)), 0) AS $rankName";
        }

        $leaderRanks = Rank::all()->pluck('name', 'id');

        foreach ($leaderRanks as $rankId => $rankName) {
            $ldrRanks[] = "COALESCE(SUM(IF(downline_users.rank_id = $rankId, 1, 0)), 0) AS $rankName";
        }
        
        $query = User::join('sponsor_trees', 'sponsor_trees.user_id', '=', 'users.id')
            ->join('sponsor_trees as downline', function ($query) {
                $query->on('downline.lft', '>', 'sponsor_trees.lft')
                    ->on('downline.rgt', '<', 'sponsor_trees.rgt');
            })
            ->join('users as downline_users', 'downline_users.id', '=', 'downline.user_id')
            ->selectRaw(implode(', ', $selection))
            ->selectRaw(implode(', ', $ldrRanks))
            ->where('users.id', $userId)
            ->first();

        return $query;
    }
}
