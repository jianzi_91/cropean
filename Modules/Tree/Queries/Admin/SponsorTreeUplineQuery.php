<?php

namespace Modules\Tree\Queries\Admin;

use Modules\Tree\Queries\SponsorTreeQuery as BaseQuery;

class SponsorTreeUplineQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'lft' => [
            'filter' => 'less_than_equal',
            'table'  => 'sponsor_trees'
        ],
        'rgt' => [
            'filter' => 'greater_than_equal',
            'table'  => 'sponsor_trees'
        ]
    ];

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function beforeBuild()
    {
    }

    /**
     *
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::beforeBuild()
     */
    public function afterBuild()
    {
        $this->builder->orderBy('lft', 'desc')
                    ->orderBy('level', 'desc');
    }
}
