<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('members/{id}/network', ['as' => 'admin.management.members.network.edit', 'uses' => 'Admin\Management\Member\TreeController@edit']);
        Route::put('members/{id}/sponsor', ['as' => 'admin.management.members.sponsor.update', 'uses' => 'Admin\Management\Member\TreeController@updateSponsor']);
        Route::put('members/{id}/referrer', ['as' => 'admin.management.members.referrer.update', 'uses' => 'Admin\Management\Member\TreeController@updateReferrer']);

        Route::group(['prefix' => 'sponsors'], function () {
            Route::get('', ['as' => 'admin.sponsor.index', 'uses' => 'Admin\SponsorTreeController@index']);
            Route::get('uplines', ['as' => 'admin.sponsor.upline', 'uses' => 'Admin\SponsorTreeController@upline']);
            Route::get('children/{user_id}', ['as' => 'admin.sponsor.children', 'uses' => 'Admin\SponsorTreeController@children']);
            Route::get('{user_id}/details', ['as' => 'admin.sponsor.details', 'uses' => 'Admin\SponsorTreeController@details']);
        });
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::group(['prefix' => 'sponsors'], function () {
            Route::get('', ['as' => 'member.sponsor.index', 'uses' => 'Member\SponsorTreeController@index']);
            Route::get('children/{user_id}', ['as' => 'member.sponsor.children', 'uses' => 'Member\SponsorTreeController@children']);
            Route::get('{user_id}/details', ['as' => 'member.sponsor.details', 'uses' => 'Member\SponsorTreeController@details']);
        });

        /*Route::group(['prefix' => 'placements'], function () {
            Route::get('', ['as' => 'member.placement.index', 'uses' => 'Member\PlacementTreeController@index']);
            Route::get('children/{user_id}', ['as' => 'member.placement.children', 'uses' => 'Member\PlacementTreeController@children']);
            Route::get('{user_id}/details', ['as' => 'member.placement.details', 'uses' => 'Member\PlacementTreeController@details']);
        });*/
    });
});
