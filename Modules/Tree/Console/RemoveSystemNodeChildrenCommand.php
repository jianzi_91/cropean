<?php

namespace Modules\Tree\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Models\SponsorTree;
use Modules\Tree\Repositories\SponsorTreeRepository;
use Modules\User\Models\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RemoveSystemNodeChildrenCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tree:clean_system_root';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all children of system root';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var SponsorTreeRepository $sponsorTreeRepo */
        $sponsorTreeRepo = resolve(SponsorTreeContract::class);
        $systemRootUser = User::where('username','systemroot')->first();
        $rootUser = User::where('username','root')->first();
        if($systemRootUser) {
            $systemRootNode = SponsorTree::where('user_id',$systemRootUser->id)->first();
            $systemRootNode->delete();
            $sponsorTreeRepo->add(['user_id' => $systemRootUser->id],$rootUser->id);
        }
    }
}
