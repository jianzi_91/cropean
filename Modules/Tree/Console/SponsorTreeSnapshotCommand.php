<?php

namespace Modules\Tree\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;

class SponsorTreeSnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'snapshot:sponsor_tree {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the sponsor tree.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = new Carbon($this->argument('date'));

        // This is to prevent multiple insert into sponsor tree, prevent the concurrency issue in tree
        Setting::lockForUpdate()->where('name', 'sponsor_tree_lock')->first();

        DB::statement("TRUNCATE sponsor_tree_snapshots");
        DB::statement("INSERT INTO sponsor_tree_snapshots SELECT * FROM sponsor_trees");
        DB::statement("INSERT INTO sponsor_tree_snapshot_history SELECT *, '{$now}' FROM sponsor_trees");
    }
}
