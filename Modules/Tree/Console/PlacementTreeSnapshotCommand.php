<?php

namespace Modules\Tree\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;

class PlacementTreeSnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'snapshot:placement_tree';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the placement tree.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();

        // This is to prevent multiple insert into sponsor tree, prevent the concurrency issue in tree
        Setting::lockForUpdate()->where('name', 'placement.tree.lock')->first();

        DB::statement("TRUNCATE placement_tree_snapshots");
        DB::statement("INSERT INTO placement_tree_snapshots SELECT * FROM placement_trees");
        DB::statement("INSERT INTO placement_tree_snapshot_history SELECT *, '{$now}' FROM placement_trees");
    }
}
