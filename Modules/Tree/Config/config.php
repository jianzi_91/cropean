<?php

return
 [
    'name' => 'Tree',

    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'bindings' => [
        'Modules\Tree\Contracts\SponsorTreeContract'                      => 'Modules\Tree\Repositories\SponsorTreeRepository',
        'Modules\Tree\Contracts\PlacementTreeContract'                    => 'Modules\Tree\Repositories\PlacementTreeRepository',
        'Modules\Tree\Contracts\SponsorTreeHistoryContract'               => 'Modules\Tree\Repositories\SponsorTreeHistoryRepository',
        \Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract::class => \Modules\Tree\Repositories\SponsorTreeSnapshotHistoryRepository::class,
    ],
    /*
    |--------------------------------------------------------------------------
    | blocked as sponsor
    |
    |--------------------------------------------------------------------------
    */
    'blocked_sponsor_username' => [
        'root'
    ],

     'display_level' => env('TREE_DISPLAY_LEVEL', 2),
];
