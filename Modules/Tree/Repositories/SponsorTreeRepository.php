<?php

namespace Modules\Tree\Repositories;

use Modules\Setting\Contracts\SettingContract;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Models\SponsorTree;
use Modules\Tree\Repositories\Concerns\HasFancyTree;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Plus65\Utility\Contracts\SnapshotContract;

class SponsorTreeRepository extends Repository implements SponsorTreeContract
{
    use HasSlug, HasFancyTree;

    /**
     * The tree array
     * @var unknown
     */
    protected $tree;

    /**
     * The snapshot object
     * @var unknown
     */
    protected $snapshot;

    /**
     * The setting object
     * @var unknown
     */
    protected $setting;

    /**
     * Class constructor
     * @param SponsorTree $model
     * @param SnapshotContract $snapshot
     * @param SettingContract $settingContract
     */
    public function __construct(SponsorTree $model, SnapshotContract $snapshot, SettingContract $settingContract)
    {
        $this->snapshot = $snapshot;
        $this->slug     = 'user_id';
        $this->setting  = $settingContract;
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::add()
     */
    public function add(array $attributes, $parentId = null)
    {
        if ($parentId) {
            $node = $this->model->where('user_id', $parentId)->firstOrFail();
        }
        $child = $this->model->create($attributes);

        if ($parentId) {
            return $child->makeChildOf($node);
        }
        $child->makeRoot();

        return $child;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::downlines()
     */
    public function downlines($userId, $withSelf = false, array $with = [], $select = ['*'])
    {
        if ($this->tree) {
            $node = $this->getNodeFromTree($userId);
            if (!$node) {
                throw new \Exception('Node not found');
            }
            return collect($this->getNodesFromTree($node, $withSelf));
        }
        $node         = $this->model->where('user_id', $userId)->firstOrFail();
        $lftCondition = $withSelf ? '>=' : '>';
        $rgtCondition = $withSelf ? '<=' : '<';
        return $this->model->where('lft', $lftCondition, $node->lft)
                            ->where('rgt', $rgtCondition, $node->rgt)
                            ->with($with)
                            ->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::uplines()
     */
    public function uplines($userId, $withSelf = false, array $with = [], $select = ['*'])
    {
        if ($this->tree) {
            $node = $this->getNodeFromTree($userId);
            if (!$node) {
                throw new \Exception('Node not found');
            }
            return collect($this->getNodesFromTree($node, $withSelf, true));
        }
        $node         = $this->model->where('user_id', $userId)->firstOrFail();
        $lftCondition = $withSelf ? '<=' : '<';
        $rgtCondition = $withSelf ? '>=' : '>';
        return $this->model->where('lft', $lftCondition, $node->lft)
                            ->where('rgt', $rgtCondition, $node->rgt)
                            ->with($with)
                            ->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::directUpline()
     */
    public function directUpline($userId, array $with = [], $select = ['*'])
    {
        if ($this->tree) {
            $node = $this->getNodeFromTree($userId);
            if (!$node) {
                throw new \Exception('Node not found');
            }
            return collect(array_filter(
                $this->tree,
                        function ($v) use ($node) {
                            return $v['id'] == $node['parent_id'];
                        }
            ));
        } else {
            $node = $this->model->where('user_id', $userId)->firstOrFail();
            return $this->model->find($node->parent_id, $select);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::directDownlines()
     */
    public function directDownlines($userId, array $with = [], $select = ['*'])
    {
        if ($this->tree) {
            $node = $this->getNodeFromTree($userId);
            if (!$node) {
                throw new \Exception('Node not found');
            }
            return collect(array_filter(
                $this->tree,
                function ($v) use ($node) {
                    return $v['parent_id'] == $node['id'];
                }
            ));
        } else {
            $node = $this->model->where('user_id', $userId)->firstOrFail();
            return $this->model->where('parent_id', $node->id)->with($with)->get($select);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::loadFromSnapshot()
     */
    public function loadFromSnapshot($key)
    {
        $data = $this->snapshot->get($key);
        if ($data) {
            $this->tree = $data->toArray();
        }
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::loadFromArray()
     */
    public function loadFromArray(array $items)
    {
        $this->tree = $items;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::move()
     */
    public function move($fromId, $toId)
    {
        $this->setting->getModel()::lockForUpdate()->where('name', 'sponsor_tree_lock')->first();

        $fromNode = $this->model->where('user_id', $fromId)->firstOrFail();
        $toNode   = $this->model->where('user_id', $toId)->firstOrFail();

        return $fromNode->makeChildOf($toNode);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::delete()
     */
    public function delete($userId)
    {
        return $this->model->where('user_id', $userId)->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::deleteDownlines()
     */
    public function deleteDownlines($userId, $withSelf = false)
    {
        $node         = $this->model->where('user_id', $userId)->firstOrFail();
        $lftCondition = $withSelf ? '>=' : '>';
        $rgtCondition = $withSelf ? '<=' : '<';

        // set parent to null
        $this->model->where('lft', $lftCondition, $node->lft)
                    ->where('rgt', $rgtCondition, $node->rgt)
                    ->update(['parent_id' => null]);

        return $this->model->where('lft', $lftCondition, $node->lft)
                           ->where('rgt', $rgtCondition, $node->rgt)
                           ->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::getNodesByLevel()
     */
    public function getNodesByLevel($level, array $with = [], $select = ['*'])
    {
        if ($this->tree) {
            return collect(array_filter(
                $this->tree,
                function ($v) use ($level) {
                    return $v['level'] == $level;
                }
            ));
        } else {
            return $this->model->where('level', $level)->with($with)->get($select);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Tree\Contracts\TreeContract::all()
     */
    public function all(array $with = [], $perPage = 0, $select = ['*'])
    {
        $results = $this->model->with($with)->orderBy('level')->orderBy('lft');
        return $perPage ? $results->paginate($perPage, $select) : $results->get($select);
    }

    /**
     * {@inheritdoc}
     *
     * @see Plus65\SctNetwork\Contracts\SponsorTreeContract::nodeHasChild()
     */
    public function nodeHasChild(int $parentUserId, int $childUserId)
    {
        $parentNode = $this->model->where('user_id', $parentUserId)->first();
        $childNode  = $this->model->where('user_id', $childUserId)->first();

        if (!$parentNode || !$childNode) {
            return false;
        }

        return $childNode->lft > $parentNode->lft && $childNode->rgt < $parentNode->rgt;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Plus65\SctNetwork\Contracts\SponsorTreeContract::nodeHasParent()
     */
    public function nodeHasParent(int $parentUserId, int $childUserId)
    {
        $parentNode = $this->model->where('user_id', $parentUserId)->first();
        $childNode  = $this->model->where('user_id', $childUserId)->first();

        if (!$parentNode || !$childNode) {
            return false;
        }

        return $childNode->lft < $parentNode->lft && $childNode->rgt > $parentNode->rgt;
    }

    /**
     * {@inheritdoc}
     *
     * @see Plus65\SctNetwork\Contracts\SponsorTreeContract::downlinesHiearchy()
     */
    public function downlinesHiearchy($userId, array $select = ['*'], $maxLevel = null)
    {
        $node = $this->model->where('user_id', $userId)->firstOrFail();

        $table   = $this->model->getTable();
        $prepare = $this->model->where('lft', '>', $node->lft)
                        ->join('users', $table . '.user_id', '=', 'users.id')
                        ->where($table . '.rgt', '<', $node->rgt);

        if ($maxLevel) {
            $prepare->where($table . '.level', '<=', $node->level + $maxLevel);
        }

        $downlines = $prepare->get($select);

        return $downlines->toHierarchy();
    }

    /**
     * Get node from tree
     * @param unknown $userId
     * @return array
     */
    protected function getNodeFromTree($userId)
    {
        $node = array_filter(
            $this->tree,
                    function ($v) use ($userId) {
                        return $v['user_id'] == $userId;
                    }
        );
        if ($node) {
            return array_shift($node);
        }

        return [];
    }

    /**
     * Get nodes from tree dynamically
     * @param unknown $node
     * @param string $withSelf
     * @param string $upDown
     * @return array
     */
    protected function getNodesFromTree($node, $withSelf = false, $upDown = false)
    {
        return array_filter(
            $this->tree,
            function ($v) use ($node, $withSelf, $upDown) {
                if ($upDown) {
                    if ($withSelf) {
                        return $v['lft'] <= $node['lft'] && $v['rgt'] >= $node['rgt'];
                    }
                    return $v['lft'] < $node['lft'] && $v['rgt'] > $node['rgt'];
                }
                if ($withSelf) {
                    return $v['lft'] >= $node['lft'] && $v['rgt'] <= $node['rgt'];
                }
                return $v['lft'] > $node['lft'] && $v['rgt'] < $node['rgt'];
            }
        );
    }
}
