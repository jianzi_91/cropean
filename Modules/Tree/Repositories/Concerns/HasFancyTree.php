<?php

namespace Modules\Tree\Repositories\Concerns;

trait HasFancyTree
{
    public function buildFancyTree($descendants, $parentId)
    {
        $tree = $this->buildTree($descendants, $parentId);

        return $tree;
    }

    private function buildTree($elements, $parentId)
    {
        $branch = [];

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }

                $branch[] = $this->transform($element);
            }
        }

        return $branch;
    }

    private function transform($child)
    {
        $data = [
            'id'               => $child['id'],
            'parent_id'        => $child['parent_id'],
            'user_id'          => $child['user_id'],
            'username'         => $child['username'],
            'name'             => $child['name'] ?? '',
            'email'            => $child['email'] ?? '',
            'goldmine_rank_id' => $child['goldmine_rank_id'] ?? '',
            'member_id'        => $child['member_id'],
            'title'            => "({$child['member_id']})",
            'rank_id'          => isset($child['rank_id']) && $child['rank_id'] > 0 ? ($child['rank_id'] + 4) : (isset($child['goldmine_rank_id']) && $child['goldmine_rank_id'] > 0 ? $child['goldmine_rank_id'] : 0),
            'children_count'   => bcmul($child['children_count'], 1)
        ];

        if (isset($child['children'])) {
            $data['children'] = $child['children'];
        }

        return $data;
    }
}
