<?php

namespace Modules\Tree\Repositories;

use Modules\Tree\Contracts\SponsorTreeHistoryContract;
use Modules\Tree\Models\SponsorTreeHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SponsorTreeHistoryRepository extends Repository implements SponsorTreeHistoryContract
{
    use HasCrud;

    /**
     * Class constructor
     * @param PlacementTree $model
     * @param SnapshotContract $snapshot
     */
    public function __construct(SponsorTreeHistory $model)
    {
        $this->slug = 'user_id';
        parent::__construct($model);
    }
}
