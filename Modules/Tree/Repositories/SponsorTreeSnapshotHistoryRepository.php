<?php

namespace Modules\Tree\Repositories;

use Carbon\Carbon;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Modules\Tree\Models\SponsorTreeSnapshotHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SponsorTreeSnapshotHistoryRepository extends Repository implements SponsorTreeSnapshotHistoryContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param SponsorTreeSnapshotHistory $model
     */
    public function __construct(SponsorTreeSnapshotHistory $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract::getSnapshot()
     */
    public function getSnapshot(Carbon $date)
    {
        $from = $date->copy()->startOfDay();
        $to   = $date->copy()->endOfDay();

        return $this->model
            ->whereBetween('run_date', [$from, $to])
            ->get([
                'id',
                'user_id',
                'lft',
                'rgt',
                'parent_id',
                'level',
            ])
            ->toArray();
    }
}
