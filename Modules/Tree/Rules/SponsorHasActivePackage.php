<?php

namespace Modules\Tree\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Product\Models\ProductPurchase;

class SponsorHasActivePackage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $userWithProducts = ProductPurchase::join('users', 'product_purchases.user_id', '=', 'users.id')
                ->where(function ($query) use ($value) {
                    $query->where('users.username', $value)
                        ->orWhere('users.member_id', $value);
                })
            ->whereNull('product_purchases.expiration_date')
            ->count();

        if ($userWithProducts != 0) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('m_register.sponsor id must have an active package');
    }
}
