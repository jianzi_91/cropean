<?php

namespace Modules\Tree\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Concerns\HasHistory;

class SponsorTreeHistory extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'sponsor_id',
        'user_id',
        'remarks',
        'is_current',
        'updated_by',
    ];

    /**
     * This model's relation to user status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_id');
    }

    /**
     * Local scope for getting current status.
     *
     * @param unknown $query
     * @param unknown $current
     *
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where($this->getTable() . '.is_current', $current);
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
