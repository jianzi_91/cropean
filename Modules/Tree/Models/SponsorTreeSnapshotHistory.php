<?php

namespace Modules\Tree\Models;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SponsorTreeSnapshotHistory extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'sponsor_tree_snapshot_history';
}
