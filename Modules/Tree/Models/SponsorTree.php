<?php

namespace Modules\Tree\Models;

use Baum\Node;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SponsorTree extends Node
{
    use UserRelation, SoftDeletes;

    // 'parent_id' column name
    protected $parentColumn = 'parent_id';

    // 'lft' column name
    protected $leftColumn = 'lft';

    // 'rgt' column name
    protected $rightColumn = 'rgt';

    // 'depth' column name
    protected $depthColumn = 'level';

    // guard attributes from mass-assignment
    protected $guarded = ['id', 'parent_id', 'lft', 'rgt', 'level'];

    protected $fillable = ['user_id'];
}
