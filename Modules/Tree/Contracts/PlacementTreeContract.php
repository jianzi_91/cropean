<?php

namespace Modules\Tree\Contracts;

interface PlacementTreeContract extends TreeContract
{
    /**
     * Get downlines in hiearchy format
     *
     * @param unknown  $userId
     * @param array    $select
     * @param int/null $maxLevel
     */
    public function downlinesHiearchy($userId, array $select = ['*'], $maxLevel = null);

    /**
     * Load data from snapshot module
     * @param unknown $key
     * @return self
     */
    public function loadFromSnapshot($key);

    /**
     * Load data from array
     * @param array $items
     * @return self
     */
    public function loadFromArray(array $items);

    /**
     * Build fancy tree
     */
    public function buildFancyTree($descendants, $parentId);
}
