<?php

namespace Modules\Tree\Contracts;

use Carbon\Carbon;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface SponsorTreeSnapshotHistoryContract extends CrudContract
{
    /**
     * Get snapshot at date.
     *
     * @param Carbon $date
     * @return mixed
     */
    public function getSnapshot(Carbon $date);
}
