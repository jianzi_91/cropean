<?php

namespace Modules\Tree\Contracts;

use Illuminate\Support\Collection;
use Plus65\Modules\Trees\Models\SponsorTree;

interface TreeContract
{
    /**
     * Add user to node
     * @param array $attributes
     * @param unknown $parentId
     * @return SponsorTree
     */
    public function add(array $attributes, $parentId = null);

    /**
     * Get downlines
     * @param unknown $userId
     * @param string $withSelf
     * @param array $with
     * @param array $select
     * @return Collection
     */
    public function downlines($userId, $withSelf = false, array $with = [], $select = ['*']);

    /**
     * Get uplines
     * @param unknown $userId
     * @param string $withSelf
     * @param array $with
     * @param array $select
     * @return Collection
     */
    public function uplines($userId, $withSelf = false, array $with = [], $select = ['*']);

    /**
     * Get immediate upline
     * @param unknown $userId
     * @param array $with
     * @param array $select
     * @return SponsorTree
     */
    public function directUpline($userId, array $with = [], $select = ['*']);

    /**
     * Get immediate downlines
     * @param unknown $userId
     * @param array $with
     * @param array $select
     * @return Collection
     */
    public function directDownlines($userId, array $with = [], $select = ['*']);

    /**
     * Move node to another node
     * @param unknown $fromId
     * @param unknown $toId
     * @return boolean
     */
    public function move($fromId, $toId);

    /**
     * Delete a node from the tree
     * @param unknown $userId
     * @return integer
     */
    public function delete($userId);

    /**
     * Delete node downlines
     * @param unknown $userId
     * @param string $withSelf
     * @return integer
     */
    public function deleteDownlines($userId, $withSelf = false);

    /**
     * Get nodes by level
     * @param unknown $level
     * @param array $with
     * @param array $select
     * @return Collection
     */
    public function getNodesByLevel($level, array $with = [], $select = ['*']);

    /**
     * Retrieve all records.
     * @param array $with
     * @param number $perPage
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function all(array $with = [], $perPage = 0, $select = ['*']);

    /**
     * Check if a specific node has a child in his downline
     *
     * @param  int $parentUserId User ID of subject node
     * @param  int $childUserId  User ID of child node with respect to subject
     * @return boolean
     */
    public function nodeHasChild(int $parentUserId, int $childUserId);

    /**
     * Check if a specific node has a parent in his upline
     *
     * @param  int $parentUserId User ID of subject node
     * @param  int $childUserId  User ID of child node with respect to subject
     * @return boolean
     */
    public function nodeHasParent(int $parentUserId, int $childUserId);
}
