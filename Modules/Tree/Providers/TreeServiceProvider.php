<?php

namespace Modules\Tree\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Tree\Console\PlacementTreeSnapshotCommand;
use Modules\Tree\Console\RemoveSystemNodeChildrenCommand;
use Modules\Tree\Console\SponsorTreeSnapshotCommand;

class TreeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    protected $commands = [
        PlacementTreeSnapshotCommand::class,
        SponsorTreeSnapshotCommand::class,
        RemoveSystemNodeChildrenCommand::class,
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->registerBindings();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->registerCommands();
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/tree');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/tree';
        }, \Config::get('view.paths')), [$sourcePath]), 'tree');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/tree');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'tree');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'tree');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * bind interfaces to implementations
     * @return void
     */
    public function registerBindings()
    {
        foreach (config('tree.bindings') as $key => $binding) {
            $this->app->bind($key, $binding);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('tree.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php',
            'tree'
        );
    }

    protected function registerCommands()
    {
        $this->commands($this->commands);
    }
}
