<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsorTreeSnapshotHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsor_tree_snapshot_history', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->datetime('created_at')->nullable();
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('lft')->index();
            $table->unsignedInteger('rgt')->index();
            $table->unsignedInteger('parent_id')->index()->nullable();
            $table->unsignedInteger('level')->index()->nullable();
            $table->datetime('run_date')->index();

            //constraints
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('parent_id')->references('id')->on('sponsor_trees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsor_tree_snapshot_history');
    }
}
