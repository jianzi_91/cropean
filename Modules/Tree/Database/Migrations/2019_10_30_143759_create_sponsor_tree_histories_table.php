<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsorTreeHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsor_tree_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('sponsor_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('sponsor_id')
                ->references('id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sponsor_tree_histories', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['sponsor_id']);
            $table->dropForeign(['updated_by']);
        });

        Schema::dropIfExists('sponsor_tree_histories');
    }
}
