<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Rbac\Models\Role;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;

class SponsorExistWithoutSystemAccount implements Rule
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userRepository        = resolve(UserContract::class);
        $this->sponsorTreeRepository = resolve(SponsorTreeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($user = $this->userRepository->findByIdentifier($value))) {
            return false;
        }

        if (in_array($user->username, config('tree.blocked_sponsor_username'))) {
            return false;
        }

        if (in_array($user->username, ['user1', 'user2', 'root']) && !(auth()->user()->role->name == Role::SYSADMIN)) {
            return false;
        }

        $sponsor = $this->sponsorTreeRepository->findBySlug($user->id);

        return $sponsor ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the :attribute is invalid');
    }
}
