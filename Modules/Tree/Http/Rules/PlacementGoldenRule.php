<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\PlacementTreeContract;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;

class PlacementGoldenRule implements Rule
{
    /**
     * The sponsor id
     *
     * @var unknown
     */
    protected $sponsorId;

    /**
     * The validation message
     *
     * @var unknown
     */
    protected $message;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * The placement tree repository
     *
     * @var unknown
     */
    protected $placementTreeRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($sponsorId)
    {
        $this->sponsorId               = $sponsorId;
        $this->userRepository          = resolve(UserContract::class);
        $this->sponsorTreeRepository   = resolve(SponsorTreeContract::class);
        $this->placementTreeRepository = resolve(PlacementTreeContract::class);
        $this->message                 = __('s_validation.the :attribute is invalid');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($placementUser = $this->userRepository->findBySlug($value))) {
            return false;
        }

        if (!($sponsorUser = $this->userRepository->findBySlug($this->sponsorId))) {
            return false;
        }

        if (!($sponsorPlacementNode = $this->placementTreeRepository->findBySlug($sponsorUser->id))) {
            $this->message = __('s_validation.sponsor id do not exist in placement tree');
            return false;
        }

        if (!($placementNode = $this->placementTreeRepository->findBySlug($placementUser->id))) {
            $this->message = __('s_validation.the placement id does not exist in placement tree');
            return false;
        }

        if (!$placementNode->isSelfOrDescendantOf($sponsorPlacementNode)) {
            $this->message = __('s_validation.the placement id must be under sponsor id');
            return false;
        }

        return true;
    }

    /**
     * Get the validation error validation.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
