<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;

class ReferrerRelation implements Rule
{
    /**
     * The from username
     *
     * @var unknown
     */
    protected $sponsorId;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * The error message
     *
     * @var unknown
     */
    protected $errorMessage;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($sponsorId)
    {
        $this->sponsorId             = $sponsorId;
        $this->userRepository        = resolve(UserContract::class);
        $this->sponsorTreeRepository = resolve(SponsorTreeContract::class);
        $this->errorMessage          = '';
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($sponsor = $this->userRepository->findByIdentifier($this->sponsorId))) {
            $this->errorMessage = __('s_validation.the :attribute not exists');
            return false;
        }

        if (!($referrer = $this->userRepository->findByIdentifier($value))) {
            $this->errorMessage = __('s_validation.the :attribute not exists');
            return false;
        }

        if ($referrer == $sponsor) {
            return true;
        }

        if ($sponsor->is_member && $referrer->is_member) {
            if (!($fromNode = $this->sponsorTreeRepository->findBySlug($sponsor->id))) {
                $this->errorMessage = __('s_validation.the :attribute not exists');
                return false;
            }

            if (!($toNode = $this->sponsorTreeRepository->findBySlug($referrer->id))) {
                $this->errorMessage = __('s_validation.the :attribute not exists');
                return false;
            }

            if ($fromNode->isDescendantOf($toNode)) {
                return true;
            }
        }

        $this->errorMessage = __('s_validation.the :attribute not related to sponsor');
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
