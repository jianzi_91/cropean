<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\PlacementTreeContract;
use Modules\User\Contracts\UserContract;

class PlacementRelation implements Rule
{
    /**
     * The from username
     *
     * @var unknown
     */
    protected $fromUsername;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $placementTreeRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($fromUsername)
    {
        $this->fromUsername            = $fromUsername;
        $this->userRepository          = resolve(UserContract::class);
        $this->placementTreeRepository = resolve(PlacementTreeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($fromUser = $this->userRepository->findBySlug($this->fromUsername))) {
            return false;
        }

        if (!($toUser = $this->userRepository->findBySlug($value))) {
            return false;
        }

        if ($fromUser->is_member) {
            if (!($fromNode = $this->placementTreeRepository->findBySlug($fromUser->id))) {
                return false;
            }

            if (!($toNode = $this->placementTreeRepository->findBySlug($toUser->id))) {
                return false;
            }

            if ($fromNode->isDescendantOf($toNode) || $fromNode->isAncestorOf($toNode)) {
                return true;
            }
        } else {
            //Return true if from user is not member
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the :attribute not related');
    }
}
