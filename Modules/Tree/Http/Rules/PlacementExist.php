<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;

class PlacementExist implements Rule
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The placement tree repository
     *
     * @var unknown
     */
    protected $placementTreeRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userRepository          = resolve(UserContract::class);
        $this->placementTreeRepository = resolve(SponsorTreeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($user = $this->userRepository->findBySlug($value))) {
            return false;
        }

        $placement = $this->placementTreeRepository->findBySlug($user->id);

        return $placement ? true : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the :attribute is invalid');
    }
}
