<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;

class SponsorRelation implements Rule
{
    /**
     * The from username
     *
     * @var unknown
     */
    protected $fromUsername;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * The error message
     *
     * @var unknown
     */
    protected $errorMessage;

    /**
     * The admin allowed
     *
     * @var unknown
     */
    protected $isAdminAllowed;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($fromUsername, $isAdminAllowed = true)
    {
        $this->fromUsername          = $fromUsername;
        $this->userRepository        = resolve(UserContract::class);
        $this->sponsorTreeRepository = resolve(SponsorTreeContract::class);
        $this->errorMessage          = '';
        $this->isAdminAllowed        = $isAdminAllowed;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($fromUser = $this->userRepository->findByIdentifier($this->fromUsername))) {
            $this->errorMessage = __('s_validation.the :attribute not exists');
            return false;
        }

        if (!($toUser = $this->userRepository->findByIdentifier($value))) {
            $this->errorMessage = __('s_validation.the :attribute not exists');
            return false;
        }

        if ($fromUser->member_id === $toUser->member_id) {
            $this->errorMessage = __('s_validation.the from member and to member cannot be same');
            return false;
        }

        if ($fromUser->is_member && $toUser->is_member) {
            if (!($fromNode = $this->sponsorTreeRepository->findBySlug($fromUser->id))) {
                $this->errorMessage = __('s_validation.the :attribute not exists');
                return false;
            }

            if (!($toNode = $this->sponsorTreeRepository->findBySlug($toUser->id))) {
                $this->errorMessage = __('s_validation.the :attribute not exists');
                return false;
            }

            if ($fromNode->isDescendantOf($toNode) || $fromNode->isAncestorOf($toNode)) {
                return true;
            }
        } else {
            //Return true if from user is not member
            if ($this->isAdminAllowed) {
                return true;
            }
            $this->errorMessage = __('s_validation.the transfer only allowed between members');
            return false;
        }

        $this->errorMessage = __('s_validation.the :attribute not related');
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
