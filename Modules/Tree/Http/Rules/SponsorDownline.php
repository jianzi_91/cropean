<?php

namespace Modules\Tree\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;

class SponsorDownline implements Rule
{
    /**
     * The from username
     *
     * @var unknown
     */
    protected $fromUsername;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($fromUsername)
    {
        $this->fromUsername          = $fromUsername;
        $this->userRepository        = resolve(UserContract::class);
        $this->sponsorTreeRepository = resolve(SponsorTreeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($fromUser = $this->userRepository->findByIdentifier($this->fromUsername))) {
            return false;
        }

        if (!($toUser = $this->userRepository->findByIdentifier($value))) {
            return false;
        }

        if (!($fromNode = $this->sponsorTreeRepository->findBySlug($fromUser->id))) {
            return false;
        }

        if (!($toNode = $this->sponsorTreeRepository->findBySlug($toUser->id))) {
            return false;
        }

        if ($toNode->isDescendantOf($fromNode)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the :attribute not related');
    }
}
