<?php

namespace Modules\Tree\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Tree\Http\Rules\SponsorExistWithoutSystemAccount;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_member_id' => ['nullable', new SponsorExistWithoutSystemAccount()]

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
