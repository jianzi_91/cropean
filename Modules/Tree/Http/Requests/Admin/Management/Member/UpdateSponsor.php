<?php

namespace Modules\Tree\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\Tree\Http\Rules\ReferrerRelation;
use Modules\Tree\Http\Rules\SponsorReferrerRelation;
use Modules\User\Http\Rules\UserIsActive;

class UpdateSponsor extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sponsor_id' => [
                'bail',
                'required',
                new SponsorExist(),
                new UserIsActive(),
                new SponsorReferrerRelation($this->referrer_id),
            ],
            'referrer_id' => [
                'bail',
                'required',
                new UserIsActive(),
                new ReferrerRelation($this->sponsor_id),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'sponsor_id' => __('a_member_management_change_network.sponsor id'),
            'referrer_id' => __('a_member_management_change_network.referrer id'),
        ];
    }
}
