<?php

namespace Modules\Tree\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\Tree\Http\Rules\ReferrerRelation;
use Modules\User\Http\Rules\UserIsActive;
use Modules\User\Contracts\UserContract;

class UpdateReferrer extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userRepo = resolve(UserContract::class);
        $user = $userRepo->find($this->id);

        return [
            'referrer_id' => [
                'bail',
                'required',
                new UserIsActive(),
                new ReferrerRelation($user->sponsor_id),
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'sponsor_id' => __('a_member_management_change_network.username'),
        ];
    }
}
