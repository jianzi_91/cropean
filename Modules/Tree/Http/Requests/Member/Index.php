<?php

namespace Modules\Tree\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Modules\Tree\Http\Rules\SponsorDownline;
use Modules\Tree\Http\Rules\SponsorExist;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_id' => ['nullable', new SponsorExist(), new SponsorDownline(Auth::user()->username)],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
