<?php

namespace Modules\Tree\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Http\Requests\Member\Index;
use Modules\Tree\Queries\Member\SponsorTreeDownlineQuery;
use Modules\Tree\Queries\SponsorTreeDetailsQuery;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Response\ApiResponse;

class SponsorTreeController extends Controller
{
    use ApiResponse;

    /**
     * Sponsor Tree repository
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * User repository
     * @var unknown
     */
    protected $userRepository;

    /**
     * The query object
     * @var unknown
     */
    protected $query;
    protected $bankCreditTypeContract;

    /**
     * The details query object
     * @var unknown
     */
    protected $sponsorTreeDetailsQuery;

    public function __construct(
        SponsorTreeContract $sponsorTreeContract,
        UserContract $userContract,
        SponsorTreeDownlineQuery $sponsorTreeQuery,
        SponsorTreeDetailsQuery $sponsorTreeDetailsQuery
    ) {
        $this->middleware('permission:member_tree_list')->only('index', 'children', 'details');

        $this->sponsorTreeRepository   = $sponsorTreeContract;
        $this->userRepository          = $userContract;
        $this->query                   = $sponsorTreeQuery;
        $this->sponsorTreeDetailsQuery = $sponsorTreeDetailsQuery;
    }

    public function index(Index $request)
    {
        $sponsorTree = [];

        $memberId = $request->member_id?: Auth::user()->member_id;

        if ($user = $this->userRepository->findByMemberId($memberId)) {
            if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
                $data = [
                    'level' => $node->level + config('tree.display_level'),
                    'lft'   => $node->lft,
                    'rgt'   => $node->rgt,
                ];
                $descendants = $this->query
                    ->setParameters($data)
                    ->get()
                    ->toArray();

                $children    = $this->sponsorTreeRepository->buildFancyTree($descendants, $node->id);
                $sponsorTree = [
                    'id'             => $node ? $node->id : $user->id,
                    'email'          => $user->email,
                    'user_id'        => $user->id,
                    'username'       => $user->username,
                    'name'           => $user->name,
                    'title'          => "({$user->member_id})",
                    'rank_id'        => $user->rank_id ? ($user->rank_id + 4) : ($user->goldmine_rank_id ? $user->goldmine_rank_id : 0),
                    'children_count' => ($node->rgt - $node->lft - 1) / 2,
                ];

                $sponsorTree['children'] = $children;
            }
        }

        return view('tree::member.sponsor', compact('sponsorTree'));
    }

    public function children(Request $request, $userId)
    {
        $sponsorTree = [];
        $user        = $this->userRepository->find($userId);

        if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
            $userNode = $this->sponsorTreeRepository->findBySlug(Auth::user()->id);
            if ($user->id != Auth::user()->id) {
                if (!$userNode->isAncestorOf($node)) {
                    $error = ValidationException::withMessages([
                    'member_id' => [__('m_network_sponsor_tree.member must be under your network')],
                ]);
                    throw $error;
                }
            }

            $data = [
                'level' => $node->level + 1,
                'lft'   => $node->lft,
                'rgt'   => $node->rgt,
            ];
            $descendants = $this->query
                ->setParameters($data)
                ->get()
                ->toArray();

            $children    = $this->sponsorTreeRepository->buildFancyTree($descendants, $node->id);
            $sponsorTree = [
                'id'             => $node ? $node->id : $user->id,
                'email'          => $user->email,
                'user_id'        => $user->id,
                'username'       => $user->username,
                'name'           => $user->name,
                'title'          => "({$user->member_id})",
                'rank_id'        => $user->rank_id ? ($user->rank_id + 4) : ($user->goldmine_rank_id ? $user->goldmine_rank_id : 0),
                'children_count' => ($node->rgt - $node->lft - 1) / 2,
            ];
            $sponsorTree['children'] = $children;
        }

        return response()->json(['data' => $sponsorTree]);
    }

    public function details(Request $request, $userId)
    {
        $data = [];
        $user = $this->userRepository->find($userId);

        if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
            $userNode = $this->sponsorTreeRepository->findBySlug(Auth::user()->id);

            if (!$userNode->isSelfOrAncestorOf($node)) {
                $error = ValidationException::withMessages([
                    'member_id' => [__('m_network_sponsor_tree.member must be under your network')],
                ]);
                throw $error;
            }

            $data = $this->sponsorTreeDetailsQuery->query($userId);
        }

        return $this->respondSuccess('', $data);
    }
}
