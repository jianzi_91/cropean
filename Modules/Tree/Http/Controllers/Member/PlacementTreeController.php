<?php

namespace Modules\Tree\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Modules\Tree\Contracts\PlacementTreeContract;
use Modules\Tree\Http\Requests\Member\Index;
use Modules\Tree\Queries\Member\SponsorTreeDownlineQuery;
use Modules\Tree\Queries\PlacementTreeQuery;
use Modules\User\Contracts\UserContract;

class PlacementTreeController extends Controller
{
    /**
     * Placement Tree repository
     * @var unknown
     */
    protected $placementTreeRepository;

    /**
     * User repository
     * @var unknown
     */
    protected $userRepository;

    /**
     * The query object
     * @var unknown
     */
    protected $query;

    /**
     * The query object
     * @var unknown
     */
    protected $uplineQuery;

    public function __construct(
        PlacementTreeContract $placementTreeContract,
        UserContract $userContract,
        PlacementTreeQuery $placementTreeQuery
    ) {
        $this->placementTreeRepository = $placementTreeContract;
        $this->userRepository          = $userContract;
        $this->query                   = $placementTreeQuery;
    }

    public function index(Request $request)
    {
        $placementTree = [];

        $memberId = $request->member_id?: Auth::user()->member_id;

        if ($user = $this->userRepository->findByIdentifier($memberId)) {
            if ($node = $this->placementTreeRepository->findBySlug($user->id)) {
                $data = [
                    'level' => $node->level + config('tree.display_level'),
                    'lft'   => $node->lft,
                    'rgt'   => $node->rgt,
                ];
                $descendants = $this->query
                    ->setParameters($data)
                    ->get();

                $children      = $this->placementTreeRepository->buildFancyTree($descendants, $node->id);
                $placementTree = [
                    'id'             => $node ? $node->id : $user->id,
                    'user_id'        => $user->id,
                    'name'           => $user->username,
                    'title'          => "({$user->member_id}) {$user->name}",
                    'children_count' => ($node->rgt - $node->lft - 1) / 2,
                ];
                $placementTree['children'] = $children;
            }
        }

        return view('tree::member.placement', compact('placementTree'));
    }

    public function children(Request $request, $id)
    {
        $user          = $this->userRepository->find($id);
        $placementTree = [];

        if ($node = $this->placementTreeRepository->findBySlug($user->id)) {
            $userNode = $this->placementTreeRepository->findBySlug(Auth::user()->id);
            if (!$userNode->isAncestorOf($node)) {
                $error = ValidationException::withMessages([
                    'member_id' => [__('m_placement.member must be under your network')],
                ]);
                throw $error;
            }

            $data = [
                'level' => $node->level + config('tree.display_level'),
                'lft'   => $node->lft,
                'rgt'   => $node->rgt,
            ];
            $descendants = $this->query
                ->setParameters($data)
                ->get();

            $children      = $this->placementTreeRepository->buildFancyTree($descendants, $node->id);
            $placementTree = [
                'id'             => $node ? $node->id : $user->id,
                'user_id'        => $user->id,
                'name'           => $user->username,
                'title'          => "({$user->member_id}) {$user->name}",
                'children_count' => ($node->rgt - $node->lft - 1) / 2,
            ];
            $placementTree['children'] = $children;
        }

        return response()->json(['data' => $placementTree]);
    }

    public function details(Request $request, $userId)
    {
        $data = [];
        $user = $this->userRepository->find($userId);

        if ($node = $this->placementTreeRepository->findBySlug($user->id)) {
            $userNode = $this->placementTreeRepository->findBySlug(Auth::user()->id);
            if (!$userNode->isAncestorOf($node)) {
                $error = ValidationException::withMessages([
                    'member_id' => [__('m_placement.member must be under your network')],
                ]);
                throw $error;
            }
        }

        return response()->json(['data' => $data]);
    }
}
