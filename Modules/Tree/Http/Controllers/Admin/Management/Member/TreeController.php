<?php

namespace Modules\Tree\Http\Controllers\Admin\Management\Member;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Http\Requests\Admin\Management\Member\UpdateSponsor;
use Modules\Tree\Http\Requests\Admin\Management\Member\UpdateReferrer;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class TreeController extends Controller
{
    protected $permissions = [
        '*' => ['*']
    ];

    /**
     * The sponsor tree repository
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param SponsorTreeContract $sponsorTreeContract
     * @param UserContract $userContract
     */
    public function __construct(SponsorTreeContract $sponsorTreeContract, UserContract $userContract)
    {
        $this->middleware('permission:admin_user_member_management_update_tree')->only('edit', 'updateSponsor');

        $this->sponsorTreeRepository = $sponsorTreeContract;
        $this->userRepository        = $userContract;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_network.user not found'));
        }

        return view('tree::admin.management.member.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updateSponsor(UpdateSponsor $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_network.user not found'));
        }

        $newParentUser = $this->userRepository->findByMemberId($request->sponsor_id);
        if (!$newParentUser) {
            return back()->withErrors('error', __('a_member_management_change_network.sponsor_not_found'));
        }

        DB::beginTransaction();
        try {
            if ($newParentUser->member_id != $user->sponsor_id) {
                $this->sponsorTreeRepository->move($user->id, $newParentUser->id);
            }

            $user->update([
                'referrer_id' => $request->referrer_id,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.network.edit', $id))->withMessage(__('a_member_management_change_network.failed to update user sponsor'));
        }

        return back()->with('success', __('a_member_management_change_network.user_sponsor_updated_successfully'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updateReferrer(UpdateReferrer $request, $id)
    {
        $user = $this->userRepository->find($id);
        $user->update([
            'referrer_id' => $request->referrer_id,
        ]);

        return back()->with('success', __('a_member_management_change_network.user referrer updated successfully'));
    }
}
