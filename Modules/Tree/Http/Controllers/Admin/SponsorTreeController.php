<?php

namespace Modules\Tree\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Rbac\Models\Role;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Http\Requests\Admin\Index;
use Modules\Tree\Http\Requests\Admin\Upline;
use Modules\Tree\Queries\Admin\SponsorTreeDownlineQuery;
use Modules\Tree\Queries\Admin\SponsorTreeUplineQuery;
use Modules\Tree\Queries\SponsorTreeDetailsQuery;
use Modules\Tree\Repositories\SponsorTreeRepository;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Response\ApiResponse;

class SponsorTreeController extends Controller
{
    use ApiResponse;

    /**
     * Sponsor Tree repository
     * @var SponsorTreeRepository $sponsorTreeRepository
     */
    protected $sponsorTreeRepository;

    /**
     * User repository
     * @var unknown
     */
    protected $userRepository;

    /**
     * The query object
     * @var unknown
     */
    protected $query;

    /**
     * The query object
     * @var unknown
     */
    protected $uplineQuery;

    /**
     * The details query object
     * @var unknown
     */
    protected $sponsorTreeDetailsQuery;

    public function __construct(
        SponsorTreeContract $sponsorTreeContract,
        UserContract $userContract,
        SponsorTreeDownlineQuery $sponsorTreeQuery,
        SponsorTreeUplineQuery $sponsorTreeUplineQuery,
        SponsorTreeDetailsQuery $sponsorTreeDetailsQuery
    ) {
        $this->middleware('permission:admin_tree_list')->only('index', 'children', 'details');
        $this->middleware('permission:admin_tree_upline_list')->only('upline');

        $this->sponsorTreeRepository   = $sponsorTreeContract;
        $this->userRepository          = $userContract;
        $this->query                   = $sponsorTreeQuery;
        $this->uplineQuery             = $sponsorTreeUplineQuery;
        $this->sponsorTreeDetailsQuery = $sponsorTreeDetailsQuery;
    }

    public function index(Index $request)
    {
        $sponsorTree = [];

        if ($user = $this->userRepository->findByIdentifier($request->email_member_id)) {
            if (in_array($user->username, ['user1', 'user2', 'root']) && !(auth()->user()->role->name == Role::SYSADMIN)) {
                return redirect(route('admin.sponsor.index'))->with('error', __('a_sponsor.invalid user'));
            }
            if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
                $data = [
                    'level' => $node->level + config('tree.display_level'),
                    'lft'   => $node->lft,
                    'rgt'   => $node->rgt,
                ];
                $descendants = $this->query
                    ->setParameters($data)
                    ->get()
                    ->toArray();
                $children    = $this->sponsorTreeRepository->buildFancyTree($descendants, $node->id);
                $sponsorTree = [
                    'id'             => $node ? $node->id : $user->id,
                    'user_id'        => $user->id,
                    'email'          => $user->email,
                    'username'       => $user->username,
                    'name'           => $user->name,
                    'title'          => "({$user->member_id})",
                    'rank_id'        => $user->rank_id ? ($user->rank_id + 4) : ($user->goldmine_rank_id ? $user->goldmine_rank_id : 0),
                    'children_count' => ($node->rgt - $node->lft - 1) / 2
                ];
                $sponsorTree['children'] = $children;
            }
        }
        return view('tree::admin.sponsor', compact('sponsorTree'));
    }

    public function children(Request $request, $id)
    {
        $user        = $this->userRepository->find($id);
        $sponsorTree = [];

        if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
            $data = [
                'level' => $node->level + 1,
                'lft'   => $node->lft,
                'rgt'   => $node->rgt,
            ];
            $descendants = $this->query
                ->setParameters($data)
                ->get()
                ->toArray();

            $children    = $this->sponsorTreeRepository->buildFancyTree($descendants, $node->id);
            $sponsorTree = [
                'id'             => $node ? $node->id : $user->id,
                'user_id'        => $user->id,
                'email'          => $user->email,
                'username'       => $user->username,
                'name'           => $user->name,
                'title'          => "({$user->member_id})",
                'rank_id'        => $user->rank_id ? ($user->rank_id + 4) : ($user->goldmine_rank_id ? $user->goldmine_rank_id : 0),
                'children_count' => ($node->rgt - $node->lft - 1) / 2
            ];
            $sponsorTree['children'] = $children;
        }

        return response()->json(['data' => $sponsorTree]);
    }

    public function upline(Upline $request)
    {
        $uplines = collect();

        $identifier = $request->email_member_id ?: $request->email ?: $request->member_id ?: null;
        if ($identifier) {
            if ($user = $this->userRepository->findByIdentifier($identifier)) {
                if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
                    $data = [
                        'lft' => $node->lft,
                        'rgt' => $node->rgt,
                    ];
                    $request->merge($data);
                    $uplines = $this->uplineQuery
                        ->setParameters($request->all())
                        ->paginate();
                }
            }
        }

        return view('tree::admin.sponsor_upline', compact('uplines'));
    }

    public function details(Request $request, $userId)
    {
        $data = [];
        $user = $this->userRepository->find($userId);

        if ($node = $this->sponsorTreeRepository->findBySlug($user->id)) {
            $data = $this->sponsorTreeDetailsQuery->query($userId);
        }

        return $this->respondSuccess('', $data);
    }
}
