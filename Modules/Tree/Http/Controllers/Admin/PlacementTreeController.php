<?php

namespace Modules\Tree\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlacementTreeController extends Controller
{
    public function index(Request $request)
    {
        $placementTree = [];

        return view('tree::admin.placement', compact('placementTree'));
    }
}
