@extends('templates.admin.master')
@section('title', __('a_page_title.change network'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'breadcrumbs' => [
        ['name' => __('a_member_management_change_network.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_change_network.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_change_network.change network')]
    ],
    'header'=>__('a_member_management_change_network.change network'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'change network', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                {{ Form::open(['route' => ['admin.management.members.sponsor.update', $user->id], 'method'=>'put', 'id'=>'change-sponsor']) }}
                    {{ Form::formText('referrer_id', old('referrer_id') ?: $user->referrer_id, __('a_member_management_change_network.referrer id')) }}
                    {{ Form::formText('sponsor_id', old('sponsor_id') ?: $user->sponsor_id, __('a_member_management_change_network.member id')) }}
                    <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit', __('a_member_management_change_network.save')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
