<!-- //filter member_id / email can put in one column [name is email_member_id] -->
@extends('templates.admin.master')
@section('title', __('a_page_title.sponsor tree'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.sponsor tree')]
    ], 
    'header' => __('a_sponsor_tree.sponsor tree')
])


@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-4">
  {{ Form::formText('email_member_id', request()->email_member_id ?: old('email_member_id'), __('a_sponsor.email / member id'), array(), [], true) }}
</div>
@endcomponent


<div class="row">
  <div class="col-xl-9">
    <div class="card">
      <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_sponsor.sponsor tree')}}</h4>
        </div>
        <div class="tree-cont">
          @if (empty($sponsorTree))
            @component('templates.__fragments.components.tables')
              @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_sponsor.no records') ])
            @endcomponent
          @else
            <div id="tree-vertical" class="tree-vertical border w-100"></div>
          @endif
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3">
    <div class="card">
      <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
          <h4 class="card-title filter-title p-0 m-0">{{__('a_sponsor.legend')}}</h4>
        </div>
        <div class="sponsor_legend_container pl-1 pr-5 pt-2">
          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
                <img src="{{ asset('images/tree/V4.png') }}" height="40px" />
                <span class="sponsor_legend_text">{{ __('a_sponsor.v4') }}</span>
            </div>
          </div>

          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/V3.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.v3') }}</span>
            </div>
          </div>

          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/V2.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.v2') }}</span>
            </div>
          </div>
          
          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/V1.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.v1') }}</span>
            </div>
          </div>
            
          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/A4.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.a4') }}</span>
            </div>
          </div>

          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/A3.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.a3') }}</span>
            </div>
          </div>

          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/A2.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.a2') }}</span>
            </div>
          </div>

          <div class="col-xs-12 col-lg-6">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/A1.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.a1') }}</span>
            </div>
          </div>

          <div class="col-12">
            <div class="sponsor_legend">
              <img src="{{ asset('images/tree/no_rank.png') }}" height="40px" />
              <span class="sponsor_legend_text">{{ __('a_sponsor.no rank') }}</span>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>



@endsection

@push('scripts')
<script src="{{ asset('js/tree.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var data = {!! json_encode($sponsorTree) !!};

        if (!_.isEmpty(data)) {
            Tree("tree-vertical", data, {
                loadingMessage: '{{ __("a_sponsor.loading data") }}',
                radius: 40,
                url: "{{route('admin.sponsor.children',':id')}}",
                displayname: "name",
                title: "title",
                urlDetail: "{{route('admin.sponsor.details',':id')}}",
                translationKeys: {
                    leader_bonus_rank: '{{ __("a_sponsor.leader bonus rank") }}',
                    goldmine_rank: '{{ __("a_sponsor.goldmine bonus rank") }}',
                    no_record:'{{ __("a_sponsor.no record")}}',
                    personal_equity: '{{ __("a_sponsor.personal equity") }}',
                    children_count: '{{ __("a_sponsor.number of downlines") }}',
                    group_equity: '{{ __("a_sponsor.group equity balance") }}',
                    email: '{{ __("a_sponsor.email") }}',
                    name: '{{ __("a_sponsor.name") }}',
                    goldmine_ranks: '{{ __("a_sponsor.goldmine ranks") }}',
                    leader_ranks: '{{ __("a_sponsor.leader ranks") }}',
                },
                tooltip: { // value is the key found in data
                    name: 'name',
                    email: 'email',
                    leader_bonus_rank: 'leader_bonus_rank',
                    goldmine_rank: 'goldmine_rank',
                    personal_equity: 'personal_equity',
                    children_count: 'children_count',
                    group_equity: 'group_equity',
                    goldmine_ranks: 'goldmine_ranks',
                    leader_ranks: 'leader_ranks',
                },
                line: "s",
                level_var:"rank_id",
                level_icons:{
                    1:"{{asset('images/tree/A1.png')}}",
                    2:"{{asset('images/tree/A2.png')}}",
                    3:"{{asset('images/tree/A3.png')}}",
                    4:"{{asset('images/tree/A4.png')}}",
                    5:"{{asset('images/tree/V1.png')}}",
                    6:"{{asset('images/tree/V2.png')}}",
                    7:"{{asset('images/tree/V3.png')}}",
                    8:"{{asset('images/tree/V4.png')}}",
                    '1_children':"{{asset('images/tree/A1_children.png')}}",
                    '2_children':"{{asset('images/tree/A2_children.png')}}",
                    '3_children':"{{asset('images/tree/A3_children.png')}}",
                    '4_children':"{{asset('images/tree/A4_children.png')}}",
                    '5_children':"{{asset('images/tree/V1_children.png')}}",
                    '6_children':"{{asset('images/tree/V2_children.png')}}",
                    '7_children':"{{asset('images/tree/V3_children.png')}}",
                    '8_children':"{{asset('images/tree/V4_children.png')}}",
                    '-':"{{asset('images/tree/no_rank.png')}}",
                    '-_children':"{{asset('images/tree/no_rank_children.png')}}",
                },
                direction:"v",
                floatTooltip: true
            });
        }
    })
</script>
@endpush