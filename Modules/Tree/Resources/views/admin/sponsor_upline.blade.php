@extends('templates.admin.master')
@section('title', __('a_sponsor_upline_upline.sponsor upline'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_sponsor_upline_upline.network upline')],
    ],
    'header'=>__('a_sponsor_upline.network upline'),
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-4">
  {{ Form::formText('email_member_id', request()->email_member_id ?: old('email_member_id'), __('a_sponsor_upline.email / member id'), array(), false) }}
</div>
@endcomponent

<div class="card">
  <div class="card-content">
    <div class="card-body d-flex justify-content-between align-items-center p-2">
        <h4 class="card-title filter-title p-0 m-0">{{__('a_sponsor_upline.sponsor tree upline')}}</h4>
    </div>
    @component('templates.__fragments.components.tables')
      <thead class="text-capitalize">
          <tr>
              <th>{{ __('a_sponsor_upline.email') }}</th>
              <th>{{ __('a_sponsor_upline.name') }}</th>
              <th>{{ __('a_sponsor_upline.member id') }}</th>
              <th>{{ __('a_sponsor_upline.level') }}</th>
          </tr>
      </thead>
      <tbody> 
          @forelse ($uplines as $upline)
          <tr>
              <td>{{ $upline->email }}</td>
              <td>{{ $upline->name }}</td>
              <td>{{ $upline->member_id }}</td>
              <td>{{ $upline->level }}</td>
          </tr>
          @empty
          @include('templates.__fragments.components.no-table-records', [ 'span' => 4,
          'text' => __('a_sponsor_upline.no records') ])
          @endforelse
      </tbody>
    @endcomponent
  </div>
</div>
@if(method_exists($uplines,'render'))
    {!! $uplines->render() !!}
@endif
@endsection
