<?php

return [
    'name'     => 'Website',
    'bindings' => [
        'Modules\Website\Contracts\UserContactContract' => 'Modules\Website\Repositories\UserContactRepository',
    ],

    /*
     * Website contact us receiver
     */
    'contact_us_receiver' => env('CONTACT_US_RECEIVER_EMAIL', 'backend@plus65.com.sg'),
];
