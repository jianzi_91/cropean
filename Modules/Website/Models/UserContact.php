<?php

namespace Modules\Website\Models;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    protected $fillable = [
        'name',
        'email_address',
        'mobile_number',
        'company',
        'message'
    ];
}
