<?php

namespace Modules\Website\Http\Requests\ContactUs;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:255',
            'email_address' => 'required|email|max:255',
            'mobile_number' => 'required|numeric|digits_between:5,20',
            'company'       => 'required',
            'message'       => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
