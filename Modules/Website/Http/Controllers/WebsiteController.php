<?php

namespace Modules\Website\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class WebsiteController extends Controller
{
    /**
     * Return home index.
     * @return Response
     */
    public function index()
    {
        return view('website::index');
    }

    public function error()
    {
        return view('website::cloudflare_error.error');
    }

    public function navigation()
    {
        return view('website::cloudflare_error.navigation');
    }
}
