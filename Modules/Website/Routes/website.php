<?php

Route::group(['domain' => config('core.website_url'), 'middleware' => 'web'], function () {
    //Route::get('/', ['as' => 'website.index', 'uses' => 'WebsiteController@index']);
    #for cloudfare error
    Route::get('/errors.html', ['as' => 'website.error', 'uses' => 'WebsiteController@error']);
    Route::get('/navigation.html', ['as' => 'website.navigation', 'uses' => 'WebsiteController@navigation']);
});
