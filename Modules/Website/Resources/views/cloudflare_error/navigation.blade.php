@extends('templates.member.wide')
@section('title', __('w_home.home'))

@push('styles')
<style>
.wrapper {
    display: flex;
    width: 100%;
    height: 100vh;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}

.code {
    font-size: 15rem;
    line-height: 30rem;
    color: #cda674;
    font-family: "Rubik Medium";
}

.text {
    font-size: 2.5rem;
    color: #58595B;
    font-family: "Rubik Medium";
    margin-bottom: 20px;
}

@media only screen and (max-width:900px) {
    .code {
        font-size: 10rem;
        line-height: 20rem;
    }

    .text {
        font-size: 2rem;
    }
}

@media only screen and (max-width:670px) {
    .code {
        font-size: 8rem;
        line-height: 15rem;
    }

    .text {
        font-size: 1.5rem;
    }
}

@media only screen and (max-width:500px) {
    .code {
        font-size: 5rem;
        line-height: 10rem;
    }

    .text {
        font-size: 1rem;
    }
}

@media only screen and (max-width:400px) {
    .code {
        font-size: 5rem;
        line-height: 10rem;
    }

    .text {
        font-size: 0.8rem;
    }
}
</style>
@endpush()

@section('main')
  <div class="wrapper">
    <div class="code">Redirect</div>
    <div class="text">Please hold on while we direct you to the page</div>
    <div class="text">请您耐心等待。我们将把您导向页面。</div>
    <div style="display:none;">::IM_UNDER_ATTACK_BOX::</div>
  </div>
@endsection