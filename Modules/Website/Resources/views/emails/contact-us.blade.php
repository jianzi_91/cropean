@component('mail::message')

    Contact Us Email
<br><br>

@component('mail::table')
| Attribute     | Input values  |
| ------------- |:-------------:|
@foreach($data as $key => $row)
| {{ $key }}    | {{ $row }}    |
@endforeach
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
