<?php

namespace Modules\Website\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;
use Modules\Website\Contracts\UserContactContract;
use Modules\Website\Models\UserContact;

class UserContactRepository extends Repository implements UserContactContract
{
    use HasCrud;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param User  $model
     */
    public function __construct(
        UserContact $model
    ) {
        $this->model = $model;

        parent::__construct($model);
    }

    // implement functions here
}
