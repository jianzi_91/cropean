<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Models\CreditConversion;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Models\Deposit;
use Modules\Promotion\Models\PromotionPayout;
use Modules\Promotion\Models\PromotionPayoutBreakdown;
use Modules\Promotion\Models\PromotionPayoutSnapshot;
use Modules\Promotion\Repositories\JulyPromotionCheckingRepository;
use Modules\Promotion\Repositories\AugustPromotionCheckingRepository;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserStatusContract;

class PromotionCheckingCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'promotions:check {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking the capital credit daily, deduct promotion credit if capital dropped';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        BankAccountCreditContract $creditRepo,
        BankAccountContract $bankAccountRepo,
        CreditConversionContract $conversionRepo,
        UserContract $userRepo,
        UserStatusContract $userStatusRepo
    ) {
        $this->creditRepo     = $creditRepo;
        $this->conversionRepo = $conversionRepo;
        $this->bankRepo       = $bankAccountRepo;
        $this->userRepo       = $userRepo;
        $this->userStatusRepo = $userStatusRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon($this->argument('date'));

        if (PromotionPayoutSnapshot::where('run_date', $this->argument('date'))->exists()) {
            $this->line("Checking on $date is already done");
            exit;
        }

        // Get all qualifier of July and contributor of June since they are the only affected user
        $junePromotionContributors = PromotionPayoutBreakdown::all()->pluck('user_id')->toArray();
        $julyPromotionQualifiers   = PromotionPayout::where('promotion_id', '2')->get()->pluck('user_id')->toArray();
        $augustPromotionQualifiers   = PromotionPayout::where('promotion_id', '3')->get()->pluck('user_id')->toArray();

        $affectedUsers             = array_unique(array_merge($junePromotionContributors, $julyPromotionQualifiers, $augustPromotionQualifiers));

        $nonEligibleStatuses = $this->userStatusRepo->getModel()->whereIn('name', [UserStatusContract::SUSPENDED, UserStatusContract::TERMINATED])->pluck('id')->toArray();

        try {
            DB::beginTransaction();
            foreach ($affectedUsers as $affectedUser) {
                $user = $this->userRepo->find($affectedUser);

                if (!in_array($user->user_status_id, $nonEligibleStatuses)) {
                    $this->line("\n Checking user $user->member_id");

                    if ($user->augustPromotionPayout) {
                        $this->line("\nThis user is eligible for August Promotion.");
                        $this->line("\nThis user current amount of August Promotion Payout: " . $user->augustPromotionPayout->amount);

                        $percentage = 0.5;
                        $augustPayout = new AugustPromotionCheckingRepository($user, $percentage);
                        $deduction  = $augustPayout->check();

                        $this->line("\nThis user current amount of August Promotion Payout after checking: " . $user->augustPromotionPayout->amount);
                    }

                    if ($user->julyPromotionPayout) {
                        $this->line("\nThis user is eligible for July Promotion.");
                        $this->line("\nThis user current amount of July Promotion Payout: " . $user->julyPromotionPayout->amount);

                        $percentage = 0.5;
                        $julyPayout = new JulyPromotionCheckingRepository($user, $percentage);

                        if (!empty($deduction)) {
                            $julyPayout->setDeduction($deduction);
                        }

                        $deduction  = $julyPayout->check();

                        $this->line("\nThis user current amount of July Promotion Payout after checking: " . $user->julyPromotionPayout->amount);
                    } 
                    
                    if (empty($user->augustPromotionPayout) && empty($user->julyPromotionPayout)){
                        // When it's here means either
                        // - The user does not eligible for august promotion
                        // - The user does not eligible for july promotion
                        // - The august promotion is havent payout
                        // Therefore, we need to do something here to cater for those cases

                        // Get the data for june maintenance checking
                        $totalDepositsAfterPayout    = $this->getDepositsAfterPayout($user->id);
                        $totalConversionsAfterPayout = $this->getConversionsAfterPayout($user->id);
                        $totalConvertedCapital       = $this->getConvertedCapitalAfterPayout($user->id);

                        $deductable     = bcadd($totalDepositsAfterPayout, $totalConversionsAfterPayout, 6);
                        $deductedAmount = bcsub($user->junePromotionContribution->initial_amount, $user->junePromotionContribution->amount, 6);

                        // To substract out deducted amount from total converted capital then we can know the actual amount to be deducted
                        // But if deductable amount is higher than the then the actual amount to be deducted that means we still have quota to be deducted
                        // Therefore, no action have to take
                        if ($deductable >= $totalConvertedCapital - $deductedAmount) {
                            continue;
                        }

                        $deduction = $totalConvertedCapital - $deductedAmount;
                        $deduction -= $deductable;
                    }

                    // If the user is a contributor of June promotion and there are deduction need to be done will be come over here
                    if ($user->junePromotionContribution && !empty($deduction)) {
                        $percentage = 0.3;
                        $breakdown  = PromotionPayoutBreakdown::where('user_id', $user->id)->first();
                        $payout     = $breakdown->payout;

                        $this->line("\nThis user is contributor of payoutId: $payout->id of June Promotion");

                        if (empty($deductedAmount)) {
                            $deductedAmount = bcsub($user->junePromotionContribution->initial_amount, $user->junePromotionContribution->amount, 6);
                            $deduction -= $deductedAmount;

                            if ($deduction == 0) {
                                continue;
                            }
                        }

                        $this->line("\nThis user current June promotion contribution is $breakdown->amount");
                        if ($deduction >= $breakdown->amount || in_array($user->user_status_id, $nonEligibleStatuses)) {
                            $deduction         = $breakdown->amount;
                            $breakdown->amount = 0;
                        } else {
                            $amountAfterDeduct = $breakdown->amount - $deduction;

                            // Revoke all contribution if june deposit value dropped to less than 500
                            if ($amountAfterDeduct < 500) {
                                $deduction         = $breakdown->amount;
                                $breakdown->amount = 0;
                            } else {
                                $breakdown->amount -= $deduction;
                            }
                        }
                        $breakdown->save();

                        $this->line("\nThis user contribution became $breakdown->amount");

                        $this->line("\nThis june promotion payout current balance is $payout->amount");

                        $payout->amount -= bcmul($deduction, $percentage, 2);
                        $payout->save();

                        $this->line("This june promotion payout new balance is $payout->amount");

                        $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, bcmul($deduction, $percentage, 2), $payout->user_id, 'promotion_penalty', 'June Promotion Penalty');
                    }

                    $this->line("\n--- Checking for userId: $user->member_id has ended --- \n");

                    $deduction = 0; // Reset all remaining deduction for this user to prevent carry over to next user
                }
            }

            //Snapshot
            $payouts = PromotionPayout::all();

            foreach ($payouts as $payout) {
                $upline = $this->userRepo->find($payout->user_id);
                if (in_array($upline->user_status_id, $nonEligibleStatuses)) {
                    $payoutDeduction = $payout->amount;
                    $payout->update([
                        'amount' => 0,
                    ]);
                    $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $payoutDeduction, $payout->user_id, 'promotion_penalty');
                }

                $firstDownline = $payout->breakdowns()->where('ranking', 1)->first();
                if ($firstDownline) {
                    $firstDownlineUser = $this->userRepo->find($firstDownline->user_id);
                }
                $secondDownline = $payout->breakdowns()->where('ranking', 2)->first();
                if ($secondDownline) {
                    $secondDownlineUser = $this->userRepo->find($secondDownline->user_id);
                }
                $thirdDownline = $payout->breakdowns()->where('ranking', 3)->first();
                if ($thirdDownline) {
                    $thirdDownlineUser = $this->userRepo->find($thirdDownline->user_id);
                }

                PromotionPayoutSnapshot::create([
                    'payout_id'                      => $payout->id,
                    'user_id'                        => $payout->user_id,
                    'amount'                         => $payout->amount,
                    'first_downline_id'              => isset($firstDownline) ? $firstDownline->user_id : null,
                    'first_downline_capital_credit'  => isset($firstDownline) ? $firstDownlineUser->capital_credit_balance : null,
                    'second_downline_id'             => isset($secondDownline) ? $secondDownline->user_id : null,
                    'second_downline_capital_credit' => isset($secondDownline) ? $secondDownlineUser->capital_credit_balance : null,
                    'third_downline_id'              => isset($thirdDownline) ? $thirdDownline->user_id : null,
                    'third_downline_capital_credit'  => isset($thirdDownline) ? $thirdDownlineUser->capital_credit_balance : null,
                    'run_date'                       => $date->toDateString(),
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the total amount member deposit in after payout
     *
     * @param $userId
     * @return string
     */
    public function getDepositsAfterPayout($userId)
    {
        $depositStatusRepo = resolve(DepositStatusContract::class);

        $approvedStatus = $depositStatusRepo->getModel()
            ->where('name', DepositStatusContract::APPROVED)
            ->first();

        return Deposit::where('user_id', $userId)
            ->whereDate('updated_at', '>=', '2020-07-01')
            ->where('deposit_status_id', $approvedStatus->id)
            ->sum('amount');
    }

    /**
     * Get the total amount member converted to capital credit after payout
     *
     * @param $userId
     * @return string
     */
    public function getConversionsAfterPayout($userId)
    {
        $conversionStatusRepo = resolve(CreditConversionStatusContract::class);
        $bankCreditTypeRepo   = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $userId)
            ->where('destination_credit_type_id', $capitalCredit->id)
            ->whereDate('created_at', '>=', '2020-07-01')
            ->sum('destination_amount');
    }

    /**
     * Get the total amount member converted out after payout
     * - From here we determine should the conversion affect the contributed promotion credit
     *
     *  @param $userId
     *  @return string
     */
    public function getConvertedCapitalAfterPayout($userId)
    {
        $bankCreditTypeRepo = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        $cashCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CASH_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $userId)
            ->where('source_credit_type_id', $capitalCredit->id)
            ->where('destination_credit_type_id', $cashCredit->id)
            ->whereDate('created_at', '>=', '2020-07-01')
            ->sum('source_amount');
    }
}
