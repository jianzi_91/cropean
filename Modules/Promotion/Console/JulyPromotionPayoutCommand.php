<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Promotion\Repositories\JulyPromotionRepository;

class JulyPromotionPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'july-promotion:payout {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payout for July\'20 promotion.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = $this->start();
        $this->line("July Promotion Payout started at " . $startTime->toDateTimeString());

        $promotion = new JulyPromotionRepository();
        $this->line("[Step 1] Get promotion to payout");
        $promotion->getPromotion();

        $this->line("[Step 2] Set non eligible users");
        $promotion->setNonEligibleUsers();

        try {
            DB::beginTransaction();

            $this->line("[Step 3] Calculate the payout");
            $promotion->calculate();

            $this->line("[Step 4] Payout");
            $promotion->payout();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            $this->line($e->getMessage());
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
