<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Promotion\Repositories\PromotionRepository;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;

class JunePromotionPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'promotions:payout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate for June\'20 promotion.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $sponsorTreeSnapshotHistoryContract
    ) {
        $this->sponsorTree                    = $sponsor;
        $this->sponsorTreeSnapshotHistoryRepo = $sponsorTreeSnapshotHistoryContract;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Validate tree
        if (!$this->sponsorTree->getModel()->isValidNestedSet()) {
            $this->error("Sponsor Tree is invalid");
            exit;
        }

        $dateFrom = Carbon::parse('2020-07-01 00:00:00');
        $dateTo   = Carbon::parse('2020-07-01 23:59:59');

        $sponsorTreeSnapshot = $this->sponsorTreeSnapshotHistoryRepo->getModel()
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
                DB::raw('0 as max_level'),
            ])
            ->toArray();

        $promotion = new PromotionRepository($sponsorTreeSnapshot);
        $promotion->setBankAccountCreditSnapshot();

        try {
            DB::beginTransaction();

            $promotion->calculate();
            $promotion->payout();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            $this->error($e->getMessage());
        }
    }
}
