<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Promotion\Models\PromotionPayout;

class JunePromotionClearanceCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'promotions:clear {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Promotion Credit when promotion ended';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        BankAccountCreditContract $creditRepo
    ) {
        $this->creditRepo = $creditRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now            = new Carbon($this->argument('date'));
        $juneCutoffDate = Carbon::parse('2020-10-01');
        $julyCutoffDate = Carbon::parse('2020-11-01');
        $augustCutoffDate = Carbon::parse('2020-11-17');

        if ($now->toDateString() == $juneCutoffDate->toDateString()) {
            $payouts = PromotionPayout::where('promotion_id', 1)->get();

            try {
                DB::beginTransaction();
                foreach ($payouts as $payout) {
                    if ($payout->amount > 0) {
                        $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $payout->amount, $payout->user_id, 'promotion_clearance');

                        $payout->update([
                            'amount' => 0,
                        ]);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                \Log::error($e);
            }
        }

        if ($now->toDateString() == $julyCutoffDate->toDateString()) {
            $payouts = PromotionPayout::where('promotion_id', 2)->get();

            try {
                DB::beginTransaction();
                foreach ($payouts as $payout) {
                    if ($payout->amount > 0) {
                        $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $payout->amount, $payout->user_id, 'promotion_clearance');

                        $payout->update([
                            'amount' => 0,
                        ]);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                \Log::error($e);
            }
        }

        if ($now->toDateString() == $augustCutoffDate->toDateString()) {
            $payouts = PromotionPayout::where('promotion_id', 3)->get();

            try {
                DB::beginTransaction();
                foreach ($payouts as $payout) {
                    if ($payout->amount > 0) {
                        $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $payout->amount, $payout->user_id, 'promotion_clearance');

                        $payout->update([
                            'amount' => 0,
                        ]);
                    }
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();

                \Log::error($e);
            }
        }
    }
}
