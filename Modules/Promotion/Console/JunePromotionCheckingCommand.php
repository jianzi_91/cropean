<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionContract;
use Modules\Promotion\Models\PromotionPayout;
use Modules\Promotion\Models\PromotionPayoutBreakdown;
use Modules\Promotion\Models\PromotionPayoutSnapshot;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserStatusContract;

class JunePromotionCheckingCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'promotions:check {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking the capital credit daily, deduct promotion credit if capital dropped';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        BankAccountCreditContract $creditRepo,
        BankAccountContract $bankAccountRepo,
        CreditConversionContract $conversionRepo,
        UserContract $userRepo,
        UserStatusContract $userStatusRepo
    ) {
        $this->creditRepo     = $creditRepo;
        $this->conversionRepo = $conversionRepo;
        $this->bankRepo       = $bankAccountRepo;
        $this->userRepo       = $userRepo;
        $this->userStatusRepo = $userStatusRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon($this->argument('date'));

        if (PromotionPayoutSnapshot::where('run_date', $this->argument('date'))->exists()) {
            $this->line("Checking on $date is already done");
            exit;
        }

        $startDate = $date->copy()->subDays(2)->toDateString();
        $endDate   = $date->copy()->subDay()->toDateString();

        $snapshots = DB::table('bank_account_credit_snapshots AS start_snapshot')
            ->where('start_snapshot.bank_credit_type_id', 1)
            ->where('start_snapshot.run_date', $startDate)
            ->join('bank_account_credit_snapshots as end_snapshot', function ($join) use ($endDate) {
                $join->on('end_snapshot.bank_account_id', '=', 'start_snapshot.bank_account_id')
                    ->where('end_snapshot.bank_credit_type_id', 1)
                    ->where('end_snapshot.run_date', $endDate);
            })
            ->select([
                'start_snapshot.bank_account_id',
                'start_snapshot.run_date as start_date',
                'end_snapshot.run_date as end_date',
                'start_snapshot.balance as start_balance',
                'end_snapshot.balance as end_balance',
                DB::raw('CAST(end_snapshot.balance - start_snapshot.balance AS SIGNED) AS difference'),
            ])
            ->get();

        $nonEligibleStatuses = $this->userStatusRepo->getModel()->whereIn('name', [UserStatusContract::SUSPENDED, UserStatusContract::TERMINATED])->pluck('id')->toArray();

        try {
            DB::beginTransaction();
            foreach ($snapshots as $snapshot) {
                $bankAccount = $this->bankRepo->find($snapshot->bank_account_id);
                $user        = $this->userRepo->find($bankAccount->reference_id);

                if ($snapshot->difference < 0 || in_array($user->user_status_id, $nonEligibleStatuses)) {
                    $deduction = abs($snapshot->difference);

                    $breakdown = PromotionPayoutBreakdown::where('user_id', $user->id)->first();
                    if (!$breakdown) {
                        continue; // Skip newly registered member
                    }
                    $payout = $breakdown->payout;

                    # checking any conversion happen on the day
                    $dailyConversion = $this->getMemberDailyConversion($user->id);
                    if ($dailyConversion == 0) {
                        $deduction = 0;
                    } else {
                        $deduction = $dailyConversion;
                    }

                    if ($deduction >= $breakdown->amount || $snapshot->end_balance < 500 || in_array($user->user_status_id, $nonEligibleStatuses)) {
                        $deduction         = $breakdown->amount;
                        $breakdown->amount = 0;
                    } else {
                        $breakdown->amount -= $deduction;
                    }
                    $breakdown->save();

                    $payout->amount -= bcmul($deduction, 0.3, 2);
                    $payout->save();

                    $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, bcmul($deduction, 0.3, 2), $payout->user_id, 'promotion_penalty');
                }
            }

            //Snapshot
            $payouts = PromotionPayout::all();

            foreach ($payouts as $payout) {
                $upline = $this->userRepo->find($payout->user_id);
                if (in_array($upline->user_status_id, $nonEligibleStatuses)) {
                    $payoutDeduction = $payout->amount;
                    $payout->update([
                        'amount' => 0,
                    ]);
                    $this->creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $payoutDeduction, $payout->user_id, 'promotion_penalty');
                }

                $firstDownline = $payout->breakdowns()->where('ranking', 1)->first();
                if ($firstDownline) {
                    $firstDownlineUser = $this->userRepo->find($firstDownline->user_id);
                }
                $secondDownline = $payout->breakdowns()->where('ranking', 2)->first();
                if ($secondDownline) {
                    $secondDownlineUser = $this->userRepo->find($secondDownline->user_id);
                }
                $thirdDownline = $payout->breakdowns()->where('ranking', 3)->first();
                if ($thirdDownline) {
                    $thirdDownlineUser = $this->userRepo->find($thirdDownline->user_id);
                }

                PromotionPayoutSnapshot::create([
                    'payout_id'                      => $payout->id,
                    'user_id'                        => $payout->user_id,
                    'amount'                         => $payout->amount,
                    'first_downline_id'              => isset($firstDownline) ? $firstDownline->user_id : null,
                    'first_downline_capital_credit'  => isset($firstDownline) ? $firstDownlineUser->capital_credit_balance : null,
                    'second_downline_id'             => isset($secondDownline) ? $secondDownline->user_id : null,
                    'second_downline_capital_credit' => isset($secondDownline) ? $secondDownlineUser->capital_credit_balance : null,
                    'third_downline_id'              => isset($thirdDownline) ? $thirdDownline->user_id : null,
                    'third_downline_capital_credit'  => isset($thirdDownline) ? $thirdDownlineUser->capital_credit_balance : null,
                    'run_date'                       => $date->toDateString(),
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->error($e->getMessage());
        }
    }

    public function getMemberDailyConversion($userId)
    {
        $date      = new Carbon($this->argument('date'));
        $startDate = $date->copy()->subDay()->startOfDay()->toDateTimeString();
        $endDate   = $date->copy()->subDay()->endOfDay()->toDateTimeString();

        return $this->conversionRepo->getModel()
            ->where('user_id', $userId)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('source_credit_type_id', 1)
            ->sum('source_amount');
    }
}
