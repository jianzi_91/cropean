<?php

namespace Modules\Promotion\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Promotion\Repositories\ReferralBonusRepository;

class ReferralBonusPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'referral-bonus:payout {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Referral Bonus payout command.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon($this->argument('date'));
        $date = $date->copy()->subDay()->toDateString();

        $startDate = Carbon::parse(setting('referral_bonus_start_date'))->toDateString();
        $endDate = Carbon::parse(setting('referral_bonus_end_date'))->toDateString();

        if ($date < $startDate || $date > $endDate) {
            exit;
        }

        $startTime = $this->start();
        $this->line("Referral Bonus started at " . $startTime->toDateTimeString());

        $referralBonus = new ReferralBonusRepository();
        $referralBonus->setDate($date);
        $referralBonus->setNonEligibleUsers($date);
        $referralBonus->setPayoutPercentage();

        DB::beginTransaction();
        
        try {
            $referralBonus->calculate();
            $referralBonus->payout();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            $this->line($e->getMessage());
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
