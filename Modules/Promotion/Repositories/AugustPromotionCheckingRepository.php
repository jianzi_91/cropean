<?php

namespace Modules\Promotion\Repositories;

use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Models\CreditConversion;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Models\Deposit;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class AugustPromotionCheckingRepository extends Repository
{
    protected $user;

    protected $percentage;

    /**
     * Class constructor
     */
    public function __construct(User $user, $percentage)
    {
        $this->user       = $user;
        $this->percentage = $percentage;
    }

    public function check()
    {
        $creditRepo = resolve(BankAccountCreditContract::class);

        $userId                      = $this->user->id;
        $augustPromotion             = $this->user->augustPromotionPayout;
        $totalDepositsAfterPayout    = $this->getDepositsAfterPayout($userId);
        $totalConversionsAfterPayout = $this->getConversionsAfterPayout($userId);
        $totalConvertedCapital       = $this->getConvertedCapitalAfterPayout($userId);

        // From addition of total deposit and total conversion to capital credit after payout
        // Then subtract the total capital converted to cash credit
        // We can have the deductable amount to prevent deduction on the contribution of july promo
        $deductable = bcadd($totalDepositsAfterPayout, $totalConversionsAfterPayout, 6);

        // Then we are getting the deducted amount by deducting the inital promo credit and current promo credit
        // After that by diving it to the percentage, we will eventually get the deducted amount of capital credit
        $deductedAmount = bcdiv(bcsub($augustPromotion->initial_amount, $augustPromotion->amount, 6), $this->percentage, 6);

        if ($deductable >= $totalConvertedCapital - $deductedAmount) {
            return 0;
        }

        switch (bccomp($totalConvertedCapital, $deductedAmount, 6)) {
            // If total converted to cash is equivalent to dedudctedAmount means the difference is already deducted no further checking needed
            case 0:
            // Impossible the deducted is greater than the total converted capital to cash
            case -1:
                return 0;
            break;
            // If the deductable is less than the deducted amount, means we have to do something
            case 1:
                $deduction = bcsub($totalConvertedCapital, $deductedAmount, 6);

                if ($deductable != 0) {
                    $deduction = bcsub($deduction, $deductable);
                }

                // Return the deduction is the july promotion can no longer be deducted
                if ($augustPromotion->amount == 0) {
                    return $deduction;
                }

                $deduction = bcmul($deduction, $this->percentage, 6);

                // Time to check is there sufficient amount to be deducted for the penalty
                if ($deduction <= $augustPromotion->amount) {
                    if ($augustPromotion->amount - $deduction < 250) {
                        $creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $augustPromotion->amount, $userId, 'promotion_penalty', 'August Promotion Penalty');
                        $augustPromotion->amount = 0;
                        $leftover = 0;
                    } else {
                        $augustPromotion->amount -= $deduction;
                        $creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $deduction, $userId, 'promotion_penalty', 'August Promotion Penalty');
                        $leftover = 0;
                    }
                } else {
                    $leftover = bcdiv($deduction - $augustPromotion->amount, $this->percentage, 6);
                    $creditRepo->deduct(BankCreditTypeContract::PROMOTION_CREDIT, $augustPromotion->amount, $userId, 'promotion_penalty', 'August Promotion Penalty');
                    $augustPromotion->amount = 0;
                }
                $augustPromotion->save();

                return $leftover;
            break;
        }
    }

    /**
     * Get the total amount member deposit in after payout
     *
     * @param $userId
     * @return string
     */
    public function getDepositsAfterPayout($userId)
    {
        $depositStatusRepo = resolve(DepositStatusContract::class);

        $approvedStatus = $depositStatusRepo->getModel()
            ->where('name', DepositStatusContract::APPROVED)
            ->first();

        return Deposit::where('user_id', $userId)
            ->whereDate('updated_at', '>=', '2020-08-17')
            ->where('deposit_status_id', $approvedStatus->id)
            ->sum('amount');
    }

    /**
     * Get the total amount member converted to capital credit after payout
     *
     * @param $userId
     * @return string
     */
    public function getConversionsAfterPayout($userId)
    {
        $conversionStatusRepo = resolve(CreditConversionStatusContract::class);
        $bankCreditTypeRepo   = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $userId)
            ->where('destination_credit_type_id', $capitalCredit->id)
            ->whereDate('created_at', '>=', '2020-08-17')
            ->sum('destination_amount');
    }

    /**
     * Get the total amount member converted out after payout
     * - From here we determine should the conversion affect the contributed promotion credit
     *
     *  @param $userId
     *  @return string
     */
    public function getConvertedCapitalAfterPayout($userId)
    {
        $bankCreditTypeRepo = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        $cashCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CASH_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $userId)
            ->where('source_credit_type_id', $capitalCredit->id)
            ->where('destination_credit_type_id', $cashCredit->id)
            ->whereDate('created_at', '>=', '2020-08-17')
            ->sum('source_amount');
    }
}
