<?php

namespace Modules\Promotion\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Carbon\Carbon;
use DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Models\BankTransactionType;
use Modules\Deposit\Models\DepositStatus;
use Modules\Promotion\Models\PromotionPayout;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class PromotionRepository extends Repository
{
    use HasNestedSet;

    protected $data = [];
    /**
     * The percentage column
     *
     * @var string
     */
    protected $percent = 'percentage';

    /**
     * Constructor
     *
     * @param array $tree
     * @param array $amounts
     */
    public function __construct(array $tree = null)
    {
        if (!is_null($tree) && count($tree) > 1) {
            $this->buildTree($tree);
        }
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function calculate($precision = 2)
    {
        $users = User::all();

        foreach ($users as $user) {
            if ($user->isMember) {
                $this->getDownlinesDeposits($user->id);
            }
        }

        try {
            DB::beginTransaction();
            foreach ($this->data as $userId => $downlines) {
                $totalContributions = 0;
                $payout             = PromotionPayout::create([
                    'promotion_id'   => 1,
                    'payout_date'    => null,
                    'user_id'        => $userId,
                    'initial_amount' => 0,
                    'amount'         => 0,
                ]);

                $ranking = 1;
                foreach ($downlines as $donwlineId => $contribution) {
                    if ($ranking > 3) {
                        break;
                    }

                    $payout->breakdowns()->create([
                        'user_id'        => $donwlineId,
                        'initial_amount' => $contribution,
                        'amount'         => $contribution,
                        'ranking'        => $ranking,
                    ]);
                    $ranking++;
                    $totalContributions += $contribution;
                }

                if ($totalContributions > 0) {
                    $payout->update([
                        'initial_amount' => bcmul($totalContributions, 0.3, 2),
                        'amount'         => bcmul($totalContributions, 0.3, 2),
                    ]);
                } else {
                    $payout->forceDelete();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            \Log::error($e->getMessage());
        }
    }

    public function payout()
    {
        $payouts    = PromotionPayout::all();
        $creditRepo = resolve(BankAccountCreditContract::class);

        try {
            DB::beginTransaction();
            foreach ($payouts as $payout) {
                if ($creditRepo->add(BankCreditTypeContract::PROMOTION_CREDIT, $payout->amount, $payout->user_id, 'promotion')) {
                    $payout->update([
                        'payout_date'      => now(),
                        'is_credit_issued' => true,
                    ]);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e);
        }
    }

    public function getDownlinesDeposits($userId)
    {
        $node = $this->getNodeByUser($userId);

        if (!isset($node)) {
            return;
        }

        $directDownlines = $this->getDirectDownlines($node);
        $snapshot        = $this->bankAccountCreditSnapshot;
        $downlines       = [];

        if ($directDownlines) {
            foreach (array_reverse($directDownlines) as $downline) {
                $user              = User::find($downline->getUserId());
                $bankAccountCredit = $snapshot->where('bank_account_id', $user->id)->first();
                $totalDeposited    = $this->getMemberTotalDeposits($user);
                $totalConverted    = $this->getMemberTotalConversions($user);
                if ($bankAccountCredit->end_balance < 500) {
                    continue;
                }

                if ($bankAccountCredit->difference > 0) {
                    if ($totalDeposited != 0) {
                        $downlines[$user->id] = $totalDeposited - $totalConverted;
                    }
                } else {
                    if ($totalDeposited != 0) {
                        $downlines[$user->id] = $totalDeposited - $totalConverted;
                    }
                }
            }
            $donwlines           = arsort($downlines);
            $this->data[$userId] = $downlines;
        }
    }

    public function setBankAccountCreditSnapshot()
    {
        $startDate = Carbon::parse('2020-05-31')->toDateString();
        $endDate   = Carbon::parse('2020-06-30')->toDateString();

        $snapshot = User::join('bank_accounts', 'bank_accounts.reference_id', 'users.id')
            ->join('bank_account_credit_snapshots AS start_snapshot', function ($join) use ($startDate) {
                $join->on('start_snapshot.bank_account_id', 'bank_accounts.id')
                    ->where('start_snapshot.bank_credit_type_id', 1)
                    ->where('start_snapshot.run_date', '>=', $startDate)
                    ->orderBy('start_snapshot.run_date');
            })
            ->leftJoin('bank_account_credit_snapshots as end_snapshot', function ($join) use ($endDate) {
                $join->on('end_snapshot.bank_account_id', '=', 'start_snapshot.bank_account_id')
                        ->where('end_snapshot.bank_credit_type_id', 1)
                        ->where('end_snapshot.run_date', $endDate);
            })
            ->select([
                'start_snapshot.bank_account_id',
                'start_snapshot.run_date as start_date',
                'end_snapshot.run_date as end_date',
                'start_snapshot.balance as start_balance',
                'end_snapshot.balance as end_balance',
                DB::raw('CAST(end_snapshot.balance - start_snapshot.balance AS SIGNED) AS difference'),
            ])
            ->groupBy('start_snapshot.bank_account_id')
            ->orderBy('start_snapshot.bank_account_id', 'DESC')
            ->get();

        $this->bankAccountCreditSnapshot = $snapshot;
    }

    public function getMemberTotalDeposits(User $member)
    {
        $approvedStatus = DepositStatus::where('name', 'approved')->first();

        return $member->deposits
            ->where('deposit_status_id', $approvedStatus->id)
            ->whereBetween('updated_at', ['2020-06-01 00:00:00', '2020-06-30 23:59:59'])
            ->sum('amount');
    }

    public function getMemberTotalConversions(User $member)
    {
        $endDate         = now()->subDay()->endOfDay()->toDateTimeString();
        $transactionType = BankTransactionType::whereIn('name', ['credit_conversion_from', 'credit_conversion_penalty_fee'])->get()->pluck('id');

        return BankAccountStatement::whereIn('bank_transaction_type_id', $transactionType)
            ->where('bank_account_id', $member->bankAccount->id)
            ->where('bank_credit_type_id', 1)
            ->whereBetween('transaction_date', ['2020-06-01 00:00:00', $endDate])
            ->sum('debit');
    }
}
