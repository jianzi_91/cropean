<?php

namespace Modules\Promotion\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Plus65\Base\Repositories\Repository;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Models\CreditConversion;
use Modules\Deposit\Models\Deposit;
use Modules\Deposit\Models\DepositStatus;
use Modules\Promotion\Models\ReferralBonusPayout;
use Modules\Promotion\Models\ReferralBonusPayoutBreakdown;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;

class ReferralBonusRepository extends Repository 
{    
    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function setPayoutPercentage()
    {
        $this->percentage = setting('referral_bonus_payout_percentage');
    }

    public function calculate()
    {
        $deposits    = $this->getDeposits();
        $conversions = $this->getConversions();
        $payouts     = [];

        foreach ($deposits as $deposit) {
            $member   = $deposit->user;
            $referrer = $member->referrer;

            ReferralBonusPayoutBreakdown::create([
                'referrer_id'  => $referrer->id,
                'user_id'      => $member->id,
                'type'         => 'deposit',
                'reference_id' => $deposit->id,
                'amount'       => $deposit->amount,
            ]);
        }

        foreach ($conversions as $conversion) {
            $member   = $conversion->user;
            $referrer = $member->referrer;

            ReferralBonusPayoutBreakdown::create([
                'referrer_id'  => $referrer->id,
                'user_id'      => $member->id,
                'type'         => 'conversion',
                'reference_id' => $conversion->id,
                'amount'       => $conversion->destination_amount,
            ]);
        }
    }

    public function payout()
    {
        $breakdowns = $this->getUncreditedReferralBonus();
        $payoutDate = Carbon::parse($this->date)->addDay();

        foreach ($breakdowns as $breakdown) {
            ReferralBonusPayout::create([
                'payout_date'      => $payoutDate,
                'user_id'          => $breakdown->referrer_id,
                'amount'           => $breakdown->total,
                'percentage'       => $this->percentage,
                'computed_amount'  => bcmul($breakdown->total, $this->percentage, 2),
                'is_credit_issued' =>false,
            ]);
        }

        $payouts    = ReferralBonusPayout::whereDate('payout_date', $payoutDate)->get();
        $creditRepo = resolve(BankAccountCreditContract::class);

        foreach ($payouts as $payout) {
            if ($creditRepo->add(BankCreditTypeContract::CASH_CREDIT, $payout->computed_amount, $payout->user_id, BankTransactionTypeContract::REFERRAL_BONUS)) {
                $payout->update([
                    'is_credit_issued' => true,
                ]);
            }

            ReferralBonusPayoutBreakdown::whereNull('referral_bonus_payout_id')
                ->where('referrer_id', $payout->user_id)
                ->update([
                    'referral_bonus_payout_id' => $payout->id,
                ]);
        }
    }

    public function getDeposits()
    {
        $approvedStatus = DepositStatus::where('name', 'approved')->first();

        return Deposit::where('deposit_status_id', $approvedStatus->id)
            ->whereDate('updated_at', $this->date)
            ->get();
    }

    public function getConversions()
    {
        $conversionStatusRepo = resolve(CreditConversionStatusContract::class);
        $bankCreditTypeRepo   = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        return CreditConversion::where('destination_credit_type_id', $capitalCredit->id)
            ->whereDate('created_at', $this->date)
            ->get();
    }

    public function getUncreditedReferralBonus()
    {
        return ReferralBonusPayoutBreakdown::whereNull('referral_bonus_payout_id')
            ->groupBy('referrer_id')
            ->select([
                'referrer_id',
                DB::raw('SUM(amount) AS total'),
            ])
            ->get();
    }
}
