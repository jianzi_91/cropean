<?php

namespace Modules\Promotion\Repositories;

use DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\CreditConversionStatusContract;
use Modules\Credit\Models\CreditConversion;
use Modules\Deposit\Models\DepositStatus;
use Modules\Promotion\Models\Promotion;
use Modules\Promotion\Models\PromotionPayout;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class JulyPromotionRepository extends Repository
{
    /**
     * Constructor
     *
     */
    public function __construct()
    {
    }

    public function getPromotion()
    {
        $this->promotion = Promotion::where('name', 'july_promotion')->first();
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function calculate()
    {
        $users     = User::whereNotIn('id', $this->nonEligibleReceiveUsers)->get();
        $promotion = Promotion::where('name', 'july_promotion')->first();

        foreach ($users as $user) {
            $totalDeposits            = $this->getMemberTotalDeposits($user);
            $totalConversions         = $this->getMemberTotalConversionToCapital($user);
            $totalConvertedOuts       = $this->getMemberCapitalToCashConversions($user->id);
            $eligiblePromotionCredits = bcsub(bcadd($totalDeposits, $totalConversions, 20), $totalConvertedOuts, 20);

            // Less than 500 is not eligible
            if ($eligiblePromotionCredits < 500) {
                continue;
            }

            $payout = PromotionPayout::create([
                'promotion_id'     => $this->promotion->id,
                'user_id'          => $user->id,
                'initial_amount'   => bcmul($eligiblePromotionCredits, 0.5, credit_precision()),
                'amount'           => bcmul($eligiblePromotionCredits, 0.5, credit_precision()),
                'is_credit_issued' => false,
            ]);
        }
    }

    public function payout()
    {
        $payouts    = PromotionPayout::where('promotion_id', $this->promotion->id)->get();
        $creditRepo = resolve(BankAccountCreditContract::class);

        try {
            DB::beginTransaction();
            foreach ($payouts as $payout) {
                if ($creditRepo->add(BankCreditTypeContract::PROMOTION_CREDIT, $payout->amount, $payout->user_id, 'promotion')) {
                    $payout->update([
                        'payout_date'      => now(),
                        'is_credit_issued' => true,
                    ]);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e);
        }
    }

    public function getMemberTotalDeposits(User $member)
    {
        $approvedStatus = DepositStatus::where('name', 'approved')->first();

        return $member->deposits
            ->where('deposit_status_id', $approvedStatus->id)
            ->whereBetween('updated_at', ['2020-07-07 00:00:00', '2020-07-31 23:59:59'])
            ->sum('amount');
    }

    public function getMemberTotalConversionToCapital(User $member)
    {
        $conversionStatusRepo = resolve(CreditConversionStatusContract::class);
        $bankCreditTypeRepo   = resolve(BankCreditTypeContract::class);

        $approvedStatus = $conversionStatusRepo->getModel()
            ->where('name', CreditConversionStatusContract::APPROVED)
            ->first();

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $member->id)
            ->where('destination_credit_type_id', $capitalCredit->id)
            ->whereBetween('created_at', ['2020-07-07 00:00:00', '2020-07-31 23:59:59'])
            ->sum('destination_amount');
    }

    public function getMemberCapitalToCashConversions($userId)
    {
        $bankCreditTypeRepo = resolve(BankCreditTypeContract::class);

        $capitalCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        $cashCredit = $bankCreditTypeRepo->getModel()
            ->where('name', BankCreditTypeContract::CASH_CREDIT)
            ->first();

        return CreditConversion::where('user_id', $userId)
            ->where('source_credit_type_id', $capitalCredit->id)
            ->where('destination_credit_type_id', $cashCredit->id)
            ->whereBetween('created_at', ['2020-07-07 00:00:00', '2020-07-31 23:59:59'])
            ->sum('source_amount');
    }
}
