<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::group(['prefix' => 'reports'], function () {
            Route::get('promotion/{payout_id}/breakdown/export', ['as' => 'admin.report.promotion.breakdown.export', 'uses' => 'Admin\PromotionBreakdownExportController@index']);
            Route::get('promotion/{payout_id}/breakdown', ['as' => 'admin.report.promotion.breakdown', 'uses' => 'Admin\PromotionBreakdownController@index']);
            Route::get('promotion/export', ['as' => 'admin.report.promotion.export', 'uses' => 'Admin\PromotionExportController@index']);
            Route::get('promotion/preview', 'Admin\PromotionBonusController@preview')->name('admin.report.promotion.preview');
            Route::get('promotion/preview/{memberId}', 'Admin\PromotionBreakdownController@preview')->name('admin.report.promotion.preview.breakdown');
            Route::resource('promotion', 'Admin\PromotionBonusController', ['as' => 'admin.report'])->only(['index']);

            Route::resource('july-promotion', 'Admin\JulyPromotionController', ['as' => 'admin.report'])->only(['index', 'show']);

            Route::resource('august-promotion', 'Admin\AugustPromotionController', ['as' => 'admin.report'])->only(['index', 'show']);

            Route::resource('referral', 'Admin\ReferralBonusController', ['as' => 'admin.report'])->only(['index', 'show']);
        });
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('promotion/{payout_id}/breakdown/export', ['as' => 'member.report.promotion.breakdown.export', 'uses' => 'Member\PromotionBreakdownExportController@index']);
        Route::get('promotion/{payout_id}/breakdown', ['as' => 'member.report.promotion.breakdown', 'uses' => 'Member\PromotionBreakdownController@index']);
        Route::get('promotion/export', ['as' => 'member.report.promotion.export', 'uses' => 'Member\PromotionExportController@index']);
        Route::get('promotion/preview', 'Member\PromotionBonusController@preview')->name('member.report.promotion.preview');
        Route::resource('promotion', 'Member\PromotionBreakdownController', ['as' => 'member.report'])->only(['index']);
        Route::resource('referral', 'Member\ReferralBonusController', ['as' => 'member.report'])->only(['index']);
    });
});
