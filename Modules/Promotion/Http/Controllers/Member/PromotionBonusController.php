<?php

namespace Modules\Promotion\Http\Controllers\Member;

use Affiliate\Commission\Concerns\HasNestedSet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Models\BankAccountStatement;
use Modules\Credit\Models\BankTransactionType;
use Modules\Deposit\Models\DepositStatus;
use Modules\Promotion\Queries\Member\PayoutBreakdownQuery;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Modules\User\Models\User;

class PromotionBonusController extends Controller
{
    use HasNestedSet;

    protected $percent = 'percentage';

    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutBreakdownQuery;

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $payoutBreakdownQuery
     */
    public function __construct(
        PayoutBreakdownQuery $payoutBreakdownQuery,
        SponsorTreeSnapshotHistoryContract $sponsorTreeSnapshotHistoryContract
    ) {
        // $this->middleware('permission:member_commission_goldmine_bonus_list')->only('index');
        $this->payoutBreakdownQuery           = $payoutBreakdownQuery;
        $this->sponsorTreeSnapshotHistoryRepo = $sponsorTreeSnapshotHistoryContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutBreakdownQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::member.index', compact('payouts'));
    }

    public function preview(Request $request)
    {
        $date     = new Carbon($request->date);
        $dateFrom = $date->copy()->startOfDay();
        $dateTo   = $date->copy()->endOfDay();

        if (!auth()->user()->has_downlines) {
            abort(404);
        }

        $sponsorTreeSnapshot = $this->sponsorTreeSnapshotHistoryRepo->getModel()
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
                DB::raw('0 as max_level'),
            ])
            ->toArray();

        $tree = $this->buildTree($sponsorTreeSnapshot);
        $this->setBankAccountCreditSnapshot();
        $this->getDownlinesDeposits(auth()->user()->id);

        $contributors = [];
        $rank         = 1;
        foreach ($this->data as $data) {
            foreach ($data as $id => $contribution) {
                if ($rank > 3) {
                    break;
                }

                $user                = User::find($id);
                $contributors[$rank] = [
                    'member_id'      => $user->member_id,
                    'name'           => $user->name,
                    'email'          => $user->email,
                    'capital_credit' => $contribution,
                ];
                $rank++;
            }
        }

        return view('promotion::member.preview', compact('contributors'));
    }

    public function getDownlinesDeposits($userId)
    {
        $node            = $this->getNodeByUser($userId);
        $directDownlines = $this->getDirectDownlines($node);
        $snapshot        = $this->bankAccountCreditSnapshot;
        $downlines       = [];

        if ($directDownlines) {
            foreach (array_reverse($directDownlines) as $downline) {
                $user              = User::find($downline->getUserId());
                $bankAccountCredit = $snapshot->where('bank_account_id', $user->id)->first();
                $totalDeposited    = $this->getMemberTotalDeposits($user);
                $totalConverted    = $this->getMemberTotalConversions($user);
                if ($bankAccountCredit->end_balance < 500) {
                    continue;
                }

                if ($bankAccountCredit->difference > 0) {
                    $downlines[$user->id] = $totalDeposited - $totalConverted;
                } else {
                    if ($totalDeposited != 0) {
                        $downlines[$user->id] = $totalDeposited - $totalConverted;
                    }
                }
            }
            $donwlines           = arsort($downlines);
            $this->data[$userId] = $downlines;
        }
    }

    public function setBankAccountCreditSnapshot($date = null)
    {
        $startDate = Carbon::parse('2020-05-31')->toDateString();
        $endDate   = Carbon::parse($date)->subDay()->toDateString();

        $snapshot = User::join('bank_accounts', 'bank_accounts.reference_id', 'users.id')
            ->join('bank_account_credit_snapshots AS start_snapshot', function ($join) use ($startDate) {
                $join->on('start_snapshot.bank_account_id', 'bank_accounts.id')
                    ->where('start_snapshot.bank_credit_type_id', 1)
                    ->where('start_snapshot.run_date', '>=', $startDate)
                    ->orderBy('start_snapshot.run_date');
            })
            ->leftJoin('bank_account_credit_snapshots as end_snapshot', function ($join) use ($endDate) {
                $join->on('end_snapshot.bank_account_id', '=', 'start_snapshot.bank_account_id')
                        ->where('end_snapshot.bank_credit_type_id', 1)
                        ->where('end_snapshot.run_date', $endDate);
            })
            ->select([
                'start_snapshot.bank_account_id',
                'start_snapshot.run_date as start_date',
                'end_snapshot.run_date as end_date',
                'start_snapshot.balance as start_balance',
                'end_snapshot.balance as end_balance',
                DB::raw('CAST(end_snapshot.balance - start_snapshot.balance AS SIGNED) AS difference'),
            ])
            ->groupBy('start_snapshot.bank_account_id')
            ->orderBy('start_snapshot.bank_account_id', 'DESC')
            ->get();

        $this->bankAccountCreditSnapshot = $snapshot;
    }

    public function getMemberTotalDeposits(User $member)
    {
        $endDate        = now()->subDay()->endOfDay()->toDateTimeString();
        $approvedStatus = DepositStatus::where('name', 'approved')->first();

        return $member->deposits
            ->where('deposit_status_id', $approvedStatus->id)
            ->whereBetween('updated_at', ['2020-06-01 00:00:00', $endDate])
            ->sum('amount');
    }

    public function getMemberTotalConversions(User $member)
    {
        $endDate         = now()->subDay()->endOfDay()->toDateTimeString();
        $transactionType = BankTransactionType::whereIn('name', ['credit_conversion_from', 'credit_conversion_penalty_fee'])->get()->pluck('id');

        return BankAccountStatement::whereIn('bank_transaction_type_id', $transactionType)
            ->where('bank_account_id', $member->bankAccount->id)
            ->whereBetween('transaction_date', ['2020-06-01 00:00:00', $endDate])
            ->sum('debit');
    }
}
