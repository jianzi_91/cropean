<?php

namespace Modules\Promotion\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Member\ReferralBonusPayoutQuery;
use Modules\Promotion\Queries\Member\ReferralBonusPayoutBreakdownQuery;

class ReferralBonusController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutQuery;

    /**
     * Class constructor
     *
     * @param ReferralBonusPayoutQuery $payoutQuery
     */
    public function __construct(
        ReferralBonusPayoutBreakdownQuery $breakdownQuery
    ) {
        $this->middleware('permission:member_referral_bonus_report_view')->only('index');

        $this->breakdownQuery = $breakdownQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $breakdowns = $this->breakdownQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::member.referral-bonus.index', compact('breakdowns'));
    }
}
