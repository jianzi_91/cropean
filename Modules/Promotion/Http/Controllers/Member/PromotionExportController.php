<?php

namespace Modules\Promotion\Http\Controllers\Member;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Member\PayoutBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class PromotionExportController extends Controller
{
    use CanExportTrait;

    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $promotionBreakdownQuery;

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $query
     *
     */
    public function __construct(PayoutBreakdownQuery $promotionBreakdownQuery)
    {
        $this->promotionBreakdownQuery = $promotionBreakdownQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payout = auth()->user()->promotionPayout;

        $request->merge([
            'promotion_payout_id' => $payout->id,
        ]);

        return $this->exportReport($this->promotionBreakdownQuery->setParameters($request->all()), __('m_report_june_promo.promotion credit breakdown') . now() . '.xlsx', auth()->user(), __('m_report_june_promo.promotion credit breakdown') . Carbon::now()->toDateString());
    }
}
