<?php

namespace Modules\Promotion\Http\Controllers\Member;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Http\Requests\Member\Goldmine\Breakdown\Index;
use Modules\Promotion\Queries\Member\PayoutBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class PromotionBreakdownExportController extends Controller
{
    use CanExportTrait;

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $query
     *
     */

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $promotionBreakdownQuery
     */
    public function __construct(PayoutBreakdownQuery $promotionBreakdownQuery)
    {
        $this->promotionBreakdownQuery = $promotionBreakdownQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Index $request, $payoutId)
    {
        $payout = auth()->user()->promotionPayout;

        $request->merge([
            'promotion_payout_id' => $payout->id,
        ]);

        return $this->exportReport($this->promotionBreakdownQuery->setParameters($request->all()), __('m_report_june_promo.promotion credit breakdown') . now() . '.xlsx', auth()->user(), __('m_report_june_promo.promotion credit breakdown') . Carbon::now()->toDateString());
    }
}
