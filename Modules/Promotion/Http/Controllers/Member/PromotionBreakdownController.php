<?php

namespace Modules\Promotion\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Member\PayoutBreakdownQuery;

class PromotionBreakdownController extends Controller
{
    /**
     * The goldmine breakdown query
     *
     * @var unknown
     */
    protected $promotionBreakdownQuery;

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $promotionBreakdownQuery
     */
    public function __construct(PayoutBreakdownQuery $promotionBreakdownQuery)
    {
        $this->middleware('permission:member_june_promotion_report_view')->only(['index']);
    
        $this->promotionBreakdownQuery = $promotionBreakdownQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $payout = auth()->user()->promotionPayout;

        if ($payout) {
            $request->merge([
                'promotion_payout_id' => $payout->id,
            ]);
        }

        $payouts = $this->promotionBreakdownQuery
            ->setParameters($request->all())
            ->get();

        return view('promotion::member.breakdown', compact('payout', 'payouts'));
    }
}
