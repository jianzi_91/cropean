<?php

namespace Modules\Promotion\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Admin\PayoutBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class PromotionBreakdownExportController extends Controller
{
    use CanExportTrait;

    /**
     * The goldmine payout contract
     *
     * @var unknown
     */
    protected $promotionBreakdownQuery;

    /**
     * Class constructor
     *
     * @param PayoutBreakdownQuery $goldmineQuery
     */
    public function __construct(PayoutBreakdownQuery $promotionBreakdownQuery)
    {
        $this->promotionBreakdownQuery = $promotionBreakdownQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $payoutId)
    {
        $request->merge([
            'promotion_payout_id' => $payoutId,
        ]);

        return $this->exportReport($this->promotionBreakdownQuery->setParameters($request->all()), __('a_goldmine.promotion payout breakdown') . now() . '.xlsx', auth()->user(), __('a_goldmine.goldmine breakdown') . Carbon::now()->toDateString());
    }
}
