<?php

namespace Modules\Promotion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Admin\PayoutQuery;
use Modules\User\Queries\Member\UserQuery;

class PromotionBonusController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutQuery;

    /**
     * Class constructor
     *
     * @param GoldmineQuery $goldmineQuery
     */
    public function __construct(
        PayoutQuery $payoutQuery,
        UserQuery $userQuery
    ) {
        $this->middleware('permission:admin_june_promotion_report_view')->only('index');

        $this->payoutQuery = $payoutQuery;
        $this->userQuery   = $userQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.index', compact('payouts'));
    }

    public function preview(Request $request)
    {
        $members = $this->userQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.preview', compact('members'));
    }
}
