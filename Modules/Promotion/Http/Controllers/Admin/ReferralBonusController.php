<?php

namespace Modules\Promotion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Admin\ReferralBonusPayoutQuery;
use Modules\Promotion\Queries\Admin\ReferralBonusPayoutBreakdownQuery;

class ReferralBonusController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutQuery;

    /**
     * Class constructor
     *
     * @param AugustPromotionQuery $payoutQuery
     */
    public function __construct(
        ReferralBonusPayoutQuery $payoutQuery,
        ReferralBonusPayoutBreakdownQuery $breakdownQuery
    ) {
        $this->middleware('permission:admin_referral_bonus_report_view')->only('index', 'show');

        $this->payoutQuery   = $payoutQuery;
        $this->breakdownQuery = $breakdownQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.referral-bonus.index', compact('payouts'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $request->merge([
            'referrer_id' => $id,
        ]);

        $breakdowns = $this->breakdownQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.referral-bonus.show', compact('breakdowns'));
    }
}
