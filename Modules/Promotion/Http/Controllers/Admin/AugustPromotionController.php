<?php

namespace Modules\Promotion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Admin\AugustPromotionQuery;
use Modules\Promotion\Queries\Admin\AugustPromotionSnapshotQuery;

class AugustPromotionController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutQuery;

    /**
     * Class constructor
     *
     * @param AugustPromotionQuery $payoutQuery
     */
    public function __construct(
        AugustPromotionQuery $payoutQuery,
        AugustPromotionSnapshotQuery $snapshotQuery
    ) {
        $this->middleware('permission:admin_august_promotion_report_view')->only('index', 'show');

        $this->payoutQuery   = $payoutQuery;
        $this->snapshotQuery = $snapshotQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.august-promotion.index', compact('payouts'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $snapshots = $this->snapshotQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.august-promotion.show', compact('snapshots'));
    }
}
