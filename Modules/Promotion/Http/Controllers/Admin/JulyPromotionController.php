<?php

namespace Modules\Promotion\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Promotion\Queries\Admin\JulyPromotionQuery;
use Modules\Promotion\Queries\Admin\JulyPromotionSnapshotQuery;

class JulyPromotionController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $payoutQuery;

    /**
     * Class constructor
     *
     * @param JulyPromotionQuery $payoutQuery
     */
    public function __construct(
        JulyPromotionQuery $payoutQuery,
        JulyPromotionSnapshotQuery $snapshotQuery
    ) {
        $this->middleware('permission:admin_july_promotion_report_view')->only('index', 'show');

        $this->payoutQuery   = $payoutQuery;
        $this->snapshotQuery = $snapshotQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->payoutQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.july-promotion.index', compact('payouts'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $snapshots = $this->snapshotQuery
            ->setParameters($request->all())
            ->paginate();

        return view('promotion::admin.july-promotion.show', compact('snapshots'));
    }
}
