<?php

namespace Modules\Promotion\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Promotion\Models\PromotionPayout;
use QueryBuilder\QueryBuilder;

class JulyPromotionQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        $query = PromotionPayout::join('users', 'users.id', 'promotion_payouts.user_id')
            ->where('promotion_id', 2)
            ->select([
                'users.member_id AS member_id',
                'users.name AS name',
                'users.email AS email',
                'promotion_payouts.initial_amount AS payout_initial_amount',
                'promotion_payouts.amount AS payout_amount',
                'promotion_payouts.id AS payout_id'
            ]);

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
