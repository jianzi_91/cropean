<?php

namespace Modules\Promotion\Queries\Admin;

use Modules\Promotion\Queries\JulyPromotionSnapshotQuery as BaseQuery;

class JulyPromotionSnapshotQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('promotion_payouts.id', request()->july_promotion);
        // Do extra process before building the query here
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }
}
