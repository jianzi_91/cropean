<?php

namespace Modules\Promotion\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Promotion\Queries\PayoutBreakdownQuery as BaseQuery;

class PayoutBreakdownQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Adhoc processes after build
     */
    public function afterBuild()
    {
        return $this->builder->where('promotion_payout_snapshots.payout_id', request()->promotion_payout_id);
    }

    public function map($payout): array
    {
        return [
            $payout->run_date,
            $payout->amount,
            amount_format($payout->first_downline_capital_credit, 2),
            amount_format($payout->second_downline_capital_credit, 2),
            amount_format($payout->third_downline_capital_credit, 2),
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_june_promo_breakdown.date'),
            __('a_report_june_promo_breakdown.promo credits'),
            __('a_report_june_promo_breakdown.direct downline 1 capital credit balance'),
            __('a_report_june_promo_breakdown.direct downline 2 capital credit balance'),
            __('a_report_june_promo_breakdown.direct downline 3 capital credit balance'),
        ];
    }
}
