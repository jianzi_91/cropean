<?php

namespace Modules\Promotion\Queries\Admin;

use Modules\Promotion\Queries\JulyPromotionQuery as BaseQuery;

class JulyPromotionQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'date' => [
            'filter'    => 'date_range',
            'table'     => 'promotion_payouts',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process before building the query here
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }
}
