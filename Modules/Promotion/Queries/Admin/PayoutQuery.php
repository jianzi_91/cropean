<?php

namespace Modules\Promotion\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Promotion\Queries\PayoutQuery as BaseQuery;

class PayoutQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'date' => [
            'filter'    => 'date_range',
            'table'     => 'promotion_payouts',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->receiver_name,
            $payout->member_id,
            $payout->email,
            $payout->contributors,
            amount_format($payout->amount, credit_precision()),
            $payout->goldmineRank->name,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_goldmine.date time'),
            __('a_report_goldmine.name'),
            __('a_report_goldmine.member id'),
            __('a_report_goldmine.email'),
            __('a_report_goldmine.number of contributors'),
            __('a_report_goldmine.issued payout amount'),
            __('a_report_goldmine.rank'),
        ];
    }
}
