<?php

namespace Modules\Promotion\Queries;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use QueryBuilder\QueryBuilder;

class ContributorQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $startDate = Carbon::parse('2020-05-31')->toDateString();
        $endDate   = now();

        $query = DB::table('bank_account_credit_snapshots AS start_snapshot')
            ->where('start_snapshot.bank_credit_type_id', 1)
            ->where('start_snapshot.run_date', $startDate)
            ->join('bank_account_credit_snapshots as end_snapshot', function ($join) use ($endDate) {
                $join->on('end_snapshot.bank_account_id', '=', 'start_snapshot.bank_account_id')
                    ->where('end_snapshot.bank_credit_type_id', 1)
                    ->where('end_snapshot.run_date', $endDate);
            })
            ->select([
                'start_snapshot.bank_account_id',
                'start_snapshot.run_date as start_date',
                'end_snapshot.run_date as end_date',
                'start_snapshot.balance as start_balance',
                'end_snapshot.balance as end_balance',
                DB::raw('(end_snapshot.balance - start_snapshot.balance) AS difference'),
            ]);

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
