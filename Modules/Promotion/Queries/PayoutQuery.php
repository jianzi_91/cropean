<?php

namespace Modules\Promotion\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Promotion\Models\PromotionPayout;
use QueryBuilder\QueryBuilder;

class PayoutQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = PromotionPayout::join('users', 'users.id', 'promotion_payouts.user_id')
            ->where('promotion_id', 1)
            ->select([
                'users.member_id AS member_id',
                'users.name AS name',
                'users.email AS email',
                'promotion_payouts.amount AS payout_amount',
                'promotion_payouts.id AS payout_id'
            ]);

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
