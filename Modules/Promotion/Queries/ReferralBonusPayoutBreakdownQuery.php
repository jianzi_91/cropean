<?php

namespace Modules\Promotion\Queries;

use Modules\Promotion\Models\ReferralBonusPayoutBreakdown;
use QueryBuilder\QueryBuilder;

class ReferralBonusPayoutBreakdownQuery extends QueryBuilder
{
    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        return ReferralBonusPayoutBreakdown::join('users as member', 'member.id', 'referral_bonus_payout_breakdowns.user_id')
            ->join('users as referrer', 'referrer.id', 'referral_bonus_payout_breakdowns.referrer_id')
            ->join('referral_bonus_payouts', 'referral_bonus_payouts.id', 'referral_bonus_payout_breakdowns.referral_bonus_payout_id')
            ->select(
                'referral_bonus_payout_breakdowns.created_at',
                'member.name as member_name',
                'member.member_id as member_id',
                'member.email as member_email',
                'referral_bonus_payout_breakdowns.amount',
                'type',
                'payout_date',
            )
            ->orderBy('payout_date', 'desc');
    }
}
