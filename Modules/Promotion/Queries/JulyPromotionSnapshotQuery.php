<?php

namespace Modules\Promotion\Queries;

use Modules\Promotion\Models\PromotionPayout;
use QueryBuilder\QueryBuilder;

class JulyPromotionSnapshotQuery extends QueryBuilder
{
    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        return PromotionPayout::leftJoin('promotion_payout_snapshots', 'promotion_payout_snapshots.payout_id', '=', 'promotion_payouts.id')
            ->select([
                'promotion_payout_snapshots.run_date',
                'promotion_payout_snapshots.amount as snapshot_amount',
                'promotion_payouts.initial_amount as qualified_capital',
            ])
            ->orderBy('run_date', 'DESC');
    }
}
