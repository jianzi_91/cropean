<?php

namespace Modules\Promotion\Queries\Member;

use Modules\Promotion\Queries\ReferralBonusPayoutBreakdownQuery as BaseQuery;

class ReferralBonusPayoutBreakdownQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'referral_bonus_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'member',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'member',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'equal',
            'table'     => 'member',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'type' => [
            'filter'    => 'equal',
            'table'     => 'referral_bonus_payout_breakdowns',
            'column'    => 'type',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('referral_bonus_payout_breakdowns.referrer_id', auth()->user()->id);
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }
}
