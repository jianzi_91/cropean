<?php

namespace Modules\Promotion\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Promotion\Queries\ContributorQuery as BaseQuery;

class ContributorQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [

    ];

    public function map($payout): array
    {
        return [
        ];
    }

    public function headings(): array
    {
    }
}
