<?php

namespace Modules\Promotion\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Promotion\Queries\PayoutBreakdownQuery as BaseQuery;

class PayoutBreakdownQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Adhoc processes after build
     */
    public function afterBuild()
    {
        return $this->builder->where('promotion_payout_snapshots.payout_id', request()->promotion_payout_id);
    }

    public function map($payout): array
    {
        return [
            $payout->run_date,
            amount_format($payout->amount),
            isset($payout->first_downline_capital_credit) ? amount_format($payout->first_downline_capital_credit) : '-',
            isset($payout->second_downline_capital_credit) ? amount_format($payout->second_downline_capital_credit) : '-',
            isset($payout->third_downline_capital_credit) ? amount_format($payout->third_downline_capital_credit) : '-',
        ];
    }

    public function headings(): array
    {
        return [
            __('m_report_june_promo.date'),
            __('m_report_june_promo.promo credits'),
            __('m_report_june_promo.direct downline 1 capital credit balance'),
            __('m_report_june_promo.direct downline 2 capital credit balance'),
            __('m_report_june_promo.direct downline 3 capital credit balance'),
        ];
    }
}
