<?php

namespace Modules\Promotion\Queries;

use Illuminate\Support\Facades\DB;
use Modules\Promotion\Models\ReferralBonusPayout;
use QueryBuilder\QueryBuilder;

class ReferralBonusPayoutQuery extends QueryBuilder
{
    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        return ReferralBonusPayout::join('users', 'users.id', 'referral_bonus_payouts.user_id')
            ->select([
                'user_id',
                'name',
                'member_id',
                'email',
                DB::raw("SUM(amount) as amount"),
                DB::raw("SUM(computed_amount) as payout"),
            ])
            ->groupBy('user_id');
    }
}
