<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class PromotionPayoutBreakdown extends Model
{
    use SoftDeletes;

    protected $table   = "promotion_payout_breakdowns";
    protected $guarded = [];

    public function payout()
    {
        return $this->belongsTo(PromotionPayout::class, 'promotion_payout_id');
    }

    public function member()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
