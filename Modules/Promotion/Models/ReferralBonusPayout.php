<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralBonusPayout extends Model
{
    protected $table   = "referral_bonus_payouts";
    protected $guarded = [];
}
