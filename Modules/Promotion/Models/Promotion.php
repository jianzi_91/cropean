<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table   = "promotions";
    protected $guarded = [];
}
