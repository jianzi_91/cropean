<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionPayout extends Model
{
    use SoftDeletes;

    protected $table   = "promotion_payouts";
    protected $guarded = [];

    public function breakdowns()
    {
        return $this->hasMany(PromotionPayoutBreakdown::class, 'promotion_payout_id');
    }
}
