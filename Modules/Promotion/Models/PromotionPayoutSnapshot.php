<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionPayoutSnapshot extends Model
{
    protected $table   = "promotion_payout_snapshots";
    protected $guarded = [];
}
