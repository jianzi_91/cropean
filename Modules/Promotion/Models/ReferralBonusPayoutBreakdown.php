<?php

namespace Modules\Promotion\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralBonusPayoutBreakdown extends Model
{
    protected $table   = "referral_bonus_payout_breakdowns";
    protected $guarded = [];
}
