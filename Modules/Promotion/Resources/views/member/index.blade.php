@extends('templates.member.master')
@section('title', __('m_page_title.june promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion')]
    ],
    'header'=>__('m_report_june_promo.june 2020 promotion'),
])

<div class="row">
    <div class="col-lg-4">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('m_report_june_promo.direct downline 1') }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.member_id'), 'dd1 member id', 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.name'), 'dd1 name', 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.current promo credits'), 'dd1 current promo credits', 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
    <div class="col-lg-4 col-xs-12">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('m_report_june_promo.direct downline 2') }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-2', __('m_report_june_promo.member_id'), 'dd2 member id', 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-2', __('m_report_june_promo.name'), 'dd2 name', 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-2', __('m_report_june_promo.current promo credits'), 'dd2 current promo credits', 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
    <div class="col-lg-4 col-xs-12">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('m_report_june_promo.direct downline 3') }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-3', __('m_report_june_promo.member_id'), 'dd3 member id', 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-3', __('m_report_june_promo.name'), 'dd3 name', 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-3', __('m_report_june_promo.current promo credits'), 'dd3 current promo credits', 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
</div>

@endsection















