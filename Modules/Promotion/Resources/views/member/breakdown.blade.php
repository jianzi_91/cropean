@extends('templates.member.master')
@section('title', __('m_page_title.june promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion')]
    ],
    'header'=>__('m_report_june_promo.june 2020 promotion'),
])

<div class="row">
    @foreach($payout->breakdowns as $breakdown)
    <div class="col-lg-4">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('m_report_june_promo.direct downline') . ' ' . $breakdown->ranking }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.member_id'), $breakdown->member->member_id, 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.name'), $breakdown->member->name, 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo_breakdown.capital credit qualified'), amount_format($breakdown->initial_amount), 'col-7', 'col-5') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.current promo credits'), amount_format(bcmul($breakdown->amount, 0.3, 2)), 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
    @endforeach
</div>

<div class="card">
    <div class="card-content">
      <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_june_promo.direct downline report') }}</h4>
            <div class="d-flex flex-row align-items-center">
            <div class="table-total-results mr-2">{{ __('m_report_june_promo.total results:') }} {{ $payouts->count() }}</div>
                {{ Form::formTabSecondary(__('m_report_june_promo.export table'), route('member.report.promotion.export'))}}
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_june_promo.date') }}</th>
                <th>{{ __('m_report_june_promo.promo credits') }}</th>
                <th>{{ __('m_report_june_promo.direct downline 1 capital credit balance') }}</th>
                <th>{{ __('m_report_june_promo.direct downline 2 capital credit balance') }}</th>
                <th>{{ __('m_report_june_promo.direct downline 3 capital credit balance') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($payouts as $payout)
                <tr>
                    <td>{{ $payout->run_date }}</td>
                    <td>{{ amount_format($payout->amount) }}</td>
                    <td>{{ isset($payout->first_downline_capital_credit) ? amount_format($payout->first_downline_capital_credit) : '-' }}</td>
                    <td>{{ isset($payout->second_downline_capital_credit) ? amount_format($payout->second_downline_capital_credit) : '-' }}</td>
                    <td>{{ isset($payout->third_downline_capital_credit) ? amount_format($payout->third_downline_capital_credit) : '-' }}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_june_promo.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>


@endsection