@extends('templates.member.master')
@section('title', __('m_page_title.june promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion')]
    ],
    'header'=>__('m_report_june_promo.june 2020 promotion preview'),
])

<div class="row">
    @foreach($contributors as $rank => $contributor)
    <div class="col-lg-4">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('m_report_june_promo.direct downline') . ' ' . $rank }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.member_id'), $contributor['member_id'], 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.name'), $contributor['name'], 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('m_report_june_promo.capital credits'), amount_format($contributor['capital_credit']), 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
    @endforeach
</div>

@endsection















