@extends('templates.admin.master')
@section('title', __('a_page_title.june promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion')]
    ],
    'header'=> __('a_report_june_promo.june 2020 promotion')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_june_promo.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_june_promo.member id'), [], false) }}
</div>
{{-- <div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_june_promo.include downlines'), ['isChecked' => request('member_id_downlines', false)]) }}
</div> --}}
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_report_june_promo.name'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_june_promo.member id') }}</th>
                <th>{{ __('a_report_june_promo.name') }}</th>
                <th>{{ __('a_report_june_promo.email') }}</th>
                <th>{{ __('a_report_june_promo.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                @if($member->has_downlines)
                <tr>
                    <td>{{ $member->member_id }}</td>
                    <td>{{ $member->name }}</td>
                    <td>{{ $member->email }}</td>
                    <td>
                      <a href="{{ route('admin.report.promotion.preview.breakdown', $member->id) }}" title="{{ __('a_report_june_promo.view') }}">
                        <i class="bx bx-show-alt"></i>
                      </a>
                    </td>
                </tr>
                @endif
                @endforeach
                <!-- @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_june_promo.no records') ]) -->
            </tbody>
        @endcomponent
    </div>
</div>
{!! $members->render() !!}
@endsection