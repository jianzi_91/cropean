@extends('templates.admin.master')
@section('title', __('a_page_title.august promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.august 2020 promotion')]
    ],
    'header'=>__('a_report_august_promo.august 2020 promotion')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_august_promo.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_august_promo.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_report_august_promo.name'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_august_promo.member id') }}</th>
                <th>{{ __('a_report_august_promo.name') }}</th>
                <th>{{ __('a_report_august_promo.email') }}</th>
                <th>{{ __('a_report_august_promo.qualified capital credits') }}</th>
                <th>{{ __('a_report_august_promo.promo credits issued') }}</th>
                <th>{{ __('a_report_august_promo.qualified capital credits balance') }}</th>
                <th>{{ __('a_report_august_promo.promo credits balance') }}</th>
                <th>{{ __('a_report_august_promo.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($payouts as $payout)
                <tr>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->name }}</td>
                    <td>{{ $payout->email }}</td>
                    <td>{{ amount_format(bcdiv($payout->payout_initial_amount, 0.5, 2), 2) }}</td>
                    <td>{{ amount_format($payout->payout_initial_amount, 2) }}</td>
                    <td>{{ amount_format(bcdiv($payout->payout_amount, 0.5, 2), 2) }}</td>
                    <td>{{ amount_format($payout->payout_amount, 2) }}</td>
                    <td>
                      <a href="{{ route('admin.report.august-promotion.show', $payout->payout_id) }}" title="{{ __('a_report_august_promo.view') }}">
                        <i class="bx bx-show-alt"></i>
                      </a>
                    </td>
                </tr>
                @endforeach
                <!-- @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_august_promo.no records') ]) -->
            </tbody>
        @endcomponent
    </div>
    {!! $payouts->render() !!}
</div>

@endsection