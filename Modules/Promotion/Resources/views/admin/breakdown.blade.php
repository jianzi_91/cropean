@extends('templates.admin.master')
@section('title', __('a_page_title.june promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion'), 'route'=>route('admin.report.promotion.index')],
        ['name'=>__('s_breadcrumb.direct downline report')]
    ],
    'header'=>__('a_report_june_promo_breakdown.direct downline report'),
    'backRoute'=> route('admin.report.promotion.index'),
])

<div class="row">
    @foreach($payout->breakdowns as $breakdown)
    <div class="col-lg-4">
      <div class="card">
          <div class="card-content">
              <div class="card-body">
                  <h4 class="card-title pb-1 m-0">{{ __('a_report_june_promo_breakdown.direct downline') . ' ' . $breakdown->ranking }}</h4>
                  {{ Form::formWidgetInfo('direct-downline-1', __('a_report_june_promo_breakdown.member_id'), $breakdown->member->member_id, 'col-6', 'col-6') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('a_report_june_promo_breakdown.name'), $breakdown->member->name, 'col-8', 'col-4') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('a_report_june_promo_breakdown.email'), $breakdown->member->email, 'col-5', 'col-7') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('a_report_june_promo_breakdown.capital credit qualified'), amount_format($breakdown->initial_amount), 'col-7', 'col-5') }}
                  {{ Form::formWidgetInfo('direct-downline-1', __('a_report_june_promo_breakdown.current promo credits'), amount_format(bcmul($breakdown->amount, 0.3, 2)), 'col-7', 'col-5') }}
              </div>
          </div>
      </div>
    </div>
    @endforeach
</div>

<div class="card">
    <div class="card-content">
      <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_june_promo_breakdown.direct downline report') }}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_report_june_promo_breakdown.total results:') }} {{ $payouts->count() }}</div>
                {{ Form::formTabSecondary(__('a_report_june_promo_breakdown.export table'), route('admin.report.promotion.breakdown.export', $payoutId))}}
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_june_promo_breakdown.date') }}</th>
                <th>{{ __('a_report_june_promo_breakdown.promo credits') }}</th>
                <th>{{ __('a_report_june_promo_breakdown.direct downline 1 capital credit balance') }}</th>
                <th>{{ __('a_report_june_promo_breakdown.direct downline 2 capital credit balance') }}</th>
                <th>{{ __('a_report_june_promo_breakdown.direct downline 3 capital credit balance') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($payouts as $payout)
                <tr>
                    <td>{{ $payout->run_date }}</td>
                    <td>{{ amount_format($payout->amount) }}</td>
                    <td>{{ isset($payout->first_downline_capital_credit) ? amount_format($payout->first_downline_capital_credit) : '-' }}</td>
                    <td>{{ isset($payout->second_downline_capital_credit) ? amount_format($payout->second_downline_capital_credit) : '-' }}</td>
                    <td>{{ isset($payout->third_downline_capital_credit) ? amount_format($payout->third_downline_capital_credit) : '-' }}</td>
                </tr>
                @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_june_promo.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>


@endsection















