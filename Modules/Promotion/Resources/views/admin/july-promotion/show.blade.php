@extends('templates.admin.master')
@section('title', __('a_page_title.july promotion report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.june 2020 promotion')]
    ],
    'header'=>__('a_report_july_promo.july 2020 promotion details'),
    'backRoute' => route('admin.report.july-promotion.index'),
])

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_july_promo.date') }}</th>
                <th>{{ __('a_report_july_promo.promo credits') }}</th>
                <th>{{ __('a_report_july_promo.qualified capital credits balance') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($snapshots as $snapshot)
                <tr>
                    <td>{{ $snapshot->run_date }}</td>
                    <td>{{ amount_format($snapshot->snapshot_amount, 2) }}</td>
                    <td>{{ amount_format(bcdiv($snapshot->snapshot_amount, 0.5, 2),2) }}</td>
                </tr>
                @endforeach
                <!-- @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_july_promo.no records') ]) -->
            </tbody>
        @endcomponent
    </div>
    {!! $snapshots->render() !!}
</div>

@endsection