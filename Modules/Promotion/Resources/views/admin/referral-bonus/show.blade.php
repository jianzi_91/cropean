@extends('templates.admin.master')
@section('title', __('a_page_title.referral bonus details report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.referral bonus details')]
    ],
    'header'=>__('a_report_referral_bonus.referral bonus details'),
    'backRoute' => route('admin.report.referral.index'),
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_referral_bonus.date'), array(), false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_referral_bonus.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_referral_bonus.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_report_referral_bonus.name'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('type', ['deposit' => __('a_report_referral_bonus.deposit'), 'conversion' => __('a_report_referral_bonus.conversion')], request('type'), __('a_report_referral_bonus.type'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_referral_bonus.date') }}</th>
                <th>{{ __('a_report_referral_bonus.name') }}</th>
                <th>{{ __('a_report_referral_bonus.member id') }}</th>
                <th>{{ __('a_report_referral_bonus.email') }}</th>
                <th>{{ __('a_report_referral_bonus.type') }}</th>
                <th>{{ __('a_report_referral_bonus.amount') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($breakdowns as $breakdown)
                <tr>
                    <td>{{ $breakdown->payout_date }}</td>
                    <td>{{ $breakdown->member_name }}</td>
                    <td>{{ $breakdown->member_id }}</td>
                    <td>{{ $breakdown->member_email }}</td>
                    <td>{{ $breakdown->type }}</td>
                    <td>{{ amount_format($breakdown->amount,2) }}</td>
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('a_report_referral_bonus.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>

    {!! $breakdowns->render() !!}
</div>

@endsection