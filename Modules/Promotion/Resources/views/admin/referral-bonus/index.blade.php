@extends('templates.admin.master')
@section('title', __('a_page_title.referral bonus report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.referral bonus')]
    ],
    'header'=>__('a_report_referral_bonus.referral bonus')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_referral_bonus.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_referral_bonus.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_report_referral_bonus.name'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_referral_bonus.name') }}</th>
                <th>{{ __('a_report_referral_bonus.member id') }}</th>
                <th>{{ __('a_report_referral_bonus.email') }}</th>
                <th>{{ __('a_report_referral_bonus.total deposit amount') }}</th>
                <th>{{ __('a_report_referral_bonus.cash credit issued') }}</th>
                <th>{{ __('a_report_referral_bonus.actions') }}</th>
            </tr>
            </thead>
            <tbody>
                @forelse($payouts as $payout)
                <tr>
                    <td>{{ $payout->name }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->email }}</td>
                    <td>{{ amount_format($payout->amount, 2) }}</td>
                    <td>{{ amount_format($payout->payout, 2) }}</td>
                    <td>
                      <a href="{{ route('admin.report.referral.show', $payout->user_id) }}" title="{{ __('a_report_referral_bonus.view') }}">
                        <i class="bx bx-show-alt"></i>
                      </a>
                    </td>
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', ['span' => 7, 'text' => __('a_report_referral_bonus.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>

@endsection