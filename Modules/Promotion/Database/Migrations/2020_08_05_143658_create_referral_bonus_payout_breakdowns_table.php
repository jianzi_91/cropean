<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralBonusPayoutBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_bonus_payout_breakdowns', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unsignedInteger('referral_bonus_payout_id')->nullable();
            $table->unsignedInteger('referrer_id');
            $table->unsignedInteger('user_id');
            $table->enum('type', ['deposit', 'conversion']);
            $table->string('reference_id');
            $table->decimal('amount', 40,20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_bonus_payout_breakdowns');
    }
}
