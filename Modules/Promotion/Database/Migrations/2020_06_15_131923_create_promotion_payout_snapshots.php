<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionPayoutSnapshots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_payout_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
            $table->unsignedInteger('payout_id');
            $table->unsignedInteger('user_id');
            $table->decimal('amount', 40, 20);
            $table->unsignedInteger('first_downline_id')->nullable();
            $table->decimal('first_downline_capital_credit', 40, 20)->nullable();
            $table->unsignedInteger('second_downline_id')->nullable();
            $table->decimal('second_downline_capital_credit', 40, 20)->nullable();
            $table->unsignedInteger('third_downline_id')->nullable();
            $table->decimal('third_downline_capital_credit', 40, 20)->nullable();
            $table->date('run_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_payout_snapshots');
    }
}
