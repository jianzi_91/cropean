<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;
use Modules\Tree\Contracts\SponsorTreeContract;
use Illuminate\Support\Facades\DB;

class PopulateExistingMemberReferralMidWithSponsorMidTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $treeRepo = resolve(SponsorTreeContract::class);

        DB::beginTransaction();

        try {
            foreach ($users as $user) {
                if ($user->isMember && !$user->is_system_generated) {
                    $directUpline = $treeRepo->directUpline($user->id);
                    $directUpline = User::find($directUpline->user->id);

                    $user->update([
                        'referrer_id' => $directUpline->member_id,
                    ]);
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw new \Exception($e->getMessage());
        }
    }
}
