<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class ReferralBonusSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'referral_bonus',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'referral_bonus_start_date',
                'setting_category_id' => SettingCategory::where('name', 'referral_bonus')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'referral_bonus_end_date',
                'setting_category_id' => SettingCategory::where('name', 'referral_bonus')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'referral_bonus_payout_percentage',
                'setting_category_id' => SettingCategory::where('name', 'referral_bonus')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'referral_bonus_start_date')->first()->id,
                'value'      => '2020-08-17',
            ],
            [
                'setting_id' => Setting::where('name', 'referral_bonus_end_date')->first()->id,
                'value'      => '2020-08-31',
            ],
            [
                'setting_id' => Setting::where('name', 'referral_bonus_payout_percentage')->first()->id,
                'value'      => '0.05',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
