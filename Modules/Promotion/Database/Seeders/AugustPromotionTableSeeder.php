<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Promotion\Models\Promotion;

class AugustPromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::create([
            'name'             => 'august_promotion',
            'name_translation' => 'August Promotion',
            'code'             => 'AUGP2020',
            'start_date'       => '2020-08-01',
            'end_date'         => '2020-11-16',
            'is_active'        => true,
        ]);
    }
}
