<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Contracts\BankTransactionTypeContract;

class ReferralBonusTransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactionTypeRepo = resolve(BankTransactionTypeContract::class);

        $transactionTypeRepo->add([
            'name'             => 'referral_bonus',
            'name_translation' => 'referral bonus',
            'is_active'        => true,
        ]);
    }
}
