<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ReferralBonusPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_promotion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Promotion',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_referral_bonus_report_view',
                        'title' => 'Admin Referral Bonus Report View',
                    ],
                ]
            ],
            'member_promotion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Promotion',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_referral_bonus_report_view',
                        'title' => 'Member Referral Bonus Report View',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_referral_bonus_report_view',
            ],
            Role::ADMIN => [
                'admin_referral_bonus_report_view',
            ],
            Role::MEMBER => [
                'member_referral_bonus_report_view',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
