<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Promotion\Models\Promotion;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class JunePromotionTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_promotion',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        Promotion::create([
            'name'             => 'june_promotion',
            'name_translation' => 'June Promotion',
            'code'             => 'JP2020',
            'start_date'       => '2020-06-01',
            'end_date'         => '2020-06-30',
            'is_active'        => true,
        ]);
    }
}
