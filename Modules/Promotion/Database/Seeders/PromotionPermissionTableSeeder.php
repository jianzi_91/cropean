<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class PromotionPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_promotion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Promotion',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_june_promotion_report_view',
                        'title' => 'Admin June Promotion Report View',
                    ],
                    [
                        'name'  => 'admin_july_promotion_report_view',
                        'title' => 'Admin June Promotion Report View',
                    ],
                ]
            ],
            'admin_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Credit System',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_credit_system_promotion_credit_list',
                        'title' => 'Admin Credit System Cash Credit List ',
                    ],
                    [
                        'name'  => 'admin_credit_system_promotion_credit_export',
                        'title' => 'Admin Credit System Cash Credit Export ',
                    ],
                ]
            ],
            'member_promotion' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Promotion',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_june_promotion_report_view',
                        'title' => 'Member June Promotion Report View',
                    ],
                ],
            ],
            'member_credit_statement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Credit Statement',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_credit_statement_promotion_credit_list',
                        'title' => 'Member Credit Statement Promotion Credit List',
                    ],
                    [
                        'name'  => 'member_credit_statement_promotion_credit_export',
                        'title' => 'Member Credit Statement Promotion Credit Export',
                    ],
                ],
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_june_promotion_report_view',
                'admin_july_promotion_report_view',
                'admin_credit_system_promotion_credit_list',
                'admin_credit_system_promotion_credit_export',
            ],
            Role::ADMIN => [
                'admin_june_promotion_report_view',
                'admin_july_promotion_report_view',
                'admin_credit_system_promotion_credit_list',
                'admin_credit_system_promotion_credit_export',
            ],
            Role::MEMBER => [
                'member_june_promotion_report_view',
                'member_credit_statement_promotion_credit_list',
                'member_credit_statement_promotion_credit_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
