<?php

namespace Modules\Promotion\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Promotion\Models\Promotion;

class JulyPromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::create([
            'name'             => 'july_promotion',
            'name_translation' => 'July Promotion',
            'code'             => 'JULP2020',
            'start_date'       => '2020-07-01',
            'end_date'         => '2020-10-31',
            'is_active'        => true,
        ]);
    }
}
