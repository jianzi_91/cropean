<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface DepositStatusContract extends CrudContract, SlugContract, HistoryableContract
{
    const PENDING   = 'pending';
    const APPROVED  = 'approved';
    const REJECTED  = 'rejected';
    const CANCELLED = 'cancelled';

    /**
     * Move status of the deposit
     *
     * @param int $depositId
     * @param int $statusId
     * @param string $remarks
     * @param int $updatedBy
     *
     * @return UserStatusHistory
     */
    public function moveStatus($depositId, $statusId, $remarks = null, $updatedBy = null);
}
