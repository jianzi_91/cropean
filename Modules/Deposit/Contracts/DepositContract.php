<?php

namespace Modules\Deposit\Contracts;

use Modules\Core\Contracts\LockContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositContract extends CrudContract, LockContract
{
}
