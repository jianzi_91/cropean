<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositTransactionContract extends CrudContract
{
}
