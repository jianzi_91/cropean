<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositBankContract extends CrudContract
{
    /**
     * Get all deposit banks given a method ID.
     *
     * @param int $depositMethodId
     * @param int $userId
     * @return mixed
     */
    public function getForDepositMethod(int $depositMethodId, int $userId);

    /**
     * Get by deposit type ID.
     *
     * @param int $depositTypeId
     * @return mixed
     */
    public function getByType(int $depositTypeId);
}
