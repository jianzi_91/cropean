<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface DepositInvoiceContract extends CrudContract, SlugContract
{
    // add functions here
}
