<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface DepositSettingContract extends CrudContract, ActiveContract, SlugContract
{
}
