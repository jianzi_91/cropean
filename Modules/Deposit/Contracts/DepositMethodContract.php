<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface DepositMethodContract extends CrudContract, SlugContract
{
    const TELEGRAPHIC_TRANSFER_VIP = 'telegraphic transfer vip';
    const TELEGRAPHIC_TRANSFER     = 'telegraphic transfer';

    /**
     * Get methods which the user is eligible for.
     *
     * @param int $userId
     * @return mixed
     */
    public function getUserEligibleMethods(int $userId);
}
