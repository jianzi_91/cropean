<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositBankDetailContract extends CrudContract
{
}
