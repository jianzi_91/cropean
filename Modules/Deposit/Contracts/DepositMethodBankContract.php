<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositMethodBankContract extends CrudContract
{
    /**
     * Delete records that are linked to a deposit bank.
     *
     * @param int $depositBankId
     * @return mixed
     */
    public function deleteForBank(int $depositBankId);

    /**
     * Get banka ccounts for method.
     *
     * @param int $depositMethodId
     * @return mixed
     */
    public function getBankAccountsForMethod(int $depositMethodId);

    /**
     * Set 1 single bank account for method.
     *
     * @param int $depositMethodId
     * @param int $depositBankId
     * @return mixed
     */
    public function setSingleBankAccount(int $depositMethodId, int $depositBankId);

    public function getCurrentDepositBank($depositMethodId);
}
