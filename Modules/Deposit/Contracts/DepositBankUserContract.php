<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositBankUserContract extends CrudContract
{
    /**
     * Delete records that are linked to a deposit bank.
     *
     * @param int $depositBankId
     * @return mixed
     */
    public function deleteForBank(int $depositBankId);

    /**
     * Get banka ccounts for method.
     *
     * @param int $depositMethodId
     * @param int $userId
     * @return mixed
     */
    public function getBankAccountsForMethodAndUser(int $depositMethodId, int $userId);

    /**
     * Get banka ccounts for method.
     *
     * @param int $depositMethodId
     * @param int $userId
     * @return mixed
     */
    public function getDepositBankAccountsForMethodAndUser(int $depositMethodId, int $userId);

    /**
     * Assign bank to user.
     *
     * @param int $userId
     * @param int $depositMethodId
     * @param int $depositBankId
     * @return mixed
     */
    public function assignBankToUser(int $userId, int $depositMethodId, int $depositBankId);

    /**
     * Unassign bank from user.
     *
     * @param int $userId
     * @param int $depositMethodId
     * @param int $depositBankId
     * @return mixed
     */
    public function unassignBankFromUser(int $userId, int $depositMethodId, int $depositBankId);

    /**
     * Assign all banks to user.
     *
     * @param int $userId
     * @param int $depositMethodId
     * @return mixed
     */
    public function assignAllBanksToUser(int $userId, int $depositMethodId);

    /**
     * Unassign all banks from user.
     *
     * @param int $userId
     * @param int $depositMethodId
     * @return mixed
     */
    public function unassignAllBanksFromUser(int $userId, int $depositMethodId);
}
