<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface DepositTypeContract extends CrudContract, SlugContract
{
    const VIP     = 'vip';
    const NON_VIP = 'non vip';
}
