<?php

namespace Modules\Deposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface DepositAttachmentContract extends CrudContract
{
    /**
     * Get image data.
     *
     * @param int $attachmentId
     * @param bool $download
     * @return mixed
     */
    public function get(int $attachmentId, bool $download = false);
}
