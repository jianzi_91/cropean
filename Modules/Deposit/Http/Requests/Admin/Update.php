<?php

namespace Modules\Deposit\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action'                 => ['bail', 'required', 'in:approve,reject'],
            'remarks'                => ['bail', 'required_if:action,reject'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'amount' => __('a_deposit_request_detail.amount')
        ];
    }
}
