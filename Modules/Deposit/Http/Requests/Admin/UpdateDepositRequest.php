<?php

namespace Modules\Deposit\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Models\Deposit;
use Modules\Deposit\Repositories\DepositStatusRepository;

class UpdateDepositRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status' => 'required|integer|gt:0'
        ];

        /** @var DepositStatusRepository $depositStatusContract */
        $depositStatusContract = resolve(DepositStatusContract::class);
        $rejectedStatus        = $depositStatusContract->findBySlug($depositStatusContract::REJECTED);
        if ($rejectedStatus->id == $this->status) {
            $rules['remarks'] = 'required|max:255';
        }

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            /**
            $deposits = Deposit::whereIn('id',[$this->deposit])->with('status.status')->get();

            /** @var DepositStatusRepository $depositStatusContract
            $depositStatusContract = resolve(DepositStatusContract::class);
            $rejectedStatus = $depositStatusContract->findBySlug($depositStatusContract::REJECTED);
            $cancelledStatus = $depositStatusContract->findBySlug($depositStatusContract::CANCELLED);

            foreach ($deposits AS $deposit)
            {
                if(in_array($deposit->status->status->id, [$rejectedStatus->id, $cancelledStatus->id]))
                {
                    $validator->errors()->add('status', __('a_deposits.cannot update rejected deposits'));
                    Session::flash('error', __('a_deposits.cannot update rejected deposits'));
                }
            }
             */
        });
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
        ];
    }
}
