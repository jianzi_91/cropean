<?php

namespace Modules\Deposit\Http\Requests\Admin\DepositBank;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Deposit\Http\Rules\SecurityPasswordMatch;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'bank_name'    => ['bail', 'required'],
            'bank_branch'  => ['bail', 'required'],
            'account_name' => ['bail', 'required'],
//            'unique_id'               => ['bail', 'required', 'unique:deposit_banks,unique_id,' . $this->deposit_bank],
            'account_number'          => ['bail', 'required'],
            'deposit_max'             => ['bail', 'required', 'numeric', new NotScientificNotation(), new MaxDecimalPlaces(2)],
            'additional_instructions' => ['bail', 'nullable'],
            'deposit_timing_from'     => ['bail', 'nullable'],
            'deposit_timing_to'       => ['bail', 'nullable'],
            'is_24_hours'             => ['nullable', 'in:1'],
            'secondary_password'      => ['bail', 'required', new SecondaryPasswordMatch()],
            'security_password'       => ['bail', 'required', new SecurityPasswordMatch()],
        ];

        if (!empty($this->deposit_timing_from) || !empty($this->deposit_timing_to)) {
            if ($this->deposit_timing_from == $this->deposit_timing_to) {
                $rules['deposit_timing_from'] = ['required', 'date_format:Hi'];
                $rules['deposit_timing_to']   = ['required', 'date_format:Hi'];
            } else {
                $rules['deposit_timing_from'] = ['required', 'date_format:Hi', 'before:deposit_timing_to'];
                $rules['deposit_timing_to']   = ['required', 'date_format:Hi', 'after:deposit_timing_from'];
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'deposit_timing_from.required' => __('a_deposit_request_detail.deposit from is require if deposit to exist'),
            'deposit_timing_to.required'   => __('a_deposit_request_detail.deposit to is require if deposit from exist')
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'bank_name'               => __('a_deposit_request_detail.bank name'),
            'account_name'            => __('a_deposit_request_detail.bank account name'),
            'unique_id'               => __('a_deposit_request_detail.unique identifier'),
            'account_number'          => __('a_deposit_request_detail.account number'),
            'bank_branch'             => __('a_deposit_request_detail.bank branch code'),
            'deposit_max'             => __('a_deposit_request_detail.maximum deposit amount'),
            'additional_instructions' => __('a_deposit_request_detail.additional instructions'),
            'deposit_timing_from'     => __('a_deposit_request_detail.deposit from'),
            'deposit_timing_to'       => __('a_deposit_request_detail.deposit to'),
            'is_24_hours'             => __('a_deposit_request_detail.is 24 hours'),
            'secondary_password'      => __('a_deposit_request_detail.secondary password'),
            'security_password'       => __('a_deposit_request_detail.security password'),
        ];
    }
}
