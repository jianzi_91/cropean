<?php

namespace Modules\Deposit\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class UpdateDepositSettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'min' => [
                'bail',
                'required',
                'integer',
                'gt:0',
                'lt:max',
                new NotScientificNotation(),
                new MaxDecimalPlaces(2),
            ],
            'max' => [
                'bail',
                'required',
                'integer',
                'gt:0',
                'gt:min',
                'max:' . config('deposit.max_amount'),
                new NotScientificNotation(),
                new MaxDecimalPlaces(2),
            ],
            'multiple'             => 'bail|required|integer|numeric|gte:0|regex:/^[0-9.]+$/|max:' . config('deposit.max_multiple_of'),
            'admin_fee_percentage' => [
                'bail',
                'required',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'min.regex'      => __('a_deposit_settings.invalid amount format'),
            'max.regex'      => __('a_deposit_settings.invalid amount format'),
            'multiple.regex' => __('a_deposit_settings.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'min'      => __('a_deposit_settings.min amount'),
            'max'      => __('a_deposit_settings.max amount'),
            'multiple' => __('a_deposit_settings.multiple'),
        ];
    }
}
