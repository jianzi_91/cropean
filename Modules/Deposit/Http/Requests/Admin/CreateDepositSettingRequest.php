<?php

namespace Modules\Deposit\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class CreateDepositSettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'qr_file' => 'required|file|image',
            'title'   => 'required|max:255',
            'min'     => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                new NotScientificNotation(),
                new MaxDecimalPlaces(),
            ],
            'max' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                new NotScientificNotation(),
                'max:' . config('deposit.max_amount'),
                new MaxDecimalPlaces(),
            ],
            'processing_fee_percentage' => [
                'bail',
                'required',
                'numeric',
                'gte:0',
                new NotScientificNotation(),
                'max:' . config('deposit.max_processing_fee_percentage'),
                new MaxDecimalPlaces(),
            ],
            'multiple_of' => 'bail|required|numeric|gte:0|regex:/^[0-9.]+$/|max:' . config('deposit.max_multiple_of'),
            'is_active'   => 'required|integer'
        ];

        return $rules;
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->min > 0 && $this->max > 0 && $this->max < $this->min) {
                $validator->errors()->add('max', __('a_create_deposit_qr_code.max should be greater than min'));
            }
        });
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'min.regex'                       => __('a_create_deposit_qr_code.invalid amount format'),
            'max.regex'                       => __('a_create_deposit_qr_code.invalid amount format'),
            'processing_fee_percentage.regex' => __('a_create_deposit_qr_code.invalid amount format'),
            'multiple_of.regex'               => __('a_create_deposit_qr_code.invalid amount format'),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'min'                       => __('a_create_deposit_qr_code.min amount'),
            'max'                       => __('a_create_deposit_qr_code.max amount'),
            'title'                     => __('a_create_deposit_qr_code.title'),
            'processing_fee_percentage' => __('a_create_deposit_qr_code.processing_fee_percentage'),
            'multiple_of'               => __('a_create_deposit_qr_code.multiple of'),
            'is_active'                 => __('a_create_deposit_qr_code.status'),
            'qr_filename'               => __('a_create_deposit_qr_code.upload qr code'),
        ];
    }
}
