<?php

namespace Modules\Deposit\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Deposit\Http\Rules\UserCanUseBankAccount;
use Modules\Deposit\Http\Rules\UserCanUseDepositMethod;
use Modules\Deposit\Http\Rules\ValidDepositAmount;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
                new NotScientificNotation(),
                new MaxDecimalPlaces(2),
                new ValidDepositAmount($this->deposit_method_id),
            ],
            'deposit_method_id' => [
                'bail',
                'required',
                Rule::exists('deposit_methods', 'id')->where(function ($query) {
                    $query->where('is_active', true);
                }),
                new UserCanUseDepositMethod(),
            ],
            'deposit_bank_id' => [
                'bail',
                'required',
                Rule::exists('deposit_banks', 'id')->where(function ($query) {
                    $query->where('is_active', true);
                }),
                new UserCanUseBankAccount(),
            ],
            'secondary_password' => [
                'bail',
                'required',
                new SecondaryPasswordMatch(),
            ],
        ];

        if (!empty(request()->proof)) {
            foreach (request()->proof as $key => $value) {
                $return['proof.' . $key] = [
                    'bail',
                    'required',
                    'mimes:jpeg,png,pdf',
                    'max:' . config('deposit.deposit_attachment_max_size'),
                ];
            }
        } else {
            $return['proof.0'] = [
                'required',
            ];
        }

        return $return;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        $return = [
            'deposit_method_id'      => __('m_deposit_create.type'),
            'deposit_bank_id'        => __('m_deposit_create.bank'),
            'amount'                 => __('m_deposit_create.amount to deposit'),
            'proof.*'                => __('m_deposit_create.transaction proof'),
            'proof_receipt_number.*' => __('m_deposit_create.proof receipt number'),
            'secondary_password'     => __('m_deposit_create.secondary password'),
        ];

        if (!empty(request()->proof_receipt_number)) {
            foreach (request()->proof_receipt_number as $key => $value) {
                $return['proof_receipt_number.' . $key] = __('m_deposit_create.transaction proof');
                $return['proof.' . $key]                = __('m_deposit_create.transaction proof for receipt number :value', ['value' => $value]);
            }
        }

        if (!empty(request()->proof)) {
            foreach (request()->proof as $key => $value) {
                $return['proof.' . $key] = __('m_deposit_create.transaction proof');
            }
        }

        return $return;
    }
}
