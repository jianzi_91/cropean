<?php

namespace Modules\Deposit\Http\Controllers\Member;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositBankDetailContract;
use Modules\Deposit\Contracts\DepositContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Http\Requests\Member\CancelDepositRequest;
use Modules\Deposit\Http\Requests\Member\Store;
use Modules\Deposit\Queries\Member\DepositQuery;
use Modules\Deposit\Repositories\DepositRepository;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;

class DepositController extends Controller
{
    /**
     * Deposit repository.
     *
     * @var DepositRepository $depositRepo
     */
    protected $depositRepo;

    /**
     * Deposit method repository.
     *
     * @var DepositMethodContract
     */
    protected $depositMethodRepo;

    /**
     * Deposit bank repository.
     *
     * @var DepositBankContract
     */
    protected $depositBankRepo;

    /**
     * Exchange rate repository.
     *
     * @var ExchangeRateContract
     */
    protected $exchangeRateRepo;

    /**
     * Deposit query.
     *
     * @var DepositQuery
     */
    protected $depositQuery;

    protected $depositStatusRepository;

    protected $depositBankDetailRepo;

    /**
     * Class constructor.
     *
     * @param DepositContract $depositContract
     * @param DepositMethodContract $depositMethodContract
     * @param DepositBankContract $depositBankContract
     * @param ExchangeRateContract $exchangeRateContract
     * @param DepositQuery $depositQuery
     * @param DepositStatusContract $depositStatusContract
     * @param DepositBankDetailContract $depositBankDetailContract
     */
    public function __construct(
        DepositContract $depositContract,
        DepositMethodContract $depositMethodContract,
        DepositBankContract $depositBankContract,
        ExchangeRateContract $exchangeRateContract,
        DepositQuery $depositQuery,
        DepositStatusContract $depositStatusContract,
        DepositBankDetailContract $depositBankDetailContract
    ) {
        $this->depositRepo             = $depositContract;
        $this->depositMethodRepo       = $depositMethodContract;
        $this->depositBankRepo         = $depositBankContract;
        $this->exchangeRateRepo        = $exchangeRateContract;
        $this->depositQuery            = $depositQuery;
        $this->depositStatusRepository = $depositStatusContract;
        $this->depositBankDetailRepo   = $depositBankDetailContract;

        $this->middleware('permission:member_deposit_list')->only('index');
        $this->middleware('permission:member_deposit_create')->only('create', 'store');
        $this->middleware('permission:member_deposit_view')->only('view');
        $this->middleware('permission:member_deposit_cancel')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $deposits = $this->depositQuery
            ->setParameters($request->all())
            ->paginate();

        return view('deposit::member.index', compact('deposits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        if (!has_default_bank()) {
            return redirect(route('member.deposits.index'))->with('error', __('m_deposit_list.deposit not open now'));
        }
        if (!validate_deposit_timing()) {
            return redirect(route('member.deposits.index'))->with('error', __('m_deposit_list.deposit timing invalid'));
        }

        if (!auth()->user()->epoaContract) {
            return redirect(route('member.epoa.create'));
        }

        $exchangeRate = amount_format($this->exchangeRateRepo->findBySlug('CNY')->sell_rate, credit_precision());

        $usdCredit   = $request->user()->usd_credit;
        $totalCredit = $usdCredit;

        $bankAccounts      = $this->depositBankRepo->getForDepositMethod($this->depositMethodRepo->findBySlug($this->depositMethodRepo::TELEGRAPHIC_TRANSFER)->id, $request->user()->id);
        $bankAccountsArray = [];

        foreach ($bankAccounts as $key => $value) {
            $bankAccountsArray[$value->id] = $value->bank_name;
        }

        return view('deposit::member.create', compact('exchangeRate', 'usdCredit', 'totalCredit', 'bankAccountsArray'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws WebException
     */
    public function store(Store $request)
    {
        if (!has_default_bank()) {
            return redirect(route('member.deposits.index'))->with('error', __('m_deposit_list.deposit not open now'));
        }
        if (!validate_deposit_timing()) {
            return redirect(route('member.deposits.index'))->with('error', __('m_deposit_list.deposit timing invalid'));
        }

        DB::beginTransaction();

        $request->merge([
            'locale' => session()->get('locale')
        ]);

        try {
            User::lockForUpdate()->find(auth()->user()->id);

            $this->depositRepo->request($request);

            DB::commit();

            return redirect(route('member.deposits.index'))->with('success', __('m_deposit_list.deposit created successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.deposits.index'))->withMessage(__('m_deposit_list.unable to create deposit request'));
        }
    }

    /**
     * Show the form for specified resource.
     *
     */
    public function show(Request $request, $id)
    {
        $deposit = $this->depositRepo->findOrFail($id);

        if ($deposit->user_id != auth()->user()->id) {
            abort('403');
        }

        $status = $deposit->status->status->name;

        $usdCredit   = $request->user()->usd_credit;
        $totalCredit = $usdCredit;

        $depositCurrency = $this->exchangeRateRepo->findBySlug($deposit->currency)->currency;

        $depositBankDetails = $this->depositBankDetailRepo->getModel()->where('deposit_id', '=', $id)->first();

        return view('deposit::member.show', compact('deposit', 'usdCredit', 'totalCredit', 'status', 'depositCurrency', 'depositBankDetails'));
    }

    /**
     * Destroy action. This is for cancelling deposit.
     *
     * @param CancelDepositRequest $request
     * @param int $id
     * @return RedirectResponse
     * @throws WebException
     */
    public function destroy(CancelDepositRequest $request, int $id)
    {
        DB::beginTransaction();

        try {
            $deposit = $this->depositRepo->lock($id);

            if ($deposit && $deposit->user_id == auth()->user()->id && $deposit->status->status->rawname == DepositStatusContract::PENDING) {
                $this->depositRepo->cancel($id);
            } else {
                DB::rollback();
                return redirect(route('member.deposits.index'))->with('error', __('m_deposit_list.unable to cancel'));
            }

            DB::commit();
            return redirect(route('member.deposits.index'))->with('success', __('m_deposit_list.deposit cancelled successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.deposits.index'))->withMessage($e->getMessage());
        }
    }

    /**
     * Ajax action. Get method settings.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMethod(Request $request, int $id)
    {
        $setting = $this->depositMethodRepo->find($id);

        $settingMin = amount_format($setting->min, credit_precision());
        $settingMul = amount_format($setting->multiple, credit_precision());

        return response()->json([
            'name'     => $setting->name,
            'min'      => amount_format($settingMin, credit_precision()),
            'multiple' => amount_format($settingMul, credit_precision()),
        ]);
    }

    /**
     * Ajax action. Get method settings.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetail(Request $request, int $id)
    {
        $this->depositRepo->find($request->deposit_id);

        $setting      = $this->depositMethodRepo->find($id);
        $exchangeRate = $this->exchangeRateRepo->findBySlug('CNY')->sell_rate;

        $settingMin = bcmul($setting->min, $exchangeRate, credit_precision());
        $settingMul = bcmul($setting->multiple, $exchangeRate, credit_precision());

        return response()->json([
            'name'     => $setting->name,
            'min'      => amount_format($settingMin, credit_precision()),
            'multiple' => amount_format($settingMul, credit_precision()),
        ]);
    }

    /**
     * Ajax action. Calculate admin fee and converted amount.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate(Request $request)
    {
        $data = $this->depositRepo->calculate($request);

        $totalAmountToDeposit    = 0;
        $totalAmountToDepositUsd = 0;

        $depositBank = $this->depositBankRepo->find($request->deposit_bank_id);

        if ($depositBank) {
            if ($request->has('amount') && is_numeric($request->amount)) {
                $totalAmountToDepositUsd = bcadd($data['amount'], $data['adminFee'], credit_precision());
                $totalAmountToDeposit    = bcadd(bcmul($data['amount'], $data['exchangeRate'], credit_precision()), bcmul($data['adminFee'], $data['exchangeRate'], credit_precision()), credit_precision());
            }

            return response()->json([
                'deposit_amount_usd'        => amount_format($data['amount'], credit_precision()),
                'admin_fee_usd'             => amount_format($data['adminFee'], credit_precision()),
                'admin_fee_percentage'      => bcmul($data['adminFeePercentage'], 100, credit_precision()),
                'total_deposit_amount_usd'  => amount_format($totalAmountToDepositUsd, credit_precision()),
                'exchange_rate'             => amount_format($data['exchangeRate'], credit_precision()),
                'total_amount_to_deposit'   => amount_format($totalAmountToDeposit, credit_precision()),
                'credit_type_exchange_rate' => amount_format($data['creditTypeExchangeRate'], credit_precision()),
                'issued_amount'             => amount_format($data['issuedAmount'], credit_precision()),
                'currency_from'             => $data['currencyFromTranslation'],
                'currency_to'               => $data['currencyToTranslation'],
                'currency_issue'            => $data['currencyIssueTranslation'],
            ]);
        }
    }

    /**
     * Ajax action. Get bank accounts.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBankAccounts(Request $request)
    {
        $bankAccounts = $this->depositBankRepo->getForDepositMethod($request->deposit_method_id, $request->user()->id);

        $bankAccountsArray = [];

        foreach ($bankAccounts as $bankAccount) {
            $bankAccountsArray[] = [
                'id'        => $bankAccount->id,
                'bank_name' => $bankAccount->bank_name,
            ];
        }

        return response()->json([
            'bank_accounts' => $bankAccountsArray,
        ]);
    }

    /**
     * Ajax action. Get bank account details.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBankAccountDetails(Request $request, int $id)
    {
        $bankAccount = $this->depositBankRepo->find($id);
        $method      = $this->depositMethodRepo->find($request->deposit_method_id);
        $deposits    = $method->depositMethodi18ns;
        $description = [];

        foreach ($deposits as $key => $value) {
            $description[$value->translatorLanguage->code] = $value->description;
        }

        return response()->json([
            'bank_name'               => $bankAccount->bank_name,
            'account_name'            => $bankAccount->account_name,
            'account_number'          => $bankAccount->account_number,
            'branch'                  => $bankAccount->bank_branch,
            'max_transaction'         => amount_format($bankAccount->max_transaction, credit_precision()),
            'deposit_max'             => amount_format($bankAccount->deposit_max, credit_precision()),
            'additional_instructions' => $bankAccount->additional_instructions,
            'deposit_timing_from'     => $bankAccount->deposit_timing_from,
            'deposit_timing_to'       => $bankAccount->deposit_timing_to,
            'description'             => $description,
        ]);
    }
}
