<?php

namespace Modules\Deposit\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Deposit\Contracts\DepositAttachmentContract;

class DepositAttachmentController extends Controller
{
    /**
     * Deposit attachment repository.
     *
     * @var DepositAttachmentContract
     */
    protected $attachmentRepo;

    /**
     * Class constructor.
     *
     * @param DepositAttachmentContract $attachmentContract
     */
    public function __construct(DepositAttachmentContract $attachmentContract)
    {
        $this->attachmentRepo = $attachmentContract;
        $this->middleware('permission:member_deposit_attachment_view')->only('get');
    }

    /**
     * Get image.
     *
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function get(Request $request, int $id)
    {
        $attachment = $this->attachmentRepo->find($id);

        if (empty($attachment) || $attachment->deposit->user_id != $request->user()->id) {
            abort(404);
        }

        return $this->attachmentRepo->get($id);
    }
}