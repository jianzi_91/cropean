<?php

namespace Modules\Deposit\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Deposit\Queries\Member\DepositQuery;
use QueryBuilder\Concerns\CanExportTrait;

class DepositExportController extends Controller
{
    use CanExportTrait;

    /**
     * Deposit query.
     *
     * @var DepositQuery
     */
    protected $depositQuery;

    /**
     * Class constructor.
     *
     * @param DepositQuery $depositQuery
     */
    public function __construct(DepositQuery $depositQuery)
    {
        $this->depositQuery = $depositQuery;
        $this->middleware('permission:member_deposit_export')->only('export');
    }

    /**
     * Export action.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->depositQuery->setParameters($request->all()), 'deposits_' . now() . '.xlsx', auth()->user(), 'Export Deposit on ' . now());
    }
}
