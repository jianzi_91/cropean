<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Deposit\Contracts\DepositAttachmentContract;

class DepositAttachmentController extends Controller
{
    /**
     * Deposit attachment repository.
     *
     * @var DepositAttachmentContract
     */
    protected $attachmentRepo;

    /**
     * Class constructor.
     *
     * @param DepositAttachmentContract $attachmentContract
     */
    public function __construct(DepositAttachmentContract $attachmentContract)
    {
        $this->attachmentRepo = $attachmentContract;
        $this->middleware('permission:admin_deposit_attachment_view')->only('get');
    }

    /**
     * Get attachment.
     *
     * @param Request $request
     * @param int $id
     * @param bool $noDownload
     * @return mixed
     */
    public function get(Request $request, int $id, bool $download = true)
    {
        $attachment = $this->attachmentRepo->find($id);

        if (empty($attachment)) {
            abort(404);
        }

        return $this->attachmentRepo->get($id);
    }
}
