<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositMethodBankContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositTypeContract;
use Modules\Deposit\Http\Requests\Admin\DepositBank\Store;
use Modules\Deposit\Http\Requests\Admin\DepositBank\Update;
use Modules\Deposit\Queries\Admin\DepositBankQuery;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\ApiException;
use Plus65\Utility\Exceptions\WebException;

class DepositBankController extends Controller
{
    /**
     * Deposit bank repository.
     *
     * @var DepositBankContract
     */
    protected $depositBankRepo;

    /**
     * Deposit type repository.
     *
     * @var DepositTypeContract
     */
    protected $depositTypeRepo;

    /**
     * Deposit method bank repository.
     *
     * @var DepositMethodBankContract
     */
    protected $methodBankRepo;

    /**
     * Deposit bank user repository.
     *
     * @var DepositBankUserContract
     */
    protected $depositBankUserRepo;

    /**
     * Deposit method repository.
     *
     * @var DepositMethodContract
     */
    protected $depositMethodRepo;

    /**
     * User repository.
     *
     * @var UserContract
     */
    protected $userRepo;

    /**
     * Deposit bank query.
     *
     * @var DepositBankQuery
     */
    protected $query;

    protected $exchangeRateRepository;

    /**
     * Class constructor.
     *
     * @param DepositBankContract $depositBankContract
     * @param DepositTypeContract $depositTypeContract
     * @param DepositMethodContract $depositMethodContract
     * @param DepositMethodBankContract $methodBankContract
     * @param DepositBankUserContract $depositBankUserContract
     * @param UserContract $userContract
     * @param DepositBankQuery $query
     */
    public function __construct(
        DepositBankContract $depositBankContract,
        DepositTypeContract $depositTypeContract,
        DepositMethodContract $depositMethodContract,
        DepositMethodBankContract $methodBankContract,
        DepositBankUserContract $depositBankUserContract,
        UserContract $userContract,
        ExchangeRateContract $exchangeRateContract,
        DepositBankQuery $query
    ) {
        $this->depositBankRepo        = $depositBankContract;
        $this->depositTypeRepo        = $depositTypeContract;
        $this->methodBankRepo         = $methodBankContract;
        $this->depositBankUserRepo    = $depositBankUserContract;
        $this->depositMethodRepo      = $depositMethodContract;
        $this->userRepo               = $userContract;
        $this->exchangeRateRepository = $exchangeRateContract;
        $this->query                  = $query;

        $this->middleware('permission:admin_set_default_nonvip_bank')->only(['nonVipBank', 'updateNonVipBank']);
        $this->middleware('permission:admin_deposit_bank_list')->only('index');
        $this->middleware('permission:admin_deposit_bank_create')->only(['create', 'store']);
        $this->middleware('permission:admin_deposit_bank_edit')->only(['edit', 'update']);
        $this->middleware('permission:admin_deposit_bank_delete')->only('destroy');
    }

    /**
     * Index action.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $banks = $this->query->setParameters($request->all())->paginate();

        $depositMethodBank = $this->methodBankRepo->getModel()->whereNull('deleted_at')->first();

        return view('deposit::admin.bank.index', compact('banks', 'depositMethodBank'));
    }

    /**
     * Create action.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $currencyId = $this->exchangeRateRepository->findBySlug($this->exchangeRateRepository::CNY)->id;

        return view('deposit::admin.bank.create', compact('currencyId'));
    }

    /**
     * Store action.
     *
     * @param Store $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function store(Store $request)
    {
        DB::beginTransaction();

        $currency = $this->exchangeRateRepository->find($request->currency_id)->rawcurrency;

        if ($request->is_24_hours) {
            $depositTimingFrom = null;
            $depositTimingTo   = null;
        } else {
            $depositTimingFrom = Carbon::parse($request->deposit_timing_from)->format("H:i:s");
            $depositTimingTo   = Carbon::parse($request->deposit_timing_to)->format("H:i:s");
        }

        try {
            $deposit_bank = $this->depositBankRepo->add([
                'deposit_type_id'         => $this->depositTypeRepo->findBySlug($this->depositTypeRepo::NON_VIP)->id,
                'bank_name'               => $request->bank_name,
                'bank_branch'             => $request->bank_branch,
                'account_name'            => $request->account_name,
                'account_number'          => $request->account_number,
                'currency'                => $currency,
                'deposit_max'             => $request->deposit_max,
                'additional_instructions' => $request->additional_instructions,
                'deposit_timing_from'     => $depositTimingFrom,
                'deposit_timing_to'       => $depositTimingTo,
                'is_active'               => true,
            ]);

            if ($request->is_default) {
                $telegraphicMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER);
                $this->methodBankRepo->setSingleBankAccount($telegraphicMethod->id, $deposit_bank->id);
            }

            DB::commit();

            return redirect()->route('admin.deposit-banks.index')
                ->with('success', __('a_deposit_banks.bank account created successfully'));
        } catch (\Exception $e) {
            DB::rollback();

            throw (new WebException($e))->redirectTo(route('admin.deposit-banks.index'))->withMessage(__('a_deposit_banks.cannot create deposit bank'));
        }
    }

    /**
     * Edit action.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, int $id)
    {
        $bank = $this->depositBankRepo->find($id);

        if (empty($bank)) {
            abort(404);
        }

        $depositMethodBank = $this->methodBankRepo->getModel()->where('deposit_bank_id', '=', $bank->id)->whereNull('deleted_at')->first();

        return view('deposit::admin.bank.edit', compact('bank', 'depositMethodBank'));
    }

    /**
     * Update action.
     *
     * @param Update $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function update(Update $request, int $id)
    {
        DB::beginTransaction();

        if ($request->is_24_hours) {
            $depositTimingFrom = null;
            $depositTimingTo   = null;
        } else {
            $depositTimingFrom = Carbon::parse($request->deposit_timing_from)->format("H:i:s");
            $depositTimingTo   = Carbon::parse($request->deposit_timing_to)->format("H:i:s");
        }

        $this->depositBankRepo->getModel()->find($id)->lockForUpdate();

        try {
            $this->depositBankRepo->edit($id, [
                'bank_name'               => $request->bank_name,
                'bank_branch'             => $request->bank_branch,
                'account_name'            => $request->account_name,
                'account_number'          => $request->account_number,
                'deposit_max'             => $request->deposit_max,
                'additional_instructions' => $request->additional_instructions,
                'deposit_timing_from'     => $depositTimingFrom,
                'deposit_timing_to'       => $depositTimingTo,
            ]);
            
            if ($request->is_default) {
                $telegraphicMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER);
                $this->methodBankRepo->setSingleBankAccount($telegraphicMethod->id, $id);
            } else {
                $this->methodBankRepo->deleteForBank($id);
            }

            DB::commit();

            return redirect()->route('admin.deposit-banks.index')
                ->with('success', __('a_deposit_banks.bank account updated successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.deposit-banks.index'))->withMessage(__('a_deposit_banks.cannot update deposit bank'));
        }
    }

    /**
     * Destroy action.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function destroy(Request $request, int $id)
    {
        DB::beginTransaction();

        try {
            $this->depositBankRepo->delete($id);

            // Must also delete for these
            $this->methodBankRepo->deleteForBank($id);
            $this->depositBankUserRepo->deleteForBank($id);

            DB::commit();

            return redirect()->route('admin.deposit-banks.index')
                ->with('success', __('a_deposit_banks.bank account deleted successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.deposit-banks.index'))->withMessage(__('a_deposit_banks.cannot delete deposit bank'));
        }
    }

    /**
     * Non VIP bank.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function nonVipBank()
    {
        $vipType = $this->depositTypeRepo->findBySlug(DepositTypeContract::NON_VIP);
        $banks   = $this->depositBankRepo->getByType($vipType->id);

        $telegraphicMethod   = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER);
        $currentSelectedBank = $this->methodBankRepo->getBankAccountsForMethod($telegraphicMethod->id);

        if (0 < $currentSelectedBank->count()) {
            $currentSelectedBankId = $currentSelectedBank[0]->id;
        } else {
            $currentSelectedBankId = 0;
        }

        return view('deposit::admin.setting.non-vip-bank.edit', compact('banks', 'currentSelectedBankId'));
    }

    /**
     * Non VIP bank update.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function updateNonVipBank(Request $request)
    {
        DB::beginTransaction();

        try {
            $telegraphicMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER);
            $this->methodBankRepo->setSingleBankAccount($telegraphicMethod->id, $request->deposit_bank_id);

            DB::commit();

            return response()->json([
                'result' => 1,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e, __('a_deposit_banks.cannot assign default nonvip bank'));
        }
    }

    /**
     * Member VIP bank management.
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function memberVipBanks(Request $request, int $userId)
    {
        $user = $this->userRepo->find($userId);

        $telegraphicVipMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER_VIP);

        $assignedBankIds = $this->depositBankUserRepo
            ->getBankAccountsForMethodAndUser($telegraphicVipMethod->id, $userId)
            ->pluck('id')
            ->toArray();

        $vipType = $this->depositTypeRepo->findBySlug(DepositTypeContract::VIP);
        $banks   = $this->depositBankRepo->getByType($vipType->id);

        $selectAll = (count($assignedBankIds) == $banks->count());

        return view('deposit::admin.management.member.vip-banks.edit', compact('user', 'banks', 'assignedBankIds', 'selectAll'));
    }

    /**
     * Assign VIP bank to user.
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function assignVipBank(Request $request, int $userId)
    {
        DB::beginTransaction();

        try {
            $telegraphicVipMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER_VIP);
            $this->depositBankUserRepo->assignBankToUser($userId, $telegraphicVipMethod->id, $request->deposit_bank_id);

            DB::commit();

            return response()->json([
                'result' => 1,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e, __('a_deposit_banks.cannot assign vip bank'));
        }
    }

    /**
     * Unassign VIP bank from user.
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function unassignVipBank(Request $request, int $userId)
    {
        DB::beginTransaction();

        try {
            $telegraphicVipMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER_VIP);
            $this->depositBankUserRepo->unassignBankFromUser($userId, $telegraphicVipMethod->id, $request->deposit_bank_id);

            DB::commit();

            return response()->json([
                'result' => 1,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e, __('a_deposit_banks.cannot unassign vip bank'));
        }
    }

    /**
     * Assign all VIP banks to user.
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function assignAllVipBanks(Request $request, int $userId)
    {
        DB::beginTransaction();

        try {
            $telegraphicVipMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER_VIP);
            $this->depositBankUserRepo->assignAllBanksToUser($userId, $telegraphicVipMethod->id);

            DB::commit();

            return response()->json([
                'result' => 1,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e, __('a_deposit_banks.cannot assign all vip banks'));
        }
    }

    /**
     * Unassign all VIP banks from user.
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws ApiException
     */
    public function unassignAllVipBanks(Request $request, int $userId)
    {
        DB::beginTransaction();

        try {
            $telegraphicVipMethod = $this->depositMethodRepo->findBySlug(DepositMethodContract::TELEGRAPHIC_TRANSFER_VIP);
            $this->depositBankUserRepo->unassignAllBanksFromUser($userId, $telegraphicVipMethod->id);

            DB::commit();

            return response()->json([
                'result' => 1,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw new ApiException($e, __('a_deposit_banks.cannot unassign all vip banks'));
        }
    }
}
