<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Announcement\Contracts\AnnouncementTranslationContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositMethodTranslationContract;
use Modules\Deposit\Http\Requests\Admin\DepositMethod\Update;
use Plus65\Utility\Exceptions\WebException;

class DepositMethodController extends Controller
{
    /**
     * Deposit method repository.
     *
     * @var DepositMethodContract
     */
    protected $depositMethodRepo;

    /**
     * Announcement translation repository.
     *
     * @var AnnouncementTranslationContract
     */
    protected $depositTranslationRepository;

    /**
     * Class constructor.
     *
     * @param DepositMethodContract $depositMethodContract
     * @param DepositMethodTranslationContract $depositMethodTranslationContract
     */
    public function __construct(
        DepositMethodContract $depositMethodContract,
        DepositMethodTranslationContract $depositMethodTranslationContract
    ) {
        $this->depositMethodRepo            = $depositMethodContract;
        $this->depositTranslationRepository = $depositMethodTranslationContract;
//        $this->middleware('permission:admin_deposit_method_update')->only(['index', 'getDetails', 'update']);
    }

    /**
     * Index action.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('deposit::admin.setting.method.index');
    }

    /**
     * Get details.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetails(Request $request, int $id)
    {
        $method      = $this->depositMethodRepo->find($id);
        $deposits    = $method->depositMethodi18ns;
        $description = [];

        foreach ($deposits as $key => $value) {
            $description[$value->translatorLanguage->code] = $value->description;
        }

        if (!empty($method)) {
            return response()->json([
                'name'                 => $method->name,
                'admin_fee_percentage' => trim_zero(bcmul($method->admin_fee_percentage, '100', 2)),
                'min'                  => trim_zero($method->min),
                'max'                  => trim_zero($method->max),
                'multiple'             => trim_zero($method->multiple),
                'is_active'            => strval($method->is_active),
                'description'          => $description,
            ]);
        }

        abort(404);
    }

    /**
     * Update deposit method settings.
     *
     * @param Update $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function update(Update $request)
    {
        DB::beginTransaction();

        try {
            $this->depositMethodRepo->edit($request->deposit_method_id, [
                'min'                  => $request->min,
                'max'                  => $request->max,
                'multiple'             => $request->multiple,
                'subsequent_min'       => $request->subsequent_min,
                'subsequent_max'       => $request->subsequent_max,
                'subsequent_multiple'  => $request->subsequent_multiple,
                'admin_fee_percentage' => bcdiv($request->admin_fee_percentage, '100', config('credit.precision.percentage_decimal')),
                'is_active'            => $request->is_active,
            ]);

            // insert into deposit method i18n
            $attributes['deposit_method_id'] = $request->deposit_method_id;
            $attributes["description"]       = $request->description;
            $this->depositTranslationRepository->delete($attributes['deposit_method_id']);
            $this->depositTranslationRepository->add($attributes);

            DB::commit();

            return redirect()->route('admin.deposit-methods.index')->with('success', __('a_deposit_method_edit.deposit method settings updated successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.deposit-methods.index'))->withMessage(__('a_deposit_method_edit.cannot update deposit method settings'));
        }
    }
}
