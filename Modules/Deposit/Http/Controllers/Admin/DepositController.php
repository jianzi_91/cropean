<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Deposit\Contracts\DepositBankDetailContract;
use Modules\Deposit\Contracts\DepositContract;
use Modules\Deposit\Contracts\DepositInvoiceContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Contracts\DepositTransactionContract;
use Modules\Deposit\Http\Requests\Admin\Update;
use Modules\Deposit\Queries\Admin\DepositQuery;
use Modules\Deposit\Repositories\DepositRepository;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Plus65\Utility\Exceptions\WebException;

class DepositController extends Controller
{
    /**
     * Deposit repository.
     *
     * @var DepositRepository $depositRepo
     */
    protected $depositRepo;

    /**
     * Deposit status repository.
     *
     * @var DepositStatusContract
     */
    protected $statusRepo;

    /**
     * Deposit transaction repository.
     *
     * @var DepositTransactionContract
     */
    protected $txRepo;

    /**
     * Bank account credit repository.
     *
     * @var BankAccountCreditContract
     */
    protected $creditRepo;

    /**
     * Exchange rate repository.
     *
     * @var ExchangeRateContract
     */
    protected $exchangeRateRepo;

    /**
     * Deposit query.
     *
     * @var DepositQuery
     */
    protected $depositQuery;
    /**
     * Deposit method repository.
     *
     * @var DepositMethodContract
     */
    protected $depositMethodRepo;

    protected $depositBankDetailRepo;

    /**
     * Class constructor.
     *
     * @param DepositContract $depositRepo
     * @param DepositMethodContract $depositMethodContract
     * @param DepositStatusContract $statusContract
     * @param DepositTransactionContract $txContract
     * @param BankAccountCreditContract $creditContract
     * @param ExchangeRateContract $exchangeRateContract
     * @param DepositQuery $depositQuery
     * @param DepositBankDetailContract $depositBankDetailContract
     */
    public function __construct(
        DepositContract $depositRepo,
        DepositInvoiceContract $depositInvoiceContract,
        DepositMethodContract $depositMethodContract,
        DepositStatusContract $statusContract,
        DepositTransactionContract $txContract,
        BankAccountCreditContract $creditContract,
        ExchangeRateContract $exchangeRateContract,
        DepositQuery $depositQuery,
        DepositBankDetailContract $depositBankDetailContract
    ) {
        $this->depositQuery          = $depositQuery;
        $this->depositMethodRepo     = $depositMethodContract;
        $this->statusRepo            = $statusContract;
        $this->depositRepo           = $depositRepo;
        $this->txRepo                = $txContract;
        $this->exchangeRateRepo      = $exchangeRateContract;
        $this->creditRepo            = $creditContract;
        $this->depositBankDetailRepo = $depositBankDetailContract;
        $this->depositInvoiceRepo    = $depositInvoiceContract;

        $this->middleware('permission:admin_deposit_list')->only('index');
        $this->middleware('permission:admin_deposit_edit')->only('edit', 'update');
        $this->middleware('double_click_prevention')->only('update');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $deposits = $this->depositQuery->setParameters($request->all())
            ->paginate();

        $pendingCount = $this->depositRepo->getPendingCount();

        return view('deposit::admin.index', compact('deposits', 'pendingCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $deposit = $this->depositRepo->find($id);
        $setting = $this->depositMethodRepo->find($deposit->deposit_method_id);

        if ($deposit->is_first) {
            $min = bcmul($setting->min, $deposit->exchange_rate, credit_precision());
            $max = bcmul($setting->max, $deposit->exchange_rate, credit_precision());
        } else {
            $min = bcmul($setting->subsequent_min, $deposit->exchange_rate, credit_precision());
            $max = bcmul($setting->subsequent_max, $deposit->exchange_rate, credit_precision());
        }

        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);
        $issueCurrency            = $bankCreditTypeRepository->find($deposit->issue_credit_type_id)->rawname;

        $pendingStatusId = $this->statusRepo->findBySlug($this->statusRepo::PENDING)->id;
        $baseCurrency    = $this->exchangeRateRepo->findBySlug($deposit->currency)->currency;
        $depositCurrency = $this->exchangeRateRepo->findBySlug($deposit->converted_currency)->currency;
        $issueCurrency   = $this->exchangeRateRepo->findBySlug($issueCurrency)->currency;

        $depositBankDetails = $this->depositBankDetailRepo->getModel()->where('deposit_id', '=', $id)->first();

        return view('deposit::admin.edit', compact('deposit', 'min', 'max', 'depositCurrency', 'baseCurrency', 'issueCurrency', 'pendingStatusId', 'depositBankDetails'));
    }

    /**
     * Update the specified resource in storage.
     * @param Update $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws WebException
     */
    public function update(Update $request, $id)
    {
        try {
            DB::beginTransaction();
            $deposit = $this->depositRepo->lock($id);

            if ($deposit->status->status->rawname != DepositStatusContract::PENDING) {
                return redirect(route('admin.deposits.index'))->with('error', __('a_deposit_list.cannot change status as this deposit is not pending status'));
            }

            $this->depositRepo->approveRequest($request, $deposit);

            if ($request->action == 'approve') {
                $this->depositInvoiceRepo->generate($deposit);
            }

            DB::commit();

            return redirect(route('admin.deposits.index'))->with('success', __('a_deposit_list.status updated successfully'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.deposits.index'))->withMessage(__('a_deposit_list.cannot update status'));
        }
    }

    public function bulkUpdate(Request $request)
    {
        $depositIds = explode(',', $request->deposit_ids);

        try {
            DB::beginTransaction();
            ini_set('max_execution_time', -1);
            foreach ($depositIds as $depositId) {
                $deposit = $this->depositRepo->find($depositId);

                if ($deposit) {
                    if ($deposit->status->status->rawname != DepositStatusContract::PENDING || $deposit->status->status->rawname == DepositStatusContract::CANCELLED) {
                        continue;
                    }

                    $this->depositRepo->approveRequest($request, $deposit);

                    if ($request->action == 'approve') {
                        $this->depositInvoiceRepo->generate($deposit);
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return back()->with('success', __('a_deposit_list.status updated successfully'));
    }
}
