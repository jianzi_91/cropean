<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositSettingContract;
use Modules\Deposit\Http\Requests\Admin\UpdateDepositSettingRequest;
use Modules\Deposit\Queries\Admin\DepositSettingQuery;
use Plus65\Utility\Exceptions\WebException;

class DepositSettingController extends Controller
{
    protected $depositMethodContract;
    protected $depositSettingContract;
    protected $depositSettingQuery;

    /**
     * Class constructor
     *
     */
    public function __construct(
        DepositMethodContract $depositMethodContract,
        DepositSettingContract $depositSettingContract,
        DepositSettingQuery $depositSettingQuery
    ) {
        $this->depositSettingQuery    = $depositSettingQuery;
        $this->depositMethodContract  = $depositMethodContract;
        $this->depositSettingContract = $depositSettingContract;
        $this->middleware('permission:admin_deposit_settings_list')->only('index');
        $this->middleware('permission:admin_deposit_settings_create')->only('create', 'store');
        $this->middleware('permission:admin_deposit_settings_edit')->only('edit', 'update');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $setting = $this->depositMethodContract->getModel()->first();

        return view('deposit::admin.setting.index', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateDepositSettingRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = request()->only('max', 'min', 'multiple', 'admin_fee_percentage');

            $data['admin_fee_percentage'] = percentage($data['admin_fee_percentage'], 4);

            $setting = $this->depositMethodContract->getModel()->first();
            $setting->update($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.deposits.settings.index'))->withMessage(__('a_deposit_settings.failed to update deposit setting'));
        }

        return redirect(route('admin.deposits.settings.index'))->with('success', __('a_deposit_settings.deposit setting updated successfully'));
    }
}
