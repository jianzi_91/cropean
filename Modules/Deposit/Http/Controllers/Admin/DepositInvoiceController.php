<?php

namespace Modules\Deposit\Http\Controllers\Admin;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Deposit\Contracts\DepositInvoiceContract;

class DepositInvoiceController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct(
        DepositInvoiceContract $invoiceContract,
        PDF $pdfService
    ) {
        $this->invoiceRepo = $invoiceContract;
        $this->pdfService  = $pdfService;
    }

    public function show(Request $request, $referenceNumber)
    {
        // $invoice = $this->invoiceRepo->find($id);
        $invoice = $this->invoiceRepo->findBySlug($referenceNumber);

        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . 'invoices' . DIRECTORY_SEPARATOR . $invoice->reference_number . '.pdf';
        return response()->file($path, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition: attachment; filename="' . $invoice->reference_number . '"'
        ]);
    }
}
