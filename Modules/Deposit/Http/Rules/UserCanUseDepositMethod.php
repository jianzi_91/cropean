<?php

namespace Modules\Deposit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Deposit\Contracts\DepositMethodContract;

class UserCanUseDepositMethod implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $depositMethodRepo = resolve(DepositMethodContract::class);
        $eligibleMethods   = $depositMethodRepo->getUserEligibleMethods(auth()->id());

        foreach ($eligibleMethods as $method) {
            if ($value == $method->id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.invalid deposit method');
    }
}
