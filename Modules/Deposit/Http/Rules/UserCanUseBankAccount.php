<?php

namespace Modules\Deposit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositMethodBankContract;

class UserCanUseBankAccount implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Deposit banks in the deposit_method_banks table are for global methods (i.e. non VIP), so if bank account is inside, can use
        $methodBankRepo = resolve(DepositMethodBankContract::class);
        $globalCount    = $methodBankRepo->getModel()
            ->where('deposit_bank_id', $value)
            ->count();

        if (0 < $globalCount) {
            return true;
        }

        $bankUserRepo   = resolve(DepositBankUserContract::class);
        $nonGlobalCount = $bankUserRepo->getModel()
            ->where('deposit_bank_id', $value)
            ->count();

        if (0 < $nonGlobalCount) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.invalid bank');
    }
}
