<?php

namespace Modules\Deposit\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Deposit\Contracts\DepositContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositSettingContract;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

class ValidDepositAmount implements Rule
{
    protected $depositType;

    /**
     * The message
     *
     * @var unknown
     */
    protected $message;

    protected $depositMethodId;

    /**
     * Create a new rule instance.
     *
     * @param unknown $withdrawalTypeId
     */
    public function __construct($depositMethodId)
    {
        $this->depositMethodId = $depositMethodId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $depositMethodRepo = resolve(DepositMethodContract::class);
        $exchangeRateRepo  = resolve(ExchangeRateContract::class);

        $depositMethod = $depositMethodRepo->find($this->depositMethodId);
        $exchangeRate  = $exchangeRateRepo->findBySlug($depositMethod->currency)->sell_rate;

        $min      = bcmul($depositMethod->min, $exchangeRate, credit_precision());
        $max      = bcmul($depositMethod->max, $exchangeRate, credit_precision());
        $multiple = bcmul($depositMethod->multiple, $exchangeRate, credit_precision());

        if (0 < $min) {
            if ($value < $min) {
                $this->message = __('s_validation.minimum amount is :minimum_amount', ['minimum_amount' => amount_format($min)]);
                return false;
            }
        }

        if (0 < $max) {
            if ($value > $max) {
                $this->message = __('s_validation.maximum amount is :maximum_amount', ['maximum_amount' => amount_format($max)]);
                return false;
            }
        }

        if (0 < $multiple) {
            $modulo        = bcmod($value, $multiple, 2);
            $this->message = __('s_validation.amount must be a multiple of :multiples', ['multiples' => amount_format($multiple)]);

            return 0 == $modulo;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
