<?php

namespace Modules\Deposit\Queries;

use Illuminate\Support\Facades\DB;
use Modules\Deposit\Models\Deposit;
use QueryBuilder\QueryBuilder;

class DepositQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = Deposit::select('deposits.*', 'deposit_statuses.name_translation AS status_name')
        ->join('deposit_status_histories', function ($join) {
            $join->on('deposit_status_histories.deposit_id', '=', 'deposits.id')
                ->where('deposit_status_histories.is_current', 1)
                ->whereNull('deposit_status_histories.deleted_at');
        })
        ->join('deposit_statuses', 'deposit_statuses.id', '=', 'deposit_status_histories.deposit_status_id')
            ->with('status.status')
        ->join('users', 'users.id', '=', 'deposits.user_id')
        ->orderBy('deposits.created_at', 'desc')
        ->select([
            'deposits.id',
            'deposits.reference_number',
            'deposits.created_at',
            'deposits.admin_fee',
            'deposits.exchange_rate',
            'deposits.amount',
            'deposits.converted_amount',
            DB::raw('deposits.admin_fee * deposits.exchange_rate as converted_admin_fee'),
            'deposits.issue_credit_type_id',
            'deposits.credit_type_exchange_rate',
            'deposits.issued_amount',
            'users.name',
            'users.member_id',
            'users.email',
            'deposit_statuses.name_translation AS status_name',
            'deposit_statuses.id AS deposit_status_id',
        ]);

        return $query;
    }
}
