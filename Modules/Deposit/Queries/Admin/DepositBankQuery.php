<?php

namespace Modules\Deposit\Queries\Admin;

use Modules\Deposit\Queries\DepositBankQuery as BaseQuery;

class DepositBankQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'deposit_banks',
            'column' => 'created_at'
        ],
        'bank_name' => [
            'filter' => 'text',
            'table'  => 'deposit_banks',
            'column' => 'bank_name'
        ],
        'account_number' => [
            'filter' => 'text',
            'table'  => 'deposit_banks',
            'column' => 'account_number'
        ],
    ];
}
