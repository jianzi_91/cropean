<?php

namespace Modules\Deposit\Queries\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Deposit\Queries\DepositQuery as BaseQuery;

class DepositQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'deposits',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'reference_number' => [
            'filter'    => 'text',
            'table'     => 'deposits',
            'column'    => 'reference_number',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'deposit_status_id' => [
            'filter'    => 'select',
            'table'     => 'deposits',
            'column'    => 'deposit_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_deposit_list.request date'),
            __('a_deposit_list.reference number'),
            __('a_deposit_list.receipt numbers'),
            __('a_deposit_list.full name'),
            __('a_deposit_list.member id'),
            __('a_deposit_list.email'),
            __('a_deposit_list.requested deposit amount usd'),
            __('a_deposit_list.admin fee usd'),
            __('a_deposit_list.total requested deposit amount usd'),
            __('a_deposit_list.total amount transferred cny'),
            __('a_deposit_list.total converted amount capital credit'),
            __('a_deposit_list.status'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($deposit): array
    {
        return [
            $deposit->created_at,
            $deposit->reference_number,
            $deposit->receipt_numbers . " ",
            $deposit->name,
            $deposit->member_id,
            $deposit->email,
            amount_format($deposit->amount, credit_precision()),
            amount_format($deposit->admin_fee, credit_precision()),
            amount_format(bcadd($deposit->amount, $deposit->admin_fee, credit_precision()), credit_precision()),
            amount_format(bcadd($deposit->converted_amount, $deposit->converted_admin_fee, credit_precision()), credit_precision()),
            amount_format($deposit->issued_amount, credit_precision()),
            __('s_deposit_statuses.' . $deposit->status_name),
        ];
    }
}
