<?php

namespace Modules\Deposit\Queries\Admin;

use Illuminate\Support\Facades\Auth;
use Modules\Deposit\Queries\DepositSettingQuery as BaseQuery;

class DepositSettingQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'title' => [
            'filter' => 'text',
            'table'  => 'deposit_settings',
            'column' => 'title'
        ],
        'is_active' => [
            'filter' => 'checkbox',
            'table'  => 'deposit_settings',
            'column' => 'is_active'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        //return $this->builder->where('user_id', Auth::user()->id);
    }
}
