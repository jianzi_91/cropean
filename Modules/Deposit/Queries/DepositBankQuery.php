<?php

namespace Modules\Deposit\Queries;

use Modules\Deposit\Models\DepositBank;
use QueryBuilder\QueryBuilder;

class DepositBankQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        return DepositBank::with('type')
            ->select([
                'id',
                'created_at',
                'deposit_type_id',
                'bank_name',
                'bank_branch',
                'account_name',
                'account_number',
            ]);
    }
}
