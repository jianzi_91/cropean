<?php

namespace Modules\Deposit\Queries\Member;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Deposit\Queries\DepositQuery as BaseQuery;

class DepositQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'deposits',
            'column' => 'created_at'
        ],
        'reference_number' => [
            'filter' => 'text',
            'table'  => 'deposits',
            'column' => 'reference_number'
        ],
        'deposit_status_id' => [
            'filter' => 'select',
            'table'  => 'deposits',
            'column' => 'deposit_status_id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', Auth::user()->id);
    }

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('m_deposit_list.request date'),
            __('m_deposit_list.reference number'),
            __('m_deposit_list.requested deposit amount usd'),
            __('m_deposit_list.admin fee usd'),
            __('m_deposit_list.total request deposit amount usd'),
            __('m_deposit_list.total deposit amount transferred rmb'),
            __('m_deposit_list.total converted amount'),
            __('m_deposit_list.status'),
        ];
    }


    /**
     * @inheritDoc
     */
    public function map($deposit): array
    {
        return [
            $deposit->created_at,
            $deposit->reference_number,
            amount_format($deposit->amount,credit_precision()),
            amount_format($deposit->admin_fee,credit_precision()),
            amount_format(bcadd($deposit->amount,$deposit->admin_fee, credit_precision()),credit_precision()),
            amount_format(bcadd($deposit->converted_amount,$deposit->converted_admin_fee, credit_precision()),credit_precision()),
            amount_format($deposit->issued_amount,credit_precision()),
            __('s_deposit_statuses.'. $deposit->status_name),
        ];
    }
}
