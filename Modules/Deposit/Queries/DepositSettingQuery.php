<?php

namespace Modules\Deposit\Queries;

use Modules\Deposit\Models\DepositSetting;
use QueryBuilder\QueryBuilder;

class DepositSettingQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = DepositSetting::select('deposit_settings.*')
        ->orderBy('deposit_settings.created_at', 'desc');

        return $query;
    }
}
