<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('deposit-settings', ['as' => 'admin.deposits.settings.index', 'uses' => 'Admin\DepositSettingController@index']);
        Route::put('deposit-settings', ['as' => 'admin.deposits.settings.update', 'uses' => 'Admin\DepositSettingController@update']);
        Route::resource('deposits', 'Admin\DepositController', ['as' => 'admin'])->only(['index', 'edit', 'update']);
        Route::post('/deposits/bulk-update', ['as' => 'admin.deposits.bulk.update', 'uses' => 'Admin\DepositController@bulkUpdate']);
        Route::get('/qr/image/{id}', ['as' => 'admin.deposits.qr', 'uses' => 'Admin\DepositController@qrImage']);

        /*
         * NOT IN USE, do not have VIP/NON VIP IN THIS PROJECT
         */
        /*Route::get('/deposit-banks/edit', ['as' => 'admin.deposit.banks.nonvip.edit', 'uses' => 'Admin\DepositBankController@nonVipBank']);*/
        Route::put('/deposit-banks/edit', ['as' => 'admin.deposit.banks.nonvip.update', 'uses' => 'Admin\DepositBankController@updateNonVipBank']);

        Route::get('/deposits/export', ['as' => 'admin.deposits.export', 'uses' => 'Admin\DepositExportController@export']);
        Route::resource('/deposit-banks', 'Admin\DepositBankController', ['as' => 'admin'])->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);

        Route::get('/deposits/export', ['as' => 'admin.deposits.export', 'uses' => 'Admin\DepositExportController@export']);

        Route::get('deposit-methods', ['as' => 'admin.deposit-methods.index', 'uses' => 'Admin\DepositMethodController@index']);
        Route::put('deposit-methods', ['as' => 'admin.deposit-methods.update', 'uses' => 'Admin\DepositMethodController@update']);
        Route::get('deposit-methods/{id}', ['as' => 'admin.deposit-methods.details', 'uses' => 'Admin\DepositMethodController@getDetails']);

        Route::get('/deposits/attachment/{id}', ['as' => 'admin.deposits.attachment', 'uses' => 'Admin\DepositAttachmentController@get']);
        Route::resource('deposit/invoices', 'Admin\DepositInvoiceController', ['as' => 'admin'])->only(['show']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('/deposit-methods/{id}', ['as' => 'member.deposits.method.details', 'uses' => 'Member\DepositController@getMethod']);
        Route::get('/deposits/{id}/details', ['as' => 'member.deposits.details', 'uses' => 'Member\DepositController@getDetail']);
        Route::get('/deposits/calculate', ['as' => 'member.deposits.method.calculate', 'uses' => 'Member\DepositController@calculate']);
        Route::get('/deposits/bank-accounts', ['as' => 'member.deposits.method.bank.accounts', 'uses' => 'Member\DepositController@getBankAccounts']);
        Route::get('/deposits/bank-accounts/{id?}', ['as' => 'member.deposits.method.bank.account.details', 'uses' => 'Member\DepositController@getBankAccountDetails']);

        Route::get('/deposits/export', ['as' => 'member.deposits.export', 'uses' => 'Member\DepositExportController@export']);
        Route::get('/deposits/attachment/{id}', ['as' => 'member.deposits.attachment', 'uses' => 'Member\DepositAttachmentController@get']);
        Route::resource('deposits', 'Member\DepositController', ['as' => 'member'])->except(['edit', 'update']);
        Route::resource('deposit/invoices', 'Member\DepositInvoiceController', ['as' => 'member'])->only(['show']);
    });
});
