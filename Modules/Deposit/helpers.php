<?php

use Carbon\Carbon;

if (!function_exists('validate_deposit_timing')) {
    function validate_deposit_timing()
    {
        $depositMethodRepo     = resolve(\Modules\Deposit\Contracts\DepositMethodContract::class);
        $depositMethodBankRepo = resolve(\Modules\Deposit\Contracts\DepositMethodBankContract::class);
        $depositBankRepo       = resolve(\Modules\Deposit\Contracts\DepositBankContract::class);

        $depositMethodId = $depositMethodRepo->findBySlug($depositMethodRepo::TELEGRAPHIC_TRANSFER)->id;

        if (empty($depositMethodBankRepo->getCurrentDepositBank($depositMethodId))) {
            return false;
        }

        $depositBankDetailId = $depositMethodBankRepo->getCurrentDepositBank($depositMethodId)->deposit_bank_id;
        $depositBank         = $depositBankRepo->find($depositBankDetailId);

        $now                = Carbon::now();
        $depositFromSetting = $depositBank->deposit_timing_from;
        $depositToSetting   = $depositBank->deposit_timing_to;

        if ($depositFromSetting == $depositToSetting) {
            return true;
        }

        $depositFrom = (new Carbon)->createFromFormat('H:i:s', $depositFromSetting);
        $depositTo   = (new Carbon)->createFromFormat('H:i:s', $depositToSetting);

        return $now->gte($depositFrom) && $now->lt($depositTo);
    }
}

if (!function_exists('has_default_bank')) {
    function has_default_bank()
    {
        $depositMethodRepo     = resolve(\Modules\Deposit\Contracts\DepositMethodContract::class);
        $depositMethodBankRepo = resolve(\Modules\Deposit\Contracts\DepositMethodBankContract::class);

        $depositMethodId = $depositMethodRepo->findBySlug($depositMethodRepo::TELEGRAPHIC_TRANSFER)->id;

        if (empty($depositMethodBankRepo->getCurrentDepositBank($depositMethodId))) {
            return false;
        }

        return true;
    }
}
