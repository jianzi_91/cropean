<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositAttachment extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_attachments';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'deposit_id',
        'filename',
        'original_filename',
        'mime_type',
        'receipt_number',
    ];

    /**
     * Relation to deposit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposit()
    {
        return $this->belongsTo(Deposit::class, 'deposit_id');
    }

    /**
     * Get URL of receipt for member portal.
     *
     * @return string|null
     */
    public function getMemberUrlAttribute()
    {
        return isset($this->id) ? route('member.deposits.attachment', $this->id) : null;
    }

    /**
     * Get URL of receipt for admin portal.
     *
     * @return string|null
     */
    public function getAdminUrlAttribute()
    {
        return isset($this->id) ? route('admin.deposits.attachment', $this->id) : null;
    }
}
