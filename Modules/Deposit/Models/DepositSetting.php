<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;

class DepositSetting extends Model
{
    use SoftDeletes, Translatable;
    protected $translatableAttributes = ['title'];
    protected $fillable               = ['title', 'min', 'max', 'processing_fee_percentage', 'multiple_of', 'is_active', 'qr_filename'];
}
