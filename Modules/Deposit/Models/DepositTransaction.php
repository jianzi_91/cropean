<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Credit\Models\BankAccountStatement;
use Plus65\Base\Models\Relations\UserRelation;

class DepositTransaction extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_transactions';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'deposit_id',
        'credit_type_exchange_rate',
        'transaction_code',
    ];

    /**
     * Relation to deposit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposit()
    {
        return $this->belongsTo(Deposit::class, 'deposit_id');
    }

    /**
     * Relation to statement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function statement()
    {
        return $this->belongsTo(BankAccountStatement::class, 'transaction_code', 'transaction_code');
    }
}
