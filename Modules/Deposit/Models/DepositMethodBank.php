<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositMethodBank extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_method_banks';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'deposit_method_id',
        'deposit_bank_id',
    ];
}
