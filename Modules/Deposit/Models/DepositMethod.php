<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Spatie\Activitylog\Traits\LogsActivity;

class DepositMethod extends Model
{
    use SoftDeletes, HasActiveScope, Translatable, HasDropDown, LogsActivity;

    /**
     * Loggable attributes.
     *
     * @var array
     */
    protected static $logAttributes = [
        'name',
        'name_translation',
        'currency',
        'min',
        'max',
        'multiple',
        'admin_fee_percentage',
        'is_active',
    ];

    /**
     * Log only dirty.
     *
     * @var bool
     */
    protected static $logOnlyDirty = true;

    /**
     * Translatable columns.
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_methods';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'currency',
        'min',
        'max',
        'multiple',
        'admin_fee_percentage',
        'is_active',
    ];

    /**
     * Relation to type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DepositType::class, 'deposit_type_id');
    }

    /**
     * Relation to type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depositMethodi18ns()
    {
        return $this->hasMany(DepositMethodi18n::class, 'deposit_method_id');
    }
}
