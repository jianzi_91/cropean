<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositBankDetail extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_bank_details';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'deposit_id',
        'bank_name',
        'bank_branch',
        'account_name',
        'unique_id',
        'account_number',
        'branch',
        'currency',
        'deposit_max',
        'max_transaction',
        'deposit_timing_from',
        'deposit_timing_to',
        'additional_instructions',
    ];

    /**
     * Relation to deposit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposit()
    {
        return $this->belongsTo(Deposit::class, 'deposit_id');
    }
}
