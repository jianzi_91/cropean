<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositStatusHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'deposit_status_id',
        'deposit_id',
        'remarks',
        'is_current',
        'updated_by',
    ];

    /**
     * This model's relation to user status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(DepositStatus::class, 'deposit_status_id');
    }

    /**
     * Local scope for getting current status.
     *
     * @param unknown $query
     * @param unknown $current
     *
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where($this->getTable() . '.is_current', $current);
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
