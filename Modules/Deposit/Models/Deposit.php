<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Relations\UserRelation;
use Spatie\Activitylog\Traits\LogsActivity;

class Deposit extends Model implements Stateable
{
    use SoftDeletes, UserRelation, HistoryRelation, LogsActivity;

    /**
     * Loggable attributes.
     *
     * @var array
     */
    protected static $logAttributes = [
        'user_id',
        'deposit_method_id',
        'deposit_bank_id',
        'deposit_type_id',
        'reference_number',
        'currency',
        'amount',
        'total_amount',
        'converted_currency',
        'converted_amount',
        'exchange_rate',
        'admin_fee_percentage',
        'admin_fee',
        'deposit_status_id',
        'issue_credit_type_id',
        'credit_type_exchange_rate',
        'issued_amount',
        'locale',
    ];

    /**
     * Log only dirty.
     *
     * @var bool
     */
    protected static $logOnlyDirty = true;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposits';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'deposit_method_id',
        'deposit_type_id',
        'reference_number',
        'currency',
        'amount',
        'total_amount',
        'converted_currency',
        'converted_amount',
        'exchange_rate',
        'admin_fee_percentage',
        'admin_fee',
        'deposit_status_id',
        'issue_credit_type_id',
        'credit_type_exchange_rate',
        'issued_amount',
        'locale',
    ];

    /**
     * Relation to attachments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(DepositAttachment::class, 'deposit_id');
    }

    /**
     * Relation to method.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function method()
    {
        return $this->belongsTo(DepositMethod::class, 'deposit_method_id');
    }

    /**
     * Relation to transaction.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transaction()
    {
        return $this->hasOne(DepositTransaction::class, 'deposit_id');
    }

    /**
     * Relation to deposit bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DepositType::class, 'deposit_type_id');
    }

    /**
     * Relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * This model's relation to user status histories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(DepositStatusHistory::class, 'deposit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status()
    {
        return $this->hasOne(DepositStatusHistory::class, 'deposit_id')->current(1);
    }

    /**
     * Relation to bank detail.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankDetail()
    {
        return $this->hasOne(DepositBankDetail::class, 'deposit_id');
    }

    /**
     * Relation to deposit invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice()
    {
        return $this->hasOne(DepositInvoice::class, 'deposit_id');
    }

    /**
     * Approved status scope
     */
    public function scopeApproved($query)
    {
        $depositStatusContract = resolve(DepositStatusContract::class);
        $approvedStatus        = $depositStatusContract->findBySlug(DepositStatusContract::APPROVED);

        return $query->where('deposit_status_id', $approvedStatus->id);
    }

    /**
     * Get the state id.
     *
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'deposit_id';
    }

    /**
     * Can cancel attribute.
     *
     * @return bool
     */
    public function getCanCancelAttribute()
    {
        return $this->status->status->rawname == DepositStatusContract::PENDING;
    }

    /**
     * Can cancel attribute.
     *
     * @return bool
     */
    public function getIsCancelledAttribute()
    {
        return $this->status->status->rawname == DepositStatusContract::CANCELLED;
    }

    /**
     * Receipt numbers attribute
     */
    public function getReceiptNumbersAttribute()
    {
        $receipts = $this->attachments->pluck('receipt_number')->toArray();

        foreach ($receipts as $key => $value) {
            if (empty($value)) {
                unset($receipts[$key]);
            }
        }

        return implode(", ", $receipts);
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return DepositStatusHistory::class;
    }

    /**
     * Listening to events
     * @throws \Exception
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($deposit) {
            $depositStatusContract = resolve(DepositStatusContract::class);
            $pendingStatus = $depositStatusContract->findBySlug(DepositStatusContract::PENDING);

            if (!$depositStatusContract->changeHistory($deposit, $pendingStatus)) {
                throw new \Exception();
            }
        });
    }
}
