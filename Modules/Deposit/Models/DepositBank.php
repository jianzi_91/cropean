<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class DepositBank extends Model
{
    use SoftDeletes, HasActiveScope, HasDropDown;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_banks';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'deposit_type_id',
        'bank_name',
        'bank_branch',
        'account_name',
        'unique_id',
        'account_number',
        'currency',
        'deposit_max',
        'max_transaction',
        'additional_instructions',
        'deposit_timing_from',
        'deposit_timing_to',
        'is_active',
    ];

    /**
     * Relation to type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(DepositType::class, 'deposit_type_id');
    }
}
