<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class DepositBankUser extends Model
{
    use SoftDeletes, UserRelation;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_bank_users';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'deposit_method_id',
        'deposit_bank_id',
    ];

    /**
     * Relation to method.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function method()
    {
        return $this->belongsTo(DepositMethod::class, 'deposit_method_id');
    }

    /**
     * Relation to bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(DepositBank::class, 'deposit_bank_id');
    }
}
