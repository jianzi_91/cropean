<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class DepositType extends Model
{
    use SoftDeletes, HasActiveScope, Translatable, HasDropDown;

    /**
     * Translatable columns.
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'deposit_types';

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'is_global',
        'is_active',
    ];
}
