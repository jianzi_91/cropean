<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Models\TranslatorLanguage;

class DepositMethodi18n extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'deposit_method_id',
        'translator_language_id',
        'title',
        'description',
    ];

    protected $table = 'deposit_method_i18ns';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depositMethod()
    {
        return $this->belongsTo(DepositMethod::class, 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function translatorLanguage()
    {
        return $this->belongsTo(TranslatorLanguage::class, 'translator_language_id');
    }
}
