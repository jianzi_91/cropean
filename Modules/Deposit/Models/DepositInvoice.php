<?php

namespace Modules\Deposit\Models;

use Illuminate\Database\Eloquent\Model;

class DepositInvoice extends Model
{
    protected $table   = "deposit_invoices";
    protected $guarded = [];
}
