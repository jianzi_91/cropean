<?php

namespace Modules\Deposit\Repositories;

use Illuminate\Support\Facades\Storage;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Deposit\Contracts\DepositSettingContract;
use Modules\Deposit\Models\DepositSetting;
use Modules\User\Models\UserStatus;

class DepositSettingRepository extends Repository implements DepositSettingContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param UserStatus  $model
     */
    public function __construct(DepositSetting $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function addSetting($data)
    {
        if ($data['is_active']) {
            DepositSetting::where('is_active', 1)->update(['is_active' => 0]);
        }
        if (isset($data['qr_file']) && $data['qr_file']) {
            $qr_filename         = $this->saveQrFile($data['qr_file']);
            $data['qr_filename'] = $qr_filename;
        }
        return $this->add($data);
    }

    public function updateSetting($id, $data)
    {
        if (isset($data['is_active']) && $data['is_active']) {
            DepositSetting::where('is_active', 1)->where('id', '!=', $id)->update(['is_active' => 0]);
        }
        if (isset($data['qr_file']) && $data['qr_file']) {
            $qr_filename         = $this->saveQrFile($data['qr_file']);
            $data['qr_filename'] = $qr_filename;
        }
        return $this->edit($id, $data);
    }

    public function saveQrFile($file)
    {
        $uploadDirectory = 'deposit_settings_qr';
        $path            = Storage::putFile($uploadDirectory, $file);
        return $path;
    }
}
