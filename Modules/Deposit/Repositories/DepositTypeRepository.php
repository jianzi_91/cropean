<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositTypeContract;
use Modules\Deposit\Models\DepositType;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositTypeRepository extends Repository implements DepositTypeContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor.
     *
     * @param DepositType $model
     */
    public function __construct(DepositType $model)
    {
        parent::__construct($model);
    }
}
