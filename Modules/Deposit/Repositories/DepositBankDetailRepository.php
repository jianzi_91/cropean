<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositBankDetailContract;
use Modules\Deposit\Models\DepositBankDetail;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositBankDetailRepository extends Repository implements DepositBankDetailContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor.
     *
     * @param DepositBankDetail $model
     */
    public function __construct(DepositBankDetail $model)
    {
        $this->slug = 'deposit_id';
        parent::__construct($model);
    }
}
