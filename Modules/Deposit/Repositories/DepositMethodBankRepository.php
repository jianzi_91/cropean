<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositMethodBankContract;
use Modules\Deposit\Models\DepositMethodBank;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DepositMethodBankRepository extends Repository implements DepositMethodBankContract
{
    use HasCrud;

    /**
     * Deposit bank repository.
     *
     * @var DepositBankContract
     */
    protected $bankRepo;

    /**
     * Class constructor.
     *
     * @param DepositMethodBank $model
     * @param DepositBankContract $bankContract
     */
    public function __construct(DepositMethodBank $model, DepositBankContract $bankContract)
    {
        parent::__construct($model);
        $this->bankRepo = $bankContract;
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::deleteForBank()
     */
    public function deleteForBank(int $depositBankId)
    {
        $this->model
            ->where('deposit_bank_id', $depositBankId)
            ->delete();
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::getBankAccountsForMethod()
     */
    public function getBankAccountsForMethod(int $depositMethodId)
    {
        return $this->bankRepo
            ->getModel()
            ->join('deposit_method_banks', 'deposit_method_banks.deposit_bank_id', 'deposit_banks.id')
            ->whereNull('deposit_banks.deleted_at')
            ->whereNull('deposit_method_banks.deleted_at')
            ->where('deposit_banks.is_active', true)
            ->get(['deposit_banks.*']);
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::setSingleBankAccount()
     */
    public function setSingleBankAccount(int $depositMethodId, int $depositBankId)
    {
        $this->model
            ->where('deposit_method_id', $depositMethodId)
            ->delete();

        return $this->add([
            'deposit_method_id' => $depositMethodId,
            'deposit_bank_id'   => $depositBankId,
        ]);
    }

    public function getCurrentDepositBank($depositMethodId)
    {
        return $this->model->where('deposit_method_id', '=', $depositMethodId)->whereNull('deleted_at')->first();
    }
}
