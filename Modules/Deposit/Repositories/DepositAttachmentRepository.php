<?php

namespace Modules\Deposit\Repositories;

use Illuminate\Support\Facades\Storage;
use Modules\Deposit\Contracts\DepositAttachmentContract;
use Modules\Deposit\Models\DepositAttachment;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositAttachmentRepository extends Repository implements DepositAttachmentContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor.
     *
     * @param DepositAttachment $model
     */
    public function __construct(DepositAttachment $model)
    {
        $this->slug = 'deposit_id';
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::get()
     */
    public function get(int $attachmentId, bool $download = false)
    {
        $attachment = $this->model->findOrFail($attachmentId);

        if (config('filesystems.cloud_enable')) {
            return $this->getFileFromSpace($attachment, $download);
        }

        return $this->getFileFromLocal($attachment, $download);
    }

    /**
     * Get file from digital ocean
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromSpace($attachment, $download)
    {
        $path     = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
        $contents = Storage::cloud()->get($path)->header(
            'Content-Type',
            $attachment->mime_type
        );

        if ($download) {
            echo $contents;
            exit;
        }
        return $contents;
    }

    /**
     * Get file from local
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromLocal($attachment, $download)
    {
        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($download) {
                header('Content-Description: File Transfer');\
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
            }

            header('Content-Type: ' . $attachment->mime_type);
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($contents));
            echo $contents;
            exit;
        }

        return;
    }
}
