<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositTransactionContract;
use Modules\Deposit\Models\DepositTransaction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DepositTransactionRepository extends Repository implements DepositTransactionContract
{
    use HasCrud;

    /**
     * Class constructor.
     *
     * @param DepositTransaction $model
     */
    public function __construct(DepositTransaction $model)
    {
        parent::__construct($model);
    }
}
