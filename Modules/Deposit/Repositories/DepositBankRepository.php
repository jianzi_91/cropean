<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositMethodBankContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Models\DepositBank;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DepositBankRepository extends Repository implements DepositBankContract
{
    use HasCrud;

    /**
     * Deposit method repository.
     *
     * @var DepositMethodContract
     */
    protected $methodRepo;

    /**
     * Class constructor.
     *
     * @param DepositBank $model
     * @param DepositMethodContract $methodContract
     */
    public function __construct(DepositBank $model, DepositMethodContract $methodContract)
    {
        parent::__construct($model);
        $this->methodRepo = $methodContract;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     * @see \Modules\Deposit\Contracts\DepositBankContract::getForDepositMethod()
     */
    public function getForDepositMethod(int $depositMethodId, int $userId)
    {
        $method = $this->methodRepo->find($depositMethodId);

        if (empty($method)) {
            throw new \Exception('invalid deposit method ID');
        }

        // Global means that all the bank accounts tied to this method are global across all users (i.e. non-VIP)
        if ($method->type->is_global) {
            $methodBankRepo = resolve(DepositMethodBankContract::class);
            return $methodBankRepo->getBankAccountsForMethod($depositMethodId);
        } else {
            $bankUserRepo = resolve(DepositBankUserContract::class);
            return $bankUserRepo->getBankAccountsForMethodAndUser($depositMethodId, $userId);
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankContract::getByType()
     */
    public function getByType(int $depositTypeId)
    {
        return $this->model
            ->where('deposit_type_id', $depositTypeId)
            ->where('is_active', true)
            ->paginate();
    }
}
