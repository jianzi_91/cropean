<?php

namespace Modules\Deposit\Repositories;

use Illuminate\Support\Facades\DB;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Models\DepositMethod;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositMethodRepository extends Repository implements DepositMethodContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor.
     *
     * @param DepositMethod $model
     */
    public function __construct(DepositMethod $model)
    {
        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositMethodContract::getUserEligibleMethods()
     */
    public function getUserEligibleMethods(int $userId)
    {
        $bankUserRepo = resolve(DepositBankUserContract::class);

        // First return the global ones
        $methods = $this->model
            ->join('deposit_types', 'deposit_types.id', 'deposit_methods.deposit_type_id')
            ->leftjoin('deposit_method_i18ns', 'deposit_method_i18ns.id', 'deposit_methods.id')
            ->where('deposit_types.is_global', true)
            ->where('deposit_methods.is_active', true)
            ->whereNull('deposit_methods.deleted_at')
            ->get(['deposit_methods.*']);

        // Now get user specific ones
        $nonGlobalMethodIds = $bankUserRepo->getModel()
            ->where('user_id', $userId)
            ->get(DB::raw('DISTINCT deposit_method_id'))
            ->pluck('deposit_method_id')
            ->toArray();

        if (!empty($nonGlobalMethodIds)) {
            $ids = implode(',', $nonGlobalMethodIds);

            $nonGlobalMethods = $this->model
                ->whereRaw("id IN ($ids)")
                ->where('is_active', true)
                ->get();

            $methods = $methods->concat($nonGlobalMethods);
        }

        return $methods;
    }
}
