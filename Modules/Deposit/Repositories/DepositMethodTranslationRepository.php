<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositMethodTranslationContract;
use Modules\Deposit\Models\DepositBankDetail;
use Modules\Deposit\Models\DepositMethodi18n;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositMethodTranslationRepository extends Repository implements DepositMethodTranslationContract
{
    use HasCrud, HasSlug;

    protected $languageContract;

    /**
     * Class constructor.
     *
     * @param DepositBankDetail $model
     */
    public function __construct(DepositMethodi18n $model, TranslatorLanguageContract $languageContract)
    {
        $this->slug             = 'deposit_method_id';
        $this->languageContract = $languageContract;

        parent::__construct($model);
    }

    /**
     * @param array $attributes
     * @param bool $forceFill
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function add(array $attributes = [], $forceFill = false)
    {
        $defaultLocale = config('deposit.default_locale');
        $description   = isset($attributes['description']) ? $attributes['description']  : null;

        if (!isset($description[$defaultLocale])) {
            throw new \Exception('Description are required for default locale');
        }

        foreach ($description as $locale => $value) {
            $translateAttributes = [
                'deposit_method_id' => $attributes['deposit_method_id'],
                'description'       => $attributes['description'][$locale],
            ];
            $translateAttributes['translator_language_id'] = $this->languageContract->findBySlug($locale)->id;
            $model                                         = $this->model->newInstance($translateAttributes);
            $model->save();
        }

        return false;
    }

    /**
     * @param \Plus65\Base\Repositories\Contracts\unknown $ids
     * @return int|void
     */
    public function delete($ids)
    {
        $this->model->where('deposit_method_id', $ids)->delete();
    }
}
