<?php

namespace Modules\Deposit\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Modules\Core\Traits\HasLock;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Contracts\CreditLockingContract;
use Modules\Deposit\Contracts\DepositAttachmentContract;
use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositBankDetailContract;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositContract;
use Modules\Deposit\Contracts\DepositMethodContract;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Contracts\DepositTransactionContract;
use Modules\Deposit\Contracts\DepositTypeContract;
use Modules\Deposit\Models\Deposit;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Setting\Contracts\SettingContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class DepositRepository extends Repository implements DepositContract
{
    use HasCrud, HasSlug, HasLock;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Depoait status repository.
     *
     * @var DepositStatusRepository $depositStatusRepo
     */
    protected $depositStatusRepo;
    /**
     * @var DepositMethodContract
     */
    private $depositMethodRepo;
    /**
     * @var DepositBankUserContract
     */
    private $depositBankUserRepo;

    private $exchangeRateRepo;

    private $bankCreditTypeRepository;

    private $depositBankRepo;

    private $depositBankDetailRepo;

    private $attachmentRepo;

    private $depositTypeRepo;

    private $statusRepo;

    private $creditLockingRepo;

    private $creditRepo;

    private $txRepo;

    /**
     * Class constructor.
     *
     * @param Deposit $model
     * @param DepositMethodContract $depositMethodContract
     * @param DepositBankUserContract $depositBankUserContract
     * @param DepositStatusContract $depositStatusRepo
     * @param ExchangeRateContract $exchangeRateContract
     * @param DepositBankContract $depositBankContract
     * @param DepositTypeContract $depositTypeContract
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param DepositAttachmentContract $attachmentContract
     * @param BankAccountCreditContract $creditContract
     * @param DepositBankDetailContract $bankDetailContract
     * @param DepositTransactionContract $txContract
     * @param SettingContract $settingContract
     * @param CreditLockingContract $creditLockingContract
     */
    public function __construct(
        Deposit $model,
        DepositMethodContract $depositMethodContract,
        DepositBankUserContract $depositBankUserContract,
        DepositStatusContract $depositStatusRepo,
        ExchangeRateContract $exchangeRateContract,
        DepositBankContract $depositBankContract,
        DepositTypeContract $depositTypeContract,
        BankCreditTypeContract $bankCreditTypeContract,
        DepositAttachmentContract $attachmentContract,
        BankAccountCreditContract $creditContract,
        DepositBankDetailContract $bankDetailContract,
        DepositTransactionContract $txContract,
        CreditLockingContract $creditLockingContract
    ) {
        $this->model                    = $model;
        $this->depositStatusRepo        = $depositStatusRepo;
        $this->depositBankUserRepo      = $depositBankUserContract;
        $this->depositMethodRepo        = $depositMethodContract;
        $this->exchangeRateRepo         = $exchangeRateContract;
        $this->bankCreditTypeRepository = $bankCreditTypeContract;
        $this->depositBankRepo          = $depositBankContract;
        $this->depositBankDetailRepo    = $bankDetailContract;
        $this->attachmentRepo           = $attachmentContract;
        $this->depositTypeRepo          = $depositTypeContract;
        $this->creditRepo               = $creditContract;
        $this->creditLockingRepo        = $creditLockingContract;
        $this->txRepo                   = $txContract;

        parent::__construct($model);
    }

    /**
     * @inheritDoc
     * @throws \Exception
     * @see \Modules\Deposit\Contracts\DepositContract::cancel()
     */
    public function cancel(int $depositId)
    {
        $deposit         = $this->model->lockForUpdate()->findOrFail($depositId);
        $cancelledStatus = $this->depositStatusRepo->findBySlug(DepositStatusContract::CANCELLED);

        if ($deposit->status->status->rawname != DepositStatusContract::PENDING) {
            throw new \Exception('deposit status must be pending in order to cancel');
        }

        return $this->depositStatusRepo->changeHistory($deposit, $cancelledStatus);
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositContract::getPendingCount()
     */
    public function getPendingCount()
    {
        $pendingStatus = $this->depositStatusRepo->findBySlug(DepositStatusContract::PENDING);

        return $this->model
            ->where('deposit_status_id', $pendingStatus->id)
            ->count();
    }

    public function request($request)
    {
        $data = $this->calculate($request);

        $depositMethod = $this->depositMethodRepo->find($request->deposit_method_id);
        $depositType   = $this->depositTypeRepo->findBySlug($this->depositTypeRepo::NON_VIP);

        $deposit = $this->add([
            'user_id'                   => $request->user()->id,
            'deposit_method_id'         => $depositMethod->id,
            'deposit_type_id'           => $depositType->id,
            'reference_number'          => generate_ref('DEP'),
            'currency'                  => $data['currencyFrom'],
            'amount'                    => $data['amount'],
            'total_amount'              => $data['amount'],
            'converted_currency'        => $data['currencyTo'],
            'converted_amount'          => $data['convertedAmount'],
            'exchange_rate'             => $data['exchangeRate'],
            'admin_fee_percentage'      => $data['adminFeePercentage'],
            'admin_fee'                 => $data['adminFee'],
            'issue_credit_type_id'      => $data['issuedCreditId'],
            'credit_type_exchange_rate' => $data['creditTypeExchangeRate'],
            'issued_amount'             => $data['issuedAmount'],
            'locale'                    => $request->locale,
        ]);

        $proofReceiptNumbers = $request->proof_receipt_number;
        foreach ($request->file('proof') as $key => $value) {
            $originalFilename = $value->getClientOriginalName();
            $mime             = $value->getMimeType();

            $filename = $value->store('deposits', 'local');

            $this->attachmentRepo->add([
                'deposit_id'        => $deposit->id,
                'filename'          => $filename,
                'original_filename' => $originalFilename,
                'mime_type'         => $mime,
                'receipt_number'    => $proofReceiptNumbers[$key],
            ]);
        }

        $selectedBank = $this->depositBankRepo->find($request->deposit_bank_id);

        $this->depositBankDetailRepo->add([
            'deposit_id'              => $deposit->id,
            'bank_name'               => $selectedBank->bank_name,
            'bank_branch'             => $selectedBank->bank_branch,
            'account_name'            => $selectedBank->account_name,
            'account_number'          => $selectedBank->account_number,
            'currency'                => $selectedBank->currency,
            'deposit_max'             => $selectedBank->deposit_max,
            'max_transaction'         => $selectedBank->max_transaction,
            'deposit_timing_from'     => $selectedBank->deposit_timing_from,
            'deposit_timing_to'       => $selectedBank->deposit_timing_to,
            'additional_instructions' => $selectedBank->additional_instructions,
        ]);
    }

    public function get($attachmentId, $download = false)
    {
        $attachment = $this->model->findOrFail($attachmentId);

        if (config('filesystems.cloud_enable')) {
            return $this->getFileFromSpace($attachment, $download);
        }

        return $this->getFileFromLocal($attachment, $download);
    }

    /**
     * Get file from digital ocean
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromSpace($attachment, $download)
    {
        $path     = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
        $contents = Storage::cloud()->get($path)->header(
            'Content-Type',
            $attachment->mime_type
        );

        if ($download) {
            echo $contents;
            exit;
        }
        return $contents;
    }

    /**
     * Get file from local
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromLocal($attachment, $download)
    {
        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /*
     *  Request is require;
     *  1. deposit_bank_id
     *  2. amount
     *  3. deposit_method_id
     */
    public function calculate($request)
    {
        $depositBankCurrency = $this->depositBankRepo->find($request->deposit_bank_id)->currency;

        $exchangeRate           = $this->exchangeRateRepo->findBySlug($depositBankCurrency)->sell_rate;
        $creditTypeExchangeRate = $this->exchangeRateRepo->findBySlug(BankCreditTypeContract::CAPITAL_CREDIT)->buy_rate;
        $depositMethod          = $this->depositMethodRepo->find($request->deposit_method_id);

        $amount             = $request->amount;
        $adminFeePercentage = $depositMethod->admin_fee_percentage;

        $adminFee        = bcmul($amount, $adminFeePercentage, credit_precision());
        $convertedAmount = bcmul($amount, $exchangeRate, credit_precision());
        $issuedAmount    = bcmul($amount, $creditTypeExchangeRate, credit_precision());

        $currencyFrom   = $this->exchangeRateRepo->findBySlug(BankCreditTypeContract::USD_CREDIT);
        $currencyTo     = $this->exchangeRateRepo->findBySlug($depositBankCurrency);
        $currencyIssue  = $this->exchangeRateRepo->findBySlug(BankCreditTypeContract::CAPITAL_CREDIT);
        $issuedCreditId = $this->bankCreditTypeRepository->findBySlug(BankCreditTypeContract::CAPITAL_CREDIT)->id;

        $data = [
            'amount'                   => $amount,
            'convertedAmount'          => $convertedAmount,
            'issuedAmount'             => $issuedAmount,
            'adminFee'                 => $adminFee,
            'adminFeePercentage'       => $adminFeePercentage,
            'exchangeRate'             => $exchangeRate,
            'creditTypeExchangeRate'   => $creditTypeExchangeRate,
            'currencyFrom'             => $currencyFrom->rawcurrency,
            'currencyTo'               => $currencyTo->rawcurrency,
            'currencyIssue'            => $currencyIssue->rawcurrency,
            'currencyFromTranslation'  => $currencyFrom->currency,
            'currencyToTranslation'    => $currencyTo->currency,
            'currencyIssueTranslation' => $currencyIssue->currency,
            'issuedCreditId'           => $issuedCreditId,
        ];

        return $data;
    }

    public function approveRequest($request, $deposit)
    {
        $amount = $request->amount;
        if ($request->action == 'reject') {
            $amount = $deposit->final_total_amount;
        }

        if ($request->action == 'approve') {
            $status = $this->depositStatusRepo->findBySlug($this->depositStatusRepo::APPROVED);

            $txCode = $this->creditRepo->add(
                $deposit->issue_credit_type_id,
                $deposit->issued_amount,
                $deposit->user_id,
                get_bank_transaction_type(BankTransactionTypeContract::DEPOSIT)->id
            );

            $this->txRepo->add([
                'deposit_id'                => $deposit->id,
                'credit_type_exchange_rate' => $deposit->credit_type_exchange_rate,
                'transaction_code'          => $txCode,
            ]);

            $capitalCreditId = $this->bankCreditTypeRepository->findBySlug($this->bankCreditTypeRepository::CAPITAL_CREDIT)->id;

            if ($deposit->issue_credit_type_id === $capitalCreditId) {
                $penaltyDuration   = (setting('penalty_duration'));
                $penaltyPercentage = (setting('penalty_percentage'));

                $this->creditLockingRepo->add([
                    'user_id'                 => $deposit->user_id,
                    'transaction_code'        => $txCode,
                    'penalty_days'            => $penaltyDuration,
                    'penalty_fee_percentages' => $penaltyPercentage,
                    'start_date'              => Carbon::now(),
                    'end_date'                => Carbon::now()->addDay($penaltyDuration - 1),
                    'original_amount'         => $deposit->issued_amount,
                    'current_amount'          => $deposit->issued_amount,
                ]);
            }
        } else {
            $status = $this->depositStatusRepo->findBySlug($this->depositStatusRepo::REJECTED);
        }

        $this->depositStatusRepo->changeHistory($deposit, $status, ['updated_by' => $request->user()->id, 'remarks' => $request->remarks]);
    }
}
