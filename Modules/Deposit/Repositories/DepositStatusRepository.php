<?php

namespace Modules\Deposit\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Deposit\Models\Deposit;
use Modules\Deposit\Models\DepositStatus;
use Modules\Deposit\Models\DepositStatusHistory;
use Modules\User\Models\UserStatus;

class DepositStatusRepository extends Repository implements DepositStatusContract
{
    use HasCrud, HasSlug, HasHistory;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param UserStatus  $model
     */
    public function __construct(DepositStatus $model, DepositStatusHistory $history)
    {
        $this->model = $model;
        $this->historyModel = $history;

        parent::__construct($model);
    }

    /**
     * {@inheritdoc}
     *
     * @see \Modules\User\Contracts\UserStatusContract::moveStatus()
     */
    public function moveStatus($depositId, $statusId, $remarks = null, $updatedBy = null)
    {
        $deposit = Deposit::findOrFail($depositId);
        $status  = $this->model->findOrFail($statusId);

        // Reset statuses
        DepositStatusHistory::where('deposit_id', $depositId)
            ->update(['is_current' => 0]);

        $history                    = new DepositStatusHistory;
        $history->deposit_id        = $depositId;
        $history->deposit_status_id = $statusId;
        $history->is_current        = 1;
        $history->remarks           = $remarks;
        $history->updated_by        = $updatedBy;

        return $history->save();
    }
}
