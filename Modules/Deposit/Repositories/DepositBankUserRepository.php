<?php

namespace Modules\Deposit\Repositories;

use Modules\Deposit\Contracts\DepositBankContract;
use Modules\Deposit\Contracts\DepositBankUserContract;
use Modules\Deposit\Contracts\DepositTypeContract;
use Modules\Deposit\Models\DepositBankUser;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DepositBankUserRepository extends Repository implements DepositBankUserContract
{
    use HasCrud;

    /**
     * Deposit bank repository.
     *
     * @var DepositBankContract
     */
    protected $bankRepo;

    protected $depositTypeRepo;

    /**
     * Class constructor.
     *
     * @param DepositBankUser $model
     * @param DepositBankContract $bankContract
     * @param DepositTypeContract $depositTypeContract
     */
    public function __construct(DepositBankUser $model, DepositBankContract $bankContract, DepositTypeContract $depositTypeContract)
    {
        parent::__construct($model);

        $this->bankRepo = $bankContract;
        $this->depositTypeRepo = $depositTypeContract;
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::deleteForBank()
     */
    public function deleteForBank(int $depositBankId)
    {
        $this->model
            ->where('deposit_bank_id', $depositBankId)
            ->delete();
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::getBankAccountsForMethodAndUser()
     */
    public function getBankAccountsForMethodAndUser(int $depositMethodId, int $userId)
    {
        return $this->bankRepo
            ->getModel()
            ->join('deposit_bank_users', 'deposit_bank_users.deposit_bank_id', 'deposit_banks.id')
            ->where('deposit_bank_users.user_id', $userId)
            ->where('deposit_banks.is_active', true)
            ->whereNull('deposit_banks.deleted_at')
            ->whereNull('deposit_bank_users.deleted_at')
            ->get(['deposit_banks.*']);
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::getBankAccountsForMethodAndUser()
     */
    public function getDepositBankAccountsForMethodAndUser(int $depositMethodId, int $userId)
    {
        return $this->model
            ->where('deposit_bank_users.user_id', $userId)
            ->whereNull('deleted_at')
            ->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::assignBankToUser()
     */
    public function assignBankToUser(int $userId, int $depositMethodId, int $depositBankId)
    {
        // Ensure there is no existing similar record
        $existingRecordCount = $this->model
            ->where('user_id', $userId)
            ->where('deposit_method_id', $depositMethodId)
            ->where('deposit_bank_id', $depositBankId)
            ->count();

        if (0 < $existingRecordCount) {
            return;
        }

        return $this->add([
            'user_id'           => $userId,
            'deposit_method_id' => $depositMethodId,
            'deposit_bank_id'   => $depositBankId,
        ]);
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::unassignBankFromUser()
     */
    public function unassignBankFromUser(int $userId, int $depositMethodId, int $depositBankId)
    {
        $this->model
            ->where('user_id', $userId)
            ->where('deposit_method_id', $depositMethodId)
            ->where('deposit_bank_id', $depositBankId)
            ->delete();
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::assignAllBanksToUser()
     */
    public function assignAllBanksToUser(int $userId, int $depositMethodId)
    {
        // This method is not SQL efficient, Should fix (Benson). For now just do easy method first.
        $vipType = $this->depositTypeRepo->findBySlug(DepositTypeContract::VIP);
        $banks   = $this->bankRepo->getByType($vipType->id);

        foreach ($banks as $bank) {
            $this->assignBankToUser($userId, $depositMethodId, $bank->id);
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\Deposit\Contracts\DepositBankUserContract::unassignAllBanksFromUser()
     */
    public function unassignAllBanksFromUser(int $userId, int $depositMethodId)
    {
        $this->model
            ->where('user_id', $userId)
            ->where('deposit_method_id', $depositMethodId)
            ->delete();
    }
}
