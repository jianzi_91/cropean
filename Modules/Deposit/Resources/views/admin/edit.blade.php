@extends('templates.admin.master')
@section('title', __('a_page_title.deposit request details'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('a_deposit_request_detail.deposits'), 'route'=>route('member.deposits.index')],
        ['name'=>__('a_deposit_request_detail.deposit request details')]
    ],
    'header'=>__('a_deposit_request_detail.deposit request details'),
    'backRoute'=> route('admin.deposits.index')
])

<div class="row">
    <div class="col-lg-8 col-xs-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title">{{ __('a_deposit_request_detail.status') }}</h4>
                    <div class="card-title kyc_status">{{ $deposit->status->status->name_translation }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                {{ Form::open(['method'=>'put', 'id'=>'deposits', 'route' => ['admin.deposits.update', $deposit->id]]) }}
                    <input id="action" type="hidden" name="action" value="">
                    {{ Form::formText('created_at', $deposit->created_at, __('a_deposit_request_detail.request date'), ['readonly']) }}
                    {{ Form::formText('member_id', $deposit->user->member_id, __('a_deposit_request_detail.member id'), ['readonly']) }}
                    {{ Form::formText('name', $deposit->user->name, __('a_deposit_request_detail.name'), ['readonly']) }}
                    {{ Form::formText('email', $deposit->user->email, __('a_deposit_request_detail.email'), ['readonly']) }}
                    {{ Form::formText('reference_number', $deposit->reference_number, __('a_deposit_request_detail.reference number'), ['readonly']) }}
                    {{ Form::formText('deposit_type', $deposit->method->name, __('a_deposit_request_detail.deposit type'), ['readonly']) }}
                    {{ Form::formText('requested_deposit_amount', amount_format($deposit->amount, credit_precision()). ' ' . $baseCurrency, __('a_deposit_request_detail.requested deposit amount'), ['readonly']) }}
                    {{ Form::formText('admin_fee', amount_format($deposit->admin_fee, credit_precision()) . ' ' . $baseCurrency, __('a_deposit_request_detail.admin fee'), ['readonly']) }}
                    {{ Form::formText('total_request_amount', amount_format(bcadd($deposit->amount, $deposit->admin_fee, credit_precision()), credit_precision()). ' ' . $baseCurrency, __('a_deposit_request_detail.total requested deposit amount usd'), ['readonly']) }}
                    {{ Form::formText('total_amount_transferred', amount_format(bcadd($deposit->converted_amount,bcmul($deposit->admin_fee,$deposit->exchange_rate,4), credit_precision()),credit_precision()). ' ' . $depositCurrency, __('a_deposit_request_detail.total amount transferred rmb'), ['readonly']) }}
                    {{ Form::formText('total_converted_amount', amount_format($deposit->issued_amount,credit_precision()). ' ' . $issueCurrency, __('a_deposit_request_detail.total capital credits to receive'), ['readonly']) }}
                    @switch($deposit->deposit_status_id)
                        @case(1)
                    <!-- pending -->
                        {{ Form::formTextArea('remarks', old('remarks'), __('a_deposit_request_detail.remarks, for rejections only')) }}
                        @break
                        @default
                    <!-- approved / rejected -->
                        {{ Form::formText('remarks', $deposit->status->remarks ?? "-", __('a_deposit_request_detail.remarks'), ['readonly']) }}
                        @break
                    @endswitch

                    @if($pendingStatusId == $deposit->deposit_status_id)
                    <!-- if deposit status is pending, show: -->
                        <div class="row d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary cro-btn-primary mr-1 js-approve">{{ __('a_deposit_request_detail.approve') }}</button>
                            <button type="submit" class="btn btn-secondary cro-btn-secondary js-reject">{{ __('a_deposit_request_detail.reject') }}</button>
                        </div>
                    @endif
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-xs-12">
        <div class="card">
            <div>
                <div class="card-body">
                    <div class="widget-label">{{ __('a_deposit_request_detail.exchange rate usd to rmb') }}</div>
                    <div class="widget-value" id="exchange-rate">1.00 : {{ amount_format($deposit->exchange_rate, credit_precision()) }}</div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title pb-1 m-0">{{ __('a_deposit_request_detail.company bank information') }}</h4>
                    {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.bank name'), $depositBankDetails->bank_name) }}
                    {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.bank branch'), $depositBankDetails->bank_branch) }}
                    {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.bank account name'), $depositBankDetails->account_name) }}
                    {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.bank account number'), $depositBankDetails->account_number) }}
                    {{-- @if($depositBankDetails->deposit_timing_from == $depositBankDetails->deposit_timing_to)
                        {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.deposit timing'), __('a_deposit_request_detail.24 hours')) }}
                    @else
                        {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.deposit timing from'), $depositBankDetails->deposit_timing_from) }}
                        {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.deposit timing to'), $depositBankDetails->deposit_timing_to) }}
                    @endif --}}
                    {{ Form::formWidgetInfo('bank-info', __('a_deposit_request_detail.deposit limit'), amount_format($depositBankDetails->deposit_max)) }}
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title m-0 p-0">{{ __('a_deposit_request_detail.transaction receipt number & transaction proof') }}</h4>
                    @foreach($deposit->attachments as $attachment)
                    <div class="mt-2">
                        @if($attachment->mime_type == 'application/pdf')
                        <a href="{{$attachment->admin_url}}" target="_blank">
                            <object data="{{$attachment->admin_url}}" type="application/pdf" class="w-100" height="500">
                                <iframe src="{{$attachment->admin_url}}" type="application/pdf"></iframe>
                            </object>
                        </a>
                        @else
                        <a href="{{$attachment->admin_url}}" target="_blank">
                            <div class="row d-flex justify-content-center align-items-center">
                                <img src="{{ $attachment->admin_url }}" class="img-fluid border border-grey rounded"/>
                            </div>
                        </a>
                        @endif
                        <br />
                        {{ Form::formWidgetInfo('trans_num', __('a_deposit_request_detail.transaction receipt number'), $attachment->receipt_number, 'col-7', 'col-5') }}
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(function () {
        var deposit_form = $('#deposits')
        var deposit_amount = parseFloat('{{ $deposit->final_total_amount }}');
        $('.js-approve').on('click', function (e) {
            $('#action').val('approve');

            $(this).attr('disabled', 'disabled');
            $('.js-reject').attr('disabled', 'disabled');

            // <input type="text" value="{{$max}}">
            // <input type="text" value="{{$min}}">

            var max = parseFloat('{{ $max }}')
            var min = parseFloat('{{ $min }}')
            var desposit_currency = '{{ $depositCurrency }}'
            var value = deposit_amount;

            if (value < min || value > max) {
                swal.fire({
                    title: "{{ __('a_deposit_request_detail.confirm approval') }}",
                    html: "<br/><div><p>{{__('a_deposit_request_detail.deposited amount usd')}}: " + value  + "</p><p>{{__('a_deposit_request_detail.minimum amount')}}: " + min + ' ' + desposit_currency + "</p><p>{{__('a_deposit_request_detail.maximum amount')}}: " + max + ' ' + desposit_currency + "</p></div><p>{{ __('a_deposit_request_detail.are you sure you want to approve this deposit?') }}</p>",
                    showCancelButton: true,
                    cancelButtonText: "{{ __('a_deposit_request_detail.no') }}",
                    confirmButtonText: "{{ __('a_deposit_request_detail.yes') }}",
                    customClass: {
                        confirmButton: 'btn btn-primary cro-btn-primary',
                        cancelButton: 'btn btn-secondary cro-btn-secondary'
                    },
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        deposit_form.submit();
                    } else {
                        $('.js-approve').attr('disabled', false);
                        $('.js-reject').attr('disabled', false);
                    }
                });
            } else {
                deposit_form.submit();
            }

        });

        $('.js-reject').on('click', function (e) {
            $('#action').val('reject');

            $(this).attr('disabled', 'disabled');
            $('.js-approve').attr('disabled', 'disabled');

            deposit_form.submit();
        });
    });
</script>
@endpush
