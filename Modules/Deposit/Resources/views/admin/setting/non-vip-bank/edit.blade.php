<div class="card-box">
    <h4 class="mt-0 mb-3 header-title">{{ __('a_non_vip_bank.non-vip bank selector') }}</h4>

    <div class="table-responsive">
        <table class="table table-hover mb-0">
            <thead class="thead-dark">
                <tr>
                    <th></th>
                    <th>{{ __('a_non_vip_bank.bank name') }}</th>
                    <th>{{ __('a_non_vip_bank.bank account name') }}</th>
                    <th>{{ __('a_non_vip_bank.unique identifier') }}</th>
                    <th>{{ __('a_non_vip_bank.bank branch code') }}</th>
                    <th>{{ __('a_non_vip_bank.account number') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($banks as $bank)
                <tr>
                    <td class="radios">{{ Form::formRadio('bank-' . $bank->id, 'bank-' . $bank->id, $bank->id, ($bank->id == $currentSelectedBankId)) }}</td>
                    <td>{{ $bank->bank_name }}</td>
                    <td>{{ $bank->account_name }}</td>
                    <td>{{ $bank->unique_id }}</td>
                    <td>{{ $bank->branch }}</td>
                    <td>{{ $bank->account_number }}</td>
                </tr>
                @empty
                  @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_non_vip_bank.no records') ])
                @endforelse
            </tbody>
        </table>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(function () {
        $(".radios input").on("change", function () {
          var selectedId = $(this).val();
          var allCheckboxIds = [];

            @foreach ($banks as $bank)
                allCheckboxIds.push('bank-{{ $bank->id }}');
            @endforeach

          axios.put('{{ route('admin.deposit.banks.nonvip.update') }}', { deposit_bank_id: selectedId })
            .then(function (response) {
              if (response.data.result == 1) {
                CreateNoty({ type: "success", text:"{{ __('a_member_management_banks.bank updated successfully') }}"});

                allCheckboxIds.forEach(function (id, index) {
                  $('#' + id).prop("checked", false);
                  $('#bank-' + selectedId).prop("checked", true);
                });
              } else {
                CreateNoty({ type: "error", text:"{{ __('a_member_management_banks.fail to update bank') }}"});
              }
            })
            .catch(function (error) {
              CreateNoty({ type: "error", text:"{{ __('a_member_management_banks.fail to update bank') }}"});
              $('#checkAll').prop("checked", false);
            })
        });
    })
</script>
@endpush
