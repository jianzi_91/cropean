@extends('templates.admin.master')
@section('title', __('a_page_title.settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
    ['name'=>__('s_breadcrumb.settings'), 'route' => route('admin.deposit-methods.index')],
    ['name'=>__('s_breadcrumb.deposit methods')]
]])
{{ Form::open(['method'=>'put', 'route' => 'admin.deposit-methods.update', 'id'=>'deposit-methods']) }}
<div class="row mt-3">
    <div class="col-lg-8 col-md-7 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h4 class="mt-0 mb-3 header-title">{{ __('a_deposit_method_edit.deposit method') }}</h4>
                        {{ Form::formSelect('deposit_method_id', deposit_methods_dropdown(), old('deposit_method_id', 1), __('a_deposit_method_edit.deposit method'), ['required'=>true]) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h4 id="second-header" class="mt-0 mb-3 header-title"></h4>
                        {{ Form::formText('name', old('name'), __('a_deposit_method_edit.deposit method name'), ['required'=>true, 'readonly']) }}
                        {{ Form::formText('admin_fee_percentage', old('admin_fee_percentage'), __('a_deposit_method_edit.admin fee percentage'), ['required'=>true]) }}
                        {{ Form::formText('min', old('min'), __('a_deposit_method_edit.minimum deposit amount'), ['required'=>true]) }}
                        {{ Form::formText('max', old('max'), __('a_deposit_method_edit.maximum deposit amount'), ['required'=>true]) }}
                        {{ Form::formText('multiple', old('multiple'), __('a_deposit_method_edit.deposit amount multiple'), ['required'=>true]) }}
                        {{ Form::formSelect('is_active', active_dropdown(), old('is_active'), __('a_deposit_method_edit.is active'), ['required'=>true]) }}
                        @foreach(languages() as $code => $language)
                            {{ Form::formText("description[{$code}]", old("description[{$code}]"), __('a_deposit_method_edit.description'). ' ('. $code . ')', ['errorkey'=>"description.{$code}", 'required'=>true]) }}
                        @endforeach
                        <button type="button" class="btn btn-primary mt-3 prevent-spamming">{{ __('a_deposit_method_edit.proceed') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection

@push('scripts')
<script type="text/javascript">


$(function () {
    $('#deposit_method_id').on('change', function (e) {
        if ($(this).val() == '') return;
        updateMethodDetails($(this).val());
    }).change();
});

function updateMethodDetails(depositMethodId) {
    axios.get("/deposit-methods/" + depositMethodId)
        .then((response) => {

            $('#second-header').text(response.data.name);

            // If there is Laravel validation error, skip this
            var update = {{ $errors->isEmpty() ? 'true' : 'false' }};

            if (update) {
                $('#name').val(response.data.name);
                $('#admin_fee_percentage').val(response.data.admin_fee_percentage);
                $('#min').val(response.data.min);
                $('#max').val(response.data.max);
                $('#multiple').val(response.data.multiple);
                $('#is_active').select2('destroy').val(response.data.is_active).select2();
                $('input[name="description[cn]"]').val(response.data.description.cn);
                $('input[name="description[en]"]').val(response.data.description.en);
            }
        })
        .catch(err => {
            console.log(err);
        });
}
</script>
@endpush
