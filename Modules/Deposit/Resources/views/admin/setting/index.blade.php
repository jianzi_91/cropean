@extends('templates.admin.master')
@section('title', __('a_page_title.deposits settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_deposits_settings.deposits')],
    ],
    'header'=>__('a_deposits_settings.deposits'),
])

<div class="row">
  <div class="col-xs-12 col-lg-6">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          {{ Form::open(['method'=>'put', 'route' => 'admin.deposits.settings.update', 'id'=>'deposit', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
          {{ Form::formNumber('max', number_format($setting->max, 2, '.', ''), __('a_deposits_settings.deposit limit')) }}
          {{-- {{ Form::formTime('deposit_time', '', __('a_deposits_settings.deposit timing')) }} --}}
          {{ Form::formNumber('min', number_format($setting->min, 2, '.', ''), __('a_deposits_settings.deposit minimum')) }}
          {{ Form::formNumber('multiple', trim_zero($setting->multiple), __('a_deposits_settings.deposit multiples')) }}
          {{ Form::formNumber('admin_fee_percentage', bcmul($setting->admin_fee_percentage, 100, 2), __('a_deposits_settings.admin fee percentage')) }}
          <div class="d-flex justify-content-end">
          {{ Form::formButtonPrimary('btn_submit', __('a_deposits_settings.save')) }}
          {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection