@extends('templates.admin.master')
@section('title', __('a_page_title.settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.company banks'), 'route' => route('admin.deposit-banks.index')],
        ['name'=>__('s_breadcrumb.edit company bank')]
    ],
    'header'=>__('a_deposit_bank_edit.edit company bank'),
    'backRoute'=>route('admin.deposit-banks.index'),
])

    <div class="col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                        {{ Form::open(['method'=>'put', 'route' => ['admin.deposit-banks.update', $bank->id], 'id'=>'update-bank', 'onsubmit' => 'submit_btn.disabled = true; return true;']) }}
                        <input type="hidden" name="is_24_hours" value=1>
                        {{ Form::formText('bank_name', old('bank_name', $bank->bank_name), __('a_deposit_bank_edit.bank name'), ['required'=>true]) }}

                        {{ Form::formText('bank_branch', old('bank_branch', $bank->bank_branch), __('a_deposit_bank_edit.bank branch'), ['required'=>true]) }}

                        {{ Form::formText('account_name', old('account_name', $bank->account_name), __('a_deposit_bank_edit.bank account name'), ['required'=>true]) }}

                        {{ Form::formText('account_number', old('account_number', $bank->account_number), __('a_deposit_bank_edit.account number'), ['required'=>true]) }}

                        {{ Form::formText('deposit_max', old('deposit_max', $bank->deposit_max), __('a_deposit_bank_edit.maximum deposit amount'), ['required'=>true]) }}

                        {{ Form::formText('additional_instructions', old('additional_instructions', $bank->additional_instructions), __('a_deposit_bank_edit.additional instructions')) }}

{{--                        {{ Form::formTime('deposit_timing_from', old('deposit_timing_from', $bank->deposit_timing_from), __('a_deposit_bank_edit.deposit from'), [], true) }}--}}

{{--                        {{ Form::formTime('deposit_timing_to', old('deposit_timing_to', $bank->deposit_timing_to), __('a_deposit_bank_edit.deposit to'), [], true) }}--}}

                        {{ Form::formPassword('secondary_password', '', __('a_deposit_bank_edit.secondary password'), ['required'=>true]) }}

                        {{ Form::formPassword('security_password', '', __('a_deposit_bank_edit.security password'), ['required'=>true]) }}

                        {{ Form::formCheckbox('is_default', 'is_default', true, __('a_deposit_bank_create.make this as default bank'), ['class'=>'js-checkbox', 'isChecked' => $depositMethodBank ?? false]) }}

                        <div class="row d-flex justify-content-end mt-2 pr-2">
                            <button name="submit_btn" type="submit" class="btn btn-primary cro-btn-primary">{{ __('a_deposit_bank_edit.proceed') }}</button>
                        </div>

                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

