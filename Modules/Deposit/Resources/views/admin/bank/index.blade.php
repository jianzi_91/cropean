@extends('templates.admin.master')
@section('title', __('a_page_title.company banks'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_deposit_banks.company banks')],
    ],
    'header'=>__('a_deposit_banks.company banks')
])

<div class="pb-2">
    @can('admin_deposit_bank_create')
        <a class="btn btn-primary" href="{{ route('admin.deposit-banks.create') }}">{{ __('a_bank_list.add new bank') }}</a>
    @endcan
</div>

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_deposit_banks.date'), array(), false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('bank_name', request('bank_name'), __('a_deposit_banks.bank name'), [], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('account_number', request('account_number'), __('a_deposit_banks.account number'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_deposit_banks.bank list') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                @can('admin_set_default_nonvip_bank')
                <th>{{ __('a_deposit_banks.default') }}</th>
                @endcan
                <th>{{ __('a_deposit_banks.date created') }}</th>
                <th>{{ __('a_deposit_banks.bank name') }}</th>
                <th>{{ __('a_deposit_banks.bank branch') }}</th>
                <th>{{ __('a_deposit_banks.bank account name') }}</th>
                <th>{{ __('a_deposit_banks.account number') }}</th>
                <th>{{ __('a_deposit_banks.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($banks as $bank)
            <tr>
                @can('admin_set_default_nonvip_bank')
                <td width="5%" class="radios">{{ Form::formRadio('bank-' . $bank->id, 'bank-' . $bank->id, $bank->id, (isset($depositMethodBank->deposit_bank_id) && $bank->id == $depositMethodBank->deposit_bank_id)) }}</td>
                @endcan
                <td>{{ $bank->created_at }}</td>
                <td>{{ $bank->bank_name }}</td>
                <td>{{ $bank->bank_branch }}</td>
                <td>{{ $bank->account_name }}</td>
                <td>{{ $bank->account_number }}</td>
                <td class="action">
                    <div class="d-flex pt-1">
                        @can('admin_deposit_bank_edit')
                        <a href="{{ route('admin.deposit-banks.edit', $bank->id) }}" title="{{ __('a_bank_list.edit') }}">
                            <i class="bx bx-pencil"></i>
                        </a>
                        @endcan
                        @can('admin_deposit_bank_delete')
                        <div>
                            <form action="{{ route('admin.deposit-banks.destroy', $bank->id) }}" method="POST">
                                {{ csrf_field() }}
                                @method('DELETE')
                                &nbsp;&nbsp;
                                <a href="#" class="js-delete">
                                    <i class="bx bx-trash"></i>
                                </button>
                            </form>
                        </div>
                        @endcan
                    </div>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_deposit_banks.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $banks->render() !!}
@endsection

@push('scripts')
<script type="text/javascript">
$(function(){
    $('.js-delete').on('click',function(e){
        e.preventDefault();
        var el = this;

	    swal.fire({
	        title: '{{ ucfirst(__("a_deposit_banks.do you want delete this bank?")) }}',
	        showCancelButton: true,
	        cancelButtonText: '{{ __("a_deposit_banks.no") }}',
            confirmButtonText: '{{ __("a_deposit_banks.yes") }}',
            customClass: {
                confirmButton: 'btn btn-primary cro-btn-primary',
                cancelButton: 'btn btn-secondary cro-btn-secondary'
            },
	        preConfirm: function(e){
                $(el).closest('form').submit();
	        }
        })
    })

    $(".radios input").on("change", function () {
          var selectedId = $(this).val();
          var allCheckboxIds = [];

            @foreach ($banks as $bank)
                allCheckboxIds.push('bank-{{ $bank->id }}');
            @endforeach

          axios.put('{{ route('admin.deposit.banks.nonvip.update') }}', { deposit_bank_id: selectedId })
            .then(function (response) {
              if (response.data.result == 1) {
                CreateNoty({ type: "success", text:"{{ __('a_member_management_banks.bank updated successfully') }}"});

                allCheckboxIds.forEach(function (id, index) {
                  $('#' + id).prop("checked", false);
                  $('#bank-' + selectedId).prop("checked", true);
                });
              } else {
                CreateNoty({ type: "error", text:"{{ __('a_member_management_banks.fail to update bank') }}"});
              }
            })
            .catch(function (error) {
              CreateNoty({ type: "error", text:"{{ __('a_member_management_banks.fail to update bank') }}"});
              $('#checkAll').prop("checked", false);
            })
        });

})
</script>
@endpush
