@extends('templates.admin.master')
@section('title', __('a_page_title.create company bank'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.company banks'), 'route' => route('admin.deposit-banks.index')],
        ['name'=>__('s_breadcrumb.create company bank')]
    ],
    'header'=>__('a_deposit_bank_create.create company bank'),
    'backRoute'=>route('admin.deposit-banks.index'),
])


<div class="col-lg-6 col-xs-12">
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                {{ Form::open(['method'=>'post', 'route' => 'admin.deposit-banks.store', 'id'=>'create-bank', 'onsubmit' => 'submit_btn.disabled = true; return true;']) }}

                {{ Form::formText('bank_name', old('bank_name'), __('a_deposit_bank_create.bank name'), ['required'=>true]) }}

                {{ Form::formText('bank_branch', old('bank_branch'), __('a_deposit_bank_create.bank branch'), ['required'=>true]) }}

                <input type="hidden" name="currency_id" value={{$currencyId}}>
                <input type="hidden" name="is_24_hours" value=1>
                {{ Form::formSelect('currency_id', currency_dropdown(), old('currency_id', $currencyId), __('a_deposit_bank_create.currency'), ['disabled'], true) }}

                {{ Form::formText('account_name', old('account_name'), __('a_deposit_bank_create.bank account name'), ['required'=>true]) }}

                {{ Form::formText('account_number', old('account_number'), __('a_deposit_bank_create.account number'), ['required'=>true]) }}

                {{ Form::formText('deposit_max', old('deposit_max'), __('a_deposit_bank_create.maximum deposit amount'), ['required'=>true]) }}

                {{ Form::formText('additional_instructions', old('additional_instructions'), __('a_deposit_bank_create.additional instructions')) }}

{{--                {{ Form::formTime('deposit_timing_from', old('deposit_timing_from'), __('a_deposit_bank_create.deposit from'), [], true) }}--}}

{{--                {{ Form::formTime('deposit_timing_to', old('deposit_timing_to'), __('a_deposit_bank_create.deposit to'), [], true) }}--}}

                {{ Form::formPassword('secondary_password', '', __('a_deposit_bank_create.secondary password'), ['required'=>true]) }}

                {{ Form::formPassword('security_password', '', __('a_deposit_bank_create.security password'), ['required'=>true]) }}

                {{ Form::formCheckbox('is_default', 'is_default', true, __('a_deposit_bank_create.make this as default bank'), ['class'=>'js-checkbox', 'isChecked' => false]) }}

                <div class="row d-flex justify-content-end mt-2 pr-2">
                    <button name="submit_btn" type="submit" class="btn btn-primary cro-btn-primary js-submit">{{ __('a_deposit_bank_create.proceed') }}</button>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
       $(function() {
            var submitted = false;

            $('.js-submit').on('click', function() {
                var cb = $('#is_default').is(':checked') ? true : false
                $('#is_default').val(cb)
                if (cb) $('#is_default_hidden').attr('disabled', true)
                else $('#is_default_hidden').attr('disabled', false)

                var db = $('#is_override').is(':checked') ? true : false
                $('#is_override').val(db)
                if (db) $('#is_override_hidden').attr('disabled', true)
                else $('#is_override_hidden').attr('disabled', false)

                if(!submitted){
                    submitted = true;
                    $('#commission').submit()
                }
            })
        })
    </script>
@endpush
