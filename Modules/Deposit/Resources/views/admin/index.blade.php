@extends('templates.admin.master')
@section('title', __('a_page_title.deposit requests'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_deposits.deposits requests')],
    ],
    'header'=>__('a_deposits.deposit requests')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('created_at', request('created_at'), __('a_deposit_list.request date'), array(), false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('reference_number', request('reference_number'), __('a_deposit_list.reference number'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('member_id', request('member_id'), __('a_deposit_list.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3 mt-2">
        {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_balance.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('email', request('email'), __('a_deposit_list.email'), [], false) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formSelect('deposit_status_id', deposit_statuses_dropdown(), request('deposit_status_id'), __('a_deposit_list.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_deposit.bank deposit requests')}}</h4>
            <div class="d-flex flex-row align-items-center">

                <div class="table-total-results mr-2">{{ __('a_deposit_list.total results:') }} {{ $deposits->total() }}</div>
                @if (request()->deposit_status_id == 1)
                    {{ Form::formButtonPrimary('btn-bulk-update', __('a_deposit_list.update'), 'button', []) }}
                @endif
                &nbsp;&nbsp;&nbsp; 
                @can('admin_deposit_export')
                    {{ Form::formTabSecondary(__('a_deposit_list.export table'), route('admin.deposits.export', array_merge(request()->except('page')))) }}
                @endcan
                
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                @if (request()->deposit_status_id == 1)
                <td class="text-center">{{ Form::formCheckbox(null, 'select-all') }}</td>
                @endif
                <th class="text-center">{{ __('a_deposit_list.action') }}</th>
                <th>{{ __('a_deposit_list.request date') }}</th>
                <th>{{ __('a_deposit_list.reference number') }}</th>
                <th>{{ __('a_deposit_list.receipt numbers') }}</th>
                <th>{{ __('a_deposit_list.full name') }}</th>
                <th>{{ __('a_deposit_list.member id') }}</th>
                <th>{{ __('a_deposit_list.email') }}</th>
                <th>{{ __('a_deposit_list.requested deposit amount usd') }}</th>
                <th>{{ __('a_deposit_list.admin fee usd') }}</th>
                <th>{{ __('a_deposit_list.total requested deposit amount usd') }}</th>
                <th>{{ __('a_deposit_list.total amount transferred cny') }}</th>
                <th>{{ __('a_deposit_list.total converted amount capital credit') }}</th>
                <th>{{ __('a_deposit_list.status') }}</th>
                <th>{{ __('a_deposit_list.invoice') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($deposits as $deposit)
            <tr>
                @if (request()->deposit_status_id == 1)
                    <td class="text-center">{{ Form::formCheckbox('cbx-deposit', 'cbx-deposit-' . $deposit->id, null, null, ['class' => 'cbx-deposit', 'data-id' => $deposit->id]) }}</td>
                @endif
                <td class="action text-center">
                    @can('admin_deposit_edit')
                        @if (!$deposit->is_cancelled)
                            <a href="{{ route('admin.deposits.edit', $deposit->id) }}">
                                @if($deposit->deposit_status_id == 1)
                                    <i class="bx bx-pencil"></i>
                                @else
                                    <i class="bx bx-show-alt"></i>
                                @endif
                            </a>
                        @endif
                    @endcan
                </td>
                <td>{{ $deposit->created_at }}</td>
                <td>{{ $deposit->reference_number }}</td>
                <td>{{ isset($deposit->receipt_numbers) ? $deposit->receipt_numbers : '-' }}</td>
                <td>{{ $deposit->name }}</td>
                <td>{{ $deposit->member_id }}</td>
                <td>{{ $deposit->email }}</td>
                <td>{{ amount_format($deposit->amount, credit_precision()) }}</td>
                <td>{{ amount_format($deposit->admin_fee, credit_precision()) }}</td>
                <td>{{ amount_format(bcadd($deposit->amount, $deposit->admin_fee, credit_precision()), credit_precision()) }}</td>
                <td>{{ amount_format(bcadd($deposit->converted_amount,$deposit->converted_admin_fee, credit_precision()),credit_precision()) }}</td>
                <td>{{ amount_format($deposit->issued_amount, credit_precision()) }}</td>
                <td>{{ __('s_deposit_statuses.' . $deposit->status_name) }}</td>
                <td>
                    @if($deposit->invoice)
                    <a href="{{ route('admin.invoices.show', ['invoice' => $deposit->reference_number]) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_deposit_list.invoice')}}" target="__blank"><i class="bx bx-receipt"></i></a>
                    @else
                    -
                    @endif
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('a_deposit_list.no records') ])
        @endforelse
        </tbody>
        {{ Form::open(['method'=>'post', 'route' => 'admin.deposits.bulk.update', 'id'=>'fm-bulk-update']) }}
        {{ Form::formHide('deposit_ids') }}
        {{ Form::formHide('remarks') }}
        {{ Form::formHide('action') }}
        {{ Form::close() }}
        @endcomponent
    </div>
</div>
{!! $deposits->render() !!}
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $("#select-all").click(function() {
            if ($(this).prop("checked")) {
                $('.cbx-deposit').attr('checked', 'checked');
            } else {
                $('.cbx-deposit').removeAttr('checked');
            }
            
        });

        $("#btn-bulk-update").click(function() {
            let deposit_ids = [];

            $.each(($('.cbx-deposit:checked')), function() {
                deposit_ids.push($(this).attr('data-id'));
            });

            if (deposit_ids.length === 0) {
                swal.fire({
                    type: 'error',
                    text: 'No deposit selected',
                })
                return;
            }

            $('#deposit_ids').val(deposit_ids);
            swal.fire({
                title: "{{ __('a_deposit_list.bulk update') }}",
                input: 'select',
                inputOptions: {
                    approve: "{{ __('a_deposit_list.approve') }}",
                    reject: "{{ __('a_deposit_list.reject') }}",
                },
                html: '<div class="form-group"><textarea class="form-control " id="sw-remarks" cols="50" rows="10" placeholder="Remarks" style="resize: none;"></textarea></div>',
                inputPlaceholder: "{{ __('a_deposit_list.please select a status') }}",
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                            let remarks = $('#sw-remarks').val();
                        if (value) {
                            if (value == 'reject' && remarks == "") {
                                resolve("{{ __('a_deposit_list.remarks is required for rejecting deposits') }}");
                                return;
                            }

                            $('#btn-bulk-update').attr('disabled','disabled');
                            
                            $('#remarks').val(remarks);
                            $('#action').val(value);

                            resolve();
                            $('#fm-bulk-update').submit();
                        } else {
                            resolve("{{ __('a_deposit_list.please select a status') }}")
                        }
                    })
                }
            })
        });
    });
</script>
@endpush