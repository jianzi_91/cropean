@extends('templates.member.master')
@section('title', __('m_page_title.create deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_deposits.deposits'), 'route'=>route('member.deposits.index')],
        ['name'=>__('m_deposits.deposit request details')]
    ],
    'header'=>__('m_deposits.deposit request details'),
    'backRoute'=> route('member.deposits.index')
])

<div class="row">
    <div class="col-lg-4 col-xs-12 order-2 order-lg-1">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title m-0 pb-1">{{ __('m_deposit_view.reference number'). ' '.$deposit->reference_number }}</h4>
                    {{ Form::formWidgetInfo('req_deposit_amount', __('m_deposit_view.requested deposit amount usd'), amount_format($deposit->amount,credit_precision(), '.', ''), 'col-7', 'col-5') }}
                    {{ Form::formWidgetInfo('admin_fee', __('m_deposit_view.admin fee - exclusive usd'), amount_format($deposit->admin_fee,credit_precision(), '.', ''), 'col-7', 'col-5') }}
                    {{ Form::formWidgetInfo('total_amount', __('m_deposit_view.total requested deposit amount usd'), amount_format(bcadd($deposit->amount, $deposit->admin_fee, credit_precision()), credit_precision(), '.', ''), 'col-7', 'col-5') }}
                    {{ Form::formWidgetInfo('remarks', __('m_deposit_view.remarks'), $deposit->status->remarks ?? "-", 'col-7', 'col-5') }}
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title">{{ __('m_deposit_view.status') }}</h4>
                    <div class="card-title kyc_status">{{ $deposit->status->status->name }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title m-0 pb-1">{{ __('m_deposit_view.company bank information') }}</h4>
                    {{ Form::formWidgetInfo('bank-name', __('m_deposit_view.bank name'), $depositBankDetails->bank_name) }}
                    {{ Form::formWidgetInfo('bank-branch', __('m_deposit_view.bank branch'), $depositBankDetails->bank_branch) }}
                    {{ Form::formWidgetInfo('account-name', __('m_deposit_view.bank account name'), $depositBankDetails->account_name) }}
                    {{ Form::formWidgetInfo('account-number', __('m_deposit_view.bank account number'), $depositBankDetails->account_number) }}
                    {{-- @if($depositBankDetails->deposit_timing_from == $depositBankDetails->deposit_timing_to)
                        {{ Form::formWidgetInfo('deposit-timing', __('m_deposit_view.deposit timing'), __('m_deposit_view.24 hours')) }}
                    @else
                        {{ Form::formWidgetInfo('deposit-timing-from', __('m_deposit_view.deposit timing from'), $depositBankDetails->deposit_timing_from) }}
                        {{ Form::formWidgetInfo('deposit-timing-to', __('m_deposit_view.deposit timing to'), $depositBankDetails->deposit_timing_to) }}
                    @endif --}}
                    {{ Form::formWidgetInfo('deposit-max', __('m_deposit_view.deposit limit'), amount_format($depositBankDetails->deposit_max)) }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-xs-12 order-3 order-lg-2">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title m-0 p-0">{{ __('m_deposit_view.transaction receipt number & transaction proof') }}</h4>
                    @foreach($deposit->attachments as $attachment)
                    <div class="mt-2">

                        @if($attachment->mime_type == 'application/pdf')
                        <a href="{{$attachment->member_url}}" target="_blank">
                            <object data="{{$attachment->member_url}}" type="application/pdf" class="w-100" height="500">
                                <iframe src="{{$attachment->member_url}}" type="application/pdf"></iframe>
                            </object>
                        </a>
                        @else
                        <a href="{{ $attachment->member_url }}" target="_blank">
                            <div class="row d-flex justify-content-center align-items-center">
                                <img src="{{ $attachment->member_url }}" class="img-fluid border border-grey rounded"/>
                            </div>
                        </a>
                        @endif
                        <br />
                        {{ Form::formWidgetInfo('trans_num', __('m_deposit_view.transaction receipt number'), $attachment->receipt_number, 'col-7', 'col-5') }}
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-xs-12 order-1 order-lg-3">
        <div class="card">
            <div>
                <div class="card-body">
                    <div class="widget-label">{{ __('m_deposit_view.total rmb to transfer') }}</div>
                    <div class="widget-value" id="total_amount_to_deposit">{{ amount_format(bcadd($deposit->converted_amount,bcmul($deposit->admin_fee,$deposit->exchange_rate,4), credit_precision()),credit_precision()) }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div>
                <div class="card-body">
                    <div class="widget-label">{{ __('m_deposit_view.total capital credits to receive') }}</div>
                    <div class="widget-value" id="issued_amount">{{ amount_format($deposit->issued_amount,credit_precision()) }}</div>
                </div>
            </div>
        </div>
        @include('templates.__fragments.components.credit-widget-bg-image', [
            'label'=>__('a_credits_statement_capital_credit.capital credits balance'),
            'value'=>amount_format(auth()->user()->capital_credit),
            'backgroundClass'=>'widget-capital-image',
        ])
        <div class="card">
            <div>
                <div class="card-body">
                    <div class="widget-label">{{ __('m_deposit_view.exchange rate usd to rmb') }}</div>
                    <div class="widget-value" id="exchange-rate">1.00 : {{ amount_format($deposit->exchange_rate, credit_precision()) }}</div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
  <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="image"
aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <h5 class="modal-title"></h5>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script type="text/javascript">
        function setValueToModal(attachment) {
            $('.modal-title').text();
            $('.modal-body').html(`<img src=${attachment.url} />`);

            $('#enlargeImageModal').modal()
        }
</script>
@endpush

