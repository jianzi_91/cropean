@extends('templates.member.master')
@section('title', __('m_page_title.create deposits'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs', [
        'breadcrumbs'=>[
            ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
            ['name'=>__('m_deposits.deposits'), 'route'=>route('member.deposits.index')],
            ['name'=>__('m_deposits.new deposit request')]
        ],
        'header'=>__('m_deposits.new deposit request'),
        'backRoute'=> route('member.deposits.index')
    ])

    <div class="row">
        <div class="col-lg-8 col-xs-12 order-2 order-lg-1">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">

                        {{ Form::open(['method'=>'post', 'route' => 'member.deposits.store', 'id'=>'deposit', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        <div class="row">
                            <div class="col-10">
                                <div id="hidden-fields">
                                    {{ Form::formSelect('deposit_method_id', user_deposit_methods_dropdown(), old('deposit_method_id'), __('m_deposit_create.type'), ['required'=>true, auth()->user()->IsUserVip?'':'readonly']) }}
                                    {{ Form::formSelect('deposit_bank_id', $bankAccountsArray, old('deposit_bank_id'), __('m_deposit_create.bank'), ['required'=>true]) }}
                                </div>
                                {{ Form::formNumber('amount', 0, __('m_deposit_create.deposit amount'), ['required'=>true, 'info'=>__('m_deposit_create.minimum :min, multiples of :multiple', ['min'=>'<span class="js-min"></span>', 'multiple'=>'<span class="js-multiple"></span>'])]) }}
                                {{ Form::formText('admin-fee-usd', '', __('m_deposit_create.admin fee exclusive'), ['readonly', 'info'=> __('m_deposit_create.admin fee percentage :adminFeePercentage', ['adminFeePercentage'=>'<span class="admin_fee_percentage"></span>']) . '%']) }}
                                {{ Form::formText('total-deposit-amount-usd', '', __('m_deposit_create.total deposit amount usd'), ['readonly']) }}
                                {{ Form::formText('exchange-rate', '', __('m_deposit_create.exchange rate'), ['readonly']) }}
                                {{ Form::formText('total_amount_to_deposit', '', __('m_deposit_create.total amount to transfer'), ['readonly']) }}
                            </div>
                            <div class="col-2"></div>
                        </div>
                       @if(!empty(old('proof_receipt_number')))
                           @foreach(old('proof_receipt_number') as $key => $oldValue)
                                <div class="proof-form{{ $key }}">
                                    <div class="row">
                                        <div class="col-10">
                                            {{ Form::formText('proof_receipt_number['. $key .']', $oldValue, __('m_deposit_create.receipt number'), ['id'=>'proof_receipt_number_'. $key .'', 'info' => __('m_deposit_create.deposit disclaimer')]) }}
                                        </div>
                                        <div class="delete-button" class="col-2">
                                            <button type="button" class="proof-delete" name="{{ $key }}" title="{{ __('m_deposit_create.remove') }}">
                                                <i class="bx bx-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-10">
                                            {{ Form::formFileUpload('proof['. $key .']', '', __('m_deposit_create.transaction proof'), __('m_deposit_create.choose file'), ['id'=>'proof-'. $key .'','accept'=>".jpeg,.pdf,.jpg,.gif,.png", 'dotId' => 'proof.'. $key], false) }}
                                        </div>
                                        <div class="col-2"></div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="proof-form0">
                                <div class="row">
                                    <div class="col-10">
                                        {{ Form::formText('proof_receipt_number[0]', '', __('m_deposit_create.receipt number'), ['id'=>'proof_receipt_number_0', 'info' => __('m_deposit_create.deposit disclaimer')]) }}
                                    </div>
                                    <div class="delete-button" class="col-2">
                                        <button type="button" class="proof-delete" name="0" title="{{ __('m_deposit_create.remove') }}">
                                            <i class="bx bx-trash"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-10">
                                        {{ Form::formFileUpload('proof[0]', '', __('m_deposit_create.transaction proof'), __('m_deposit_create.choose file'), ['id'=>'proof-0','accept'=>".jpeg,.pdf,.jpg,.gif,.png", 'dotId' => 'proof.0'], false) }}
                                    </div>
                                    <div class="col-2"></div>
                                </div>
                            </div>
                        @endif

                        <div class="pb-2 btn_more_proof_cont">
                            {{ Form::formButtonSecondary('btn_more_proof', __('m_deposit_create.upload more transaction'), 'button') }}
                        </div>

                        <div class="row">
                            <div class="col-10">
                                {{ Form::formPassword('secondary_password', '', __('m_deposit_create.secondary password'), ['required'=>true]) }}
                            </div>
                            <div class="col-2"></div>
                        </div>

                        <div class="d-flex justify-content-end">
                            {{ Form::formButtonPrimary('btn_submit',  __('m_deposit_create.submit')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-12 order-1 order-xs-2">
            @include('templates.__fragments.components.credit-widget-bg-image', [
                'label'=>__('m_deposit_create.capital credits balance'),
                'value'=>amount_format(auth()->user()->capital_credit),
                'backgroundClass'=>'widget-capital-image',
            ])

            <div class="card">
                <div>
                    <div class="card-body">
                        <div class="widget-label">{{ __('m_deposit_create.capital credits to receive') }}</div>
                        <div class="widget-value" id="issued_amount"></div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title pb-1 m-0">{{ __('m_deposit_create.company bank information') }}</h4>
                        {{ Form::formWidgetInfo('bank-name', __('m_deposit_create.bank name')) }}
                        {{ Form::formWidgetInfo('branch', __('m_deposit_create.bank branch code')) }}
                        {{ Form::formWidgetInfo('account-name', __('m_deposit_create.bank account name')) }}
                        {{ Form::formWidgetInfo('account-number', __('m_deposit_create.bank account number')) }}
                        {{-- {{ Form::formWidgetInfo('deposit-timing-from', __('m_deposit_create.deposit timing from')) }}
                        {{ Form::formWidgetInfo('deposit-timing-to', __('m_deposit_create.deposit timing to')) }} --}}
                        {{ Form::formWidgetInfo('deposit-max', __('m_deposit_create.deposit limit')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
                var proofCount = 1;
                var cancelToken = undefined
                var cancelSource = undefined
                $('button[name="0"]').hide(); //this hides the delete button on the first transaction proof.
                $('#hidden-fields').hide();

                //This ADDS a new empty transation proof
                $('#btn_more_proof').on('click', function(e) {

                    var cloneProof = $(`.proof-form${proofCount - 1}`).clone();
                    proofCount = proofCount + 1;

                    cloneProof.removeClass(`proof-form${proofCount - 2}`);
                    cloneProof.addClass(`proof-form${proofCount - 1}`);
                    cloneProof.find('input').val('');
                    cloneProof.find('input').each(function() {
                        this.id = this.id.replace(`-${proofCount - 2}`, `-${ proofCount - 1}`)
                        this.name = this.name.replace(`[${proofCount - 2}]`, `[${ proofCount - 1}]`)
                        $(this).removeClass('is-invalid')
                    });
                    cloneProof.find('label.custom-file-label').each(function(e) {
                        $(this).attr("for", `proof-${ proofCount - 1}`)
                        $(this).html('{{ __("m_deposit_create.choose file") }}')
                    })
                    cloneProof.find('button').each(function() {
                        this.id = this.id.replace(`${proofCount - 2}`, `${proofCount - 1}`)
                        this.name = this.name.replace(`${proofCount - 2}`, `${proofCount - 1}`)
                    })
                    cloneProof.find('.invalid-feedback').each(function() {
                        $(this).html('')
                    })
                    cloneProof.insertBefore('.btn_more_proof_cont');
                    $(`button.proof-delete[name="${proofCount - 1}"]`).show();
                });

                //this REMOVES a transaction proof
                $('body').on('click', '.proof-delete', function (e) {
                    $(this).parents(`.proof-form${this.name}`).fadeOut(300, function(){
                        $(this).remove();
                    });
                    proofCount = proofCount - 1;
                });


                //FE: running the trigger manually can retrieve record. Please assist to integrate to above.
                function updateMinAndMultiple() {
                    axios.get('/deposit-methods/' + $('#deposit_method_id').val())
                        .then((response) => {
                            $('.js-min').text(response.data.min);
                            $('.js-multiple').text(response.data.multiple);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                }

                updateMinAndMultiple();

                //fire this when keyup
                function updateConvertedAmountAndAdminFee() {
                    var method_id = $('#deposit_method_id').val(),
                        amount = $('#amount').val(),
                        deposit_bank_id = $('#deposit_bank_id').val();

                    if(amount && deposit_bank_id){
                        if (cancelSource) cancelSource.cancel('')
                        cancelToken = axios.CancelToken
                        cancelSource = cancelToken.source()
                        axios.get(
                          "{{ route('member.deposits.method.calculate') }}?deposit_method_id=" + method_id + "&amount=" + amount + "&deposit_bank_id=" + deposit_bank_id,
                          { cancelToken: cancelSource.token }
                        ).then((response) => {
                                $('#deposit-amount-usd').val(response.data.deposit_amount_usd + ' ' + response.data.currency_from);
                                $('#admin-fee-usd').val(response.data.admin_fee_usd + ' ' + response.data.currency_from);
                                $('.admin_fee_percentage').text(response.data.admin_fee_percentage);
                                $('#total-deposit-amount-usd').val(response.data.total_deposit_amount_usd + ' ' + response.data.currency_from);
                                $('#exchange-rate').val(response.data.exchange_rate);
                                $('#total_amount_to_deposit').val(response.data.total_amount_to_deposit + ' ' + response.data.currency_to);
                                $('#issued_amount').text(response.data.issued_amount);
                        }).catch(err => {
                            console.log(err);
                        });
                    }
                }

                updateConvertedAmountAndAdminFee();

                $('#amount').on('keyup', _.debounce(updateConvertedAmountAndAdminFee, 500))

                // $('#amount').keyup(function() {
                //     updateConvertedAmountAndAdminFee();
                // })


                function updateBankDetails() {
                    var lang = '{{ app()->getLocale() }}',
                        bank_id = $('#deposit_bank_id').val(),
                        deposit_method_id = $('#deposit_method_id').val()

                    if(bank_id){
                        axios.get('{{ route("member.deposits.method.bank.account.details") }}/' + bank_id + "?deposit_method_id=" + deposit_method_id)
                            .then((response) => {
                                $('#bank-name').text(response.data.bank_name);
                                $('#account-name').text(response.data.account_name);
                                $('#account-number').text(response.data.account_number);
                                $('#branch').text(response.data.branch);
                                $('#deposit-max').text(response.data.deposit_max);
                                $('#deposit-timing-from').text(response.data.deposit_timing_from ? response.data.deposit_timing_from : '-');
                                $('#deposit-timing-to').text(response.data.deposit_timing_to ? response.data.deposit_timing_to : '-');
                                $('#additional-instructions').text(response.data.additional_instructions ? response.data.additional_instructions : ' -');
                                $('#description').text(response.data.description[lang]);
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }
                }

                updateBankDetails();

                $('body').on('change', 'input[type="file"]', function(e) {
                    var id = $(this).attr('id')
                    $('label.custom-file-label[for="' + id + '"]').html(e.target.files[0].name)
                })

            }
        )
    </script>
@endpush
