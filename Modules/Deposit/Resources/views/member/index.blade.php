@extends('templates.member.master')
@section('title', __('m_page_title.deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.deposits')],
    ],
    'header'=>__('m_deposit_list.deposits')
])

<div class="pb-2">
@can('member_deposit_create')
@if(!has_default_bank())
<span data-toggle="tooltip" data-placement="bottom" title="{{ __('m_deposit_list.no default bank') }}">
    <button
        class="btn btn-primary cro-btn-primary"
        type="button"
        id="btn_create"
        name="btn_create"
        style="pointer-events: none;"
        disabled
    >
        {{ __('m_deposit_list.new deposit request') }}
    </button>
</span>
@elseif(validate_deposit_timing())
{{ Form::formButtonPrimary('btn_create', __('m_deposit_list.new deposit request'), 'button') }}
@endif
@endcan
</div>

@component('templates.__fragments.components.filter')
    <div class="col-12 col-lg-6 col-xl-3">
        {{ Form::formDateRange('created_at', request('created_at'), __('m_deposit_list.request date')) }}
    </div>
    <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
        {{ Form::formText('reference_number', request('reference_number'),__('m_deposit_list.reference number'), [], false) }}
    </div>
    <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
        {{ Form::formSelect('deposit_status_id', deposit_statuses_dropdown(), old('deposit_status_id', request('deposit_status_id')), __('m_deposit_list.status'), [], false) }}
    </div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_deposit_list.deposits')}}</h4>
            @can('member_deposit_export')
                {{ Form::formTabSecondary(__('m_deposit_list.export table'), route('member.deposits.export', array_merge(request()->except('page')))) }}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_deposit_list.request date') }}</th>
                <th>{{ __('m_deposit_list.reference number') }}</th>
                <th>{{ __('m_deposit_list.requested deposit amount usd') }}</th>
                <th>{{ __('m_deposit_list.admin fee usd') }}</th>
                <th>{{ __('m_deposit_list.total request deposit amount usd') }}</th>
                <th>{{ __('m_deposit_list.total deposit amount transferred rmb') }}</th>
                <th>{{ __('m_deposit_list.total converted amount') }}</th>
                <th>{{ __('m_deposit_list.status') }}</th>
                <th>{{ __('m_deposit_list.invoice') }}</th>
                <th>
                    @if(auth()->user()->can('member_deposit_cancel')||auth()->user()->can('member_deposit_view'))
                        {{ __('m_deposit_list.action') }}
                    @endif
                </th>
            </tr>
        </thead>
        <tbody>
        @forelse ($deposits as $deposit)
            <tr>
                <td>{{ $deposit->created_at }}</td>
                <td>{{ $deposit->reference_number }}</td>
                <td>{{ amount_format($deposit->amount,credit_precision()) }}</td>
                <td>{{ amount_format($deposit->admin_fee,credit_precision()) }}</td>
                <td>{{ amount_format(bcadd($deposit->amount,$deposit->admin_fee, credit_precision()),credit_precision()) }}</td>
                <td>{{ amount_format(bcadd($deposit->converted_amount,$deposit->converted_admin_fee, credit_precision()),credit_precision()) }}</td>
                <td>{{ amount_format($deposit->issued_amount,credit_precision()) }}</td>
                <td>{{ __('s_deposit_statuses.' . $deposit->status_name) }}</td>
                <td>
                    @if($deposit->invoice)
                    <a href="{{ route('member.invoices.show', ['invoice' => $deposit->reference_number]) }}" data-toggle="tooltip" data-placement="top" title="{{ __('m_deposit_list.invoice')}}" target="__blank"><i class="bx bx-receipt"></i></a>
                    @else
                    -
                    @endif
                </td>
                <td class="action">
                    <div class="d-flex">
                        @can('member_deposit_view')
                            <a href="{{ route('member.deposits.show', $deposit->id) }}">
                                <i class="bx bx-show-alt"></i>
                            </a>
                            &nbsp;&nbsp;
                        @endcan
                        @can('member_deposit_cancel')
                            @if ($deposit->can_cancel)
                                <div>
                                    <input type="hidden" value={{$deposit->id}} name="deposit_id" id="deposit_id" />
                                    <form action="{{ route('member.deposits.destroy', $deposit->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        @method('DELETE')
                                        <a role="button" class="js-delete" href="#">
                                            <i class="bx bx-trash"></i>
                                        </a>
                                    </form>
                                </div>
                            @endif
                        @endcan
                    </div>
                </td>
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('m_deposit.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $deposits->render() !!}
@endsection


@push('scripts')
        <script>
        $(function() {
            $('#btn_create').on('click', function(e) {
                e.preventDefault();
                window.location.href = "{{ route('member.deposits.create') }}"
            })
        });

        $('.js-delete').on('click',function(e){
        e.preventDefault();
        var el = this;

        swal.fire({
            title: '{{ ucfirst(__("m_deposit_list.do you want to delete this deposit?")) }}',
            showCancelButton: true,
            cancelButtonText: '{{ __("m_deposit_list.no") }}',
            confirmButtonText: '{{ __("m_deposit_list.yes") }}',
            customClass: {
                confirmButton: 'btn btn-primary cro-btn-primary',
                cancelButton: 'btn btn-secondary cro-btn-secondary',
            },
            preConfirm: function(e){
                $(el).closest('form').submit();
            }
        })
    })
    </script>
@endpush
