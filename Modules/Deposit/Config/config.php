<?php

return [
    'name'     => 'Deposit',
    'bindings' => [
        Modules\Deposit\Contracts\DepositStatusContract::class             => Modules\Deposit\Repositories\DepositStatusRepository::class,
        Modules\Deposit\Contracts\DepositContract::class                   => Modules\Deposit\Repositories\DepositRepository::class,
        \Modules\Deposit\Contracts\DepositSettingContract::class           => \Modules\Deposit\Repositories\DepositSettingRepository::class,
        \Modules\Deposit\Contracts\DepositMethodContract::class            => \Modules\Deposit\Repositories\DepositMethodRepository::class,
        \Modules\Deposit\Contracts\DepositBankUserContract::class          => \Modules\Deposit\Repositories\DepositBankUserRepository::class,
        \Modules\Deposit\Contracts\DepositTypeContract::class              => \Modules\Deposit\Repositories\DepositTypeRepository::class,
        \Modules\Deposit\Contracts\DepositBankContract::class              => \Modules\Deposit\Repositories\DepositBankRepository::class,
        \Modules\Deposit\Contracts\DepositMethodBankContract::class        => \Modules\Deposit\Repositories\DepositMethodBankRepository::class,
        \Modules\Deposit\Contracts\DepositAttachmentContract::class        => \Modules\Deposit\Repositories\DepositAttachmentRepository::class,
        \Modules\Deposit\Contracts\DepositBankDetailContract::class        => \Modules\Deposit\Repositories\DepositBankDetailRepository::class,
        \Modules\Deposit\Contracts\DepositMethodTranslationContract::class => \Modules\Deposit\Repositories\DepositMethodTranslationRepository::class,
        \Modules\Deposit\Contracts\DepositTransactionContract::class       => \Modules\Deposit\Repositories\DepositTransactionRepository::class,
        \Modules\Deposit\Contracts\DepositInvoiceContract::class           => \Modules\Deposit\Repositories\DepositInvoiceRepository::class,
    ],
    'max_amount'                    => 999999999999,
    'max_multiple_of'               => 999999999999,
    'max_processing_fee_percentage' => 100,
    'deposit_attachment_max_size'   => env('DEPOSIT_ATTACHMENT_MAX_SIZE', 2048),
    'storage'                       => 'deposits',
    'invoice_storage'               => 'invoices',
];
