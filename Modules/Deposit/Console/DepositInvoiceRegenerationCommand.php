<?php

namespace Modules\Deposit\Console;

use Illuminate\Console\Command;
use Modules\Deposit\Models\Deposit;
use Modules\Deposit\Models\DepositStatus;
use Modules\Deposit\Repositories\DepositInvoiceRepository;

class DepositInvoiceRegenerationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'deposit:generate-invoice {lang?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is use to generate deposit invoice manually.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DepositInvoiceRepository $invoiceRepo)
    {
        $this->invoiceRepo = $invoiceRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lang = $this->argument('lang') ?: 'en';

        $approvedStatus = DepositStatus::where('name', 'approved')->first();
        $deposits       = Deposit::where('deposit_status_id', $approvedStatus->id)->get();

        foreach ($deposits as $deposit) {
            if ($deposit->invoice == null) {
                $this->line("Generating invoice for $deposit->reference_number");
                $deposit->update([
                    'locale' => $lang,
                ]);
                $invoice = $this->invoiceRepo->generate($deposit);
                $this->line("Invoice generated for " . $invoice->reference_number);
            }
        }
    }
}
