<?php

namespace Modules\Deposit\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Deposit\Contracts\DepositContract;
use Modules\Deposit\Contracts\DepositStatusContract;
use Modules\Rbac\Contracts\RoleContract;
use Modules\User\Contracts\UserContract;

class ImportDummyDepositsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'import:deposits {file}';

    /**
     * Command body.445
     * @param UserContract $userRepo
     * @param RoleContract $roleRepo
     * @param BankAccountCreditContract $bankAccountCreditContract
     * @throws \Exception
     */
    public function handle(
        DepositContract $depositRepo,
        DepositStatusContract $depositStatusRepo,
        BankAccountCreditContract $creditRepo
    ) {
        if (app()->environment() == 'production') {
            $this->line('This command unable to process in production environment');
            exit;
        }

        $file = fopen(storage_path('test') . '/' . $this->argument('file'), 'r');

        $deposits  = [];
        $firstLine = true;

        while (!feof($file)) {
            $data = fgetcsv($file);

            if ($firstLine) {
                $firstLine = false;
            } else {
                $deposits[] = (object) [
                    'created_at' => empty($data[0]) ? Carbon::now()->toDateTimeString() : $data[0],
                    'updated_at' => empty($data[0]) ? Carbon::now()->toDateTimeString() : $data[0],
                    'user_id'    => $data[1],
                    'amount'     => empty($data[2]) ? 0 :$data[2],
                    'status'     => empty($data[3]) ? 'pending' :$data[3],
                ];
            }
        }

        fclose($file);

        DB::beginTransaction();

        try {
            foreach ($deposits as &$deposit) {
                $newDeposit = $depositRepo->add([
                    'user_id'                   => $deposit->user_id,
                    'deposit_method_id'         => 1,
                    'deposit_type_id'           => 1,
                    'reference_number'          => generate_ref('DEP'),
                    'currency'                  => 'USD',
                    'amount'                    => $deposit->amount,
                    'total_amount'              => $deposit->amount,
                    'converted_currency'        => 'CNY',
                    'converted_amount'          => $deposit->amount,
                    'exchange_rate'             => 1,
                    'admin_fee_percentage'      => 0,
                    'admin_fee'                 => 0,
                    'issue_credit_type_id'      => 1,
                    'credit_type_exchange_rate' => 1,
                    'issued_amount'             => $deposit->amount,
                    'locale'                    => 'en',
                ]);

                if ($deposit->status != 'pending') {
                    $status = $depositStatusRepo->findBySlug($deposit->status);
                    $depositStatusRepo->changeHistory($newDeposit, $status);

                    if ($deposit->status == 'approved') {
                        $creditRepo->add(BankCreditTypeContract::CAPITAL_CREDIT, $deposit->amount, $deposit->user_id, 'deposit');
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
