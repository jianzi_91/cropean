<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class DepositPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_deposit_list',
                        'title' => 'Admin Deposit List',
                    ],
                    [
                        'name'  => 'admin_deposit_edit',
                        'title' => 'Admin Deposit Edit',
                    ],
                    [
                        'name'  => 'admin_deposit_export',
                        'title' => 'Admin Deposit Export',
                    ],
                    [
                        'name'  => 'admin_deposit_settings_list',
                        'title' => 'Admin Deposit Settings List',
                    ],
                    [
                        'name'  => 'admin_deposit_settings_edit',
                        'title' => 'Admin Deposit Settings Edit',
                    ],
                    [
                        'name'  => 'admin_deposit_method_update',
                        'title' => 'Admin Deposit Method Update',
                    ],
                ]
            ],
            'member_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_deposit_list',
                        'title' => 'Member Deposit List',
                    ],
                    [
                        'name'  => 'member_deposit_create',
                        'title' => 'Member Deposit Create',
                    ],
                    [
                        'name'  => 'member_deposit_view',
                        'title' => 'Member Deposit View',
                    ],
                    [
                        'name'  => 'member_deposit_cancel',
                        'title' => 'Member Deposit Cancel',
                    ],
                    [
                        'name'  => 'member_deposit_export',
                        'title' => 'Member Deposit Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_deposit_list',
                'admin_deposit_edit',
                'admin_deposit_export',
                'admin_deposit_settings_list',
                'admin_deposit_settings_edit',
                'admin_deposit_method_update',
            ],
            Role::ADMIN => [
                'admin_deposit_list',
                'admin_deposit_edit',
                'admin_deposit_export',
                'admin_deposit_settings_list',
                'admin_deposit_settings_edit',
                'admin_deposit_method_update',
            ],
            Role::MEMBER => [
                'member_deposit_list',
                'member_deposit_create',
                'member_deposit_view',
                'member_deposit_cancel',
                'member_deposit_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
