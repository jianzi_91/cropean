<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class DepositBankPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_deposit_bank_list',
                        'title' => 'Admin Deposit Bank List',
                    ], [
                        'name'  => 'admin_deposit_bank_create',
                        'title' => 'Admin Deposit Bank Create',
                    ], [
                        'name'  => 'admin_deposit_bank_edit',
                        'title' => 'Admin Deposit Bank Edit',
                    ], [
                        'name'  => 'admin_deposit_bank_delete',
                        'title' => 'Admin Deposit Bank Delete',
                    ], /*[
                        'name'  => 'admin_member_deposit_banks_edit',
                        'title' => 'Admin Member Deposit Banks Edit',
                    ], */[
                        'name'  => 'admin_set_default_nonvip_bank',
                        'title' => 'Admin Set Default Bank',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_deposit_bank_list',
                'admin_deposit_bank_create',
                'admin_deposit_bank_edit',
                'admin_deposit_bank_delete',
                'admin_set_default_nonvip_bank'
            ],
            Role::ADMIN => [
                'admin_deposit_bank_list',
                'admin_deposit_bank_create',
                'admin_deposit_bank_edit',
                'admin_deposit_bank_delete',
                'admin_set_default_nonvip_bank'
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
