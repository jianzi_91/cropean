<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class DepositAttachmentPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_deposit_attachment_view',
                        'title' => 'Admin Deposit Attachment View',
                    ],
                ]
            ],
            'member_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_deposit_attachment_view',
                        'title' => 'Member Deposit Attachment View',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_deposit_attachment_view',
            ],
            Role::ADMIN => [
                'admin_deposit_attachment_view',
            ],
            Role::MEMBER => [
                'member_deposit_attachment_view',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
