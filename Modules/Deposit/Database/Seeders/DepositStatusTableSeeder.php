<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Deposit\Contracts\DepositStatusContract;

class DepositStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param DepositStatusContract $statusRepo
     * @return void
     */
    public function run(DepositStatusContract $statusRepo)
    {
        $statuses = [
            DepositStatusContract::PENDING,
            DepositStatusContract::APPROVED,
            DepositStatusContract::REJECTED,
            DepositStatusContract::CANCELLED,
        ];

        foreach ($statuses as $status) {
            $statusRepo->add([
                'name'      => $status,
                'is_active' => true,
            ]);
        }
    }
}
