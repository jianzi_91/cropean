<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class SecurityPasswordSettingSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'system',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'security_password',
                'setting_category_id' => SettingCategory::where('name', 'system')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'security_password')->first()->id,
                'value'      => bcrypt('testSecurityPassword'),
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
