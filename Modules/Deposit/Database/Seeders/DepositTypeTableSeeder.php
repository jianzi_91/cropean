<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Deposit\Contracts\DepositTypeContract;

class DepositTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(DepositTypeContract $depositTypeRepo)
    {
        $depositTypeRepo->add([
            'name'      => DepositTypeContract::NON_VIP,
            'is_global' => true,
            'is_active' => true,
        ]);

        $depositTypeRepo->add([
            'name'      => DepositTypeContract::VIP,
            'is_global' => false,
            'is_active' => true,
        ]);
    }
}
