<?php

namespace Modules\Deposit\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Deposit\Models\DepositSetting;

class DepositSettingSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $active = DepositSetting::where('is_active',1)->first();
        if($active)
            return true;

        $setting = [
            [
                'created_at'                => Carbon::now(),
                'updated_at'                => Carbon::now(),
                'title'                     => 'Default',
                'min'                       => 0,
                'max'                       => 0,
                'processing_fee_percentage' => 0,
                'multiple_of'               => 0,
                'is_active'                 => 1,
                'qr_filename'               => ''
            ]
        ];

        foreach ($setting as $status) {
            $result = (new DepositSetting($status))->save();
        }
    }
}
