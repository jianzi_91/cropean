<?php

namespace Modules\Deposit\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Deposit\Contracts\DepositMethodContract;

class DepositMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(DepositMethodContract $methodRepo)
    {
        $methodRepo->add([
            'deposit_type_id'      => 1,
            'name'                 => DepositMethodContract::TELEGRAPHIC_TRANSFER,
            'currency'             => 'usd_credit',
            'min'                  => 500,
            'max'                  => 10000,
            'multiple'             => 100,
            'admin_fee_percentage' => '0.01',
            'is_active'            => true,
        ]);
    }
}
