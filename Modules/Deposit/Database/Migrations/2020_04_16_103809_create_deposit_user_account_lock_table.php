<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositUserAccountLockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_account_locks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->unsignedInteger('lock_in_days')->default(90);
            $table->decimal('penalty_fee_percentage', 20, 6);
            $table->decimal('penalty_fee_amount', 40, 20);
            $table->unsignedInteger('done_by')->index();
            $table->unsignedInteger('withdrawal_penalty_days')->nullable();
            $table->boolean('is_current')->default(0);

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('done_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_lock');
    }
}
