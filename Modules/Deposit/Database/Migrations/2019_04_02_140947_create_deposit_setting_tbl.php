<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositSettingTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('title')->index();
            $table->string('title_translation');
            $table->decimal('min', 20, 6);
            $table->decimal('max', 20, 6);
            $table->decimal('processing_fee_percentage', 10, 4);
            $table->unsignedInteger('multiple_of');
            $table->boolean('is_active', 0);
            $table->string('qr_filename');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_settings');
    }
}
