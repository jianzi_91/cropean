<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveBankColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_bank_details', function (Blueprint $table) {
            $table->dropColumn('unique_id');
            $table->dropColumn('branch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_bank_details', function (Blueprint $table) {
            $table->string('unique_id');
            $table->string('branch');
        });
    }
}
