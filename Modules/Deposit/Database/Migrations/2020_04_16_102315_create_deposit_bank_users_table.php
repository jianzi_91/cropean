<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositBankUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_bank_users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('deposit_method_id');
            $table->unsignedInteger('deposit_bank_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('deposit_method_id')
                ->references('id')
                ->on('deposit_methods');

            $table->foreign('deposit_bank_id')
                ->references('id')
                ->on('deposit_banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_bank_users');
    }
}
