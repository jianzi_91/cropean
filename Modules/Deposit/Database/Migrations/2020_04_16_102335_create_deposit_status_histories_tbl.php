<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositStatusHistoriesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('deposit_status_id')->index();
            $table->unsignedInteger('deposit_id')->index();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('deposit_status_id')
                ->references('id')
                ->on('deposit_statuses');

            $table->foreign('deposit_id')
                ->references('id')
                ->on('deposits');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_status_histories');
    }
}
