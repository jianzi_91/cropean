<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_bank_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('deposit_id');
            $table->string('bank_name');
            $table->string('account_name');
            $table->string('unique_id');
            $table->string('account_number');
            $table->string('branch');
            $table->string('currency');
            $table->string('deposit_min')->nullable();
            $table->string('deposit_max')->nullable();
            $table->string('max_transaction');
            $table->time('deposit_timing_from')->nullable();
            $table->time('deposit_timing_to')->nullable();
            $table->text('additional_instructions')->nullable();

            $table->foreign('deposit_id')
                ->references('id')
                ->on('deposits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_bank_details');
    }
}
