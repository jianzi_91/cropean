<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositMethodI18ns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_method_i18ns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('deposit_method_id')->unsigned();
            $table->integer('translator_language_id')->unsigned();
            $table->text('description')->collation('utf8mb4_unicode_ci');

            // foreign key
            $table->foreign('deposit_method_id')->references('id')->on('deposit_methods');
            $table->foreign('translator_language_id')->references('id')->on('translator_languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_method_i18ns');
    }
}
