<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('deposit_type_id');
            $table->string('name')->unique();
            $table->string('name_translation');
            $table->string('currency');
            $table->unsignedDecimal('min', 40, 20);
            $table->unsignedDecimal('max', 40, 20);
            $table->unsignedDecimal('multiple', 40, 20);
            $table->unsignedDecimal('admin_fee_percentage', 40, 20);
            $table->boolean('is_active')->default(false);

            $table->foreign('deposit_type_id')
                ->references('id')
                ->on('deposit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_methods');
    }
}
