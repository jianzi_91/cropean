<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('deposit_id');
            $table->unsignedDecimal('credit_type_exchange_rate', 40, 20);
            $table->string('transaction_code');

            $table->foreign('deposit_id')
                ->references('id')
                ->on('deposits');

            $table->foreign('transaction_code')
                ->references('transaction_code')
                ->on('bank_account_statements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_transactions');
    }
}
