<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepositBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_banks', function (Blueprint $table) {
            $table->string('bank_branch')->nullable()->after('bank_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_banks', function (Blueprint $table) {
            if (Schema::hasColumn('deposit_banks', 'bank_branch')) {
                $table->dropColumn(['bank_branch']);
            }
        });
    }
}
