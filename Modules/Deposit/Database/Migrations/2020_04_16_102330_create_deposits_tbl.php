<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('deposit_method_id');
            $table->unsignedInteger('deposit_type_id');
            $table->string('reference_number')->unique();
            $table->string('currency');
            $table->unsignedDecimal('amount', 40, 20);
            $table->unsignedDecimal('total_amount', 40, 20);
            $table->string('converted_currency');
            $table->unsignedDecimal('converted_amount', 40, 20);
            $table->unsignedDecimal('exchange_rate', 40, 20);
            $table->unsignedDecimal('admin_fee_percentage', 5, 4);
            $table->unsignedDecimal('admin_fee', 40, 20);
            $table->unsignedInteger('deposit_status_id')->nullable()->index();
            $table->unsignedInteger('issue_credit_type_id')->nullable()->index();
            $table->unsignedDecimal('credit_type_exchange_rate', 40, 20);
            $table->unsignedDecimal('issued_amount', 40, 20);

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('deposit_method_id')
                ->references('id')
                ->on('deposit_methods');

            $table->foreign('deposit_type_id')
                ->references('id')
                ->on('deposit_types');

            $table->foreign('deposit_status_id')
                ->references('id')
                ->on('deposit_statuses');

            $table->foreign('issue_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
