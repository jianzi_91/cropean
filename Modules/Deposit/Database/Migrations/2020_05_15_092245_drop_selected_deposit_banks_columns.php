<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropSelectedDepositBanksColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposit_banks', function (Blueprint $table) {
            $table->dropColumn('branch');
            $table->dropColumn('unique_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposit_banks', function (Blueprint $table) {
            $table->string('unique_id')->unique();
            $table->string('branch');
        });
    }
}
