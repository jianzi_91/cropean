<?php

namespace Modules\Translation\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($locale = null)
    {
        session()->put('locale', $locale);

        $user = auth()->user();
        if ($user) {
            $user->update([
                'locale' => $locale,
            ]);
        }

        return back();
    }
}
