<?php

namespace Modules\Translation\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface TranslatorLanguageContract extends CrudContract, SlugContract, ActiveContract
{
    const ENGLISH = 'en';
    const CHINESE = 'cn';
    const ARABIC = 'ar';
}
