<?php

namespace Modules\Translation\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface TranslatorPageContract extends CrudContract, SlugContract
{
}
