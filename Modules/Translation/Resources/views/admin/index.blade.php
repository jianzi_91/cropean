@extends('templates.admin.master')
@section('title', __('a_translations.translations'))
@section('main')

@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_translations.translations')],
    ],
    'header'=>__('a_translations.translations'),
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('search', request('search'), __('a_translations.keyword'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('group_id', translator_groups_dropdown(), old('group_id'), __('a_translations.group'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('page_id', translator_pages_dropdown(), old('page_id'), __('a_translations.page'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_translations.translation list') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_translations.group') }}</th>
                <th>{{ __('a_translations.page') }}</th>
                <!-- <th>{{ __('a_translations.link') }}</th> -->
                <th>{{ __('a_translations.key') }}</th>
                @foreach ($languages as $language)
                <th class="text-uppercase" width="25%">{{ $language->code }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
        @if(count($translations))
            @foreach($translations as $key => $translation)
            <tr>
                <td>{{ __($translation->group_name) }}</td>
                <td>{{ $translation->name }}</td>
                {{--<td><a target="_blank"
                        href="{{ $translation->route_name && Route::has($translation->route_name)? route($translation->route_name): '#' }}">{{ $translation->route_name && Route::has($translation->route_name)? __('a_translations.view') : '-' }}</a>
                </td>--}}
                <td>{{ $translation->key }}</td>
                @foreach ($languages as $language)
                <td class="editable">
                    @can('admin_translation_edit')
                    <a href="#" data-name="{{ $language->id }}"
                        data-pk="{{ $localeTranslations[$language->id][$translation->translator_page_id][$translation->key]->id }}"
                        data-url="/translations/{{ $localeTranslations[$language->id][$translation->translator_page_id][$translation->key]->id }}">{{ $localeTranslations[$language->id][$translation->translator_page_id][$translation->key]->value }}</a>
                    @endcan
                </td>
                @endforeach
            </tr>
            @endforeach
        @else
            @include('templates.__fragments.components.no-table-records', [ 'span' => 5, 'text' => __('a_translations.no records') ])
        @endif
        </tbody>
        @endcomponent
    </div>
</div>
{!! $translations->render() !!}
@endsection

@push('styles')
<link type="text/css" rel="stylesheet" href="{{ asset('third-party/x-editable/bootstrap-editable.css') }}" />
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('third-party/x-editable/bootstrap-editable.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.fn.editableform.buttons ='<button type="submit" class="btn btn_dark btn-sm editable-submit w-100">'+
        '<span class="s7-plus">{!!__('a_translations.submit')!!}</span>'+
        '</button><br />'+
        '<button type="button" class="btn btn_light btn-sm editable-cancel w-100">'+
        '<span class="s7-close">{!!__('a_translations.cancel')!!}</span>'+
        '</button>';

        // submit edit
        $('.editable a').editable({
            type: 'text',
            mode:'inline',
            title: "{{ __('a_translations.enter translation') }}",
            ajaxOptions: {
                type: 'put',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            },
            success: function (response, newValue) {
                CreateNoty({
                    'text': response.message,
                    'type': 'success'
                });
            },
            error: function (response, newValue) {
                CreateNoty({
                    'text': '{{ __("a_translations.fail to update translation") }',
                    'type': 'error'
                });
            }
        });
    });

</script>
@endpush
