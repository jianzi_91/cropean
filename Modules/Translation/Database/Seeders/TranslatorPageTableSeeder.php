<?php

namespace Modules\Translation\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Models\TranslatorPage;

class TranslatorPageTableSeeder extends Seeder
{
    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorGroupContract $groupContract)
    {
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $pages = [
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_announcements',
                'name_translation'    => 's_announcements',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_bank_transaction_types',
                'name_translation'    => 's_bank_transaction_types',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_bank_credit_types',
                'name_translation'    => 's_bank_credit_types',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_support_ticket_statuses',
                'name_translation'    => 's_support_ticket_statuses',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_support_ticket_types',
                'name_translation'    => 's_support_ticket_types',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
            [
                'created_at'          => Carbon::now(),
                'updated_at'          => Carbon::now(),
                'name'                => 's_settings',
                'name_translation'    => 's_settings',
                'translator_group_id' => $this->groupRepo->findBySlug('system')->id
            ],
        ];
        TranslatorPage::insert($pages);
    }
}
