<?php

namespace Modules\Translation\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Translation\Models\TranslatorLanguage;

class TranslatorLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = Carbon::now();

        TranslatorLanguage::insert([
            [
                'code'       => 'cn',
                'name'       => '中文',
                'created_at' => $today,
                'updated_at' => $today,
                'is_active'  => true
            ],
            [
                'code'       => 'en',
                'name'       => 'English',
                'created_at' => $today,
                'updated_at' => $today,
                'is_active'  => true
            ],
        ]);
    }
}
