<?php

namespace Modules\Translation\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class TranslationPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_translation' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Translation',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_translation_list',
                        'title' => 'Admin Translation List',
                    ],
                    [
                        'name'  => 'admin_translation_edit',
                        'title' => 'Admin Edit Translation',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_translation_list',
                'admin_translation_edit',
            ],
            Role::ADMIN => [
                'admin_translation_list',
                'admin_translation_edit',
            ]
        ];

        $this->seedPermissions($permissions);
    }
}
