<?php

namespace Modules\Translation\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Translation\Models\TranslatorPage;

class UpdateTranslationPageRouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->updateRouteNameForAdminPage();
        $this->updateRouteNameForMemberPage();
    }

    private function updateRouteNameForMemberPage()
    {
        $memberPages = [
            'm_create_announcement'    => 'member.announcements.create',
            'm_edit_announcement'      => 'member.announcements.index',
            'm_announcements'          => 'member.announcements.index',
            'm_login'                  => 'member.login',
            'm_bank'                   => 'member.bank.index',
            'm_create_bank'            => 'member.bank.index',
            'm_bank_edit'              => 'member.bank.index',
            'm_my_banks'               => 'member.bank.index',
            'm_dashboard'              => 'member.dashboard',
            'm_change_password'        => 'member.password.edit',
            'm_forgot_password'        => 'member.password.forgot.index',
            'm_reset_password'         => 'member.password.reset.index',
            'm_registration_success'   => 'member.register.success',
            'm_register'               => 'member.register',
            'm_create_support_ticket'  => 'member.support.tickets.create',
            'm_view_support_ticket'    => 'member.support.tickets.index',
            'm_support_tickets'        => 'member.support.tickets.index',
            'm_network'                => 'member.sponsor.index',
            'm_my_profile'             => 'member.profile.index',
            'm_withdrawal'             => 'member.withdrawals.index',
            'm_report_goldmine'        => 'member.report.goldmine.index',
            'm_report_roi'             => 'member.report.rebate.index',
            'm_report_sponsor'         => 'member.report.sponsor.index',
            'm_sponsor'                => 'member.sponsor.index',
            'm_withdrawal_list'        => 'member.withdrawals.index',
            'm_deposit'                => 'member.deposits.index',
            'm_create_deposit'         => 'member.deposits.create',
            'm_create_deposit_success' => 'member.deposits.create'

        ];

        $this->updateRouteName($memberPages);
    }

    private function updateRouteNameForAdminPage()
    {
        $adminPages = [
            'a_announcements'                     => 'admin.announcements.index',
            'a_login'                             => 'admin.login',
            'a_bank'                              => 'admin.bank.index',
            'a_dashboard'                         => 'admin.dashboard',
            'a_country_exchange_rate'             => 'admin.country.exchange.rates.index',
            'a_exchange_rate'                     => 'admin.exchange.rates.index',
            'a_change_password'                   => 'admin.password.edit',
            'a_forgot_password'                   => 'admin.password.forgot.index',
            'a_reset_password'                    => 'admin.password.reset.index',
            'a_support_tickets'                   => 'admin.support.tickets.index',
            'a_view_support_ticket'               => 'admin.support.tickets.index',
            'a_translations'                      => 'admin.translations.index',
            'a_network'                           => 'admin.sponsor.index',
            'a_network_upline'                    => 'admin.sponsor.upline',
            'a_my_profile'                        => 'admin.profile.index',
            'a_admin_management'                  => 'admin.management.admins.index',
            'a_admin_management_adjust_credit'    => 'admin.management.admins.index',
            'a_admin_management_transfer_credit'  => 'admin.management.admins.index',
            'a_admin_management_profile'          => 'admin.management.admins.index',
            'a_admin_management_change_password'  => 'admin.management.admins.index',
            'a_admin_permissions'                 => 'admin.management.admins.index',
            'a_admin_management_menu'             => 'admin.management.admins.index',
            'a_member_management'                 => 'admin.management.members.index',
            'a_member_management_menu'            => 'admin.management.members.index',
            'a_member_management_profile'         => 'admin.management.members.index',
            'a_member_management_qr_code'         => 'admin.management.members.index',
            'a_member_management_change_network'  => 'admin.management.members.index',
            'a_member_management_adjust_credit'   => 'admin.management.members.index',
            'a_member_management_transfer_credit' => 'admin.management.members.index',
            'a_member_management_change_password' => 'admin.management.members.index',
            'a_member_management_bank'            => 'admin.management.members.index',
            'a_member_management_login_as'        => 'admin.management.members.index',
            'a_member_permissions'                => 'admin.management.members.index',
            'a_create_announcement'               => 'admin.announcements.create',
            'a_edit_announcement'                 => 'admin.announcements.index',
            'a_goldmine_bonus'                    => 'admin.report.goldmine.index',
            'a_sponsor_bonus'                     => 'admin.report.sponsor.index',
            'a_daily_summary'                     => 'admin.report.summary.index',
            'a_view_deposit'                      => 'admin.deposits.index',
            'a_deposits'                          => 'admin.deposits.index',
            'a_deposit_settings'                  => 'admin.deposits.settings.index ',
            'a_roles'                             => 'admin.roles.index',
            'a_role_permissions'                  => 'admin.roles.index',
            'a_create_role'                       => 'admin.roles.index',
            'a_edit_role'                         => 'admin.roles.index',
            'a_roi_bonus'                         => 'admin.report.rebate.index',
            'a_network_sponsor_tree'              => 'admin.sponsor.index',
            'a_sponsor_tree_upline'               => 'admin.sponsor.upline',
            'a_view_withdrawal'                   => 'admin.withdrawals.index',
            'a_withdrawals'                       => 'admin.withdrawals.index',
            'a_withdrawal_settings'               => 'admin.withdrawals.index',

        ];

        $this->updateRouteName($adminPages);
    }

    private function updateRouteName($pages)
    {
        foreach ($pages as $name => $page) {
            if (strlen($page) > 0) {
                TranslatorPage::where('name', $name)->update(
                    [
                        'route_name' => $page
                    ]
                );
            }
        }
    }
}
