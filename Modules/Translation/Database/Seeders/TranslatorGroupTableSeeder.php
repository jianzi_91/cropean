<?php

namespace Modules\Translation\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Translation\Models\TranslatorGroup;
use Modules\Translation\Models\TranslatorPage;

class TranslatorGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $pages = [
            [
                'created_at'       => Carbon::now(),
                'updated_at'       => Carbon::now(),
                'name'             => 's_translator_pages',
                'name_translation' => 's_translator_pages',
            ]
        ];
        TranslatorPage::insert($pages);

        $groups = [
            [
                'name'   => 'website',
                'prefix' => 'w',
            ],
            [
                'name'   => 'system',
                'prefix' => 's',
            ],
            [
                'name'   => 'mobile',
                'prefix' => 'o',
            ],
            [
                'name'   => 'admin',
                'prefix' => 'a',
            ],
            [
                'name'   => 'member',
                'prefix' => 'm',
            ]
        ];

        foreach ($groups as $group) {
            $newGroup = new TranslatorGroup($group);
            $newGroup->save();
        }
    }
}
