<?php

namespace Modules\Translation\Repositories;

use Illuminate\Foundation\Application;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Country\Models\Country;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Modules\Translation\Models\TranslatorLanguage;

class TranslatorLanguageRepository extends Repository implements TranslatorLanguageContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor
     *
     * @param Application $app
     * @param Country $model
     */
    public function __construct(TranslatorLanguage $model)
    {
        $this->slug = 'code';
        parent::__construct($model);
    }
}
