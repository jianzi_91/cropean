<?php

namespace Modules\Translation\Repositories;

use Illuminate\Foundation\Application;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Country\Models\Country;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Models\TranslatorGroup;

class TranslatorGroupRepository extends Repository implements TranslatorGroupContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor
     *
     * @param Application $app
     * @param Country $model
     */
    public function __construct(TranslatorGroup $model)
    {
        parent::__construct($model);
    }
}
