<?php

Route::group(['middleware' => ['web']], function () {
    Route::any('/lang/{locale}', 'LanguagesController@index');

    Route::group(['domain' => config('core.admin_url'), 'middleware' => ['auth']], function () {
        Route::resource('translations', 'Admin\TranslationsController', ['names' => 'admin.translations', 'only' => ['index', 'update']]);
    });
});
