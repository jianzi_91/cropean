<?php

namespace Modules\Translation\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Translation\Models\TranslatorTranslation;

class ImportTranslationFile implements ToCollection, WithHeadingRow
{
    public function collection(Collection $collections)
    {
        $cn = 1;
        $en = 2;

        foreach ($collections as $row) {
            if ($row->filter()->isNotEmpty() && isset($row['translator_page_id']) && isset($row['key'])) {
                DB::beginTransaction();
                try {
                    \Log::info(json_encode($row, true));

                    if (isset($row['en'])) {
                        //english
                        TranslatorTranslation::where('translator_page_id', $row['translator_page_id'])
                            ->where('key', $row['key'])
                            ->where('translator_language_id', $en)
                            ->update(['value' => $row['en']]);
                    }

                    if(isset($row['cn'])){
                        //chinese
                        TranslatorTranslation::where('translator_page_id', $row['translator_page_id'])
                            ->where('key', $row['key'])
                            ->where('translator_language_id', $cn)
                            ->update(['value' => $row['cn']]);
                    }

                    DB::commit();
                } catch (\Exception $exception) {
                    DB::rollBack();
                    \Log::error($exception->getMessage());
                }
            }
        }
    }

    public function headingRow(): int
    {
        return 1;
    }
}
