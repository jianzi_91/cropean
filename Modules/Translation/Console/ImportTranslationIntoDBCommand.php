<?php

namespace Modules\Translation\Console;

use Illuminate\Console\Command;
use Illuminate\Contracts\Console\Kernel;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Translation\Imports\ImportTranslationFile;
use Symfony\Component\Console\Input\InputArgument;

class ImportTranslationIntoDBCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:translation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translation.';

    protected $artisan;

    /**
     * Create a new command instance.
     * @param Kernel $artisan
     * @return void
     */
    public function __construct(Kernel $artisan)
    {
        $this->artisan = $artisan;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Importing translation ...');
        $path = storage_path('translation/') . $this->argument('xlsx');
        if ($this->argument('reader_type') == 1) {
            Excel::import(new ImportTranslationFile(), $path, null, \Maatwebsite\Excel\Excel::XLSX);
        } elseif ($this->argument('reader_type') == 2) {
            Excel::import(new ImportTranslationFile(), $path, null, \Maatwebsite\Excel\Excel::XLS);
        } else {
            Excel::import(new ImportTranslationFile(), $path, null, \Maatwebsite\Excel\Excel::CSV);
        }

        $this->artisan->call('translation:generate_files');

        $this->line('Imported translation list');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['xlsx', InputArgument::REQUIRED, 'Xlsx filename ending .xlsx'],
            ['reader_type', InputArgument::REQUIRED, 'readerType']
        ];
    }
}
