<?php

namespace Modules\Translation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Modules\Translation\Traits\Translatable;

class TranslatorGroup extends Model
{
    use SoftDeletes, Translatable, HasDropDown;

    protected $fillable = [
        'name',
        'name_translation'
    ];

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * This model's relation to translation_groups
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(TranslatorTranslation::class, 'translator_group_id', 'id');
    }

    /**
     * Listening to events
     */
    protected static function boot()
    {
        parent::boot();
    }
}
