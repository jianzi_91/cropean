<?php

namespace Modules\Translation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class TranslatorLanguage extends Model
{
    use SoftDeletes, HasActiveScope, HasDropDown;

    protected $fillable = [
        'code',
        'name'
    ];

    /**
     * This model's relation to translations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(TranslatorTranslation::class, 'translator_language_id', 'id');
    }

    /**
     * Listening to events
     * @throws Plus65Exception
     */
    protected static function boot()
    {
        parent::boot();
    }
}
