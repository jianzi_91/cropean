@extends('templates.admin.master')
@section('title', __('a_page_title.blockchain deposits settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_blockchain_settings.blockchain settings')],
    ],
    'header'=>__('a_blockchain_settings.blockchain settings'),
])

<div class="row">
    @foreach($categories as $category)
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title">{{ $category->name }}</h4>
                    {{ Form::open(['method'=>'post', 'route' => 'admin.setting.blockchain-settings.store', 'id'=>'blockchain-deposit', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    @foreach ($category->settings as $setting)
                        @if(strpos($setting->rawname, 'percentage')) 
                        {{ Form::formNumber($setting->rawname, bcmul($setting->value->value, 100, 2), $setting->name) }}
                        @else
                        {{ Form::formNumber($setting->rawname, $setting->value->value, $setting->name) }}
                        @endif
                    @endforeach
                    <div class="d-flex justify-content-end">
                    {{ Form::formButtonPrimary('btn_submit', __('a_blockchain_settings.save')) }}
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection