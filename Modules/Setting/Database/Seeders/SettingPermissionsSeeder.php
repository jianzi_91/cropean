<?php

namespace Modules\Setting\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class SettingPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_setting' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Setting',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_setting_list',
                        'title' => 'Admin Setting List',
                    ],
                    [
                        'name'  => 'admin_setting_edit',
                        'title' => 'Admin Edit Setting',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_setting_list',
                'admin_setting_edit',
            ],
            Role::ADMIN => [
                'admin_setting_list',
                'admin_setting_edit',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
