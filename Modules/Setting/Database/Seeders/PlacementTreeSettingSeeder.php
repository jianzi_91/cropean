<?php

namespace Modules\Setting\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class PlacementTreeSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'placement_tree',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'placement_tree_lock',
                'setting_category_id' => SettingCategory::where('name', 'placement_tree')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'placement_tree_lock')->first()->id,
                'value'      => '1',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
