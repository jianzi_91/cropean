<?php

namespace Modules\Setting\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class PenaltySettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'penalty_fee',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'penalty_duration',
                'setting_category_id' => SettingCategory::where('name', 'penalty_fee')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
            [
                'name'                => 'penalty_percentage',
                'setting_category_id' => SettingCategory::where('name', 'penalty_fee')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'penalty_duration')->first()->id,
                'value'      => '90',
            ],
            [
                'setting_id' => Setting::where('name', 'penalty_percentage')->first()->id,
                'value'      => '{"30": 0.1, "90": 0.05}',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
