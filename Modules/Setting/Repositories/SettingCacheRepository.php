<?php

namespace Modules\Setting\Repositories;

use Illuminate\Contracts\Cache\Store;
use Illuminate\Foundation\Application;
use Plus65\Base\Repositories\Repository;
use Modules\Setting\Contracts\SettingCacheContract;

class SettingCacheRepository extends Repository implements SettingCacheContract
{
    /**
     * The cache
     * @var unknown
     */
    protected $cache;

    /**
     * Constructor
     * @param Store $cache
     */
    public function __construct(Application $app)
    {
        $this->cache = $app['cache'];
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingCacheContract::getCache()
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingCacheContract::clear()
     */
    public function clear($setting = null)
    {
        $this->cache->forget($setting);

        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Doctrine\Common\Cache\FlushableCache::flushAll()
     */
    public function flushAll()
    {
        $this->cache->flush();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingCacheContract::get()
     */
    public function get($setting)
    {
        return $this->cache->get($setting);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingCacheContract::set()
     */
    public function set($setting, $value)
    {
        $this->cache->forever($setting, $value);

        return $this;
    }

    /**
     * Get the cache tag.
     *
     * @return string
     */
    protected function tag()
    {
        return config('setting.cache.prefix');
    }
}
