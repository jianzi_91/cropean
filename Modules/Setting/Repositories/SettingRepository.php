<?php

namespace Modules\Setting\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Setting\Contracts\SettingContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingValue;

class SettingRepository extends Repository implements SettingContract
{
    use HasCrud, HasSlug;

    /**
     * The value model
     * @var unknown
     */
    protected $valueModel;

    /**
     * Class constructor
     * @param Setting $model
     * @param SettingValue $value
     */
    public function __construct(Setting $model, SettingValue $value)
    {
        $this->valueModel = $value;
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::addValue()
     */
    public function addValue($id, $value)
    {
        $model             = new $this->valueModel;
        $model->value      = $value;
        $model->setting_id = $id;
        return $model->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::addValuesBySlug()
     */
    public function addValuesBySlug($slug, $value)
    {
        $setting = $this->model->where('name', $slug)->firstOrFail();

        $model             = new $this->valueModel;
        $model->value      = $value;
        $model->setting_id = $setting->id;
        return $model->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::getValues()
     */
    public function getValues($settingId, $select = ['*'])
    {
        return $this->valueModel->where('setting_id', $settingId)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::getValue()
     */
    public function getValue($settingId, $select = ['*'])
    {
        return $this->valueModel->where('setting_id', $settingId)->first($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::getValuesBySlug()
     */
    public function getValuesBySlug($slug, $select = ['*'])
    {
        return $this->valueModel->whereHas('setting', function ($query) use ($slug) {
            $query->where('name', $slug);
        })->get($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::getValueBySlug()
     */
    public function getValueBySlug($slug, $select = ['*'])
    {
        return $this->valueModel->whereHas('setting', function ($query) use ($slug) {
            $query->where('name', $slug);
        })->first($select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::findValue()
     */
    public function findValue($id, $select = ['*'])
    {
        return $this->valueModel->find($id, $select);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Plus65\Contracts\SlugContract::deleteBySlug()
     */
    public function deleteBySlug($slug)
    {
        return $this->model->where('name', $slug)->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Plus65\Contracts\CrudContract::delete()
     */
    public function delete($ids)
    {
        // destroy the values
        $this->valueModel->whereIn('setting_id', (array) $ids)->delete();

        return $this->model->destroy($ids);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Plus65\Contracts\CrudContract::deleteAll()
     */
    public function deleteAll()
    {
        // destroy the values
        $this->valueModel->newQuery()->delete();

        return $this->model->newQuery()->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::deleteValues()
     */
    public function deleteValues($id)
    {
        $this->valueModel->where('setting_id', (array) $id)->delete();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Setting\Contracts\SettingContract::deleteValuesBySlug()
     */
    public function deleteValuesBySlug($slug)
    {
        return $this->valueModel->whereHas('setting', function ($query) use ($slug) {
            $query->where('name', $slug);
        })->delete();
    }
}
