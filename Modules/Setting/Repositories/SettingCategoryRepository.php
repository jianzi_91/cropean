<?php

namespace Modules\Setting\Repositories;

use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Setting\Contracts\SettingCategoryContract;
use Modules\Setting\Models\SettingCategory;

class SettingCategoryRepository extends Repository implements SettingCategoryContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor
     * @param SettingCategory $model
     */
    public function __construct(SettingCategory $model)
    {
        parent::__construct($model);
    }
}
