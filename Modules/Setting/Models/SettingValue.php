<?php

namespace Modules\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class SettingValue extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty  = true;

    protected $fillable = [
        'setting_id',
        'value',
    ];

    /**
     * This model's relation to setting
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function setting()
    {
        return $this->belongsTo(Setting::class, 'setting_id');
    }
}
