<?php

namespace Modules\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Repositories\Concerns\HasActive;
use Modules\Translation\Traits\Translatable;
use Spatie\Activitylog\Traits\LogsActivity;

class SettingCategory extends Model
{
    use Translatable, SoftDeletes, HasActive, LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty  = true;

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    protected $fillable = [
        'name',
        'name_translation',
        'is_active'
    ];

    /**
     * This model's relation to settings
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings()
    {
        return $this->hasMany(Setting::class, 'setting_category_id');
    }
}
