<?php

namespace Modules\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Repositories\Concerns\HasActive;
use Spatie\Activitylog\Traits\LogsActivity;

class Setting extends Model
{
    use Translatable, HasActive, SoftDeletes, LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty  = true;

    /**
     * Translatable columns
     * @var array
     */
    protected $translatableAttributes = ['name'];

    protected $fillable = [
        'name',
        'name_translation',
        'setting_category_id',
        'is_hidden',
        'is_active'
    ];

    /**
     * Cascade delete relations
     * @var array
     */
    protected $softCascade = ['values'];

    /**
     * This model's relation to values
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(SettingValue::class, 'setting_id');
    }

    /**
     * This model's relation to values
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function value()
    {
        return $this->hasOne(SettingValue::class, 'setting_id');
    }

    /**
     * This model's relation to category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(SettingCategory::class, 'setting_category_id');
    }
}
