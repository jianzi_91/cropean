<?php

Route::group(['middleware' => 'web'], function () {
    Route::group(['domain' => config('core.admin_url'), 'middleware' => ['auth']], function () {
        Route::resource('settings', 'Admin\SettingController', ['as' => 'admin.setting'])->only(['index', 'store']);
        Route::resource('blockchain-settings', 'Admin\Blockchain\SettingController', ['as' => 'admin.setting'])->only(['index', 'store']);
    });
});
