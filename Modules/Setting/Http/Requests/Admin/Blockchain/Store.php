<?php

namespace Modules\Setting\Http\Requests\Admin\Blockchain;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*_redirection_amount' => [
                'required',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
            '*_confirmation' => 'required|gte:0|integer',
            '*_max'          => [
                'required',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
            '*_multiple' => [
                'required',
                'gte:0',
                'integer',
            ],
            '*_admin_fee_percentage' => [
                'required',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
            'withdrawal_usdt_erc20_min' => [
                'sometimes',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
            'withdrawal_usdc_min' => [
                'sometimes',
                'gte:0',
                new MaxDecimalPlaces(2),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
}
