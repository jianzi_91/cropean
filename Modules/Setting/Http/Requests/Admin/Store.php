<?php

namespace Modules\Setting\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\NotZero;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Commission setting
            'commission_sponsor_percentage'  => 'required|regex:/^[0-9]+(\.[0-9]{1,2})?$/',
            'commission_goldmine_max_level'  => 'required|numeric|min:0',
            'commission_goldmine_percentage' => 'required|required|regex:/^[0-9]+(\.[0-9]{1,2})?$/'
                    // new NotZero(),
                ,

            // Rebate setting
            'rebate_reward'       => 'required|numeric|min:0',
            'rebate_lapse_reward' => 'required|integer|min:0',
            'rebate_reward_days'  => 'required|integer|min:0',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'commission_sponsor_percentage.regex'  => __('messages.only accept up to 2 decimal points'),
            'commission_goldmine_percentage.regex' => __('messages.only accept up to 2 decimal points'),
        ];
    }

    public function attributes()
    {
        return [
            'commission_sponsor_percentage'  => __('a_setting_others.commission sponsor percentage'),
            'commission_goldmine_max_level'  => __('a_setting_others.commission goldmine max level'),
            'commission_goldmine_percentage' => __('a_setting_others.commission goldmine percentage'),
            'rebate_reward'                  => __('a_setting_others.rebate reward'),
            'rebate_lapse_reward'            => __('a_setting_others.rebate lapse reward'),
            'rebate_reward_days'             => __('a_setting_others.rebate reward days'),
            'sponsor_tree_lock'              => __('a_setting_others.sponsor tree lock'),
        ];
    }
}
