<?php

namespace Modules\Setting\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Contracts\SettingCacheContract;
use Modules\Setting\Contracts\SettingCategoryContract;
use Modules\Setting\Contracts\SettingContract;
use Modules\Setting\Http\Requests\Admin\Store;
use Modules\Setting\Repositories\SettingCacheRepository;

class SettingController extends Controller
{
    /**
     * The setting category repository
     * @var unknown
     */
    protected $category;

    /**
     * The setting repository
     * @var unknown
     */
    protected $setting;
    /** @var SettingCacheRepository $settingCacheContract */
    protected $settingCacheContract;

    /**
     * Class constructor
     * @param SettingContract $setting
     */
    public function __construct(SettingCategoryContract $category, SettingContract $setting, SettingCacheContract $settingCacheContract)
    {
        $this->middleware('permission:admin_setting_list')->only('index');
        $this->middleware('permission:admin_setting_edit')->only('store');

        $this->category             = $category;
        $this->setting              = $setting;
        $this->settingCacheContract = $settingCacheContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = $this->category->allActive([
            'settings' => function ($query) {
                $query->where('is_active', 1);
                $query->where('is_hidden', 0);
            },
            'settings.value'
        ]);
        $categories = $categories->filter(function ($value, $key) {
            if ($value->settings->isEmpty()) {
                return false;
            }
            return true;
        });
        return view('setting::admin.index')->with(compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $settings = $this->setting->getModel()->where('is_active', 1)->get();
        foreach ($settings as $setting) {
            if ($request->has($setting->rawname)) {
                $this->setting->deleteValuesBySlug($setting->rawname);
                $value = $request->get($setting->rawname);
                $this->setting->addValuesBySlug($setting->rawname, $value);
                $this->settingCacheContract->clear($setting->rawname);
            }
        }
        return back()->with('success', __('a_setting_others.setting updated successfully'));
    }
}
