<?php

if (!function_exists('setting')) {
    /**
     * Get a setting value based on a specified key.
     *
     * @param unknown     $key
     * @param string|bool $default
     *
     * @return bool|unknown
     */
    function setting($key, $default = false)
    {
        try {
            $cacheEnabled = config('setting.cache.enabled');
            $cache        = resolve(Modules\Setting\Contracts\SettingCacheContract::class);
            if ($cacheEnabled) {
                if ($value = $cache->get($key)) {
                    return $value;
                }
            }

            $setting = resolve(Modules\Setting\Contracts\SettingContract::class);
            $value   = $setting->getValueBySlug($key);
            if ($value) {
                $cache->set($key, $value->value);
                return $value->value;
            }

            return $default;
        } catch (Exception $exception) {
            return $default;
        }
    }
}
