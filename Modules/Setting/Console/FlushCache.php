<?php

namespace Modules\Setting\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Modules\Setting\Contracts\SettingCacheContract;

class FlushCache extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'setting:flush {setting?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush setting cache.';

    /**
     * The cache
     * @var unknown
     */
    protected $cache;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SettingCacheContract $cache)
    {
        $this->cache = $cache;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $setting = $this->argument('setting');
        $setting ? $this->cache->clear($setting) : $this->cache->flushAll();
        Redis::flushAll();
        $this->info('Cache flushed...');
    }
}
