<?php

namespace Modules\Setting\Contracts;

use Doctrine\Common\Cache\FlushableCache;

interface SettingCacheContract extends FlushableCache
{
    /**
     * Get the cache instance.
     *
     * @return \Illuminate\Contracts\Cache\Store
     */
    public function getCache();

    /**
     * Clear the cache.
     *
     * @param unknown $setting
     * @return $this
     */
    public function clear($setting);

    /**
     * Get setting value
     *
     * @param unknown $setting
     * @return string
     */
    public function get($setting);

    /**
     * Set setting value
     * @param unknown $setting
     * @param unknown $value
     */
    public function set($setting, $value);
}
