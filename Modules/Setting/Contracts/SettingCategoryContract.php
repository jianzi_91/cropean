<?php

namespace Modules\Setting\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface SettingCategoryContract extends CrudContract, SlugContract, ActiveContract
{
}
