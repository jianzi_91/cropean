<?php

namespace Modules\Setting\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface SettingContract extends CrudContract, SlugContract
{
    /**
     * Add value
     * @param unknown $id
     * @param unknown $value
     * @return boolean
     */
    public function addValue($id, $value);

    /**
     * Add value by slug
     * @param unknown $slug
     * @param unknown $value
     * @return boolean
     */
    public function addValuesBySlug($slug, $value);

    /**
     * Find option value
     * @param unknown $id
     * @param array $select
     * @return SettingValue
     */
    public function findValue($id, $select = ['*']);

    /**
     * Get Setting values
     * @param unknown $settingId
     * @param array $select
     * @return Collection
     */
    public function getValues($settingId, $select = ['*']);

    /**
     * Get Setting values
     * @param unknown $settingId
     * @param array $select
     * @return Model
     */
    public function getValue($settingId, $select = ['*']);

    /**
     * Get Setting values by slug
     * @param unknown $slug
     * @param array $select
     * @return Collection
     */
    public function getValuesBySlug($slug, $select = ['*']);

    /**
     * Get Setting values by slug
     * @param unknown $slug
     * @param array $select
     * @return Model
     */
    public function getValueBySlug($slug, $select = ['*']);

    /**
     * Delete setting values
     * @param unknown $id
     * @return integer
     */
    public function deleteValues($id);

    /**
     * Delete setting values by slug
     * @param unknown $slug
     * @return integer
     */
    public function deleteValuesBySlug($slug);
}
