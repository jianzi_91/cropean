<?php

return [
    'name' => 'Setting',

    /*
     |--------------------------------------------------------------------------
     | Repositories and Contract bindings
     |
     |--------------------------------------------------------------------------
     */
    'bindings' => [
        'Modules\Setting\Contracts\SettingCategoryContract' => 'Modules\Setting\Repositories\SettingCategoryRepository',
        'Modules\Setting\Contracts\SettingContract'         => 'Modules\Setting\Repositories\SettingRepository',
        'Modules\Setting\Contracts\SettingCacheContract'    => 'Modules\Setting\Repositories\SettingCacheRepository',
    ],

    /*
     |--------------------------------------------------------------------------
     | Cache settings
     |
     |--------------------------------------------------------------------------
     */
    'cache' => [
        'enabled' => true,
        'prefix'  => 'settings',
    ],
];
