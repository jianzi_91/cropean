<?php

namespace Modules\Withdrawal\Repositories;

use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Traits\ValidateCredit;
use Modules\ExchangeRate\Contracts\CountryExchangeRateContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Modules\Withdrawal\Contracts\WithdrawalContract;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Models\WithdrawalTransaction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class WithdrawalRepository extends Repository implements WithdrawalContract
{
    use ValidateCredit, HasCrud, HasSlug;

    /**
     * The withdrawal transaction model
     *
     * @var unknown
     */
    protected $withdrawalTransactionModel;

    /**
     * The withdrawal status contract
     *
     * @var unknown
     */
    protected $withdrawalStatus;

    /**
     * The withdrawal type contract
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * The exchange rate contract
     *
     * @var unknown
     */
    protected $exchangeRateRepository;

    /**
     * The country exchange rate contract
     *
     * @var unknown
     */
    protected $countryExchangeRateRepository;

    /**
     * The user bank contract
     *
     * @var unknown
     */
    protected $userBankRepository;

    /**
     * The bank account contract
     *
     * @var unknown
     */
    protected $bankAccountRepository;

    /**
     * The bank account credit
     *
     * @var unknown
     */
    protected $bankAccountCreditRepository;

    /**
     * Class constructor
     *
     * @param Withdrawal $model
     * @param WithdrawalTransaction $withdrawalTransactionModel
     * @param WithdrawalTypeContract $withdrawalTypeContract
     * @param ExchangeRateContract $exchangeRateContract
     * @param CountryExchangeRateContract $countryExchangeRateContract
     * @param WithdrawalBankContract $userBankContract
     * @param BankAccountContract $bankAccountContract
     * @param BankAccountCreditContract $BankAccountCreditContract
     * @param WithdrawalStatusContract $withdrawalStatus
     */
    public function __construct(
        Withdrawal $model,
        WithdrawalTransaction $withdrawalTransactionModel,
        WithdrawalTypeContract $withdrawalTypeContract,
        ExchangeRateContract $exchangeRateContract,
        CountryExchangeRateContract $countryExchangeRateContract,
        WithdrawalBankContract $userBankContract,
        BankAccountContract $bankAccountContract,
        BankAccountCreditContract $BankAccountCreditContract,
        WithdrawalStatusContract $withdrawalStatus
    ) {
        $this->withdrawalTransactionModel    = $withdrawalTransactionModel;
        $this->withdrawalTypeRepository      = $withdrawalTypeContract;
        $this->exchangeRateRepository        = $exchangeRateContract;
        $this->countryExchangeRateRepository = $countryExchangeRateContract;
        $this->userBankRepository            = $userBankContract;
        $this->bankAccountRepository         = $bankAccountContract;
        $this->bankAccountCreditRepository   = $BankAccountCreditContract;
        $this->withdrawalStatus              = $withdrawalStatus;
        $this->slug                          = 'request_id';

        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::request()
     */
    public function request($withdrawalTypeId, $userBankId, $amount, $creditType, $remarks = null)
    {
        // Get withdrawal type
        $withdrawalType = $this->withdrawalTypeRepository->find($withdrawalTypeId);
        if (!$withdrawalType) {
            throw new \Exception('Withdrawal type not found');
        }

        // Get user bank
        $userBank = $this->userBankRepository->find($userBankId);
        if (!$userBank) {
            throw new \Exception('Bank not found');
        }

        // Get default withdrawal status
        $pendingStatus = $this->withdrawalStatus->findBySlug($this->withdrawalStatus::PENDING);

        // Get credit exchange rate
        $creditExchangeRate = $this->getCreditExchangeRate($creditType);

        // Get bank country exchange rate
        $exchangeRate = $this->getExchangeRate($userBank->country_id);

        // Generate request id
        $config    = config('withdrawal.request_id');
        $requestId = (new $config['generator'])->generate($config['prefix'], $config['length']);

        /*
         * member key in cash credit amount
         * - convert cash credit amount to base amount [usd]
         * - calculate admin fee in usd
         * - calculate total withdrawal in usd
         * - assign converted amount
         * - converted to destination currency
         * - change converted base amount to cash credit
         * - change admin fee in usd to cash credit
         */
        $withdrawalFeePercentage = $withdrawalType->fee_percentage;
        //convert requested amount into usd from cash credit
        $requestedBaseAmount = $this->convertAmount($amount, $creditExchangeRate->sell_rate);
        //calculate admin fee in usd
        $withdrawalFeeInBaseAmount = $this->calculateAdminFee($requestedBaseAmount, $withdrawalFeePercentage);
        $baseTotalWithdrawalAmount = $this->calculateAmount($requestedBaseAmount, $withdrawalFeeInBaseAmount, $withdrawalType->fee_type);
        $convertedBaseAmount       = $withdrawalType->fee_type == 'inclusive' ? $baseTotalWithdrawalAmount : $requestedBaseAmount;
        //total amount will deduct from cash credit
        $receivableAmount            = $this->convertAmount($convertedBaseAmount, $exchangeRate->buy_rate, false);
        $creditTotalWithdrawalAmount = $this->convertAmount($convertedBaseAmount, $creditExchangeRate->buy_rate, false);
        //withdrawal fee in cash credit
        $withdrawalFeeAmount = $this->convertAmount($withdrawalFeeInBaseAmount, $creditExchangeRate->buy_rate, false);

        $withdrawal = $this->addWithdrawal(
            $requestId,
            $userBank->user_id,
            $amount,
            $requestedBaseAmount,
            $receivableAmount,
            $convertedBaseAmount,
            $exchangeRate->currency,
            $exchangeRate->sell_rate,
            $withdrawalTypeId,
            $withdrawalFeePercentage,
            $withdrawalFeeAmount,
            $pendingStatus->id,
            $withdrawalFeeInBaseAmount,
            $creditExchangeRate->sell_rate,
            $creditExchangeRate->buy_rate,
            $userBank->id
        );

        if ($withdrawal) {
            if (!$this->withdrawalStatus->changeHistory($withdrawal, $pendingStatus, ['updated_by' => $userBank->user_id])) {
                throw new \Exception('Failed saving of withdrawal status history');
            }

            $withdrawalTxnId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_REQUEST);
            $creditTypeId    = $this->validateCreditType($creditType);
            $withdrawTxnCode = $this->deductCredit($withdrawal, $creditType, $withdrawalTxnId, $remarks);

            $this->addWithdrawalTransaction($withdrawal, $creditTypeId, $withdrawTxnCode, $withdrawalTxnId);

            if ($withdrawalFeeAmount > 0 && $withdrawalType->fee_type == 'exclusive') {
                $this->addFee($withdrawal, $withdrawalFeeAmount, $creditType);
            }
        }

        return $withdrawal;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::cancel()
     */
    public function cancel($withdrawalId, $remarks = null)
    {
        $withdrawal = $this->model->findOrFail($withdrawalId);

        // Refund credits
        $refundTxnId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_REQUEST);
        $refundTxn   = $this->withdrawalTransactionModel->where('bank_transaction_type_id', $refundTxnId)
                                                                ->where('withdrawal_id', $withdrawal->id)
                                                                ->firstOrFail();

        $refundTxnTypeId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_CANCEL);
        $refundTxnCode   = $this->bankAccountCreditRepository->add($refundTxn->bank_credit_type_id, $withdrawal->amount, $withdrawal->user_id, $refundTxnTypeId, $remarks);

        if (!$refundTxnCode) {
            throw new \Exception('Can\'t add withdrawal amount credits');
        }

        $transaction                           = $this->withdrawalTransactionModel->newInstance();
        $transaction->amount                   = $withdrawal->amount;
        $transaction->transaction_code         = $refundTxnCode;
        $transaction->withdrawal_id            = $withdrawal->id;
        $transaction->bank_credit_type_id      = $refundTxn->bank_credit_type_id;
        $transaction->bank_transaction_type_id = $refundTxnTypeId;

        if (!$transaction->save()) {
            throw new \Exception('Fail saving the withdrawal cancel transaction');
        }

        $cancelledStatus = $this->withdrawalStatus->getModel()->where('name', $this->withdrawalStatus::CANCELLED)->firstOrFail();
        if (!$this->withdrawalStatus->changeHistory($withdrawal, $cancelledStatus, ['updated_by' => $withdrawal->user_id, 'remarks' => $remarks])) {
            throw new \Exception('Fail updating status of withdrawal as cancelled');
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::reject()
     */
    public function reject($withdrawalId, $remarks = null)
    {
        $withdrawal = $this->model->findOrFail($withdrawalId);

        // Refund credits
        $refundTxnId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_REQUEST);
        $refundTxn   = $this->withdrawalTransactionModel->where('bank_transaction_type_id', $refundTxnId)
                            ->where('withdrawal_id', $withdrawal->id)
                            ->firstOrFail();

        $refundTxnTypeId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_REJECTED);
        $refundTxnCode   = $this->bankAccountCreditRepository->add($refundTxn->bank_credit_type_id, $withdrawal->amount, $withdrawal->user_id, $refundTxnTypeId, $remarks);

        if (!$refundTxnCode) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        $transaction                           = $this->withdrawalTransactionModel->newInstance();
        $transaction->amount                   = $withdrawal->amount;
        $transaction->transaction_code         = $refundTxnCode;
        $transaction->withdrawal_id            = $withdrawal->id;
        $transaction->bank_credit_type_id      = $refundTxn->bank_credit_type_id;
        $transaction->bank_transaction_type_id = $refundTxnTypeId;
        if (!$transaction->save()) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        $cancelledStatus = $this->withdrawalStatus->getModel()->where('name', $this->withdrawalStatus::REJECTED)->firstOrFail();
        if (!$this->withdrawalStatus->changeHistory($withdrawal, $cancelledStatus, ['updated_by' => $withdrawal->user_id, 'remarks' => $remarks])) {
            throw new \Exception('Failed updating status of withdrawal as rejected');
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::refund()
     */
    public function refund($withdrawalId, $remarks = null, $transactionType = 'withdrawal_reject')
    {
        $withdrawal = $this->model->findOrFail($withdrawalId);

        $withdrawalTxnId = $this->validateTransactionType(BankTransactionTypeContract::WITHDRAWAL_REQUEST);
        $withdrawalTxn   = $this->withdrawalTransactionModel->where('bank_transaction_type_id', $withdrawalTxnId)
                            ->where('withdrawal_id', $withdrawal->id)
                            ->firstOrFail();

        $refundTxnTypeId  = $this->validateTransactionType($transactionType);
        $refundFeeTxnCode = $this->bankAccountCreditRepository->add($withdrawalTxn->bank_credit_type_id, $withdrawalTxn->amount, $withdrawal->user_id, $refundTxnTypeId);

        if (!$refundFeeTxnCode) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        $transaction                           = $this->withdrawalTransactionModel->newInstance();
        $transaction->amount                   = $withdrawalTxn->amount;
        $transaction->transaction_code         = $refundFeeTxnCode;
        $transaction->withdrawal_id            = $withdrawal->id;
        $transaction->bank_credit_type_id      = $withdrawalTxn->bank_credit_type_id;
        $transaction->bank_transaction_type_id = $refundTxnTypeId;

        return $transaction->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::addFee()
     */
    public function addFee($withdrawal, $amount, $creditType, $remarks = null, $transactionType = 'withdrawal_fee')
    {
        $feeTxnId     = $this->validateTransactionType($transactionType);
        $creditTypeId = $this->validateCreditType($creditType);
        $feeTxnCode   = $this->bankAccountCreditRepository->deduct($creditType, $amount, $withdrawal->user_id, $feeTxnId, $remarks);
        if (!$feeTxnCode) {
            throw new \Exception('Can\'t deduct withdrawal fee credits');
        }

        $transactionFee                           = new $this->withdrawalTransactionModel;
        $transactionFee->amount                   = $amount;
        $transactionFee->transaction_code         = $feeTxnCode;
        $transactionFee->bank_transaction_type_id = $feeTxnId;
        $transactionFee->withdrawal_id            = $withdrawal->id;
        $transactionFee->bank_credit_type_id      = $creditTypeId;

        return $transactionFee->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::refundFee()
     */
    public function refundFee($withdrawalId, $remarks = null, $transactionType = 'withdrawal_fee_refund', $feeTxnType = 'withdrawal_fee')
    {
        $withdrawal = $this->model->findOrFail($withdrawalId);

        $feeTxnId     = $this->validateTransactionType($feeTxnType);
        $refundFeeTxn = $this->withdrawalTransactionModel->where('bank_transaction_type_id', $feeTxnId)
                            ->where('withdrawal_id', $withdrawal->id)
                            ->firstOrFail();

        $refundFeeTxnTypeId = $this->validateTransactionType($transactionType);
        $refundFeeTxnCode   = $this->bankAccountCreditRepository->add($refundFeeTxn->bank_credit_type_id, $refundFeeTxn->amount, $withdrawal->user_id, $refundFeeTxnTypeId, $remarks);

        if (!$refundFeeTxnCode) {
            throw new \Exception('Failed saving withdrawal refund transaction');
        }

        $transaction                           = $this->withdrawalTransactionModel->newInstance();
        $transaction->amount                   = $refundFeeTxn->amount;
        $transaction->transaction_code         = $refundFeeTxnCode;
        $transaction->withdrawal_id            = $withdrawal->id;
        $transaction->bank_credit_type_id      = $refundFeeTxn->bank_credit_type_id;
        $transaction->bank_transaction_type_id = $refundFeeTxnTypeId;

        return $transaction->save();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::validateAdminFee()
     */
    public function validateAdminFee($withdrawalId, $remarks = null, $feeTxnType = 'withdrawal_fee')
    {
        $withdrawal = $this->model->findOrFail($withdrawalId);
        $feeTxnId   = $this->validateTransactionType($feeTxnType);

        return $this->withdrawalTransactionModel->where('bank_transaction_type_id', $feeTxnId)
            ->where('withdrawal_id', $withdrawal->id)
            ->first();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::getCreditExchangeRate()
     */
    public function getCreditExchangeRate($creditType)
    {
        // Credits to base currency CNY conversion
        // E.g. 100 x 0.50 = 50 CNY
        $exchangeRate = $this->exchangeRateRepository->findBySlug($creditType);

        if (!$exchangeRate) {
            throw new \Exception('Credit exchange rate is not defined');
        }

        return $exchangeRate;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::getExchangeRate()
     */
    public function getExchangeRate($countryId)
    {
        $countryExchangeRate = $this->countryExchangeRateRepository->getModel()
            ->where('country_id', $countryId)
            ->first();

        // If bank country exchange rate not supported, use default
        if (!$countryExchangeRate) {
            return $this->exchangeRateRepository->findBySlug($this->exchangeRateRepository::BASE);
        }

        $exchangeRate = $this->exchangeRateRepository->find($countryExchangeRate->exchange_rate_id);

        // If country exchange rate not found, use default
        if (!$exchangeRate) {
            return $this->exchangeRateRepository->findBySlug($this->exchangeRateRepository::BASE);
        }

        return $exchangeRate;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::calculateAdminFee()
     */
    public function calculateAdminFee($amount, $percentage)
    {
        return bcmul($amount, $percentage, 2);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::calculateAmount()
     */
    public function calculateAmount($amount, $fee, $feeType = 'inclusive')
    {
        //Exclusive amount
        if ($feeType == 'exclusive') {
            return bcadd($amount, $fee, credit_precision());
        }
        return bcsub($amount, $fee, credit_precision());
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Withdrawal\Contracts\WithdrawalContract::convertAmount()
     */
    public function convertAmount($amount, $rate, $isConvertToBase = true, $precision = 2)
    {
        if ($isConvertToBase) {
            return bcdiv($amount, $rate, $precision);
        }
        return bcmul($amount, $rate, $precision);
    }

    /**
     * add withdrawal record

     * @param string $requestId
     * @param int $userId
     * @param float $amount
     * @param float $requestAmount
     * @param float $receivableAmount
     * @param float $convertedAmount
     * @param string $currency
     * @param float $sellRate
     * @param int $withdrawalTypeId
     * @param float $feePercentage
     * @param float $fee
     * @param int $withdrawalStatusId
     * @param float $withdrawalBaseFee
     * @param float $baseSellRate
     * @param float $baseBuyRate
     * @param int $userBankId
     * @return Withdrawal
     */
    protected function addWithdrawal($requestId, $userId, $amount, $requestAmount, $receivableAmount, $convertedAmount, $currency, $sellRate, $withdrawalTypeId, $feePercentage, $fee, $withdrawalStatusId, $withdrawalBaseFee, $baseSellRate, $baseBuyRate, $userBankId)
    {
        $withdrawal                                = new $this->model;
        $withdrawal->request_id                    = $requestId;
        $withdrawal->user_id                       = $userId;
        $withdrawal->user_bank_id                  = $userBankId;
        $withdrawal->amount                        = $amount;
        $withdrawal->requested_amount              = $requestAmount;
        $withdrawal->converted_base_amount         = $convertedAmount;
        $withdrawal->receivable_destination_amount = $receivableAmount;
        $withdrawal->currency                      = $currency;
        $withdrawal->destination_sell_rate         = $sellRate;
        $withdrawal->withdrawal_type_id            = $withdrawalTypeId;
        $withdrawal->withdrawal_fee_percentage     = $feePercentage;
        $withdrawal->withdrawal_fee                = $fee;
        $withdrawal->withdrawal_status_id          = $withdrawalStatusId;
        $withdrawal->withdrawal_base_fee           = $withdrawalBaseFee;
        $withdrawal->base_sell_rate                = $baseSellRate;
        $withdrawal->base_buy_rate                 = $baseBuyRate;

        return $withdrawal->save() ? $withdrawal : null;
    }

    /**
     * Add withdrawal transaction record
     *
     * @param Withdrawal $withdrawalId
     * @param int $creditTypeId
     * @param string $withdrawTxnCode
     * @param string $withdrawalTxnId
     * @return WithdrawalTransaction
     */
    protected function addWithdrawalTransaction($withdrawal, $creditTypeId, $withdrawTxnCode, $withdrawalTxnId)
    {
        $transaction                           = new $this->withdrawalTransactionModel;
        $transaction->amount                   = $withdrawal->amount;
        $transaction->transaction_code         = $withdrawTxnCode;
        $transaction->withdrawal_id            = $withdrawal->id;
        $transaction->bank_credit_type_id      = $creditTypeId;
        $transaction->bank_transaction_type_id = $withdrawalTxnId;

        if (!$transaction->save()) {
            throw new \Exception('Failed saving withdrawal transaction');
        }

        return $transaction;
    }

    /**
     * Deduct withdrawal amount from user credit
     *
     * @param  Withdrawal $withdrawal
     * @param  int $creditType
     * @param  string $withdrawalTxnId
     * @param  string $remarks
     * @return string
     */
    protected function deductCredit(Withdrawal $withdrawal, $creditType, $withdrawalTxnId, $remarks)
    {
        $withdrawTxnCode = $this->bankAccountCreditRepository->deduct($creditType, $withdrawal->amount, $withdrawal->user_id, $withdrawalTxnId, $remarks);

        if (!$withdrawTxnCode) {
            throw new \Exception('Failed deducting user credits');
        }

        return $withdrawTxnCode;
    }
}
