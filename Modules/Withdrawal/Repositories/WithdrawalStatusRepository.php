<?php

namespace Modules\Withdrawal\Repositories;

use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Models\WithdrawalStatus;
use Modules\Withdrawal\Models\WithdrawalStatusHistory;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class WithdrawalStatusRepository extends Repository implements WithdrawalStatusContract
{
    use HasCrud, HasSlug, HasHistory, HasActive;

    protected $historyModel;

    /**
     * Class constructor
     *
     * @param WithdrawalStatus $model
     */
    public function __construct(WithdrawalStatus $model, WithdrawalStatusHistory $history)
    {
        $this->model        = $model;
        $this->historyModel = $history;
        parent::__construct($model);
    }
}
