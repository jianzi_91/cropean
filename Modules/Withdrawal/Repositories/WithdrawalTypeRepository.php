<?php

namespace Modules\Withdrawal\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Models\WithdrawalType;

class WithdrawalTypeRepository extends Repository implements WithdrawalTypeContract
{
    use HasCrud, HasSlug;

    /**
     * Class constructor
     *
     * @param WithdrawalType $model
     */
    public function __construct(WithdrawalType $model)
    {
        parent::__construct($model);
    }
}
