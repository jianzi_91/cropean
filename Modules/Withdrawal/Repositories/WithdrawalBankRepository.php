<?php

namespace Modules\Withdrawal\Repositories;

use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Modules\Withdrawal\Models\WithdrawalBank;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class WithdrawalBankRepository extends Repository implements WithdrawalBankContract
{
    use HasCrud;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param Bank  $model
     */
    public function __construct(
        WithdrawalBank $model
    ) {
        $this->model = $model;

        parent::__construct($model);
    }
}
