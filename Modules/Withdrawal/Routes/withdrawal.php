<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::put('withdrawals/update/bulk', ['as' => 'admin.withdrawals.update.bulk', 'uses' => 'Admin\WithdrawalController@bulkUpdate']);
        Route::get('withdrawals/export', ['as' => 'admin.withdrawals.export', 'uses' => 'Admin\WithdrawalExportController@export']);
        Route::resource('withdrawals', 'Admin\WithdrawalController', ['as' => 'admin'])->only(['index', 'edit', 'update']);
        Route::resource('withdrawal-settings', 'Admin\WithdrawalSettingController', ['as' => 'admin.withdrawals'])->only(['index', 'update']);
        Route::resource('members/{id}/banks', 'Admin\Management\Member\WithdrawalBankController', ['names' => 'admin.member-management.bank', 'except' => ['show']]);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('withdrawals/export', ['as' => 'member.withdrawals.export', 'uses' => 'Member\WithdrawalExportController@export']);
        Route::get('withdrawals/calculate', 'Member\WithdrawalController@calculate')->name('member.withdrawals.calculate');
        Route::get('withdrawals/exchange-rate', 'Member\WithdrawalController@getExchangeCreditDetails')->name('member.withdrawals.exchange-rate');
        Route::resource('withdrawals', 'Member\WithdrawalController', ['as' => 'member'])->only(['index', 'create', 'store', 'destroy']);
        Route::get('/bank/export', ['as' => 'member.banks.export', 'uses' => 'Member\WithdrawalBankExportController@index']);
        Route::resource('/banks', 'Member\WithdrawalBankController', ['names' => 'member.withdrawals.banks', 'except' => ['show']]);
        Route::post('withdrawals/calculate', 'Member\WithdrawalController@calculate')->name('member.withdrawals.calculate');
    });
});
