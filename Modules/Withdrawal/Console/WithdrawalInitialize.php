<?php

namespace Modules\Withdrawal\Console;

use DB;
use Illuminate\Console\Command;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class WithdrawalInitialize extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'withdrawals:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize withdrawals.';

    /**
     * The withdrawal type repository
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * The withdrawal status repository
     *
     * @var unknown
     */
    protected $withdrawalStatusRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(WithdrawalTypeContract $withdrawalTypeContract, WithdrawalStatusContract $withdrawalStatusContract)
    {
        $this->withdrawalTypeRepository   = $withdrawalTypeContract;
        $this->withdrawalStatusRepository = $withdrawalStatusContract;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            foreach (config('withdrawal.types') as $slug => $setting) {
                foreach ($setting as $slug => $settingValues) {
                    $row = $this->withdrawalTypeRepository->add([
                        'name'           => $slug,
                        'fee_percentage' => $settingValues['fee_percentage'],
                        'maximum_amount' => $settingValues['maximum_amount'],
                        'minimum_amount' => $settingValues['minimum_amount'],
                        'multiples'      => $settingValues['multiples'],
                        'is_active'      => 1
                    ]);
                }
            }

            foreach (config('withdrawal.statuses') as $slug => $status) {
                $row = $this->withdrawalStatusRepository->add(['name' => $slug, 'is_active' => 1]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $this->error($e);
        }
        $this->info('Withdrawal initialized.');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::OPTIONAL, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
