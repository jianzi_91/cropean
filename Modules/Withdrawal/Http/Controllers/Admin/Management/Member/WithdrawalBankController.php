<?php

namespace Modules\Withdrawal\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\User\Contracts\UserContract;
use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Modules\Withdrawal\Http\Requests\Admin\Bank\Management\Member\Store;
use Modules\Withdrawal\Http\Requests\Admin\Bank\Management\Member\Update;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Queries\Admin\WithdrawalBankQuery;
use Plus65\Utility\Exceptions\WebException;

class WithdrawalBankController extends Controller
{
    /**
      * The bank repository
      *
      * @var unknown
      */
    protected $bankRepository;
    protected $bankQuery;
    protected $userContract;

    /**
     * Class constructor
     *
     * @param BankContract $bankContract
     */
    public function __construct(WithdrawalBankContract $bankContract, WithdrawalBankQuery $bankQuery, UserContract $userContract)
    {
        $this->middleware('permission:admin_user_member_management_bank_list')->only('index');
        $this->middleware('permission:admin_user_member_management_bank_create')->only('create', 'store');
        $this->middleware('permission:admin_user_member_management_bank_edit')->only('edit', 'update');
        $this->middleware('permission:admin_user_member_management_bank_delete')->only('destroy');

        $this->bankRepository = $bankContract;
        $this->bankQuery      = $bankQuery;
        $this->userContract   = $userContract;
    }

    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request, $id)
    {
        $user = $this->userContract->find($id);

        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_password.user not found'));
        }

        $data = array_merge(
            $request->all(),
            [
                'id' => $id,
            ]
        );
        $banks = $this->bankQuery->setParameters($data)
            ->paginate();

        return view('withdrawal::admin.bank.management.member.index', compact('banks', 'id', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     * @param  Request $request
     * @return Response
     */
    public function create(Request $request, $id)
    {
        $user = $this->userContract->find($id);
        return view('withdrawal::admin.bank.management.member.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request, $id)
    {
        DB::beginTransaction();

        try {
            $data = array_merge($request->all(), [
                'user_id'    => $id,
                'action_by'  => auth()->user()->id,
                'country_id' => 1
            ]);
            $newBank = $this->bankRepository->add($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.member-management.bank.index', $id))->withMessage(__('a_member_management_bank.failed to create bank'));
        }

        return redirect(route('admin.member-management.bank.index', $id))->with('success', __('a_member_management_bank.bank created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit($id, $bankId)
    {
        $bank = $this->bankRepository->find($bankId);

        if (!$bank) {
            return back()->withErrors('error', __('a_member_management_bank.bank not found'));
        }

        return view('withdrawal::admin.bank.management.member.edit', compact('bank', 'id', 'bankId'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, $bankId, Update $request)
    {
        DB::beginTransaction();

        try {
            if (Withdrawal::where('user_bank_id', $bankId)->exists()) {
                $user = $this->userContract->find($id);
                // Need to maintain the state of this bank so we'll soft delete it
                $this->bankRepository->delete($bankId);

                //todo:: hard fixed country id
                $request->merge(['user_id' => $id, 'country_id' => 1, 'action_by' => auth()->user()->id]);
                $this->bankRepository->add($request->all());
            } else {
                $this->bankRepository->edit($bankId, $request->all());
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.member-management.bank.index', $id))->withMessage(__('a_member_management_bank.failed to update bank'));
        }

        return redirect(route('admin.member-management.bank.index', $id))->with('success', __('a_member_management_bank.bank updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, $bankId)
    {
        $bank = $this->bankRepository->find($bankId);
        if (!$bank) {
            return back()->withErrors('error', __('a_member_management_bank.bank not found'));
        }

        DB::beginTransaction();

        try {
            $this->bankRepository->delete($bankId);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.member-management.bank.index', $id))->withMessage(__('a_member_management_bank.failed to delete bank'));
        }

        return redirect(route('admin.member-management.bank.index', $id))->with('success', __('a_member_management_bank.bank deleted sucessfully'));
    }
}
