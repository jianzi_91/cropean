<?php

namespace Modules\Withdrawal\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Withdrawal\Contracts\WithdrawalContract;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Http\Requests\Admin\BulkUpdate;
use Modules\Withdrawal\Http\Requests\Admin\Update;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Queries\Admin\WithdrawalQuery;
use Plus65\Base\Response\ApiResponse;
use Plus65\Utility\Exceptions\WebException;

class WithdrawalController extends Controller
{
    use ApiResponse;

    /**
     * The withdrawal repository
     *
     * @var unknown
     */
    protected $withdrawalRepository;

    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * The withdrawal statuses repository
     *
     * @var unknown
     */
    protected $withdrawalStatusRepository;

    /**
     * Class constructor
     *
     * @param WithdrawalContract $withdrawalContract
     * @param WithdrawalQuery $withdrawalQuery
     * @param WithdrawalStatusContract $withdrawalStatusContract
     */
    public function __construct(WithdrawalContract $withdrawalContract, WithdrawalQuery $withdrawalQuery, WithdrawalStatusContract $withdrawalStatusContract)
    {
        $this->middleware('permission:admin_withdrawal_list')->only('index');
        $this->middleware('permission:admin_withdrawal_edit')->only('edit', 'update');

        $this->withdrawalRepository       = $withdrawalContract;
        $this->withdrawalQuery            = $withdrawalQuery;
        $this->withdrawalStatusRepository = $withdrawalStatusContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $withdrawals = $this->withdrawalQuery
            ->setParameters($request->all())
            ->paginate();

        $statuses          = system_withdrawal_statuses_dropdown();
        $isUpdatable       = $this->isUpdatable($request->status_id);
        $updatableStatuses = json_encode($this->updatableStatuses($request->status_id));

        return view('withdrawal::admin.index')->with(compact('withdrawals', 'statuses', 'isUpdatable', 'updatableStatuses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param unknown $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit($id)
    {
        $with = [
            'user',
            'bank' => function ($query) {
                $query->withTrashed();
            },
            'status.status'
        ];

        $withdrawal = $this->withdrawalRepository->find($id, $with);

        if ($withdrawal->status->status->rawname == WithdrawalStatusContract::CANCELLED) {
            return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal request not found'));
        }

        if (!$withdrawal && $withdrawal->user_id != auth()->user()->id) {
            return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal request not found'));
        }

        return view('withdrawal::admin.edit')->with(compact('withdrawal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        DB::beginTransaction();

        $withdrawal = Withdrawal::lockForUpdate()->find($id);

        if (!$withdrawal && $withdrawal->user_id != auth()->user()->id) {
            return back()->with('error', __('a_withdrawals.withdrawal request not found'));
        }

        $paidStatus = $this->withdrawalStatusRepository->findBySlug($this->withdrawalStatusRepository::PAID);
        if ($withdrawal->status->withdrawal_status_id == $paidStatus->id) {
            return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal request already paid'));
        }

        if ($request->withdrawal_status_id == $paidStatus->id) {
            $approvedStatus = $this->withdrawalStatusRepository->findBySlug($this->withdrawalStatusRepository::APPROVED);
            if ($withdrawal->status->withdrawal_status_id != $approvedStatus->id) {
                return back()->with('error', __('a_withdrawals.withdrawal request must be approved first before paid'));
            }
        }

        $cancelledStatus = $this->withdrawalStatusRepository->findBySlug($this->withdrawalStatusRepository::CANCELLED);
        if ($withdrawal->status->withdrawal_status_id == $cancelledStatus->id) {
            return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal request already cancelled'));
        }

        if ($request->withdrawal_status_id == $cancelledStatus->id) {
            if ($this->withdrawalRepository->cancel($id)) {
                if ($this->withdrawalRepository->validateAdminFee($id)) {
                    $this->withdrawalRepository->refundFee($id, '', BankTransactionTypeContract::WITHDRAWAL_FEE_CANCEL);
                }
            }
        }

        try {
            $rejectedStatus = $this->withdrawalStatusRepository->findBySlug($this->withdrawalStatusRepository::REJECTED);
            if ($rejectedStatus->id == $request->withdrawal_status_id) {
                if ($withdrawal->status->withdrawal_status_id == $rejectedStatus->id) {
                    return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal request already rejected'));
                }

                if (!$this->withdrawalRepository->reject($id, $request->remarks)) {
                    return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.withdrawal not updated'));
                }

                if ($this->withdrawalRepository->validateAdminFee($id)) {
                    $this->withdrawalRepository->refundFee($id, $request->remarks, BankTransactionTypeContract::WITHDRAWAL_FEE_REFUND);
                }
            } else {
                $status = $this->withdrawalStatusRepository->find($request->withdrawal_status_id);
                $this->withdrawalStatusRepository->changeHistory($withdrawal, $status, ['remarks' => $request->remarks, 'updated_by' => auth()->user()->id]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw (new WebException($e))->redirectTo(route('admin.withdrawals.index'))->withMessage(__('a_withdrawals.failed updating status of withdrawal'));
        }

        return redirect(route('admin.withdrawals.index'))->with('success', __('a_withdrawals.withdrawal updated successfully'));
    }

    /**
     * Bulk update status
     *
     * @param BulkUpdate $request
     * @return \Illuminate\Http\RedirectResponse|unknown
     */
    public function bulkUpdate(BulkUpdate $request)
    {
        DB::beginTransaction();
        try {
            ini_set('max_execution_time', -1);

            $status           = $this->withdrawalStatusRepository->find($request->withdrawal_status_id);
            $processingStatus = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PROCESSING);
            $approvedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::APPROVED);
            $paidStatus       = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PAID);
            $rejectedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::REJECTED);
            $cancelledStatus  = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::CANCELLED);

            $withdrawalIds = explode(',', $request->withdrawal_ids);

            foreach ($withdrawalIds as $withdrawalId) {
                $withdrawal = $this->withdrawalRepository->find($withdrawalId);

                if ($request->withdrawal_status_id == $withdrawal->withdrawal_status_id) {
                    throw new \Exception(__('a_withdrawals.failed to update status due to same status selected'));
                }

                // Ignore cancelled withdrawals
                if ($withdrawal->withdrawal_status_id == $cancelledStatus->id) {
                    continue;
                }

                if ($request->withdrawal_status_id == $cancelledStatus->id) {
                    if ($this->withdrawalRepository->cancel($withdrawal->id)) {
                        if ($this->withdrawalRepository->validateAdminFee($withdrawal->id)) {
                            $this->withdrawalRepository->refundFee($withdrawal->id, '', BankTransactionTypeContract::WITHDRAWAL_FEE_CANCEL);
                        }
                    }
                }

                if ($request->withdrawal_status_id == $rejectedStatus->id) {
                    if ($this->withdrawalRepository->reject($withdrawal->id, $request->remarks)) {
                        if ($this->withdrawalRepository->validateAdminFee($withdrawal->id)) {
                            $this->withdrawalRepository->refundFee($withdrawal->id, '', BankTransactionTypeContract::WITHDRAWAL_FEE_REFUND);
                        }
                    }
                }

                $this->withdrawalStatusRepository->changeHistory($withdrawal, $status, ['remarks' => $request->remarks, 'updated_by' => auth()->user()->id]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return redirect(route('admin.withdrawals.index'))->with('error', __('a_withdrawals.failed to update withdrawals'));
        }

        return redirect(route('admin.withdrawals.index'))->with('success', __('a_withdrawals.withdrawals updated successfully'));
    }

    public function isUpdatable($statusId = null)
    {
        $pendingStatus    = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PENDING);
        $processingStatus = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PROCESSING);
        $approvedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::APPROVED);
        $paidStatus       = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PAID);
        $rejectedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::REJECTED);
        $cancelledStatus  = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::CANCELLED);

        if ($statusId) {
            if (in_array($statusId, [$pendingStatus->id, $processingStatus->id, $approvedStatus->id])) {
                return true;
            }
        }

        return false;
    }

    public function updatableStatuses($statusId = null)
    {
        $pendingStatus    = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PENDING);
        $processingStatus = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PROCESSING);
        $approvedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::APPROVED);
        $paidStatus       = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::PAID);
        $rejectedStatus   = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::REJECTED);
        $cancelledStatus  = $this->withdrawalStatusRepository->findBySlug(WithdrawalStatusContract::CANCELLED);

        if ($statusId) {
            switch ($statusId) {
                case $pendingStatus->id:
                    $exclusions = [WithdrawalStatusContract::PENDING, WithdrawalStatusContract::PAID, WithdrawalStatusContract::CANCELLED];
                break;
                case $processingStatus->id:
                    $exclusions = [WithdrawalStatusContract::PENDING, WithdrawalStatusContract::PROCESSING, WithdrawalStatusContract::PAID, WithdrawalStatusContract::CANCELLED];
                break;
                case $approvedStatus->id:
                    $exclusions = [WithdrawalStatusContract::PENDING, WithdrawalStatusContract::PROCESSING, WithdrawalStatusContract::APPROVED, WithdrawalStatusContract::CANCELLED];
                break;
                default:
                    $exclusions = [WithdrawalStatusContract::PENDING, WithdrawalStatusContract::PROCESSING, WithdrawalStatusContract::APPROVED, WithdrawalStatusContract::PAID, WithdrawalStatusContract::REJECTED, WithdrawalStatusContract::CANCELLED];
                break;
            }

            return $this->withdrawalStatusRepository->getModel()
                ->whereNotIn('name', $exclusions)
                ->pluck('name', 'id')
                ->toArray();
        }

        return [];
    }
}
