<?php

namespace Modules\Withdrawal\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Http\Requests\Settings\Update;
use Plus65\Utility\Exceptions\WebException;

class WithdrawalSettingController extends Controller
{
    /**
     * The withdrawal type repository
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * Class constructor
     *
     * @param WithdrawalContract $withdrawalContract
     */
    public function __construct(WithdrawalTypeContract $withdrawalTypeContract)
    {
        $this->middleware('permission:admin_withdrawal_settings_edit')->only('index', 'update');

        $this->withdrawalTypeRepository = $withdrawalTypeContract;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $settings = $this->withdrawalTypeRepository->getModel()->where('name', 'bank')->get();

        return view('withdrawal::admin.setting.edit')->with(compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        if (!$withdrawalType = $this->withdrawalTypeRepository->find($id)) {
            return back()->with('errors', __('a_withdrawal_settings.withdrawal type not found'));
        }

        DB::beginTransaction();
        try {
            $data                   = $request->only('fee_percentage', 'minimum_amount', 'maximum_amount', 'multiples');
            $data['fee_percentage'] = percentage($data['fee_percentage'], 4);

            $this->withdrawalTypeRepository->edit($id, $data);

            DB::commit();
            return back()->with('success', __('a_withdrawal_settings.withdarawal settings updated'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route())->withMessage(__('a_withdrawal_settings.failed to update withdrawal settings'));
        }
    }
}
