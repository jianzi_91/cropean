<?php

namespace Modules\Withdrawal\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Withdrawal\Queries\Admin\WithdrawalQuery;
use QueryBuilder\Concerns\CanExportTrait;

class WithdrawalExportController extends Controller
{
    use CanExportTrait;

    /**
     * The withdrawal query
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * Class constructor
     *
     * @param WithdrawalQuery $withdrawalQuery
     */
    public function __construct(WithdrawalQuery $withdrawalQuery)
    {
        $this->middleware('permission:admin_withdrawal_export')->only('export');

        $this->withdrawalQuery = $withdrawalQuery;
    }

    /*
     * @param Request $request
     * @param $type
    */
    public function export(Request $request)
    {
        return $this->exportReport($this->withdrawalQuery->setParameters($request->all()), 'withdrawal_' . now() . '.xlsx', auth()->user(), 'Export Withdrawal on ' . now());
    }
}
