<?php

namespace Modules\Withdrawal\Http\Controllers\Member;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Bank\Contracts\BankContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\User\Models\User;
use Modules\Withdrawal\Contracts\WithdrawalContract;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Queries\Member\WithdrawalQuery;
use QueryBuilder\Concerns\CanExportTrait;

class WithdrawalExportController extends Controller
{
    use CanExportTrait;

    /**
     * The withdrawal repository
     *
     * @var unknown
     */
    protected $withdrawalRepository;

    /**
     * The withdrawal type repository
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * The user bank repository
     *
     * @var unknown
     */
    protected $userBankRepository;

    /**
     * The bank credit type repository
     *
     * @var unknown
     */
    protected $bankCreditTypeRepository;

    /**
     * The withdrawal status repository
     *
     * @var unknown
     */
    protected $withdrawalStatusRepository;

    /**
     * Class constructor
     *
     * @param WithdrawalContract $withdrawalContract
     * @param WithdrawalTypeContract $withdrawalTypeContract
     * @param WithdrawalQuery $withdrawalQuery
     * @param BankContract $userBankContract
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param WithdrawalStatusContract $withdrawalStatusContract
     */
    public function __construct(
        WithdrawalQuery $withdrawalQuery
    ) {
        $this->middleware('permission:member_withdrawal_export')->only('export');
        $this->withdrawalQuery = $withdrawalQuery;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function export(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->withdrawalQuery->setParameters($request->all()), 'withdrawals_' . $now . '.xlsx', auth()->user(), 'Export withdrawal on ' . $now);
    }
}
