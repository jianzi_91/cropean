<?php

namespace Modules\Withdrawal\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\User\Models\User;
use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Modules\Withdrawal\Contracts\WithdrawalContract;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Http\Requests\Member\Calculation;
use Modules\Withdrawal\Http\Requests\Member\ExchangeRate;
use Modules\Withdrawal\Http\Requests\Member\Store;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Queries\Member\WithdrawalQuery;
use Plus65\Base\Response\ApiResponse;
use Plus65\Utility\Exceptions\WebException;

class WithdrawalController extends Controller
{
    use ApiResponse;

    /**
     * The withdrawal repository
     *
     * @var unknown
     */
    protected $withdrawalRepository;

    /**
     * The withdrawal type repository
     *
     * @var unknown
     */
    protected $withdrawalTypeRepository;

    /**
     * The withdrawal query repository
     *
     * @var unknown
     */
    protected $withdrawalQuery;

    /**
     * The user bank repository
     *
     * @var unknown
     */
    protected $userBankRepository;

    /**
     * The bank credit type repository
     *
     * @var unknown
     */
    protected $bankCreditTypeRepository;

    /**
     * The withdrawal status repository
     *
     * @var unknown
     */
    protected $withdrawalStatusRepository;

    protected $exchangeRateRepository;

    /**
     * Class constructor
     *
     * @param WithdrawalContract $withdrawalContract
     * @param WithdrawalTypeContract $withdrawalTypeContract
     * @param WithdrawalQuery $withdrawalQuery
     * @param WithdrawalBankContract $userBankContract
     * @param BankCreditTypeContract $bankCreditTypeContract
     * @param WithdrawalStatusContract $withdrawalStatusContract
     * @param ExchangeRateContract $exchangeRateRepository
     */
    public function __construct(
        WithdrawalContract $withdrawalContract,
        WithdrawalTypeContract $withdrawalTypeContract,
        WithdrawalQuery $withdrawalQuery,
        WithdrawalBankContract $userBankContract,
        BankCreditTypeContract $bankCreditTypeContract,
        WithdrawalStatusContract $withdrawalStatusContract,
        ExchangeRateContract $exchangeRateRepository
    ) {
        $this->middleware('permission:member_withdrawal_list')->only('index');
        $this->middleware('permission:member_withdrawal_create')->only('create', 'store', 'calculate');
        $this->middleware('permission:member_withdrawal_cancel')->only('destroy');

        $this->withdrawalRepository       = $withdrawalContract;
        $this->withdrawalTypeRepository   = $withdrawalTypeContract;
        $this->withdrawalQuery            = $withdrawalQuery;
        $this->userBankRepository         = $userBankContract;
        $this->bankCreditTypeRepository   = $bankCreditTypeContract;
        $this->withdrawalStatusRepository = $withdrawalStatusContract;
        $this->exchangeRateRepository     = $exchangeRateRepository;
    }

    /**
     * Show the form for editing the specified resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (auth()->user()->document_verification_status_id != document_status_approve_id()) {
            abort(404);
        }

        $withdrawals = $this->withdrawalQuery
            ->setParameters($request->all())
            ->paginate();

        //check is withdrawal status
        $withdrawalStatusCount = $this->withdrawalRepository->getModel()
            ->join('withdrawal_statuses', 'withdrawal_statuses.id', 'withdrawals.withdrawal_status_id')
            ->where('withdrawals.user_id', auth()->user()->id)
            ->whereIn('withdrawal_statuses.name', [
                WithdrawalStatusContract::PENDING,
                WithdrawalStatusContract::PROCESSING
            ])
            ->count('withdrawals.id');

        $isEligibleWithdrawal = $withdrawalStatusCount == 0;

        return view('withdrawal::member.index')->with(compact('withdrawals', 'isEligibleWithdrawal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (auth()->user()->document_verification_status_id != document_status_approve_id()) {
            abort(404);
        }

        $withdrawalStatusCount = $this->withdrawalRepository->getModel()
            ->join('withdrawal_statuses', 'withdrawal_statuses.id', 'withdrawals.withdrawal_status_id')
            ->where('withdrawals.user_id', auth()->user()->id)
            ->whereIn('withdrawal_statuses.name', [
                WithdrawalStatusContract::PENDING,
                WithdrawalStatusContract::PROCESSING
            ])
            ->count('withdrawals.id');

        //check is withdrawal status
        if ($withdrawalStatusCount > 0) {
            return back()->with('error', __('m_withdrawals.unable to create a new withdrawal because there is already an active request in progress'));
        }

        $withdrawalType = $this->withdrawalTypeRepository->findBySlug($this->withdrawalTypeRepository::BANK);

        return view('withdrawal::member.create')->with(compact('withdrawalType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Store $request
     * @return Response
     */
    public function store(Store $request)
    {
        $bank = $this->userBankRepository->find($request->user_bank_id);
        if (!$bank) {
            return redirect(route('member.withdrawals.index'))->with('error', __('m_withdrawals.bank not found'));
        }

        if (!$withdrawalType = $this->withdrawalTypeRepository->findBySlug($this->withdrawalTypeRepository::BANK)) {
            return redirect(route('member.withdrawals.index'))->with('error', __('m_withdrawals.withdrawal type not found'));
        }

        DB::beginTransaction();
        try {
            User::lockForUpdate()->find(Auth::id());
            $this->withdrawalRepository->request($withdrawalType->id, $request->user_bank_id, $request->amount, $this->bankCreditTypeRepository::CASH_CREDIT, $request->remarks);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            throw (new WebException($e))->redirectTo(route('member.withdrawals.index'))->withMessage(__('m_withdrawals.failed to request withdrawal'));
        }

        return redirect(route('member.withdrawals.index'))->with('success', __('m_withdrawals.withdrawal requested successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return Mixed
     * @throws
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        $withdrawal = Withdrawal::lockForUpdate()->find($id);

        if (!$withdrawal || $withdrawal->user_id != auth()->user()->id) {
            return back()->with('error', __('m_withdrawals.withdrawal request not found'));
        }

        if ($withdrawal->status->status->rawname != $this->withdrawalStatusRepository::PENDING) {
            return back()->with('error', __('m_withdrawals.withdrawal request cannot cancel'));
        }

        try {
            if ($this->withdrawalRepository->cancel($id)) {
                if ($this->withdrawalRepository->validateAdminFee($id)) {
                    $this->withdrawalRepository->refundFee($id, '', BankTransactionTypeContract::WITHDRAWAL_FEE_CANCEL);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }
        return redirect(route('member.withdrawals.index'))->with('success', __('m_withdrawals.withdrawal cancelled successfully'));
    }

    /**
     * Calculate fee for withdrawal
     *
     * @param  Calculation $request
     * @return Response
     */
    public function calculate(Calculation $request)
    {
        $bank = $this->userBankRepository->find($request->user_bank_id);

        if (!$bank) {
            return $this->respondErrors(__('m_withdrawals.bank not found'));
        }

        if (!$withdrawalType = $this->withdrawalTypeRepository->findBySlug($this->withdrawalTypeRepository::BANK)) {
            return $this->respondErrors(__('m_withdrawals.withdrawal type not found'));
        }

        $creditExchangeRate      = $this->withdrawalRepository->getCreditExchangeRate($this->bankCreditTypeRepository::CASH_CREDIT);
        $exchangeRate            = $this->withdrawalRepository->getExchangeRate($bank->country_id);
        $withdrawalFeePercentage = $withdrawalType->fee_percentage;
        /*
        * member key in cash credit amount
        * - convert cash credit amount to base amount [usd]
        * - calculate admin fee in usd
        * - calculate total withdrawal in usd
        * - assign converted amount
        * - converted to destination currency
        * - change converted base amount to cash credit
        * - change admin fee in usd to cash credit
        */
        //convert requested amount into usd from cash credit
        $requestedBaseAmount = $this->withdrawalRepository->convertAmount($request->amount, $creditExchangeRate->sell_rate);
        //calculate admin fee in usd
        $withdrawalFeeInBaseAmount = $this->withdrawalRepository->calculateAdminFee($requestedBaseAmount, $withdrawalFeePercentage);
        $baseTotalWithdrawalAmount = $this->withdrawalRepository->calculateAmount($requestedBaseAmount, $withdrawalFeeInBaseAmount, $withdrawalType->fee_type);
        $convertedBaseAmount       = $withdrawalType->fee_type == 'inclusive' ? $baseTotalWithdrawalAmount : $requestedBaseAmount;
        //total amount will deduct from cash credit
        $receivableAmount = $this->withdrawalRepository->convertAmount($convertedBaseAmount, $exchangeRate->buy_rate, false);

        $data = [
            'admin_fee_percentage'    => $withdrawalFeePercentage,
            'admin_fee_in_usd'        => $withdrawalFeeInBaseAmount . ' ' . __('m_withdrawal_create.usd'),
            'requested_amount_in_usd' => $requestedBaseAmount,
            'base_currency'           => $creditExchangeRate->currency,
            'converted_amount_in_usd' => amount_format($convertedBaseAmount, 2) . ' ' . __('m_withdrawal_create.usd'),
            'receivable_amount'       => amount_format($receivableAmount, 2) . ' ' . __('m_withdrawal_create.cny'),
            'receivable_currency'     => $exchangeRate->currency,
            'exchange_rate'           => $exchangeRate->sell_rate,
        ];

        return $this->respondSuccess(__('m_withdrawals.calculate success'), $data);
    }

    public function getExchangeCreditDetails(ExchangeRate $request)
    {
        $bank = $this->userBankRepository->find($request->user_bank_id);
        if (!$bank) {
            return $this->respondErrors(__('m_withdrawals.bank not found'));
        }

        $destinationRate = $this->withdrawalRepository->getExchangeRate($bank->country_id);
        $baseRate        = $this->exchangeRateRepository->findBySlug($this->exchangeRateRepository::USD_CREDIT);
        $creditRate      = $this->exchangeRateRepository->findBySlug($request->bank_credit_type);

        $data = [
            'base_currency'        => $baseRate ? $baseRate->currency : '',
            'base_rate'            => $baseRate ? amount_format($baseRate->sell_rate, 2) : 1,
            'credit_from_currency' => $creditRate ? $creditRate->currency : '',
            'credit_from_rate'     => $creditRate ? amount_format($creditRate->sell_rate, 2) : 1,
            'destination_currency' => $destinationRate ? $destinationRate->currency : '',
            'destination_rate'     => $destinationRate ? amount_format($destinationRate->buy_rate, 2) : 1,
        ];

        return $this->respondSuccess(__('m_withdrawals.get exchange rate success'), $data);
    }
}
