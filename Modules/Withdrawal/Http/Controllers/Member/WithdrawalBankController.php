<?php

namespace Modules\Withdrawal\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Modules\Withdrawal\Http\Requests\Member\Bank\Store;
use Modules\Withdrawal\Http\Requests\Member\Bank\Update;
use Modules\Withdrawal\Models\Withdrawal;
use Modules\Withdrawal\Queries\Member\WithdrawalBankQuery;
use Plus65\Utility\Exceptions\WebException;

class WithdrawalBankController extends Controller
{
    /**
      * The bank repository
      *
      * @var unknown
      */
    protected $bankRepository;

    /**
      * The bank query
      *
      * @var unknown
      */
    protected $bankQuery;

    /**
     * Class constructor
     *
     * @param BankContract $bankContract
     */
    public function __construct(WithdrawalBankContract $bankContract, WithdrawalBankQuery $bankQuery)
    {
        $this->middleware('permission:member_withdrawal_bank_list')->only('index');
        $this->middleware('permission:member_withdrawal_bank_create')->only('create', 'store');
        $this->middleware('permission:member_withdrawal_bank_edit')->only('edit', 'update');
        $this->middleware('permission:member_withdrawal_bank_delete')->only('destroy');

        $this->bankRepository = $bankContract;
        $this->bankQuery      = $bankQuery;
    }

    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $banks = $this->bankQuery->setParameters($request->all())
            ->paginate();

        return view('withdrawal::member.bank.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     * @param  Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        return view('withdrawal::member.bank.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();

        try {
            $data    = array_merge($request->all(), ['user_id' => $request->user()->id, 'country_id' => 1]);
            $newBank = $this->bankRepository->add($data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.withdrawals.banks.create'))->withMessage(__('m_bank.failed to create bank'));
        }

        return redirect(route('member.withdrawals.banks.index'))->with('success', __('m_bank.bank created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit($id)
    {
        $bank = $this->bankRepository->find($id);

        if (!$bank) {
            return back()->withErrors('error', __('m_bank.bank not found'));
        }

        return view('withdrawal::member.bank.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Update $request)
    {
        if (!$bank = $this->bankRepository->find($id)) {
            return redirect(route('member.withdrawals.banks.index'))->with('error', __('m_bank.bank not found'));
        }

        DB::beginTransaction();

        try {
            if (Withdrawal::where('user_bank_id', $id)->exists()) {
                // Need to maintain the state of this bank so we'll soft delete it
                $this->bankRepository->delete($id);

                $request->merge(['user_id' => $request->user()->id, 'country_id' => $request->user()->country_id]);
                $this->bankRepository->add($request->all());
            } else {
                $this->bankRepository->edit($id, $request->all());
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.withdrawals.banks.edit', $id))->withMessage(__('m_bank.failed to update bank'));
        }

        return redirect(route('member.withdrawals.banks.index'))->with('success', __('m_bank.bank updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $bank = $this->bankRepository->find($id);
        if (!$bank) {
            return back()->withErrors('error', __('m_bank.bank not found'));
        }

        DB::beginTransaction();

        try {
            $this->bankRepository->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.withdrawals.banks.index'))->withMessage(__('m_bank.failed to delete bank'));
        }

        return redirect(route('member.withdrawals.banks.index'))->with('success', __('m_bank.bank deleted sucessfully'));
    }
}
