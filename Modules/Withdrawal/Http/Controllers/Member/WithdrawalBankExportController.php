<?php

namespace Modules\Withdrawal\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Withdrawal\Queries\Member\WithdrawalBankQuery;
use QueryBuilder\Concerns\CanExportTrait;

class WithdrawalBankExportController extends Controller
{
    use CanExportTrait;

    /**
      * The bank query
      *
      * @var unknown
      */
    protected $bankQuery;

    /**
     * Class constructor
     *
     * @param WithdrawalBankQuery $bankQuery
     */
    public function __construct(WithdrawalBankQuery $bankQuery)
    {
        $this->middleware('permission:member_withdrawal_bank_export')->only('index');
        $this->bankQuery = $bankQuery;
    }

    /**
     * Display a listing of the resource.
     * @param  Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->bankQuery->setParameters($request->all()), 'bank_' . now() . '.xlsx', auth()->user(), 'Export Withdrawal Bank on ' . now());
    }
}
