<?php

namespace Modules\Withdrawal\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;

class ValidWithdrawalAmount implements Rule
{
    /**
     * The withdrawal type id
     *
     * @var unknown
     */
    protected $withdrawalTypeId;

    /**
     * The message
     *
     * @var unknown
     */
    protected $message;

    /**
     * Create a new rule instance.
     *
     * @param unknown $withdrawalTypeId
     */
    public function __construct($creditType, $withdrawalTypeId = null)
    {
        $this->creditTypeRepository     = resolve(BankCreditTypeContract::class);
        $this->withdrawalTypeRepository = resolve(WithdrawalTypeContract::class);
        $this->withdrawalTypeId         = $withdrawalTypeId;
        $this->creditType               = $creditType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $withdrawalType = $this->withdrawalTypeRepository->find($this->withdrawalTypeId);

        if (!$withdrawalType) {
            $this->message = __('s_validation.invalid withdrawal type');
            return false;
        }

        if ($withdrawalType->rawname == WithdrawalTypeContract::BLOCKCHAIN) {
            $creditType = $this->creditTypeRepository->findBySlug($this->creditType);

            if (isset($creditType)) {
                $minimumAmount = setting('withdrawal_' . $creditType->rawname . '_min');
                $maximumAmount = setting('withdrawal_' . $creditType->rawname . '_max');
                $multiples     = setting('withdrawal_' . $creditType->rawname . '_multiple');
            } else {
                $this->message = __('s_validation.please select a credit type');
                return false;
            }
        } else {
            $minimumAmount = $withdrawalType->minimum_amount;
            $maximumAmount = $withdrawalType->maximum_amount;
            $multiples     = $withdrawalType->multiples;
        }

        if ($minimumAmount > 0) {
            if ($value < $minimumAmount) {
                $this->message = __('s_validation.minimum amount is :minimum_amount', ['minimum_amount' => amount_format($minimumAmount)]);
                return false;
            }
        }

        if ($maximumAmount > 0) {
            if ($value > $maximumAmount) {
                $this->message = __('s_validation.maximum amount is :maximum_amount', ['maximum_amount' => amount_format($maximumAmount)]);
                return false;
            }
        }

        if ($multiples > 0) {
            $modulo        = fmod((float) $value, (float) $multiples);
            $this->message = __('s_validation.amount must be a multiples of :multiples', ['multiples' => amount_format($multiples)]);

            return 0 === $modulo || 0.0 === $modulo;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
