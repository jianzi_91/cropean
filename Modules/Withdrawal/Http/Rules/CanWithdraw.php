<?php

namespace Modules\Withdrawal\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\Credit\Contracts\BankCreditTypeContract;

class CanWithdraw implements Rule
{
    /**
     * Withdrawal address repository.
     *
     * @var BankCreditTypeContract
     */
    protected $bankCreditTypeRepo;

    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
        $this->bankCreditTypeRepo = resolve(BankCreditTypeContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $creditType = $this->bankCreditTypeRepo->findBySlug($value);
        return $creditType->can_withdraw;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('m_withdrawals.cannot withdraw this currency');
    }
}
