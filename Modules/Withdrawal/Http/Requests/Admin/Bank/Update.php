<?php

namespace Modules\Withdrawal\Http\Requests\Admin\Bank;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'account_name'       => 'required|string|max:255',
            'account_number'     => 'required|string|max:255',
            'name'               => 'required|string|max:255',
            'branch'             => 'required|string|max:255',
            'province'           => 'required|string|max:255',
            'address'            => 'required|string|max:255',
            'swift_code'         => 'required|string|max:255',
            'state'              => 'required|string|max:255',
            'city'               => 'required|string|max:255',
            'secondary_password' => ['required', new SecondaryPasswordMatch()]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'name'               => __('a_edit_bank.name'),
            'branch'             => __('a_edit_bank.branch'),
            'swift_code'         => __('a_edit_bank.swift code'),
            'account_number'     => __('a_edit_bank.account number'),
            'account_name'       => __('a_edit_bank.account name'),
            'address'            => __('a_edit_bank.address'),
            'zipcode'            => __('a_edit_bank.zipcode'),
            'state'              => __('a_edit_bank.state'),
            'city'               => __('a_edit_bank.city'),
            'secondary_password' => __('a_edit_bank.secondary password'),
        ];
    }
}
