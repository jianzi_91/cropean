<?php
namespace Modules\Withdrawal\Http\Requests\Admin\Bank\Management\Member;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'account_name'   => 'required|string|max:255',
            'account_number' => 'required|string|max:255',
            'name'           => 'required|string|max:255',
            'province'       => 'required|string|max:255',
            'city'           => 'required|string|max:255',
            'branch'         => 'required|string|max:255',
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'account_name'   => __('a_member_management_edit_bank.full name'),
            'account_number' => __('a_member_management_edit_bank.account number'),
            'name'           => __('a_member_management_edit_bank.bank name'),
            'province'       => __('a_member_management_edit_bank.bank province'),
            'city'           => __('a_member_management_edit_bank.bank city'),
            'branch'         => __('a_member_management_edit_bank.bank branch'),
        ];
    }
}
