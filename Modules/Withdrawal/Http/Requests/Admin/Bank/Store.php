<?php

namespace Modules\Withdrawal\Http\Requests\Admin\Bank;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'member_id'          => 'required|string|exists:users,member_id',
            'account_name'       => 'required|string|max:255',
            'account_number'     => 'required|string|max:255',
            'name'               => 'required|string|max:255',
            'branch'             => 'required|string|max:255',
            'province'           => 'required|string|max:255',
            'address'            => 'required|string|max:255',
            'swift_code'         => 'required|string|max:255',
            'state'              => 'required|string|max:255',
            'city'               => 'required|string|max:255',
            'secondary_password' => ['required', new SecondaryPasswordMatch()]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'member_id'          => __('a_bank.member id'),
            'name'               => __('a_bank.name'),
            'branch'             => __('a_bank.branch'),
            'swift_code'         => __('a_bank.swift code'),
            'account_number'     => __('a_bank.account number'),
            'account_name'       => __('a_bank.account name'),
            'address'            => __('a_bank.address'),
            'zipcode'            => __('a_bank.zipcode'),
            'state'              => __('a_bank.state'),
            'city'               => __('a_bank.city'),
            'country_id'         => __('a_bank.country'),
            'secondary_password' => __('a_bank.secondary password'),
        ];
    }
}
