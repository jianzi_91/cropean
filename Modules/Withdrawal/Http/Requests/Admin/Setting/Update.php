<?php

namespace Modules\Withdrawal\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fee_percentage' => [
                'required',
                'numeric',
                'min:0',
                'max:100',
                new MaxDecimalPlaces(2),
            ],
            'minimum_amount' => 'required|integer|gte:0',
            'multiples'      => 'required|integer|gte:0',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
