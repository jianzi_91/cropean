<?php

namespace Modules\Withdrawal\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Repositories\WithdrawalStatusRepository;

class BulkUpdate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'withdrawal_ids'       => 'required',
            'withdrawal_status_id' => 'required|integer|exists:withdrawal_statuses,id',
        ];

        /** @var WithdrawalStatusRepository $withdrawalStatusContract */
        $withdrawalStatusContract = resolve(WithdrawalStatusContract::class);
        $rejectedStatus           = $withdrawalStatusContract->findBySlug($withdrawalStatusContract::REJECTED);
        if ($rejectedStatus->id == $this->withdrawal_status_id) {
            $rules['remarks'] = 'required|max:255';
        }
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
