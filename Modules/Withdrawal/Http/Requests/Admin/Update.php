<?php

namespace Modules\Withdrawal\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Modules\Withdrawal\Repositories\WithdrawalStatusRepository;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'withdrawal_status_id' => 'required|integer|exists:withdrawal_statuses,id',
        ];

        /** @var WithdrawalStatusRepository $withdrawalStatusContract */
        $withdrawalStatusContract = resolve(WithdrawalStatusContract::class);
        $rejectedStatus           = $withdrawalStatusContract->findBySlug($withdrawalStatusContract::REJECTED);
        if ($rejectedStatus->id == $this->withdrawal_status_id) {
            $rules['remarks'] = 'required|max:255';
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'withdrawal_status_id' => __('a_view_withdrawal.status'),
        ];
    }
}
