<?php

namespace Modules\Withdrawal\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class Calculation extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_bank_id' => [
                'bail',
                'required',
                'exists:withdrawal_banks,id',
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'gt:0',
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_bank_id' => __('m_create_withdrawal.user bank'),
            'amount'       => __('m_create_withdrawal.amount'),
        ];
    }
}
