<?php

namespace Modules\Withdrawal\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\EnoughCredit;
use Modules\Credit\Http\Rules\IsWithdrawable;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Withdrawal\Contracts\WithdrawalTypeContract;
use Modules\Withdrawal\Rules\ValidWithdrawalAmount;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $withdrawalTypeRepo = resolve(WithdrawalTypeContract::class);

        // For Withdrawal type Bank
        $this->merge([
            'withdrawal_type' => WithdrawalTypeContract::BANK,
        ]);

        return [
            'bank_credit_type' => [
                'bail',
                'required',
                'exists:bank_credit_types,name',
                new IsWithdrawable(),
            ],
            'user_bank_id' => [
                'bail',
                'required_if:withdrawal_type,' . WithdrawalTypeContract::BANK,
                'exists:withdrawal_banks,id',
            ],
            'amount' => [
                'bail',
                'required',
                'numeric',
                'max:' . config('withdrawal.withdrawal.max'),
                'gt:0',
                new NotScientificNotation(),
                new ValidWithdrawalAmount($this->bank_credit_type, $withdrawalTypeRepo->findBySlug($this->withdrawal_type)->id),
                new EnoughCredit($this->bank_credit_type, auth()->user()->id), //Admin Fee Inclusive
                new MaxDecimalPlaces(2),
            ],
            'secondary_password' => [
                'required',
                new SecondaryPasswordMatch()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'amount.regex' => __('m_create_withdrawal.invalid amount format'),
        ];
    }
}
