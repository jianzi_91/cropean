<?php

namespace Modules\Withdrawal\Http\Requests\Member\Bank;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;
use Modules\Withdrawal\Contracts\WithdrawalBankContract;
use Plus65\Base\Rules\CheckOwnership;

class Update extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'account_number'     => 'required|string|max:255',
            'name'               => 'required|string|max:255',
            'province'           => 'required|string|max:255',
            'city'               => 'required|string|max:255',
            'branch'             => 'required|string|max:255',
            'secondary_password' => ['required', new SecondaryPasswordMatch()]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $bank = resolve(WithdrawalBankContract::class)->find($this->bank);
        if (!$bank) {
            return false;
        }

        return $this->isOwner($bank, $this->user());
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'account_name'       => __('m_edit_bank.bank account name'),
            'account_number'     => __('m_edit_bank.bank account number'),
            'name'               => __('m_edit_bank.bank name'),
            'branch'             => __('m_edit_bank.bank branch'),
            'province'           => __('m_edit_bank.bank province'),
            'address'            => __('m_edit_bank.address'),
            'swift_code'         => __('m_edit_bank.swift code'),
            'state'              => __('m_edit_bank.state'),
            'city'               => __('m_edit_bank.city'),
            'secondary_password' => __('m_edit_bank.secondary password'),
        ];
    }
}
