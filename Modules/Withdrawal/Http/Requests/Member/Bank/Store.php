<?php

namespace Modules\Withdrawal\Http\Requests\Member\Bank;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'account_name'       => 'required|string|max:255',
            'account_number'     => 'required|string|max:255',
            'name'               => 'required|string|max:255',
            'province'           => 'required|string|max:255',
            'city'               => 'required|string|max:255',
            'branch'             => 'required|string|max:255',
            'secondary_password' => ['required', new SecondaryPasswordMatch()]
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'account_name'       => __('m_create_bank.bank account name'),
            'account_number'     => __('m_create_bank.bank account number'),
            'name'               => __('m_create_bank.bank name'),
            'branch'             => __('m_create_bank.bank branch'),
            'secondary_password' => __('m_create_bank.secondary password'),
        ];
    }
}
