<?php

namespace Modules\Withdrawal\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class ExchangeRate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_bank_id' => [
                'bail',
                'required',
                'exists:withdrawal_banks,id',
            ],
            'bank_credit_type' => [
                'bail',
                'required',
                'exists:bank_credit_types,name',
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' messages.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_bank_id'     => __('m_create_withdrawal.user bank'),
            'bank_credit_type' => __('m_create_withdrawal.bank credit type'),
        ];
    }
}
