<?php

namespace Modules\Withdrawal\Queries\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Withdrawal\Queries\WithdrawalBankQuery as BaseQuery;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class WithdrawalBankQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize, FromCollection, WithCustomValueBinder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'id' => [
            'filter'    => 'equal',
            'table'     => 'withdrawal_banks',
            'column'    => 'user_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'withdrawal_banks',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'username' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'username',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'withdrawal_banks',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'account_number' => [
            'filter'    => 'text',
            'table'     => 'withdrawal_banks',
            'column'    => 'account_number',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'account_name' => [
            'filter'    => 'text',
            'table'     => 'withdrawal_banks',
            'column'    => 'account_name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'search' => [
            'filter'    => 'search_bank',
            'table'     => 'withdrawal_banks',
            'namespace' => 'Modules\Withdrawal\Queries\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
        return $this->builder;
    }

    public function map($bank): array
    {
        return [
            $bank->username,
            $bank->name,
            $bank->branch,
            $bank->swift_code,
            $bank->account_number,
            $bank->account_name,
            $bank->address,
            $bank->state,
            $bank->city,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_banks.username'),
            __('a_banks.bank name'),
            __('a_banks.bank branch'),
            __('a_banks.swift code'),
            __('a_banks.bank account number'),
            __('a_banks.bank account name'),
            __('a_banks.bank address'),
            __('a_banks.bank state'),
            __('a_banks.bank city'),
        ];
    }

    public function collection()
    {
        return $this->get();
    }

    public function bindValue(Cell $cell, $value)
    {
        // Force column C(Account Number) to be string format
        if (in_array($cell->getColumn(), ['E'])) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
