<?php

namespace Modules\Withdrawal\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Withdrawal\Queries\WithdrawalQuery as BaseQuery;

class WithdrawalQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'withdrawals',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'updated_at' => [
            'filter'    => 'date_range',
            'table'     => 'withdrawals',
            'column'    => 'updated_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'like',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'reference_number' => [
            'filter'    => 'equal',
            'table'     => 'withdrawals',
            'column'    => 'request_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'bank_name' => [
            'filter'    => 'equal',
            'table'     => 'withdrawal_banks',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'bank_account_number' => [
            'filter'    => 'equal',
            'table'     => 'withdrawal_banks',
            'column'    => 'account_number',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'status_id' => [
            'filter'    => 'equal',
            'table'     => 'withdrawal_statuses',
            'column'    => 'id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder;
    }

    public function map($withdrawal): array
    {
        return [
            $withdrawal->created_at,
            $withdrawal->request_id,
            $withdrawal->user->member_id,
            $withdrawal->user->email,
            $withdrawal->account_name,
            " " . $withdrawal->account_number,
            $withdrawal->bank_name,
            $withdrawal->province,
            $withdrawal->city,
            $withdrawal->branch,
            amount_format($withdrawal->amount, credit_precision()),
            amount_format($withdrawal->withdrawal_base_fee, credit_precision()),
            amount_format($withdrawal->converted_base_amount, credit_precision()),
            amount_format($withdrawal->receivable_destination_amount, credit_precision()),
            __('s_withdrawal_statuses.' . $withdrawal->status_name),
            $withdrawal->remarks,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_withdrawal_list.date'),
            __('a_withdrawal_list.reference number'),
            __('a_withdrawal_list.member id'),
            __('a_withdrawal_list.email'),
            __('a_withdrawal_list.bank account name'),
            __('a_withdrawal_list.bank account number'),
            __('a_withdrawal_list.bank name'),
            __('a_withdrawal_list.bank province'),
            __('a_withdrawal_list.bank city'),
            __('a_withdrawal_list.bank branch'),
            __('a_withdrawal_list.withdrawal amount cash credits'),
            __('a_withdrawal_list.admin fee usd'),
            __('a_withdrawal_list.total converted amount usd'),
            __('a_withdrawal_list.total receivable amount cny'),
            __('a_withdrawal_list.status'),
            __('a_withdrawal_list.remarks'),
        ];
    }
}
