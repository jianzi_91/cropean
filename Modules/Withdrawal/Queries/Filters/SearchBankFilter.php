<?php

namespace Modules\Withdrawal\Queries\Filters;

use QueryBuilder\QueryFilter;

class SearchBankFilter extends QueryFilter
{
    /**
     *
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryFilter::filter()
     */
    public function filter($builder)
    {
        $elName = $this->name; // search attribute

        if (!$this->parameter->has($elName)
            || !$elName
            || !$this->parameter->get($elName)) {
            return $builder;
        }

        if ($this->alias) {
            if (false !== strpos($this->alias, '.')) {
                [$table, $colName] = explode('.', $this->alias);
            } else {
                $colName = $this->alias;
                $table   = $this->getTableName($builder);
            }
        } else {
            $table   = $this->getTableName($builder);
            $colName = $elName;
        }

        if (!$table) {
            throw new \Exception('Unknown instance of builder');
        }

        $value  = $this->parameter->get($elName);
        $column = $table . '.' . $colName;

        return $builder->where(function ($builder) use ($table, $value) {
            $builder->where($table . '.name', 'like', '%' . $value . '%')
                ->orWhere($table . '.branch', 'like', '%' . $value . '%')
                ->orWhere($table . '.swift_code', 'like', '%' . $value . '%')
                ->orWhere($table . '.account_number', 'like', '%' . $value . '%')
                ->orWhere($table . '.account_name', 'like', '%' . $value . '%');
        });
    }
}
