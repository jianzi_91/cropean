<?php

namespace Modules\Withdrawal\Queries\Member;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Withdrawal\Queries\WithdrawalBankQuery as BaseQuery;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class WithdrawalBankQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize, FromCollection, WithCustomValueBinder
{
    use Exportable;

    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'withdrawal_banks',
            'column' => 'created_at'
        ],
        'name' => [
            'filter' => 'text',
            'table'  => 'withdrawal_banks',
            'column' => 'name'
        ],
        'account_number' => [
            'filter' => 'text',
            'table'  => 'withdrawal_banks',
            'column' => 'account_number'
        ],
        'account_name' => [
            'filter' => 'text',
            'table'  => 'withdrawal_banks',
            'column' => 'account_name'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('users.id', '=', Auth::user()->id);
    }

    public function map($bank): array
    {
        return [
            $bank->account_name,
            $bank->account_number,
            $bank->name,
            $bank->province,
            $bank->city,
            $bank->branch,
        ];
    }

    public function headings(): array
    {
        return [
            __('m_banks.bank account name'),
            __('m_banks.bank account number'),
            __('m_banks.bank name'),
            __('m_banks.bank province'),
            __('m_banks.bank city'),
            __('m_banks.bank branch'),
        ];
    }

    public function collection()
    {
        return $this->get();
    }

    public function bindValue(Cell $cell, $value)
    {
        // Force column C(Account Number) to be string format
        if (in_array($cell->getColumn(), ['B'])) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
