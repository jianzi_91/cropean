<?php

namespace Modules\Withdrawal\Queries\Member;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Withdrawal\Queries\WithdrawalQuery as BaseQuery;

class WithdrawalQuery extends BaseQuery implements WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;

    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'withdrawals',
            'column' => 'created_at'
        ],
        'reference_number' => [
            'filter' => 'equal',
            'table'  => 'withdrawals',
            'column' => 'request_id'
        ],
        'bank_name' => [
            'filter' => 'equal',
            'table'  => 'withdrawal_banks',
            'column' => 'name'
        ],
        'status_id' => [
            'filter' => 'equal',
            'table'  => 'withdrawal_statuses',
            'column' => 'id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('withdrawals.user_id', auth()->user()->id);
    }

    public function map($withdrawal): array
    {
        return [
            $withdrawal->created_at,
            $withdrawal->request_id,
            $withdrawal->account_name,
            " " . $withdrawal->account_number,
            $withdrawal->bank_name,
            $withdrawal->province,
            $withdrawal->city,
            $withdrawal->branch,
            amount_format($withdrawal->amount, credit_precision()),
            amount_format($withdrawal->requested_amount, credit_precision()),
            amount_format($withdrawal->withdrawal_base_fee, credit_precision()),
            amount_format($withdrawal->converted_base_amount, credit_precision()),
            amount_format($withdrawal->receivable_destination_amount, credit_precision()),
            __('s_withdrawal_statuses.' . $withdrawal->status_name),
            $withdrawal->remarks,
        ];
    }

    public function headings(): array
    {
        return [
            __('m_withdrawal_list.date'),
            __('m_withdrawal_list.reference number'),
            __('m_withdrawal_list.bank account name'),
            __('m_withdrawal_list.bank account number'),
            __('m_withdrawal_list.bank name'),
            __('m_withdrawal_list.bank province'),
            __('m_withdrawal_list.bank city'),
            __('m_withdrawal_list.bank branch'),
            __('m_withdrawal_list.withdrawal amount cash credits'),
            __('m_withdrawal_list.requested withdrawal amount usd'),
            __('m_withdrawal_list.admin fee usd'),
            __('m_withdrawal_list.total converted amount usd'),
            __('m_withdrawal_list.total receivable amount cny'),
            __('m_withdrawal_list.status'),
            __('m_withdrawal_list.remarks'),
        ];
    }
}
