<?php

namespace Modules\Withdrawal\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Withdrawal\Models\Withdrawal;
use QueryBuilder\QueryBuilder;

class WithdrawalQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = Withdrawal::select([
                'withdrawals.*',
                'withdrawal_banks.name as bank_name',
                'withdrawal_banks.branch',
                'withdrawal_banks.swift_code',
                'withdrawal_banks.account_name',
                'withdrawal_banks.account_number',
                'withdrawal_banks.address',
                'withdrawal_banks.province',
                'withdrawal_banks.zipcode',
                'withdrawal_banks.state',
                'withdrawal_banks.city',
                'withdrawal_statuses.name as status_name',
                'withdrawal_statuses.id as status_id',
                'countries.name as country_name',
                'users.email'
            ])
            ->join('users', 'users.id', 'withdrawals.user_id')
            ->join('withdrawal_banks', 'withdrawal_banks.id', 'withdrawals.user_bank_id')
            ->join('withdrawal_status_histories', function ($join) {
                $join->on('withdrawal_status_histories.withdrawal_id', '=', 'withdrawals.id')
                    ->where('withdrawal_status_histories.is_current', 1)
                    ->whereNull('withdrawal_status_histories.deleted_at');
            })
            ->join('withdrawal_statuses', 'withdrawal_statuses.id', '=', 'withdrawal_status_histories.withdrawal_status_id')
            ->join('countries', 'countries.id', 'withdrawal_banks.country_id')
            ->orderBy('withdrawals.created_at', 'desc');

        return $query;
    }

    public function collection()
    {
        return $this->get();
    }
}
