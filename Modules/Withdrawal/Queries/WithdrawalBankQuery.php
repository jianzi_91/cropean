<?php

namespace Modules\Withdrawal\Queries;

use Modules\Withdrawal\Models\WithdrawalBank;
use QueryBuilder\QueryBuilder;

class WithdrawalBankQuery extends QueryBuilder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = WithdrawalBank::join('users', 'withdrawal_banks.user_id', 'users.id')->select(['withdrawal_banks.*', 'users.username']);
        return $query;
    }
}
