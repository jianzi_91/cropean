<?php

namespace Modules\Withdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface WithdrawalTypeContract extends CrudContract, SlugContract
{
    const BANK       = 'bank';
    const BLOCKCHAIN = 'blockchain';
}
