<?php

namespace Modules\Withdrawal\Contracts;

interface RequestIdContract
{
    /**
     * Generate request id
     * @param string $prefix
     * @param number $length
     * @return string
     */
    public function generate($prefix = 'REQ', $length = 6);
}
