<?php

namespace Modules\Withdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface WithdrawalBankContract extends CrudContract
{
}
