<?php

namespace Modules\Withdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface WithdrawalContract extends CrudContract, SlugContract
{
    /**
     * Withdraw bank account credits
     *
     * @param unknown $withdrawalTypeId
     * @param unknown $userBankId
     * @param unknown $amount
     * @param unknown $currency
     * @param unknown $creditType
     * @param unknown $remarks
     * @return string
     */
    public function request($withdrawalTypeId, $userBankId, $amount, $creditType, $remarks = null);

    /**
     * Cancel withdrawal request
     *
     * @param unknown $withdrawalId
     * @param unknown $remarks
     * @return boolean
     */
    public function cancel($withdrawalId, $remarks = null);

    /**
     * Reject withdrawal
     *
     * @param unknown $withdrawalId
     * @param unknown $remarks
     */
    public function reject($withdrawalId, $remarks = null);

    /**
     * Refund withdrawal
     *
     * @param unknown $withdrawalId
     * @param unknown $remarks
     * @param string $transactionType
     */
    public function refund($withdrawalId, $remarks = null, $transactionType = 'withdrawal_refund');

    /**
     * Add fee
     *
     * @param unknown $withdrawal
     * @param unknown $amount
     * @param unknown $creditType
     * @param unknown $remarks
     * @param string $transactionType
     */
    public function addFee($withdrawal, $amount, $creditType, $remarks = null, $transactionType = 'withdrawal_fee');

    /**
     * Refund withdrawal fee
     *
     * @param unknown $withdrawalId
     * @param unknown $remarks
     * @param string $transactionType
     * @param string $feeTxnType
     */
    public function refundFee($withdrawalId, $remarks = null, $transactionType = 'withdrawal_fee_refund', $feeTxnType = 'withdrawal_fee');

    /**
     * Validate admin fee
     *
     * @param unknown $withdrawalId
     * @param unknown $remarks
     * @param string $feeTxnType
     */
    public function validateAdminFee($withdrawalId, $remarks = null, $feeTxnType = 'withdrawal_fee');

    /**
     * Get exchange rate of the credit to base currency
     *
     * @param unknown $creditType
     */
    public function getCreditExchangeRate($creditType);

    /**
     * Get exchange rate based on the country
     *
     * @param unknown $countryId
     */
    public function getExchangeRate($countryId);

    /**
     * Calculate admin fee
     *
     * @param  float $amount
     * @param  float $percentage
     * @return float
     */
    public function calculateAdminFee($amount, $percentage);

    /**
     * Calculate withdrawal amount
     *
     * @param  float $amount
     * @param  float $fee
     * @param string $feeType
     * @return float
     */
    public function calculateAmount($amount, $fee, $feeType = 'inclusive');

    /**
     * Convert withdrawal amount to bank currency
     *
     * @param  float $amount
     * @param  float $rate
     * @param bool $isConvertToBase
     * @param int $precision
     * @return float
     */
    public function convertAmount($amount, $rate, $isConvertToBase = true, $precision = 2);
}
