<?php

namespace Modules\Withdrawal\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface WithdrawalStatusContract extends CrudContract, SlugContract, ActiveContract, HistoryableContract
{
    const PENDING    = 'pending';
    const PROCESSING = 'processing';
    const APPROVED   = 'approved';
    const PAID       = 'paid';
    const REJECTED   = 'rejected';
    const CANCELLED  = 'cancelled';
}
