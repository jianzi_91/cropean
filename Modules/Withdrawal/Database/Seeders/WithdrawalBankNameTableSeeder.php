<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;
use Modules\Withdrawal\Models\WithdrawalBankName;

class WithdrawalBankNameTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_withdrawal_bank_names',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $names = [
            0 => [
                'code'             => 'BOC',
                'name'             => 'bank_of_china',
                'name_translation' => 'Bank of China',
                'is_active'        => 1,
            ],
            1 => [
                'code'             => 'ICBC',
                'name'             => 'industrial_commercial_bank_china',
                'name_translation' => 'Industrial and Commercial Bank of China',
                'is_active'        => 1,
            ],
            2 => [
                'code'             => 'ABC',
                'name'             => 'agricultural_bank_china',
                'name_translation' => 'Agricultural Bank of China',
                'is_active'        => 1,
            ],
            3 => [
                'code'             => 'CCB',
                'name'             => 'china_construction_bank',
                'name_translation' => 'China Construction Bank',
                'is_active'        => 1,
            ],
        ];

        foreach ($names as $name) {
            $result = (new WithdrawalBankName($name))->save();
        }
    }
}
