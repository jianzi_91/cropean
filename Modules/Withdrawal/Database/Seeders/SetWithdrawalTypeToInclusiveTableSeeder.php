<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Withdrawal\Models\WithdrawalType;

class SetWithdrawalTypeToInclusiveTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $withdrawalType = WithdrawalType::where('name', 'bank')->first();

        $withdrawalType->update([
            'fee_type' => 'inclusive',
        ]);
    }
}
