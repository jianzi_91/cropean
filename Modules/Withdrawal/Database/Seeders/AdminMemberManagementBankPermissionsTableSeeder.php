<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class AdminMemberManagementBankPermissionsTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_bank' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Bank',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_bank_list',
                        'title' => 'Admin Member Management Bank List',
                    ],
                    [
                        'name'  => 'admin_user_member_management_bank_create',
                        'title' => 'Admin Member Management Create Bank',
                    ],
                    [
                        'name'  => 'admin_user_member_management_bank_edit',
                        'title' => 'Admin Member Management Edit Bank',
                    ],
                    [
                        'name'  => 'admin_user_member_management_bank_delete',
                        'title' => 'Admin Member Management Delete Bank',
                    ]
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_user_member_management_bank_list',
                'admin_user_member_management_bank_create',
                'admin_user_member_management_bank_edit',
                'admin_user_member_management_bank_delete',
            ],
            Role::ADMIN => [
                'admin_user_member_management_bank_list',
                'admin_user_member_management_bank_create',
                'admin_user_member_management_bank_edit',
                'admin_user_member_management_bank_delete',
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
