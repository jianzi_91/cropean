<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;
use Modules\Withdrawal\Models\WithdrawalStatus;

class WithdrawalStatusPageTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_withdrawal_statuses',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);
    }
}
