<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class WithdrawalBankPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'member_withdrawal_bank' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Withdrawal Bank',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_withdrawal_bank_list',
                        'title' => 'Member Withdrawal Bank List',
                    ],
                    [
                        'name'  => 'member_withdrawal_bank_create',
                        'title' => 'Member Create Withdrawal Bank',
                    ],
                    [
                        'name'  => 'member_withdrawal_bank_edit',
                        'title' => 'Member Edit Withdrawal Bank',
                    ],
                    [
                        'name'  => 'member_withdrawal_bank_delete',
                        'title' => 'Member Delete Withdrawal Bank',
                    ],
                    [
                        'name'  => 'member_withdrawal_bank_export',
                        'title' => 'Member Export Withdrawal Bank List',
                    ]
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
            ],
            Role::ADMIN => [
            ],
            Role::MEMBER => [
                'member_withdrawal_bank_list',
                'member_withdrawal_bank_create',
                'member_withdrawal_bank_edit',
                'member_withdrawal_bank_delete',
                'member_withdrawal_bank_export'
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
