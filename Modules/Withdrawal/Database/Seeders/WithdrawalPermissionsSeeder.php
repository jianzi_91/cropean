<?php

namespace Modules\Withdrawal\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class WithdrawalPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_withdrawal_list',
                        'title' => 'Admin Withdrawal List',
                    ],
                    [
                        'name'  => 'admin_withdrawal_edit',
                        'title' => 'Admin Withdrawal Edit',
                    ],
                    [
                        'name'  => 'admin_withdrawal_export',
                        'title' => 'Admin Withdrawal Export',
                    ],
                    [
                        'name'  => 'admin_withdrawal_settings_list',
                        'title' => 'Admin Withdrawal Settings List',
                    ],
                    [
                        'name'  => 'admin_withdrawal_settings_edit',
                        'title' => 'Admin Withdrawal Settings Edit',
                    ],
                ]
            ],
            'member_withdrawal' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Withdrawal',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_withdrawal_list',
                        'title' => 'Member Withdrawal List',
                    ],
                    [
                        'name'  => 'member_withdrawal_create',
                        'title' => 'Member Withdrawal Create',
                    ],
                    [
                        'name'  => 'member_withdrawal_cancel',
                        'title' => 'Member Withdrawal Cancel',
                    ],
                    [
                        'name'  => 'member_withdrawal_export',
                        'title' => 'Member Withdrawal Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_withdrawal_list',
                'admin_withdrawal_edit',
                'admin_withdrawal_export',
                'admin_withdrawal_settings_list',
                'admin_withdrawal_settings_edit',
            ],
            Role::ADMIN => [
                'admin_withdrawal_list',
                'admin_withdrawal_edit',
                'admin_withdrawal_export',
                'admin_withdrawal_settings_list',
                'admin_withdrawal_settings_edit',
            ],
            Role::MEMBER => [
                'member_withdrawal_list',
                'member_withdrawal_create',
                'member_withdrawal_cancel',
                'member_withdrawal_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
