<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name')->index();
            $table->string('branch');
            $table->string('swift_code')->nullable()->index();
            $table->string('account_number')->index();
            $table->string('account_name')->index();
            $table->string('address')->nullable();
            $table->string('province')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('action_by')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('action_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
