<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdrawals', function (Blueprint $table) {
            $table->decimal('requested_amount', 20, 6)->after('amount')->default(0.000000);
            $table->decimal('receivable_destination_amount', 20, 6)->after('requested_amount')->default(0.000000);
            $table->renameColumn('converted_amount', 'converted_base_amount')->after('requested_amount')->change();
            $table->decimal('withdrawal_base_fee', 20, 6)->default(0.000000);
            $table->decimal('base_sell_rate', 12, 6)->default(0.000000);
            $table->decimal('base_buy_rate', 12, 6)->default(0.000000);
            $table->renameColumn('sell_rate', 'destination_sell_rate')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawals', function (Blueprint $table) {
            if (Schema::hasColumn('withdrawals', 'requested_amount')) {
                $table->dropColumn('requested_amount');
            }
            if (Schema::hasColumn('withdrawals', 'receivable_destination_amount')) {
                $table->dropColumn('receivable_amount');
            }
            if (Schema::hasColumn('withdrawals', 'base_sell_rate')) {
                $table->dropColumn('base_sell_rate');
            }
            if (Schema::hasColumn('withdrawals', 'base_buy_rate')) {
                $table->dropColumn('base_buy_rate');
            }
            if (Schema::hasColumn('withdrawals', 'withdrawal_base_fee')) {
                $table->dropColumn('withdrawal_base_fee');
            }
        });
    }
}
