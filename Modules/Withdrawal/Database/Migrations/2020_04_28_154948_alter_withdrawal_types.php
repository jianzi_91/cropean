<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWithdrawalTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdrawal_types', function (Blueprint $table) {
            $table->string('fee_type', 20)->after('fee_percentage')->default('exclusive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawal_types', function (Blueprint $table) {
            if (Schema::hasColumn('withdrawal_types', 'fee_type')) {
                $table->dropColumn('fee_type');
            }
        });
    }
}
