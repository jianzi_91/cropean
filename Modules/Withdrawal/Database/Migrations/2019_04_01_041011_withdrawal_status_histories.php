<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WithdrawalStatusHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('withdrawal_status_id')->index();
            $table->unsignedInteger('withdrawal_id')->index();
            $table->unsignedInteger('updated_by')->index()->nullable();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);

            // Constraints
            $table->foreign('withdrawal_id')->references('id')->on('withdrawals');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('withdrawal_status_id')->references('id')->on('withdrawal_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_status_histories');
    }
}
