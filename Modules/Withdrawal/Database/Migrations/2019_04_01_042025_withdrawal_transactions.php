<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WithdrawalTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->decimal('amount', 40, 20)->default(0.00000000000000000000);
            $table->string('transaction_code')->index()->unique();
            $table->unsignedInteger('withdrawal_id')->index();
            $table->unsignedInteger('bank_credit_type_id')->index()->nullable();
            $table->unsignedInteger('bank_transaction_type_id')->index();

            // Constraints
            $table->unique(['withdrawal_id', 'bank_credit_type_id', 'bank_transaction_type_id'], 'withdrawal_duplicate_constraint');
            $table->foreign('withdrawal_id')->references('id')->on('withdrawals');
            $table->foreign('bank_credit_type_id')->references('id')->on('bank_credit_types');
            $table->foreign('bank_transaction_type_id')->references('id')->on('bank_transaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_transactions');
    }
}
