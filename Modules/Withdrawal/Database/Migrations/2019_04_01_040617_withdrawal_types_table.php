<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WithdrawalTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->unique()->index();
            $table->string('name_translation')->nullable();
            $table->string('instruction')->nullable();
            $table->string('instruction_translation')->nullable();
            $table->decimal('fee_percentage', 20, 6)->default(0.000000);
            $table->decimal('minimum_amount', 20, 6)->default(0.00000000000000000000);
            $table->decimal('maximum_amount', 20, 6)->default(0.00000000000000000000);
            $table->decimal('multiples', 20, 6)->default(0.00000000000000000000);
            $table->boolean('is_active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_types');
    }
}
