<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Withdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('request_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('user_bank_id')->nullable()->index();
            $table->decimal('amount', 40, 20)->default(0.00000000000000000000);
            $table->decimal('converted_amount', 40, 20)->default(0.00000000000000000000);
            $table->string('currency', 10)->index();
            $table->decimal('sell_rate', 12, 6)->default(0.000000);
            $table->unsignedInteger('withdrawal_type_id')->index();
            $table->decimal('withdrawal_fee_percentage', 20, 6)->default(0.000000);
            $table->decimal('withdrawal_fee', 20, 6)->default(0.000000);
            $table->unsignedInteger('withdrawal_status_id')->nullable()->index('withdrawal_status_id');
            $table->text('remarks')->nullable();
            $table->unsignedInteger('updated_by')->nullable()->index('updated_by');

            // Constraints
            $table->foreign('withdrawal_type_id')->references('id')->on('withdrawal_types');
            $table->foreign('user_bank_id')->references('id')->on('withdrawal_banks');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('withdrawal_status_id')->references('id')->on('withdrawal_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawals');
    }
}
