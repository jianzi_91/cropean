<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Modules\Withdrawal\Contracts\WithdrawalStatusContract;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;

class Withdrawal extends Model implements Stateable
{
    use SoftDeletes, HistoryRelation;

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $fillable = [
        'request_id',
        'user_id',
        'user_bank_id',
        'amount',
        'requested_amount',
        'converted_base_amount', //converted to usd amount
        'receivable_destination_amount',
        'currency',
        'destination_sell_rate',
        'withdrawal_type_id',
        'withdrawal_fee_percentage',
        'withdrawal_status_id',
        'remarks',
        'updated_by',
        'withdrawal_base_fee',
        'base_sell_rate',
        'base_buy_rate'
    ];

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->belongsTo(WithdrawalBank::class, 'user_bank_id');
    }

    /**
     * This model's relation to withdrawal transaction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(WithdrawalTransaction::class, 'withdrawal_id');
    }

    /**
     * This model's relation to withdrawal status histories
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(WithdrawalStatusHistory::class, 'withdrawal_id');
    }

    /**
     * Getting model's current status
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function status()
    {
        return $this->hasOne(WithdrawalStatusHistory::class, 'withdrawal_id')->current(1);
    }

    /**
     * This model's relation to user bank
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(WithdrawalType::class, 'withdrawal_type_id');
    }

    /**
     * Approved status scope
     */
    public function scopeApproved($query)
    {
        $withdrawalStatusContract = resolve(WithdrawalStatusContract::class);
        $approvedStatus           = $withdrawalStatusContract->findBySlug(WithdrawalStatusContract::APPROVED);
        $paidStatus               = $withdrawalStatusContract->findBySlug(WithdrawalStatusContract::PAID);

        return $query->whereIn('withdrawal_status_id', [$approvedStatus->id, $paidStatus->id]);
    }

    /**
     * Approved status scope
     */
    public function scopePending($query)
    {
        $withdrawalStatusContract = resolve(WithdrawalStatusContract::class);
        $pendingStatus            = $withdrawalStatusContract->findBySlug(WithdrawalStatusContract::PENDING);

        return $query->where('withdrawal_status_id', $pendingStatus->id);
    }

    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'withdrawal_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return WithdrawalStatusHistory::class;
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($withdrawal) {
            $withdrawal->transactions()->get()->each(
                function ($transaction) {
                    $transaction->delete();
                }
            );

            $withdrawal->payments()->get()->each(
                function ($payment) {
                    $payment->delete();
                }
            );

            $withdrawal->statuses()->get()->each(
                function ($status) {
                    $status->delete();
                }
            );
        });
    }
}
