<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithdrawalStatusHistory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $fillable = [
        'withdrawal_status_id',
        'withdrawal_id',
        'updated_by',
        'remarks',
        'is_current',
    ];

    /**
     * This model's relation to withdrawals
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function withdrawal()
    {
        return $this->belongsTo(Withdrawal::class, 'withdrawal_id');
    }

    /**
     * This model's relation to withdrawal status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(WithdrawalStatus::class, 'withdrawal_status_id');
    }

    /**
     * Local scope for getting current status
     * @param unknown $query
     * @param unknown $current
     * @return unknown
     */
    public function scopeCurrent($query, $current)
    {
        return $query->where('is_current', $current);
    }
}
