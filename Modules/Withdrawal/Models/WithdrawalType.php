<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Modules\Translation\Traits\Translatable;

class WithdrawalType extends Model
{
    use SoftDeletes,Translatable,HasActiveScope;

    /**
     * The attributes that translatable.
     *
     * @var array
     */
    protected $translatableAttributes = ['name', 'instruction'];

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'instruction',
        'fee_percentage',
        'fee_type',
        'minimum_amount',
        'maximum_amount',
        'multiples',
        'is_active'
    ];

    /**
     * Get admin fee display attribute
     *
     * @param unknown $query
     * @return unknown
     */
    public function getFeeDisplayAttribute()
    {
        return bcmul($this->fee_percentage, 100, 2);
    }

    /**
     * Get status attribute
     *
     * @param unknown $query
     * @return unknown
     */
    public function getStatusAttribute()
    {
        $status = __('s_withdrawal_types.active');

        if (!$this->is_active) {
            $status = __('s_withdrawal_types.inactive');
        }

        return $status;
    }
}
