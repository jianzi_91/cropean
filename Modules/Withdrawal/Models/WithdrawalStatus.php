<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Scopes\HasActiveScope;

class WithdrawalStatus extends Model implements Stateable
{
    use Translatable, SoftDeletes, HasActiveScope, HasDropDown, HistoryRelation;

    /**
     * The translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_translation',
        'is_active',
    ];

    /**
     * This model's relation to withdrawal status history
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statusHistory()
    {
        return $this->hasMany(WithdrawalStatusHistory::class, 'withdrawal_status_id');
    }

    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'withdrawal_status_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return WithdrawalStatusHistory::class;
    }
}
