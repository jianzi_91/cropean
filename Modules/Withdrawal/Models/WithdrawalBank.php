<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Relations\UserRelation;
use Modules\Country\Models\Country;
use Modules\User\Models\User;

class WithdrawalBank extends Model
{
    use SoftDeletes, UserRelation, HasDropDown;

    protected $fillable = [
        'user_id',
        'name',
        'branch',
        'swift_code',
        'account_number',
        'account_name',
        'address',
        'province',
        'zipcode',
        'state',
        'city',
        'country_id',
        'action_by'
    ];

    /**
     * This model's relation to country.
     *
     * @return \Illuminate\Database\Eloquent\Concerns\HasRelationships
     */
    public function member()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * This model's relation to country.
     *
     * @return \Illuminate\Database\Eloquent\Concerns\HasRelationships
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
