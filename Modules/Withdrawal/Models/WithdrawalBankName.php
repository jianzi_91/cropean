<?php

namespace Modules\Withdrawal\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;

class WithdrawalBankName extends Model
{
    use Translatable, HasActiveScope, HasDropDown;

    protected $table = "withdrawal_bank_names";

    /**
     * The translatable columns
     *
     * @var array
     */
    protected $translatableAttributes = ['name'];

    /**
     * The attributes that are fillable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'name_translation',
        'is_active',
    ];
}
