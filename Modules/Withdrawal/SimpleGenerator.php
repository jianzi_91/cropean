<?php

namespace Modules\Withdrawal;

use Modules\Withdrawal\Contracts\RequestIdContract;

class SimpleGenerator implements RequestIdContract
{
    /**
     * Generate transaction code
     *
     * @param string $prefix
     */
    public function generate($prefix = 'REQ', $length = 20)
    {
        $code      = $prefix . str_replace('.', '', microtime(true));
        $strLength = strlen($code);
        if ($strLength < $length) {
            $start = str_pad(1, ($length - $strLength), 1);
            $limit = str_pad(9, ($length - $strLength), 9);

            if ($limit > mt_getrandmax()) {
                $start = 1;
                $limit = mt_getrandmax();
            }

            $code .= mt_rand(intval($start), intval($limit));
        }

        return substr($code, 0, $length);
    }
}
