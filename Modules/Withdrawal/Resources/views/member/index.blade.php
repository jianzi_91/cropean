@extends('templates.member.master')
@section('title', __('m_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.withdrawals')],
    ],
    'header'=>__('m_withdrawal_list.withdrawals')
])

@can('member_withdrawal_create')
    <div class="pb-2">
        {{ Form::formButtonPrimary('btn_create', __('m_withdrawal_list.new withdrawal request'), 'button') }}
    </div>
@endcan

@component('templates.__fragments.components.filter')
    <div class="col-xl-3 col-lg-6 col-12">
        {{ Form::formDateRange('created_at', request('created_at'), __('m_withdrawal_list.date')) }}
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        {{ Form::formText('reference_number', request('reference_number'), __('m_withdrawal_list.reference number'), [], false) }}
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        {{ Form::formText('bank_name', request('bank_name'), __('m_withdrawal_list.bank name'), [], false) }}
    </div>
    <div class="col-xl-3 col-lg-6 col-12">
        {{ Form::formSelect('status_id', system_withdrawal_statuses_dropdown(), request('status_id'), __('m_withdrawal_list.status'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('m_withdrawal_list.withdrawals') }}</h4>
            @can('member_withdrawal_export')
            {{ Form::formTabSecondary(__('m_withdrawal_list.export'), route('member.withdrawals.export',http_build_query(request()->except('page')))) }}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <th>{{ __('m_withdrawal_list.date')}}</th>
                <th>{{ __('m_withdrawal_list.reference number')}}</th>
                <th>{{ __('m_withdrawal_list.bank account name')}}</th>
                <th>{{ __('m_withdrawal_list.bank account number')}}</th>
                <th>{{ __('m_withdrawal_list.bank name')}}</th>
                <th>{{ __('m_withdrawal_list.bank province')}}</th>
                <th>{{ __('m_withdrawal_list.bank city')}}</th>
                <th>{{ __('m_withdrawal_list.bank branch')}}</th>
                <th>{{ __('m_withdrawal_list.withdrawal amount cash credits')}}</th>
                <th>{{ __('m_withdrawal_list.admin fee usd')}}</th>
                <th>{{ __('m_withdrawal_list.total converted amount usd')}}</th>
                <th>{{ __('m_withdrawal_list.total receivable amount cny')}}</th>
                <th>{{ __('m_withdrawal_list.status')}}</th>
                <th>{{ __('m_withdrawal_list.remarks')}}</th>
                <th>{{ __('m_withdrawal_list.actions')}}</th>
            </thead>
            <tbody>
            @forelse ($withdrawals as $withdrawal)
                <tr>
                    <td>{{ $withdrawal->created_at }}</td>
                    <td>{{ $withdrawal->request_id }}</td>
                    <td>{{ $withdrawal->account_name }}</td>
                    <td>{{ $withdrawal->account_number }}</td>
                    <td>{{ $withdrawal->bank_name }}</td>
                    <td>{{ $withdrawal->province }}</td>
                    <td>{{ $withdrawal->city }}</td>
                    <td>{{ $withdrawal->branch }}</td>
                    <td>{{ amount_format($withdrawal->amount, credit_precision()) }}</td>
                    <td>{{ amount_format($withdrawal->withdrawal_base_fee, credit_precision()) }}</td>
                    <td>{{ amount_format($withdrawal->converted_base_amount, credit_precision()) }}</td>
                    <td>{{ amount_format($withdrawal->receivable_destination_amount, credit_precision()) }}</td>
                    <td>{{ __('s_withdrawal_statuses.'.$withdrawal->status_name)  }}</td>
                    <td>{{ $withdrawal->remarks  }}</td>
                    <td>
                        @if($withdrawal->status_name == \Modules\Withdrawal\Contracts\WithdrawalStatusContract::PENDING)
                            <div class="d-flex">
                                @can('member_withdrawal_cancel')
                                    <div>
                                        <input type="hidden" value={{$withdrawal->id}} name="withdrawal_id" id="withdrawal_id" />
                                        <form action="{{ route('member.withdrawals.destroy', $withdrawal->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <a role="button" class="js-delete" href="#">
                                                <i class="bx bx-trash"></i>
                                            </a>
                                        </form>
                                    </div>
                                @endcan
                            </div>
                        @endif
                    </td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 16, 'text' => __('m_withdrawal_list.no records') ])
            @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        $('#btn_create').on('click', function(e){
            e.preventDefault();
            window.location.href = "{{ route('member.withdrawals.create') }}"
        })

        $('.js-delete').on('click',function(e){
            e.preventDefault();
            var el = this;

            swal.fire({
                title: '{{ ucfirst(__("m_withdrawal_list.do you want to cancel this withdrawal?")) }}',
                showCancelButton: true,
                cancelButtonText: '{{ __("m_withdrawal_list.no") }}',
                confirmButtonText: '{{ __("m_withdrawal_list.yes") }}',
                customClass: {
                    confirmButton: 'btn btn-primary cro-btn-primary',
                    cancelButton: 'btn btn-secondary cro-btn-secondary',
                },
                preConfirm: function(e){
                    $(el).closest('form').submit();
                }
            })
        })
    </script>
@endpush