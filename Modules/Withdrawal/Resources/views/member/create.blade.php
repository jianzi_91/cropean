@extends('templates.member.master')
@section('title', __('m_page_title.new withdrawal request'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.withdrawals')],
        ['name'=>__('s_breadcrumbs.new bank withdrawal request')],
    ],
    'header'=>__('m_withdrawal_create.new bank withdrawal request'),
    'backRoute'=> route('member.withdrawals.index'),
])
<div class="row">
  <div class="col-lg-8 col-xs-12 order-lg-1 order-2">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
            <div class="col-10">
              {{ Form::open(['method'=>'post', 'route'=>'member.withdrawals.store', 'id'=>'create-withdrawal', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
              {{ Form::formSelect('user_bank_id', user_banks_dropdown(), old('user_bank_id'), __('m_withdrawal_create.select bank'), ['info'=>"<a href=". route('member.withdrawals.banks.create') ." class='small-hyperlink'>" . __('m_withdrawal_create.create new bank') . "</a>"]) }}
{{--              {{ Form::formText('credit_type', 'cash credits', __('m_withdrawal_create.credit type'), ['readonly']) }}--}}
              <input type="hidden" id="bank_credit_type" name="bank_credit_type" value="{{ \Modules\Credit\Contracts\BankCreditTypeContract::CASH_CREDIT }}">
              {{ Form::formNumber('amount', '', __('m_withdrawal_create.amount to withdraw')) }}
{{--              {{ Form::formText('requested_amount', '', __('m_withdrawal_create.requested withdraw amount usd'), ['readonly']) }}--}}
              {{ Form::formText('admin_fee', '', __('m_withdrawal_create.admin fee inclusive usd'), ['readonly', 'info'=> __('m_withdrawal_create.admin fee percentage :adminFeePercentage', ['adminFeePercentage' => bcmul($withdrawalType->fee_percentage, 100, 2)]) . '%']) }}
              {{ Form::formText('total_converted', '', __('m_withdrawal_create.total converted amount usd'), ['readonly']) }}
              {{ Form::formText('exchange-rate-value', 0, __('m_withdrawal_create.exchange rate cny'), ['disabled']) }}
              {{ Form::formText('total_receivable', '', __('m_withdrawal_create.total receivable amount cny'), ['readonly']) }}
              {{ Form::formPassword('secondary_password', null, __('m_withdrawal_create.secondary password').' <span>*</span>', ['autocomplete'=>'new-password']) }}
              <div class="d-flex justify-content-end">
                <button id="btn_submit" type="submit" class="btn btn-primary cro-btn-primary">{{ __('m_withdrawal_create.submit') }}</button>
              </div>
            </div>
            <div class="col-2"></div>
        {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-xs-12 order-lg-2 order-1">
      @include('templates.__fragments.components.credit-widget-bg-image', [
          'label'=>__('m_withdrawal_create.cash credits balance'),
          'value'=>amount_format(auth()->user()->cash_credit),
          'backgroundClass'=>'widget-cash-image',
      ])
      {{-- <div class="card">
        <div>
          <div class="card-body">
            <div class="widget-label" id="exchange-rate-label">
              {{__('m_withdrawal_create.exchange rate cash:usd:cny')}} 
            </div>
            <div class="widget-value" id="exchange-rate-value"></div>
          </div>
        </div>
      </div> --}}
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

$(document).ready(function() {
  function getWithdrawalData(amount) {
    axios.post('{{ route('member.withdrawals.calculate') }}', {
        user_bank_id: $('#user_bank_id').val(),
        amount: amount,
      })
      .then(function (response) {
        // $('#requested_amount').val(response.data.data.requested_amount_in_usd);
        $('#admin_fee').val(response.data.data.admin_fee_in_usd);
        $('#total_converted').val(response.data.data.converted_amount_in_usd);
        $('#total_receivable').val(response.data.data.receivable_amount);
      });
  }

  function getExchangeRateData(user_bank_id, bank_credit_type) {
    axios.get('{{ route('member.withdrawals.exchange-rate') }}', {
        params: {
          user_bank_id: user_bank_id,
          bank_credit_type: bank_credit_type,
        }
      })
      .then(function (response) {
        $('#exchange-rate-value').val(response.data.data.destination_rate)
      });
  }

  $('#user_bank_id').on('change', function(e) {
    getWithdrawalData($('#amount').val());
    getExchangeRateData($('#user_bank_id').val(), $('#bank_credit_type').val())
  })

  $('#amount').on('keyup', function(){
    _.debounce(getWithdrawalData(this.value), 500)
  })

});
</script>
@endpush