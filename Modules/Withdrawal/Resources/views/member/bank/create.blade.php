@extends('templates.member.master')
@section('title', __('m_page_title.my banks'))

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.my banks'), 'route' => route('member.withdrawals.banks.index')],
        ['name'=>__('s_breadcrumb.new bank')]
    ]
])

<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            @include('templates.member.includes.profile_nav', ['page' => 'banks'])
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title p-0">{{ __('m_create_bank.new bank') }}</h4>
                        {{ Form::open(['method'=>'post', 'route'=>'member.withdrawals.banks.store', 'id'=>'create-member-bank', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        {{ Form::formText('account_name', auth()->user()->name, __('m_create_bank.bank account holder name'), ['readonly'=>true])}}
                        {{ Form::formText('account_number', old('account_number'), __('m_create_bank.bank account number').' <span>*</span>') }}
                        {{ Form::formSelect('name', withdrawal_bank_names_dropdown(), old('name'), __('m_create_bank.bank name').' <span>*</span>') }}
                        {{ Form::formText('province', old('province'), __('m_create_bank.bank province').' <span>*</span>') }}
                        {{ Form::formText('city', old('city'), __('m_create_bank.city').' <span>*</span>') }}
                        {{ Form::formText('branch', old('branch'), __('m_create_bank.bank branch').' <span>*</span>') }}
                        {{ Form::formPassword('secondary_password', old('secondary_password'), __('m_create_bank.secondary password').' <span>*</span>', ['autocomplete'=>'new-password']) }}
                        <div class="d-flex justify-content-end">
                            <button id="btn_submit" type="submit" class="btn btn-primary cro-btn-primary">{{ __('m_create_bank.submit') }}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>  
    </div>              
<section class="page-user-profile">

@endsection
