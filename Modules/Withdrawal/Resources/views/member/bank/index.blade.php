@extends('templates.member.master')
@section('title', __('m_page_title.my banks'))

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.my banks')]
    ]
])

<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            @include('templates.member.includes.profile_nav', ['page' => 'banks'])
        </div>
    </div>

    @can('member_withdrawal_bank_create')
        <div class="col-xs-12 col-lg-4">
            {{ Form::formButtonPrimary('btn_create', __('m_banks.add new bank'), 'button') }}
        </div>
        <br>
    @endcan

    @component('templates.__fragments.components.filter')
        <div class="col-xs-12 col-lg-4">
            {{ Form::formText('name', request('name'), __('m_banks.bank name'), [], false) }}
        </div>
        <div class="col-xs-12 col-lg-4">
            {{ Form::formText('account_number', request('account_number'), __('m_banks.bank account number'), [], false) }}
        </div>
    @endcomponent

    <div class="card">
        <div class="card-content">
            <div class="card-body d-flex justify-content-between align-items-center p-2">
                <h4 class="card-title filter-title p-0 m-0">{{ __('m_banks.my bank list') }}</h4>
                @can('member_bank_export')
                    {{ Form::formTabSecondary(__('m_withdrawal_list.export table')), route('member.banks.export', array_merge(request()->except('page'))) }}
                @endcan
            </div>
            @component('templates.__fragments.components.tables')
                <thead class="text-capitalize">
                    <tr>
                        <th>{{ __('m_banks.bank account name') }}</th>
                        <th>{{ __('m_banks.bank account number') }}</th>
                        <th>{{ __('m_banks.bank name') }}</th>
                        <th>{{ __('m_banks.bank province') }}</th>
                        <th>{{ __('m_banks.bank city') }}</th>
                        <th>{{ __('m_banks.bank branch') }}</th>
                        <th>{{ __('m_banks.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($banks as $bank)
                    <tr>
                        <td>{{ $bank->account_name }}</td>
                        <td>{{ $bank->account_number }}</td>
                        <td>{{ $bank->name }}</td>
                        <td>{{ $bank->province }}</td>
                        <td>{{ $bank->city }}</td>
                        <td>{{ $bank->branch }}</td>
                        <td class="action">
                            <div class="row">
                            @can('member_withdrawal_bank_edit')
                                <a href="{{ route('member.withdrawals.banks.edit', $bank->id) }}"><i class="bx bx-pencil"></i></a>
                            @endcan
                            @can('member_withdrawal_bank_delete')
                            <div>
                                <form action="{{ route('member.withdrawals.banks.destroy', $bank->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    &nbsp;&nbsp;
                                    <a href="#" class="js-delete">
                                        <i class="bx bx-trash"></i>
                                    </button>
                                </form>
                            </div>
                            @endcan
                            </div>
                        </td>
                    </tr>
                    @empty
                        @include('templates.__fragments.components.no-table-records', [ 'span' => 9, 'text' => __('m_banks.no records') ])
                    @endforelse
                </tbody>
            @endcomponent
        </div>
    </div>
    {!! $banks->render() !!}
</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#btn_create').on('click', function() {
            window.location.href = "{{ route('member.withdrawals.banks.create') }}";
        })

        $('.js-delete').on('click',function(e){
            e.preventDefault();
            var el = this;

            swal.fire({
                title: '{{ ucfirst(__("m_banks.do you want delete this bank")) }}',
                showCancelButton: true,
                cancelButtonText: '{{ __("m_banks.no") }}',
                confirmButtonText: '{{ __("m_banks.yes") }}',
                customClass: {
                    confirmButton: 'btn btn-primary cro-btn-primary',
                    cancelButton: 'btn btn-secondary cro-btn-secondary'
                },
                preConfirm: function(e){
                    $(el).closest('form').submit();
                }
            })
        })
    })
</script>
@endpush