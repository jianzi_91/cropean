@extends('templates.admin.master')
@section('title', __('a_page_title.bank withdrawal settings'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('s_breadcrumbs.bank withdrawal settings')],
    ],
    'header'=>__('a_withdrawal_settings.bank withdrawal settings')
])

<div class="row">
  <div class="col-xs-12 col-lg-6">
    @foreach($settings as $setting)
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          {{ Form::open(['method'=>'put', 'route'=> ['admin.withdrawals.withdrawal-settings.update', $setting->id], 'id'=>'form', 'class'=>'form-vertical -with-label']) }}

          {{ Form::formText('fee_percentage', bcmul($setting->fee_percentage, 100, 2) ?: old('fee_percentage'), __('a_withdrawal_settings.admin fee %')) }}
          {{ Form::formText('minimum_amount', amount_format($setting->minimum_amount, 2) ?: old('minimum_amount'), __('a_withdrawal_settings.minimum amount')) }}
          {{ Form::formText('multiples', amount_format($setting->multiples, 2) ?: old('multiples'), __('a_withdrawal_settings.multiples')) }}

          <div class="row d-flex justify-content-end">
            <button id="btn_submit" type="submit" class="btn cro-btn-primary btn-primary">{{ __('a_withdrawal_settings.submit') }}</button>
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>



@endsection