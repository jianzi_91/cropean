@extends('templates.admin.master')
@section('title', __('a_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('s_breadcrumbs.withdrawals')],
    ],
    'header'=>__('a_withdrawal_list.withdrawals')
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_withdrawal_list.date'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_withdrawal_list.email'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_withdrawal_list.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_roi.include downlines'), ['isChecked' => request('member_id_downlines', false)]) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('reference_number', request('reference_number'), __('a_withdrawal_list.reference number'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('bank_name', request('bank_name'), __('a_withdrawal_list.bank name'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('bank_account_number', request('bank_account_number'), __('a_withdrawal_list.bank account number'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formSelect('status_id', $statuses, request('status_id'), __('a_withdrawal_list.status'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formDateRange('updated_at', request('updated_at'), __('a_withdrawal_list.update date'), [], false) }}
</div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_withdrawal_list.withdrawals') }}</h4>
            <div class="d-flex flex-row align-items-center">

                <div class="table-total-results mr-2">{{ __('a_withdrawal_list.total results:') }} {{ $withdrawals->total() }}</div>
                @if ($isUpdatable)
                    {{ Form::formButtonPrimary('btn-bulk-update', __('a_withdrawal_list.update'), 'button', []) }}
                @endif
                &nbsp;&nbsp;&nbsp;
                @can('admin_withdrawal_export')
                {{ Form::formTabSecondary(__('a_withdrawal_list.export'), route('admin.withdrawals.export',http_build_query(request()->except('page')))) }}
                @endcan
                
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            @if ($isUpdatable)
            <td class="text-center">{{ Form::formCheckbox(null, 'select-all') }}</td>
            @endif
            <th>{{ __('a_withdrawal_list.actions')}}</th>
            <th>{{ __('a_withdrawal_list.date')}}</th>
            <th>{{ __('a_withdrawal_list.reference number')}}</th>
            <th>{{ __('a_withdrawal_list.member id')}}</th>
            <th>{{ __('a_withdrawal_list.email')}}</th>
            <th>{{ __('a_withdrawal_list.bank account name')}}</th>
            <th>{{ __('a_withdrawal_list.bank account number')}}</th>
            <th>{{ __('a_withdrawal_list.bank name')}}</th>
            <th>{{ __('a_withdrawal_list.bank province')}}</th>
            <th>{{ __('a_withdrawal_list.bank city')}}</th>
            <th>{{ __('a_withdrawal_list.bank branch')}}</th>
            <th>{{ __('a_withdrawal_list.withdrawal amount cash credits')}}</th>
            <th>{{ __('a_withdrawal_list.admin fee usd')}}</th>
            <th>{{ __('a_withdrawal_list.total converted amount usd')}}</th>
            <th>{{ __('a_withdrawal_list.total receivable amount cny')}}</th>
            <th>{{ __('a_withdrawal_list.status')}}</th>
            <th>{{ __('a_withdrawal_list.updated_at')}}</th>
            <th>{{ __('a_withdrawal_list.remarks')}}</th>
        </thead>
        <tbody>
        @forelse ($withdrawals as $withdrawal)
            <tr>
                @if ($isUpdatable)
                    <td class="text-center">{{ Form::formCheckbox('cbx-withdrawal', 'cbx-withdrawal-' . $withdrawal->id, null, null, ['class' => 'cbx-withdrawal', 'data-id' => $withdrawal->id]) }}</td>
                @endif
                <td>
                    @can('admin_withdrawal_edit')
                        @if ($withdrawal->status_name != \Modules\Withdrawal\Contracts\WithdrawalStatusContract::CANCELLED)
                            @if ($withdrawal->status_name == \Modules\Withdrawal\Contracts\WithdrawalStatusContract::PAID || $withdrawal->status_name == \Modules\Withdrawal\Contracts\WithdrawalStatusContract::REJECTED)
                                <a href="{{ route('admin.withdrawals.edit', $withdrawal->id) }}"><i class="bx bx-show-alt"></i></a>
                            @else
                                <a href="{{ route('admin.withdrawals.edit', $withdrawal->id) }}"><i class="bx bx-pencil"></i></a>
                            @endif
                        @endif
                    @endcan
                </td>
                <td>{{ $withdrawal->created_at }}</td>
                <td>{{ $withdrawal->request_id }}</td>
                <td>{{ $withdrawal->user->member_id }}</td>
                <td>{{ $withdrawal->email }}</td>
                <td>{{ $withdrawal->account_name }}</td>
                <td>{{ $withdrawal->account_number }}</td>
                <td>{{ $withdrawal->bank_name }}</td>
                <td>{{ $withdrawal->province }}</td>
                <td>{{ $withdrawal->city }}</td>
                <td>{{ $withdrawal->branch }}</td>
                <td>{{ amount_format($withdrawal->amount, credit_precision()) }}</td>
                <td>{{ amount_format($withdrawal->withdrawal_base_fee, credit_precision()) }}</td>
                <td>{{ amount_format($withdrawal->converted_base_amount, credit_precision()) }}</td>
                <td>{{ amount_format($withdrawal->receivable_destination_amount, credit_precision()) }}</td>
                <td>{{ __('s_withdrawal_statuses.'.$withdrawal->status_name) }}</td>
                <td>{{ $withdrawal->updated_at }}</td>
                <td>{{ $withdrawal->remarks }}</td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 17, 'text' => __('a_withdrawal_list.no records') ])
        @endforelse
        </tbody>
        @endcomponent
        {{ Form::open(['method'=>'put', 'route' => 'admin.withdrawals.update.bulk', 'id'=>'fm-bulk-update']) }}
        {{ Form::formHide('withdrawal_ids') }}
        {{ Form::formHide('remarks') }}
        {{ Form::formHide('withdrawal_status_id') }}
        {{ Form::close() }}
    </div>
</div>
{!! $withdrawals->render() !!}

@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        
        let updatableStatuses = '{!! $updatableStatuses !!}';
        let updatableStatusOptions = $.parseJSON(updatableStatuses);

        $("#select-all").click(function() {
            if ($(this).prop("checked")) {
                $('.cbx-withdrawal').attr('checked', 'checked');
            } else {
                $('.cbx-withdrawal').removeAttr('checked');
            }
            
        });

        $("#btn-bulk-update").click(function() {
            let withdrawal_ids = [];

            $.each(($('.cbx-withdrawal:checked')), function() {
                withdrawal_ids.push($(this).attr('data-id'));
            });

            if (withdrawal_ids.length === 0) {
                swal.fire({
                    type: 'error',
                    text: 'No withdrawal selected',
                })
                return;
            }

            $('#withdrawal_ids').val(withdrawal_ids);
            swal.fire({
                title: "{{ __('a_withdrawal_list.bulk update') }}",
                input: 'select',
                inputOptions: updatableStatusOptions,
                html: '<div class="form-group"><textarea class="form-control " id="sw-remarks" cols="50" rows="10" placeholder="Remarks" style="resize: none;"></textarea></div>',
                inputPlaceholder: "{{ __('a_withdrawal_list.please select a status') }}",
                showCancelButton: true,
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                            let remarks = $('#sw-remarks').val();
                        if (value) {
                            if (value == 5 && remarks == "") {
                                resolve("{{ __('a_withdrawal_list.remarks is required for rejecting withdrawals') }}");
                                return;
                            }

                            $('#btn-bulk-update').attr('disabled','disabled');
                            
                            $('#remarks').val(remarks);
                            $('#withdrawal_status_id').val(value);

                            resolve();
                            $('#fm-bulk-update').submit();
                        } else {
                            resolve("{{ __('a_withdrawal_list.please select a status') }}")
                        }
                    })
                }
            })
        });
    });
</script>
@endpush