<thead>
<tr>
    <!-- table header -->
    <th>{{ __('a_banks.username') }}</th>
    <th>{{ __('a_banks.bank name') }}</th>
    <th>{{ __('a_banks.bank branch') }}</th>
    <th>{{ __('a_banks.swift code') }}</th>
    <th>{{ __('a_banks.bank account number') }}</th>
    <th>{{ __('a_banks.bank account name') }}</th>
    <th>{{ __('a_banks.bank address') }}</th>
    <th>{{ __('a_banks.bank state') }}</th>
    <th>{{ __('a_banks.bank city') }}</th>
    <th>{{ __('a_banks.action') }}</th>
</tr>
</thead>
</br>
@if (count($banks))
    <tbody>
    <!-- If there's data, render the below section per row -->
    <!-- START -->
    @foreach ($banks as $bank)
        <tr>
            <td>{{ $bank->username }}</td>
            <td>{{ $bank->name }}</td>
            <td>{{ $bank->branch }}</td>
            <td>{{ $bank->swift_code }}</td>
            <td>{{ $bank->account_number }}</td>
            <td>{{ $bank->account_name }}</td>
            <td>{{ $bank->address }}</td>
            <td>{{ $bank->state }}</td>
            <td>{{ $bank->city }}</td>
            <td>
                @can('admin_bank_delete')
                    <a class="btn btn-gold btn-sm btn-round mr-1 js-action_delete" data-id="{{ $bank->id }}">{{ __('a_banks.delete') }}</a>
                @endcan
                @can('admin_bank_edit')
                    <a class="btn btn-gold btn-sm btn-round" href="{{ route('admin.banks.edit', $bank->id) }}">{{ __('a_banks.edit') }}</a>
                @endcan
            </td>
        </tr>
    @endforeach
    <!-- END -->
    </tbody>
@else
@endif