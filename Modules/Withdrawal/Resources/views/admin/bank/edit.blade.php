{{ Form::open(['method'=>'put', 'route'=> ['admin.banks.update', $bank->id], 'id'=>'update-member-bank', 'class'=>'form-vertical -with-label']) }}
{{ Form::formText('account_name', ($bank->account_name) ?: old('account_name'), __('a_edit_bank.bank account name').' <span>*</span>') }}
{{ Form::formText('account_number', ($bank->account_number) ?: old('account_number'), __('a_edit_bank.bank account number').' <span>*</span>') }}
{{ Form::formText('name', ($bank->name) ?: old('bank_name'), __('a_edit_bank.bank name').' <span>*</span>') }}
{{ Form::formText('branch', ($bank->branch) ?: old('branch'), __('a_edit_bank.bank branch').' <span>*</span>') }}
{{ Form::formText('province', ($bank->province) ?: old('province'), __('a_edit_bank.bank province').' <span>*</span>') }}
{{ Form::formText('address', ($bank->address) ?:old('address'), __('a_edit_bank.address').' <span>*</span>') }}
{{ Form::formText('swift_code', ($bank->swift_code) ?:old('swift_code'), __('a_edit_bank.swift_code').' <span>*</span>') }}
{{ Form::formText('state',($bank->state) ?: old('state'), __('a_edit_bank.state').' <span>*</span>') }}
{{ Form::formText('city',($bank->city) ?: old('city'), __('a_edit_bank.city').' <span>*</span>') }}
{{ Form::formPassword('secondary_password', old('secondary_password'), __('a_edit_bank.secondary password').' <span>*</span>', ['autocomplete'=>'new-password']) }}
<button type="submit" class="btn btn-gold prevent-spamming">{{ __('a_edit_bank.submit') }}</button>
{{ Form::close() }}