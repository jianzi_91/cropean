{{ Form::open(['method'=>'post', 'route'=>'admin.banks.store', 'id'=>'create-admin-bank', 'class'=>'form-vertical -with-label']) }}
{{ Form::formText('member_id', old('member_id'), __('a_create_bank.member id').' <span>*</span>') }}
{{ Form::formText('account_name', old('account_name'), __('a_create_bank.full name').' <span>*</span>') }}
{{ Form::formText('account_number', old('account_number'), __('a_create_bank.account number').' <span>*</span>') }}
{{ Form::formText('name', old('name'), __('a_create_bank.bank name').' <span>*</span>') }}
{{ Form::formText('branch', old('branch'), __('a_create_bank.bank branch').' <span>*</span>') }}
{{ Form::formText('province', old('province'), __('a_create_bank.bank province').' <span>*</span>') }}
{{ Form::formText('address', old('address'), __('a_create_bank.address').' <span>*</span>') }}
{{ Form::formText('swift_code', old('swift_code'), __('a_create_bank.swift_code').' <span>*</span>') }}
{{ Form::formText('state', old('state'), __('a_create_bank.state').' <span>*</span>') }}
{{ Form::formText('city', old('city'), __('a_create_bank.city').' <span>*</span>') }}
{{ Form::formPassword('secondary_password', old('secondary_password'), __('a_create_bank.secondary password').' <span>*</span>', ['autocomplete'=>'new-password']) }}
<button type="submit" class="btn btn-gold prevent-spamming">{{ __('a_create_bank.submit') }}</button>
{{ Form::close() }}