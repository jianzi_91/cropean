@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'breadcrumbs' => [
        ['name' => __('a_member_management_profile.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_profile.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_profile.bank settings')]
    ],
    'header'=>__('a_member_management_profile.bank settings'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'bank', 'uid' => $user->id ])
<br />

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-4">
        {{ Form::formText('name', request('name'), __('m_banks.bank name'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-4">
        {{ Form::formText('account_number', request('account_number'), __('a_member_management_profile.bank account number'), [], false) }}
    </div>
@endcomponent

@can('admin_user_member_management_bank_create')
    <div class="col-xs-12 col-lg-4">
        {{ Form::formButtonPrimary('btn_create', __('a_member_management_profile.add new bank'), 'button') }}
    </div>
    <br>
@endcan

<div class="card">
    <div class="card-content">
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <th>{{ __('a_member_management_bank.bank account name') }}</th>
            <th>{{ __('a_member_management_bank.bank account number') }}</th>
            <th>{{ __('a_member_management_bank.bank name') }}</th>
            <th>{{ __('a_member_management_bank.bank province') }}</th>
            <th>{{ __('a_member_management_bank.bank city') }}</th>
            <th>{{ __('a_member_management_bank.bank branch') }}</th>
            <th>{{ __('a_member_management_bank.action') }}</th>
        </thead>
        <tbody>
            @forelse($banks as $bank)
                <tr>
                    <td>{{ $bank->account_name }}</td>
                    <td>{{ $bank->account_number }}</td>
                    <td>{{ $bank->name }}</td>
                    <td>{{ $bank->province }}</td>
                    <td>{{ $bank->city }}</td>
                    <td>{{ $bank->branch }}</td>
                    
                    <td class="action">
                        <div class="row">
                        @can('admin_user_member_management_bank_edit')
                            <a href="{{ route('admin.member-management.bank.edit', ['id' => $user->id, 'bank' => $bank->id]) }}"><i class="bx bx-pencil"></i></a>
                        @endcan
                        @can('admin_user_member_management_bank_delete')
                        <div>
                            <form action="{{ route('admin.member-management.bank.destroy', ['id' => $user->id, 'bank' => $bank->id]) }}" method="POST">
                                {{ csrf_field() }}
                                @method('DELETE')
                                &nbsp;&nbsp;
                                <a href="#" class="js-delete">
                                    <i class="bx bx-trash"></i>
                                </button>
                            </form>
                        </div>
                        @endcan
                        </div>
                    </td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_member_management_bank.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $banks->render() !!}
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#btn_create').on('click', function() {
            window.location.href = "{{ route('admin.member-management.bank.create', $user->id) }}";
        });

        $('.js-delete').on('click',function(e){
            e.preventDefault();
            var el = this;

            swal.fire({
                title: '{{ ucfirst(__("a_banks.do you want delete this bank")) }}',
                showCancelButton: true,
                cancelButtonText: '{{ __("a_banks.no") }}',
                confirmButtonText: '{{ __("a_banks.yes") }}',
                customClass: {
                    confirmButton: 'btn btn-primary cro-btn-primary',
                    cancelButton: 'btn btn-secondary cro-btn-secondary'
                },
                preConfirm: function(e){
                    $(el).closest('form').submit();
                }
            })
        })
    })
</script>
@endpush