@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',
        [
        'breadcrumbs' => [
            ['name' => __('a_member_management_create_bank.dashboard'), 'route' => route('admin.dashboard')],
            ['name' => __('a_member_management_create_bank.member management'), 'route' => route('admin.management.members.index')],
            ['name' => __('a_member_management_create_bank.bank settings'), 'route' => route('admin.member-management.bank.index', $user->id)],
            ['name' => __('a_member_management_create_bank.create bank')]
        ],
        'header'=>__('a_member_management_create_bank.create bank'),
        'backRoute'=>route('admin.member-management.bank.index', $user->id),
    ])

    @include('templates.admin.includes._mm-nav', ['page' => 'bank', 'uid' => $user->id])
    <br />

    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        {{ Form::open(['method'=>'post', 'route'=>['admin.member-management.bank.store', $user->id], 'id'=>'create-member-bank', 'class'=>'form-vertical -with-label']) }}
                        {{ Form::formText('account_name', $user->name, __('a_member_management_create_bank.bank account holder name'), ['readonly'=>true])}}
                        {{ Form::formText('account_number', old('account_number'), __('a_member_management_create_bank.bank account number').' <span>*</span>') }}
                        {{ Form::formSelect('name', withdrawal_bank_names_dropdown(), old('name'), __('a_member_management_create_bank.bank name').' <span>*</span>') }}
                        {{ Form::formText('province', old('province'), __('a_member_management_create_bank.bank province').' <span>*</span>') }}
                        {{ Form::formText('city', old('city'), __('a_member_management_create_bank.bank city').' <span>*</span>') }}
                        {{ Form::formText('branch', old('branch'), __('a_member_management_create_bank.bank branch').' <span>*</span>') }}
                        <div class="d-flex justify-content-end">
                            <button id="btn_submit" type="submit" class="btn btn-primary cro-btn-primary">{{ __('a_member_management_create_bank.submit') }}</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection