@extends('templates.admin.master')

@section('content')
    <h1>Admin Create Bank Page</h1>

    <p>
        This view is loaded from module: {!! config('bank.name') !!}
    </p>

@stop
