@extends('templates.admin.master')
{{-- @extends('bank::layouts.master') --}}

@section('content')
    <h1>Admin List of banks Page</h1>

    <p>
        This view is loaded from module: {!! config('bank.name') !!}
    </p>
@stop
