@extends('templates.admin.master')

@section('content')
    <h1>Admin Edit Bank Page</h1>

    <p>
        This view is loaded from module: {!! config('bank.name') !!}
    </p>

@stop
