@extends('templates.admin.master')
@section('title', __('a_page_title.withdrawals'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('s_breadcrumbs.withdrawals'), 'route'=>route('admin.withdrawals.index')],
        ['name'=>__('s_breadcrumbs.view withdrawal details')],
    ],
    'header'=>__('a_view_withdrawal.view withdrawal details'),
    'backRoute'=>route('admin.withdrawals.index')
])

<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['method'=>'put', 'route'=> ['admin.withdrawals.update', $withdrawal->id], 'id'=>'edit-withdrawal', 'class'=>'form-vertical -with-label', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formText('date', $withdrawal->created_at, __('a_view_withdrawal.date'), ['readonly']) }}
                    {{ Form::formText('member_id', $withdrawal->user->member_id, __('a_view_withdrawal.member id'), ['readonly']) }}
                    {{ Form::formText('email', $withdrawal->user->email, __('a_view_withdrawal.email'), ['readonly']) }}
                    {{ Form::formText('reference_number', $withdrawal->request_id, __('a_view_withdrawal.reference number'), ['readonly']) }}
                    {{ Form::formText('bank_account_name', $withdrawal->bank->account_name, __('a_view_withdrawal.bank account name'), ['readonly']) }}
                    {{ Form::formText('bank_account_number', $withdrawal->bank->account_number, __('a_view_withdrawal.bank account number'), ['readonly']) }}
                    {{ Form::formText('bank_name', $withdrawal->bank->name, __('a_view_withdrawal.bank name'), ['readonly']) }}
                    {{ Form::formText('bank_province', $withdrawal->bank->province, __('a_view_withdrawal.bank province'), ['readonly']) }}
                    {{ Form::formText('bank_city', $withdrawal->bank->city, __('a_view_withdrawal.bank city'), ['readonly']) }}
                    {{ Form::formText('bank_branch', $withdrawal->bank->branch, __('a_view_withdrawal.bank branch'), ['readonly']) }}
                    {{ Form::formText('withdrawal_amount_cash_credits', amount_format($withdrawal->amount) . ' ' . __('a_view_withdrawal.usd'), __('a_view_withdrawal.withdrawal amount cash credits'), ['readonly']) }}
                    {{ Form::formText('admin_fee', amount_format($withdrawal->withdrawal_fee) . ' ' . __('a_view_withdrawal.usd'), __('a_view_withdrawal.admin fee usd'), ['readonly']) }}
                    {{ Form::formText('total_converted_usd', amount_format($withdrawal->converted_base_amount) . ' ' . __('a_view_withdrawal.usd'), __('a_view_withdrawal.total converted amount usd'), ['readonly']) }}
                    {{ Form::formText('exchange_rate_value', bcdiv($withdrawal->receivable_destination_amount, $withdrawal->converted_base_amount, 2), __('a_view_withdrawal.exchange rate cny'), ['disabled']) }}
                    {{ Form::formText('total_receivable_amount_cny', amount_format($withdrawal->receivable_destination_amount) . ' ' . __('a_view_withdrawal.cny'), __('a_view_withdrawal.total receivable amount cny'), ['readonly']) }}
                    {{ Form::formText('remarks', $withdrawal->remarks, __('a_view_withdrawal.remarks')) }}
                    @if(!in_array($withdrawal->status->status->rawname, [Modules\Withdrawal\Contracts\WithdrawalStatusContract::REJECTED, Modules\Withdrawal\Contracts\WithdrawalStatusContract::PAID]))
                        {{ Form::formSelect('withdrawal_status_id', system_withdrawal_statuses_dropdown(['cancelled']), $withdrawal->status->status->id, __('a_view_withdrawal.status')) }}
                        <div class="row d-flex justify-content-end">
                            <button id="btn_submit" type="submit" class="btn cro-btn-primary btn-primary">{{ __('a_view_withdrawal.submit') }}</button>
                        </div>
                    @endif
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4"></div>
</div>

@endsection
