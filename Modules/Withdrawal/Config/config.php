<?php

return [
    'name'     => 'Withdrawal',
    'bindings' => [
        Modules\Withdrawal\Contracts\WithdrawalBankContract::class   => Modules\Withdrawal\Repositories\WithdrawalBankRepository::class,
        Modules\Withdrawal\Contracts\WithdrawalContract::class       => Modules\Withdrawal\Repositories\WithdrawalRepository::class,
        Modules\Withdrawal\Contracts\WithdrawalStatusContract::class => Modules\Withdrawal\Repositories\WithdrawalStatusRepository::class,
        Modules\Withdrawal\Contracts\WithdrawalTypeContract::class   => Modules\Withdrawal\Repositories\WithdrawalTypeRepository::class,
    ],

    /*
     |--------------------------------------------------------------------------
     | Request ID Generator
     |--------------------------------------------------------------------------
     |
     | Here you can configure the setting when generating request ID
     |
     */
    'request_id' => [
        'generator' => 'Modules\Withdrawal\SimpleGenerator',
        'length'    => 18,
        'prefix'    => 'WDL',
    ],

    /*
     |--------------------------------------------------------------------------
     | Transaction Types
     |--------------------------------------------------------------------------
     |
     | Here are the default withdrawal transaction types.
     | When initialized, these transactions will be created bank transaction types.
     | You are free to change the values, but the keys are fixed
     |
     */
    'transaction_types' => [
        'withdrawal_request'    => 'Withdrawal Request',
        'withdrawal_refund'     => 'Withdrawal Refund',
        'withdrawal_fee'        => 'Withdrawal Fee',
        'withdrawal_fee_refund' => 'Withdrawal Fee Refund',
        'withdrawal_cancel'     => 'Withdrawal Cancelled',
    ],

    /*
     |--------------------------------------------------------------------------
     | Statuses
     |--------------------------------------------------------------------------
     |
     | Here you may pre define the default statuses.
     | These will be seeded when initialized.
     |
     */
    'statuses' => [
        'pending'    => 'Pending',
        'processing' => 'Processing',
        'approved'   => 'Approved',
        'paid'       => 'Paid',
        'rejected'   => 'Rejected',
        'cancelled'  => 'Cancelled',
    ],

    /*
     |--------------------------------------------------------------------------
     | Withdrawal types
     |--------------------------------------------------------------------------
     |
     | Here you may pre define the withdrawal types.
     | These will be seeded when initialized.
     |
     */
    'types' => [
        [
            'bank' => [
                'fee_percentage' => 0.10,
                'minimum_amount' => 300,
                'maximum_amount' => 0,
                'multiples'      => 50,
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Withdrawal
    |--------------------------------------------------------------------------
    |
    | Here you can configure the withdrawal settings
    |
    */
    'withdrawal' => [
        'max' => 999999999999,
    ]

];
