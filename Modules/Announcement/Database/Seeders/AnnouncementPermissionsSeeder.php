<?php

namespace Modules\Announcement\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class AnnouncementPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);
        $abilitySections         = [
            'admin_announcement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Announcement',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_announcement_list',
                        'title' => 'Admin Announcement List',
                    ],
                    [
                        'name'  => 'admin_announcement_create',
                        'title' => 'Admin Create Announcement',
                    ],
                    [
                        'name'  => 'admin_announcement_edit',
                        'title' => 'Admin Edit Announcement',
                    ],
                    [
                        'name'  => 'admin_announcement_delete',
                        'title' => 'Admin Delete Announcement',
                    ],
                    [
                        'name'  => 'admin_announcement_attachment_show',
                        'title' => 'Admin Show Announcement Attachment',
                    ],
                ]
            ],
            'member_announcement' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Announcement',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_announcement_list',
                        'title' => 'Member Announcement List',
                    ],
                    [
                        'name'  => 'member_announcement_show',
                        'title' => 'Member Show Announcement',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_announcement_list',
                'admin_announcement_create',
                'admin_announcement_edit',
                'admin_announcement_delete',
                'admin_announcement_attachment_show',
            ],
            Role::ADMIN => [
                'admin_announcement_list',
                'admin_announcement_create',
                'admin_announcement_edit',
                'admin_announcement_delete',
                'admin_announcement_attachment_show',
            ],
            Role::MEMBER => [
                'member_announcement_list',
                'member_announcement_show',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
