<?php

namespace Modules\Announcement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Announcement\Models\Video;

class WelcomeMessageVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            'name'             => 'CEO Welcome Message',
            'name_translation' => 'CEO Welcome Message',
            'filename'         => 'CEO_Welcome_Message',
            'ext'              => 'mp4',
        ]);
    }
}
