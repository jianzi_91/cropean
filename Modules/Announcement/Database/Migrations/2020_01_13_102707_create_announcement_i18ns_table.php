<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementI18nsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_i18ns', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('announcement_id')->unsigned();
            $table->integer('translator_language_id')->unsigned();
            $table->string('title', 255)->collation('utf8mb4_unicode_ci');
            $table->text('body')->collation('utf8mb4_unicode_ci');

            // foreign key
            $table->foreign('announcement_id')->references('id')->on('announcements');
            $table->foreign('translator_language_id')->references('id')->on('translator_languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_i18ns');
    }
}
