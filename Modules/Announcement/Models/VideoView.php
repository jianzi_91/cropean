<?php

namespace Modules\Announcement\Models;

use Illuminate\Database\Eloquent\Model;

class VideoView extends Model
{
    protected $table   = "video_views";
    protected $guarded = [];
}
