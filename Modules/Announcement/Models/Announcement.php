<?php

namespace Modules\Announcement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Modules\Translation\Traits\Translatable;

class Announcement extends Model
{
    use SoftDeletes, HasActiveScope;

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'valid_from',
        'valid_to',
        'is_popup',
        'is_active'
    ];

    /**
     * This model's relation to attachments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(AnnouncementAttachment::class, 'announcement_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(Announcementi18n::class, 'announcement_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($announcement) {
            $announcement->attachments()->get()->each(
                    function ($attachment) {
                        $attachment->delete();
                    }
                );
        });
    }
}
