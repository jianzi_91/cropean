<?php

namespace Modules\Announcement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Models\TranslatorLanguage;

class Announcementi18n extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'announcement_id',
        'translator_language_id',
        'title',
        'body',
    ];

    protected $table = 'announcement_i18ns';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcement()
    {
        return $this->belongsTo(Announcement::class, 'announcement_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(TranslatorLanguage::class, 'translator_language_id');
    }
}
