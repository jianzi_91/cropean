<?php

namespace Modules\Announcement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnnouncementAttachment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'locale',
        'filename',
        'original_filename',
        'mime_type',
        'path',
        'announcement_id',
    ];

    /**
     * This model's relation to announcement
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function announcement()
    {
        return $this->belongsTo(Announcement::class, 'announcement_id');
    }

    /**
     * This model's relation to language
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'locale', 'locale');
    }
}
