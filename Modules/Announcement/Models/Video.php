<?php

namespace Modules\Announcement\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Translation\Traits\Translatable;

class Video extends Model
{
    use Translatable;

    protected $translatableAttributes = ['name'];
    protected $table   = "videos";
    protected $guarded = [];

    public function views()
    {
        return $this->hasMany(VideoView::class, 'video_id');
    }
}
