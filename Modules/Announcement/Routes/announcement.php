<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::get('/announcements/attachments/{id}', ['uses' => 'Admin\AnnouncementAttachmentController@show'])->name('admin.announcements.attachments.show');
        Route::resource('announcements', 'Admin\AnnouncementController', ['as' => 'admin'])->except(['show']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('/announcements/attachments/{id}', ['uses' => 'Member\AnnouncementAttachmentController@show'])->name('member.announcements.attachments.show');
        Route::resource('announcements', 'Member\AnnouncementController', ['as' => 'member'])->only(['index', 'show']);

        Route::resource('videos', 'Member\VideoController', ['as' => 'member'])->only('show', 'update');
    });
});
