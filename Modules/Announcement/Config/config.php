<?php

return [
    'name'     => 'Announcement',
    'bindings' => [
        Modules\Announcement\Contracts\AnnouncementContract::class             => Modules\Announcement\Repositories\AnnouncementRepository::class,
        Modules\Announcement\Contracts\AnnouncementAttachmentContract::class   => Modules\Announcement\Repositories\AnnouncementAttachmentRepository::class,
        \Modules\Announcement\Contracts\AnnouncementTranslationContract::class => \Modules\Announcement\Repositories\AnnouncementTranslationRepository::class,
    ],

    /*
     |--------------------------------------------------------------------------
     | Default locale
     |--------------------------------------------------------------------------
     |
     | Here you can configure the default locale.
     | If no translation found, the default locale will be returned
     |
     */
    'default_locale' => 'en',

    'title_max_size' => 250, // num of characters

    'body_max_size' => 5000, // num of characters

    /*
     |--------------------------------------------------------------------------
     | Attachment settings
     |--------------------------------------------------------------------------
     |
     | Here you can configure the attachments
     |
     */
    'attachments' => [

        'mimes' => [
            '.gif'  => 'image/gif',
            '.ico'  => 'image/x-icon',
            '.jpg'  => 'image/jpeg',
            '.png'  => 'image/png',
            '.doc'  => 'application/msword',
            '.pdf'  => 'application/pdf',
            '.docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            '.mp4'  => 'video/mp4',
            '.mov'  => 'video/quicktime',
            '.avi'  => 'video/avi',
        ],

        'max_size' => 20000 // in kilo bytes
    ],

    /*
     |--------------------------------------------------------------------------
     | Attachment storage location
     |--------------------------------------------------------------------------
     |
     | Here you can configure where to store the attachments
     |
     */
    'storage' => 'announcements',
];
