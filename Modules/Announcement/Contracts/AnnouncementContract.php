<?php

namespace Modules\Announcement\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface AnnouncementContract extends CrudContract, ActiveContract, SlugContract
{
}
