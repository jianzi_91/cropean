<?php

namespace Modules\Announcement\Contracts;

use Illuminate\Http\UploadedFile;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface AnnouncementAttachmentContract extends CrudContract
{
    /**
     * Attach a file
     * @param unknown $announcementId
     * @param unknown $locale
     * @param unknown $filename
     * @param unknown $path
     * @param string $move
     * @return boolean
     */
    public function attach($announcementId, $locale, $filename, $path, $move = false);

    /**
     * Attach file using byte contents
     * @param unknown $announcementId
     * @param unknown $locale
     * @param unknown $filename
     * @param unknown $mime
     * @param unknown $contents
     * @return boolean
     */
    public function attachContents($announcementId, $locale, $filename, $mime, $contents);

    /**
     * Attach file using Uploaded Object
     * @param unknown $announcementId
     * @param unknown $locale
     * @param UploadedFile $file
     * @param string $move
     * @return boolean
     */
    public function attachUploadedFile($announcementId, $locale, UploadedFile $file, $move = false);

    /**
     * Get attachment as byte contents
     * @param unknown $attachmentId
     * @param string $download
     * @return string bytes
     */
    public function get($attachmentId, $download = false);

    /**
     * Get attachments
     * @param unknown $announcementId
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function getAttachments($announcementId, array $with = [], $select = ['*']);

    /**
     * Delete attachment
     * @param unknown $attachmentId
     * @return integer
     */
    public function delete($attachmentId);

    /**
     * Delete announcement attachments
     * @param unknown $requestId
     * @param unknow $locale
     * @return integer
     */
    public function deleteAnnouncementAttachments($announcementId, $locale = null);

    /**
     * Delete all attachments
     * @return integer
     */
    public function deleteAll();
}
