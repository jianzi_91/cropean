<?php

namespace Modules\Announcement\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface AnnouncementTranslationContract extends CrudContract
{
}
