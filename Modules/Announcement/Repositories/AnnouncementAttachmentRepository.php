<?php

namespace Modules\Announcement\Repositories;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;
use Modules\Announcement\Models\AnnouncementAttachment;
use Modules\Announcement\Repositories\Concerns\ImageSizes;
use Modules\Core\Exceptions\MaxFileSize;
use Modules\Core\Exceptions\UnsupportedMimeType;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class AnnouncementAttachmentRepository extends Repository implements AnnouncementAttachmentContract
{
    use ImageSizes,HasCrud;

    /**
     * The file system class
     *
     * @var unknown
     */
    protected $fileSystem;

    /**
     * Class constructor
     *
     * @param AnnouncementAttachment $model
     */
    public function __construct(
        AnnouncementAttachment $model
    ) {
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::attach()
     */
    public function attach($announcementId, $locale, $filename, $path, $move = false)
    {
        $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
        $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));

        return $this->attachUploadedFile($announcementId, $locale, $upload);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::attachContents()
     */
    public function attachContents($announcementId, $locale, $filename, $mime, $contents)
    {
        $this->validateByteString($contents, $mime);

        $request = $this->model->findOrFail($announcementId);

        $path                          = config('announcement.storage');
        $attachment                    = new $this->model;
        $randomFilename                = md5(str_replace('.', '', Str::random(5) . microtime(true)));
        $randomFilename                = $randomFilename . '.' . $mime;
        $attachment->locale            = $locale;
        $attachment->filename          = $randomFilename;
        $attachment->original_filename = $filename;
        $attachment->path              = $path;
        $attachment->mime_type         = $mime;
        $attachment->announcement_id   = $announcementId;

        if ($attachment->save()) {
            $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $path;
            $this->checkDirectory($fullpath);

            if (config('filesystems.cloud_enable')) {
                Storage::cloud()->put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);
            }

            Storage::put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);

            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::attachUploadedFile()
     */
    public function attachUploadedFile($announcementId, $locale, UploadedFile $file, $move = false)
    {
        $this->validateFile($file);

        $path                          = config('announcement.storage');
        $attachment                    = new $this->model;
        $randomFilename                = md5(str_replace('.', '', Str::random(5) . microtime(true)));
        $randomFilename                = $randomFilename . '.' . $file->extension();
        $attachment->locale            = $locale;
        $attachment->filename          = $randomFilename;
        $attachment->original_filename = $file->getClientOriginalName();
        $attachment->path              = $path;
        $attachment->mime_type         = $file->getMimeType();
        $attachment->announcement_id   = $announcementId;
        if ($attachment->save()) {
            $this->saveFile($file, $attachment);
            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::get()
     */
    public function get($attachmentId, $download = false)
    {
        $attachment = $this->model->findOrFail($attachmentId);

        if (config('filesystems.cloud_enable')) {
            return $this->getFileFromSpace($attachment, $download);
        }

        return $this->getFileFromLocal($attachment, $download);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::getAttachments()
     */
    public function getAttachments($announcementId, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->where('announcement_id', $announcementId);

        return $prepare->with($with)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::delete()
     */
    public function delete($attachmentId)
    {
        $attachment = $this->model->find($attachmentId);

        if ($attachment) {
            $this->deleteFile($attachment);

            return $attachment->delete();
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::deleteAnnouncementAttachment()
     */
    public function deleteAnnouncementAttachments($announcementId, $locale = null)
    {
        $prepare = $this->model->where('announcement_id', $announcementId);

        if ($locale) {
            $prepare->where('locale', $locale);
        }

        $attachments = $prepare->get();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Announcement\Contracts\AnnouncementAttachmentContract::deleteAll()
     */
    public function deleteAll()
    {
        $attachments = $this->all();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     * Get file from digital ocean
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromSpace($attachment, $download)
    {
        $path     = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
        $contents = Storage::cloud()->get($path)->header(
            'Content-Type',
            $attachment->mime_type
        );

        if ($download) {
            echo $contents;
            exit;
        }
        return $contents;
    }

    /**
     * Get file from local
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromLocal($attachment, $download)
    {
        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     * Validate file
     *
     * @param unknown $filename
     * @param unknown $path
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateFile($filename, $path = null)
    {
        $config = config('announcement.attachments');

        if ($filename instanceof UploadedFile) {
            $mime = $filename->getClientMimeType();
            $size = $filename->getSize();
        } else {
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
            $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
            $mime     = $upload->getClientMimeType();
            $size     = $upload->getSize();
        }

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Validate byte strings
     *
     * @param unknown $contents
     * @param unknown $mime
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateByteString($contents, $mime)
    {
        $config = config('announcement.attachments');
        $size   = strlen($contents);

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Check directory
     *
     * @param unknown $path
     * @return boolean
     */
    protected function checkDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path, 0777, true);
        }

        return false;
    }
}
