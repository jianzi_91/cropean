<?php

namespace Modules\Announcement\Repositories;

use Modules\Announcement\Contracts\AnnouncementTranslationContract;
use Modules\Announcement\Models\Announcementi18n;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class AnnouncementTranslationRepository extends Repository implements AnnouncementTranslationContract
{
    use HasCrud;

    protected $languageContract;

    /**
     * AnnouncementTranslationRepository constructor.
     * @param Announcementi18n|null $model
     * @param TranslatorLanguageContract $languageContract
     */
    public function __construct(Announcementi18n $model, TranslatorLanguageContract $languageContract)
    {
        $this->slug             = 'title';
        $this->languageContract = $languageContract;

        parent::__construct($model);
    }

    /**
     * @param array $attributes
     * @param bool $forceFill
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function add(array $attributes = [], $forceFill = false)
    {
        $defaultLocale = config('announcement.default_locale');
        $titles        = isset($attributes['title']) ? $attributes['title']  : null;
        $bodies        = isset($attributes['body']) ? $attributes['body']  : null;

        if (!isset($titles[$defaultLocale]) && !isset($bodies[$defaultLocale])) {
            throw new \Exception('Title and body are required for default locale');
        }

        foreach ($titles as $locale => $value) {
            $translateAttributes = [
                'announcement_id' => $attributes['announcement_id'],
                'title'           => $value,
                'body'            => $attributes['body'][$locale],
            ];
            $translateAttributes['translator_language_id'] = $this->languageContract->findBySlug($locale)->id;
            $model                                         = $this->model->newInstance($translateAttributes);
            $model->save();
        }

        return false;
    }

    /**
     * @param \Plus65\Base\Repositories\Contracts\unknown $id
     * @param array $attributes
     * @param bool $forceFill
     * @return bool|\Plus65\Base\Repositories\Contracts\Illuminate\Database\Eloquent\Model|void
     */
    public function edit($id, array $attributes = [], $forceFill = false)
    {
        $titles = isset($attributes['title']) ? $attributes['title'] : null;

        foreach ($titles as $locale => $title) {
            $model               = $this->model->where('announcement_id', $id)->where('translator_language_id', $this->languageContract->findBySlug($locale)->id)->first();
            $translateAttributes = [
                'title' => $title,
                'body'  => $attributes['body'][$locale],
            ];
            $model->fill($translateAttributes)->save();
        }
    }

    /**
     * @param \Plus65\Base\Repositories\Contracts\unknown $ids
     * @return int|void
     */
    public function delete($ids)
    {
        $this->model->where('announcement_id', $ids)->delete();
    }
}
