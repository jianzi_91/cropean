<?php

namespace Modules\Announcement\Repositories;

use Modules\Announcement\Contracts\AnnouncementContract;
use Modules\Announcement\Models\Announcement;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Modules\Translation\Contracts\TranslatorTranslationContract;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class AnnouncementRepository extends Repository implements AnnouncementContract
{
    use HasCrud, HasActive, HasSlug;

    /**
     * The translator translation repository
     *
     * @var unknown
     */
    protected $translatorTranslationRepository;

    /**
     * The translator language repository
     *
     * @var unknown
     */
    protected $translatorLanguageRepository;

    /**
     * Class constructor
     *
     * @param Announcement $model
     * @param TranslatorTranslationContract $translatorTranslationContract
     * @param TranslatorLanguageContract $translatorLanguageContract
     */
    public function __construct(
        Announcement $model,
        TranslatorTranslationContract $translatorTranslationContract,
        TranslatorLanguageContract $translatorLanguageContract
    ) {
        $this->slug = 'title';

        $this->translatorTranslationRepository = $translatorTranslationContract;
        $this->translatorLanguageRepository    = $translatorLanguageContract;

        parent::__construct($model);
    }

    /**
     * Add a new record
     *
     * @param array $attributes
     * @return bool|\Illuminate\Database\Eloquent\Model|\Plus65\Base\Repositories\unknown
     * @throws \Exception
     */
    public function add(array $attributes = [], $forceFill = false)
    {
        $defaultLocale = config('announcement.default_locale');
        $titles        = isset($attributes['title']) ? $attributes['title']  : null;
        $bodies        = isset($attributes['body']) ? $attributes['body']  : null;

        if (!isset($titles[$defaultLocale]) && !isset($bodies[$defaultLocale])) {
            throw new \Exception('Title and body are required for default locale');
        }

        $model = $this->model->newInstance($attributes);

        if ($model->save()) {
            return $model;
        }

        return false;
    }
}
