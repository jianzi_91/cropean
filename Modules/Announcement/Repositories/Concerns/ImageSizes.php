<?php

namespace Modules\Announcement\Repositories\Concerns;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait ImageSizes
{
    /**
     * Save different sizes
     *
     * @param UploadedFile $file
     * @param Model $attachment
     * @param string $visibility
     */
    public function saveFile(UploadedFile $file, Model $attachment, $visibility = 'private')
    {
        $type = explode('/', $attachment->mime_type)[0];
        foreach ($this->getDimensions() as $size) {

            // No sizes for non images
            if ($type != 'image' && $size != 'original') {
                continue;
            }

            if ($size == 'original') {
                // These files should be placed as private?
                if (config('filesystems.cloud_enable')) {
                    Storage::cloud()->putFileAs($attachment->path, $file, $attachment->filename);
                }

                $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
                if (!file_exists($fullpath)) {
                    Storage::putFileAs($attachment->path, $file, $attachment->filename);
                }
            } else {
                if ($template = $this->getTemplate($size)) {
                    $filename     = $size . '_' . $attachment->filename;
                    $relativePath = $attachment->path . DIRECTORY_SEPARATOR . $filename;
                    $fullpath     = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $relativePath;
                    Image::make($file)->filter($template)->save($fullpath);

                    if (config('filesystems.cloud_enable')) {
                        Storage::cloud()->put($relativePath, file_get_contents($fullpath), $visibility);
                    }

                    if ($visibility == 'public') {
                        Storage::disk('public')->put($relativePath, file_get_contents($fullpath));
                        @unlink($fullpath);
                    }
                }
            }
        }
    }

    /**
     * Delete files from storages
     *
     * @param Model $attachment
     */
    public function deleteFile(Model $attachment)
    {
        $cloudFiles = [];
        $localFiles = [];
        $type       = explode('/', $attachment->mime_type)[0];
        foreach ($this->getDimensions() as $size) {

            // No sizes for non images
            if ($type != 'image' && $size != 'original') {
                continue;
            }

            $localFiles[] = $size == 'original'
                            ? $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename
                            : $attachment->path . DIRECTORY_SEPARATOR . $size . '_' . $attachment->filename;

            if (config('filesystems.cloud_enable')) {
                $cloudFiles[] = $size == 'original'
                        ? $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename
                        : $attachment->path . DIRECTORY_SEPARATOR . $size . '_' . $attachment->filename;
            }
        }

        Storage::delete($localFiles);

        if ($cloudFiles) {
            Storage::cloud()->delete($cloudFiles);
        }
    }

    /**
     * Get different sizes
     *
     * @return string[]
     */
    public function getDimensions()
    {
        return array_merge(['original'], array_keys(config('imagecache.templates')));
    }

    /**
     * Returns corresponding template object from given template name
     *
     * @param  string $template
     * @return mixed
     */
    private function getTemplate($template)
    {
        $template = config("imagecache.templates.{$template}");

        switch (true) {
            // closure template found
            case is_callable($template):
                return $template;

                // filter template found
            case class_exists($template):
                return new $template;

            default:
                return;
        }
    }
}
