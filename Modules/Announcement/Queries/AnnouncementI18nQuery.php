<?php

namespace Modules\Announcement\Queries;

use Modules\Announcement\Models\Announcementi18n;
use QueryBuilder\QueryBuilder;

class AnnouncementI18nQuery extends QueryBuilder
{
    protected $locale = 'cn';

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\ QueryBuilder::query()
     */
    public function query()
    {
        $languageId = get_translator_language($this->locale)->id;

        return Announcementi18n::where('translator_language_id', $languageId)
            ->with('announcement', 'announcement.attachments')
            ->whereNull('announcement_i18ns.deleted_at')
            ->orderBy('announcement_i18ns.created_at', 'desc');
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->builder = $this->query();
        return $this;
    }
}
