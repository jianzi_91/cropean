<?php

namespace Modules\Announcement\Queries\Member;

use Modules\Announcement\Queries\AnnouncementQuery as BaseQuery;

class AnnouncementQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'announcements',
            'column' => 'created_at'
        ],
        'search' => [
            'filter'    => 'search_announcement',
            'table'     => 'announcements',
            'namespace' => 'Modules\Announcement\Queries\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->active();
    }
}
