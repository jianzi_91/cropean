<?php

namespace Modules\Announcement\Queries;

use Modules\Announcement\Models\Announcement;
use QueryBuilder\QueryBuilder;

class AnnouncementQuery extends QueryBuilder
{
    protected $locale = 'cn';

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $languageId = get_translator_language($this->locale)->id;

        return Announcement::with('attachments')
            ->join('announcement_i18ns', function ($join) use ($languageId) {
                $join->on('announcement_i18ns.announcement_id', 'announcements.id')
                    ->where('announcement_i18ns.translator_language_id', $languageId);
            })
            ->whereNull('announcements.created_at')
            ->whereNull('announcement_i18ns.created_at')
            ->select([
                'announcements.id',
                'announcements.created_at',
                'announcement_i18ns.title',
                'announcement_i18ns.body',
            ])
            ->orderBy('announcements.created_at', 'desc');
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->builder = $this->query();
        return $this;
    }
}
