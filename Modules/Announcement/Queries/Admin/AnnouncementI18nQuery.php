<?php

namespace Modules\Announcement\Queries\Admin;

use Modules\Announcement\Queries\AnnouncementI18nQuery as BaseQuery;

class AnnouncementI18nQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'announcement_i18ns',
            'column' => 'created_at'
        ],
        'search' => [
            'filter'    => 'search_announcement',
            'table'     => 'announcement_i18ns',
            'namespace' => 'Modules\Announcement\Queries\Filters',
        ],
    ];
}
