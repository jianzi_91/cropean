@extends('templates.admin.master')
@section('title', __('a_page_title.announcements'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
    ['name'=>__('s_breadcrumb.announcements'), 'route' => route('admin.announcements.index')],
    ['name'=>__('s_breadcrumb.edit announcement')]
],
'header'=>__('a_edit_announcment_title.edit announcement'),
'backRoute'=>route('admin.announcements.index'),
])

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="card-box">
                        {{ Form::open(['route' => ['admin.announcements.update', $announcement->id], 'method'=>'put', 'id'=>'edit-announcement', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        <div class="row">
                            @foreach($announcement->translations as $key => $translate)
                            <div class="col-sm-6">
                                {{ Form::formText("title[{$translate->language->code}]", $translate->title, __('a_edit_announcement.announcement title'). ' ('. $translate->language->code .')', ['required'=>true], '', true) }}
                                {{-- @if($errors->has("title.{$translate->language->code}"))
                                <p class="help-block">{{ $errors->first("title.{$translate->language->code}") }}</p>
                                @endif --}}

                                {{ Form::formTextArea("body[{$translate->language->code}]", $translate->body, __('a_edit_announcement.announcement message'). ' ('. $translate->language->code .')', ['required'=>true]) }}
                                {{-- @if($errors->has("body.{$translate->language->code}"))
                                <p class="help-block">{{ $errors->first("body.{$translate->language->code}") }}</p>
                                @endif --}}

                                {{ Form::formFileUpload("attachments[{$translate->language->code}]", '', __('a_edit_announcement.attachments').' ('.$translate->language->code.')', __('a_edit_announcement_file_uploader_placeholder.choose file'), ['accept'=>".jpeg,.pdf,.jpg,.gif,.png",'id'=>'attachments-'.$translate->language->code, 'dotId' =>'attachments.'.$translate->language->code]) }}
                                @if ($attachment = $announcement->attachments->where('locale', $translate->language->code)->first())
                                <div class="current-attachment">
                                    <a href="{{ route('admin.announcements.attachments.show', $attachment->id) }}" target="_blank">{{ $attachment->original_filename }}</a>
                                </div>
                                @endif
                                {{-- @if($errors->has("attachments.{$translate->language->code}"))
                                <p class="help-block">{{ $errors->first("attachments.{$translate->language->code}") }}</p>
                                @endif --}}
                            </div>
                            @endforeach
                        </div>
                        <div class="row mt-3">
                            <div class="col btn-container">
                                {{ Form::formButtonPrimary('btn_submit', __('a_edit_announcements.proceed'), 'submit') }}
                                &nbsp;&nbsp;
                                {{ Form::formButtonSecondary('btn_cancel', __('a_edit_announcements.cancel'), 'button') }}
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#btn_cancel').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("admin.announcements.index") }}'
    })
})
</script>
@endpush
