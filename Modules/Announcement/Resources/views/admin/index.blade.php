@extends('templates.admin.master')
@section('title', __('a_page_title.announcements'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.announcements')]
    ], 
    'header' => __('a_announcements.announcements')
])

<div class="pb-2">
    @can('admin_announcement_create')
        {{ Form::formButtonPrimary('btn_new', __('a_announcements.add new announcement'), 'button') }}
    @endcan
</div>

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_announcements.date'), array(), false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('search', request('search'), __('a_announcements.title'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_announcements.announcement list')}}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_announcements.date') }}</th>
                <th>{{ __('a_announcements.title') }}</th>
                <th>{{ __('a_announcements.message') }}</th>
                @can('admin_announcement_attachment_show')
                <th>{{ __('a_announcements.attachment') }}</th>
                @endcan
                <th>{{ __('a_announcements.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse($announcements as $announcement)
            <tr>
                <td>{{ $announcement->created_at }}</td>
                <td>{{ $announcement->title }}</td>
                <td>{!! nl2br($announcement->body) !!}</td>
                @can('admin_announcement_attachment_show')
                <td>
                    @if($announcement->announcement)
                        @if ($attachment = $announcement->announcement->attachments->where('locale', app()->getLocale())->first())
                        <a href="{{ route('admin.announcements.attachments.show', $attachment->id) }}">
                            <i class="bx bx-download"></i>
                        </a>
                        @endif
                    @endif
                </td>
                @endcan
                <td class="action">
                    <div class="d-flex">
                        @can('admin_announcement_edit')
                        <a href="{{ route('admin.announcements.edit', $announcement->announcement->id) }}" title="{{ __('a_announcements.edit') }}">
                            <i class="bx bx-pencil"></i>
                        </a>
                        @endcan
                        &nbsp;&nbsp;
                        @can('admin_announcement_delete')
                        <div>
                            <form action="{{ route('admin.announcements.destroy', $announcement->announcement->id) }}" method="POST">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <a role="button" class="js-delete" href="#">
                                    <i class="bx bx-trash"></i>
                                </a>
                            </form>
                        </div>
                        @endcan
                    </div>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 5, 'text' => __('a_announcements.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $announcements->render() !!}
@endsection


@push('scripts')
<script>
$(function() {
    $('#btn_new').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("admin.announcements.create") }}'
    })
})
$(function () {
    $('.js-delete').on('click',function(e){
        e.preventDefault();
        var el = this;

        swal.fire({
            title: '{{ ucfirst(__("a_announcements.do you want to delete this announcement?")) }}',
            showCancelButton: true,
            cancelButtonText: '{{ __("a_announcements.no") }}',
            confirmButtonText: '{{ __("a_announcements.yes") }}',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-primary'
            },
            preConfirm: function(e){
                $(el).closest('form').submit();
            }
        })
    })
})
</script>
@endpush