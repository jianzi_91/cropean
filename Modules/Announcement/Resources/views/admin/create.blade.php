@extends('templates.admin.master')
@section('title', __('a_page_title.announcements'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.announcements'), 'route' => route('admin.announcements.index')],
        ['name'=>__('s_breadcrumb.add new announcement')],
        ],
    'header'=> __('a_announcement.add new announcement'),
    'backRoute'=>route('admin.announcements.index'),
])

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                @can('admin_announcement_create')
                {{ Form::open(['route' => 'admin.announcements.store', 'method'=>'post', 'id'=>'create-announcement', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                <div class="row">
                    @foreach(languages() as $code => $language)
                    <div class="col-sm-6">
                        {{ Form::formText("title[{$code}]", old("title[{$code}]"), __('a_announcement.announcement title'). ' ('. $code . ')', ['errorkey'=>"title.{$code}", 'required'=>true], '', true) }}
                        {{ Form::formTextArea("body[{$code}]", old("body[{$code}]"), __('a_announcement.announcement message'). ' ('. $code . ')', ['errorkey'=>"body.{$code}", 'required'=>true]) }}
                        {{ Form::formFileUpload("attachments[{$code}]", '', __('a_create_announcement.attachments').' ('.$code.')', __('a_create_announcement_file_uploader_placeholder.choose file'), ['accept'=>".jpeg,.pdf,.jpg,.gif,.png,.mp4,.avi,.mov", 'id'=>'attachments-'.$code, 'dotId' => 'attachments.'.$code]) }}
                    </div>
                    @endforeach
                </div>
                <div class="row mt-3">
                    <div class="col btn-container">
                        {{ Form::formButtonPrimary('btn_submit', __('a_create_announcement.create'), 'submit') }}
                        &nbsp;&nbsp;
                        {{ Form::formButtonSecondary('btn_cancel', __('a_create_announcement.cancel'), 'button') }}
                    </div>
                </div>
                {{ Form::close() }}
                @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#btn_cancel').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("admin.announcements.index") }}'
    })
})
</script>
@endpush
