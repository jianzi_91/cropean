@extends('templates.member.master')
@section('title', __('m_page_title.announcements'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.announcements')]
    ], 
    'header' => __('m_announcements.announcements')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formDateRange('created_at', request('created_at'),__('m_announcements.date')) }}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
        {{ Form::formText('search', request('search'),__('m_announcements.title'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_announcements.announcement list')}}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_announcements.date') }}</th>
                <th>{{ __('m_announcements.title') }}</th>
                <th>{{ __('m_announcements.message') }}</th>
                @can('member_announcement_show')
                <th>{{ __('m_announcements.action') }}</th>
                @endcan
            </tr>
        </thead>
        <tbody>
        @forelse ($announcements as $announcement)
                <tr>
                    <td>{{ $announcement->created_at }}</td>
                    <td>{{ $announcement->title }}</td>
                    <td>{!! nl2br($announcement->body) !!}</td>
                    @can('member_announcement_show')
                    <td class="action">
                        <a href="javascript:void(0);" onclick="setValueToModal({{$announcement}})" data-toggle="modal" data-target="#announcementModal"><i class="bx bx-show-alt"></i></a>
                    </td>
                    @endcan
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 4, 'text' => __('m_announcements.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $announcements->render() !!}

<!-- Modal -->
<div class="modal fade" id="announcementModal" tabindex="-1" role="dialog" aria-labelledby="announcement"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                <h5 class="modal-title"></h5>
                <div class="created-at"></div>
                    </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            @can('member_announcement_show')
            <div class="modal-footer">
                <div class="attachment"></div>
            </div>
            @endcan
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">
    function setValueToModal(announcementData) {
        $('.modal-title').text(announcementData.title);
        $('.modal-body').html(announcementData.body);
        $('.modal-header .created-at').text(announcementData.created_at);

        var attachments = announcementData.announcement.attachments;

        if (attachments.length > 0) {
            var currentLocale = '{{ app()->getLocale() }}';
            var attachmentId = 0;
            var attachmentName = '';

            Object.entries(attachments).forEach(function (entry) {
                let value = entry[1];
                if (value.locale == currentLocale) {
                    attachmentId = value.id;
                    attachmentName = value.original_filename;
                }
            });

            var attachmentTxt = '{{ __("m_announcement.attachment") }}';

            if (attachmentId) {
                link = '{{ route("member.announcements.attachments.show", ":id") }}';
                link = link.replace(':id', attachmentId);
                $('.modal-footer').show().find('.attachment').html(
                    attachmentTxt + ' : <a href="' + link + '" target="_blank">' + attachmentName +
                    '</a>'
                );
            }
        } else {
            $('.modal-footer').hide().find('attachment').html('');
        }

        $('#announcementModal').modal()
    }
</script>
@endpush
