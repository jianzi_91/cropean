@extends('templates.member.master')
@section('title', __('m_page_title.announcements'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.announcements')]
    ]])

    <div class="card-box">
        <h4 class="mt-0 mb-3 header-title">{{ __('m_announcements.announcement') }}</h4>

    </div>
@endsection
