<?php

namespace Modules\Announcement\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title.*'       => 'required|max:' . config('announcement.title_max_size'),
            'body.*'        => 'required|max:' . config('announcement.body_max_size'),
            'attachments.*' => 'file|mimes:pdf,gif,jpg,png,jpeg|max:' . config('announcement.attachments.max_size')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Custom error validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title.*'       => __('a_create_announcement.announcement title'),
            'body.*'        => __('a_create_announcement.announcement body'),
            'attachments.*' => __('a_create_announcement.attachments')
        ];
    }
}
