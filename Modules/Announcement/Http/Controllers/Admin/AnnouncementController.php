<?php

namespace Modules\Announcement\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;
use Modules\Announcement\Contracts\AnnouncementContract;
use Modules\Announcement\Contracts\AnnouncementTranslationContract;
use Modules\Announcement\Http\Requests\Admin as AnnouncementRequest;
use Modules\Announcement\Queries\Admin\AnnouncementI18nQuery;
use Modules\Announcement\Queries\Admin\AnnouncementQuery;
use Plus65\Utility\Exceptions\WebException;

class AnnouncementController extends Controller
{
    /**
     * The announcement repository
     *
     * @var unknown
     */
    protected $announcementRepository;

    /**
     * The announcement attachment repository
     *
     * @var unknown
     */
    protected $attachmentRepository;

    /**
     * The announcement query
     *
     * @var unknown
     */
    protected $announcementQuery;

    /**
     * Announcement translation repository.
     *
     * @var AnnouncementTranslationContract
     */
    protected $announcementTranslationRepository;

    /**
     * Query.
     *
     * @var AnnouncementI18nQuery
     */
    protected $announcement18nQuery;

    /**
     * Class constructor
     *
     * @param AnnouncementContract $announcementContract
     * @param AnnouncementAttachmentContract $attachmentContract
     * @param AnnouncementQuery $announcementQuery
     * @param AnnouncementTranslationContract $translateContract
     * @param AnnouncementI18nQuery $announcement18nQuery
     */
    public function __construct(
        AnnouncementContract $announcementContract,
        AnnouncementAttachmentContract $attachmentContract,
        AnnouncementQuery $announcementQuery,
        AnnouncementTranslationContract $translateContract,
        AnnouncementI18nQuery $announcement18nQuery
    ) {
        $this->middleware('permission:admin_announcement_list')->only('index');
        $this->middleware('permission:admin_announcement_create')->only('create', 'store');
        $this->middleware('permission:admin_announcement_edit')->only('edit', 'update');
        $this->middleware('permission:admin_announcement_delete')->only('destroy');

        $this->announcementRepository            = $announcementContract;
        $this->attachmentRepository              = $attachmentContract;
        $this->announcementQuery                 = $announcementQuery;
        $this->announcementTranslationRepository = $translateContract;
        $this->announcement18nQuery              = $announcement18nQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        $announcements = $this->announcement18nQuery
            ->setLocale(session()->get('locale'))
            ->setParameters($request->all())
            ->paginate();

        return view('announcement::admin.index')->with(compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create()
    {
        return view('announcement::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function store(AnnouncementRequest\Store $request)
    {
        DB::beginTransaction();

        try {
            $attributes               = $request->except('attachments');
            $attributes['valid_from'] = Carbon::now()->format('Y-m-d');
            $attributes['valid_to']   = Carbon::now()->format('Y-m-d');
            $attributes['is_active']  = 1;
            $attributes['is_popup']   = $request->has('is_popup');
            $announcement             = $this->announcementRepository->add($attributes);
            if ($announcement) {
                if ($request->hasFile('attachments')) {
                    $attachments = $request->file('attachments');
                    foreach ($attachments as $locale => $attachment) {
                        $this->attachmentRepository->attachUploadedFile($announcement->id, $locale, $attachment, true);
                    }
                }

                // insert into announcement i18n
                $attributes['announcement_id'] = $announcement->id;
                $this->announcementTranslationRepository->add($attributes);
            }

            DB::commit();

            return redirect()->route('admin.announcements.index')->with('success', __('a_announcements.announcement successfully added'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.announcements.create'))->withMessage(__('a_announcements.failed to create announcement'));
        }
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(AnnouncementRequest\Show $request, $id)
    {
        if (!$announcement = $this->announcementRepository->find($id, ['attachments'])) {
            return back()->with('error', __('a_announcements.announcement not found'));
        }

        return view('announcement::admin.show')->with(compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(AnnouncementRequest\Edit $request, $id)
    {
        if (!$announcement = $this->announcementRepository->find($id, ['attachments', 'translations', 'translations.language'])) {
            return back()->with('error', __('a_announcements.announcement not found'));
        }

        return view('announcement::admin.edit')->with(compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function update(AnnouncementRequest\Update $request, $id)
    {
        if (!$announcement = $this->announcementRepository->find($id, ['attachments'])) {
            return back()->with('error', __('a_announcements.announcement not found'));
        }

        DB::beginTransaction();
        try {
            $attributes               = $request->except('attachments');
            $attributes['valid_from'] = Carbon::now()->format('Y-m-d');
            $attributes['valid_to']   = Carbon::now()->format('Y-m-d');
            $attributes['is_active']  = 1;
            $attributes['is_popup']   = $request->has('is_popup');

            if ($this->announcementRepository->edit($announcement->id, $attributes)) {
                if ($request->hasFile('attachments')) {
                    $attachments = $request->file('attachments');
                    foreach ($attachments as $locale => $attachment) {
                        $this->attachmentRepository->deleteAnnouncementAttachments($announcement->id, $locale);
                        $this->attachmentRepository->attachUploadedFile($announcement->id, $locale, $attachment, true);
                    }
                }

                // edit announcement translate
                $this->announcementTranslationRepository->edit($announcement->id, $attributes);
            }

            DB::commit();

            return redirect()->route('admin.announcements.index')->with('success', __('a_announcements.announcement successfully updated'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.announcements.edit', $id))->withMessage(__('a_announcements.failed to update announcement'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws WebException
     */
    public function destroy(AnnouncementRequest\Destroy $request, $id)
    {
        if (!$announcement = $this->announcementRepository->find($id, ['attachments'])) {
            return back()->with('error', __('a_announcements.announcement not found'));
        }

        DB::beginTransaction();
        try {
            $this->attachmentRepository->deleteAnnouncementAttachments($announcement->id);
            $this->announcementRepository->delete($announcement->id);
            $this->announcementTranslationRepository->delete($announcement->id);
            DB::commit();
            return redirect()->route('admin.announcements.index')->with('success', __('a_announcements.announcement successfully deleted'));
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.announcements.index'))->withMessage(__('a_announcements.failed to delete announcement'));
        }
    }
}
