<?php

namespace Modules\Announcement\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;
use Modules\Announcement\Contracts\AnnouncementContract;
use Modules\Announcement\Http\Requests\Member as AnnouncementRequest;
use Modules\Announcement\Queries\Member\AnnouncementI18nQuery;
use Modules\Announcement\Queries\Member\AnnouncementQuery;

class AnnouncementController extends Controller
{
    /**
     * The announcement repository
     *
     * @var unknown
     */
    protected $announcementRepository;

    /**
     * The announcement attachment repository
     *
     * @var unknown
     */
    protected $attachmentRepository;

    /**
     * The announcement query
     *
     * @var unknown
     */
    protected $announcementQuery;
    protected $announcementi18nQuery;

    /**
     * Class constructor
     *
     * @param AnnouncementContract $announcementContract
     * @param AnnouncementAttachmentContract $attachmentContract
     * @param AnnouncementQuery $announcementQuery
     * @param AnnouncementI18nQuery $announcementI18nQuery
     */
    public function __construct(
        AnnouncementContract $announcementContract,
        AnnouncementAttachmentContract $attachmentContract,
        AnnouncementQuery $announcementQuery,
        AnnouncementI18nQuery $announcementI18nQuery
    ) {
        $this->middleware('permission:member_announcement_list')->only('index');
        $this->middleware('permission:member_announcement_show')->only('show');

        $this->announcementRepository = $announcementContract;
        $this->attachmentRepository   = $attachmentContract;
        $this->announcementQuery      = $announcementQuery;
        $this->announcementi18nQuery  = $announcementI18nQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        $announcements = $this->announcementi18nQuery
            ->setLocale(session()->get('locale'))
            ->setParameters($request->all())
            ->paginate();

        return view('announcement::member.index')->with(compact('announcements'));
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show(AnnouncementRequest\Show $request, $id)
    {
        if (!$announcement = $this->announcementRepository->find($id, ['attachments'])) {
            return back()->with('error', __('m_announcements.announcement not found'));
        }

        return view('announcement::member.show')->with(compact('announcement'));
    }
}
