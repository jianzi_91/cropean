<?php

namespace Modules\Announcement\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;

class AnnouncementAttachmentController extends Controller
{
    /**
     * The announcement attachment repository
     *
     * @var unknown
     */
    protected $attachmentRepository;

    /**
     * Class constructor
     *
     * @param AnnouncementAttachmentContract $attachment
     */
    public function __construct(AnnouncementAttachmentContract $attachmentContract)
    {
        $this->middleware('permission:member_announcement_show')->only('show');

        $this->attachmentRepository = $attachmentContract;
    }

    /**
     * Show the specified resource.
     *
     * @param unknown $id
     * @return Response
     */
    public function show($id)
    {
        $attachment = $this->attachmentRepository->find($id);

        if (!$attachment) {
            return;
        }

        return $this->attachmentRepository->get($id, true);
    }
}
