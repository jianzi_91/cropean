<?php

namespace Modules\Announcement\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;
use Modules\Announcement\Models\Video;
use Modules\Announcement\Models\VideoView;

class VideoController extends Controller
{
    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show(Request $request, $id)
    {
        $video = Video::findOrFail($id);

        $path  = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR  . 'CEO_Welcome_Message.mp4';

        return response()->file($path, [
            'Content-Type' => 'video/mp4',
        ]);
    }

    /**
     * Update the specified resource.
     *
     * @param  Request $request
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function update(Request $request, $id)
    {
        $video = Video::findOrFail($id);
        $videoView = VideoView::where('video_id', $video->id)
            ->where('user_id', auth()->user()->id)
            ->first();

        if (empty($videoView)) {
            $video->increment('views');
            $view = $video->views()->create([
                'user_id' => auth()->user()->id, 
            ]);
        }
    }
}
