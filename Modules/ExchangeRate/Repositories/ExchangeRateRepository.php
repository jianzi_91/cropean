<?php

namespace Modules\ExchangeRate\Repositories;

use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\ExchangeRate\Models\ExchangeRate;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class ExchangeRateRepository extends Repository implements ExchangeRateContract
{
    use HasCrud, HasSlug;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param ExchangeRate  $model
     */
    public function __construct(
        ExchangeRate $model,
        BankCreditTypeContract $creditTypeRepo
    ) {
        $this->slug  = 'currency';
        $this->creditTypeRepo = $creditTypeRepo;

        parent::__construct($model);
    }

    public function getExchangeRates()
    {
        return $this->model->whereIn('currency', [
            BankCreditTypeContract::CAPITAL_CREDIT,
            BankCreditTypeContract::ROLL_OVER_CREDIT,
            BankCreditTypeContract::CASH_CREDIT,
        ])->get()->keyBy('rawcurrency')->toArray();
    }

    public function toBaseCurrency($creditType, $amount)
    {
        $creditType   = $this->creditTypeRepo->findBySlug($creditType);
        $exchangeRate = ExchangeRate::where('currency', $creditType->rawname)->first();

        return bcdiv($amount, $exchangeRate->sell_rate, 2);
    }
}
