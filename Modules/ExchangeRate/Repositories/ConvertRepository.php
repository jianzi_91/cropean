<?php

namespace Modules\ExchangeRate\Repositories;

use Modules\ExchangeRate\Contracts\ConvertContract;

class ConvertRepository implements ConvertContract
{
    public function convertToCredit($data)
    {
        $to     = $data['to'];
        $rate   = $data['rate'];
        $amount = $data['amount'];

        //$converted_amount = special_ceil(bcdiv($amount, $rate, 10), $precision);
        $converted_amount = bcmul($amount, $rate, credit_precision());

        return ['converted_amount' => $converted_amount];
    }
}
