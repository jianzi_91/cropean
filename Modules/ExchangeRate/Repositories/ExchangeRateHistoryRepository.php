<?php

namespace Modules\ExchangeRate\Repositories;

use Modules\ExchangeRate\Contracts\ExchangeRateHistoryContract;
use Modules\ExchangeRate\Models\ExchangeRateHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class ExchangeRateHistoryRepository extends Repository implements ExchangeRateHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(ExchangeRateHistory $model)
    {
        parent::__construct($model);
    }
}
