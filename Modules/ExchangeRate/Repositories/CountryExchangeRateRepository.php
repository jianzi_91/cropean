<?php

namespace Modules\ExchangeRate\Repositories;

use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;
use Modules\ExchangeRate\Contracts\CountryExchangeRateContract;
use Modules\ExchangeRate\Models\CountryExchangeRate;

class CountryExchangeRateRepository extends Repository implements CountryExchangeRateContract
{
    use HasCrud;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param CountryExchangeRate  $model
     */
    public function __construct(
        CountryExchangeRate $model
    ) {
        parent::__construct($model);
    }
}
