<?php

namespace Modules\ExchangeRate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;

class ExchangeRate extends Model
{
    use SoftDeletes, Translatable, HasDropDown;

    protected $fillable = ['currency', 'sell_rate', 'buy_rate', 'is_currency', 'current', 'updated_by'];
    protected $table    = 'exchange_rates';
    /**
     * Translatable columns.
     *
     * @var array
     */
    protected $translatableAttributes = ['currency'];

    /**
     * Get currency id by currency abbreviation
     * @param unknown $query
     * @return unknown
     */
    public static function getCurrencyIdByName($name)
    {
        return (new static)->newQuery()
            ->where('currency', $name)->first()->id;
    }

    /**
     * Set currency abbreviation uppercase
     * @param unknown $value
     * @return unknown
     */
    public function setCurrency($value)
    {
        $this->attributes['currency'] = strtoupper($value);
    }

    /**
     * Local scope for is currency
     * @param unknown $query
     * @return unknown
     */
    public function scopeIsCurrency($query)
    {
        return $query->where('is_currency', 1);
    }

    /**
     * Local scope for current
     * @param unknown $query
     * @return unknown
     */
    public function scopeCurrent($query)
    {
        return $query->where('current', 1);
    }
}
