<?php

namespace Modules\ExchangeRate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Concerns\HasHistory;

class ExchangeRateHistory extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'exchange_rate_id',
        'currency',
        'sell_rate',
        'buy_rate',
        'is_currency',
        'is_current',
        'updated_by'
    ];


    /**
     * This model's relation to user status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exchangeRate()
    {
        return $this->belongsTo(ExchangeRate::class, 'exchange_rate_id');
    }

    /**
     * Local scope for getting current status.
     *
     * @param unknown $query
     * @param unknown $current
     *
     * @return unknown
     */
    public function scopeCurrent($query, $current = true)
    {
        return $query->where('is_current', $current);
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
