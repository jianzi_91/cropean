<?php

namespace Modules\ExchangeRate\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Country\Models\Country;

class CountryExchangeRate extends Model
{
    use SoftDeletes;

    protected $fillable = ['country_id', 'exchange_rate_id'];
    protected $table    = 'countries_exchange_rates';

    /**
     * This model's relation to exchange rate
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function exchange_rate()
    {
        return $this->hasOne(ExchangeRate::class, 'id', 'exchange_rate_id');
    }

    /**
     * This model's relation to country
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
