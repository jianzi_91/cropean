<?php

return [
    'name'     => 'ExchangeRate',
    'bindings' => [
        'Modules\ExchangeRate\Contracts\ExchangeRateContract'        => 'Modules\ExchangeRate\Repositories\ExchangeRateRepository',
        'Modules\ExchangeRate\Contracts\CountryExchangeRateContract' => 'Modules\ExchangeRate\Repositories\CountryExchangeRateRepository',
        'Modules\ExchangeRate\Contracts\ConvertContract'             => 'Modules\ExchangeRate\Repositories\ConvertRepository',
        'Modules\ExchangeRate\Contracts\ExchangeRateHistoryContract' => 'Modules\ExchangeRate\Repositories\ExchangeRateHistoryRepository',
    ],
    'buy_rate_precision'  => env('BUY_RATE_PRECISION', 4),
    'sell_rate_precision' => env('SELL_RATE_PRECISION', 4),
];
