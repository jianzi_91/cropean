<?php

namespace Modules\ExchangeRate\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ExchangeRateMappingsDataTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_exchange_rate' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Exchange Rate',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_exchange_rate_mappings_list',
                        'title' => 'Admin Exchange Rate Mappings List',
                    ],
                    [
                        'name'  => 'admin_exchange_rate_mappings_create',
                        'title' => 'Admin Create Exchange Rate Mappings',
                    ],
                    [
                        'name'  => 'admin_exchange_rate_mappings_edit',
                        'title' => 'Admin Edit Exchange Rate Mappings',
                    ],
                    [
                        'name'  => 'admin_exchange_rate_mappings_delete',
                        'title' => 'Admin Delete Exchange Rate Mappings',
                    ]
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_exchange_rate_mappings_list',
                'admin_exchange_rate_mappings_create',
                'admin_exchange_rate_mappings_edit',
                'admin_exchange_rate_mappings_delete',
            ]
        ];

        $this->seedPermissions($permissions);
    }
}
