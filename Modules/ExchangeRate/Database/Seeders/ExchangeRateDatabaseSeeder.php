<?php

namespace Modules\ExchangeRate\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class ExchangeRateDatabaseSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->pageRepo->add([
            'name'                => 's_exchange_rates',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $bankCreditTypeContract = resolve(BankCreditTypeContract::class);

        $data = [
            ['currency' => $bankCreditTypeContract::USD_CREDIT,  'sell_rate' => 1, 'buy_rate' => 1, 'is_currency' => 0, 'current' => 1],
            ['currency' => 'CNY', 'sell_rate' => 7, 'buy_rate' => 7, 'is_currency' => 1, 'current' => 1],
        ];

        foreach ($data as $rate) {
            $exchangeRate              = new ExchangeRate;
            $exchangeRate->currency    = $rate['currency'];
            $exchangeRate->sell_rate   = $rate['sell_rate'];
            $exchangeRate->buy_rate    = $rate['buy_rate'];
            $exchangeRate->is_currency = $rate['is_currency'];
            $exchangeRate->current     = $rate['current'];
            $exchangeRate->save();
        }
    }
}
