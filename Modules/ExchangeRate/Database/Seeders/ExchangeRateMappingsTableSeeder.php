<?php

namespace Modules\ExchangeRate\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Country\Models\Country;
use Modules\ExchangeRate\Models\CountryExchangeRate;
use Modules\ExchangeRate\Models\ExchangeRate;

class ExchangeRateMappingsTableSeeder extends Seeder
{
    /**
     * Map exchange rate to country
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $cny = ExchangeRate::where('currency', 'CNY')->first();

        $countries = Country::all();
        foreach ($countries as $country) {
            $countryExchangeRate                   = new CountryExchangeRate;
            $countryExchangeRate->exchange_rate_id = $cny->id;
            $countryExchangeRate->country_id       = $country->id;
            $countryExchangeRate->save();
        }
    }
}
