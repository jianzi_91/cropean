<?php

namespace Modules\ExchangeRate\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ExchangeRatePermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_exchange_rate' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Exchange Rate',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_exchange_rate_list',
                        'title' => 'Admin Exchange Rate List',
                    ],
                    [
                        'name'  => 'admin_exchange_rate_edit',
                        'title' => 'Admin Edit Exchange Rate',
                    ],
                ]
            ]
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_exchange_rate_list',
                'admin_exchange_rate_edit',
            ],
            Role::ADMIN => [
                'admin_exchange_rate_list',
                'admin_exchange_rate_edit',
            ]
        ];

        $this->seedPermissions($permissions);
    }
}
