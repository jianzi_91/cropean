<?php

namespace Modules\ExchangeRate\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;

class AddCreditToExchangeRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $bankCreditTypeContract = resolve(BankCreditTypeContract::class);

        $data = [
            ['currency' => $bankCreditTypeContract::CAPITAL_CREDIT,  'sell_rate' => 1, 'buy_rate' => 1, 'is_currency' => 0, 'current' => 1],
            ['currency' => $bankCreditTypeContract::ROLL_OVER_CREDIT,  'sell_rate' => 1, 'buy_rate' => 1, 'is_currency' => 0, 'current' => 1],
            ['currency' => $bankCreditTypeContract::CASH_CREDIT,  'sell_rate' => 1, 'buy_rate' => 1, 'is_currency' => 0, 'current' => 1],
        ];

        foreach ($data as $rate) {
            $exchangeRate              = new ExchangeRate;
            $exchangeRate->currency    = $rate['currency'];
            $exchangeRate->sell_rate   = $rate['sell_rate'];
            $exchangeRate->buy_rate    = $rate['buy_rate'];
            $exchangeRate->is_currency = $rate['is_currency'];
            $exchangeRate->current     = $rate['current'];
            $exchangeRate->save();
        }
    }
}
