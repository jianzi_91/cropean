<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeRateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_rate_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('exchange_rate_id');
            $table->string('currency', 20)->index();
            $table->decimal('sell_rate', 20, 6)->default(0);
            $table->decimal('buy_rate', 20, 6)->default(0);
            $table->boolean('is_currency')->default(true);
            $table->boolean('is_current')->default(true);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('exchange_rate_id')
                ->on('exchange_rates')
                ->references('id');

            $table->foreign('updated_by')
                ->on('users')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_rate_histories');
    }
}
