<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('currency', 20)->index();
            $table->string('currency_translation')->index();
            $table->decimal('sell_rate', 20, 6)->default(0);
            $table->decimal('buy_rate', 20, 6)->default(0);
            $table->boolean('is_currency')->default(true);
            $table->boolean('current')->default(false);
            $table->unsignedInteger('updated_by')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_rates');
    }
}
