<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries_exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('exchange_rate_id')->index();
            $table->unsignedInteger('country_id')->index();
            $table->engine = 'InnoDB';

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');

            $table->foreign('exchange_rate_id')
                ->references('id')
                ->on('exchange_rates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_exchange_rates');
    }
}
