<?php

namespace Modules\ExchangeRate\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\ExchangeRate\Contracts\ExchangeRateHistoryContract;
use Modules\ExchangeRate\Http\Admin\Requests\ExchangeRate\Update;
use Modules\ExchangeRate\Queries\Admin\ExchangeRateQuery;
use Plus65\Utility\Exceptions\WebException;

class ExchangeRateController extends Controller
{
    /**
     * The exchange rate contract
     * @var unknown
     */
    protected $exchangeRateContract;

    /**
     * The exchange rate query
     * @var unknown
     */
    protected $exchangeRateQuery;

    protected $exchangeRateHistoryRepo;

    /**
     * The class constructor
     * @param ExchangeRateContract $exchangeRateContract
     * @param ExchangeRateQuery $exchangeRateQuery
     * @param ExchangeRateHistoryContract $exchangeRateHistoryContract
     */
    public function __construct(
        ExchangeRateContract $exchangeRateContract,
        ExchangeRateQuery $exchangeRateQuery,
        ExchangeRateHistoryContract $exchangeRateHistoryContract
    ) {
        $this->middleware('permission:admin_exchange_rate_list')->only('index');
        $this->middleware('permission:admin_exchange_rate_edit')->only('edit', 'update');

        $this->exchangeRateContract    = $exchangeRateContract;
        $this->exchangeRateQuery       = $exchangeRateQuery;
        $this->exchangeRateHistoryRepo = $exchangeRateHistoryContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $rates = $this->exchangeRateQuery
                      ->setParameters($request->all())
                      ->paginate();

        return view('exchangerate::admin.rates.index', compact('rates'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $currency = $this->exchangeRateContract->find($id);
        if (!$currency) {
            return redirect(route('admin.exchange.rates.index'))->with('error', __('a_exchange_rate.currency_not_found'));
        }

        return view('exchangerate::admin.rates.edit')->with(compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $currency = $this->exchangeRateContract->find($id);
        if (!$currency) {
            return back()->with('error', __('a_exchange_rate.currency not found'));
        }

        DB::beginTransaction();
        try {
            $this->exchangeRateHistoryRepo->getModel()
                ->current()
                ->where('exchange_rate_id', $currency->id)
                ->update(['is_current' => 0]);

            $extra = [
                'updated_by' => $request->user()->id,
            ];

            $request = $request->merge($extra);
            $data    = $request->only(['sell_rate', 'buy_rate', 'updated_by']);

            //update exchange rate history
            if (!$exchangeRate = $this->exchangeRateContract->edit($currency->id, $data)) {
                throw new \Exception(__('a_country_exchange_rate.failed to update exchange rate'));
            }

            $exchangeRateHistory = array_merge($data, [
                'exchange_rate_id' => $exchangeRate->id,
                'currency'         => $exchangeRate->currency,
                'is_current'       => 1
            ]);

            $this->exchangeRateHistoryRepo->add($exchangeRateHistory);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.exchange.rates.edit', $id))->withMessage(__('a_country_exchange_rate.failed to update currency'));
        }

        return redirect(route('admin.exchange.rates.index'))->with('success', __('a_exchange_rate.currency updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $currency = $this->exchangeRateContract->find($id);
        if (!$currency) {
            return redirect(route('admin.exchange.rates.index'))->with('error', __('a_exchange_rate.currency not found'));
        }

        if ($currency->country) {
            return redirect(route('admin.exchange.rates.index'))->with('error', __('a_exchange_rate.cannot delete currency'));
        }

        DB::beginTransaction();
        try {
            $this->exchangeRateContract->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.exchange.rates.index'))->withMessage(__('a_country_exchange_rate.failed to update currency'));
        }

        return redirect(route('admin.exchange.rates.index'))->with('success', __('a_exchange_rate.currency deleted successfully'));
    }
}
