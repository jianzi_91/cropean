<?php

namespace Modules\ExchangeRate\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\ExchangeRate\Contracts\CountryExchangeRateContract;
use Modules\ExchangeRate\Http\Requests\Admin\CountryExchangeRate\Store;
use Modules\ExchangeRate\Http\Requests\Admin\CountryExchangeRate\Update;
use Modules\ExchangeRate\Queries\Admin\CountryExchangeRateQuery;
use Modules\ExchangeRate\Queries\Admin\ExchangeRateQuery;
use Plus65\Utility\Exceptions\WebException;

class CountryExchangeRateController extends Controller
{
    /**
     * The country exchange rate contract
     * @var unknown
     */
    protected $countryExchangeRateContract;

    /**
     * The country exchange rate query
     * @var unknown
     */
    protected $countryExchangeRateQuery;

    /**
     * The exchange rate query
     * @var unknown
     */
    protected $exchangeRateQuery;

    public function __construct(
        CountryExchangeRateContract $countryExchangeRateContract,
                                CountryExchangeRateQuery $countryExchangeRateQuery,
                                ExchangeRateQuery $exchangeRateQuery
    ) {
        $this->middleware('permission:admin_exchange_rate_mappings_list')->only('index');
        $this->middleware('permission:admin_exchange_rate_mappings_create')->only('create', 'store');
        $this->middleware('permission:admin_exchange_rate_mappings_edit')->only('edit', 'update');
        $this->middleware('permission:admin_exchange_rate_mappings_delete')->only('destroy');

        $this->countryExchangeRateContract = $countryExchangeRateContract;
        $this->countryExchangeRateQuery    = $countryExchangeRateQuery;
        $this->exchangeRateQuery           = $exchangeRateQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $countryRates = $this->countryExchangeRateQuery
                          ->setParameters($request->all())
                          ->paginate();

        return view('exchangerate::admin.countries.index', compact('countryRates'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('exchangerate::admin.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();
        try {
            $this->countryExchangeRateContract->add($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.country.exchange.rates.create'))->withMessage(__('a_country_exchange_rate.failed to set country currency'));
        }

        return redirect(route('admin.country.exchange.rates.index'))->with('success', __('a_country_exchange_rate.currency set successfully'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('exchangerate::admin.countries.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $currency = $this->countryExchangeRateContract->find($id);
        if (!$currency) {
            return redirect(route('admin.country.exchange.rates.index'))->with('error', __('a_country_exchange_rate.country currency set not found'));
        }

        $rates = $this->exchangeRateQuery->get()->pluck('currency', 'id')->toArray();

        return view('exchangerate::admin.countries.edit', compact('currency', 'rates'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $countryCurrency = $this->countryExchangeRateContract->find($id);
        if (!$countryCurrency) {
            return redirect(route('admin.country.exchange.rates.index'))->with('error', __('a_country_exchange_rate.country currency set not found'));
        }

        DB::beginTransaction();
        try {
            $this->countryExchangeRateContract->edit($id, $request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.country.exchange.rates.edit', $id))->withMessage(__('a_country_exchange_rate.failed to update country currency'));
        }

        return redirect(route('admin.country.exchange.rates.index'))->with('success', __('a_country_exchange_rate.country currency updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $currency = $this->countryExchangeRateContract->find($id);
        if (!$currency) {
            return redirect(route('admin.country.exchange.rates.index'))->with('error', __('a_country_exchange_rate.country currency not found'));
        }

        if ($currency->country) {
            return redirect(route('admin.country.exchange.rates.index'))->with('error', __('a_country_exchange_rate.cannot delete country currency'));
        }

        DB::beginTransaction();
        try {
            $this->countryExchangeRateContract->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw (new WebException($e))->redirectTo(route('admin.country.exchange.rates.index', $id))->withMessage(__('a_country_exchange_rate.failed to delete country currency'));
        }

        return redirect(route('admin.country.exchange.rates.index'))->with('success', __('a_country_exchange_rate.country currency deleted successfully'));
    }
}
