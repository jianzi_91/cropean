<?php

namespace Modules\ExchangeRate\Http\Requests\Admin\CountryExchangeRate;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id'       => 'required|unique:countries_exchange_rates,country_id',
            'exchange_rate_id' => 'required|exists:exchange_rates,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
    * Set custom attributes' names.
    *
    * @return bool
    */
    public function attributes()
    {
        return [
            'country_id'       => __('a_country_exchange_rate.country'),
            'exchange_rate_id' => __('a_country_exchange_rate.exchange rate'),
        ];
    }
}
