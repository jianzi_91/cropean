<?php

namespace Modules\ExchangeRate\Http\Admin\Requests\ExchangeRate;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sell_rate' => [
                'required',
                'min:0',
                'numeric',
                new NotScientificNotation(),
                new MaxDecimalPlaces(config('credit.precision.exchange_rate'))
            ],
            'buy_rate'  => ['required', 'min:0', 'numeric', new NotScientificNotation(), new MaxDecimalPlaces(config('credit.precision.exchange_rate'))]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
    * Set custom attributes' names.
    *
    * @return bool
    */
    public function attributes()
    {
        return [
            'sell_rate' => __('a_exchange_rate_edit.sell rate'),
            'buy_rate'  => __('a_exchange_rate_edit.buy rate'),
        ];
    }

    /**
     * custom error validation messages
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
