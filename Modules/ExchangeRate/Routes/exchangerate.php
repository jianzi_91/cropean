<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::resource('exchange/rates', 'Admin\ExchangeRateController', ['as' => 'admin.exchange'])->only(['index','edit','update']);
        Route::resource('country/exchange/rates', 'Admin\CountryExchangeRateController', ['as' => 'admin.country.exchange'])->except(['show', 'create', 'store']);
    });
});
