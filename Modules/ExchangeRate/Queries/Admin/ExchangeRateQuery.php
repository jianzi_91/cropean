<?php

namespace Modules\ExchangeRate\Queries\Admin;

use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\ExchangeRate\Queries\ExchangeRateQuery as BaseQuery;

class ExchangeRateQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'currency' => [
            'filter' => 'text',
            'table'  => 'exchange_rates',
            'column' => 'currency'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('currency', '!=', ExchangeRateContract::USD_CREDIT);
        // Do extra process befor building the query here
    }
}
