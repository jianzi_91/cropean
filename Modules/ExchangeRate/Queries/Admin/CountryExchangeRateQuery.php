<?php

namespace Modules\ExchangeRate\Queries\Admin;

use Modules\ExchangeRate\Queries\CountryExchangeRateQuery as BaseQuery;

class CountryExchangeRateQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'country' => [
            'filter' => 'select',
            'table'  => 'countries',
            'column' => 'id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
        return $this->builder;
    }
}
