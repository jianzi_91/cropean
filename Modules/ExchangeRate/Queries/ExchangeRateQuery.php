<?php

namespace Modules\ExchangeRate\Queries;

use QueryBuilder\QueryBuilder;
use Modules\ExchangeRate\Models\ExchangeRate;

class ExchangeRateQuery extends QueryBuilder
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        // Base query implementation here
        // Only shows list of country currencies
        $query = ExchangeRate::select('*');
        return $query;
    }
}
