<?php

namespace Modules\ExchangeRate\Queries;

use Modules\ExchangeRate\Models\CountryExchangeRate;
use QueryBuilder\QueryBuilder;

class CountryExchangeRateQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = CountryExchangeRate::join('countries', 'countries_exchange_rates.country_id', 'countries.id')->select('*');
        return $query;
    }
}
