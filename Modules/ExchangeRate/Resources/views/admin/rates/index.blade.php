@extends('templates.admin.master')
@section('title', __('a_page_title.exchange rates'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.exchange rates')]
    ],
    'header'=>__('a_exchange_rate_list.exchange rates')
])


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_exchange_rate_list.exchange rate list') }}</h4>
        </div>

        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_exchange_rate_list.currency') }}</th>
                    <th>{{ __('a_exchange_rate_list.buy rate') }}</th>
                    <th>{{ __('a_exchange_rate_list.sell rate') }}</th>
                    @can('admin_exchange_rate_edit')
                        <th>{{ __('a_exchange_rate_list.action') }}</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
            @forelse($rates as $rate)
                <tr>
                    <td>{{ $rate->currency }}</td>
                    <td>{{ amount_format($rate->buy_rate, credit_precision(\Modules\Credit\Contracts\BankCreditTypeContract::EXCHANGE_RATE)) }}</td>
                    <td>{{ amount_format($rate->sell_rate, credit_precision(\Modules\Credit\Contracts\BankCreditTypeContract::EXCHANGE_RATE)) }}</td>
                    @can('admin_exchange_rate_edit')
                        <td class="action">
                            <a href="{{ route('admin.exchange.rates.edit', $rate->id) }}">
                                <i class="bx bx-pencil"></i>
                            </a>
                        </td>
                    @endcan
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 4, 'text' => __('a_exchange_rate_list.no records') ])
            @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $rates->render() !!}
@endsection