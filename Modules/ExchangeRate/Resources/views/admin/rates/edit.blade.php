@extends('templates.admin.master')
@section('title', __('a_page_title.exchange rates'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.exchange rates'), 'route' => route('admin.exchange.rates.index')],
        ['name'=>__('s_breadcrumb.exchange rate details')]
    ],
    'header'=> __('a_exchange_rate_edit.update exchange rate').' - '.$currency->currency,
    'backRoute'=> route('admin.exchange.rates.index')
])

<div class="card col-lg-6 col-xs-12">
    <div class="card-content">
        <div class="card-body">
            {{ Form::open(['route' => ['admin.exchange.rates.update', $currency->id], 'method'=>'put', 'id'=>'edit-exchangerate', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
            {{ Form::formText('buy_rate', amount_format($currency->buy_rate, credit_precision(\Modules\Credit\Contracts\BankCreditTypeContract::EXCHANGE_RATE)), __('a_exchange_rate_edit.buy rate'), [], true) }}
            {{ Form::formText('sell_rate', amount_format($currency->sell_rate, credit_precision(\Modules\Credit\Contracts\BankCreditTypeContract::EXCHANGE_RATE)), __('a_exchange_rate_edit.sell rate'), [], true) }}
            <div class="d-flex justify-content-end mt-1">
                <button id="btn_submit" type="submit" class="btn btn-primary cro-btn-primary">{{ __('a_exchange_rate_edit.submit') }}</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection