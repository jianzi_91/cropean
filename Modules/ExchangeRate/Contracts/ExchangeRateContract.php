<?php

namespace Modules\ExchangeRate\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface ExchangeRateContract extends CrudContract, SlugContract
{
    const BASE       = 'USD';
    const CNY        = 'CNY';
    const USD_CREDIT = 'usd_credit';

    public function getExchangeRates();
}
