<?php

namespace Modules\ExchangeRate\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface ExchangeRateHistoryContract extends CrudContract
{
}
