<?php

if (!function_exists('currency_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function currency_dropdown()
    {
        $exchangeRateRepository = resolve(\Modules\ExchangeRate\Contracts\ExchangeRateContract::class);

        return $exchangeRateRepository->getModel()->where('is_currency', '=', '1')->get()->dropDown('currency', 'id');
    }
}
