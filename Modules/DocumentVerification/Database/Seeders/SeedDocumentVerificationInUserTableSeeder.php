<?php

namespace Modules\DocumentVerification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\User\Contracts\UserContract;

class SeedDocumentVerificationInUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userDocumentStatusRepository = resolve(UserDocumentStatusContract::class);

        $userRepository       = resolve(UserContract::class);
        $users                = $userRepository->getModel()->all();
        $userDocumentStatusId = $userDocumentStatusRepository->findBySlug($userDocumentStatusRepository::PENDING)->id;

        DB::beginTransaction();
        foreach ($users as $user) {
            $user->document_verification_status_id = $userDocumentStatusId;
            $user->save();
        }
        DB::commit();
    }
}
