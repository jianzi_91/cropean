<?php

namespace Modules\DocumentVerification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\DocumentVerification\Models\UserDocumentType;

class CreateDocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $types = [
            0 => [
                'name'      => 'front_document',
                'is_active' => 1,
            ],
            1 => [
                'name'      => 'back_document',
                'is_active' => 1,
            ],
            2 => [
                'name'      => 'user_document',
                'is_active' => 1,
            ],
        ];

        foreach ($types as $type) {
            $result = (new UserDocumentType($type))->save();
        }
    }
}
