<?php

namespace Modules\DocumentVerification\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class DocumentVerificationPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);
        $abilitySections         = [
            'admin_kyc' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Admin Kyc',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_kyc_list',
                        'title' => 'Admin Kyc List',
                    ],
                    [
                        'name'  => 'admin_kyc_edit',
                        'title' => 'Admin Kyc Edit',
                    ],
                    [
                        'name'  => 'admin_kyc_export',
                        'title' => 'Admin Kyc Export',
                    ],
                    [
                        'name'  => 'admin_kyc_attachment_show',
                        'title' => 'Admin Kyc Attachment Show',
                    ]
                ]
            ],
            'member_kyc' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Member Kyc',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_kyc_list',
                        'title' => 'Member Kyc List',
                    ],
                    [
                        'name'  => 'member_kyc_create',
                        'title' => 'Member Kyc Create',
                    ],
                    [
                        'name'  => 'member_kyc_attachment_show',
                        'title' => 'Member Kyc Attachment Show',
                    ]
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_kyc_list',
                'admin_kyc_edit',
                'admin_kyc_export',
                'admin_kyc_attachment_show',
            ],
            Role::ADMIN => [
                'admin_kyc_list',
                'admin_kyc_edit',
                'admin_kyc_export',
                'admin_kyc_attachment_show',
            ],
            Role::MEMBER => [
                'member_kyc_list',
                'member_kyc_create',
                'member_kyc_attachment_show',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
