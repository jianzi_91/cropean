<?php

namespace Modules\DocumentVerification\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\DocumentVerification\Models\UserDocumentStatus;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;

class CreateDocumentStatusesTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_user_document_statuses',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        Model::unguard();
        $statuses = [
            0 => [
                'name'      => 'pending',
                'is_active' => 1,
            ],
            1 => [
                'name'      => 'requested',
                'is_active' => 1,
            ],
            2 => [
                'name'      => 'approved',
                'is_active' => 1,
            ],
            3 => [
                'name'      => 'rejected',
                'is_active' => 1,
            ],

        ];

        foreach ($statuses as $status) {
            $result = (new UserDocumentStatus($status))->save();
        }
    }
}
