<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDocumentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_document_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_document_id')->nullable()->index('user_document_id');
            $table->unsignedInteger('user_document_status_id')->nullable()->index('user_document_status_id');
            $table->unsignedInteger('updated_by')->nullable()->index('updated_by');
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);

            $table->foreign('user_document_id')->references('id')->on('user_documents');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('user_document_status_id')->references('id')->on('user_document_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_document_histories');
    }
}
