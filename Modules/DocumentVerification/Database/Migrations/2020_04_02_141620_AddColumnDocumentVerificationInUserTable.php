<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocumentVerificationInUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('document_verification_status_id')->nullable()->index('document_verification_status_id');

            $table->foreign('document_verification_status_id')->references('id')->on('user_document_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['country_id', 'document_verification_status_id', 'user_status_id']);

            $table->dropColumn('document_verification_status_id');
        });
    }
}
