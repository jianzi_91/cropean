<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDocumentAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_document_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('filename')->unique();
            $table->string('original_filename');
            $table->string('mime_type');
            $table->string('path');
            $table->unsignedInteger('user_document_type_id')->index('user_document_type_id');
            $table->unsignedInteger('user_document_id')->nullable()->index('user_document_id');

            $table->foreign('user_document_type_id')->references('id')->on('user_document_types');
            $table->foreign('user_document_id')->references('id')->on('user_documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_document_attachments');
    }
}
