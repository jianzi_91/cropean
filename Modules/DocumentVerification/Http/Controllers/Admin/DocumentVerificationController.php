<?php

namespace Modules\DocumentVerification\Http\Controllers\Admin;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\DocumentVerification\Contracts\UserDocumentContract;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\DocumentVerification\Http\Requests\Admin\Update;
use Modules\DocumentVerification\Queries\Admin\DocumentVerificationAttachmentQuery;
use Modules\DocumentVerification\Queries\Admin\DocumentVerificationQuery;
use Plus65\Utility\Exceptions\WebException;

class DocumentVerificationController extends Controller
{
    protected $auth;
    protected $documentQuery;
    protected $documentAttachmentQuery;
    protected $userDocumentRepository;
    protected $userDocumentStatusRepository;

    public function __construct(
        Guard $auth,
        DocumentVerificationQuery $documentQuery,
        DocumentVerificationAttachmentQuery $documentVerificationAttachmentQuery,
        UserDocumentContract $userDocumentContract,
        UserDocumentStatusContract $userDocumentStatusContract
    ) {
        $this->auth                         = $auth;
        $this->documentQuery                = $documentQuery;
        $this->documentAttachmentQuery      = $documentVerificationAttachmentQuery;
        $this->userDocumentRepository       = $userDocumentContract;
        $this->userDocumentStatusRepository = $userDocumentStatusContract;

        $this->middleware('permission:admin_kyc_list')->only('index');
        $this->middleware('permission:admin_kyc_edit')->only('edit', 'update');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $documents = $this->documentQuery->setParameters($request->all())->paginate();

        return view('documentverification::admin.index', compact('documents'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $userDocument = $this->userDocumentRepository->findOrFail($id, ['documentStatus']);

        $request->merge(['user_document_id' => $id]);
        $statuses            = $this->userDocumentRepository->getVerificationStatus()->dropDown('name', 'id');
        $documentAttachments = $this->documentAttachmentQuery->setParameters($request->all())->get()->keyby('user_document_type');
        $rejectStatusId      = $this->userDocumentStatusRepository->findBySlug($this->userDocumentStatusRepository::REJECTED)->id;

        return view('documentverification::admin.edit', compact('statuses', 'id', 'userDocument', 'documentAttachments', 'rejectStatusId'));
    }

    /**
     * Update the specified resource in storage.
     * @param Update $request
     * @param int $id
     * @return Response
     * @throws WebException
     */
    public function update(Update $request, $id)
    {
        DB::beginTransaction();

        if (!$this->userDocumentRepository->getModel()->lockForUpdate()->find($id)) {
            return back()->with('error', __('a_documents_upload.document request failed'));
        }

        try {
            $user = $this->auth->user();
            $this->userDocumentRepository->update($user, $id, $request->only(['status', 'remarks']));
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.documents.index'))->withMessage(__('a_documents_upload.document request cancellation failed'));
        }
        return redirect()->route('admin.documents.index')->with('success', __('a_documents_upload.document request successfully updated'));
    }
}
