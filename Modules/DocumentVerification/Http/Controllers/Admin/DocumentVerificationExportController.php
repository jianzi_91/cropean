<?php

namespace Modules\DocumentVerification\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Deposit\Queries\Admin\DepositQuery;
use Modules\DocumentVerification\Queries\Admin\DocumentVerificationQuery;
use QueryBuilder\Concerns\CanExportTrait;

class DocumentVerificationExportController extends Controller
{
    use CanExportTrait;

    /**
     * Deposit query.
     *
     * @var DepositQuery
     */
    protected $documentQuery;

    /**
     * Class constructor.
     *
     * @param DepositQuery $depositQuery
     */
    public function __construct(DocumentVerificationQuery $documentQuery)
    {
        $this->documentQuery = $documentQuery;

        $this->middleware('permission:admin_kyc_export')->only('export');
    }

    /**
     * Export action.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return $this->exportReport($this->documentQuery->setParameters($request->all()), 'document_' . now() . '.xlsx', auth()->user(), 'Export Document Verification on ' . now());
    }
}
