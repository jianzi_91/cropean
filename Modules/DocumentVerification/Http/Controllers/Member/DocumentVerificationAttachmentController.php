<?php

namespace Modules\DocumentVerification\Http\Controllers\Member;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Announcement\Contracts\AnnouncementAttachmentContract;
use Modules\DocumentVerification\Contracts\UserDocumentAttachmentContract;

class DocumentVerificationAttachmentController extends Controller
{
    /**
     * The announcement attachment repository
     *
     * @var unknown
     */
    protected $attachmentRepository;

    /**
     * Class constructor
     *
     * @param AnnouncementAttachmentContract $attachment
     */
    public function __construct(UserDocumentAttachmentContract $uploadDocumentAttachmentContract)
    {
        $this->attachmentRepository = $uploadDocumentAttachmentContract;

        $this->middleware('permission:member_kyc_attachment_show')->only('show');
    }

    /**
     * Show the specified resource.
     *
     * @param unknown $id
     * @return Response
     */
    public function show($id)
    {
        $attachment = $this->attachmentRepository->find($id);

        if (!$attachment) {
            return;
        }

        if ($attachment->userDocument->user_id != auth()->user()->id) {
            abort(403);
        }

        return $this->attachmentRepository->get($id, false);
    }
}
