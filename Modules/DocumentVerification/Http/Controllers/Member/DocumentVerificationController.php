<?php

namespace Modules\DocumentVerification\Http\Controllers\Member;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\DocumentVerification\Contracts\UserDocumentContract;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\DocumentVerification\Http\Requests\Member\Store;
use Modules\DocumentVerification\Queries\Member\DocumentVerificationAttachmentQuery;
use Modules\DocumentVerification\Queries\Member\DocumentVerificationQuery;
use Plus65\Utility\Exceptions\WebException;

class DocumentVerificationController extends Controller
{
    protected $auth;
    protected $documentQuery;
    protected $documentAttachmentQuery;
    protected $userDocumentRepository;
    protected $documentStatusRepository;

    public function __construct(
        Guard $auth,
        DocumentVerificationQuery $documentQuery,
        DocumentVerificationAttachmentQuery $documentVerificationAttachmentQuery,
        UserDocumentContract $userDocumentContract,
        UserDocumentStatusContract $documentStatus
    ) {
        $this->auth                     = $auth;
        $this->documentQuery            = $documentQuery;
        $this->documentAttachmentQuery  = $documentVerificationAttachmentQuery;
        $this->userDocumentRepository   = $userDocumentContract;
        $this->documentStatusRepository = $documentStatus;

        $this->middleware('permission:member_kyc_list')->only('index');
        $this->middleware('permission:member_kyc_create')->only('create', 'store');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $document            = $this->documentQuery->first();
        $documentAttachments = [];
        if ($document) {
            $request->merge(['user_document_id' => $document->user_document_id]);
            $documentAttachments = $this->documentAttachmentQuery->setParameters($request->all())->get()->keyby('user_document_type');
        }

        return view('documentverification::member.index', compact('document', 'documentAttachments'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Store $request
     * @return Response
     * @throws WebException
     */
    public function store(Store $request)
    {
        $user = $this->auth->user();
        DB::beginTransaction();
        try {
            if ($lastDocumentRequest = $this->userDocumentRepository->getLastDocument($user->id)) {
                if ($lastDocumentRequest->name == $this->documentStatusRepository::REQUESTED) {
                    return back()->with('error', __('m_kyc_create.previous document request still in pending'));
                }
            }
            if (isset($user->documentVerification->rawname) && $user->documentVerification->rawname === $this->documentStatusRepository::APPROVED) {
                return back()->with('error', __('m_documents_upload.user has been verified'));
            }
            if ($request->hasFile('front_document') && $request->hasFile('back_document') && $request->hasFile('user_document')) {
                $this->userDocumentRepository->request($request, $user);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.documents.index'))->withMessage(__('m_kyc_create.document request upload failed'));
        }
        return redirect()->route('member.documents.index')->with('success', __('m_kyc_create.document request successfully upload'));
    }
}
