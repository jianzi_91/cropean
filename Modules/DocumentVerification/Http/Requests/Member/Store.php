<?php

namespace Modules\DocumentVerification\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'front_document' => [
                'required',
                'max:' . config('documentverification.validation.max_size_in_kb'),
                'mimes:' . config('documentverification.validation.mimes_type')
            ],
            'back_document'  => [
                'required',
                'max:' . config('documentverification.validation.max_size_in_kb'),
                'mimes:' . config('documentverification.validation.mimes_type')
            ],
            'user_document'  => [
                'required',
                'max:' . config('documentverification.validation.max_size_in_kb'),
                'mimes:' . config('documentverification.validation.mimes_type')
            ],
        ];
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'front_document.max' => __('m_kyc_create.the front document may not be greater than :max mb', ['max' => bcdiv(config('documentverification.validation.max_size_in_kb'),1024)]),
            'back_document.max'  => __('m_kyc_create.the back document may not be greater than :max mb', ['max' => bcdiv(config('documentverification.validation.max_size_in_kb'),1024)]),
            'user_document.max'  => __('m_kyc_create.the user document may not be greater than :max mb', ['max' => bcdiv(config('documentverification.validation.max_size_in_kb'),1024)]),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return bool
     */
    public function attributes()
    {
        return [
            'front_document' => __('m_kyc_create.front document'),
            'back_document'  => __('m_kyc_create.back document'),
            'user_document'  => __('m_kyc_create.user document'),
        ];
    }
}
