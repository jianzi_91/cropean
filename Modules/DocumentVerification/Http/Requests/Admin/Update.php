<?php

namespace Modules\DocumentVerification\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status   = resolve(UserDocumentStatusContract::class);
        $rejected = $status->findBySlug(UserDocumentStatusContract::REJECTED);
        return [
            'status'  => 'required',
            'remarks' => 'required_if:status,' . $rejected->id
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'remarks.required_if'                   => __('a_documents_upload.rejected reason required'),
            'phone_verification_status.required_if' => __('a_documents_upload.phone verification status required')
        ];
    }

    public function attributes()
    {
        return [
            'phone_verification_status' => __('a_documents_upload.phone verification status'),
            'status'                    => __('a_documents_upload.status'),
            'remarks'                   => __('a_documents_upload.remarks'),
        ];
    }
}
