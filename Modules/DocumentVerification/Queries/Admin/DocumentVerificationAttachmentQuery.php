<?php

namespace Modules\DocumentVerification\Queries\Admin;

use Modules\DocumentVerification\Queries\DocumentVerificationAttachmentQuery as BaseQuery;

class DocumentVerificationAttachmentQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'user_document_id' => [
            'filter' => 'equal',
            'table'  => 'user_documents',
            'column' => 'id'
        ],
    ];

    public function beforeBuild()
    {
    }
}
