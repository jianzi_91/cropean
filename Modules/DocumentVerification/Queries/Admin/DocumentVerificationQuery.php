<?php

namespace Modules\DocumentVerification\Queries\Admin;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\DocumentVerification\Queries\DocumentVerificationQuery as BaseQuery;

class DocumentVerificationQuery extends BaseQuery implements FromCollection, WithHeadings, WithMapping
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'email' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'email'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'member_id'
        ],
        'status_id' => [
            'filter' => 'equal',
            'table'  => 'user_document_statuses',
            'column' => 'id'
        ],
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'user_documents',
            'column' => 'created_at'
        ],
    ];

    public function beforeBuild()
    {
        return $this->builder->orderByDesc('created_at');
    }

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return $this->get();
    }

    /**
     * @inheritDoc
     */
    public function headings(): array
    {
        return [
            __('a_kyc_management.request date'),
            __('a_kyc_management.full name'),
            __('a_kyc_management.member id'),
            __('a_kyc_management.email address'),
            __('a_kyc_management.kyc status'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function map($document): array
    {
        return [
            $document->created_at,
            $document->full_name,
            $document->member_id,
            $document->email,
            __('s_user_document_statuses.' . $document->status)
        ];
    }
}
