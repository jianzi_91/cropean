<?php

namespace Modules\DocumentVerification\Queries;

use Modules\DocumentVerification\Models\UserDocument;
use QueryBuilder\QueryBuilder;

class DocumentVerificationQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = UserDocument::join('user_document_histories', function ($join) {
            $join->on('user_document_histories.user_document_id', '=', 'user_documents.id')
                ->where('user_document_histories.is_current', 1)
                ->whereNull('user_document_histories.deleted_at');
        })
            ->join('user_document_statuses', 'user_document_statuses.id', '=', 'user_document_histories.user_document_status_id')
            ->join('users', 'users.id', '=', 'user_documents.user_id')
            ->select(
                [
                    'user_documents.created_at as created_at',
                    'user_documents.id as user_document_id',
                    'user_documents.user_id',
                    'user_document_statuses.name as status',
                    'user_document_histories.remarks',
                    'users.name as full_name',
                    'users.member_id as member_id',
                    'users.email as email'
                ]
            );
        return $query;
    }
}
