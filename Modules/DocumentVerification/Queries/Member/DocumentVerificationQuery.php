<?php

namespace Modules\DocumentVerification\Queries\Member;

use Illuminate\Support\Facades\Auth;
use Modules\DocumentVerification\Queries\DocumentVerificationQuery as BaseQuery;

class DocumentVerificationQuery extends BaseQuery
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    public function beforeBuild()
    {
        return $this->builder->where('user_id', Auth::user()->id);
    }

    public function afterBuild()
    {
        return $this->builder->orderBy('user_document_id', 'desc');
    }
}
