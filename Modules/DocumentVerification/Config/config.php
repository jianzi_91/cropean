<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'bindings' => [
        'Modules\DocumentVerification\Contracts\UserDocumentContract'           => 'Modules\DocumentVerification\Repositories\UserDocumentRepository',
        'Modules\DocumentVerification\Contracts\UserDocumentStatusContract'     => 'Modules\DocumentVerification\Repositories\UserDocumentStatusRepository',
        'Modules\DocumentVerification\Contracts\UserDocumentAttachmentContract' => 'Modules\DocumentVerification\Repositories\UserDocumentAttachmentRepository',
        'Modules\DocumentVerification\Contracts\UserDocumentTypeContract'       => 'Modules\DocumentVerification\Repositories\UserDocumentTypeRepository',
    ],

    'validation' => [
        'max_size_in_kb' => env('DOCUMENT_MAX_SIZE_IN_KB', 2048),
        'mimes_type'     => env('DOCUMENT_MIMES_TYPE', 'jpeg,gif,png,jpg'),
    ],

    'attachments' => [

        'mimes' => [
            '.gif'  => 'image/gif',
            '.ico'  => 'image/x-icon',
            '.jpg'  => 'image/jpeg',
            '.png'  => 'image/png',
            '.jpeg' => 'image/jpeg',
        ],

        'max_size' => 20000
    ],

    'storage' => 'documentverifications',
];
