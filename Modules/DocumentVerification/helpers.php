<?php

use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;

if (!function_exists('document_status_approve_id')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function document_status_approve_id()
    {
        $documentStatusRepository = resolve(UserDocumentStatusContract::class);

        $documentStatus = $documentStatusRepository->findBySlug(UserDocumentStatusContract::APPROVED);

        return $documentStatus->id;
    }
}

if (!function_exists('document_status_request_id')) {
    /**
     * Returns the precision supported by the app, defined in config.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    function document_status_request_id()
    {
        $documentStatusRepository = resolve(UserDocumentStatusContract::class);

        $documentStatus = $documentStatusRepository->findBySlug(UserDocumentStatusContract::REQUESTED);

        return $documentStatus->id;
    }
}

if (!function_exists('kyc_status_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function kyc_status_dropdown()
    {
        $documentStatusRepository = resolve(Modules\DocumentVerification\Contracts\UserDocumentStatusContract::class);

        $documentStatus = $documentStatusRepository->getModel()->where('name', '<>', $documentStatusRepository::PENDING)->get()->dropDown('name', 'id');

        return $documentStatus;
    }
}

if (!function_exists('user_management_kyc_status_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function user_management_kyc_status_dropdown()
    {
        $userDocumentStatusRepository = resolve(Modules\DocumentVerification\Contracts\UserDocumentStatusContract::class);

        $userDocumentStatus = $userDocumentStatusRepository->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');

        return $userDocumentStatus;
    }
}

if (!function_exists('get_kyc_count')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function get_kyc_count($status)
    {
        $documentStatusRepository = resolve(UserDocumentStatusContract::class);
        $documentStatus           = $documentStatusRepository->findBySlug($status);
        $userRepo                 = resolve(\Modules\User\Contracts\UserContract::class);
        $roleIds                  = resolve(\Modules\Rbac\Contracts\RoleContract::class)->getModel()
            ->members()
            ->pluck('id')
            ->toArray();

        $userDocumentStatus = $userRepo->getModel()
            ->join('assigned_roles', 'assigned_roles.entity_id', 'users.id')
            ->where('users.document_verification_status_id', $documentStatus->id)
            ->whereIn('assigned_roles.role_id', $roleIds)
            ->where('users.is_system_generated', 0)
            ->count('users.id');

        return $userDocumentStatus;
    }
}
