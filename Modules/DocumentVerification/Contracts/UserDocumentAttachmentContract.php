<?php

namespace Modules\DocumentVerification\Contracts;

use Illuminate\Http\UploadedFile;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface UserDocumentAttachmentContract extends CrudContract
{
    public function attachUploadedFile($userDocumentId, $documentTypeId, UploadedFile $file, $move = false);

    public function get($userDocumentAttId, $download = false);
}
