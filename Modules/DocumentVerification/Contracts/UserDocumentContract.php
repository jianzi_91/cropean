<?php

namespace Modules\DocumentVerification\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface UserDocumentContract extends CrudContract
{
    public function getLastDocument($userId);

    public function getVerificationStatus();

    public function request($request, $user);

    public function update($adminUser, $id, $data);
}
