<?php

namespace Modules\DocumentVerification\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserDocumentStatusContract extends CrudContract, SlugContract, ActiveContract, HistoryableContract
{
    const PENDING   = "pending";
    const REQUESTED = "requested";
    const APPROVED  = "approved";
    const REJECTED  = "rejected";
}
