<?php

namespace Modules\DocumentVerification\Contracts;

use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserDocumentTypeContract extends SlugContract
{
    const FRONT_DOCUMENT = 'front_document';
    const BACK_DOCUMENT  = 'back_document';
    const USER_DOCUMENT  = 'user_document';
}
