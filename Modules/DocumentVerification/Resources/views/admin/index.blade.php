@extends('templates.admin.master')
@section('title', __('a_page_title.user management'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.kyc management')]
    ],
    'header' => __('a_kyc_management.kyc management')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_kyc_management.request date')) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('email', request('email'), __('a_kyc_management.email address'), ['placeholder'=>__('a_kyc_management.email address')], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('member_id', request('member_id'), __('a_kyc_management.member id'), ['placeholder'=>__('a_kyc_management.member id')], false) }}
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formSelect('status_id', kyc_status_dropdown(), old('status_id', request('status_id')), __('a_kyc_management.kyc status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{ __('a_kyc_management.member list') }}</h4>
            @can('admin_kyc_export')
                {{ Form::formTabSecondary(__('a_kyc_management.export table'), route('admin.documents.export', array_merge(request()->except('page')))) }}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_kyc_management.request date') }}</th>
                    <th>{{ __('a_kyc_management.full name') }}</th>
                    <th>{{ __('a_kyc_management.member id') }}</th>
                    <th>{{ __('a_kyc_management.email address') }}</th>
                    <th>{{ __('a_kyc_management.kyc status') }}</th>
                    <th>{{ __('a_kyc_management.action') }}</th>
                </tr>
            </thead>
            <tbody>
            @if(count($documents))
                @foreach ($documents as $document)
                    <tr>
                        <td>{{ $document->created_at }}</td>
                        <td>{{ $document->full_name }}</td>
                        <td>{{ $document->member_id }}</td>
                        <td>{{ $document->email }}</td>
                        <td>{{ __('s_user_document_statuses.' . $document->status) }}</td>
                        <td class="action">
                            @can('admin_kyc_edit')
                            <a href="{{ route('admin.documents.edit', $document->user_document_id) }}" title="{{ __('a_kyc_management.view edit') }}">
                            @if($document->status == \Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REQUESTED)
                                <!-- requested -->
                                <i class="bx bx-pencil"></i>
                            @else
                                <!-- approved / rejected -->
                                <i class="bx bx-show-alt"></i>
                            @endif
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            @else
                @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_kyc_management.no records') ])
            @endif
            </tbody>
        @endcomponent
    </div>
</div>
{!! $documents->render() !!}
@endsection
