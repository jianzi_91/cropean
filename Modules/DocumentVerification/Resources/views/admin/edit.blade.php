@extends('templates.admin.master')
@section('title', __('a_page_title.user management'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.kyc management'), 'route' => route('admin.documents.index')],
        ['name'=>__('s_breadcrumb.view details')]
    ],
    'header'=> __('a_kyc_view.view details'),
    'backRoute'=> route('admin.documents.index')
])

<div class="row">
    <div class="col-lg-6 col-xs-12 order-lg-1 order-2">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="kyc_label">{{ __('a_kyc_view.status') }}</div>
                    <div class="kyc_status">{{ isset($userDocument['documentStatus']['name']) ?  $userDocument['documentStatus']['name'] : __('s_user_document_statuses.pending') }}</div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title mb-2">{{ __('a_kyc_view.member details') }}</h4>

                    {{ Form::formText('name', $userDocument->user->name ?? '-', __('a_kyc_view.full name'), ['readonly']) }}

                    {{ Form::formText('national_id', $userDocument->user->id_number ?? '-', __('a_kyc_view.national id'), ['readonly']) }}

                    {{ Form::formText('country_id', $userDocument->user->country->name ?? '-', __('a_kyc_view.country'), ['readonly']) }}

                    {{ Form::formText('date_of_birth', $userDocument->user->birth_date ?? '-', __('a_kyc_view.date of birth'), ['readonly']) }}

                    {{ Form::formText('email', $userDocument->user->email ?? '-', __('a_kyc_view.email address'), ['readonly']) }}

                    {{ Form::formText('phone', '(' . $userDocument->user->mobile_prefix . ') ' .$userDocument->user->mobile_number ?? '-', __('a_kyc_view.phone'), ['readonly']) }}

                    {{ Form::formText('address', $userDocument->user->address ?? '-', __('a_kyc_view.address'), ['readonly']) }}

                    @if($userDocument->user_document_status_id <> $rejectStatusId)
                    <div>
                        {{ Form::open(['method'=>'put', 'route'=> ['admin.documents.update', $userDocument->id], 'id'=>'edit-document', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                        {{ Form::formSelect('status', $statuses, $userDocument->user_document_status_id ?? '', __('a_kyc_view.status'), ['required'=>true]) }}

                        {{ Form::formTextArea('remarks', old('remarks'), __('a_kyc_view.remarks'), ['required'=>true]) }}

                        <div class="d-flex justify-content-end">
                            {{ Form::formButtonPrimary('btn_submit', __('a_kyc_view.submit'), 'submit') }}
                        </div>

                        {{ Form::close() }}
                    </div>
                    @else
                        {{ Form::formText('status', $userDocument->documentStatus->name ?? '-', __('a_kyc_view.status'), ['readonly']) }}

                        {{ Form::formText('remarks', $userDocument->remarks ?? '-', __('a_kyc_view.remarks'), ['readonly']) }}
                    @endif
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-6 col-xs-12 order-lg-2 order-1">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title mb-2">{{ __('a_kyc_view.national id front') }}</h4>
                    <div class="d-flex justify-content-center align-items-center">
                        <a href="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::FRONT_DOCUMENT]->user_document_attachment_id) }}" target="_blank"><img style="max-width:100%" src="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::FRONT_DOCUMENT]->user_document_attachment_id) }}"/></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title mb-2">{{ __('a_kyc_view.national id back') }}</h4>
                    <div class="d-flex justify-content-center align-items-center">
                      <a href="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::BACK_DOCUMENT]->user_document_attachment_id) }}" target="_blank"><img style="max-width:100%" src="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::BACK_DOCUMENT]->user_document_attachment_id) }}"/></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h4 class="card-title mb-2">{{ __('a_kyc_view.selfie with identification proof') }}</h4>
                    <div class="d-flex justify-content-center align-items-center w-200">
                        <a href="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::USER_DOCUMENT]->user_document_attachment_id) }}" target="_blank"><img style="max-width:100%" src="{{ route('admin.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::USER_DOCUMENT]->user_document_attachment_id) }}"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
