@extends('templates.member.master')
@section('title', __('m_page_title.kyc'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_kyc.kyc')],
    ],
])
<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'kyc'])
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-xs-12 order-lg-1 order-2">
            <div class="card">
                <div class="card-body">
                    {{ Form::open(['method'=>'post', 'route' => 'member.documents.store', 'id'=>'deposit', 'enctype'=>'multipart/form-data', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    <div class="row">
                        <div class="col-md-12 col-lg-6 mb-4">
                            <div class="card-title">{{ __('m_kyc.national id front') }}</div>
                            <div class=" kyc-subtext mb-2">{{ __('m_kyc.upload a clear photo of your national id front view') }}</div>

                            @can('member_kyc_attachment_show')
                            @if(isset($document['status']))
                                <img src="{{ route('member.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::FRONT_DOCUMENT]->user_document_attachment_id) }}" class="mb-2 js-test" style="max-width:100%;"/>
                            @endif
                            @endcan

                            @if( !isset($document['status']) || $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED )
                                {{ Form::formFileUpload('front_document', '', '', __('m_kyc.choose file'), ['name'=>'front_document','id'=>'front_document','required'=>true, 'accept'=>"image/png, image/jpeg, image/gif", 'info'=>__('m_kyc.2mb file limit, jpg/jpeg/png/gif types'), 'class'=>'js-document']) }}
                            @endif
                        </div>
                        <div class="col-md-12 col-lg-6 mb-4">
                            <div class="card-title">{{ __('m_kyc.national id back') }}</div>
                            <div class="kyc-subtext mb-2">{{ __('m_kyc.upload a clear photo of your national id back view') }}</div>

                            @can('member_kyc_attachment_show')
                            @if(isset($document['status']))
                                <img src="{{ route('member.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::BACK_DOCUMENT]->user_document_attachment_id) }}" class="mb-2 js-test" style="max-width:100%;"/>
                            @endif
                            @endcan

                            @if( !isset($document['status']) || $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED )
                                {{ Form::formFileUpload('back_document', '', '', __('m_kyc.choose file'), ['name'=>'back_document','id'=>'back_document','required'=>true, 'accept'=>"image/png, image/jpeg, image/gif", 'info'=>__('m_kyc.2mb file limit, jpg/jpeg/png/gif types'), 'class'=>'js-document']) }}
                            @endif
                        </div>
                        <div class="col-md-12 col-lg-6 mb-4">
                            <div class="card-title">{{ __('m_kyc.selfie with identification proof') }}</div>
                            <div class="kyc-subtext mb-2">{{ __('m_kyc.upload a photo of you with your national id, shown clearly') }}</div>

                            @can('member_kyc_attachment_show')
                            @if(isset($document['status']))
                                <img src="{{ route('member.documents.attachments.show', $documentAttachments[\Modules\DocumentVerification\Contracts\UserDocumentTypeContract::USER_DOCUMENT]->user_document_attachment_id) }}" class="mb-2 js-test" style="max-width:100%;"/>
                            @endif
                            @endcan

                            @if( !isset($document['status']) || $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED )
                                {{ Form::formFileUpload('user_document', '', '', __('m_kyc.choose file'), ['name'=>'user_document','id'=>'user_document','required'=>true, 'accept'=>"image/png, image/jpeg, image/gif", 'info'=>__('m_kyc.2mb file limit, jpg/jpeg/png/gif types'), 'class'=>'js-document']) }}
                            @endif
                        </div>

                        @can('member_kyc_create')
                        @if(!isset($document) || $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED)
                        <div class="col-12 d-flex justify-content-end">
                            {{ Form::formButtonPrimary('btn_submit', __('m_kyc.submit')) }}
                        </div>
                        @endif
                        @endcan
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 order-lg-2 order-1">

            @php
                $status_color = "-green";
                if( !isset($document['status']) || $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED ){
                    $status_color = "-red";
                }
            @endphp
            {{-- @include('templates.__fragments.components.box', ['title'=>__('m_kyc.status'), 'value'=>isset(), 'textColor'=>$status_color]) --}}
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="kyc_label">{{ __('m_kyc.status') }}</div>
                        <div class="kyc_status">{{ isset($document['status']) ? __('s_user_document_statuses.' . $document['status']) : __('s_user_document_statuses.pending') }}</div>
                    </div>
                </div>
            </div>
            @if(isset($document['remarks']) && $document['status'] == Modules\DocumentVerification\Contracts\UserDocumentStatusContract::REJECTED)
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 mb-2 card-title">{{ __('m_kyc.remarks') }}</h4>
                    <div class="kyc-subtext">{{ $document['remarks'] }}</div>
                </div>
            </div>
            @endif

        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function(){
    $(".js-document").on("change", function(){
        var currentImage = $(this).parent().prev("img");

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.addEventListener("load", function (e) {
                currentImage.attr("src", e.target.result);
            })
            reader.readAsDataURL(this.files[0]);
        }
    })
})
</script>
@endpush
