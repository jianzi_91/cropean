<?php

namespace Modules\DocumentVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Relations\UserRelation;

class UserDocument extends Model implements Stateable
{
    use SoftDeletes, UserRelation, HistoryRelation;
    protected $fillable = ['user_id', 'user_document_status_id', 'remarks', 'updated_by'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function statuses()
    {
        return $this->hasMany(UserDocumentHistory::class, 'user_document_id');
    }

    public function status()
    {
        return $this->hasOne(UserDocumentHistory::class, 'id', 'user_document_id')->current(1);
    }

    public function getEntityId()
    {
        return $this->id;
    }

    public function getForeignKey()
    {
        return 'user_document_id';
    }

    public function getUrlAttribute()
    {
        return !empty($this->filename) ? '/storage/' . $this->path . '/' . $this->filename : null;
    }

    public function documentAttachment()
    {
        return $this->hasMany(UserDocumentAttachment::class, 'user_document_id');
    }

    public function documentStatus()
    {
        return $this->belongsTo(UserDocumentStatus::class, 'user_document_status_id');
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return UserDocumentHistory::class;
    }
}
