<?php

namespace Modules\DocumentVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Scopes\HasActiveScope;

class UserDocumentType extends Model
{
    use SoftDeletes, Translatable, HasActiveScope;

    protected $fillable               = ['name', 'name_translation', 'is_active'];
    protected $translatableAttributes = ['name'];
}
