<?php

namespace Modules\DocumentVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDocumentHistory extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_document_id', 'user_document_status_id', 'updated_by', 'remarks', 'is_current'];

    public function user_document()
    {
        return $this->belongsTo(UserDocument::class, 'user_document_id');
    }

    public function status()
    {
        return $this->belongsTo(UserDocumentStatus::class, 'user_document_status_id');
    }
}
