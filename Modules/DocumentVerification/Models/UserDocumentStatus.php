<?php

namespace Modules\DocumentVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Translation\Traits\Translatable;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Plus65\Base\Models\Scopes\HasActiveScope;

class UserDocumentStatus extends Model implements Stateable
{
    use SoftDeletes, Translatable, HasDropDown, HistoryRelation, HasActiveScope;

    protected $fillable               = ['name', 'name_translation', 'is_active'];
    protected $translatableAttributes = ['name'];

    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'user_document_status_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return UserDocumentHistory::class;
    }
}
