<?php

namespace Modules\DocumentVerification\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDocumentAttachment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'filename',
        'original_filename',
        'mime_type',
        'path',
        'user_document_type_id',
        'user_document_id'
    ];

    public function userDocument()
    {
        return $this->belongsTo(UserDocument::class, 'user_document_id');
    }
}
