<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::resource('/documents', 'Admin\DocumentVerificationController', ['as' => 'admin'])->only(['index', 'update', 'edit']);
        Route::get('/documents/attachments/{id}', ['uses' => 'Admin\DocumentVerificationAttachmentController@show'])->name('admin.documents.attachments.show');
        Route::get('/documents/export', ['as' => 'admin.documents.export', 'uses' => 'Admin\DocumentVerificationExportController@export']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::resource('/documents', 'Member\DocumentVerificationController', ['as' => 'member'])->only(['index', 'store']);
        Route::get('/documents/attachments/{id}', ['uses' => 'Member\DocumentVerificationAttachmentController@show'])->name('member.documents.attachments.show');
    });
});
