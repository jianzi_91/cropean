<?php

namespace Modules\DocumentVerification\Repositories;

use Carbon\Carbon;
use Modules\DocumentVerification\Contracts\UserDocumentAttachmentContract;
use Modules\DocumentVerification\Contracts\UserDocumentContract;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\DocumentVerification\Contracts\UserDocumentTypeContract;
use Modules\DocumentVerification\Models\UserDocument;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserDocumentRepository extends Repository implements UserDocumentContract
{
    use HasCrud;

    protected $userRepository;
    protected $documentStatusRepository;
    protected $userDocumentTypeRepository;
    protected $userDocumentAttachmentRepository;

    public function __construct(
        UserDocument $model,
        UserContract $userContract,
        UserDocumentStatusContract $documentStatusContract,
        UserDocumentAttachmentContract $userDocumentAttachmentContract,
        UserDocumentTypeContract $userDocumentType
    ) {
        $this->userRepository                   = $userContract;
        $this->documentStatusRepository         = $documentStatusContract;
        $this->userDocumentAttachmentRepository = $userDocumentAttachmentContract;
        $this->userDocumentTypeRepository       = $userDocumentType;

        parent::__construct($model);
    }

    public function getLastDocument($userId)
    {
        $document = $this->model->lockForUpdate()
            ->join('user_document_histories', function ($join) {
                $join->on('user_documents.id', '=', 'user_document_histories.user_document_id')
                    ->where('user_document_histories.is_current', 1);
            })
            ->join('user_document_statuses', 'user_document_statuses.id', '=', 'user_document_histories.user_document_status_id')
            ->where('user_documents.user_id', $userId)
            ->select([
                'user_document_statuses.name'
            ])
            ->first();

        if ($document) {
            return $document;
        }
        return;
    }

    public function request($request, $user)
    {
        $status = $this->documentStatusRepository->findBySlug($this->documentStatusRepository::REQUESTED);

        # User Document
        $attributes['created_at']              = Carbon::now()->format('Y-m-d');
        $attributes['updated_at']              = Carbon::now()->format('Y-m-d');
        $attributes['user_id']                 = $user->id;
        $attributes['user_document_status_id'] = $status->id;
        $attributes['updated_by']              = $user->id;
        $userDocument = $this->add($attributes);

        # History
        $this->documentStatusRepository->changeHistory($userDocument, $status, ['updated_by' => $user->id]);

        # Attachment
        if ($userDocument) {
            if ($request->hasFile('front_document')) {
                $attachment     = $request->file('front_document');
                $documentTypeId = $this->userDocumentTypeRepository->findBySlug('front_document');

                $this->userDocumentAttachmentRepository->attachUploadedFile($userDocument->id, $documentTypeId->id, $attachment, true);
            }
            if ($request->hasFile('back_document')) {
                $attachment     = $request->file('back_document');
                $documentTypeId = $this->userDocumentTypeRepository->findBySlug('back_document');

                $this->userDocumentAttachmentRepository->attachUploadedFile($userDocument->id, $documentTypeId->id, $attachment, true);
            }
            if ($request->hasFile('user_document')) {
                $attachment     = $request->file('user_document');
                $documentTypeId = $this->userDocumentTypeRepository->findBySlug('user_document');

                $this->userDocumentAttachmentRepository->attachUploadedFile($userDocument->id, $documentTypeId->id, $attachment, true);
            }
        }

        # User
        $user = $this->userRepository->find($user->id);

        $user->document_verification_status_id = $status->id;
        $user->save();
    }

    public function getVerificationStatus()
    {
        return $this->documentStatusRepository->getModel()->whereNotIn('name', [$this->documentStatusRepository::PENDING, $this->documentStatusRepository::REQUESTED])->get();
    }

    public function update($adminUser, $id, $data)
    {
        $document = $this->find($id, ['user']);
        $remarks  = isset($data['remarks']) ? $data['remarks'] : null;
        $status   = $this->documentStatusRepository->find($data['status']);

        # User
        $user                                  = $this->userRepository->find($document->user_id);
        $user->document_verification_status_id = $status->id;
        $user->save();

        if ($status) {
            $document->user_document_status_id = $status->id;
            $document->remarks                 = $data['remarks'];
            $document->save();

            $this->documentStatusRepository->changeHistory($document, $status, ['updated_by' => $adminUser->id, 'remarks' => $remarks]);

            return $document;
        }
        return false;
    }
}
