<?php

namespace Modules\DocumentVerification\Repositories;

use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\DocumentVerification\Models\UserDocumentHistory;
use Modules\DocumentVerification\Models\UserDocumentStatus;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserDocumentStatusRepository extends Repository implements UserDocumentStatusContract
{
    use HasCrud, HasSlug, HasHistory, HasActive;

    /**
     * Class constructor
     * @param UserDocumentStatus $model
     * @param UserDocumentHistory $history
     */
    public function __construct(UserDocumentStatus $model, UserDocumentHistory $history)
    {
        $this->historyModel = $history;
        parent::__construct($model);
    }
}
