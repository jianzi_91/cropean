<?php

namespace Modules\DocumentVerification\Repositories;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Core\Exceptions\MaxFileSize;
use Modules\Core\Exceptions\UnsupportedMimeType;
use Modules\DocumentVerification\Contracts\UserDocumentAttachmentContract;
use Modules\DocumentVerification\Models\UserDocumentAttachment;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserDocumentAttachmentRepository extends Repository implements UserDocumentAttachmentContract
{
    use HasCrud;

    /**
     * The root directory
     * @var unknown
     */
    protected $rootDir;

    /**
     * The storage path location
     * @var unknown
     */
    protected $storagePath;

    public function __construct(UserDocumentAttachment $model)
    {
        $this->rootDir     = config('filesystems.disks.local.root');
        $this->storagePath = config('documentverification.storage');
        parent::__construct($model);
    }

    public function attachUploadedFile($userDocumentId, $documentTypeId, UploadedFile $file, $move = false)
    {
        $this->validateFile($file);

        $path                              = config('documentverification.storage');
        $attachment                        = new $this->model;
        $randomFilename                    = md5(str_replace('.', '', Str::random(5) . microtime(true)));
        $randomFilename                    = $randomFilename . '.' . $file->extension();
        $attachment->filename              = $randomFilename;
        $attachment->original_filename     = $file->getClientOriginalName();
        $attachment->path                  = $path;
        $attachment->mime_type             = $file->getMimeType();
        $attachment->user_document_type_id = $documentTypeId;
        $attachment->user_document_id      = $userDocumentId;

        if ($attachment->save()) {
            if (config('filesystems.cloud_enable')) {
                Storage::cloud()->putFileAs($this->storagePath, $file, $randomFilename, 'private');
            } else {
                $dirPath = $this->rootDir . DIRECTORY_SEPARATOR . $this->storagePath;
                $this->checkDirectory($dirPath);
                Storage::putFileAs($this->storagePath, $file, $randomFilename);
            }
            $dirPath = $this->rootDir . DIRECTORY_SEPARATOR . $this->storagePath;
            $this->checkDirectory($dirPath);
            Storage::putFileAs($this->storagePath, $file, $randomFilename);
            return $attachment;
        }

        return false;
    }

    public function get($userDocumentAttId, $download = false)
    {
        $attachment = $this->model->findOrFail($userDocumentAttId);
        $path       = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;
        if (config('filesystems.cloud_enable')) {
            return response(Storage::cloud()->get($path))->header(
                'Content-Type',
                $attachment->mime_type
            );
        }
        if (Storage::exists($path)) {
            $contents = Storage::get($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    protected function validateFile($filename, $path = null)
    {
        $config = config('documentverification.attachments');

        if ($filename instanceof UploadedFile) {
            $mime = $filename->getClientMimeType();
            $size = $filename->getSize();
        } else {
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
            $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
            $mime     = $upload->getClientMimeType();
            $size     = $upload->getSize();
        }

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Check directory
     * @param unknown $path
     * @return boolean
     */
    protected function checkDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path, 0777, true);
        }

        return false;
    }
}
