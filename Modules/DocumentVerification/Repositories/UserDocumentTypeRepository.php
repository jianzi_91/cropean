<?php

namespace Modules\DocumentVerification\Repositories;

use Modules\DocumentVerification\Contracts\UserDocumentTypeContract;
use Modules\DocumentVerification\Models\UserDocumentType;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserDocumentTypeRepository extends Repository implements UserDocumentTypeContract
{
    use HasSlug;

    public function __construct(UserDocumentType $model)
    {
        parent::__construct($model);
    }
}
