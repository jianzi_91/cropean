<?php

namespace Modules\Password\Repositories;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Plus65\Base\Repositories\Repository;
use Modules\Password\Contracts\PasswordContract;
use Modules\User\Models\User;

class PasswordRepository extends Repository implements PasswordContract
{
    /**
     * Class constructor
     *
     * @param Model $model
     * @param Application $app
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * Generate random password
     *
     * @param int $length
     * @return string
     */
    public function generateRandomPassword(int $length = 10)
    {
        return Str::random($length);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Password\Contracts\PasswordContract::reset($userId)
     */
    public function resetPrimary(int $userId)
    {
        $user           = $this->getModel()->findOrFail($userId);
        $newPassword    = $this->generateRandomPassword(6);
        $user->password = bcrypt($newPassword);
        if ($user->save()) {
            return $newPassword;
        }

        throw new \Exception('Unable to save to DB');
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Password\Contracts\PasswordContract::update($userId, $password)
     */
    public function updatePrimary(int $userId, string $password)
    {
        $user           = $this->getModel()->findOrFail($userId);
        $user->password = bcrypt($password);
        return $user->save();
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Password\Contracts\PasswordContract::resetSecondary($userId)
     */
    public function resetSecondary(int $userId)
    {
        $user                     = $this->getModel()->findOrFail($userId);
        $newPassword              = $this->generateRandomPassword(6);
        $user->secondary_password = bcrypt($newPassword);
        if ($user->save()) {
            return $newPassword;
        }

        throw new \Exception('Unable to save to DB');
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\Password\Contracts\PasswordContract::updateSecondary($userId, $password)
     */
    public function updateSecondary(int $userId, string $password)
    {
        $user                     = $this->getModel()->findOrFail($userId);
        $user->secondary_password = bcrypt($password);
        return $user->save();
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Password\Contracts\PasswordContract::validate()
     */
    public function validatePrimary(int $userId, string $password)
    {
        $user = $this->getModel()->findOrFail($userId);
        return Hash::check($password, $user->password);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Password\Contracts\PasswordContract::validateSecondary()
     */
    public function validateSecondary(int $userId, string $password)
    {
        $user = $this->getModel()->findOrFail($userId);
        return Hash::check($password, $user->secondary_password);
    }

    public function createNewSecondaryPasswordResetRequest($user){
        $token = Str::random(60);

        DB::table('secondary_password_resets')->where('email', $user->email)->delete();

        DB::table('secondary_password_resets')->insert([
            'email'      => $user->email,
            'token'      => bcrypt($token),
            'created_at' => now()
        ]);

        return $token;
    }
}
