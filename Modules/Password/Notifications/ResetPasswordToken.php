<?php

namespace Modules\Password\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPasswordToken extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->is_member) {
            $template = 'password::member.reset_password.email.reset-password';
            $subject  = __('m_reset_password.reset password email subject');
        } else {
            $template = 'password::admin.reset_password.email.reset-password';
            $subject  = __('a_reset_password.reset password email subject');
        }

        return (new MailMessage)
                    ->subject($subject)
                    ->markdown($template, ['name' => $notifiable->name, 'token' => $this->token, 'email' => $notifiable->email]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
