<html>
  <head>
    <meta charset="utf-8" />
    <link
      href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;700&display=swap"
      rel="stylesheet"
    />
  </head>
  <body style="font-family:'IBM Plex Sans'">
    <div
      style="padding:50px;background-image:linear-gradient(90deg, rgba(205, 166, 116, 1), rgba(171, 124, 98, 1));"
    >
      <div style="padding: 0 0 40px;">
        <img
          style="width:40px;height:auto;"
          src="{{ asset('images/email_logo.png') }}"
        />
      </div>
      <div style="background-color:white;padding:40px;">
        <div style="padding: 0 0 40px;">{{ __('m_reset_secondary_password_email.dear') }} {{ $user->name }},</div>
        <div style="padding: 0 0 30px;">
        {{ __('m_reset_secondary_password_email.you have requested to reset your account secondary password please click the link below to reset') }}
        </div>
        <!-- Button -->
        <div style="margin: 0 0 50px;">
          <a
            href="{!! route('member.password.secondary.reset.index', ['token' => $token, 'email' => $user->email]) !!}"
            style="color:white;text-decoration:none;padding:10px;border-radius:4px;background-color:#CDA674;"
            >{{ __('m_reset_secondary_password_email.reset secondary password') }}</a
          >
        </div>
        <div style="padding: 0 0 100px;">
        {{ __('m_reset_secondary_password_email.regards') }},<br />
          <b>{{ __('m_reset_secondary_password_email.cropean') }}</b> {{ __('m_reset_secondary_password_email.team') }}
        </div>
        <div style="font-size: 14px;">
        {{ __('m_reset_secondary_password_email.if you are having trouble clicking the reset secondary password button, copy and paste the url below into your web browser') }}:
          <span style="color:#CDA674;">[{{ route('member.password.secondary.reset.index', ['token' => $token, 'email' => $user->email ]) }}]</span>
        </div>
      </div>
      <div style="color:white;font-size:12px;padding:40px 0 0;">
        &copy; {{__("m_reset_secondary_password_email.cropean 2020")}}. {{__("m_reset_secondary_password_email.all rights reserved")}}.
      </div>
    </div>
  </body>
</html>
