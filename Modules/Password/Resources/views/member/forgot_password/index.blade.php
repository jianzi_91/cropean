@extends('templates.member.wide')
@section('title', __('m_page_title.forgot password'))

@section('main')
    <section id="auth-login" class="row flexbox-container">
        <div class="col-xl-8 col-11">
            <div class="wide lang-selector">
                @foreach(languages() as $locale=>$name)
                <div data-lang="{{ $locale }}" class="lang-item {{ app()->getLocale() == $locale ? 'active' : '' }}">{{ $name }}</div>
                @if (!$loop->last)
                <div>&nbsp;&nbsp;|&nbsp;&nbsp;</div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-xl-8 col-11">
            <div class="card bg-authentication mb-0">
                <div class="row m-0" style="background-color: white;">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="card-header pb-1 d-flex justify-content-center">
                                <img src="{{ asset('images/logo_text.svg') }}" class="img-fluid" alt="logo" />
                            </div>
                            <br/>
                            <div class="card-content">
                                <div class="card-body">
                                    
                                    {{ Form::open(['method'=>'post', 'route' => 'member.password.forgot.store', 'id'=>'form', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}

                                        <div class="form-group pb-1">
                                            {{ Form::formText('email', old('email'), __('m_forgot_password.email address'), ['required'=>true], false) }}
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" name="btn_submit" class="btn btn-primary w-100 position-relative">{{ __('m_forgot_password.submit') }}<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                        </div>
                                    {{ Form::close() }}
                                    <hr>
                                    <div class="text-center login-to-register"><a href="{{ route('member.login') }}" class="a-text"><span>{{ __('m_forgot_password.back to login') }}</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3" style="padding: 0 !important;">
                        <div class="card-content">
                            <img class="img-fluid" src="{{ asset('images/bull.png') }}" alt="branding logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#form', {
        email: { presence: { message: __.validation.field_required } },
    })

    $('.lang-item').on('click', function() {
        var lang = $(this).data('lang')
        window.location.href = '/lang/' + lang
    })
})
</script>
@endpush