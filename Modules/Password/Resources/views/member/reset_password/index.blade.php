@extends('templates.member.wide')
@section('title', __('m_page_title.reset password'))

@section('main')
    <section id="auth-login" class="row flexbox-container">
        <div class="col-xl-8 col-11">
            <div class="wide lang-selector">
                @foreach(languages() as $locale=>$name)
                <div data-lang="{{ $locale }}" class="lang-item {{ app()->getLocale() == $locale ? 'active' : '' }}">{{ $name }}</div>
                @if (!$loop->last)
                <div>&nbsp;&nbsp;|&nbsp;&nbsp;</div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-xl-8 col-11">
            <div class="card bg-authentication mb-0">
                <div class="row m-0" style="background-color: white;">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="card-header pb-1 d-flex justify-content-center">
                                <img src="{{ asset('images/logo_text.svg') }}" class="img-fluid" alt="logo" />
                            </div>
                            <br/>
                            <div class="card-content">
                                <div class="card-body">
                                    
                                    {{ Form::open(['method'=>'post', 'route'=>'member.password.reset.store', 'id'=>'form', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                                        <div style="display:none;">{{ Form::formText('token', $token, '') }}</div>
                                        <div style="display:none;">{{ Form::formText('email', $email, __('m_reset_password.email'), ['readonly']) }}</div>

                                        <div class="form-group">
                                            {{ Form::formPassword('password','', __('m_reset_password.new password'), [], false) }}
                                        </div>
                                        <div class="form-group pb-1">
                                            {{ Form::formPassword('password_confirmation','', __('m_reset_password.confirm new password'), [], false) }}
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" name="btn_submit" class="btn btn-primary w-100 position-relative">{{ __('m_reset_password.submit') }}<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3" style="padding: 0 !important;">
                        <div class="card-content">
                            <img class="img-fluid" src="{{ asset('images/bull.png') }}" alt="branding logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#form', {
        password: { 
            presence: { message: __.validation.field_required },
            length: { minimum: 6, message: __.validation.minimum_6_characters },
        },
        password_confirmation: { 
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "password",
                message: __.validation.password_not_match,
            }
        },
    })

    $('.lang-item').on('click', function() {
        var lang = $(this).data('lang')
        window.location.href = '/lang/' + lang
    })
})
</script>
@endpush