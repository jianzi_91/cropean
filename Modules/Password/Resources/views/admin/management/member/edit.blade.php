@extends('templates.admin.master')
@section('title', __('a_member_management_change_password.change password'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_change_password.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_change_password.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_change_password.change password')]
    ],
    'header'=>__('a_member_management_change_password.change password'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'change password', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                <h5 class="card-title mb-3">{{ __("a_member_management_change_password.change primary password") }}</h5>
                {{ Form::open(['route' => ['admin.management.members.password.update', $user->id], 'method'=>'put', 'id'=>'password-primary', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formPassword('password', '', __('a_member_management_change_password.new login password')) }}
                    {{ Form::formPassword('password_confirmation', '', __('a_member_management_change_password.confirm new login password')) }}
                    <div style="width:100%;text-align:right;">
                    {{ Form::formButtonPrimary('btn_submit', __('a_member_management_change_password.save')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                <h5 class="card-title mb-3">{{ __("a_member_management_change_password.change secondary password") }}</h5>
                {{ Form::open(['route' => ['admin.management.members.secondary_password.update', $user->id], 'method'=>'put', 'id'=>'password-secondary', 'onsubmit' => 'btn_submit_2.disabled = true; return true;']) }}
                    {{ Form::formPassword('secondary_password', '', __('a_member_management_change_password.new secondary password')) }}
                    {{ Form::formPassword('secondary_password_confirmation', '', __('a_member_management_change_password.confirm new secondary password')) }}
                    <div style="width:100%;text-align:right;">
                    {{ Form::formButtonPrimary('btn_submit_2', __('a_member_management_change_password.save')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script type="text/javascript">
$(function(){
    // validation
    CreateValidation('form#password-primary', {
        password: {
            length: { minimum: 6, message: __.validation.minimum_6_characters },
            presence: {message: __.validation.field_required},
        },
        password_confirmation: {
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "password",
                message: __.validation.password_not_match,
            }
        }
    });

    CreateValidation('form#password-secondary', {
        secondary_password: {
            length: { minimum: 6, message: __.validation.minimum_6_characters },
            presence: {message: __.validation.field_required},
            equality: {
                attribute: "password",
                message: __.validation.not_as_same_as_password,
                comparator: function(v1, v2) {
                    return JSON.stringify(v1) !== JSON.stringify(v2);
                }
            },
        },
        secondary_password_confirmation: {
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "secondary_password",
                message: __.validation.secondary_password_not_match,
            }
        }
    });
});
</script>
@endpush
