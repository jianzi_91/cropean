@component('mail::panel')
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<div style="min-height:700px; height:100%; width: 100%;background-color: #F0F0F0; position:relative; font-family:lato; color: #1F2C37;">
    <div style="height:519px;width:509px; background:transparent; position:absolute; top:40%; left:50%; transform:translate(-50%, -50%); clear:both; box-sizing:border-box; padding:20px 40px;">
        <div style="">
            <div style="width:100%; height:50%;">
                <img style="height: 86px; width: 74px;" src="{{asset('svg/logo.svg')}}" />
                <h2 style="font-size:16px; margin-bottom:0px; ">{{ __('a_reset_password_email.dear') }} {{ $name }},</h2>
                <p style="color: #1F2C37; font-size: 16px;"> {{ __('a_reset_password_email.you have requested to reset your account password please click the link below to reset') }}: </p>
                <a style="width: 136px;color:white; display:block; height: 36px; border-radius: 4px; background-color: #F17032;  border: 1px solid #F17032; font-size: 14px; line-height:36px; text-align: center; text-decoration:none;" href="{!! route('admin.password.reset.index', ['token' => $token, 'email' => $email]) !!}">
                    {{ __('a_reset_password_email.reset password') }}
                </a>
                <h2 style="font-size:16px;">{{ __('a_reset_password_email.regards') }},</h2>
                <h2 style="font-size:16px;">{{ __('a_reset_password_email.cropean team') }},</h2>
                <p style="font-size:12px;">{{ __('a_reset_password_email.if you re having trouble clicking the reset password button, copy and paste the url below into your web browser') }}: <br> <a>[[{{ route('admin.password.reset.index', ['token' => $token, 'email' => $email]) }}]({{ route('admin.password.reset.index', ['token' => $token, 'email' => $email]) }})]</a> </p>
            </div>
        </div>
    </div>
</div>
@endcomponent
