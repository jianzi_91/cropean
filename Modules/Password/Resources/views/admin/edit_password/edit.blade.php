@extends('templates.admin.master')
@section('title', __('a_page_title.my password'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

{{--@section('body-class')
no-card-shadow
@endsection--}}

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_change_password.my password')],
    ],
])
<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.admin.includes.profile_nav', ['page' => 'password'])
        </div>
    </div>

    <div class="row">
        @can('admin_profile_update_password')
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <h5 class="card-title mb-3">{{ __("a_change_password.change primary password") }}</h5>
                    {{ Form::open(['route' => ['admin.password.update'], 'method'=>'put', 'id'=>'password-primary', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        {{ Form::formPassword('current_password', '', __('a_change_password.current password')) }}
                        {{ Form::formPassword('password', '', __('a_change_password.new login password')) }}
                        {{ Form::formPassword('password_confirmation', '', __('a_change_password.confirm new login password')) }}
                        <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit', __('a_change_password.save')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @endcan

        @can('admin_profile_update_secondary_password')
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <h5 class="card-title mb-3">{{ __("a_change_password.change secondary password") }}</h5>
                    {{ Form::open(['route' => ['admin.secondary_password.update'], 'method'=>'put', 'id'=>'password-secondary', 'onsubmit' => 'btn_submit_2.disabled = true; return true;']) }}
                        {{ Form::formPassword('current_secondary_password', '', __('a_change_password.current secondary password')) }}
                        {{ Form::formPassword('secondary_password', '', __('a_change_password.new secondary password')) }}
                        {{ Form::formPassword('secondary_password_confirmation', '', __('a_change_password.confirm new secondary password')) }}
                        <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit_2', __('a_change_password.save')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @endcan

        @can('admin_profile_reset_secondary_password')
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    <h5 class="card-title mb-3">{{ __("a_change_password.reset secondary password") }}</h5>
                    {{ Form::open(['route' => ['admin.secondary_password.reset'], 'method'=>'put', 'id'=>'reset-password-secondary', 'onsubmit' => 'btn_submit_3.disabled = true; return true;']) }}
                        {{ Form::formPassword('reset_current_password', '', __('a_change_password.current primary password')) }}
                        {{ Form::formPassword('reset_secondary_password', '', __('a_change_password.new secondary password')) }}
                        {{ Form::formPassword('reset_secondary_password_confirmation', '', __('a_change_password.confirm new secondary password')) }}
                        <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit_3', __('a_change_password.save')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @endcan
    </div>
</section>
@endsection