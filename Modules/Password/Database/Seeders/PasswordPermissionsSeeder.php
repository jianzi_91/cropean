<?php

namespace Modules\Password\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class PasswordPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_password' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Password',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_profile_update_password',
                        'title' => 'Admin Profile Update Password',
                    ],
                    [
                        'name'  => 'admin_profile_update_secondary_password',
                        'title' => 'Admin Profile Update Secondary Password',
                    ],
                    [
                        'name'  => 'admin_profile_reset_secondary_password',
                        'title' => 'Admin Profile Reset Secondary Password',
                    ],
                ]
            ],
            'admin_user_admin_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Admin Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_admin_management_update_password',
                        'title' => 'Admin User Admin Management Update Password',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_update_secondary_password',
                        'title' => 'Admin User Admin Management Update Secondary Password',
                    ],
                ]
            ],
            'admin_user_member_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Member Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_update_password',
                        'title' => 'Admin User Member Management Update Password',
                    ],
                    [
                        'name'  => 'admin_user_member_management_update_secondary_password',
                        'title' => 'Admin User Member Management Update Secondary Password',
                    ],
                ]
            ],
            'member_password' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Password',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_profile_update_password',
                        'title' => 'Member Profile Update Password',
                    ],
                    [
                        'name'  => 'member_profile_update_secondary_password',
                        'title' => 'Member Profile Update Secondary Password',
                    ],
                    [
                        'name'  => 'member_profile_reset_secondary_password',
                        'title' => 'Member Profile Reset Secondary Password',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_profile_update_password',
                'admin_profile_update_secondary_password',
                'admin_profile_reset_secondary_password',
                'admin_user_admin_management_update_password',
                'admin_user_admin_management_update_secondary_password',
                'admin_user_member_management_update_password',
                'admin_user_member_management_update_secondary_password',
            ],
            Role::ADMIN => [
                'admin_profile_update_password',
                'admin_profile_update_secondary_password',
                'admin_profile_reset_secondary_password',
                'admin_user_admin_management_update_password',
                'admin_user_admin_management_update_secondary_password',
                'admin_user_member_management_update_password',
                'admin_user_member_management_update_secondary_password',
            ],
            Role::MEMBER => [
                'member_profile_update_password',
                'member_profile_update_secondary_password',
                'member_profile_reset_secondary_password'
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
