<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        // Forgot password
        //Route::resource('password/forgot', 'Admin\ForgotPasswordController', ['as' => 'admin.password'])->only(['index', 'store']);

        // Reset password
        //Route::resource('password/reset', 'Admin\ResetPasswordController', ['as' => 'admin.password'])->only(['index', 'store']);

        Route::group(['middleware' => ['auth']], function () {
            Route::get('password', ['as' => 'admin.password.edit', 'uses' => 'Admin\PasswordController@edit']);
            Route::put('password', ['as' => 'admin.password.update', 'uses' => 'Admin\PasswordController@updatePassword']);
            Route::put('password/secondary', ['as' => 'admin.secondary_password.update', 'uses' => 'Admin\PasswordController@updateSecondaryPassword']);
            Route::put('password/secondary/reset', ['as' => 'admin.secondary_password.reset', 'uses' => 'Admin\PasswordController@resetSecondaryPassword']);

            // Member Management
            Route::get('members/{id}/password', ['as' => 'admin.management.members.password.edit', 'uses' => 'Admin\Management\Member\PasswordController@edit']);
            Route::put('members/{id}/password', ['as' => 'admin.management.members.password.update', 'uses' => 'Admin\Management\Member\PasswordController@updatePassword']);
            Route::put('members/{id}/password/secondary', ['as' => 'admin.management.members.secondary_password.update', 'uses' => 'Admin\Management\Member\PasswordController@updateSecondaryPassword']);

            // Admin Management
            Route::get('admins/{id}/password', ['as' => 'admin.management.admins.password.edit', 'uses' => 'Admin\Management\Admin\PasswordController@edit']);
            Route::put('admins/{id}/password', ['as' => 'admin.management.admins.password.update', 'uses' => 'Admin\Management\Admin\PasswordController@updatePassword']);
            Route::put('admins/{id}/password/secondary', ['as' => 'admin.management.admins.secondary_password.update', 'uses' => 'Admin\Management\Admin\PasswordController@updateSecondaryPassword']);
        });
    });

    Route::group(['domain' => config('core.member_url')], function () {
        // Forgot password
        Route::resource('password/forgot', 'Member\ForgotPasswordController', ['as' => 'member.password'])->only(['index', 'store']);
        Route::resource('password/secondary/forgot', 'Member\ForgotSecondaryPasswordController', ['as' => 'member.password.secondary'])->only(['index', 'store']);

        // Reset password
        Route::resource('password/reset', 'Member\ResetPasswordController', ['as' => 'member.password'])->only(['index', 'store']);

        // Reset secondary password
        Route::resource('password/secondary/reset', 'Member\ResetSecondaryPasswordController', ['as' => 'member.password.secondary'])->only(['index', 'store']);

        Route::group(['middleware' => ['auth']], function () {
            Route::get('password', ['as' => 'member.password.edit', 'uses' => 'Member\PasswordController@edit']);
            Route::put('password', ['as' => 'member.password.update', 'uses' => 'Member\PasswordController@updatePassword']);
            Route::put('password/secondary', ['as' => 'member.secondary_password.update', 'uses' => 'Member\PasswordController@updateSecondaryPassword']);
            Route::put('password/secondary/reset', ['as' => 'member.secondary_password.reset', 'uses' => 'Member\PasswordController@resetSecondaryPassword']);
        });
    });
});
