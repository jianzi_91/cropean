<?php

namespace Modules\Password\Http\Requests\Admin\ResetPassword;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordDifferent;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userRepository = resolve(UserContract::class);
        $user           = $userRepository->findBySlug($this->email)->first();

        return [
            'bail',
            'token'    => 'required|max:255',
            'password' => [
                'required',
                'min:' . config('password.primary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new PasswordDifferent($user ? $user->id : null),

            ],
            'password_confirmation' => 'required|min:' . config('password.primary_password_min_characters'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'password'              => __('a_reset_password.new password'),
            'password_confirmation' => __('a_reset_password.confirm new password'),
        ];
    }
}
