<?php

namespace Modules\Password\Http\Requests\Admin\ForgotPassword;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Http\Rules\UserIsAdmin;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'bail',
                'required',
                'exists:users,email',
                new UserIsAdmin()
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => __('a_forgot_password.email'),
        ];
    }
}
