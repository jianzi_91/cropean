<?php

namespace Modules\Password\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\Password\Http\Rules\SecondaryPasswordDifferent;
use Modules\Password\Http\Rules\SecondaryPasswordMatch;

class UpdateSecondaryPassword extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        $rules = [
            'current_secondary_password' => [
                'required',
                new SecondaryPasswordMatch(),
            ],
            'secondary_password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new SecondaryPasswordDifferent($user->id)
            ],
            'secondary_password_confirmation' => 'required|min:' . config('password.secondary_password_min_characters'),

        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'current_secondary_password'      => __('a_change_password.current secondary password'),
            'secondary_password'              => __('a_change_password.secondary password'),
            'secondary_password_confirmation' => __('a_change_password.confirm secondary password'),
        ];
    }

    public function messages()
    {
        return [
            'secondary_password.min'              => __('a_change_password.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')]),
            'secondary_password_confirmation.min' => __('a_change_password.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')]),
        ];
    }
}
