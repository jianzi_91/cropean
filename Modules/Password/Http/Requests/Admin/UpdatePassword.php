<?php

namespace Modules\Password\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordDifferent;
use Modules\Password\Http\Rules\PasswordMatch;
use Modules\Password\Http\Rules\PasswordRegex;

class UpdatePassword extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        $rules = [
            'current_password' => [
                'required',
                new PasswordMatch(),
            ],
            'password' => [
                'required',
                'min:' . config('password.primary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new PasswordDifferent($user->id)
            ],
            'password_confirmation' => 'required|min:' . config('password.primary_password_min_characters'),
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'current_password'      => __('a_change_password.current password'),
            'password'              => __('a_change_password.password'),
            'password_confirmation' => __('a_change_password.confirm password'),
        ];
    }

    public function messages()
    {
        return [
            'password.min'              => __('a_change_password.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')]),
            'password_confirmation.min' => __('a_change_password.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')])
        ];
    }
}
