<?php

namespace Modules\Password\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Contracts\UserContract;

class Edit extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Determine if the user is authorized to make this request.
     *d
     * @return bool
     */
    public function authorize()
    {
        $user = resolve(UserContract::class)->find($this->id);
        return $user && $user->is_member;
    }
}
