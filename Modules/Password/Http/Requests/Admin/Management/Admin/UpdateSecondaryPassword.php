<?php

namespace Modules\Password\Http\Requests\Admin\Management\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\Password\Http\Rules\SecondaryPasswordDifferent;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Rules\CheckOwnership;

class UpdateSecondaryPassword extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'secondary_password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new SecondaryPasswordDifferent($this->id)
            ],
            'secondary_password_confirmation' => 'required|min:' . config('password.secondary_password_min_characters'),

        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->userColumn = 'id';
        $user             = resolve(UserContract::class)->find($this->id);
        return $this->isOwner($user, $this->user(), true) || $user->is_admin;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'secondary_password'              => __('a_admin_management_change_password.password'),
            'secondary_password_confirmation' => __('a_admin_management_change_password.confirm password'),
        ];
    }
}
