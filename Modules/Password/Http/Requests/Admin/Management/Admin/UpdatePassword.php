<?php

namespace Modules\Password\Http\Requests\Admin\Management\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordDifferent;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Rules\CheckOwnership;

class UpdatePassword extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'password' => [
                'required',
                'min:' . config('password.primary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new PasswordDifferent($this->id)
            ],
            'password_confirmation' => 'required|min:' . config('password.primary_password_min_characters'),
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *d
     * @return bool
     */
    public function authorize()
    {
        $this->userColumn = 'id';
        $user             = resolve(UserContract::class)->find($this->id);
        return $this->isOwner($user, $this->user(), true) || $user->is_admin;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'password'              => __('a_admin_management_change_password.password'),
            'password_confirmation' => __('a_admin_management_change_password.confirm password'),
        ];
    }
}
