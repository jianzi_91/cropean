<?php

namespace Modules\Password\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordMatch;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\Password\Http\Rules\SecondaryPasswordDifferent;

class ResetSecondaryPassword extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();

        $rules = [
            'reset_current_password' => [
                'required',
                new PasswordMatch(),
            ],
            'reset_secondary_password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new SecondaryPasswordDifferent($user->id)
            ],
            'reset_secondary_password_confirmation' => 'required|min:' . config('password.secondary_password_min_characters'),

        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'reset_current_password'                => __('m_change_password.current primary password'),
            'reset_secondary_password'              => __('m_change_password.new secondary password'),
            'reset_secondary_password_confirmation' => __('m_change_password.confirm new secondary password'),
        ];
    }
}
