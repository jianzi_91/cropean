<?php

namespace Modules\Password\Http\Requests\Member\ResetSecondaryPassword;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\Password\Http\Rules\SecondaryPasswordDifferent;
use Modules\Password\Http\Rules\ValidateSecondaryPasswordToken;
use Modules\User\Contracts\UserContract;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userRepository = resolve(UserContract::class);
        $user           = $userRepository->findByEmail($this->email);

        return [
            'bail',
            'token'    => ['required', new ValidateSecondaryPasswordToken($user ? $user->id : null)],
            'password' => [
                'required',
                'min:' . config('password.secondary_password_min_characters'),
                'confirmed',
                new PasswordRegex(),
                new SecondaryPasswordDifferent($user ? $user->id : null),

            ],
            'password_confirmation' => 'required|min:' . config('password.secondary_password_min_characters'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'password'              => __('a_reset_password.new secondary password'),
            'password_confirmation' => __('a_reset_password.confirm new secondary password'),
        ];
    }
}
