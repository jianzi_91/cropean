<?php

namespace Modules\Password\Http\Controllers\Admin\Management\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Modules\Password\Http\Requests\Admin\Management\Admin\Edit;
use Modules\Password\Http\Requests\Admin\Management\Admin\UpdatePassword;
use Modules\Password\Http\Requests\Admin\Management\Admin\UpdateSecondaryPassword;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class PasswordController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The class constructor
     *
     * @param UserContract     $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('permission:admin_user_admin_management_update_password')->only('updatePassword');
        $this->middleware('permission:admin_user_admin_management_update_secondary_password')->only('updateSecondaryPassword');

        $this->userRepository = $userContract;
    }

    /**
     * Edit password.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management_change_password.user not found'));
        }

        if(!$user->can('admin_user_admin_management_update_password')&&!$user->can('admin_user_admin_management_update_secondary_password'))
            abort('403');

        return view('password::admin.management.admin.edit', compact('user'));
    }

    /**
     * Update password
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updatePassword(UpdatePassword $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management_change_password.user not found'));
        }

        DB::beginTransaction();

        try {
            $data             = $request->only(['password']);
            $data['password'] = bcrypt($data['password']);

            if (!($result = $this->userRepository->edit($id, $data))) {
                return back()->withErrors('error', __('a_admin_management_change_password.fail to update user password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.password.edit', $id))->withMessage(__('a_admin_management_change_password.failed to update password'));
        }

        return back()->with('success', __('a_admin_management_change_password.update user password successfully'));
    }

    /**
     * Update secondary password
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updateSecondaryPassword(UpdateSecondaryPassword $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management_change_password.user not found'));
        }

        DB::beginTransaction();

        try {
            $data                       = $request->only(['secondary_password']);
            $data['secondary_password'] = bcrypt($data['secondary_password']);

            if (!($result = $this->userRepository->edit($id, $data))) {
                return back()->withErrors('error', __('a_admin_management_change_password.fail to update user secondary password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.password.edit', $id))->withMessage(__('a_admin_management_change_password.failed to update secondary password'));
        }

        return back()->with('success', __('a_admin_management_change_password.update user secondary password successfully'));
    }
}
