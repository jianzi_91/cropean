<?php

namespace Modules\Password\Http\Controllers\Admin\Management\Member;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Modules\Password\Http\Requests\Admin\Management\Member\Edit;
use Modules\Password\Http\Requests\Admin\Management\Member\UpdatePassword;
use Modules\Password\Http\Requests\Admin\Management\Member\UpdateSecondaryPassword;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class PasswordController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The class constructor
     *
     * @param UserContract     $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('permission:admin_user_member_management_update_password')->only('edit', 'updatePassword');
        $this->middleware('permission:admin_user_member_management_update_secondary_password')->only('updateSecondaryPassword');

        $this->userRepository = $userContract;
    }

    /**
     * Edit password.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_password.user not found'));
        }

        return view('password::admin.management.member.edit', compact('user'));
    }

    /**
     * Update password
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updatePassword(UpdatePassword $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_password.user not found'));
        }

        DB::beginTransaction();

        try {
            $data             = $request->only(['password']);
            $data['password'] = bcrypt($data['password']);

            if (!($result = $this->userRepository->edit($id, $data))) {
                return back()->withErrors('error', __('a_member_management_change_password.fail to update user password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.password.edit', $id))->withMessage(__('a_member_management_change_password.failed to update password'));
        }

        return back()->with('success', __('a_member_management_change_password.update user password successfully'));
    }

    /**
     * Update secondary password
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function updateSecondaryPassword(UpdateSecondaryPassword $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management_change_password.user not found'));
        }

        DB::beginTransaction();

        try {
            $data                       = $request->only(['secondary_password']);
            $data['secondary_password'] = bcrypt($data['secondary_password']);

            if (!($result = $this->userRepository->edit($id, $data))) {
                return back()->withErrors('error', __('a_member_management_change_password.fail to update user secondary password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.password.edit', $id))->withMessage(__('a_member_management_change_password.failed to update secondary password'));
        }

        return back()->with('success', __('a_member_management_change_password.update user secondary password successfully'));
    }
}
