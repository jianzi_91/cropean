<?php

namespace Modules\Password\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Password;
use Modules\Password\Http\Requests\Admin\ForgotPassword\Store;
use Modules\User\Contracts\UserContract;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->userRepository = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('password::admin.forgot_password.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $user = $this->userRepository->findBySlug($request->email);

        if (!$user) {
            return back()->withErrors('error', __('a_forgot_password.user not found'));
        }

        $request->merge([
            'email' => $user->email
        ]);

        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
            ? redirect(route('admin.login'))->with('success', __('a_forgot_password.reset password email sent'))
            : $this->sendResetLinkFailedResponse($request, $response);
    }
}
