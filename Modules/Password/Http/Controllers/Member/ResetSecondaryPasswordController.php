<?php

namespace Modules\Password\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Modules\Password\Contracts\PasswordContract;
use Modules\Password\Http\Requests\Member\ResetSecondaryPassword\Store;
use Modules\User\Contracts\UserContract;

class ResetSecondaryPasswordController extends Controller
{
    protected $userRepository;
    protected $passwordRepository;

    public function __construct(UserContract $userContract, PasswordContract $passwordContract)
    {
        $this->userRepository     = $userContract;
        $this->passwordRepository = $passwordContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $secondaryPassword = optional(DB::table('secondary_password_resets')
            ->where('email', $request->email)
            ->first());

        if ($secondaryPassword) {
            if (Hash::check($request->token, $secondaryPassword->token)) {
                if (isset(auth()->user()->email) && auth()->user()->email != $secondaryPassword->email) {
                    Session::put('reset_error', __('m_reset_secondary_password.invalid auth to reset password'));

                    return redirect(route('member.login'));
                }
                return view('password::member.reset_secondary_password.index')
                    ->with([
                        'token' => $request->token
                    ]);
            }
        }

        Session::put('reset_error', __('m_reset_secondary_password.reset secondary password token expired'));
        return redirect(route('member.login'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $user = $this->userRepository->findByEmail($request->email);

        if (!$user) {
            return back()->withErrors('error', __('m_reset_secondary_password.user not found'));
        }

        $request->merge([
            'email' => $user->email
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = Password::PASSWORD_RESET;
        $this->passwordRepository->updateSecondary($user->id, $request->password);
        $user->save();

        // delete password resets
        DB::table('secondary_password_resets')->where('email', $user->email)->delete();

        if ($response == Password::PASSWORD_RESET) {
            Session::put('reset_success', __('m_reset_secondary_password.reset secondary password successful'));
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? redirect()->route('member.login')
            : $this->sendResetFailedResponse($request, $response);
    }
}
