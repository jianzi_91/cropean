<?php

namespace Modules\Password\Http\Controllers\Member;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Password\Http\Requests\Member\ResetSecondaryPassword;
use Modules\Password\Http\Requests\Member\UpdatePassword;
use Modules\Password\Http\Requests\Member\UpdateSecondaryPassword;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;

class PasswordController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The class constructor
     *
     * @param UserContract     $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('permission:member_profile_update_password')->only('updatePassword');
        $this->middleware('permission:member_profile_update_secondary_password')->only('updateSecondaryPassword');
        $this->middleware('permission:member_profile_reset_secondary_password')->only('resetSecondaryPassword');

        $this->userRepository = $userContract;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit()
    {
        $user = auth()->user();

        if (!$user) {
            return back()->withErrors('error', __('m_change_password.user not found'));
        }

        if (!$user->can('member_profile_update_password') && !$user->can('member_profile_update_secondary_password') && !$user->can('member_profile_reset_secondary_password')) {
            abort('403');
        }

        return view('password::member.edit_password.edit', compact($user));
    }

    /**
     * Update password
     *
     * @param  Request $request
     * @return Response
     */
    public function updatePassword(UpdatePassword $request)
    {
        $user = auth()->user();

        DB::beginTransaction();

        try {
            $data             = $request->only(['password']);
            $data['password'] = bcrypt($data['password']);

            if (!($result = $this->userRepository->edit($user->id, $data))) {
                return back()->withErrors('error', __('m_change_password.fail to update password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.password.edit'))->withMessage(__('m_change_password.failed to update password'));
        }

        return back()->with('success', __('m_change_password.update password successfully'));
    }

    /**
     * Update secondary password
     *
     * @param  Request $request
     * @return Response
     */
    public function updateSecondaryPassword(UpdateSecondaryPassword $request)
    {
        $user = auth()->user();

        DB::beginTransaction();

        try {
            $data                       = $request->only(['secondary_password']);
            $data['secondary_password'] = bcrypt($data['secondary_password']);

            if (!($result = $this->userRepository->edit($user->id, $data))) {
                return back()->withErrors('error', __('m_change_password.fail to update secondary password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.password.edit'))->withMessage(__('m_change_password.failed to update secondary password'));
        }

        return back()->with('success', __('m_change_password.update secondary password successfully'));
    }

    /**
     * Reset secondary password
     *
     * @param  Request $request
     * @return Response
     */
    public function resetSecondaryPassword(ResetSecondaryPassword $request)
    {
        $user = auth()->user();

        DB::beginTransaction();

        try {
            $data                       = $request->only(['reset_secondary_password']);
            $data['secondary_password'] = bcrypt($data['reset_secondary_password']);

            if (!($result = $this->userRepository->edit($user->id, $data))) {
                return back()->withErrors('error', __('m_change_password.fail to update secondary password'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.password.edit'))->withMessage(__('m_change_password.failed to reset secondary password'));
        }

        return back()->with('success', __('m_change_password.reset secondary password successfully'));
    }
}
