<?php

namespace Modules\Password\Http\Controllers\Member;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Modules\Password\Contracts\PasswordContract;
use Modules\Password\Notifications\ResetSecondaryPassword;
use Modules\User\Contracts\UserContract;

class ForgotSecondaryPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    protected $passwordRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract, PasswordContract $passwordContract)
    {
        $this->userRepository     = $userContract;
        $this->passwordRepository = $passwordContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('password::member.forgot_secondary_password.index');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $this->userRepository->findByEmail(auth()->user()->email);

        if (!$user) {
            return back()->withErrors('error', __('m_forgot_secondary_password.user not found'));
        }

        $request->merge([
            'email' => $user->email
        ]);

        $this->validateEmail($request);

        // delete existing and insert a new records
        $token = $this->passwordRepository->createNewSecondaryPasswordResetRequest($user);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = Password::RESET_LINK_SENT;
        $user->notify(new ResetSecondaryPassword($user, $token));
        // $response = $this->broker()->sendResetLink(
        //     $request->only('email')
        // );

        return $response == Password::RESET_LINK_SENT
            ? back()->with('success', __('m_forget_secondary_password.please check email'))
            : $this->sendResetLinkFailedResponse($request, $response);
    }
}
