<?php

namespace Modules\Password\Http\Controllers\Member;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Modules\Password\Contracts\PasswordContract;
use Modules\Password\Http\Requests\Member\ResetPassword\Store;
use Modules\User\Contracts\UserContract;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * The password respository
     *
     * @var unknown
     */
    protected $passwordRepository;

    /**
     * The user respository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Class constructor
     *
     * @param PasswordContract $passwordContract
     * @param UserContract $userContract
     */
    public function __construct(PasswordContract $passwordContract, UserContract $userContract)
    {
        $this->passwordRepository = $passwordContract;
        $this->userRepository     = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $token = null)
    {
        $password = optional(DB::table('password_resets')
            ->where('email', $request->email)
            ->first());

        if ($password && Hash::check($request->token, $password->token)) {
            return view('password::member.reset_password.index')
                ->with([
                    'token' => $request->token,
                    'email' => $request->email
                ]);
        }

        Session::put('reset_error', __('m_reset_password.reset password token expired'));
        return redirect(route('member.login'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        $user = $this->userRepository->findByEmail($request->email);

        if (!$user) {
            return back()->withErrors('error', __('m_reset_password.user not found'));
        }

        $request->merge([
            'email' => $user->email
        ]);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $password = $request->password;
        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->passwordRepository->updatePrimary($user->id, $password);
            }
        );

        if ($response == Password::PASSWORD_RESET) {
            Session::put('reset_success',  __('m_reset_password.reset password successful'));
        }

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? redirect(route('member.login'))
            : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        if ($response == 'passwords.token') {
            $errors = ['email' => trans($response)];
        } else {
            $errors = ['email' => trans($response)];
        }

        return back()->withErrors($errors);
    }
}
