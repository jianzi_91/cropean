<?php

namespace Modules\Password\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\User\Models\User;

class ValidateSecondaryPasswordToken implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @var $userId
     */
    protected $userId;

    /**
     * Create a new rule instance.
     *
     * @param unknown $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::find($this->userId);

        $secondaryPassword = optional(DB::table('secondary_password_resets')
            ->where('email', $user->email)
            ->first());

        return $secondaryPassword? Hash::check($value, $secondaryPassword->token): false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.:attribute invalid token');
    }
}
