<?php

namespace Modules\Password\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordRegex implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match(config('password.format'), $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.:attribute minimum five characters, at least one number, one letter');
    }
}
