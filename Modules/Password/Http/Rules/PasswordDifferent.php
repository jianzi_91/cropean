<?php

namespace Modules\Password\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Modules\User\Models\User;

class PasswordDifferent implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @var  $userId
     */
    protected $userId;

    /**
     * Create a new rule instance.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::find($this->userId);

        return $user ? !Hash::check($value, $user->secondary_password) : false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.:attribute must be different with secondary password');
    }
}
