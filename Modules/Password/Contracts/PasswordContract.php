<?php

namespace Modules\Password\Contracts;

use Illuminate\Database\Eloquent\ModelNotFoundException;

interface PasswordContract
{
    /** Generate random password
     *
     * @param int $length   Length of password
     * @return string
     */
    public function generateRandomPassword(int $length = 10);

    /**
     * Reset user primary password
     *
     * @param int $userId
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function resetPrimary(int $userId);

    /**
     * Update user primary password
     *
     * @param int $userId
     * @param string $password
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function updatePrimary(int $userId, string $password);

    /**
     * Reset user secondary password
     *
     * @param int $userId
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function resetSecondary(int $userId);

    /**
     * Update user secondary password
     *
     * @param int $userId
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function updateSecondary(int $userId, string $password);

    /**
     * Validate primary password
     *
     * @param int $userId
     * @param string $password
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function validatePrimary(int $userId, string $password);

    /**
     * Validate secondary password
     *
     * @param int $userId
     * @param string $password
     * @return boolean
     * @throws ModelNotFoundException
     */
    public function validateSecondary(int $userId, string $password);
}
