<?php

return [
    'name'     => 'Password',
    'bindings' => [
        Modules\Password\Contracts\PasswordContract::class => Modules\Password\Repositories\PasswordRepository::class
    ],
    'format'                            => env('PASSWORD_FORMAT', '/(?=.*[A-Za-z])(?=.*[0-9]?)/'),
    'primary_password_min_characters'   => 6,
    'secondary_password_min_characters' => 6,
];
