@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',
        [
        'breadcrumbs' => [
            ['name' => __('a_member_management_leader_bonus.dashboard'), 'route' => route('admin.dashboard')],
            ['name' => __('a_member_management_leader_bonus.member management'), 'route' => route('admin.management.members.index')],
            ['name' => __('a_member_management_leader_bonus.leader bonus rank')]
        ],
        'header'=>__('a_member_management_leader_bonus.profile settings'),
        'backRoute'=>route('admin.management.members.index'),
    ])

@include('templates.admin.includes._mm-nav', ['page' => 'leader bonus rank', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    {{ Form::open(['route' => ['admin.management.members.leader.update', $user->id], 'method'=>'put', 'id'=>'leader-bonus-rank', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formSelect('manual_rank_id', ranks_dropdown(), $userRank && $userRank->is_manual ? $userRank->rank_id : [], __('a_member_management_leader_bonus.manual rank'), ['required'=>true]) }}
                    <input type="hidden" value="false" name="is_locked" id="is_locked_hidden" />
                    {{ Form::formSelect('natural_rank_id', ranks_dropdown(), $userRank->qualified_rank_id ?? old('natural_rank_id'), __('a_member_management_leader_bonus.natural rank'), ['readonly'=> true, 'disabled' => true]) }}
                    @can ('admin_user_management_member_rank_lock')
                        {{ Form::formCheckbox('is_locked', 'is_locked', '', __('a_member_management_leader_bonus.leader bonus rank lock'), ['isChecked'=>$userRank->is_locked ?? false, 'class'=>'js-checkbox']) }}
                    @endcan
                    <div class="row d-flex justify-content-end m-0 p-0">
                        {{ Form::formButtonPrimary('btn_submit', __('a_member_management_leader_bonus.save')) }}
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
    <script type="text/javascript">
        $(function() {
            @isset($userRank->is_locked)
            $('#is_locked_hidden').attr('disabled', true)
            @endisset

            var submitted = false;

            $('#btn_submit').on('click', function() {
                var cb = $('#is_locked').is(':checked') ? true : false
                $('#is_locked').val(cb)
                if (cb) $('#is_locked_hidden').attr('disabled', true)
                else $('#is_locked_hidden').attr('disabled', false)

                if(!submitted){
                    submitted = true;
                    $('#leader-bonus-rank').submit()
                }
            })

            if ($('#manual_rank_id').val() == "") {
                $('#is_locked').attr('disabled', 'disabled');
            }

            $('#manual_rank_id').on('change', function() {
                if ($(this).val() == "") {
                    $('#is_locked').prop('checked', false);
                    $('#is_locked').attr('disabled', 'disabled');
                } else {
                    $('#is_locked').removeAttr('disabled');
                }
            });
        })
    </script>
@endpush