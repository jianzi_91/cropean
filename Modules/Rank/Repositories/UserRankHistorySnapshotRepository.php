<?php

namespace Modules\Rank\Repositories;

use Modules\Rank\Contracts\UserRankHistorySnapshotContract;
use Modules\Rank\Models\UserRankHistorySnapshot;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserRankHistorySnapshotRepository extends Repository implements UserRankHistorySnapshotContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserRankHistorySnapshot $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
