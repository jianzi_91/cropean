<?php

namespace Modules\Rank\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Models\GoldmineRank;
use Modules\Rank\Models\UserRankHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class GoldmineRankRepository extends Repository implements GoldmineRankContract
{
    use HasCrud, HasSlug, HasHistory, HasNestedSet;

    public function __construct(GoldmineRank $model, UserGoldmineLevelHistory $userGoldmineRankHistories)
    {
        $this->historyModel = $userGoldmineRankHistories;
        $this->model        = $model;
        $this->slug         = 'name';

        parent::__construct($model);
    }
}
