<?php

namespace Modules\Rank\Repositories;

use Modules\Rank\Contracts\UserGoldmineRankHistoryContract;
use Modules\Rank\Models\UserGoldmineRankHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserGoldmineRankHistoryRepository extends Repository implements UserGoldmineRankHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserGoldmineRankHistory $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
