<?php

namespace Modules\Rank\Repositories;

use Modules\Rank\Contracts\UserGoldmineRankHistorySnapshotContract;
use Modules\Rank\Models\UserGoldmineRankHistorySnapshot;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserGoldmineRankHistorySnapshotRepository extends Repository implements UserGoldmineRankHistorySnapshotContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserGoldmineRankHistorySnapshot $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
