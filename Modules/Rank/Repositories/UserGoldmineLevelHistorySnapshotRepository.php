<?php

namespace Modules\Rank\Repositories;

use Modules\Rank\Contracts\UserGoldmineLevelHistorySnapshotContract;
use Modules\Rank\Models\UserGoldmineLevelHistorySnapshot;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserGoldmineLevelHistorySnapshotRepository extends Repository implements UserGoldmineLevelHistorySnapshotContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserGoldmineLevelHistorySnapshot $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
