<?php

namespace Modules\Rank\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Models\Rank;
use Modules\Rank\Models\UserRankHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class RankRepository extends Repository implements RankContract
{
    use HasCrud, HasSlug, HasHistory, HasNestedSet;

    public function __construct(Rank $model, UserRankHistory $userRankHistories)
    {
        $this->historyModel = $userRankHistories;
        $this->model        = $model;
        $this->slug         = 'name';

        parent::__construct($model);
    }
}
