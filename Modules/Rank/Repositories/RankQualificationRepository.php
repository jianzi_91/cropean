<?php

namespace Modules\Rank\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Rank\Models\UserRankHistory;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class RankQualificationRepository extends Repository
{
    use HasNestedSet;

    protected $rankUpDate;
    protected $leaderBonusRanks;
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $currentLeaderBonusRank;
    protected $userLeaderBonusManualRank    = [];
    protected $userLeaderBonusQualifiedRank = [];
    protected $userLeaderBonusRankLocked    = [];
    protected $convertTotalEquityExchangeRates;
    protected $amounts;
    protected $totalEquity;
    protected $creditPrecision;

    /**
     * The percentage column
     *
     * @var string
     */
    protected $percent = 'percentage';

    public function __construct(array $sponsorTrees = [], array $amounts = [])
    {
        parent::__construct();
        $this->amounts = $amounts;
        if (!is_null($sponsorTrees)) {
            $this->buildTree($sponsorTrees);
        }
    }

    public function setRankQualificationCheckDate($rankUpDate)
    {
        $this->rankUpDate = $rankUpDate;
    }

    public function setLeaderBonusRank($leaderBonusRank)
    {
        $this->leaderBonusRanks = $leaderBonusRank;
    }

    public function setUserLeaderBonusRank($userLeaderBonusRankHistory)
    {
        $this->currentLeaderBonusRank       = $userLeaderBonusRankHistory;
        $this->userLeaderBonusManualRank    = $userLeaderBonusRankHistory->where('is_manual', 1)->pluck('rank_id', 'user_id')->toArray();
        $this->userLeaderBonusQualifiedRank = $userLeaderBonusRankHistory->pluck('qualified_rank_id', 'user_id')->toArray();
        $this->userLeaderBonusRankLocked    = $userLeaderBonusRankHistory->where('is_locked', 1)->pluck('rank_id', 'user_id')->toArray();
    }

    public function setExchangeRate($exchangeRates)
    {
        $capitalCredit  = BankCreditTypeContract::CAPITAL_CREDIT;
        $rolloverCredit = BankCreditTypeContract::ROLL_OVER_CREDIT;

        $this->convertTotalEquityExchangeRates = [
            $capitalCredit  => $exchangeRates[$capitalCredit]['sell_rate'] ?? 1,
            $rolloverCredit => $exchangeRates[$rolloverCredit]['sell_rate'] ?? 1
        ];

        # convert amount to total equity
        $this->convertUserTotalEquity();
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function qualification($precision = 2)
    {
        $this->creditPrecision = $precision;

        if ($this->tree) {
            foreach ($this->tree as $user) {
                $userId = $user->getUserId();
                $node   = $this->getNodeByUser($userId);

                $userDownlinesPerLine = [];
                $directDownlines      = $this->getDirectDownlines($node);
                $directDownlineEquity = [];

                if ($directDownlines) {
                    foreach ($directDownlines as $downline) {
                        $downlineUserId         = $downline->getUserId();
                        $downlineNode           = $this->getNodeByUser($downlineUserId);
                        $userDownlinesPerLine[] = $this->getDownlines($downlineNode, true); //with direct downline its own

                        $directDownlineEquity[] = isset($this->totalEquity[$downlineUserId])? $this->totalEquity[$downlineUserId] :0;
                    } #end foreach directdownlines
                } #end if $directDownlines

                $allDownlineEquity = $this->downlineSales($userDownlinesPerLine);

                /* If manual ranking is enabled then consider the rank_id column otherwise consider the qualified_rank_id column for the calculations */
                if (setting('is_manual_leader_bonus_rankup_activated', 0) == 1) {
                    $downlineRanks = $this->countDownlineRank($userDownlinesPerLine, $this->userLeaderBonusManualRank);
                } else {
                    $downlineRanks = $this->countDownlineRank($userDownlinesPerLine, $this->userLeaderBonusQualifiedRank);
                }

                // assign current manual rank id
                $manualRankId = isset($this->userLeaderBonusManualRank[$userId]) ? $this->userLeaderBonusManualRank[$userId] : null;

                if (in_array($userId, $this->nonEligibleReceiveUsers)) {
                    $qualifiedRankId = isset($this->userLeaderBonusQualifiedRank[$userId]) ? $this->userLeaderBonusQualifiedRank[$userId] : null;
                } else {
                    $personalEquity  = $this->getPersonalEquity($userId);
                    $qualifiedRankId = $this->getRank($userId, $personalEquity, $directDownlineEquity, $allDownlineEquity, $downlineRanks);
                }

                # to check manual rank up
                if ($manualRankId && $manualRankId > $qualifiedRankId) {
                    $this->userLeaderBonusManualRank[$userId]    = $manualRankId;
                    $this->userLeaderBonusQualifiedRank[$userId] = $qualifiedRankId;
                } else {
                    $this->userLeaderBonusManualRank[$userId]    = $qualifiedRankId;
                    $this->userLeaderBonusQualifiedRank[$userId] = $qualifiedRankId;
                } #end if $manualRankId && $manualRankId > $qualifiedRankId

                #check is rank locked or not
                if (isset($this->userLeaderBonusRankLocked[$userId])) {
                    $this->userLeaderBonusManualRank[$userId] = $this->userLeaderBonusRankLocked[$userId];
                } #end if
            } #end foreach $this->tree
        } #end if not empty $this->tree
    }

    public function updateRank()
    {
        $userRankHistories        = [];
        $userIds                  = [];
        $updatedBy                = $this->currentLeaderBonusRank->pluck('updated_by', 'user_id');
        $currentUserRank          = $this->currentLeaderBonusRank->pluck('rank_id', 'user_id');
        $currentQualifiedUserRank = $this->currentLeaderBonusRank->pluck('qualified_rank_id', 'user_id');
        $newRankUser              = [];
        $noRankUser               = [];

        foreach ($this->userLeaderBonusManualRank as $userId => $rankId) {
            $isManual    = 0;
            $isLocked    = 0;
            $updatedUser = null;

            if ($rankId) {
                if (is_null($this->userLeaderBonusQualifiedRank[$userId])) {
                    $isManual    = 1;
                    $updatedUser = $updatedBy[$userId];
                } else {
                    if ($this->userLeaderBonusQualifiedRank[$userId] < $rankId) {
                        $isManual    = 1;
                        $updatedUser = $updatedBy[$userId];
                    }
                }

                if (isset($this->userLeaderBonusRankLocked[$userId])) {
                    $isLocked    = 1;
                    $isManual    = 1;
                    $updatedUser = $updatedBy[$userId];
                }

                // only insert for upgrade or downgrade rank and also not available in user_rank_histories
                if (!isset($currentUserRank[$userId])
                    || (isset($currentUserRank[$userId]) && $currentUserRank[$userId] != $rankId)
                    || !isset($currentQualifiedUserRank[$userId])
                    || (isset($currentQualifiedUserRank[$userId]))) {
                    $userRankHistories[] = [
                        'user_id'           => $userId,
                        'rank_id'           => $rankId,
                        'qualified_rank_id' => $this->userLeaderBonusQualifiedRank[$userId] ?? null,
                        'is_current'        => 1,
                        'is_manual'         => $isManual,
                        'is_locked'         => $isLocked,
                        'updated_by'        => $updatedUser,
                        'created_at'        => $this->rankUpDate,
                        'updated_at'        => $this->rankUpDate,
                    ];

                    $userIds[]              = $userId;
                    $newRankUser[$rankId][] = $userId;
                }
            } else {
                //if found rank history then insert or update
                $noRankUser[] = $userId;
            }
        } #end foreach

        //Update user rank histories and user rank_id
        if ($userRankHistories) {
            $this->updateUserRankHistory($userRankHistories, $userIds, $newRankUser);
        } #end if

        if ($noRankUser) {
            //update no rank id, down rank to no rank
            $this->updateNoRankUsers($noRankUser);
        }
    }

    private function updateNoRankUsers($noRankUser)
    {
        foreach (array_chunk($noRankUser, 1000) as $noRankUserId) {
            UserRankHistory::where('is_current', 1)->whereIn('user_id', $noRankUserId)->update([
                'is_current' => 0,
            ]);

            User::whereIn('id', $noRankUserId)->update([
                'rank_id' => null,
            ]);
        }
    }

    private function updateUserRankHistory($userRankHistories = [], $userIds = [], $newRankUser = [])
    {
        foreach (array_chunk($userIds, 1000) as $uids) {
            UserRankHistory::where('is_current', 1)->whereIn('user_id', $uids)->update([
                'is_current' => 0,
            ]);
        }

        foreach (array_chunk($userRankHistories, 1000) as $rankHistories) {
            UserRankHistory::insert($rankHistories);
        }

        foreach ($newRankUser as $rankID => $userIDs) {
            User::whereIn('id', $userIDs)->update([
                'rank_id' => $rankID,
            ]);
        }
    }

    private function convertUserTotalEquity()
    {
        foreach ($this->amounts as $userId => $userBalances) {
            $capitalCredit              = $userBalances['capital_credit'] ?? 0;
            $rollOverCredit             = $userBalances['roll_over_credit'] ?? 0;
            $this->totalEquity[$userId] = get_total_equity($this->convertTotalEquityExchangeRates, $capitalCredit, $rollOverCredit);
        }
    }

    private function downlineSales($downlines)
    {
        $downlineSales = [];
        foreach ($downlines as $downline) {
            $totalSalesPerLine = 0;

            foreach ($downline as $user) {
                $userId = $user->getUserId();
                if (isset($this->totalEquity[$userId])) {
                    $totalSalesPerLine = bcadd($totalSalesPerLine, $this->totalEquity[$userId], 3);
                }
            }

            $downlineSales[] = $totalSalesPerLine;
        }

        return $downlineSales;
    }

    private function countDownlineRank($downlines, $userRanks)
    {
        $downlineTotalRanks = [];
        foreach ($downlines as $downline) {
            $downlineRanks = [];

            foreach ($this->leaderBonusRanks as $rank) {
                $downlineRanks[$rank->id] = 0;
            }

            foreach ($downline as $user) {
                $user_id = $user->getUserId();
                if (isset($userRanks[$user_id]) && !in_array($user_id, $this->nonEligibleContributionUsers)) {
                    $rankId = $userRanks[$user_id];
                    if (isset($downlineRanks[$rankId])) {
                        $downlineRanks[$rankId] += 1;
                    }
                }
            }

            $downlineTotalRanks[] = $downlineRanks;
        }

        return $downlineTotalRanks;
    }

    private function getPersonalEquity($userId)
    {
        $totalPersonalSales = 0;
        if (isset($this->totalEquity[$userId])) {
            $totalPersonalSales = bcadd($totalPersonalSales, $this->totalEquity[$userId], 3);
        }

        return $totalPersonalSales;
    }

    private function getRank($userId, $personalEquity, array $directDownlineEquities, array $allDownlineEquities, array $downlineRanks, $currentRankId = null)
    {
        $newRankId = null;
        foreach ($this->leaderBonusRanks as $leaderBonusRank) {
            if (!empty($leaderBonusRank->qualification_condition)) {
                $rankCriteria = json_decode($leaderBonusRank->qualification_condition);

                #1 check personal equity
                #2 check direct downline total equity amount
                #3 check total downline equity
                #4 check total rank in line
                $isPersonalEquity = $this->isPersonalEquity($rankCriteria->sales->personal, $personalEquity);
                //$isDirectDownlineTotalEquity = $this->isDirectDownlineTotalEquity($rankCriteria->sales->direct_downlines, $directDownlineEquities);
                $isDownlineTotalEquity = $this->isDownlineTotalEquity($rankCriteria->sales->downlines, $allDownlineEquities);
                $isDownlineRank        = $this->isDownlineRank($rankCriteria->rank, $downlineRanks);

                if ($isPersonalEquity && $isDownlineTotalEquity && $isDownlineRank) {
                    $newRankId = $leaderBonusRank->id;
                    break;
                }
            } #end if !empty($leaderBonusRank->qualification_condition)
        } #end foreach $this->leaderBonusRanks
        return $newRankId;
    }

    private function isPersonalEquity($personalEquityCriteria, $personalEquity)
    {
        return !empty($personalEquityCriteria) && $personalEquityCriteria > 0 && $personalEquity >= $personalEquityCriteria;
    }

    private function isDirectDownlineTotalEquity($directDownlineCriteria, $directDownlineEquities)
    {
        $countDirectDownlineCriteriaPerLine = 0;
        $totalGroupEquity                   = 0;

        $isDirectDownlineTotalEquity = false;
        if ($directDownlineCriteria->is_total) {
            if ($directDownlineCriteria->amount > 0) {
                foreach ($directDownlineEquities as $directDownlineEquity) {
                    if ($directDownlineEquity > 0) {
                        $countDirectDownlineCriteriaPerLine++;
                    }
                    $totalGroupEquity = bcadd($directDownlineEquity, $totalGroupEquity, $this->creditPrecision);
                }

                if ($totalGroupEquity >= $directDownlineCriteria->amount && $countDirectDownlineCriteriaPerLine >= $directDownlineCriteria->line) {
                    $isDirectDownlineTotalEquity = true;
                }
            } else {
                # no total fixed deposit sales in direct downline then return true
                $isDirectDownlineTotalEquity = true;
            }
        } else {
            # for min equity per line
            if ($directDownlineCriteria->amount > 0) {
                foreach ($directDownlineEquities as $directDownlineEquity) {
                    if ($directDownlineCriteria->line > 0) {
                        // Per line amount checking
                        if ($directDownlineEquity >= $directDownlineCriteria->amount) {
                            ++$countDirectDownlineCriteriaPerLine;
                            if ($countDirectDownlineCriteriaPerLine == $directDownlineCriteria->line) {
                                $isDirectDownlineTotalEquity = true;
                                break;
                            }
                        }
                    } else {
                        $totalGroupEquity += $directDownlineEquity;
                        if ($totalGroupEquity >= $directDownlineCriteria->amount) {
                            $isDirectDownlineTotalEquity = true;
                            break;
                        }
                    }
                }
            } else {
                // no sales amount condition, thus this always be true
                $isDirectDownlineTotalEquity = true;
            } #end direct downline sales no sum of total lines
        }

        return $isDirectDownlineTotalEquity;
    }

    private function isDownlineTotalEquity($downlineCriteria, $downlineEquities)
    {
        $totalGroupEquity                = 0;
        $countTotalDownlineEquityPerLine = 0;
        $isDownlineTotalEquity           = false;

        if ($downlineCriteria->is_total) {
            if ($downlineCriteria->amount > 0) {
                foreach ($downlineEquities as $downlineEquity) {
                    $totalGroupEquity = bcadd($downlineEquity, $totalGroupEquity, $this->creditPrecision);
                }

                if ($totalGroupEquity >= $downlineCriteria->group_equity) {
                    $isDownlineTotalEquity = true;
                }
            }
        } else {
            //for min sales per line
            if ($downlineCriteria->amount > 0) {
                foreach ($downlineEquities as $downlineEquity) {
                    $totalGroupEquity = bcadd($totalGroupEquity, $downlineEquity, $this->creditPrecision);

                    if ($downlineCriteria->line > 0) {
                        // Per line amount checking
                        if ($downlineEquity >= $downlineCriteria->amount) {
                            ++$countTotalDownlineEquityPerLine;
                        }

                        if ($countTotalDownlineEquityPerLine >= $downlineCriteria->line && $totalGroupEquity >= $downlineCriteria->group_equity) {
                            $isDownlineTotalEquity = true;
                            break;
                        }
                    } else {
                        if ($downlineCriteria->group_equity > 0 && $totalGroupEquity >= $downlineCriteria->group_equity) {
                            $isDownlineTotalEquity = true;
                            break;
                        }
                    }
                }
            } else {
                // no sales amount condition, thus this always be true
                $isDownlineTotalEquity = true;
            }
        }
        return $isDownlineTotalEquity;
    }

    private function isDownlineRank($downlineRankCriteria, $downlineRanks)
    {
        $countRankPerLine = 0;
        $rankCondition    = false;
        if ($downlineRankCriteria->line > 0) {
            foreach ($downlineRanks as $line) {
                $criteriaRankId = $downlineRankCriteria->rank_id;

                foreach ($line as $rankId => $count) {
                    if ($rankId >= $criteriaRankId && $count > 0) {
                        $countRankPerLine += 1;
                        break;
                    }
                }
            }

            if ($countRankPerLine >= $downlineRankCriteria->line) {
                $rankCondition = true;
            }
        } else {
            // no rank condition, thus this always be true
            $rankCondition = true;
        }

        return $rankCondition;
    }
}
