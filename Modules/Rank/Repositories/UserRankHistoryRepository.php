<?php

namespace Modules\Rank\Repositories;

use Modules\Rank\Contracts\UserRankHistoryContract;
use Modules\Rank\Models\UserRankHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserRankHistoryRepository extends Repository implements UserRankHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserRankHistory $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
