<?php

namespace Modules\Rank\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Rank\Models\UserGoldmineRankHistory;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class GoldmineRankQualificationRepository extends Repository
{
    use HasNestedSet;

    protected $checkDate;
    protected $amounts;
    protected $goldmineRanks;
    protected $currentGoldmineRank;
    protected $userGoldmineManualRank    = [];
    protected $userGoldmineQualifiedRank = [];
    protected $userGoldmineRankLocked    = [];
    protected $convertTotalEquityExchangeRates;
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $totalEquity;
    protected $creditPrecision;

    /**
     * The percentage column
     *
     * @var string
     */
    protected $percent = 'percentage';

    public function __construct(array $sponsorTrees = null, array $amounts = [])
    {
        parent::__construct();
        $this->amounts = $amounts;
        if (!is_null($sponsorTrees)) {
            $this->buildTree($sponsorTrees);
        }
    }

    public function setRankQualificationCheckDate($checkDate)
    {
        $this->checkDate = $checkDate;
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setGoldmineRank($goldmineRanks)
    {
        $this->goldmineRanks = $goldmineRanks;
    }

    public function setUserGoldmineRank($userGoldmineRankHistory)
    {
        $this->currentGoldmineRank       = $userGoldmineRankHistory;
        $this->userGoldmineManualRank    = $userGoldmineRankHistory->where('is_manual', 1)->pluck('goldmine_rank_id', 'user_id')->toArray();
        $this->userGoldmineQualifiedRank = $userGoldmineRankHistory->pluck('qualified_goldmine_rank_id', 'user_id')->toArray();
        $this->userGoldmineRankLocked    = $userGoldmineRankHistory->where('is_locked', 1)->pluck('goldmine_rank_id', 'user_id')->toArray();
    }

    public function setExchangeRate($exchangeRates)
    {
        $capitalCredit  = BankCreditTypeContract::CAPITAL_CREDIT;
        $rolloverCredit = BankCreditTypeContract::ROLL_OVER_CREDIT;

        $this->convertTotalEquityExchangeRates = [
            $capitalCredit  => $exchangeRates[$capitalCredit]['sell_rate'] ?? 1,
            $rolloverCredit => $exchangeRates[$rolloverCredit]['sell_rate'] ?? 1
        ];

        # convert amount to total equity
        $this->convertUserTotalEquity();
    }

    public function qualification($precision = 2)
    {
        $this->creditPrecision = $precision;
        if ($this->tree && $this->totalEquity) {
            foreach ($this->tree as $tree) {
                $userId = $tree->getUserId();
                $node   = $this->getNodeByUser($userId);

                $userDownlinesPerLine = [];
                $directDownlines      = $this->getDirectDownlines($node);
                $directDownlineEquity = [];

                if ($directDownlines) {
                    foreach ($directDownlines as $downline) {
                        $downlineUserId         = $downline->getUserId();
                        $downlineNode           = $this->getNodeByUser($downlineUserId);
                        $userDownlinesPerLine[] = $this->getDownlines($downlineNode, true); //include direct downline its own

                        $directDownlineEquity[] = isset($this->totalEquity[$downlineUserId])? $this->totalEquity[$downlineUserId] :0;
                    } #end foreach directdownlines
                } #end if $directDownlines

                $allDownlineEquity = $this->downlineSales($userDownlinesPerLine);

                // assign current rank id
                $manualRankId = isset($this->userGoldmineManualRank[$userId]) ? $this->userGoldmineManualRank[$userId] : null;

                if (in_array($userId, $this->nonEligibleReceiveUsers)) {
                    $qualifiedRankId = isset($this->userGoldmineQualifiedRank[$userId]) ? $this->userGoldmineQualifiedRank[$userId] : null;
                } else {
                    $personalSales   = $this->getPersonalEquity($userId);
                    $qualifiedRankId = $this->getRank($userId, $personalSales, $directDownlineEquity, $allDownlineEquity);
                }

                # to check manual rank up
                if ($manualRankId && $manualRankId > $qualifiedRankId) {
                    $this->userGoldmineManualRank[$userId]    = $manualRankId;
                    $this->userGoldmineQualifiedRank[$userId] = $qualifiedRankId;
                } else {
                    $this->userGoldmineManualRank[$userId]    = $qualifiedRankId;
                    $this->userGoldmineQualifiedRank[$userId] = $qualifiedRankId;
                }

                #check is rank locked or not
                if (isset($this->userGoldmineRankLocked[$userId])) {
                    $this->userGoldmineManualRank[$userId] = $this->userGoldmineRankLocked[$userId];
                }
            } #end foreach $this->tree
        } #end if tree and amounts
    }

    public function updateRank()
    {
        $userGoldmineRankHistories = [];
        $userIds                   = [];
        $updatedBy                 = $this->currentGoldmineRank->pluck('updated_by', 'user_id');
        $currentUserRank           = $this->currentGoldmineRank->pluck('rank_id', 'user_id');
        $currentQualifiedUserRank  = $this->currentGoldmineRank->pluck('qualified_rank_id', 'user_id');
        $newRankUser               = [];
        $noRankUser                = [];

        foreach ($this->userGoldmineManualRank as $userId => $rankId) {
            $isManual    = 0;
            $isOverride  = 0;
            $updatedUser = null;

            if ($rankId) {
                if (is_null($this->userGoldmineQualifiedRank[$userId])) {
                    $isManual    = 1;
                    $updatedUser = $updatedBy[$userId];
                } else {
                    if ($this->userGoldmineQualifiedRank[$userId] < $rankId) {
                        $isManual    = 1;
                        $updatedUser = $updatedBy[$userId];
                    }
                }

                if (isset($this->userGoldmineRankLocked[$userId])) {
                    $isOverride  = 1;
                    $isManual    = 1;
                    $updatedUser = $updatedBy[$userId];
                }

                // only insert for upgrade or downgrade rank and also not available in user_rank_histories
                if (!isset($currentUserRank[$userId]) || (isset($currentUserRank[$userId]) && $currentUserRank[$userId] != $rankId) || !isset($currentQualifiedUserRank[$userId]) || (isset($currentQualifiedUserRank[$userId]) && $currentQualifiedUserRank[$userId] != $rankId)) {
                    $userGoldmineRankHistories[] = [
                        'user_id'                    => $userId,
                        'goldmine_rank_id'           => $rankId,
                        'qualified_goldmine_rank_id' => $this->userGoldmineQualifiedRank[$userId] ?? null,
                        'is_current'                 => 1,
                        'is_manual'                  => $isManual,
                        'is_locked'                  => $isOverride,
                        'updated_by'                 => $updatedUser,
                        'created_at'                 => $this->checkDate,
                        'updated_at'                 => $this->checkDate,
                    ];

                    $userIds[]              = $userId;
                    $newRankUser[$rankId][] = $userId;
                }
            } else {
                //if found rank history then insert or update
                $noRankUser[] = $userId;
            }
        }

        //Update user rank histories and user rank_id
        if ($userGoldmineRankHistories) {
            $this->updateUserGoldmineRankHistory($userGoldmineRankHistories, $userIds, $newRankUser);
        }

        if ($noRankUser) {
            //update no rank id, down rank to no rank
            $this->updateNoRankUsers($noRankUser);
        }
    }

    private function updateNoRankUsers($noRankUser)
    {
        foreach (array_chunk($noRankUser, 1000) as $noRankUserId) {
            UserGoldmineRankHistory::where('is_current', 1)->whereIn('user_id', $noRankUserId)->update([
                'is_current' => 0,
            ]);

            User::whereIn('id', $noRankUserId)->update([
                'goldmine_rank_id' => null,
                'goldmine_level'   => null,
            ]);
        }
    }

    private function updateUserGoldmineRankHistory($userGoldmineRankHistories = [], $userIds = [], $newRankUser = [])
    {
        foreach (array_chunk($userIds, 1000) as $uids) {
            UserGoldmineRankHistory::where('is_current', 1)->whereIn('user_id', $uids)->update([
                'is_current' => 0,
            ]);
        }

        foreach (array_chunk($userGoldmineRankHistories, 1000) as $rankHistories) {
            UserGoldmineRankHistory::insert($rankHistories);
        }

        foreach ($newRankUser as $rankID => $userIDs) {
            User::whereIn('id', $userIDs)->update([
                'goldmine_rank_id' => $rankID,
            ]);
        }
    }

    private function convertUserTotalEquity()
    {
        foreach ($this->amounts as $userId => $userBalances) {
            $capitalCredit              = $userBalances['capital_credit'] ?? 0;
            $rollOverCredit             = $userBalances['roll_over_credit'] ?? 0;
            $promotionCredit            = $userBalances['promotion_credit'] ?? 0;
            $this->totalEquity[$userId] = bcadd(get_total_equity($this->convertTotalEquityExchangeRates, $capitalCredit, $rollOverCredit), $promotionCredit, 2);
        }
    }

    private function downlineSales($downlines)
    {
        $downlineSales = [];
        foreach ($downlines as $downline) {
            $totalSalesPerLine = 0;

            foreach ($downline as $user) {
                $userId = $user->getUserId();
                if (isset($this->totalEquity[$userId])) {
                    $totalSalesPerLine = bcadd($totalSalesPerLine, $this->totalEquity[$userId], 3);
                }
            }

            $downlineSales[] = $totalSalesPerLine;
        }

        return $downlineSales;
    }

    /*
   * get personal total equity
   */
    private function getPersonalEquity($userId)
    {
        $totalPersonalSales = 0;
        if (isset($this->totalEquity[$userId])) {
            $totalPersonalSales = bcadd($totalPersonalSales, $this->totalEquity[$userId], 3);
        }

        return $totalPersonalSales;
    }

    private function getRank($userId, $personalEquity, array $directDownlineEquities, array $allDownlineEquities, $currentRankId = null)
    {
        $newRankId = null;
        foreach ($this->goldmineRanks as $goldmineRank) {
            if (!empty($goldmineRank->qualification_condition)) {
                $rankCriteria = json_decode($goldmineRank->qualification_condition);

                #1 check personal equity
                #2 check direct downline total equity amount
                #3 check total downline equity
                $isPersonalEquity = $this->isPersonalEquity($rankCriteria->sales->personal, $personalEquity);
                //$isDirectDownlineTotalEquity = $this->isDirectDownlineTotalEquity($rankCriteria->sales->direct_downlines, $directDownlineEquities);
                $isDownlineTotalEquity = $this->isDownlineTotalEquity($rankCriteria->sales->downlines, $allDownlineEquities);

                //if rank a1 then only check personal equity qualification ($isDirectDownlineTotalEquity && $isDownlineTotalEquity)
                if ($goldmineRank->id > 1 && ($isPersonalEquity || $isDownlineTotalEquity)) {
                    $newRankId = $goldmineRank->id;
                    break;
                }
                if ($isPersonalEquity) {
                    $newRankId = $goldmineRank->id;
                    break;
                }
            } #end if !empty($goldmineRank->qualification_condition)
        } #end foreach $this->goldmineRanks

        return $newRankId;
    }

    private function isPersonalEquity($personalEquityCriteria, $personalEquity)
    {
        return !empty($personalEquityCriteria) && $personalEquityCriteria > 0 && $personalEquity >= $personalEquityCriteria;
    }

    private function isDirectDownlineTotalEquity($directDownlineCriteria, $directDownlineEquities)
    {
        $countDirectDownlineCriteriaPerLine = 0;
        $totalGroupEquity                   = 0;

        $isDirectDownlineTotalEquity = false;
        if ($directDownlineCriteria->is_total) {
            if ($directDownlineCriteria->amount > 0) {
                foreach ($directDownlineEquities as $directDownlineEquity) {
                    if ($directDownlineEquity > 0) {
                        $countDirectDownlineCriteriaPerLine++;
                    }
                    $totalGroupEquity = bcadd($directDownlineEquity, $totalGroupEquity, $this->creditPrecision);
                }

                if ($totalGroupEquity >= $directDownlineCriteria->amount && $countDirectDownlineCriteriaPerLine >= $directDownlineCriteria->line) {
                    $isDirectDownlineTotalEquity = true;
                }
            } else {
                # no total direct downline then return true
                $isDirectDownlineTotalEquity = true;
            }
        } else {
            # for min equity per line
            if ($directDownlineCriteria->amount > 0) {
                foreach ($directDownlineEquities as $directDownlineEquity) {
                    if ($directDownlineCriteria->line > 0) {
                        // Per line amount checking
                        if ($directDownlineEquity >= $directDownlineCriteria->amount) {
                            ++$countDirectDownlineCriteriaPerLine;
                            if ($countDirectDownlineCriteriaPerLine == $directDownlineCriteria->line) {
                                $isDirectDownlineTotalEquity = true;
                                break;
                            }
                        }
                    } else {
                        $totalGroupEquity += $directDownlineEquity;
                        if ($totalGroupEquity >= $directDownlineCriteria->amount) {
                            $isDirectDownlineTotalEquity = true;
                            break;
                        }
                    }
                }
            } else {
                // no sales amount condition, thus this always be true
                $isDirectDownlineTotalEquity = true;
            } #end direct downline sales no sum of total lines
        }

        return $isDirectDownlineTotalEquity;
    }

    private function isDownlineTotalEquity($downlineCriteria, $downlineEquities)
    {
        $totalGroupEquity                = 0;
        $countTotalDownlineEquityPerLine = 0;
        $isDownlineTotalEquity           = false;

        if ($downlineCriteria->is_total) {
            if ($downlineCriteria->amount > 0) {
                foreach ($downlineEquities as $downlineEquity) {
                    $totalGroupEquity = bcadd($downlineEquity, $totalGroupEquity, $this->creditPrecision);
                }

                if ($totalGroupEquity >= $downlineCriteria->group_equity) {
                    $isDownlineTotalEquity = true;
                }
            }
        } else {
            //for min sales per line
            if ($downlineCriteria->amount > 0) {
                foreach ($downlineEquities as $downlineEquity) {
                    $totalGroupEquity = bcadd($totalGroupEquity, $downlineEquity, $this->creditPrecision);

                    if ($downlineCriteria->line > 0) {
                        // Per line amount checking
                        if ($downlineEquity >= $downlineCriteria->amount) {
                            ++$countTotalDownlineEquityPerLine;
                        }
                        if ($countTotalDownlineEquityPerLine >= $downlineCriteria->line && $totalGroupEquity >= $downlineCriteria->group_equity) {
                            $isDownlineTotalEquity = true;
                            break;
                        }
                    } else {
                        if ($downlineCriteria->group_equity > 0 && $totalGroupEquity >= $downlineCriteria->group_equity) {
                            $isDownlineTotalEquity = true;
                            break;
                        }
                    }
                }
            } else {
                // no sales amount condition, thus this always be true
                $isDownlineTotalEquity = true;
            }
        }
        return $isDownlineTotalEquity;
    }
}
