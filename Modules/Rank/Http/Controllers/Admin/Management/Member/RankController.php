<?php

namespace Modules\Rank\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Http\Requests\Admin\Management\Member\Edit;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Contracts\UserRankHistoryContract;
use Modules\Rank\Http\Requests\Admin\UpdateLeaderBonusRank;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class RankController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The rank qualification repository
     *
     * @var unknown
     */
    protected $rankRepository;

    /**
     * The user special bonus percentage history repository
     *
     * @var unknown
     */
    protected $userRankHistoryRepo;

    /**
     * The class constructor
     *
     * @param UserContract $userContract
     * @param UserRankHistoryContract $userRankHistoryRepo
     * @param RankContract $rankContract
     */
    public function __construct(
        UserContract $userContract,
        UserRankHistoryContract $userRankHistoryRepo,
        RankContract $rankContract
    ) {
        $this->middleware('permission:admin_user_member_management_leader_bonus_edit')->only('edit', 'update');

        $this->userRepository      = $userContract;
        $this->userRankHistoryRepo = $userRankHistoryRepo;
        $this->rankRepository      = $rankContract;
    }

    /**
     * Show the form for editing the specified resource.
     * @param Edit $request
     * @param int $id
     * @return Response
     */
    public function edit(Edit $request, $id)
    {
        $user     = $this->userRepository->find($id);
        $userRank = $this->userRankHistoryRepo
            ->getModel()
            ->where('user_id', $user->id)
            ->current()
            ->first();

        return view('rank::admin.management.member.edit', compact('user', 'userRank'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateLeaderBonusRank $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateLeaderBonusRank $request, $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user) {
            return back()->with('error', __('a_member_management.user not exist'));
        }

        $userId = $user->id;
        DB::beginTransaction();

        try {
            $userRank = $this->userRankHistoryRepo->getModel()
                ->where('user_id', $userId)
                ->where('is_current', 1)
                ->first();

            if (empty($request->manual_rank_id)) {
                //go back to natural rank
                if (!empty($userRank)) {
                    $this->userRepository->edit($userId, ['rank_id' => $userRank->qualified_rank_id]);
                    if ($userRank) {
                        $userRank->update([
                            'is_current'        => 0,
                            'qualified_rank_id' => $userRank->qualified_rank_id
                        ]);

                        if ($userRank->qualified_rank_id > 0) {
                            $rank = $this->rankRepository->find($userRank->qualified_rank_id);
                            $this->rankRepository->changeHistory($user, $rank, [
                                'updated_by'        => auth()->user()->id,
                                'is_manual'         => false,
                                'is_locked'         => filter_var(false, FILTER_VALIDATE_BOOLEAN),
                                'qualified_rank_id' => $userRank->qualified_rank_id,
                                'rank_id'           => $userRank->qualified_rank_id,
                            ]);
                        }
                    }
                }
            } else {
                $rank = $this->rankRepository->find($request->manual_rank_id);
                $this->userRepository->edit($userId, ['rank_id' => $request->manual_rank_id]);
                $this->rankRepository->changeHistory($user, $rank, [
                    'updated_by'        => auth()->user()->id,
                    'is_manual'         => true,
                    'is_locked'         => filter_var($request->is_locked, FILTER_VALIDATE_BOOLEAN),
                    'qualified_rank_id' => $userRank->qualified_rank_id ?? null,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.leader.edit', $user->id))->withMessage(__('a_member_management_leader_bonus.failed to update user leader bonus rank'));
        }

        return back()->with('success', __('a_member_management_leader_bonus.user leader bonus rank updated successfully'));
    }
}
