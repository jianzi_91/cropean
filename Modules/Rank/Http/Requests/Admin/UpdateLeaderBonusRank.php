<?php

namespace Modules\Rank\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateLeaderBonusRank extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'manual_rank_id' => [
                'nullable',
                'exists:ranks,id'
            ],
            'is_locked' => ['required', Rule::in(['true', 'false'])],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'manual_rank_id' => __('a_member_management_leader_bonus.manual rank id'),
            'is_locked'      => __('a_member_management_leader_bonus.is locked'),
        ];
    }
}
