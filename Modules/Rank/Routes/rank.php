<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function () {
    #admin manage user special bonus
    Route::get('members/{id}/leader/edit', ['as' => 'admin.management.members.leader.edit', 'uses' => 'Admin\Management\Member\RankController@edit']);
    Route::put('members/{id}/leader', ['as' => 'admin.management.members.leader.update', 'uses' => 'Admin\Management\Member\RankController@update']);
});
