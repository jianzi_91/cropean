<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class UserGoldmineLevelHistorySnapshot extends Model
{
    protected $table = 'user_goldmine_level_history_snapshots';

    protected $fillable = [
        'user_id',
        'level',
        'qualified_level',
        'is_current',
        'is_manual',
        'is_locked',
        'updated_by',
        'run_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'id', 'updated_by');
    }
}
