<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class UserGoldmineRankHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'goldmine_rank_id',
        'qualified_goldmine_rank_id',
        'is_current',
        'is_manual',
        'is_locked',
        'updated_by'
    ];

    public function rank()
    {
        return $this->belongsTo(GoldmineRank::class, 'id', 'goldmine_rank_id');
    }

    public function qualifiedRank()
    {
        return $this->belongsTo(GoldmineRank::class, 'id', 'qualified_goldmine_rank_id');
    }

    public function goldmineRank()
    {
        return $this->hasOne(GoldmineRank::class, 'id', 'goldmine_rank_id');
    }

    public function GoldmineQualifiedRank()
    {
        return $this->hasOne(GoldmineRank::class, 'id', 'qualified_goldmine_rank_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'id', 'updated_by');
    }

    public function scopeCurrent($query)
    {
        return $query->where('is_current', 1);
    }
}
