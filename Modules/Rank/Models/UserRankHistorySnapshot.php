<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class UserRankHistorySnapshot extends Model
{
    protected $table = 'user_rank_history_snapshots';

    protected $fillable = [
        'user_id',
        'rank_id',
        'qualified_rank_id',
        'is_current',
        'is_manual',
        'is_locked',
        'updated_by',
        'run_date'
    ];

    public function rank()
    {
        return $this->belongsTo(Rank::class, 'id', 'rank_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }
}
