<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class UserRankHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'rank_id',
        'qualified_rank_id',
        'is_current',
        'is_manual',
        'is_locked',
        'updated_by'
    ];

    public function rank()
    {
        return $this->belongsTo(Rank::class, 'id', 'rank_id');
    }

    public function leaderBonusRank()
    {
        return $this->hasOne(Rank::class, 'id', 'rank_id');
    }

    public function leaderBonusQualifiedRank()
    {
        return $this->hasOne(Rank::class, 'id', 'qualified_rank_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function scopeCurrent($query)
    {
        return $query->where('is_current', 1);
    }
}
