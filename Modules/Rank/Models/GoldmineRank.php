<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Translation\Traits\Translatable;
use Modules\User\Models\User;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;

class GoldmineRank extends Model implements Stateable
{
    use SoftDeletes, Translatable, HasDropDown, HistoryRelation;

    protected $translatableAttributes = ['name'];

    protected $fillable = [
        'name',
        'name_translation',
        'level',
        'personal_capital_balance',
        'qualification_condition',
    ];

    protected $table = 'goldmine_ranks';

    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    /**
     * This model's relation to user status histories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function histories()
    {
        return $this->hasMany(UserGoldmineLevelHistory::class, 'rank_id');
    }

    /**
     * Get the state id
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'goldmine_level';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return UserGoldmineLevelHistory::class;
    }
}
