<?php

namespace Modules\Rank\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Models\User;

class UserGoldmineRankHistorySnapshot extends Model
{
    protected $table = 'user_goldmine_rank_history_snapshots';

    protected $fillable = [
        'user_id',
        'goldmine_rank_id',
        'qualified_goldmine_rank_id',
        'is_current',
        'is_manual',
        'is_locked',
        'updated_by',
        'run_date'
    ];

    public function goldmineRank()
    {
        return $this->belongsTo(GoldmineRank::class, 'id', 'goldmine_rank_id');
    }

    public function qualifiedGoldmineRank()
    {
        return $this->belongsTo(GoldmineRank::class, 'id', 'qualified_goldmine_rank_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'id', 'updated_by');
    }
}
