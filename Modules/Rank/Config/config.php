<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'bindings' => [
        Modules\Rank\Contracts\RankContract::class                             => Modules\Rank\Repositories\RankRepository::class,
        Modules\Rank\Contracts\GoldmineRankContract::class                     => Modules\Rank\Repositories\GoldmineRankRepository::class,
        Modules\Rank\Contracts\UserRankHistoryContract::class                  => Modules\Rank\Repositories\UserRankHistoryRepository::class,
        Modules\Rank\Contracts\UserRankHistorySnapshotContract::class          => Modules\Rank\Repositories\UserRankHistorySnapshotRepository::class,
        Modules\Rank\Contracts\UserGoldmineRankHistoryContract::class          => Modules\Rank\Repositories\UserGoldmineRankHistoryRepository::class,
        Modules\Rank\Contracts\UserGoldmineRankHistorySnapshotContract::class  => Modules\Rank\Repositories\UserGoldmineRankHistorySnapshotRepository::class,
        Modules\Rank\Contracts\UserGoldmineLevelHistorySnapshotContract::class => Modules\Rank\Repositories\UserGoldmineLevelHistorySnapshotRepository::class,
    ],
];
