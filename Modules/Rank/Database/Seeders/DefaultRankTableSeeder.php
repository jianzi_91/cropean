<?php

namespace Modules\Rank\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Models\Rank;

class DefaultRankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $ranks = [
            [
                'name'                    => RankContract::V1,
                'name_translation'        => 'v1',
                'level'                   => 1,
                'leader_bonus_percentage' => 0.04,
                'same_rank_percentage'    => 0.1,
                'qualification_condition' => '{"sales":{"personal":50000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":400000,"group_equity":2000000,"is_total":0}},"rank":{"line":0,"rank_id":0}}'
            ],
            [
                'name'                    => RankContract::V2,
                'name_translation'        => 'v2',
                'level'                   => 2,
                'leader_bonus_percentage' => 0.08,
                'same_rank_percentage'    => 0.1,
                'qualification_condition' => '{"sales":{"personal":100000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":1000000,"group_equity":5000000,"is_total":0}},"rank":{"line":0,"rank_id":0}}'
            ],
            [
                'name'                    => RankContract::V3,
                'name_translation'        => 'v3',
                'level'                   => 3,
                'leader_bonus_percentage' => 0.12,
                'same_rank_percentage'    => 0.1,
                'qualification_condition' => '{"sales":{"personal":150000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":2000000,"group_equity":10000000,"is_total":0}},"rank":{"line":0,"rank_id":0}}'
            ],
            [
                'name'                    => RankContract::V4,
                'name_translation'        => 'v4',
                'level'                   => 4,
                'leader_bonus_percentage' => 0.15,
                'same_rank_percentage'    => 0.1,
                'qualification_condition' => '{"sales":{"personal":300000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":0,"amount":0,"group_equity":0,"is_total":0}},"rank":{"line":3,"rank_id":3}}'
            ],
        ];

        foreach ($ranks as $rank) {
            (new Rank($rank))->save();
        }

        DB::commit();
    }
}
