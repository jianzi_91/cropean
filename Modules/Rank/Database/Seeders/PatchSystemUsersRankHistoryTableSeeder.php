<?php

namespace Modules\Rank\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Models\UserRankHistory;
use Modules\Rbac\Contracts\RoleContract;
use Modules\Rbac\Models\Role;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;

class PatchSystemUsersRankHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->membersRankHistory();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function membersRankHistory()
    {
        $userContract = resolve(UserContract::class);
        $roleContract = resolve(RoleContract::class);

        $memberRoles = $roleContract->getModel()->whereIn('name', [Role::MEMBER, Role::MEMBER_SUSPENDED, Role::MEMBER_TERMINATED, Role::MEMBER_ON_HOLD])->get()->pluck('id');

        $members = User::join('assigned_roles', function ($join) use ($memberRoles) {
            $join->on('assigned_roles.entity_id', '=', 'users.id')
                ->where('assigned_roles.entity_type', 'Modules\User\Models\User')
                ->whereIn('role_id', $memberRoles);
        })
        ->join('roles', function ($join) {
            $join->on('roles.id', '=', 'assigned_roles.role_id');
        })
        ->whereIn('username', ['root', 'systemroot'])
        ->select(
            [
                'users.*'
            ]
        )
        ->get();

        $rankContract = resolve(RankContract::class);
        $partnerRank  = $rankContract->findBySlug($rankContract::V1);

        foreach ($members as $member) {
            if (empty($member->rank_id)) {
                $userContract->edit($member->id, ['rank_id' => $partnerRank->id]);
            }

            if (!$userRank = UserRankHistory::where('user_id', $member->id)->where('is_current', 1)->first()) {
                $userRankHistory                    = new UserRankHistory;
                $userRankHistory->user_id           = $member->id;
                $userRankHistory->rank_id           = $partnerRank->id;
                $userRankHistory->qualified_rank_id = $partnerRank->id;
                $userRankHistory->is_manual         = false;
                $userRankHistory->is_locked         = false;
                $userRankHistory->save();
            }
        }
    }
}
