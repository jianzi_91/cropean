<?php

namespace Modules\Rank\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Models\GoldmineRank;

class DefaultGoldmineRankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $goldmineRanks = [
            [
                'name'                     => GoldmineRankContract::A1,
                'name_translation'         => 'a1',
                'level'                    => 3,
                'qualification_condition'  => '{"sales":{"personal":500,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":0,"amount":0,"group_equity":0,"is_total":0}}}'
            ],
            [
                'name'                     => GoldmineRankContract::A2,
                'name_translation'         => 'a2',
                'level'                    => 8,
                'qualification_condition'  => '{"sales":{"personal":3000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":1000,"group_equity":8000,"is_total":0}}}'
            ],
            [
                'name'                     => GoldmineRankContract::A3,
                'name_translation'         => 'a3',
                'level'                    => 15,
                'qualification_condition'  => '{"sales":{"personal":10000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":5000,"group_equity":30000,"is_total":0}}}'
            ],
            [
                'name'                     => GoldmineRankContract::A4,
                'name_translation'         => 'a4',
                'level'                    => 25,
                'qualification_condition'  => '{"sales":{"personal":20000,"direct_downlines":{"line":0,"amount":0,"is_total":0},"downlines":{"line":3,"amount":10000,"group_equity":60000,"is_total":0}}}'
            ],
        ];

        foreach ($goldmineRanks as $goldmineRank) {
            (new GoldmineRank($goldmineRank))->save();
        }

        DB::commit();
    }
}
