<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRankHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rank_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedBigInteger('rank_id')->index();
            $table->unsignedBigInteger('qualified_rank_id')->nullable();
            $table->boolean('is_current')->default(1);
            $table->boolean('is_manual')->default(0);
            $table->boolean('is_locked')->default(0);
            $table->unsignedInteger('updated_by')->nullable()->index();
        });

        Schema::table('user_rank_histories', function ($table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');

            $table->foreign('rank_id')
                ->references('id')
                ->on('ranks');

            $table->foreign('qualified_rank_id')
                ->references('id')
                ->on('ranks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rank_histories');
    }
}
