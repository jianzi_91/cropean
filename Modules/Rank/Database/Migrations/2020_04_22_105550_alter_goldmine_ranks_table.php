<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGoldmineRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goldmine_ranks', function (Blueprint $table) {
            if (Schema::hasColumn('goldmine_ranks', 'personal_capital_balance')) {
                $table->dropColumn(['personal_capital_balance']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goldmine_ranks', function (Blueprint $table) {
            if (Schema::hasColumn('goldmine_ranks', 'personal_capital_balance')) {
                $table->dropColumn(['personal_capital_balance']);
            }
        });
    }
}
