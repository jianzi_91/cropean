<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserGoldmineLevelHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('user_goldmine_level_histories', function (Blueprint $table) {
            if (Schema::hasColumn('user_goldmine_level_histories', 'goldmine_rank_id')) {
                $table->dropForeign(['goldmine_rank_id']);
                $table->dropColumn('goldmine_rank_id');
            }

            if (Schema::hasColumn('user_goldmine_level_histories', 'qualified_goldmine_rank_id')) {
                $table->dropForeign(['qualified_goldmine_rank_id']);
                $table->dropColumn('qualified_goldmine_rank_id');
            }

            if (Schema::hasColumn('user_goldmine_level_histories', 'is_override')) {
                $table->renameColumn('is_override', 'is_locked');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('user_goldmine_level_histories', function (Blueprint $table) {
            if (Schema::hasColumn('user_goldmine_level_histories', 'goldmine_rank_id')) {
                $table->dropForeign(['goldmine_rank_id']);
                $table->dropColumn('goldmine_rank_id');
            }

            if (Schema::hasColumn('user_goldmine_level_histories', 'qualified_goldmine_rank_id')) {
                $table->dropForeign(['qualified_goldmine_rank_id']);
                $table->dropColumn('qualified_goldmine_rank_id');
            }

            if (Schema::hasColumn('user_goldmine_level_histories', 'is_override')) {
                $table->renameColumn('is_override', 'is_locked');
            }
        });
    }
}
