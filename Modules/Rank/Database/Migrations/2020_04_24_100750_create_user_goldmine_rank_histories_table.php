<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGoldmineRankHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_goldmine_rank_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedBigInteger('goldmine_rank_id')->nullable();
            $table->unsignedBigInteger('qualified_goldmine_rank_id')->nullable();
            $table->boolean('is_locked')->default(0);
            $table->boolean('is_current')->default(1);
            $table->boolean('is_manual')->default(1);
            $table->unsignedInteger('updated_by')->nullable()->index();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('goldmine_rank_id')
                ->references('id')
                ->on('goldmine_ranks');

            $table->foreign('qualified_goldmine_rank_id')
                ->references('id')
                ->on('goldmine_ranks');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_goldmine_rank_histories');
    }
}
