<?php

namespace Modules\Rank\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface RankContract extends CrudContract, SlugContract, HistoryableContract
{
    const V1 = 'v1';
    const V2 = 'v2';
    const V3 = 'v3';
    const V4 = 'v4';
}
