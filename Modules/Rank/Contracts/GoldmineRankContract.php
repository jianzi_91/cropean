<?php

namespace Modules\Rank\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface GoldmineRankContract extends CrudContract, SlugContract, HistoryableContract
{
    const A1 = 'a1';
    const A2 = 'a2';
    const A3 = 'a3';
    const A4 = 'a4';
	// add functions here
}
