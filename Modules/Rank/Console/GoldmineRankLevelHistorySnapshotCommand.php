<?php

namespace Modules\Rank\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Rank\Models\UserGoldmineLevelHistorySnapshot;
use Modules\Rank\Models\UserGoldmineRankHistory;
use Modules\Rank\Models\UserGoldmineRankHistorySnapshot;

class GoldmineRankLevelHistorySnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'snapshot:goldmine_rank_level {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the user goldmine rank and level snapshot histories.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $now = new Carbon($this->argument('date'));

        $date = $now->copy()->endOfDay()->toDateTimeString();

        $rankHistory = UserGoldmineRankHistory::where('created_at', '<=', $date)
            ->where('is_current', 1)
            ->whereNull('deleted_at')
            ->select(['user_id', 'goldmine_rank_id', 'qualified_goldmine_rank_id', 'is_current', 'is_manual', 'is_locked', 'updated_by'])
            ->get()
            ->toArray();

        foreach ($rankHistory as $rankRecord) {
            $rankSnapshot = array_merge($rankRecord, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'run_date' => $date]);
            UserGoldmineRankHistorySnapshot::insert($rankSnapshot);
        }

        $levelHistory = UserGoldmineLevelHistory::where('created_at', '<=', $date)
            ->where('is_current', 1)
            ->whereNull('deleted_at')
            ->select(['user_id', 'level', 'qualified_level', 'is_current', 'is_manual', 'is_locked', 'updated_by'])
            ->get()
            ->toArray();

        foreach ($levelHistory as $levelRecord) {
            $levelSnapshot = array_merge($levelRecord, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'run_date' => $date]);
            UserGoldmineLevelHistorySnapshot::insert($levelSnapshot);
        }
    }
}
