<?php

namespace Modules\Rank\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Rank\Models\UserGoldmineRankHistory;
use Modules\User\Models\User;

class RestoreDefaultRankCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'goldmine-rank:restore-default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore all members manual goldmine rank to natural goldmine rank.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $manuallyAdjustedRanks = UserGoldmineRankHistory::where('is_current', true)
            ->where('is_manual', true)
            ->orderBy('user_id')
            ->get();

        DB::beginTransaction();

        try {
            foreach ($manuallyAdjustedRanks as $adjustedRank)
            {
                // Update member table goldmine rank id to natural rank id
                $adjustedMember = User::find($adjustedRank->user_id);
                $adjustedMember->update([
                    'goldmine_rank_id' => $adjustedRank->qualified_goldmine_rank_id,
                ]);

                // Update user goldmine rank history table to natural rank and make is_manual false
                if ($adjustedRank->qualified_goldmine_rank_id > 0) {
                    $adjustedRank->update([
                        'goldmine_rank_id' => $adjustedRank->qualified_goldmine_rank_id,
                        'is_manual'        => 0,
                    ]);
                } else {
                    $adjustedRank->forceDelete(); // Delete the model if qualified is null
                }
                
            }
            DB::commit();

            $this->line("ALL USER GOLDMINE RANK ARE RESET TO DEFAULT");
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        
    }
}
