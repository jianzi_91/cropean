<?php

namespace Modules\Rank\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Rank\Models\UserRankHistory;
use Modules\Rank\Models\UserRankHistorySnapshot;

class LeaderBonusRankSnapshotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'snapshot:leader_bonus_rank {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the user rank snapshot histories.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $now = new Carbon($this->argument('date'));

        $date = $now->copy()->endOfDay()->toDateTimeString();

        $history = UserRankHistory::where('created_at', '<=', $date)
            ->where('is_current', 1)
            ->whereNull('deleted_at')
            ->select(['user_id', 'rank_id', 'qualified_rank_id', 'is_current', 'is_manual', 'is_locked', 'updated_by'])
            ->get()
            ->toArray();

        foreach ($history as $record) {
            $snapshot = array_merge($record, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'run_date' => $date]);
            UserRankHistorySnapshot::insert($snapshot);
        }
    }
}
