<?php

namespace Modules\Rank\Console;

use Artisan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditSnapshotContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Contracts\UserGoldmineRankHistoryContract;
use Modules\Rank\Repositories\GoldmineRankQualificationRepository;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Plus65\Utility\Contracts\SnapshotContract;

class GoldmineRankQualificationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'rank-qualification:goldmine {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and update goldmine rank qualification.';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;

    protected $goldmineRankRepo;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     * @param SponsorTreeContract $sponsor
     * @param SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
     * @param GoldmineRankContract $goldmineRankRepo
     * @return void
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $snapshotHistoryContract,
        GoldmineRankContract $goldmineRankRepo
    ) {
        parent::__construct();
        $this->sponsorTree         = $sponsor;
        $this->snapshotHistoryRepo = $snapshotHistoryContract;
        $this->goldmineRankRepo    = $goldmineRankRepo;
    }

    /**
     * Execute the console command.
     * @param BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo
     * @param ExchangeRateContract $exchangeRateRepo
     * @param BankCreditTypeContract $bankCreditTypeRepo
     * @param UserGoldmineRankHistoryContract $userGoldmineRankHistoryRepo
     * @return mixed
     * @throws \Exception
     */
    public function handle(
        BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo,
        ExchangeRateContract $exchangeRateRepo,
        BankCreditTypeContract $bankCreditTypeRepo,
        UserGoldmineRankHistoryContract $userGoldmineRankHistoryRepo
    ) {
        $qualificationCheckDate = new Carbon($this->argument('date'));

        ini_set('memory_limit', '-1');
        set_time_limit(0);

        // Validate tree
        if (!$this->sponsorTree->getModel()->isValidNestedSet()) {
            $this->error("Sponsor Tree is invalid $qualificationCheckDate");
            exit;
        }

        $startTime = $this->start();
        $this->line("Goldmine Bonus Rank started at " . $startTime->toDateTimeString());

        $snapshotDate = $qualificationCheckDate->copy()->subDay()->toDateString();
        $this->line("[Step 1] Get member capital and rollover credit snapshot $snapshotDate");
        $creditTypes = $bankCreditTypeRepo->getModel()->whereIn('name', [
            $bankCreditTypeRepo::CAPITAL_CREDIT,
            $bankCreditTypeRepo::ROLL_OVER_CREDIT,
            $bankCreditTypeRepo::PROMOTION_CREDIT,
        ])->pluck('name', 'id')->toArray();
        $equities = $bankAccountCreditSnapshotRepo->getTotalCredits($snapshotDate, $creditTypes);

        $this->line("[Step 2] Get exchange rate");
        $exchangeRates = $exchangeRateRepo->getExchangeRates();

        if (empty($equities)) {
            $this->line("No equity for these dates");
            exit;
        }

        $this->line('[Step 3] Get Sponsor Tree Snapshot History');
        $dateFrom            = $qualificationCheckDate->copy()->startOfDay();
        $dateTo              = $qualificationCheckDate->copy()->endOfDay();
        $sponsorTreeSnapshot = $this->snapshotHistoryRepo->getModel()
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->where('sponsor_tree_snapshot_history.level', '!=', 0)
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
            ])
            ->toArray();

        if (!$sponsorTreeSnapshot) {
            $this->error('Snapshot is empty');
            exit;
        }

        $this->line('[Step 4] Get goldmine ranks');
        $goldmineRanks = $this->goldmineRankRepo->getModel()
            ->orderBy('level', 'desc')
            ->get();

        $this->line('[Step 5] Get user goldmine rank histories');
        $userGoldmineRankHistory = $userGoldmineRankHistoryRepo->getModel()->current()->get();

        $goldmineRankQualification = new GoldmineRankQualificationRepository($sponsorTreeSnapshot, $equities);
        $this->line('[Step 6] Set goldmine rank qualification check date');
        $goldmineRankQualification->setRankQualificationCheckDate($qualificationCheckDate);

        $this->line('[Step 7] Set goldmine rank');
        $goldmineRankQualification->setGoldmineRank($goldmineRanks);

        $this->line('[Step 8] Set user goldmine rank');
        $goldmineRankQualification->setUserGoldmineRank($userGoldmineRankHistory);

        $this->line('[Step 9] Set exchange rate');
        $goldmineRankQualification->setExchangeRate($exchangeRates);

        $this->line('[Step 10] Set non eligible users');
        $goldmineRankQualification->setNonEligibleUsers();

        $this->line('[Step 11] Calculate rank qualification');
        $goldmineRankQualification->qualification();

        DB::beginTransaction();
        try {
            $this->line('[Step 12] Insert user goldmine rank history');
            $goldmineRankQualification->updateRank();

            $this->line('[Step 13] Take snapshot for goldmine bonus rank and level');
            $this->call('snapshot:goldmine_rank_level', ['date' => $this->argument('date')]);
            $this->line('End goldmine rank qualification and goldmine rank level snapshot');
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));

        # Running the goldmine payout
        Artisan::call('commissions:goldmine_bonus ' . $this->argument('date'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
