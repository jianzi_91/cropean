<?php

namespace Modules\Rank\Console;

use Artisan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Credit\Contracts\BankAccountCreditSnapshotContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Contracts\UserRankHistoryContract;
use Modules\Rank\Repositories\RankQualificationRepository;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Plus65\Utility\Contracts\SnapshotContract;

class RankQualificationCommand extends Command
{
    protected $signature = 'rank-qualification:leader_bonus {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily rank up and rank down';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;

    protected $rankRepo;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     * @param SponsorTreeContract $sponsor
     * @param SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
     * @param RankContract $rankRepo
     * @return void
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $snapshotHistoryContract,
        RankContract $rankRepo
    ) {
        parent::__construct();
        $this->sponsorTree         = $sponsor;
        $this->snapshotHistoryRepo = $snapshotHistoryContract;
        $this->rankRepo            = $rankRepo;
    }

    /**
     * Execute the console command.
     * @param BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo
     * @param ExchangeRateContract $exchangeRateRepo
     * @param BankCreditTypeContract $bankCreditTypeRepo
     * @param UserRankHistoryContract $userRankHistoryRepo
     * @return mixed
     * @throws \Exception
     */
    public function handle(
        BankAccountCreditSnapshotContract $bankAccountCreditSnapshotRepo,
        ExchangeRateContract $exchangeRateRepo,
        BankCreditTypeContract $bankCreditTypeRepo,
        UserRankHistoryContract $userRankHistoryRepo
    ) {
        $rankUpDate = new Carbon($this->argument('date'));

        ini_set('memory_limit', -1);
        set_time_limit(0);

        // Validate tree
        if (!$this->sponsorTree->getModel()->isValidNestedSet()) {
            $this->error("Sponsor Tree is invalid $rankUpDate");
            exit;
        }

        $startTime = $this->start();
        $this->line("Leader Bonus Rank started at " . $startTime->toDateTimeString());

        $snapshotDate = $rankUpDate->copy()->subDay()->toDateString();
        $this->line("[Step 1] Get member capital and rollover credit snapshot $snapshotDate");

        $creditTypes = $bankCreditTypeRepo->getModel()->whereIn('name', [
            $bankCreditTypeRepo::CAPITAL_CREDIT,
            $bankCreditTypeRepo::ROLL_OVER_CREDIT
        ])->pluck('name', 'id')->toArray();
        $equities = $bankAccountCreditSnapshotRepo->getTotalCredits($snapshotDate, $creditTypes);

        $this->line("[Step 2] Get exchange rate");
        $exchangeRates = $exchangeRateRepo->getExchangeRates();

        if (empty($equities)) {
            $this->line("No equity for these dates");
            exit;
        }

        $this->line('[Step 3] Get Sponsor Tree Snapshot History');
        $dateFrom            = $rankUpDate->copy()->startOfDay();
        $dateTo              = $rankUpDate->copy()->endOfDay();
        $sponsorTreeSnapshot = $this->snapshotHistoryRepo->getModel()
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->where('sponsor_tree_snapshot_history.level', '!=', 0)
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
            ])
            ->toArray();

        if (!$sponsorTreeSnapshot) {
            $this->error('Snapshot is empty');
            exit;
        }

        $this->line('[Step 4] Get leader bonus ranks');
        $leaderBonusRank = $this->rankRepo->getModel()
            ->orderBy('level', 'desc')
            ->get();

        $userLeaderBonusRankHistory = $userRankHistoryRepo->getModel()->current()->get();

        $dailyRankQualification = new RankQualificationRepository($sponsorTreeSnapshot, $equities);
        $this->line('[Step 6] Set leader bonus rank qualification check date');
        $dailyRankQualification->setRankQualificationCheckDate($rankUpDate);

        $this->line('[Step 7] Set leader bonus rank');
        $dailyRankQualification->setLeaderBonusRank($leaderBonusRank);

        $this->line('[Step 8] Set user leader bonus rank');
        $dailyRankQualification->setUserLeaderBonusRank($userLeaderBonusRankHistory);

        $this->line('[Step 9] Set exchange rate');
        $dailyRankQualification->setExchangeRate($exchangeRates);

        $this->line('[Step 10] Set non eligible users');
        $dailyRankQualification->setNonEligibleUsers();

        $this->line('[Step 11] Check rank qualification');
        $dailyRankQualification->qualification();

        DB::beginTransaction();
        try {
            $this->line('[Step 12] Insert user leader bonus rank history');
            $dailyRankQualification->updateRank();

            //call user rank histories snapshot
            $this->line('[Step 13] Take snapshot for leader bonus rank');
            $this->call('snapshot:leader_bonus_rank', ['date' => $this->argument('date')]);
            $this->line('End rank qualification and rank snapshot');

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));

        // Run leader bonus calculation
        Artisan::call('commissions:leader_bonus ' . $this->argument('date'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
