<?php

namespace Modules\User\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Core\Traits\LogCronOutput;
use Modules\Translation\Models\TranslatorLanguage;
use Modules\Translation\Models\TranslatorPage;
use Modules\Translation\Models\TranslatorTranslation;
use Modules\User\Contracts\UserEpoaContractContract;
use Modules\User\Contracts\UserSignatureContract;
use Modules\User\Models\User;
use Modules\User\Models\UserSignature;
use PDF;

class GenerateEpoaContractPdf implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, LogCronOutput;

    /**
     * Credit Conversion.
     *
     * @var int
     */
    protected $user;

    /**
     * Credit Conversion Signature.
     *
     * @var int
     */
    protected $signature;

    /**
     * Credit Conversion Signature.
     *
     * @var int
     */
    protected $locale;

    /**
     * Class constructor.
     *
     * @param int $creditConversion
     */
    public function __construct(User $user, UserSignature $signature, $locale)
    {
        $this->user      = $user;
        $this->signature = $signature;
        $this->locale    = $locale;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(
        UserSignatureContract $signtureRepo,
        UserEpoaContractContract $userEpoaContractRepo
    ) {
        ini_set('memory_limit', -1);

        DB::beginTransaction();

        $this->logEContract('');
        $this->logEContract('------------------------------');
        $this->logEContract("Generating e-POA contract pdf");

        $signature = $signtureRepo->find($this->signature->id);

        try {
            $this->logEContract("User:" . $this->user->member_id);
            $this->logEContract("Locale:" . $this->locale);
            $data = [
                'name'                => strtoupper($this->user->name),
                'email'               => $this->user->email,
                'member_id'           => $this->user->member_id,
                'personal_id'         => $this->user->id_number,
                'country'             => $this->user->country->name,
                'address'             => $this->user->address,
                'date'                => now()->format('d/m/Y'),
                'locale'              => $this->locale,
                'penalty_percentage'  => json_decode(setting('penalty_percentage'), true),
                'signature_path'      => $signature->path,
                'signature_filename'  => $signature->filename,
                'signature_mime_type' => $signature->mime_type,
                'signature_id'        => $signature->id,
                'date'                => Carbon::now()->format('d/m/Y'),
            ];

            $this->logEContract("Rendering e-POA contract pdf");

            $view  = "user::member.epoa.contract";
            $trans = $this->getTranslations();
            // $view = "user::member.epoa.contract_" . $this->locale;
            $pdf = PDF::setOptions(['isRemoteEnabled' => true,  'isHtml5ParserEnabled' => true, 'defaultFont' => 'simhei', 'fontDir' => storage_path('fonts/')])
                ->loadView($view, compact('data', 'trans'))
                ->setPaper('A4', 'portrait')
                ->download();

            $filename = 'Power_Of_Attorney_' . $this->user->member_id . '.pdf';

            $this->logEContract("Saving e-POA contract pdf");

            $userEpoaContractRepo->attachContents($this->user->id, $signature->id, $data['locale'], $filename, '.pdf', $pdf);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->logEContract($e->getMessage());
            throw $e;
        }
    }

    // First approach: Tried to retrieve the translation at the blade but it's not translatiing with some sort of reasons
    // Second approach: Generates blade files based on languages available. Cons: Not able to be translated by admin
    // Third approach: Get the the translation first in the blade then inject into the blade with the translated data

    /**
     * Third approach of generating chinese pdf
     */
    public function getTranslations()
    {
        $transPages    = TranslatorPage::whereIn('name', ['s_epoa_contract_header', 's_epoa_contract_content', 's_epoa_contract_footer'])->get();
        $transLanguage = TranslatorLanguage::where('code', $this->locale)->first();

        $trans = [];
        foreach ($transPages as $page) {
            $translations = TranslatorTranslation::where('translator_page_id', $page->id)
                ->where('translator_language_id', $transLanguage->id)
                ->get();

            $substitutions = $this->substitutions();

            foreach ($translations as $translation) {
                if (empty($substitutions)) {
                    $trans[$page->rawname][$translation->key] = $translation->value;
                } else {
                    $trans[$page->rawname][$translation->key] = str_replace(array_keys($substitutions), array_values($substitutions), $translation->value);
                }
            }
        }

        return $trans;
    }

    public function substitutions()
    {
        $penaltyPercentage = json_decode(setting('penalty_percentage'), true);

        return [
            ':30_days_penalty' => bcmul($penaltyPercentage['30'], 100, 2),
            ':90_days_penalty' => bcmul($penaltyPercentage['90'], 100, 2),
        ];
    }
}
