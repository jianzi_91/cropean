<?php

namespace Modules\User\Repositories;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\User\Contracts\UserSignatureContract;
use Modules\User\Models\UserSignature;
use Modules\User\Repositories\Concerns\ImageSizes;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserSignatureRepository extends Repository implements UserSignatureContract
{
    use ImageSizes, HasCrud, HasSlug;

    /**
     * The file system class
     *
     * @var unknown
     */
    protected $fileSystem;

    /**
     * Class constructor
     *
     * @param  UserSignature $model
     */
    public function __construct(
        UserSignature $model
    ) {
        $this->slug = 'filename';

        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::attach()
     */
    public function attach($userId, $filename, $path, $move = false)
    {
        $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
        $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));

        return $this->attachUploadedFile($userId, $upload);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::attachContents()
     */
    public function attachContents($suerId, $filename, $mime, $contents)
    {
        $this->validateByteString($contents, $mime);

        $request = $this->model->findOrFail($userId);

        $path                          = config('user.signature.storage');
        $attachment                    = new $this->model;
        $randomFilename                = md5(str_replace('.', '', str_random(5) . microtime(true)));
        $randomFilename                = $randomFilename . '.' . $file->extension();
        $attachment->filename          = $randomFilename;
        $attachment->original_filename = $filename;
        $attachment->path              = $path;
        $attachment->mime_type         = $mime;
        $attachment->user_id           = $userId;

        if ($attachment->save()) {
            $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $path;
            $this->checkDirectory($fullpath);

            if (config('filesystems.cloud_enable')) {
                Storage::cloud()->put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);
            }

            Storage::put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);

            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::attachUploadedFile()
     */
    public function attachUploadedFile($userId, UploadedFile $file, $move = false)
    {
        $this->validateFile($file);

        $path = config('user.signature.storage') . '/' . $userId;

        $signature                    = new $this->model;
        $randomFilename               = md5(str_replace('.', '', str_random(5) . microtime(true)));
        $randomFilename               = $randomFilename . '.' . $file->extension();
        $signature->filename          = $randomFilename;
        $signature->original_filename = $file->getClientOriginalName();
        $signature->path              = $path;
        $signature->mime_type         = $file->getMimeType();
        $signature->user_id           = $userId;

        if ($signature->save()) {
            $this->saveFile($file, $signature);
            return $signature;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::get()
     */
    public function get($signatureId, $download = false)
    {
        $attachment = $this->model->findOrFail($signatureId);

        if (config('filesystems.cloud_enable')) {
            return $this->getFileFromSpace($attachment, $download);
        }

        return $this->getFileFromLocal($attachment, $download);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::getSignatures()
     */
    public function getSignatures($userId, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->where('user_id', $userId);

        return $prepare->with($with)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::delete()
     */
    public function delete($signatureId)
    {
        $attachment = $this->model->find($signatureId);

        if ($attachment) {
            $this->deleteFile($attachment);

            return $attachment->delete();
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::deleteUserConversionAttachment()
     */
    public function deleteUserConversionSignatures($userId)
    {
        $prepare = $this->model->where('user_id', $userId);

        $Signatures = $prepare->get();
        foreach ($Signatures as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\User\Contracts\UserSignatureContract::deleteAll()
     */
    public function deleteAll()
    {
        $signatures = $this->all();
        foreach ($signatures as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     * Get file from digital ocean
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromSpace($attachment, $download)
    {
        $path = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (Storage::cloud()->exists($path)) {
            $contents = Storage::cloud()->response($path);

            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     * Get file from local
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromLocal($attachment, $download)
    {
        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     * Validate file
     *
     * @param unknown $filename
     * @param unknown $path
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateFile($filename, $path = null)
    {
        $config = config('user.signature');

        if ($filename instanceof UploadedFile) {
            $mime = $filename->getClientMimeType();
            $size = $filename->getSize();
        } else {
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
            $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
            $mime     = $upload->getClientMimeType();
            $size     = $upload->getSize();
        }

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Validate byte strings
     *
     * @param unknown $contents
     * @param unknown $mime
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateByteString($contents, $mime)
    {
        $config = config('user.signature');
        $size   = strlen($contents);

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Check directory
     *
     * @param unknown $path
     * @return boolean
     */
    protected function checkDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path, 0777, true);
        }

        return false;
    }
}
