<?php

namespace Modules\User\Repositories;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\Core\Exceptions\UnsupportedMimeType;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserEpoaContractContract;
use Modules\User\Models\UserEpoaContract as Model;
use Modules\User\Repositories\Concerns\ImageSizes;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserEpoaContractRepository extends Repository implements UserEpoaContractContract
{
    use ImageSizes, HasCrud, HasSlug;

    /**
     * The file system class
     *
     * @var unknown
     */
    protected $fileSystem;

    /**
     * Class constructor
     *
     * @param UserEpoaContractContract $model
     */
    public function __construct(
        Model $model,
        UserContract $userContract
    ) {
        $this->slug = "original_filename";

        parent::__construct($model);
        $this->userRepo = $userContract;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::attach()
     */
    public function attach($creditConversionId, $locale, $filename, $path, $move = false)
    {
        $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
        $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));

        return $this->attachUploadedFile($creditConversionId, $locale, $upload);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::attachContents()
     */
    public function attachContents($userId, $signatureId, $locale, $filename, $mime, $contents)
    {
        $this->validateByteString($contents, $mime);

        $user = $this->userRepo->find($userId);

        $path = config('user.contract.storage') . '/' . $user->id;

        $attachment                    = new $this->model;
        $randomFilename                = md5(str_replace('.', '', str_random(5) . microtime(true)));
        $randomFilename                = $randomFilename . $mime;
        $attachment->locale            = $locale;
        $attachment->filename          = $randomFilename;
        $attachment->original_filename = $filename;
        $attachment->path              = $path;
        $attachment->mime_type         = config('user.contract.mimes')[$mime];
        $attachment->user_id           = $userId;
        $attachment->user_signature_id = $signatureId;

        if ($attachment->save()) {
            $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $path;
            $this->checkDirectory($fullpath);

            if (config('filesystems.cloud_enable')) {
                Storage::cloud()->put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);
            }

            Storage::put($path . DIRECTORY_SEPARATOR . $randomFilename, $contents);

            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::attachUploadedFile()
     */
    public function attachUploadedFile($creditConversionId, $locale, UploadedFile $file, $move = false)
    {
        $this->validateFile($file);

        $path                                       = config('credit.contract.storage');
        $attachment                                 = new $this->model;
        $randomFilename                             = md5(str_replace('.', '', str_random(5) . microtime(true)));
        $randomFilename                             = $randomFilename . '.' . $file->extension();
        $attachment->locale                         = $locale;
        $attachment->filename                       = $randomFilename;
        $attachment->original_filename              = $file->getClientOriginalName();
        $attachment->path                           = $path;
        $attachment->mime_type                      = $file->getMimeType();
        $attachment->credit_conversion_id           = $creditConversionId;
        $attachment->credit_conversion_signature_id = $creditConversionSignatureId;
        if ($attachment->save()) {
            $this->saveFile($file, $attachment);
            return $attachment;
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::get()
     */
    public function get($attachmentId, $download = false)
    {
        $attachment = $this->model->findOrFail($attachmentId);

        if (config('filesystems.cloud_enable')) {
            return $this->getFileFromSpace($attachment, $download);
        }

        return $this->getFileFromLocal($attachment, $download);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::getAttachments()
     */
    public function getAttachments($creditConversionId, array $with = [], $select = ['*'])
    {
        $prepare = $this->model->where('credit_conversion_id', $creditConversionId);

        return $prepare->with($with)->get($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::delete()
     */
    public function delete($attachmentId)
    {
        $attachment = $this->model->find($attachmentId);

        if ($attachment) {
            $this->deleteFile($attachment);

            return $attachment->delete();
        }

        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::deleteCreditConversionContract()
     */
    public function deleteCreditConversionContracts($creditConversionId, $locale = null)
    {
        $prepare = $this->model->where('credit_conversion_id', $creditConversionId);

        if ($locale) {
            $prepare->where('locale', $locale);
        }

        $attachments = $prepare->get();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see Modules\Credit\Contracts\UserEpoaContractContract::deleteAll()
     */
    public function deleteAll()
    {
        $attachments = $this->all();
        foreach ($attachments as $attachment) {
            $this->delete($attachment->id);
        }

        return true;
    }

    /**
     * Get file from digital ocean
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromSpace($attachment, $download)
    {
        $path = $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (Storage::cloud()->exists($path)) {
            $contents = Storage::cloud()->response($path);

            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     * Get file from local
     *
     * @param unknown $attachment
     * @param unknown $download
     * @return void|unknown
     */
    protected function getFileFromLocal($attachment, $download)
    {
        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $attachment->path . DIRECTORY_SEPARATOR . $attachment->filename;

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if ($download) {
                header('Content-Description: File Transfer');
                header('Content-Type: ' . $attachment->mime_type);
                header('Content-Disposition: attachment; filename="' . basename($attachment->original_filename) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($contents));
                echo $contents;
                exit;
            }

            return $contents;
        }

        return;
    }

    /**
     * Validate file
     *
     * @param unknown $filename
     * @param unknown $path
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateFile($filename, $path = null)
    {
        $config = config('user.contract');

        if ($filename instanceof UploadedFile) {
            $mime = $filename->getClientMimeType();
            $size = $filename->getSize();
        } else {
            $fullpath = $path . DIRECTORY_SEPARATOR . $filename;
            $upload   = new UploadedFile($fullpath, $filename, mime_content_type($fullpath));
            $mime     = $upload->getClientMimeType();
            $size     = $upload->getSize();
        }

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, $mimes)) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Validate byte strings
     *
     * @param unknown $contents
     * @param unknown $mime
     * @throws UnsupportedMimeType
     * @throws MaxFileSize
     * @return boolean
     */
    protected function validateByteString($contents, $mime)
    {
        $config = config('user.contract');
        $size   = strlen($contents);

        if ($mime) {
            $mimes = $config['mimes'];
            if (!in_array($mime, array_keys($mimes))) {
                throw new UnsupportedMimeType($mime, $mimes);
            }
        }

        if ($size) {
            $limit = $config['max_size'] * 1000;
            if ($limit < $size) {
                throw new MaxFileSize($size, $limit);
            }
        }

        return true;
    }

    /**
     * Check directory
     *
     * @param unknown $path
     * @return boolean
     */
    protected function checkDirectory($path)
    {
        if (!file_exists($path)) {
            return mkdir($path, 0777, true);
        }

        return false;
    }
}
