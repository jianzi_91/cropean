<?php

namespace Modules\User\Repositories;

use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\UserStatus;
use Modules\User\Models\UserStatusHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserStatusRepository extends Repository implements UserStatusContract
{
    use HasCrud, HasSlug, HasHistory;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * Class constructor.
     *
     * @param UserStatus  $model
     */
    public function __construct(UserStatus $model, UserStatusHistory $history)
    {
        $this->model        = $model;
        $this->historyModel = $history;

        parent::__construct($model);
    }
}
