<?php

namespace Modules\User\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Repositories\BankAccountCreditRepository;
use Modules\DocumentVerification\Contracts\UserDocumentStatusContract;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Contracts\RankContract;
use Modules\Rbac\Contracts\RoleContract;
use Modules\Setting\Models\Setting;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Models\UserSponsorHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasHistory;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class UserRepository extends Repository implements UserContract
{
    use HasCrud, HasSlug, HasHistory;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /**
     * The role repository.
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * The sponsor tree repository.
     *
     * @var unknown
     */
    protected $sponsorTreeRepository;

    /** @var BankAccountCreditRepository $bankAccountCreditContract */
    protected $bankAccountCreditContract;

    protected $userSponsorHistory;

    protected $rankRepository;

    protected $goldmineRankRepository;

    protected $userDocumentStatusRepository;

    /**
     * Class constructor.
     *
     * @param User $model
     * @param RoleContract $roleContract
     * @param SponsorTreeContract $sponsorTreeContract
     * @param BankAccountCreditContract $bankAccountCreditContract
     * @param UserSponsorHistory $history
     * @param RankContract $rankContract
     * @param GoldmineRankContract $goldmineRankContract
     */
    public function __construct(
        User $model,
        RoleContract $roleContract,
        SponsorTreeContract $sponsorTreeContract,
        BankAccountCreditContract $bankAccountCreditContract,
        UserSponsorHistory $history,
        RankContract $rankContract,
        GoldmineRankContract $goldmineRankContract,
        UserDocumentStatusContract $userDocumentStatusContract
    ) {
        $this->model                        = $model;
        $this->slug                         = 'username';
        $this->roleRepository               = $roleContract;
        $this->sponsorTreeRepository        = $sponsorTreeContract;
        $this->bankAccountCreditContract    = $bankAccountCreditContract;
        $this->historyModel                 = $history;
        $this->rankRepository               = $rankContract;
        $this->goldmineRankRepository       = $goldmineRankContract;
        $this->userDocumentStatusRepository = $userDocumentStatusContract;
        parent::__construct($model);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\User\Contracts\UserContract::findByIdentifier()
     */
    public function findByIdentifier($value, array $with = [], $select = ['*'])
    {
        return $this->model->where('member_id', $value)
                        ->orWhere('username', $value)
                        ->orWhere('email', $value)
                        ->with($with)
                        ->first($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\User\Contracts\UserContract::findByMemberId()
     */
    public function findByMemberId($value, array $with = [], $select = ['*'])
    {
        return $this->model->where('member_id', $value)
            ->with($with)
            ->first($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\User\Contracts\UserContract::findByMemberId()
     */
    public function findByEmail($value, array $with = [], $select = ['*'])
    {
        return $this->model->where('email', $value)
            ->with($with)
            ->first($select);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\User\Contracts\UserContract::register()
     */
    public function register(array $data)
    {
        Setting::lockForUpdate()->where('name', 'sponsor_tree_lock')->first();

        $data['birth_date']                      = isset($data['birth_date'])? Carbon::parse($data['birth_date'])->format('Y-m-d'):null;
        $data['password']                        = bcrypt($data['password']);
        $data['secondary_password']              = bcrypt($data['secondary_password']);
        $data['member_id']                       = isset($data['member_id'])? $data['member_id']:$this->generateMemberId();
        $data['username']                        = $data['member_id'];
        $data['is_system_generated']             = isset($data['is_system_generated'])? $data['is_system_generated']:0;
        $data['document_verification_status_id'] = $this->userDocumentStatusRepository->findBySlug($this->userDocumentStatusRepository::PENDING)->id;

        $newUser = $this->add($data);

        // Assign user to member role
        $role = $this->roleRepository->findBySlug($this->roleRepository->getModel()::MEMBER);
        $newUser->assign($role);

        // Add to sponsor tree
        if ($data['sponsor_id']) {
            $sponsorUser = $this->findByIdentifier($data['sponsor_id']);
            if (!$sponsorUser) {
                throw new ModelNotFoundException();
            }

            if (!($data['sponsor_id'] == 'systemroot')) {
                $this->sponsorTreeRepository->add(['user_id' => $newUser->id], $sponsorUser->id);
                //add user sponsor histories, in case change network
                $this->generateUserSponsorHistories($newUser->id, ['sponsor_id' => $sponsorUser->id]);
            }
        } else {
            throw new \Exception(__('m_register.empty sponsor id'));
        }

        return $newUser;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Modules\User\Contracts\UserContract::generateMemberId()
     */
    public function generateMemberId($isSystem = false)
    {
        $memberIdStartPrefix = setting('member_id_start_prefix', '01');
        $length              = setting('member_id_length', '8') - strlen($memberIdStartPrefix);
        $systemPrefixSample  = str_split(setting('system_member_id_characters', 'ABCDEFGHIJKLMNPQRSTUVWXYZ'));
        $range               = str_repeat(9, $length);

        do {
            if ($isSystem) {
                $prefix = $systemPrefixSample[mt_rand(0, count($systemPrefixSample) - 1)];
                $length = 7;
            } else {
                $prefix = $memberIdStartPrefix;
            }
            $memberId = $prefix . sprintf('%0' . $length . 'd', mt_rand(0, $range));
        } while (!empty($this->model->where('member_id', $memberId)->first()));
        return $memberId;
    }

    public function generateUserSponsorHistories($userId, $attributes = [])
    {
        $attributes['is_current'] = 1;
        $attributes['user_id']    = $userId;

        (new UserSponsorHistory($attributes))->save();
    }

    public function updateUserSponsor($userId, $attributes = [])
    {
        if ($userId) {
            if (UserSponsorHistory::where('user_id', $userId)->where('is_current', 1)->exists()) {
                UserSponsorHistory::where('user_id', $userId)->where('is_current', 1)->update(['is_current' => 0]);
            }
        }

        $attributes['is_current'] = 1;
        $attributes['user_id']    = $userId;
        (new UserSponsorHistory($attributes))->save();
    }
}
