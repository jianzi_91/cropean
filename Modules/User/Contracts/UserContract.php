<?php

namespace Modules\User\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserContract extends CrudContract, SlugContract
{
    /**
     * Find a record by identifier
     * @param string $value
     * @param array $with
     * @param array $select
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findByIdentifier($value, array $with = [], $select = ['*']);

    /**
     * Find a record by member id
     * @param string $value
     * @param array $with
     * @param array $select
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findByMemberId($value, array $with = [], $select = ['*']);

    /**
     * Find a record by email
     * @param string $value
     * @param array $with
     * @param array $select
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findByEmail($value, array $with = [], $select = ['*']);

    /**
     * Register a user
     *
     * @param array $data
     * @return Modules\User\Models\User
     */
    public function register(array $data);

    /**
     * Generate a random member ID based on a specific prefix.
     *
     * @param string $prefix
     * @return string The new member ID
     */
    public function generateMemberId($isSystem = false);
}
