<?php

namespace Modules\User\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\HistoryableContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserStatusContract extends CrudContract, SlugContract, HistoryableContract
{
    const ACTIVE     = 'active';
    const HOLD       = 'hold';
    const SUSPENDED  = 'suspended';
    const TERMINATED = 'terminated';
}
