<?php

namespace Modules\User\Contracts;

use Illuminate\Http\UploadedFile;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserEpoaContractContract extends CrudContract, SlugContract
{
    /**
     * Attach a file
     * @param unknown $userId
     * @param unknown $locale
     * @param unknown $filename
     * @param unknown $path
     * @param string $move
     * @return boolean
     */
    public function attach($userId, $locale, $filename, $path, $move = false);

    /**
     * Attach file using byte contents
     * @param unknown $userId
     * @param unknown $signatureId
     * @param unknown $txnCode
     * @param unknown $locale
     * @param unknown $filename
     * @param unknown $mime
     * @param unknown $contents
     * @return boolean
     */
    public function attachContents($userId, $signatureId, $locale, $filename, $mime, $contents);

    /**
     * Attach file using Uploaded Object
     * @param unknown $userId
     * @param unknown $locale
     * @param UploadedFile $file
     * @param string $move
     * @return boolean
     */
    public function attachUploadedFile($userId, $locale, UploadedFile $file, $move = false);

    /**
     * Get contract as byte contents
     * @param unknown $contractId
     * @param string $download
     * @return string bytes
     */
    public function get($contractId, $download = false);

    /**
     * Delete contract
     * @param unknown $contractId
     * @return integer
     */
    public function delete($contractId);

    /**
     * Delete credit conversion contracts
     * @param unknown $requestId
     * @param unknow $locale
     * @return integer
     */
    public function deleteCreditConversionContracts($userId, $locale = null);

    /**
     * Delete all contracts
     * @return integer
     */
    public function deleteAll();
}
