<?php

namespace Modules\User\Contracts;

use Illuminate\Http\UploadedFile;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface UserSignatureContract extends CrudContract, SlugContract
{
    /**
     * Attach a file
     * @param unknown $userId
     * @param unknown $filename
     * @param unknown $path
     * @param string $move
     * @return boolean
     */
    public function attach($userId, $filename, $path, $move = false);

    /**
     * Attach file using byte contents
     * @param unknown $userId
     * @param unknown $filename
     * @param unknown $mime
     * @param unknown $contents
     * @return boolean
     */
    public function attachContents($userId, $filename, $mime, $contents);

    /**
     * Attach file using Uploaded Object
     * @param unknown $userId
     * @param UploadedFile $file
     * @param string $move
     * @return boolean
     */
    public function attachUploadedFile($userId, UploadedFile $file, $move = false);

    /**
     * Get signature as byte contents
     * @param unknown $signatureId
     * @param string $download
     * @return string bytes
     */
    public function get($signatureId, $download = false);

    /**
     * Get signatures
     * @param unknown $userId
     * @param array $with
     * @param array $select
     * @return Illuminate\Support\Collection
     */
    public function getSignatures($userId, array $with = [], $select = ['*']);

    /**
     * Delete signature
     * @param unknown $signatureId
     * @return integer
     */
    public function delete($signatureId);

    /**
     * Delete all signatures
     * @return integer
     */
    public function deleteAll();
}
