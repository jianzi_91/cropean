<?php

namespace Modules\User\Http\Requests\Admin\Management\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\User\Contracts\UserContract;
use Plus65\Base\Rules\CheckOwnership;

class Update extends FormRequest
{
    use CheckOwnership;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|max:255',
            'email'          => ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore($this->admin)],
            'role_id'        => 'required|exists:roles,id',
            'user_status_id' => 'required|exists:user_statuses,id',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->userColumn = 'id';
        $user             = resolve(UserContract::class)->find($this->admin);
        return $this->isOwner($user, $this->user(), true) || $user->is_admin;
    }

    /**
     * Determine the custom attributes
     *
     * @return string
     */
    public function attributes()
    {
        return [
            'name'           => __('a_admin_management_profile.name'),
            'email'          => __('a_admin_management_profile.email address'),
            'role_id'        => __('a_admin_management_profile.role'),
            'user_status_id' => __('a_admin_management_profile.status')
        ];
    }
}
