<?php

namespace Modules\User\Http\Requests\Admin\Management\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Password\Http\Rules\PasswordRegex;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id'                         => 'required|exists:roles,id',
            'name'                            => 'required|max:255',
            'email'                           => 'required|email|max:255|unique:users,email',
            'password'                        => ['required', 'min:' . config('password.primary_password_min_characters'), 'alpha_num', new PasswordRegex(), 'confirmed'],
            'password_confirmation'           => ['required', 'min:' . config('password.primary_password_min_characters')],
            'secondary_password'              => ['required', 'min:' . config('password.secondary_password_min_characters'), 'alpha_num', new PasswordRegex(), 'different:password', 'confirmed'],
            'secondary_password_confirmation' => ['required', 'min:' . config('password.secondary_password_min_characters')],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Determine the custom attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'                            => __('a_admin_management_profile.name'),
            'email'                           => __('a_admin_management_profile.email address'),
            'password'                        => __('a_admin_management_profile.password'),
            'password_confirmation'           => __('a_admin_management_profile.confirm password'),
            'secondary_password'              => __('a_admin_management_profile.secondary password'),
            'secondary_password_confirmation' => __('a_admin_management_profile.confirm secondary password'),
            'role_id'                         => __('a_admin_management_profile.role'),
        ];
    }

    public function messages()
    {
        return [
            'password.min'                        => __('a_admin_management_profile.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')]),
            'password_confirmation.min'           => __('a_admin_management_profile.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')]),
            'secondary_password.min'              => __('a_admin_management_profile.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')]),
            'secondary_password_confirmation.min' => __('a_admin_management_profile.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')])
        ];
    }
}
