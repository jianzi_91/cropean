<?php

namespace Modules\User\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Rules\PhoneNumberExistExcludeOwn;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'           => 'required|max:255',
            'email'          => ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore($this->member)],
            'id_number'      => ['required', 'max:255', Rule::unique('users', 'id_number')->ignore($this->member)],
            'birth_date'     => 'required|date|before:' . date('Y-m-d'),
            'address'        => 'required|max:255',
            'country_id'     => 'required|numeric|exists:countries,id',
            'user_status_id' => 'required|exists:user_statuses,id',
            'mobile_prefix'  => 'required|numeric|exists:countries,dialling_code',
            'mobile_number'  => [
                'required', 'numeric', 'digits_between:5,20',
                new PhoneNumberExistExcludeOwn($this->member),
            ],
            'gender' => ['bail', 'required', Rule::in(['M', 'F'])],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = resolve(UserContract::class)->find($this->member);
        return $user && $user->is_member;
    }

    /**
     * Get the validation error messages.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'username.regex'    => __('a_member_management_profile.only small letters, numbers and period are allowed, period cannot be the first or last character'),
            'birth_date.before' => __('a_member_management_profile.must be at least 18 years old'),
        ];
    }

    /**
     * Determine the custom attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'           => __('a_member_management_profile.name'),
            'email'          => __('a_member_management_profile.email address'),
            'id_number'      => __('a_member_management_profile.national id'),
            'birth_date'     => __('a_member_management_profile.date of birth'),
            'country_id'     => __('a_member_management_profile.country'),
            'address'        => __('a_member_management_profile.address'),
            'user_status_id' => __('a_member_management_profile.status'),
            'mobile_prefix'  => __('a_member_management_profile.mobile register country'),
            'mobile_number'  => __('a_member_management_profile.mobile number'),
            'gender'         => __('a_member_management_profile.gender'),
        ];
    }
}
