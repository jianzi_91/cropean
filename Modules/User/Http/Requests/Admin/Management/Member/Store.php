<?php

namespace Modules\User\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:255',
            'username' => ['required',
                            Rule::unique('users', 'username')->ignore($this->member, 'id'),
                            'regex:/^[a-z0-9\p{Han}]+\.?([a-z0-9\p{Han}])+$/u',
                            'max:255'],
            'role_id'                         => 'required|numeric|exists:roles,id',
            'email'                           => 'required|email|max:255|unique:users,email',
            'country_id'                      => 'nullable|numeric|exists:countries,id',
            'birth_date'                      => 'nullable|date|before:' . date('Y-m-d'),
            'mobile_number'                   => 'nullable|min:0|numeric|max:255',
            'mobile_prefix'                   => 'nullable|numeric|exists:countries,dialling_code',
            'password'                        => 'required|min:8|confirmed',
            'password_confirmation'           => 'required|min:8',
            'secondary_password'              => 'required|min:8|different:password|confirmed',
            'secondary_password_confirmation' => 'required|min:8',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation error messages.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'username.regex'    => __('s_validation.only small letters, numbers and period are allowed, period cannot be the first or last character'),
            'birth_date.before' => __('s_validation.must be at least 18 years old'),
        ];
    }

    public function attributes()
    {
        return [
            'name'           => __('a_admin_management_profile.name'),
            'username'       => __('a_admin_management_profile.username'),
            'email'          => __('a_admin_management_profile.email address'),
            'id_number'      => __('a_admin_management_profile.identity number'),
            'birth_date'     => __('a_admin_management_profile.date of birth'),
            'country_id'     => __('a_admin_management_profile.country'),
            'province'       => __('a_admin_management_profile.province'),
            'city'           => __('a_admin_management_profile.city'),
            'region'         => __('a_admin_management_profile.region'),
            'address'        => __('a_admin_management_profile.address'),
            'mobile_prefix'  => __('a_admin_management_profile.mobile prefix'),
            'mobile_number'  => __('a_admin_management_profile.mobile number'),
            'user_status_id' => __('a_admin_management_profile.status'),

        ];
    }
}
