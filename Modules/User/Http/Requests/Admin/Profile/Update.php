<?php

namespace Modules\User\Http\Requests\Admin\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:255',
            'email'      => ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore(Auth::id())],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // #Todo: Add CheckOwnership validation

        return true;
    }

    /**
     * Determine the custom attributes
     *
     * @return string
     */
    public function attributes()
    {
        return [
            'name'       => __('a_my_profile.name'),
            'email'      => __('a_my_profile.email address'),
        ];
    }
}
