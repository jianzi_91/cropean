<?php

namespace Modules\User\Http\Requests\Member\Epoa;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'signature' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Determine the custom attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'signature' => __('m_epoa.signature')
        ];
    }
}
