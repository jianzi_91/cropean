<?php

namespace Plus65\SctUser\Http\Requests\User\Management;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required',
            'username'   => 'required|unique:users,username',
            'email'      => 'required|email',
            'id_number'  => 'required|unique:users,id_number',
            'birth_date' => 'required|date|before:18 years ago',
            'country_id' => 'required|exists:countries,id',
            'province'   => 'required',
            'city'       => 'required',
            'region'     => 'required',
            'address'    => 'required',
            // #Todo: Add in later
            // 'sponsor_id'  => [
            //         'required',
            //         resolve(SponsorExist::class)
            // ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation error messages.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'birth_date.before' => __('a_member_management_profile.must be at least 18 years old'),
        ];
    }

    /**
     * Determine the custom attributes
     *
     * @return string
     */
    public function attributes()
    {
        return [
            'name'       => __('a_member_management_profile.name'),
            'username'   => __('a_member_management_profile.username'),
            'email'      => __('a_member_management_profile.email address'),
            'id_number'  => __('a_member_management_profile.identity number'),
            'birth_date' => __('a_member_management_profile.date of birth'),
            'country_id' => __('a_member_management_profile.country'),
            'province'   => __('a_member_management_profile.province'),
            'city'       => __('a_member_management_profile.city'),
            'region'     => __('a_member_management_profile.region'),
            'address'    => __('a_member_management_profile.address'),

        ];
    }
}
