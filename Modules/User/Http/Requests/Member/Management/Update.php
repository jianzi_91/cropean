<?php

namespace Modules\User\Http\Requests\User\Management;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'          => 'nullable',
            'username'      => 'required|regex:/^[a-z0-9]+.?([a-z0-9])+$/|unique:users,username,' . $this->segment(2),
            'email'         => 'required|email',
            'id_number'     => 'nullable',
            'birth_date'    => 'nullable|date|before:18 years ago',
            'country_id'    => 'required|exists:countries,id',
            'province'      => 'nullable',
            'city'          => 'nullable',
            'region'        => 'nullable',
            'address'       => 'nullable',
            'mobile_prefix' => 'required|numeric|exists:countries,dialling_code',
            'mobile_number' => 'required|numeric|digits_between:5,20',
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return string
     */
    public function messages()
    {
        return [
            'username.regex'    => __('a_member_management_profile.only small letters, numbers and period are allowed, period cannot be the first or last character'),
            'birth_date.before' => __('a_member_management_profile.must be at least 18 years old'),
        ];
    }

    /**
     * Determine the custom attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'          => __('a_member_management_profile.name'),
            'username'      => __('a_member_management_profile.username'),
            'email'         => __('a_member_management_profile.email address'),
            'id_number'     => __('a_member_management_profile.identity number'),
            'birth_date'    => __('a_member_management_profile.date of birth'),
            'country_id'    => __('a_member_management_profile.country'),
            'province'      => __('a_member_management_profile.province'),
            'city'          => __('a_member_management_profile.city'),
            'region'        => __('a_member_management_profile.region'),
            'address'       => __('a_member_management_profile.address'),
            'mobile_prefix' => __('a_member_management_profile.mobile prefix'),
            'mobile_number' => __('a_member_management_profile.mobile number'),

        ];
    }
}
