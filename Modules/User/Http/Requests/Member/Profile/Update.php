<?php

namespace Modules\User\Http\Requests\Member\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Modules\User\Http\Rules\PhoneNumberExistExcludeOwn;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:255',
            'email'      => ['required', 'email', 'max:255', Rule::unique('users', 'email')->ignore(Auth::id())],
            'birth_date' => 'required|date|before:18 years ago',
            'gender'     => [
                'bail',
                'required',
                Rule::in(['M', 'F']),
            ],
            'country_id'    => 'required|exists:countries,id',
            'address'       => 'required|max:256',
            'mobile_prefix' => 'required|numeric|exists:countries,dialling_code',
            'mobile_number' => [
                'required', 'numeric', 'digits_between:5,20',
                new PhoneNumberExistExcludeOwn(Auth::id()),
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Determine the custom messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'birth_date.before' => __('m_my_profile.you must be at least 18 years old'),
        ];
    }

    /**
     * Determine the custom attributes
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'  => __('m_my_profile.name'),
            'email' => __('m_my_profile.email address'),
//            'id_number'  => __('m_my_profile.national id'),
            'birth_date' => __('m_my_profile.date of birth'),
            'country_id' => __('m_my_profile.country'),
            'address'    => __('m_my_profile.address'),
            'gender'     => __('m_my_profile.gender')
        ];
    }
}
