<?php

namespace Modules\User\Http\Controllers\Admin\Management\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Rbac\Contracts\RoleContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Http\Requests\Admin\Management\Admin\Edit;
use Modules\User\Http\Requests\Admin\Management\Admin\Store;
use Modules\User\Http\Requests\Admin\Management\Admin\Update;
use Modules\User\Queries\Admin\UserQuery;
use Plus65\Utility\Exceptions\WebException;

class UserController extends Controller
{
    /**
     * The user status to logout user
     *
     * @var unknown
     */
    protected $toLogoutUserStatuses = [UserStatusContract::HOLD, UserStatusContract::TERMINATED];

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The role repository
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * The user query
     *
     * @var unknown
     */
    protected $userQuery;

    /**
     * The user status repository
     *
     * @var unknown
     */
    protected $userStatusContract;

    /**
     * The user status repository
     *
     * @var unknown
     */
    protected $statusRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     * @param RoleContract $roleContract
     * @param UserQuery $userQuery
     */
    public function __construct(UserContract $userContract, UserQuery $userQuery, RoleContract $roleContract, UserStatusContract $userStatusContract)
    {
        $this->middleware('permission:admin_user_admin_management_list')->only('index');
        $this->middleware('permission:admin_user_admin_management_create')->only('create', 'store');
        $this->middleware('permission:admin_user_admin_management_edit')->only('edit', 'update');
        $this->middleware('permission:admin_user_admin_management_delete')->only('destroy');

        $this->userRepository   = $userContract;
        $this->roleRepository   = $roleContract;
        $this->userQuery        = $userQuery;
        $this->statusRepository = $userStatusContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        $users = $this->userQuery
            ->setParameters($request->all())
            ->setLocale(session()->get('locale'))
            ->paginate();

        return view('user::admin.management.admin.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create()
    {
        return view('user::admin.management.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();
        try {
            $data                       = $request->except(['password_confirmation', 'secondary_password_confirmation']);
            $data['password']           = bcrypt($data['password']);
            $data['secondary_password'] = bcrypt($data['secondary_password']);
            $data['member_id']          = $this->userRepository->generateMemberId();

            if (!($user = $this->userRepository->add($data))) {
                return back()->withErrors('error', __('a_admin_management.user not created'));
            }

            if ($role = $this->roleRepository->find($request->role_id)) {
                $user->assign($role);
            } else {
                return back()->withErrors('error', __('a_admin_management.role not found'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.index'))->withMessage(__('a_admin_management.failed to create admin'));
        }

        return redirect(route('admin.management.admins.index'))->with('success', __('a_admin_management.admin_created_successfuly'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user) {
            return back()->withErrors('error', __('a_admin_management.user not found'));
        }

        if ($user->is_member) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        return view('user::admin.management.admin.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        DB::beginTransaction();
        try {
            $user = $this->userRepository->find($id);

            if (!$this->userRepository->edit($id, $request->only('name', 'email'))) {
                return back()->withErrors('error', __('a_admin_management.user not updated'));
            }

            if (isset($request->user_status_id) && $user->status->status->id != $request->user_status_id) {
                $status = $this->statusRepository->find($request->user_status_id);
                if (in_array($status->rawname, $this->toLogoutUserStatuses)) {
                    $this->userRepository->edit($id, ['to_logout' => true]);
                } else {
                    $this->userRepository->edit($id, ['to_logout' => false]);
                }
                if (!($userStatus = $this->statusRepository->changeHistory($user, $status, ['updated_by' => auth()->user()->id]))) {
                    return back()->withErrors('error', __('a_admin_management.user status not found'));
                }
            }

            if ($request->role_id) {
                if ($role = $this->roleRepository->find($request->role_id)) {
                    if ($user->role->id != $request->role_id) {
                        $user->retract($this->roleRepository->find($user->role->id));
                        $user->assign($this->roleRepository->find($request->role_id));
                    }
                } else {
                    return back()->withErrors('error', __('a_admin_management.role not found'));
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.index'))->withMessage(__('a_admin_management.failed to update admin'));
        }

        return redirect(route('admin.management.admins.index'))->with('success', __('a_admin_management.user updated successfuly'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_admin_management.user not found'));
        }

        DB::beginTransaction();

        try {
            $this->userRepository->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.admins.index'))->withMessage(__('a_admin_management.failed to delete admin'));
        }

        return redirect(route('admin.management.admins.index'))->with('success', __('a_admin_management.user deleted sucessfuly'));
    }
}
