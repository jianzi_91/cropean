<?php

namespace Modules\User\Http\Controllers\Admin\Management\Member;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Modules\Rbac\Contracts\RoleContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Http\Requests\Admin\Management\Member\Edit;
use Modules\User\Http\Requests\Admin\Management\Member\Store;
use Modules\User\Http\Requests\Admin\Management\Member\Update;
use Modules\User\Models\User;
use Modules\User\Queries\Member\UserQuery;
use Plus65\Utility\Exceptions\WebException;

class UserController extends Controller
{
    /**
     * The user status to logout user
     *
     * @var unknown
     */
    protected $toLogoutUserStatuses = [UserStatusContract::HOLD, UserStatusContract::TERMINATED];

    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The user query
     *
     * @var unknown
     */
    protected $userQuery;

    /**
     * The user status repository
     *
     * @var unknown
     */
    protected $statusRepository;

    /**
     * The role repository
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * The class constructor
     *
     * @param UserContract     $userContract
     * @param UserStatusContract $userStatusContract,
     * @param UserQuery        $userQuery
     * @param RoleContract $roleContract
     */
    public function __construct(
        UserContract $userContract,
        UserQuery $userQuery,
        UserStatusContract $userStatusContract,
        RoleContract $roleContract
    ) {
        $this->middleware('permission:admin_user_member_management_list')->only('index');
        $this->middleware('permission:admin_user_member_management_create')->only('create', 'store');
        $this->middleware('permission:admin_user_member_management_edit')->only('edit', 'update');
        $this->middleware('permission:admin_user_member_management_delete')->only('destroy');
        $this->middleware('permission:admin_user_member_management_show_qr')->only('showQR');
        $this->middleware('permission:admin_user_member_management_login_as')->only('loginAs');

        $this->userRepository   = $userContract;
        $this->userQuery        = $userQuery;
        $this->statusRepository = $userStatusContract;
        $this->roleRepository   = $roleContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index(Request $request)
    {
        $users = $this->userQuery
            ->setParameters($request->all())
            ->setLocale(session()->get('locale'))
            ->paginate();

        return view('user::admin.management.member.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function create()
    {
        return view('user::admin.management.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        return redirect()->route('member.register.success');
        DB::beginTransaction();
        try {
            $data = $request->except(['password_confirmation', 'secondary_password_confirmation']);

            if (!($user = $this->userRepository->register($data))) {
                return back()->withErrors('error', __('a_member_management.user not created'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.index'))->withMessage(__('a_member_management.failed to create user'));
        }

        return redirect(route('admin.management.members.index'))->with('success', __('a_member_management.user created successfuly'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        if (!$user->is_member) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        return view('user::admin.management.member.edit')->with(compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        DB::beginTransaction();

        try {
            $data = $request->except(['special_bonus_percentage', 'member_id', 'password_confirmation', 'secondary_password_confirmation']);

            if (isset($data['password']) && $data['password']) {
                $data['password'] = bcrypt($data['password']);
            } else {
                unset($data['password']);
            }

            if (isset($data['secondary_password']) && $data['secondary_password']) {
                $data['secondary_password'] = bcrypt($data['secondary_password']);
            } else {
                unset($data['secondary_password']);
            }

            if (isset($data['birth_date'])) {
                $data['birth_date'] = new Carbon($data['birth_date']);
            }

            if (isset($data['user_status_id']) && $user->user_status_id != $data['user_status_id']) {
                $status = $this->statusRepository->find($data['user_status_id']);
                if (in_array($status->rawname, $this->toLogoutUserStatuses)) {
                    $this->userRepository->edit($id, ['to_logout' => true]);
                } else {
                    $this->userRepository->edit($id, ['to_logout' => false]);
                }
                // Update of member status and member role
                if (!($userStatus = $this->statusRepository->changeHistory($user, $status, ['updated_by' => auth()->user()->id])) || !($roleUpdate = $this->roleRepository->updateMemberRole($user, $status))) {
                    return back()->withErrors('error', __('a_member_management.user not updated'));
                }
            }

            if (!($result = $this->userRepository->edit($id, $data))) {
                return back()->withErrors('error', __('a_member_management.user not updated'));
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.index'))->withMessage(__('a_member_management.failed to update user'));
        }

        return back()->with('success', __('a_member_management.user updated successfuly'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        DB::beginTransaction();

        try {
            $this->userRepository->delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.index'))->withMessage(__('a_member_management.failed to delete user'));
        }

        return redirect(route('admin.management.members.index'))->with('success', __('a_member_management.user deleted sucessfully'));
    }

    /**
     * Login as
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function loginAs($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user) {
            return back()->withErrors('error', __('a_member_management.user not found'));
        }

        return view('user::admin.management.member.login_as')->with(compact('user'));
    }
}
