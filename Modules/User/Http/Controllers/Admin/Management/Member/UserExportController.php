<?php

namespace Modules\User\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\User\Queries\Member\UserQuery;
use QueryBuilder\Concerns\CanExportTrait;

class UserExportController extends Controller
{
    use CanExportTrait;

    /**
     * The member export query
     *
     * @var unknown
     */
    protected $exportQuery;

    /**
     * Class constructor
     *
     * @param UserQuery $userQuery
     */
    public function __construct(UserQuery $userQuery)
    {
        $this->middleware('permission:admin_user_member_management_export')->only('export');

        $this->exportQuery = $userQuery;
    }

    /*
     * @param Request $request
     * @param $type
    */
    public function export(Request $request)
    {
        return $this->exportReport($this->exportQuery->setParameters($request->all()), 'member_' . now() . '.xlsx', auth()->user(), 'Export Member on ' . now());
    }
}
