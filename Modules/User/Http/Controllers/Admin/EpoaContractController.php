<?php

namespace Modules\User\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Contracts\UserEpoaContractContract;

class EpoaContractController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct(
        UserEpoaContractContract $userEpoaContractContract
    ) {
        $this->middleware('permission:admin_user_member_management_epoa_view')->only('show');

        $this->userEpoaContractRepo = $userEpoaContractContract;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $contract = $this->userEpoaContractRepo->findBySlug($id);

        if (!$contract) {
            return;
        }

        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $contract->path . DIRECTORY_SEPARATOR . $contract->filename;

        return response()->file($path, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition: attachment; filename="' . basename($contract->original_filename) . '"'
        ]);
    }
}
