<?php

namespace Modules\User\Http\Controllers\Admin\Profile;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Requests\Admin\Profile\Update;
use Modules\User\Models\User;
use Plus65\Utility\Exceptions\WebException;

class ProfileController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('permission:admin_profile_edit')->only('index', 'update');

        $this->userRepository = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $user = auth()->user();

        return view('user::admin.profile.index', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Update $request)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            $data = $request->only(['name', 'email', 'country_id']);

            $this->userRepository->edit($user->id, $data);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.profile.index'))->withMessage(__('a_my_profile.failed to update profile'));
        }

        return back()->with('success', __('a_my_profile.user profile update successfully'));
    }
}
