<?php

namespace Modules\User\Http\Controllers\Member;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Contracts\UserSignatureContract;

class UserSignatureController extends Controller
{
    /**
     * UserSignatureContract Repository
     *
     */
    protected $userSignatureRepository;

    /**
     * Class constructor
     *
     * @param UserSignatureContract $userSignatureContract
     */
    public function __construct(
        UserSignatureContract $userSignatureContract
    ) {
        $this->userSignatureRepository = $userSignatureContract;
    }

    /**
     * Show the specified resource.
     * @param string $filename
     * @return Response
     */
    public function show($filename)
    {
        $contract = $this->userSignatureRepository->findBySlug($filename);

        if (!$contract) {
            return;
        }

        return $this->userSignatureRepository->get($contract->id);
    }
}
