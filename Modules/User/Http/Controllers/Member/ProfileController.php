<?php

namespace Modules\User\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Modules\User\Contracts\UserContract;
use Modules\User\Http\Requests\Member\Profile\Update;
use Plus65\Utility\Exceptions\WebException;

class ProfileController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $user
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('permission:member_profile_edit')->only('index', 'update');

        $this->userRepository = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        $user = auth()->user();
        return view('user::member.profile.index', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        DB::beginTransaction();

        try {
            $user = auth()->user();
            $data = $request->only(['name', 'email', 'id_number', 'birth_date', 'gender', 'country_id', 'address', 'mobile_prefix', 'mobile_number']);
            if (isset($data['birth_date'])) {
                $data['birth_date'] = new Carbon($data['birth_date']);
            }

            $this->userRepository->edit($user->id, $data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route(route('member.profile.index')), __('m_my_profile.failed to update profile'));
        }

        return back()->with('success', __('m_my_profile.user profile update successfully'));
    }
}
