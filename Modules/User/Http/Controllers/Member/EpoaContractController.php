<?php

namespace Modules\User\Http\Controllers\Member;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\User\Contracts\UserEpoaContractContract;
use Modules\User\Contracts\UserSignatureContract;
use Modules\User\Http\Requests\Member\Epoa\Store;
use Modules\User\Jobs\GenerateEpoaContractPdf;

class EpoaContractController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct(
        UserSignatureContract $userSignatureContract,
        UserEpoaContractContract $userEpoaContractContract,
        PDF $pdfService
    ) {
        $this->userSignatureRepo    = $userSignatureContract;
        $this->userEpoaContractRepo = $userEpoaContractContract;
        $this->pdfService           = $pdfService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::member.epoa.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $user              = auth()->user();
        $penaltySetting    = setting('penalty_percentage');
        $penaltyPercentage = json_decode($penaltySetting, true);

        if ($user->epoaContract) {
            return redirect(route('member.deposits.index'))->with('error', __('m_epoa.epoa contract is signed'));
        }

        return view('user::member.epoa.create', compact('user', 'penaltyPercentage'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Store $request)
    {
        DB::beginTransaction();

        try {
            $user                = auth()->user();
            $signatureAttachment = $request->file('signature');
            $signature           = $this->userSignatureRepo->attachUploadedFile($user->id, $signatureAttachment, true);
            $locale              = session()->get('locale');
            DB::commit();

            GenerateEpoaContractPdf::dispatchNow($user, $signature, $locale);
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', $e->getMessage());
        }

        return redirect(route('member.deposits.create'))->with('success', 'm_epoa.epoa contract is signed successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $contract = $this->userEpoaContractRepo->findBySlug($id);

        if (!$contract) {
            abort(404);
        }

        if ($contract->user_id != auth()->user()->id) {
            abort(404);
        }

        $path = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $contract->path . DIRECTORY_SEPARATOR . $contract->filename;

        return response()->file($path, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition: attachment; filename="' . basename($contract->original_filename) . '"'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }
}
