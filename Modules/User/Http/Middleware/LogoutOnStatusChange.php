<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogoutOnStatusChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($user = $request->user()) {
            if ($user->to_logout) {
                session()
                ->flash('error', __('messages.you have been logged out because your account status has changed'));
                $user->to_logout = false;
                $user->save();
                Auth::logout();
                return redirect('/login');
            }
        }

        return $next($request);
    }
}
