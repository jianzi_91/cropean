<?php

namespace Modules\User\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Contracts\UserContract;

class UserIsMember implements Rule
{
    /**
     * The user repository
     *
     * @return void
     */
    protected $userRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userRepository = resolve(UserContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($user = $this->userRepository->findBySlug($value))) {
            if (!($user = $this->userRepository->findByEmail($value))) {
                if (!($user = $this->userRepository->findByIdentifier($value))) {
                    return false;
                }
            }
        }

        return $user->is_member;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.invalid user');
    }
}
