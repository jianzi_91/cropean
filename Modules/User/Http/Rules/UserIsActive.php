<?php

namespace Modules\User\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Contracts\UserContract;
use Modules\User\Contracts\UserStatusContract;

class UserIsActive implements Rule
{
    /**
     * The user repository
     *
     * @return void
     */
    protected $userRepository;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userRepository       = resolve(UserContract::class);
        $this->userStatusRepository = resolve(UserStatusContract::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!($user = $this->userRepository->findByIdentifier($value))) {
            return false;
        }

        return $user->status->status->rawname == $this->userStatusRepository::ACTIVE;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.:attribute is not an active user');
    }
}
