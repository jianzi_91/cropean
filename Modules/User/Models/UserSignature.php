<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserSignature extends Model
{
    protected $table    = "user_signatures";
    protected $fillable = [];

    /**
     * This model's relation to epao contract.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
