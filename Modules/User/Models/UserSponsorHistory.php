<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Repositories\Concerns\HasHistory;

class UserSponsorHistory extends Model
{
    use SoftDeletes, HasHistory;

    protected $fillable = [
        'user_id',
        'sponsor_id',
        'updated_by',
        'is_current'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function sponsor()
    {
        return $this->hasOne(User::class, 'sponsor_id');
    }

    public function updatedBy()
    {
        return $this->hasOne(User::class, 'updated_by');
    }
}
