<?php

namespace Modules\User\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Models\BlockchainWallet;
use Modules\Campaign\Models\CampaignParticipant;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Country\Models\Country;
use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Models\BankAccount;
use Modules\Credit\Models\CreditConversion;
use Modules\Deposit\Models\Deposit;
use Modules\Deposit\Models\DepositStatus;
use Modules\Password\Notifications\ResetPasswordToken;
use Modules\Promotion\Models\PromotionPayout;
use Modules\Promotion\Models\PromotionPayoutBreakdown;
use Modules\Rank\Models\GoldmineRank;
use Modules\Rank\Models\Rank;
use Modules\Rank\Models\UserGoldmineRankHistorySnapshot;
use Modules\Rank\Models\UserRankHistorySnapshot;
use Modules\Rbac\Models\Role;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\User\Contracts\UserStatusContract;
use Modules\Withdrawal\Models\WithdrawalBank;
use Plus65\Base\Models\Contracts\BypassableContract;
use Plus65\Base\Models\Contracts\Stateable;
use Plus65\Base\Models\Relations\HistoryRelation;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable implements BypassableContract, Stateable
{
    use Notifiable, SoftDeletes, CanResetPassword, HasRolesAndAbilities, HistoryRelation, LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty  = true;

    protected $fillable = [
        'name',
        'last_name',
        'member_id',
        'username',
        'email',
        'password',
        'secondary_password',
        'id_number',
        'referrer_id',
        'birth_date',
        'gender',
        'mobile_prefix',
        'mobile_number',
        'country_id',
        'province',
        'city',
        'region',
        'address',
        'user_status_id',
        'created_by',
        'is_system_generated',
        'to_logout',
        'special_bonus_percentage',
        'view_special_bonus',
        'last_login',
        'last_logout',
        'rank_id',
        'goldmine_rank_id',
        'goldmine_level',
        'document_verification_status_id',
        'locale',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * This model's relation to user status histories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(UserStatusHistory::class, 'user_id');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accountStatus()
    {
        return $this->belongsTo(UserStatus::class, 'user_status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(UserStatusHistory::class, 'user_id')->current(1);
    }

    public function sponsorHistories()
    {
        return $this->hasMany(UserSponsorHistory::class, 'user_id');
    }

    public function blockchainWallets()
    {
        return $this->hasMany(BlockchainWallet::class, 'user_id');
    }

    public function conversions()
    {
        return $this->hasMany(CreditConversion::class, 'user_id');
    }

    /**
     * This model's relation to bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function banks()
    {
        return $this->hasMany(WithdrawalBank::class, 'user_id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordToken($token));
    }

    public function canLogin()
    {
        $userStatusContract = resolve(UserStatusContract::class);
        return in_array($this->status->status->rawname, [$userStatusContract::ACTIVE, $userStatusContract::SUSPENDED]);
    }

    /**
     * The bank account
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function bankAccount()
    {
        return $this->morphOne(BankAccount::class, 'reference');
    }

    /**
     * Custom attribute for role
     *
     * @return \Modules\Rbac\Models\Role
     */
    public function getRoleAttribute()
    {
        return $this->roles->first();
    }

    /**
     * Custom attribute for sponsor id
     *
     * @return string
     */
    public function getSponsorIdAttribute()
    {
        try {
            $sponsorTreeRepository = resolve(SponsorTreeContract::class);
            $upline                = $sponsorTreeRepository->directUpline($this->id);

            return $upline ? $upline->user->member_id : '';
        } catch (\Exception $e) {
            // That means this user can't be found
        }

        return '';
    }

    /**
     * Custom attribute for downline
     */
    public function getHasDownlinesAttribute()
    {
        $sponsorTreeRepo = resolve(SponsorTreeContract::class);
        $sponsorTree     = $sponsorTreeRepo->getModel()->where('user_id', $this->id)->first();
        $downlines       = $sponsorTreeRepo->getModel()->where('parent_id', $sponsorTree->id)->get();

        if ($downlines->count() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Custom attribute for is member flag
     *
     * @return string
     */
    public function getIsMemberAttribute()
    {
        if ($this->isA(Role::MEMBER)) {
            return true;
        }
        if ($this->isA(Role::MEMBER_SUSPENDED)) {
            return true;
        }
        if ($this->isA(Role::MEMBER_TERMINATED)) {
            return true;
        }
        if ($this->isA(Role::MEMBER_ON_HOLD)) {
            return true;
        }

        return false;
    }

    /**
     * Check if user is normal admin
     * @return boolean
     */
    public function getIsAdminAttribute()
    {
        return !$this->is_member && !$this->isA(Role::SYSADMIN);
    }

    /**
     * Can by bypass for ownership rules
     *
     * @return boolean
     */
    public function bypass()
    {
        return false;
    }

    /**
     * USDT Credit Balance
     * @return string
     */
    public function getCapitalCreditBalanceAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::CAPITAL_CREDIT);
    }

    public function getRolloverCreditBalanceAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::ROLL_OVER_CREDIT);
    }

    public function getCashCreditBalanceAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::CASH_CREDIT);
    }

    public function getPromotionCreditBalanceAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::PROMOTION_CREDIT);
    }

    public function getGoldmineRankNameAttribute()
    {
        return $this->goldmineRank->name ?? null;
    }

    public function getLeaderBonusRankNameAttribute()
    {
        return $this->rank->name ?? null;
    }

    public function getTotalDepositedAmountAttribute()
    {
        $approvedStatus = DepositStatus::where('name', 'approved')->first();

        return $this->deposits->where('deposit_status_id', $approvedStatus->id)->sum('amount');
    }

    public function getSpecialBonusPercentage()
    {
        return is_null($this->special_bonus_percentage) ? null : bcmul($this->special_bonus_percentage, '100', 2);
    }

    /**
     * Member scope
     */
    public function scopeMembers($query)
    {
        $memberRoles = Role::whereIn('name', [Role::MEMBER, Role::MEMBER_SUSPENDED, Role::MEMBER_TERMINATED, Role::MEMBER_ON_HOLD])->pluck('id')->toArray();

        return $query->join('assigned_roles', function ($join) use ($memberRoles) {
            $join->on('assigned_roles.entity_id', '=', 'users.id')
                ->where('assigned_roles.entity_type', 'Modules\User\Models\User')
                ->whereIn('role_id', $memberRoles);
        })
        ->where('is_system_generated', false);
    }

    /**
     * Active status scope
     */
    public function scopeActive($query)
    {
        $userStatusRepository = resolve(UserStatusContract::class);
        $activeStatus         = $userStatusRepository->findBySlug($userStatusRepository::ACTIVE);

        return $query->where('user_status_id', $activeStatus->id);
    }

    /**
     * Get the state id
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->id;
    }

    /**
     * Get the foreign key
     * @return string
     */
    public function getForeignKey()
    {
        return 'user_id';
    }

    /**
     * Get the history model
     *
     * @return string
     */
    public function getHistoryModel()
    {
        return UserStatusHistory::class;
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * This model's relation to rank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo(Rank::class, 'rank_id');
    }

    public function goldmineRank()
    {
        return $this->belongsTo(GoldmineRank::class, 'goldmine_rank_id');
    }

    public function goldmineRankHistorySnapshots()
    {
        return $this->hasMany(UserGoldmineRankHistorySnapshot::class, 'user_id');
    }

    public function leaderBonusRankHistorySnapshots()
    {
        return $this->hasMany(UserRankHistorySnapshot::class, 'user_id');
    }

    public function promotionPayout()
    {
        return $this->hasOne(PromotionPayout::class, 'user_id')->where('promotion_id', 1);
    }

    public function junePromotionContribution()
    {
        return $this->hasOne(PromotionPayoutBreakdown::class, 'user_id');
    }

    public function julyPromotionPayout()
    {
        return $this->hasOne(PromotionPayout::class, 'user_id')->where('promotion_id', 2);
    }

    public function augustPromotionPayout()
    {
        return $this->hasOne(PromotionPayout::class, 'user_id')->where('promotion_id', 3);
    }

    /**
     * This model's relation to rank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goldmine()
    {
        return $this->hasOne(UserGoldmineLevelHistory::class, 'user_id')->current(1);
    }

    /**
     * This model's relation to epao contract.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function epoaContract()
    {
        return $this->hasOne(UserEpoaContract::class, 'user_id');
    }

    /**
     * This model's relation to signature.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signature()
    {
        return $this->hasOne(UserSignature::class, 'user_id');
    }

    /**
     * This model's relation to deposit.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deposits()
    {
        return $this->hasMany(Deposit::class, 'user_id');
    }

    public function referrer()
    {
        return $this->hasOne(User::class, 'member_id', 'referrer_id');
    }

    public function campaignParticipations()
    {
        return $this->hasMany(CampaignParticipant::class, 'user_id');
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getCapitalCreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::CAPITAL_CREDIT);
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getRollOverCreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::ROLL_OVER_CREDIT);
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getCashCreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::CASH_CREDIT);
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getPromotionCreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::PROMOTION_CREDIT);
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getUsdtErc20CreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::USDT_ERC20);
    }

    /**
     * Custom attribute for credit
     * @return string
     */
    public function getUsdcCreditAttribute()
    {
        $account = resolve(BankAccountCreditContract::class);
        return $account->getBalanceByReference($this->id, BankCreditTypeContract::USDC);
    }

    public function getTotalEquityAttribute()
    {
        return bcsum([$this->capital_credit, $this->roll_over_credit, $this->promotion_credit]);
    }

    /**
     * Listening to events
     * @throws Plus65Exception
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            // Create user status histories
            $userStatusRepository = resolve(UserStatusContract::class);
            $activeStatus = $userStatusRepository->findBySlug($userStatusRepository::ACTIVE);
            $result = $userStatusRepository->changeHistory($user, $activeStatus);
            if (!$result) {
                throw new Plus65Exception();
            }

            // Create bank account
            $bankAccountRepository = resolve(BankAccountContract::class);
            $bankAccountRepository->add([
                'name'           => $user->member_id,
                'reference_id'   => $user->id,
                'reference_type' => User::class,
            ]);

            // Set created by
            $loginUser = auth()->user();
            $user->created_by = $loginUser ? $loginUser->id : $user->id;
            $user->save();

            // Create wallet
            $ethRepo = resolve(EthereumContract::class);
            $wallet = $ethRepo->createNewAddress();
            $walletRepo = resolve(BlockchainWalletContract::class);
            $data = [
                'address'     => $wallet['address'],
                'private_key' => encrypt($wallet['private_key']),
                'qr_code'     => $walletRepo->generateQrCode($wallet['address']),
            ];
            $walletRepo->add([
                'address'             => $data['address'],
                'user_id'             => $user->id,
                'bank_credit_type_id' => bank_credit_type_id('usdt_erc20'),
                'private_key'         => $data['private_key'],
                'qr_code'             => $data['qr_code'],
            ]);

            $walletRepo->add([
                'address'             => $data['address'],
                'user_id'             => $user->id,
                'bank_credit_type_id' => bank_credit_type_id('usdc'),
                'private_key'         => $data['private_key'],
                'qr_code'             => $data['qr_code'],
            ]);
        });

        static::deleted(function ($user) {
            //update users table unique column value
            $user->email = "{$user->email}.deleted.{$user->id}";
            $user->member_id = "{$user->member_id}.deleted.{$user->id}";
            $user->id_number = "{$user->id_number}.deleted.{$user->id}";
            $user->save();
        });
    }
}
