<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserEpoaContract extends Model
{
    protected $table    = "user_epoa_contracts";
    protected $fillable = [];

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * This model's relation to user signature.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function signature()
    {
        return $this->hasOne(UserSignature::class, 'user_signature_id');
    }
}
