<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.member_url')], function () {
        Route::resource('/profile', 'Member\ProfileController', ['names' => 'member.profile', 'only' => ['index', 'update']]);
        Route::resource('/epoa', 'Member\EpoaContractController', ['names' => 'member.epoa'])->only(['index', 'show', 'create', 'store']);
    });
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::resource('/profile', 'Admin\Profile\ProfileController', ['names' => 'admin.profile', 'only' => ['index', 'update']]);

        Route::get('members/{id}/login_as', ['as' => 'admin.management.members.login_as', 'uses' => 'Admin\Management\Member\UserController@loginAs']);
        Route::get('/admins/export', ['as' => 'admin.management.admins.export', 'uses' => 'Admin\Management\Admin\UserExportController@export']);
        Route::get('/members/export', ['as' => 'admin.management.members.export', 'uses' => 'Admin\Management\Member\UserExportController@export']);
        Route::resource('/members', 'Admin\Management\Member\UserController', ['as' => 'admin.management', 'except' => ['show']]);
        Route::get('/member/epoa/{id}', 'Admin\EpoaContractController@show')->name('admin.management.member.epoa');
        Route::resource('/admins', 'Admin\Management\Admin\UserController', ['as' => 'admin.management', 'except' => ['show']]);
    });
});

Route::group(['middleware' => ['web']], function () {
    Route::group(['domain' => config('core.member_url')], function () {
        Route::group(['prefix' => 'epoa'], function () {
            Route::get('/signature/{filename}', ['uses' => 'Member\UserSignatureController@show'])->name('member.epoa.signature.show');
        });
    });
});
