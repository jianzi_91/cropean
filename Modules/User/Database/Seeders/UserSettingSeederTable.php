<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class UserSettingSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'member_id_setting',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'member_id_start_prefix',
                'setting_category_id' => SettingCategory::where('name', 'member_id_setting')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'member_id_length',
                'setting_category_id' => SettingCategory::where('name', 'member_id_setting')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'system_member_id_characters',
                'setting_category_id' => SettingCategory::where('name', 'member_id_setting')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'member_id_start_prefix')->first()->id,
                'value'      => '01',
            ],
            [
                'setting_id' => Setting::where('name', 'member_id_length')->first()->id,
                'value'      => '8',
            ],
            [
                'setting_id' => Setting::where('name', 'system_member_id_characters')->first()->id,
                'value'      => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
