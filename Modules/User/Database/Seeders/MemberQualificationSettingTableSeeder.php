<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class MemberQualificationSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'member_qualification',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'member_minimum_deposit_amount_qualification',
                'setting_category_id' => SettingCategory::where('name', 'member_qualification')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'member_qualification_period',
                'setting_category_id' => SettingCategory::where('name', 'member_qualification')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'member_minimum_deposit_amount_qualification')->first()->id,
                'value'      => '500',
            ],
            [
                'setting_id' => Setting::where('name', 'member_qualification_period')->first()->id,
                'value'      => '7',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
