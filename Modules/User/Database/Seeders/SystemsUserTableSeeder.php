<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Seeder;
use Modules\Rbac\Models\Role;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Repositories\SponsorTreeRepository;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Models\UserSponsorHistory;
use Modules\User\Repositories\UserRepository;

class SystemsUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();
        /** @var UserRepository $userContract */
        $userContract = resolve(UserContract::class);
        /** @var SponsorTreeRepository $sponsorTreeContract */
        $sponsorTreeContract = resolve(SponsorTreeContract::class);
        $users               = [
            [
                'name'                => 'systemroot',
                'username'            => 'systemroot',
                'member_id'           => $userContract->generateMemberId(true),
                'email'               => 'systemroot@test.com',
                'password'            => bcrypt('Qwer1234!'),
                'secondary_password'  => bcrypt('Qwer1234!'),
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s'),
                'country_id'          => 1,
                'birth_date'          => Carbon::now()->subYears(18),
                'is_system_generated' => 1,
            ]];
        $role = Role::where('name', Role::MEMBER)->firstOrFail();
        foreach ($users as $user) {
            $user = new User($user);
            $user->save();

            $user->assign($role);
        }
        $sponsorUser = $userContract->findByIdentifier('root');
        if (!$sponsorUser) {
            throw new ModelNotFoundException();
        }

        $sponsorTreeContract->add(['user_id' => $user->id], $sponsorUser->id);
        $attribute = [
            'user_id'    => User::where('username', 'systemroot')->first()->id,
            'sponsor_id' => 1,
            'is_current' => 1
        ];
        (new UserSponsorHistory($attribute))->save();
    }
}
