<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class UserPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_profile' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Profile',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_profile_edit',
                        'title' => 'Admin Profile Edit',
                    ]
                ]
            ],
            'admin_user_admin_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Admin Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_admin_management_list',
                        'title' => 'Admin User Admin Management List',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_create',
                        'title' => 'Admin User Admin Management Create',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_edit',
                        'title' => 'Admin User Admin Management Edit',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_export',
                        'title' => 'Admin User Admin Management Export',
                    ],
                    [
                        'name'  => 'admin_user_admin_management_adjust_credit',
                        'title' => 'Admin User Admin Management Adjust Credit',
                    ],
//                    [
//                        'name'  => 'admin_user_admin_management_transfer_credit',
//                        'title' => 'Admin User Admin Management Transfer Credit',
//                    ],
                    [
                        'name'  => 'admin_user_admin_management_update_permissions',
                        'title' => 'Admin User Admin Management Update Permissions',
                    ],
                ]
            ],
            'admin_user_member_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Member Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_list',
                        'title' => 'Admin User Member Management List',
                    ],
                    [
                        'name'  => 'admin_user_member_management_edit',
                        'title' => 'Admin User Member Management Edit',
                    ],
                    [
                        'name'  => 'admin_user_member_management_export',
                        'title' => 'Admin User Member Management Export',
                    ],
                    [
                        'name'  => 'admin_user_member_management_adjust_credit',
                        'title' => 'Admin User Member Management Adjust Credit',
                    ],
//                    [
//                        'name'  => 'admin_user_member_management_transfer_credit',
//                        'title' => 'Admin User Member Management Transfer Credit',
//                    ],
                    [
                        'name'  => 'admin_user_member_management_update_permissions',
                        'title' => 'Admin User Member Management Update Permissions',
                    ],
                    [
                        'name'  => 'admin_user_member_management_update_tree',
                        'title' => 'Admin User Member Management Update Tree',
                    ],
                    [
                        'name'  => 'admin_user_member_management_login_as',
                        'title' => 'Admin User Member Management Login As',
                    ],
                ]
            ],
            'member_profile' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Profile',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_profile_edit',
                        'title' => 'Member Profile Edit',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_profile_edit',
                'admin_user_admin_management_list',
                'admin_user_admin_management_create',
                'admin_user_admin_management_edit',
                'admin_user_admin_management_export',
                'admin_user_admin_management_adjust_credit',
//                'admin_user_admin_management_transfer_credit',
                'admin_user_admin_management_update_permissions',
                'admin_user_member_management_list',
                'admin_user_member_management_edit',
                'admin_user_member_management_export',
                'admin_user_member_management_adjust_credit',
//                'admin_user_member_management_transfer_credit',
                'admin_user_member_management_update_permissions',
                'admin_user_member_management_update_permissions',
                'admin_user_member_management_update_tree',
                'admin_user_member_management_login_as',
            ],
            Role::ADMIN => [
                'admin_profile_edit',
                'admin_user_admin_management_list',
                'admin_user_admin_management_create',
                'admin_user_admin_management_edit',
                'admin_user_admin_management_export',
                'admin_user_admin_management_adjust_credit',
//                'admin_user_admin_management_transfer_credit',
                'admin_user_admin_management_update_permissions',
                'admin_user_member_management_list',
                'admin_user_member_management_edit',
                'admin_user_member_management_export',
                'admin_user_member_management_adjust_credit',
//                'admin_user_member_management_transfer_credit',
                'admin_user_member_management_update_permissions',
                'admin_user_member_management_update_permissions',
                'admin_user_member_management_update_tree',
                'admin_user_member_management_login_as',
            ],
            Role::MEMBER => [
                'member_profile_edit',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
