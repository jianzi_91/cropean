<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorPageContract;
use Modules\User\Models\UserStatus;

class UserStatusSeederTableSeeder extends Seeder
{
    /**
     * Page repository.
     *
     * @var TranslatorPageContract
     */
    protected $pageRepo;

    /**
     * Group repository.
     *
     * @var TranslatorGroupContract
     */
    protected $groupRepo;

    /**
     * Class constructor.
     *
     * @param TranslatorPageContract $pageContract
     * @param TranslatorGroupContract $groupContract
     */
    public function __construct(TranslatorPageContract $pageContract, TranslatorGroupContract $groupContract)
    {
        $this->pageRepo  = $pageContract;
        $this->groupRepo = $groupContract;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->pageRepo->add([
            'name'                => 's_user_statuses',
            'translator_group_id' => $this->groupRepo->findBySlug('system')->id
        ]);

        $statuses = [
            0 => [
                'name'      => 'active',
                'is_active' => 1,
            ],
            1 => [
                'name'      => 'hold',
                'is_active' => 1,
            ],
            2 => [
                'name'      => 'suspended',
                'is_active' => 1,
            ],
            3 => [
                'name'      => 'terminated',
                'is_active' => '1',
            ],
        ];

        foreach ($statuses as $status) {
            $result = (new UserStatus($status))->save();
        }
    }
}
