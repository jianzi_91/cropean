<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Rbac\Models\Role;
use Modules\Tree\Models\SponsorTree;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Models\UserSponsorHistory;

class InitialUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();
        $this->insertUsers();
        $this->insertTree();
        $this->insertAdmins();
    }

    public function insertAdmins()
    {
        $sysadminData = [
            [
                'name'               => 'sysadmin',
                'username'           => 'sysadmin',
                'email'              => 'sysadmin@test.com',
                'member_id'          => 'sysadmin',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadmin88',
                'username'           => 'sysadmin88',
                'email'              => 'sysadmin88@test.com',
                'member_id'          => 'sysadmin88',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminfe',
                'username'           => 'sysadminfe',
                'email'              => 'sysadminfe@test.com',
                'member_id'          => 'sysadminfe',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminpm',
                'username'           => 'sysadminpm',
                'email'              => 'sysadminpm@test.com',
                'member_id'          => 'sysadminpm',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminqa',
                'username'           => 'sysadminqa',
                'email'              => 'sysadminqa@test.com',
                'member_id'          => 'sysadminqa',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminbl',
                'username'           => 'sysadminbl',
                'email'              => 'sysadminbl@test.com',
                'member_id'          => 'sysadminbl',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminps',
                'username'           => 'sysadminps',
                'email'              => 'sysadminps@test.com',
                'member_id'          => 'sysadminps',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
            [
                'name'               => 'sysadminjt',
                'username'           => 'sysadminjt',
                'email'              => 'sysadminjt@test.com',
                'member_id'          => 'sysadminjt',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],

        ];

        $sysadminRole = Role::where('name', Role::SYSADMIN)->firstOrFail();
        foreach ($sysadminData as $admin) {
            $user = new User($admin);
            $user->save();

            $user->assign($sysadminRole);
        }

        $adminData = [
            [
                'name'               => 'admin',
                'username'           => 'admin',
                'member_id'          => 'admin',
                'email'              => 'admin@test.com',
                'password'           => bcrypt('Qwer1234'),
                'secondary_password' => bcrypt('Qwer1234'),
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s'),
                'country_id'         => 1,
                'birth_date'         => Carbon::now()->subYears(18),
            ],
        ];

        $adminRole = Role::where('name', Role::ADMIN)->firstOrFail();
        foreach ($adminData as $admin) {
            $user = new User($admin);
            $user->save();

            $user->assign($adminRole);
        }
    }

    public function insertUsers()
    {
        $userContract = resolve(UserContract::class);
        $users        = [
            [
                'name'                => 'root',
                'username'            => 'root',
                'member_id'           => $userContract->generateMemberId(true),
                'email'               => 'root@test.com',
                'password'            => bcrypt('Qwer1234'),
                'secondary_password'  => bcrypt('Qwer1234'),
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s'),
                'country_id'          => 1,
                'birth_date'          => Carbon::now()->subYears(18),
                'is_system_generated' => 1,
            ],
        ];

        if (!in_array(strtolower(config('app.env')), ['uat', 'production'])) {
            $users = array_merge($users, [
                [
                    'name'               => 'user1',
                    'username'           => 'user1',
                    'member_id'          => $userContract->generateMemberId(),
                    'email'              => 'user1@test.com',
                    'password'           => bcrypt('Qwer1234'),
                    'secondary_password' => bcrypt('Qwer1234'),
                    'created_at'         => date('Y-m-d H:i:s'),
                    'updated_at'         => date('Y-m-d H:i:s'),
                    'country_id'         => 1,
                    'birth_date'         => Carbon::now()->subYears(18),
                ],
                [
                    'name'               => 'user2',
                    'username'           => 'user2',
                    'member_id'          => $userContract->generateMemberId(),
                    'email'              => 'user2@test.com',
                    'password'           => bcrypt('Qwer1234'),
                    'secondary_password' => bcrypt('Qwer1234'),
                    'created_at'         => date('Y-m-d H:i:s'),
                    'updated_at'         => date('Y-m-d H:i:s'),
                    'country_id'         => 1,
                    'birth_date'         => Carbon::now()->subYears(18),
                ]
            ]);
        }

        $memberRole = Role::where('name', Role::MEMBER)->firstOrFail();
        foreach ($users as $user) {
            $user = new User($user);
            $user->save();

            $user->assign($memberRole);
        }
    }

    public function insertTree()
    {
        $table = [];
        if (!in_array(strtolower(config('app.env')), ['uat', 'production'])){
            $table = [
                ['user_id' => 2, 'children' => [
                ]],
                ['user_id' => 3, 'children' => [
                ]]
            ];
        }
        $sponsorTree = [
            ['user_id' => 1, 'children' => $table]
        ];

        SponsorTree::buildTree($sponsorTree);

        if (!in_array(strtolower(config('app.env')), ['uat', 'production'])) {
            $attribute = [
                'user_id'    => 2,
                'sponsor_id' => 1,
                'is_current' => 1
            ];
            (new UserSponsorHistory($attribute))->save();
            $attribute1 = [
                'user_id'    => 3,
                'sponsor_id' => 1,
                'is_current' => 1
            ];
            (new UserSponsorHistory($attribute1))->save();
        }
    }
}
