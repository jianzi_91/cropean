<?php

namespace Modules\User\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rbac\Models\Role;
use Modules\Tree\Models\SponsorTree;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Models\UserSponsorHistory;

class AddInitialCropeanClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();
        DB::beginTransaction();
        $userContract = resolve(UserContract::class);
        $users        = [
            [
                'name'                => 'company@ecntradefx.com',
                'username'            => 'company@ecntradefx.com',
                'member_id'           => $userContract->generateMemberId(false),
                'email'               => 'company@ecntradefx.com',
                'password'            => bcrypt('Ecntradefx1234'),
                'secondary_password'  => bcrypt('Ecntradefx12345'),
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s'),
                'country_id'          => 1,
                'birth_date'          => Carbon::now()->subYears(18),
                'is_system_generated' => 0,
            ]
        ];

        $memberRole = Role::where('name', Role::MEMBER)->firstOrFail();
        foreach ($users as $user) {
            $user = new User($user);
            $user->save();

            $user->assign($memberRole);
        }

        $insertUsers = User::whereIn('email', [
            'company@ecntradefx.com',
        ])->get();

        $parentId   = User::where('username', 'root')->first();
        $attributes = [];
        foreach ($insertUsers as $insertUser) {
            $sponsorTree[] = [
                'created_at' => Carbon::now()->startOfDay()->format('Y-m-d H:i:s'),
                'user_id'    => $insertUser->id,
                'parent_id'  => $parentId->id,
            ];

            $attributes[] = [
                'user_id'    => $insertUser->id,
                'sponsor_id' => $parentId->id,
                'is_current' => 1
            ];

            SponsorTree::insert($sponsorTree);
        }

        SponsorTree::rebuild();

        foreach ($attributes as $attribute) {
            (new UserSponsorHistory($attribute))->save();
        }

        DB::commit();
    }
}
