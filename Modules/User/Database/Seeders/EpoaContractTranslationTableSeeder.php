<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Modules\Translation\Contracts\TranslatorGroupContract;
use Modules\Translation\Contracts\TranslatorLanguageContract;
use Modules\Translation\Contracts\TranslatorPageContract;
use Modules\Translation\Contracts\TranslatorTranslationContract;

class EpoaContractTranslationTableSeeder extends Seeder
{
    protected $translation;
    protected $transGroup;
    protected $transPage;
    protected $transLanguage;

    /**
     * Constructor
     * @param TranslatorTranslationContract $translation
     * @param TranslatorGroupContract $group
     * @param TranslatorPageContract $page
     */
    public function __construct(
        TranslatorTranslationContract $translation,
        TranslatorGroupContract $group,
        TranslatorPageContract $page,
        TranslatorLanguageContract $lang
  ) {
        $this->translation   = $translation;
        $this->transGroup    = $group;
        $this->transPage     = $page;
        $this->transLanguage = $lang;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        Artisan::call('translation:find_keys', ['--latest' => false]);

        $languages = $this->transLanguage->all()->pluck('id', 'code')->toArray();
        foreach ($this->getTranslations() as $group => $translation) {
            $group = $this->transGroup->findBySlug($group);
            if ($group) {
                foreach ($translation as $page => $translations) {
                    $pageModel = $this->transPage->getModel();
                    $pageId    = $pageModel->insertGetId([
                        'created_at'          => now(),
                        'updated_at'          => now(),
                        'name'                => $page,
                        'name_translation'    => str_replace('_', ' ', $page),
                        'translator_group_id' => $group->id,
                    ]);

                    if ($pageId) {
                        foreach ($translations as $key => $trans) {
                            foreach ($languages as $locale => $id) {
                                if (isset($trans[$locale])) {
                                    $row = $this->translation->add([
                                        'translator_language_id' => $id,
                                        'key'                    => $key,
                                        'translator_page_id'     => $pageId,
                                        'value'                  => $trans[$locale]
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        DB::commit();

        Artisan::call('translation:generate_files');

        /**
         * Get translations
         * @return string[][][][]
         */
    }

    protected function getTranslations()
    {
        return [
            'system' => [
                's_epoa_contract_header' => [
                    'title' => [
                        'en' => 'CROPEAN TRADE - POWER OF ATTORNEY',
                        'cn' => 'CROPEAN TRADE - POWER OF ATTORNEY',
                    ],
                    'name' => [
                        'en' => 'Name',
                        'cn' => '姓名',
                    ],
                    'email' => [
                        'en' => 'Email Address',
                        'cn' => '邮箱',
                    ],
                    'cropean member id' => [
                        'en' => 'Cropean Member ID',
                        'cn' => '克比亚会员号',
                    ],
                    'country' => [
                        'en' => 'Country',
                        'cn' => '国家',
                    ],
                    'personal id number' => [
                        'en' => 'Personal ID Number',
                        'cn' => '个人身份证号',
                    ],
                    'address' => [
                        'en' => 'Address',
                        'cn' => '地址',
                    ],
                    'hereinafter referred to as the principal' => [
                        'en' => 'hereinafter referred to as the Principal',
                        'cn' => '以上简称委托人',
                    ],
                    'the princical hereby grants POA to' => [
                        'en' => 'The Principal hereby grants Power of Attorney to',
                        'cn' => '委托人在此授权委托',
                    ],
                    'entity name' => [
                        'en' => 'Entity Name',
                        'cn' => '企业名称',
                    ],
                    'entity name value' => [
                        'en' => 'Cropean Trade Ltd',
                        'cn' => '克比亚国际交易有限公司',
                    ],
                    'entity country' => [
                        'en' => 'Country',
                        'cn' => '注册国家',
                    ],
                    'entity country value' => [
                        'en' => 'Republic of Seychelles',
                        'cn' => '塞舌尔共和国',
                    ],
                    'entity address' => [
                        'en' => 'Address',
                        'cn' => '注册地址',
                    ],
                    'entity address value' => [
                        'en' => '2 Cheetham Hill Road, Cheetham Hill, Manchester, United Kingdom',
                        'cn' => '2 Cheetham Hill Road, Cheetham Hill, Manchester, United Kingdom',
                    ],
                    'jurisdiction' => [
                        'en' => 'Jurisdiction',
                        'cn' => '管辖法规',
                    ],
                    'jurisdiction value' => [
                        'en' => 'International Business Companies Act 1994',
                        'cn' => '1994年国际商业公司法',
                    ],
                    'incorporation number' => [
                        'en' => 'Incorporation Number',
                        'cn' => '企业编号',
                    ],
                    'incorporation number value' => [
                        'en' => '171652',
                        'cn' => '171652',
                    ],
                    'governance' => [
                        'en' => 'Governance',
                        'cn' => '监管机构',
                    ],
                    'governance value' => [
                        'en' => 'Financial Services Authority',
                        'cn' => '金融服务管理局',
                    ],
                    'hereinafter referred to as the agent' => [
                        'en' => 'hereinafter referred to as the Agent',
                        'cn' => '以上简称代理方',
                    ],
                ],
                's_epoa_contract_content' => [
                    'intro' => [
                        'en' => 'to perform, execute and approve all transactions and legal acts according to Cropean Trade Ltd and ECNTRADEFX.COM’s (“Cropean Trade”) general trading and capital operations, for the sole purpose of maximising the Principal’s capital returns, which are entered into with the list of approved partner brokerages Cropean Trade Ltd has vetted and deemed suitable and which govern the client relationship as if they were performed by the Principal itself.',
                        'cn' => '根据克比亚国际交易和ECNTRADEFX.COM（“克比亚国际交易”）的常规交易和资金运作，来执行和批准所有交易和法律行为，其唯一目是为了让委托人获得最大的资本回报，获批准的经纪公司已通过克比亚国际交易的审核并且认定合法，所有交易行为将视为委托人本人意愿。',
                    ],
                    'thus the principal understands and agree that' => [
                        'en' => 'Thus, the Principal understands and agrees that',
                        'cn' => '因此，委托人需了解并同意以下条例',
                    ],
                    'agreement 1' => [
                        'en' => 'The Principal may accept from the Agent, without any inquiry or investigation, any order for the purchase and sale of all instruments available on Cropean Trade’s system, to be executed on a licensed brokerage on the Agent’s list of approved brokerages;',
                        'cn' => '委托人可以在不进行任何询问或调查的情况下，接受代理方在克比亚系统中的所有工具的买卖订单，该订单将在代理方认可的经纪公司名单上的持牌经纪公司中执行；',
                    ],
                    'agreement 2' => [
                        'en' => 'Cropean Trade may establish internet trading facilities, deploy technological trading systems and algorithms according to the instructions of the Agent and thus enable the Agent to execute trades on behalf of the Principal in accordance to any of the Agent’s trading systems;',
                        'cn' => '克比亚国际交易可以根据代理方的指令建立互联网交易设施，部署技术交易系统和算法，使代理方能够根据代理方的任何交易系统代表委托人执行交易;',
                    ],
                    'agreement 3' => [
                        'en' => 'The Agent shall have no responsibility or liability towards the Principal in performing the Agent’s instructions;',
                        'cn' => '代理方履行代理方的指示，对委托人不承担任何责任；',
                    ],
                    'agreement 4' => [
                        'en' => 'Cropean Trade is under no duty to provide advice or otherwise know and review the trading data conducted by and/or any other acts of the proprietary trading algorithms which the Agent relies on to perform its trading and transactions on the account(s) conducted by the Agent;',
                        'cn' => '克比亚国际交易没有义务提供建议或以其他方式了解和审查由任何其他自营交易算法行为（代理方依据这些算法在代理方的帐户中进行交易）所进行的交易数据；',
                    ],
                    'agreement 5' => [
                        'en' => 'Cropean Trade is allowed to reveal all information about the Principal’s account to its brokerage partners and thus, for instance, send copy of any and all personal information, transaction notes, account statements etc. to these brokerages;',
                        'cn' => '克比亚国际交易可向其经纪合作伙伴透露委托人账户中的所有信息，例如：向这些经纪公司发送任何个人信息、交易票据、账户报表等的副本；',
                    ],
                    'agreement 6' => [
                        'en' => 'Cropean Trade is authorized to manage and is responsible for the principal’s funds held with the Principal or with any of the Principal’s licensed brokerage accounts, under an omnibus account;',
                        'cn' => '在综合账户下，克比亚国际交易被授权管理并负责委托人在本人或其任何授权经纪账户中持有的资金；',
                    ],
                    'agreement 7' => [
                        'en' => 'Both the Principal and the Agent will be managing all transfers, trades and transactions to the best of their ability, with the purpose of maximizing capital returns;',
                        'cn' => '委托人和代理方都将尽其所能管理所有的转账、投资和交易，目的是使资本收益最大化；',
                    ],
                    'agreement 8' => [
                        'en' => 'This Power of Attorney acts as an acknowledgement for the early withdrawal fee scheduled on both CROPEAN TRADE and the Client;',
                        'cn' => '本授权书将作为克比亚国际交易及其客户早撤手续费的确认通知；',
                    ],
                    'agreement 9' => [
                        'en' => 'For the 1st – 30th day upon receipt of fund, there will be an early withdrawal fee of :30_days_penalty% on the withdrawal of this fund capital;',
                        'cn' => '单笔入金的第1至30天内，提取本资金将产生:30_days_penalty%的早撤手续费；',
                    ],
                    'agreement 10' => [
                        'en' => 'For the 31st – 90th day upon receipt of fund, there will be an early withdrawal fee of :90_days_penalty% on the withdrawal of this fund capital;',
                        'cn' => '单笔入金的第31至90天内，提取本资金将产生:90_days_penalty%的早撤手续费；',
                    ],
                    'agreement 11' => [
                        'en' => 'For the 91st day onwards upon receipt of fund, there will be no early withdrawal fee on the withdrawal of this fund capital.',
                        'cn' => '单笔入金的第91天开始，提取本资金将不产生任何早撤手续费。',
                    ],
                    'conclusion 1' => [
                        'en' => 'This Power of Attorney shall remain in effect until revoked by the end of trading campaign.',
                        'cn' => '本授权书将持续有效，直至交易活动结束，委托书被撤销。',
                    ],
                    'conclusion 2' => [
                        'en' => 'This Power of Attorney shall remain in effect until revoked by a duly signed written notice by the Client (if such notice is sent by e-mail then to: helpdesk@ecntradefx.com) or when the client withdraws all capital from the Cropean Trade platform.',
                        'cn' => '在客户正式签署书面通知（若该通知通过电子邮件发送至：helpdesk@ecntradefx.com）或客户从克比亚国际交易平台中提取所有资金之前，该委托书将持续有效。',
                    ],
                    'conclusion 3' => [
                        'en' => 'This Power of Agent (and any dispute, controversy, proceedings or claims of whatever nature arising out of or in any way relating to this Power of Agent or its formation or any act performed or claimed to be performed under it) shall be governed by and construed in accordance the Capital Markets & Collective Investment Schemes Supervision Section (CM&CISSS), by the Seychelles Financial Services Authority. Each Party irrevocably agrees that the courts of Seychelles shall have exclusive jurisdiction to determine any proceedings in connection with or arising out of this Power of Attorney.',
                        'cn' => '该委托书（以及任何争议、争辩、诉讼或索赔或任何与此委托书或其形式有关，或根据此委托书所进行或声称进行的任何行为）应受塞舌尔金融服务管理局的资本市场和集体投资计划监察组（CM&CISSS）的管辖并按其进行解释。各方同意塞舌尔法院对与本授权委托书有关或由本授权委托书引起的任何诉讼具有专有管辖权。',
                    ],
                ],
                's_epoa_contract_footer' => [
                    'signed by' => [
                        'en' => 'Signed By',
                        'cn' => '委托人签名',
                    ],
                    'counsel name' => [
                        'en' => 'David Lynch',
                        'cn' => '大卫·林奇',
                    ],
                    'date' => [
                        'en' => 'Date',
                        'cn' => '日期',
                    ],
                    'the principal' => [
                        'en' => 'The Principal',
                        'cn' => ' ',
                    ],
                    'cropean trade inhouse counsel' => [
                        'en' => 'Cropean Trade In-house Counsel',
                        'cn' => '克比亚内部法律顾问',
                    ],
                ],
            ],

        ];
    }
}
