<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserStatusHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_status_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_status_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->text('remarks')->nullable();
            $table->boolean('is_current')->default(0);
            $table->unsignedInteger('updated_by')->index()->nullable();

            $table->foreign('user_status_id')
                ->references('id')
                ->on('user_statuses');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('user_status_histories', function (Blueprint $table) {
            $table->dropForeign(['user_status_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['updated_by']);
        });
        Schema::dropIfExists('user_status_histories');
    }
}
