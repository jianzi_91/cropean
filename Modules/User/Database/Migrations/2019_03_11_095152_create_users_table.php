<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->string('member_id')->unique()->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('secondary_password');
            $table->string('id_number')->unique()->nullable();
            $table->date('birth_date')->nullable();
            $table->string('mobile_prefix')->nullable();
            $table->string('mobile_number')->nullable();
            $table->unsignedInteger('country_id')->nullable()->index();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('user_status_id')->nullable()->index('user_status_id');
            $table->string('session_id')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('remember_token')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->boolean('is_system_generated')->default(0);
            $table->boolean('to_logout')->default(false);
            $table->decimal('special_bonus_percentage', 6, 4)->nullable();
            $table->boolean('view_special_bonus')->default(false);

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('user_status_id')->references('id')->on('user_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropForeign(['user_status_id']);
        });

        Schema::dropIfExists('users');
    }
}
