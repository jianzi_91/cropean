<?php

return [
    'name'     => 'User',
    'bindings' => [
        'Modules\User\Contracts\UserContract'             => 'Modules\User\Repositories\UserRepository',
        'Modules\User\Contracts\UserStatusContract'       => 'Modules\User\Repositories\UserStatusRepository',
        'Modules\User\Contracts\UserSignatureContract'    => 'Modules\User\Repositories\UserSignatureRepository',
        'Modules\User\Contracts\UserEpoaContractContract' => 'Modules\User\Repositories\UserEpoaContractRepository',
    ],
    'root_username' => 'root',
    'test_users'    => 'systemroot',

    /*
     |--------------------------------------------------------------------------
     | Signature setting
     |--------------------------------------------------------------------------
     |
     | Here you can configure the signature for e-contracts
     |
     */
    'signature' => [

        'mimes' => [
            '.gif'  => 'image/gif',
            '.ico'  => 'image/x-icon',
            '.jpg'  => 'image/jpeg',
            '.jpeg' => 'image/jpeg',
            '.png'  => 'image/png',
        ],

        'max_size' => 20000, // in kilo bytes

        /*
         |--------------------------------------------------------------------------
         | Contract storage location
         |--------------------------------------------------------------------------
         |
         | Here you can configure where to store the e-contracts
         |
         */
        'storage' => 'signatures',
    ],

    /*
     |--------------------------------------------------------------------------
     | Contract setting
     |--------------------------------------------------------------------------
     |
     | Here you can configure the e-contracts
     |
     */
    'contract' => [
        'mimes' => [
            '.pdf' => 'application/pdf',
        ],

        'max_size' => 20000, // in kilo bytes

        /*
         |--------------------------------------------------------------------------
         | Contract storage location
         |--------------------------------------------------------------------------
         |
         | Here you can configure where to store the e-contracts
         |
         */
        'storage' => 'contracts',
    ],
];
