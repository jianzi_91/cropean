@extends('templates.admin.master')
@section('title', __('a_page_title.user management'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_admin_management.admin management')],
    ],
    'header'=>__('a_admin_management.admin management')
])

@can('admin_user_admin_management_create')
<div class="pb-2">
{{ Form::formButtonPrimary('btn_new', __('a_admin_management.add admin'), route('admin.management.admins.create'), 'button') }}
</div>
@endcan

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('email', request('email'),__('a_admin_management.email address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
{{ Form::formSelect('role_id', admin_roles_dropdown(), old('role_id', request('role_id')), __('a_admin_management.role'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('status_id', user_statuses_dropdown(), old('status_id', request('status_id')), __('a_admin_management.status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_admin_management.admin list')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_admin_management.total results:') }} {{ $users->total() }}</div>
                @can('admin_user_admin_management_export')
                    {{ Form::formTabSecondary(__('a_admin_management.export excel'), route('admin.management.admins.export',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_admin_management.date') }}</th>
                <th>{{ __('a_admin_management.name') }}</th>
                <th>{{ __('a_admin_management.email address') }}</th>
                <th>{{ __('a_admin_management.role') }}</th>
                <th>{{ __('a_admin_management.status') }}</th>
                <th>{{ __('a_admin_management.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @if(count($users))
            @foreach($users as $user)
            <tr>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role_name }}</td>
                <td>{{ __('s_user_statuses.'. $user->status_name) }}</td>
                <td class="text-capitalize">
                    @can('admin_user_admin_management_edit')
                        <a href="{{ route('admin.management.admins.edit', $user->id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_admin_management.view')}}"><i class="bx bx-show-alt"></i></a>
                    @endcan
                </td>
            </tr>
            @endforeach
        @else
            @include('templates.__fragments.components.no-table-records', [ 'span' => 6, 'text' => __('a_admin_management.no records') ])
        @endif
        </tbody>
        @endcomponent
    </div>
</div>
{!! $users->render() !!}
@endsection


@push('scripts')
<script>
$(function() {
    $('#btn_new').on('click', function(e) {
        e.preventDefault()
        window.location.href = '{{ route("admin.management.admins.create") }}'
    })
})
</script>
@endpush
