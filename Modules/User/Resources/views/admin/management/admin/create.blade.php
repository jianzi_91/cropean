@extends('templates.admin.master')
@section('title', __('a_admin_management_create.create admin'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_admin_management_create.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_admin_management_create.admin management'), 'route' => route('admin.management.admins.index')],
        ['name' => __('a_admin_management_create.create admin')]
    ],
    'header'=>__('a_admin_management_create.create admin'),
    'backRoute'=>route('admin.management.admins.index'),
])

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                <h5 class="card-title mb-3">{{ __("a_admin_management_create.new admin") }}</h5>
                {{ Form::open(['route' => ['admin.management.admins.store'], 'method'=>'post', 'id'=>'adminForm', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formText('name', old('name'), __('a_admin_management_create.name')) }}
                    {{ Form::formText('email', old('email'), __('a_admin_management_create.email address')) }}
                    {{ Form::formPassword('password', old('password'), __('a_admin_management_create.password')) }}
                    {{ Form::formPassword('password_confirmation', old('password_confirmation'), __('a_admin_management_create.confirm password')) }}
                    {{ Form::formPassword('secondary_password', old('secondary_password'), __('a_admin_management_create.secondary password')) }}
                    {{ Form::formPassword('secondary_password_confirmation', old('secondary_password_confirmation'), __('a_admin_management_create.confirm secondary password')) }}
                    {{ Form::formSelect('role_id', admin_roles_dropdown(), old('role_id'), __('a_admin_management_create.role')) }}

                    <div style="width:100%;text-align:right;">
                    {{ Form::formButtonPrimary('btn_submit', __('a_admin_management_create.save')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
