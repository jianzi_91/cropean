@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'breadcrumbs' => [
        ['name' => __('a_admin_management_profile.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_admin_management_profile.admin management'), 'route' => route('admin.management.admins.index')],
        ['name' => __('a_admin_management_profile.profile settings')]
    ],
    'header'=>__('a_admin_management_profile.profile settings'),
    'backRoute'=>route('admin.management.admins.index'),
])

@include('templates.admin.includes._am-nav', ['page' => 'profile', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                {{ Form::open(['route' => ['admin.management.admins.update', $user->id], 'method'=>'put', 'id'=>'profile', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formText('name', old('name') ?: $user->name, __('a_admin_management_profile.name')) }}
                    {{ Form::formText('email', old('email') ?: $user->email, __('a_admin_management_profile.email address')) }}
                    {{ Form::formSelect('role_id', admin_roles_dropdown(), old('role_id') ?: $user->role->id, __('a_admin_management_profile.role')) }}
                    {{ Form::formSelect('user_status_id', admin_statuses_dropdown(), old('user_status_id') ?: $user->status->status->id, __('a_admin_management_profile.status')) }}
                    {{ Form::formButtonPrimary('btn_submit', __('a_admin_management_profile.save')) }}
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

