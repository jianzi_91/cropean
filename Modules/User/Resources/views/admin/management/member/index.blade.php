@extends('templates.admin.master')
@section('title', __('a_page_title.user management'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_member_management.member management')],
    ],
    'header'=>__('a_member_management.member management')
])

@component('templates.__fragments.components.filter')
<div class="col-12 col-lg-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_member_management.date'), [], false) }}
</div>
<div class="col-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_member_management.name'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('member_id', request('member_id'),__('a_member_management.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3 mt-2">
    {{ Form::formCheckbox('member_id_downlines', 'member_id_downlines', '1', __('a_report_balance.include downlines'), ['isChecked' => request('member_id_downlines', false)])}}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('email', request('email'),__('a_member_management.email address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('status_id', user_statuses_dropdown(), old('status_id', request('status_id')), __('a_member_management.status'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formSelect('kyc_status_id', user_management_kyc_status_dropdown(), old('kyc_status_id', request('kyc_status_id')), __('a_member_management.kyc status'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        @can('admin_user_member_management_export')
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_member_management.member list')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_member_management.total results:') }} {{ $users->total() }}</div>
                @can('admin_user_member_management_export')
                    {{ Form::formTabSecondary(__('a_member_management.export excel'), route('admin.management.members.export',http_build_query(request()->except('page')))) }}
                @endcan
            </div>
        </div>
        @endcan
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_member_management.date') }}</th>
                <th>{{ __('a_member_management.full name') }}</th>
                <th>{{ __('a_member_management.member id') }}</th>
                <th>{{ __('a_member_management.email address') }}</th>
                <th>{{ __('a_member_management.status') }}</th>
                <th>{{ __('a_member_management.kyc status') }}</th>
                @can('admin_user_member_management_epoa_view')
                <th>{{ __('a_member_management.epoa contract') }}</th>
                @endcan
                <th>{{ __('a_member_management.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @if(count($users))
            @foreach($users as $user)
            <tr>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->member_id }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ __('s_user_statuses.'. $user->status_name) }}</td>
                <td>{{ __('s_user_document_statuses.' . $user->document_status) }}</td>
                @can('admin_user_member_management_epoa_view')
                <td>
                    @if($user->epoaContract)
                        <a href="{{ route('admin.management.member.epoa', $user->epoaContract->original_filename) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_member_management.view')}}" target="__blank"><i class="bx bx-file"></i></a>
                    @else
                        -
                    @endif
                </td>
                @endcan
                <td class="text-capitalize">
                    @can('admin_user_member_management_edit')
                        <a href="{{ route('admin.management.members.edit', $user->id) }}" data-toggle="tooltip" data-placement="top" title="{{ __('a_member_management.view')}}"><i class="bx bx-show-alt"></i></a>
                    @endcan
                </td>
            </tr>
            @endforeach
        @else
            @include('templates.__fragments.components.no-table-records', [ 'span' => 7, 'text' => __('a_member_management.no records') ])
        @endif
        </tbody>
        @endcomponent
    </div>
</div>
{!! $users->render() !!}
@endsection
