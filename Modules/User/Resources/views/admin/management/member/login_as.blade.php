@extends('templates.admin.master')
@section('title', __('a_member_management_login_as.login as'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_login_as.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_login_as.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_login_as.login as')]
    ],
    'header'=>__('a_member_management_login_as.login as'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'login as', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
                        <li class="nav-item pb-0">
                            {{ Form::formTabPrimary(__('a_member_management_login_as.login as'), route('admin.login_as', $user->id), true) }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
