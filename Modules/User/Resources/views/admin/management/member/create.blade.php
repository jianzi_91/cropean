@extends('templates.member.master')
@section('title', __('a_page_title.user management'))

@section('main')
    <div class="row">
        <!-- column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                <h4 class="card-title">{{ __('a_admin_management_create.create')}}</h4>
                    {{-- <h6 class="card-subtitle">Add class <code>.table</code></h6> --}}
                    {{ Form::open(['route' => ['admin.management.admins.store'], 'method'=>'post', 'id'=>'adminForm', 'class'=>'form-vertical -with-label']) }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="jv-form-inputs form-horizontal m-t-40">
                                    <div class="row">
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formText('username', old('username'), __('a_admin_management_create.username').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formText('name', old('name'), __('a_admin_management_create.name').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formText('email', old('email'), __('a_admin_management_create.email address').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formPassword('password', old('password'), __('a_admin_management_create.password').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formPassword('password_confirmation', old('password_confirmation'), __('a_admin_management_create.confirm password').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formPassword('secondary_password', old('secondary_password'), __('a_admin_management_create.secondary password').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formPassword('secondary_password_confirmation', old('secondary_password_confirmation'), __('a_admin_management_create.confirm secondary password').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formSelect('country_id', countries_dropdown(),old('country_id'), __('a_admin_management_create.country').' <span>*</span>') }}
                                        </div>
                                        <div class="col-12 col-lg-6 col-xl-3 jv-label-color">
                                             {{ Form::formSelect('role_id', admin_roles_dropdown(), old('role_id'), __('a_admin_management_create.role').' <span>*</span>') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn waves-effect waves-light btn-lg btn-primary">{{ __('a_admin_management_create.submit') }}</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
        <script>
        $(function() {

        });
    </script>
@endpush
