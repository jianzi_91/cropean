@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'breadcrumbs' => [
        ['name' => __('a_member_management_profile.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_profile.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_profile.profile settings')]
    ],
    'header'=>__('a_member_management_profile.profile settings'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'profile', 'uid' => $user->id ])
<br />

<div class="row">
    <div class="col-xs-12 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                {{ Form::open(['route' => ['admin.management.members.update', $user->id], 'method'=>'put', 'id'=>'profile', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    {{ Form::formText('member_id', old('member_id') ?: $user->member_id, __('a_member_management_profile.member id'), ['disabled'=> true]) }}
                    {{ Form::formText('name', old('name') ?: $user->name, __('a_member_management_profile.full name')) }}
                    {{ Form::formText('email', old('email') ?: $user->email, __('a_member_management_profile.email address')) }}
                    {{ Form::formText('id_number', old('id_number') ?: $user->id_number, __('a_member_management_profile.national id')) }}
                    {{ Form::formSelect('country_id', countries_dropdown(), old('country_id') ?: $user->country_id, __('a_member_management_profile.country')) }}
                    {{ Form::formDate('birth_date', old('birth_date') ?: $user->birth_date, __('a_member_management_profile.date of birth')) }}
                    {{ Form::formSelect('gender', gender_dropdown(), old('gender', $user->gender), __('a_member_management_profile.gender')) }}
                    {{ Form::formSelect('mobile_prefix', mobile_prefix_dropdowns(), old('mobile_prefix') ?: $user->mobile_prefix, __('m_my_profile.mobile register country')) }}
                    {{ Form::formContactNumber('mobile_number', old('mobile_number') ?: $user->mobile_number, __('m_my_profile.contact number'), '') }}
                    {{ Form::formText('address', old('address') ?: $user->address, __('a_member_management_profile.address')) }}
                    {{ Form::formText('referrer_id', $user->referrer_id, __('a_member_management_profile.referrer id'), ['disabled'=> true]) }}
                    {{ Form::formText('sponsor_id', $user->sponsor_id, __('a_member_management_profile.sponsor id'), ['disabled'=> true]) }}
                    {{ Form::formSelect('user_status_id', user_statuses_dropdown(), old('user_status_id') ?: $user->status->status->id, __('a_member_management_profile.status')) }}
                    <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit', __('a_member_management_profile.save')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    @if (old('mobile_prefix'))
    $('div[name="mobile_number"] .prefix').html('+' + $('#mobile_prefix').val())
    @endif

    $('#mobile_prefix').on('change', function() {
        $('div[name="mobile_number"] .prefix').html('+' + $(this).val())
    })
})
</script>
@endpush

