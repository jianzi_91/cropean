@extends('templates.admin.master')
@section('title', __('a_page_title.my profile'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

{{--@section('body-class')
no-card-shadow
@endsection--}}

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
        ['name'=>__('a_my_profile.my profile')],
    ],
])
<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.admin.includes.profile_nav', ['page' => 'profile'])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    {{ Form::open(['route' => ['admin.profile.update', $user->id], 'method'=>'put', 'id'=>'profile', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        {{ Form::formText('name', old('name') ?: $user->name, __('a_my_profile.name')) }}
                        {{ Form::formText('email', old('email') ?: $user->email, __('a_my_profile.email address')) }}
                        <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit', __('a_my_profile.save')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
