@extends('templates.member.master')
@section('title', __('m_page_title.convert credits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_epoa.epoa contract')],
    ],
    'header'=>__('m_epoa.epoa form'),
    'backRoute'=>route('member.deposits.index')
])

<div class="row">
    <div class="col-xs-12 col-lg-8">
        <div class="epoa-subheader pb-1">{{ __('m_epoa.please read through the contract below') }}</div>
        <div class="card">
            <div class="card-content">
                <div class='card-body'>
                    <div class="review">
                        <div class="contract cont">
                            <div class="sub">
                                <div class="section-header">{{ __('s_epoa_contract_header.title') }}</div>
                                <br/>
                                <div class="info">{{ __('s_epoa_contract_header.name') }}: {{ $user->name }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.email') }}: {{ $user->email }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.cropean member id') }}: {{ $user->member_id }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.country') }}: {{ $user->country->name }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.personal id number') }}: {{ $user->id_number }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.address') }}: {{ $user->address }}</div>
                                <br/>
                                <div class="section-header-italic">{{ __('s_epoa_contract_header.hereinafter referred to as the principal') }}</div>
                                <br/>
                                <div class="section-header">{{ __('s_epoa_contract_header.the princical hereby grants POA to') }}: - </div>
                                <br/>
                                <div class="info">{{ __('s_epoa_contract_header.entity name') }}: {{ __('s_epoa_contract_header.entity name value') }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.entity country') }}: {{ __('s_epoa_contract_header.entity country value') }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.entity address') }}: {{ __('s_epoa_contract_header.entity address value') }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.jurisdiction') }}: {{ __('s_epoa_contract_header.jurisdiction value') }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.incorporation number') }}: {{ __('s_epoa_contract_header.incorporation number value') }}</div>
                                <div class="info">{{ __('s_epoa_contract_header.governance') }}: {{ __('s_epoa_contract_header.governance value') }}</div>
                                <br />
                                <div class="section-header-italic">{{ __('s_epoa_contract_header.hereinafter referred to as the agent') }}</div>
                                <br />
                                <div class="section-item">{{ __('s_epoa_contract_content.intro') }}</div>
                                <br/>
                                <div class="section-header">{{ __('s_epoa_contract_content.thus the principal understands and agree that') }}:</div>
                                <div class="section-item">
                                    <div class="number">1.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 1') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">2.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 2') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">3.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 3') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">4.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 4') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">5.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 5') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">6.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 6') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">7.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 7') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">8.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 8') }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">9.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 9', ['30_days_penalty' => bcmul($penaltyPercentage['30'], 100, 2)]) }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">10.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 10', ['90_days_penalty' => bcmul($penaltyPercentage['90'], 100, 2)]) }}</div>
                                </div>
                                <div class="section-item">
                                    <div class="number">11.</div>
                                    <div class="content">{{ __('s_epoa_contract_content.agreement 11') }}</div>
                                </div>
                                <br/>
                                <div class="section-item">{{ __('s_epoa_contract_content.conclusion 1') }}</div>
                                <div class="section-item">{{ __('s_epoa_contract_content.conclusion 2') }}</div>
                                <div class="section-item">{{ __('s_epoa_contract_content.conclusion 3') }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::open(['route' => 'member.epoa.store', 'method'=>'post', 'id' => 'create-epoa', 'enctype'=>'multipart/form-data']) }}

<div class="row">
    <div class="col-xs-12 col-lg-8">
        <div class="epoa-subheader pb-1">{{ __('m_epoa.please sign below') }}</div>
        <div class="card">
            <div class="card-content">
                <div class='card-body'>
                    <div class="row">
                        <div class="col-5">
                            <div class="signature-cont d-flex flex-row">
                                <canvas class="signature" style="width:300px"></canvas>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <div class="signature-clear" style="padding-top:5px;">
                                    <a href="javascript:void(0);" class="js-sig-clear"><i class="bx bx-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="signature-error" class="invalid-feedback"></div>
                    <div class="row pt-1">
                        <div class="col-lg-5 col-xs-12 d-flex justify-content-end pr-2">
                            {{ Form::formTabSecondary(__('m_epoa.cancel'), route('member.deposits.index')) }}
                            &nbsp;&nbsp;&nbsp;
                            <button id="btn_next" type="button" class="btn btn-primary cro-btn-primary">{{ __('m_epoa.submit') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    var sourceCreditArr = []
    var destCreditArr = []

    var canvas = document.querySelector('.signature-cont .signature')
    var signaturePad = new SignaturePad(canvas)

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }

    $('#btn_next').on('click', function(e) {
        e.preventDefault()
        if ($('#btn_next').is(':disabled')) return
        $('#btn_next').attr('disabled', true)
        if (signaturePad.isEmpty()) {
            $('#signature-error').html('{{ __("m_epoa.signature is required") }}')
            $('#signature-error').show()
            $('#btn_next').attr('disabled', false)
            return
        }
        var base64 = signaturePad.toDataURL()
        var form = document.getElementById('create-epoa')
        var block = base64.split(";")
        var contentType = block[0].split(":")[1]
        var realData = block[1].split(",")[1]

        var blob = b64toBlob(realData, contentType);
        var formDataToUpload = new FormData(form);
        formDataToUpload.append('signature', blob);

        $('.invalid-feedback').html('')
        $('.invalid-feedback').hide()

        axios.post('{{ route("member.epoa.store") }}', formDataToUpload)
          .then(function(data) {
            window.location.href = '{{ route("member.epoa.index") }}'    
          })
          .catch(function(err) {
              var data = err.response.data
              var message = data.message
              var errors = data.errors

              var keys = Object.keys(errors)
              _.forEach(keys, function(k) {
                  $('[data-id="' + k + '"] .invalid-feedback').html(errors[k])
                  $('[data-id="' + k + '"] .invalid-feedback').show()
              })

              CreateNoty({
                'text': message,
                'type': 'error'
              });

              $('#btn_next').attr('disabled', false)
          })
    })

    $('.js-sig-clear').on('click', function(e) {
        e.preventDefault()
        signaturePad.clear()
    })
})
</script>
@endpush