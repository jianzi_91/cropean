@extends('templates.member.master')
@section('title', __('m_page_title.my profile'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

{{--@section('body-class')
no-card-shadow
@endsection--}}

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_my_profile.my epoa')],
    ],
])
<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'epoa'])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title">{{ __('m_epoa.power of attorney contract') }}</h4>
                        @if (auth()->user()->epoaContract)
                        <div class="row">
                            <div class="d-flex flex-row align-items-center col-11 epoa-file">
                                <i class="bx bx-file"></i>
                                &nbsp;&nbsp;&nbsp;
                                {{ auth()->user()->epoaContract->original_filename }}
                            </div>
                            <div class="col-1">
                                <a href="{{ route('member.epoa.show', ['epoa' => auth()->user()->epoaContract->original_filename]) }}" target="__blank"><i class="bx bx-show-alt"></i></a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$(function() {
    @if (old('mobile_prefix'))
    $('div[name="mobile_number"] .prefix').html('+' + $('#mobile_prefix').val())
    @endif

    $('#mobile_prefix').on('change', function() {
        $('div[name="mobile_number"] .prefix').html('+' + $(this).val())
    })
})
</script>
@endpush
