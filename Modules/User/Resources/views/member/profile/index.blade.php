@extends('templates.member.master')
@section('title', __('m_page_title.my profile'))

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/page-user-profile.css') }}">
@endpush

{{--@section('body-class')
no-card-shadow
@endsection--}}

@section('main')
@include('templates.__fragments.components.breadcrumbs', [
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumbs.home'), 'route'=>route('member.dashboard')],
        ['name'=>__('m_my_profile.my profile')],
    ],
])
<section class="page-user-profile">
    <div class="row">
        <div class="col-12">
            <!-- user profile heading section start -->
            @include('templates.member.includes.profile_nav', ['page' => 'profile'])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    {{ Form::open(['route' => ['member.profile.update', $user->id], 'method'=>'put', 'id'=>'profile', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                        {{ Form::formText('member_id', $user->member_id, __('m_my_profile.member id'), ['readonly']) }}
                        {{ Form::formText('name', old('name') ?: $user->name, __('m_my_profile.name'), ['readonly']) }}
                        {{ Form::formText('email', old('email') ?: $user->email, __('m_my_profile.email address'), ['readonly']) }}
                        {{ Form::formText('id_number', old('id_number') ?: $user->id_number, __('m_my_profile.national id'), ['readonly']) }}
                        {{ Form::formSelect('country_id', countries_dropdown(), old('country_id') ?: $user->country_id, __('m_my_profile.country')) }}
                        {{ Form::formDate('birth_date', old('birth_date') ?: $user->birth_date, __('m_my_profile.date of birth')) }}
                        {{ Form::formSelect('gender', gender_dropdown(), old('gender', $user->gender), __('m_my_profile.gender')) }}
                        {{ Form::formSelect('mobile_prefix', mobile_prefix_dropdowns(), old('mobile_prefix') ?: $user->mobile_prefix, __('m_my_profile.mobile register country')) }}
                        {{ Form::formContactNumber('mobile_number', old('mobile_number') ?: $user->mobile_number, __('m_my_profile.contact number')) }}
                        {{ Form::formText('address', old('address') ?: $user->address, __('m_my_profile.address')) }}
                        {{ Form::formText('referrer_id', $user->referrer_id, __('m_my_profile.referrer id'), ['disabled'=> true]) }}
                        {{ Form::formText('sponsor_id', $user->sponsor_id, __('m_my_profile.sponsor id'), ['disabled'=> true]) }}
                        <div style="width:100%;text-align:right;">
                        {{ Form::formButtonPrimary('btn_submit', __('m_my_profile.save')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$(function() {
    @if (old('mobile_prefix'))
    $('div[name="mobile_number"] .prefix').html('+' + $('#mobile_prefix').val())
    @endif

    $('#mobile_prefix').on('change', function() {
        $('div[name="mobile_number"] .prefix').html('+' + $(this).val())
    })
})
</script>
@endpush
