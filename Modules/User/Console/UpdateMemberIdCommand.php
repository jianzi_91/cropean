<?php

namespace Modules\User\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Models\GoldmineCommission;
use Modules\Commission\Queries\GoldmineSponsorQuery;
use Modules\Commission\Repositories\GoldmineCommissionRepository;
use Modules\Product\Models\ProductPurchaseDetail;
use Modules\Product\Models\ProductType;
use Modules\Tree\Models\SponsorTreeSnapshot;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;
use Symfony\Component\Console\Input\InputArgument;

class UpdateMemberIdCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:update_member_id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update member id from users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var UserRepository $userContract */
        $userContract = resolve(UserContract::class);

        $users = User::where('is_system_generated',1)->whereNotIn('name',['root','systemroot'])->get();
        foreach($users AS $user)
        {
            if($user->is_member)
            {
                $user->member_id = $userContract->generateMemberId(true);
                $user->email = $user->member_id.'@test.com';
                $user->name = $user->member_id;
                $user->username = $user->member_id;
                $user->save();
            }
        }

        $users = User::where('is_system_generated',0)->whereNotIn('name',['root','systemroot'])->get();
        foreach($users AS $user)
        {
            if($user->is_member)
            {
                $user->member_id = $userContract->generateMemberId(false);
                $user->save();
            }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['date', InputArgument::OPTIONAL, 'The date when to run the rebate.']
        ];
    }
}
