<?php

namespace Modules\User\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Commission\Models\UserSpecialBonusPercentageHistory;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Rank\Models\GoldmineRank;
use Modules\Rank\Models\UserGoldmineRankHistory;
use Modules\Rank\Models\UserRankHistory;
use Modules\Rbac\Contracts\RoleContract;
use Modules\Tree\Models\SponsorTree;
use Modules\User\Contracts\UserContract;

class ImportDummyUserCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'import:users {file}';

    /**
     * Command body.445
     * @param UserContract $userRepo
     * @param RoleContract $roleRepo
     * @param BankAccountCreditContract $bankAccountCreditContract
     * @throws \Exception
     */
    public function handle(
        UserContract $userRepo,
        RoleContract $roleRepo,
        BankAccountCreditContract $bankAccountCreditContract
    ) {
        if (app()->environment() == 'production') {
            $this->line('This command unable to process in production environment');
            exit;
        }

        $file = fopen(storage_path('test') . '/' . $this->argument('file'), 'r');

        $users             = [];
        $firstLine         = true;
        $userRanks         = [];
        $userGoldmines     = [];
        $userGoldmineRanks = [];
        $userSpecialBonus  = [];

        $goldmineRanks = GoldmineRank::pluck('id')->toArray();

        while (!feof($file)) {
            $data = fgetcsv($file);

            if ($firstLine) {
                $firstLine = false;
            } else {
                $users[] = (object) [
                    'username'        => 'commstest' . $data[0],
                    'sponsor'         => (empty($data[1])) ? null : $data[1],
                    'referrer'        => (empty($data[2])) ? null : $data[2],
                    'capital_credit'  => empty($data[3]) ? 0 :$data[3],
                    'rollover_credit' => empty($data[4]) ? 0 :$data[4],
                    'cash_credit'     => empty($data[5]) ? 0 :$data[5],
                    'usdt_erc20'      => empty($data[6]) ? 0 :$data[6],
                    'usdc'            => empty($data[7]) ? 0 :$data[7],
                    'rank_id'         => empty($data[8]) ? null :$data[8],
                    'goldmine_level'  => empty($data[9]) ? null :$data[9],
                    'special_bonus'   => empty($data[10]) ? null :$data[10],
                    'goldmine_rank'   => empty($data[11]) ? 0 :$data[11],
                ];
            }
        }

        fclose($file);

        $role = $roleRepo->findBySlug($roleRepo->getModel()::MEMBER);

        DB::beginTransaction();

        try {
            foreach ($users as &$user) {
                $newUser = $userRepo->add([
                    'name'                     => $user->username,
                    'username'                 => $user->username,
                    'nickname'                 => $user->username,
                    'email'                    => $user->username . '@test.com',
                    'member_id'                => $user->username,
                    'password'                 => bcrypt('Qwer1234'),
                    'secondary_password'       => bcrypt('Qwer1234'),
                    'created_at'               => now()->toDateTimeString(),
                    'updated_at'               => now()->toDateTimeString(),
                    'country_id'               => 1,
                    'birth_date'               => now()->subYears(18),
                    'rank_id'                  => $user->rank_id,
                    'goldmine_level'           => $user->goldmine_level,
                    'goldmine_rank_id'         => in_array($user->goldmine_rank, $goldmineRanks) ? $user->goldmine_rank : null,
                    'special_bonus_percentage' => $user->special_bonus,
                ]);

                $newUser->assign($role);
                $user->id = $newUser->id;

                if (isset($user->special_bonus) && $user->special_bonus > 0) {
                    $userSpecialBonus[] = [
                        'created_at' => now()->toDateTimeString(),
                        'updated_at' => now()->toDateTimeString(),
                        'user_id'    => $user->id,
                        'percentage' => $user->special_bonus,
                        'is_current' => 1,
                        'updated_by' => null
                    ];
                }

                // Insert into sponsor tree
                $sponsorId = (empty($user->sponsor)) ? null : $user->sponsor;
                $parentId  = SponsorTree::where('user_id', $sponsorId)->first();

                if (empty($parentId)) {
                    DB::rollback();
                    $this->line("Sponsor does not exist in tree: $sponsorId");
                    return false;
                }

                $sponsorTree = [
                    'created_at' => Carbon::now()->startOfDay()->format('Y-m-d H:i:s'),
                    'user_id'    => $user->id,
                    'parent_id'  => $parentId->id,
                ];

                SponsorTree::insert($sponsorTree);

                $sponsor = $userRepo->getModel()->find($sponsorId);
                $newUser->update([
                    'referrer_id' => $sponsor->member_id
                ]);

                $this->line("inserted user id {$user->id} in sponsor tree");

                if (!empty($sponsorId)) {
                    if (isset($user->capital_credit) && $user->capital_credit > 0) {
                        $bankAccountCreditContract->add(BankCreditTypeContract::CAPITAL_CREDIT, $user->capital_credit, $user->id, BankTransactionTypeContract::DEPOSIT, null, $user->id);
                    }
                    if (isset($user->rollover_credit) && $user->rollover_credit > 0) {
                        $bankAccountCreditContract->add(BankCreditTypeContract::ROLL_OVER_CREDIT, $user->rollover_credit, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                    }
                    if (isset($user->cash_credit) && $user->cash_credit > 0) {
                        $bankAccountCreditContract->add(BankCreditTypeContract::CASH_CREDIT, $user->cash_credit, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                    }
                    if (isset($user->usdt_erc20) && $user->usdt_erc20 > 0) {
                        $bankAccountCreditContract->add(BankCreditTypeContract::USDT_ERC20, $user->usdt_erc20, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                    }
                    if (isset($user->usdc) && $user->usdc > 0) {
                        $bankAccountCreditContract->add(BankCreditTypeContract::USDC, $user->usdc, $user->id, BankTransactionTypeContract::CREDIT_ADJUSTMENT, null, $user->id);
                    }
                }

                #insert user rank history
                if (isset($user->rank_id) && $user->rank_id > 0) {
                    $userRanks[] = [
                        'created_at'        => now()->toDateTimeString(),
                        'updated_at'        => now()->toDateTimeString(),
                        'user_id'           => $user->id,
                        'rank_id'           => $user->rank_id,
                        'qualified_rank_id' => null,
                        'is_current'        => 1,
                        'is_manual'         => 1,
                    ];
                }

                if (isset($user->goldmine_level) && $user->goldmine_level > 0) {
                    $userGoldmines[] = [
                        'created_at'      => now()->toDateTimeString(),
                        'updated_at'      => now()->toDateTimeString(),
                        'user_id'         => $user->id,
                        'level'           => $user->goldmine_level,
                        'qualified_level' => null,
                        'is_current'      => 1,
                        'is_locked'       => 0,
                        'is_manual'       => 1,
                    ];
                }

                if (isset($user->goldmine_rank) && $user->goldmine_rank > 0 && isset($goldmineRanks[$user->goldmine_rank])) {
                    $userGoldmineRanks[] = [
                        'created_at'                 => now()->toDateTimeString(),
                        'updated_at'                 => now()->toDateTimeString(),
                        'user_id'                    => $user->id,
                        'goldmine_rank_id'           => in_array($user->goldmine_rank, $goldmineRanks) ? $user->goldmine_rank : null,
                        'qualified_goldmine_rank_id' => null,
                        'is_current'                 => 1,
                        'is_locked'                  => 0,
                        'is_manual'                  => 1,
                    ];
                }
            }

            DB::statement('DELETE FROM sponsor_tree_snapshot_history');

            $this->line('Rebuilding sponsor tree...');
            SponsorTree::rebuild();

            $this->line('Rebuild done');

            $this->line('Insert user special bonus history');
            foreach (array_chunk($userSpecialBonus, 1000) as $userbs) {
                UserSpecialBonusPercentageHistory::insert($userbs);
            }

            $this->line('Insert user rank history');
            foreach (array_chunk($userRanks, 1000) as $userRank) {
                UserRankHistory::insert($userRank);
            }

            $this->line('Insert user goldmine history');
            foreach (array_chunk($userGoldmines, 1000) as $userGoldmine) {
                UserGoldmineLevelHistory::insert($userGoldmine);
            }

            $this->line('Insert user goldmine history');
            foreach (array_chunk($userGoldmineRanks, 1000) as $userGoldmineRank) {
                UserGoldmineRankHistory::insert($userGoldmineRank);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
