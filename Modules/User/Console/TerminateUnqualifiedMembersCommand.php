<?php

namespace Modules\User\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Core\Traits\LogCronOutput;
use Modules\Rbac\Models\Role;
use Modules\Rbac\Repositories\RoleRepository;
use Modules\User\Models\User;
use Modules\User\Queries\Member\UserQuery;
use Modules\User\Repositories\UserStatusRepository;

class TerminateUnqualifiedMembersCommand extends Command
{
    use LogCronOutput;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'user:terminate {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Terminate member without any rank within specified days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        UserQuery $userQuery,
        UserStatusRepository $userStatusRepo,
        RoleRepository $roleRepo
    ) {
        $this->userQuery      = $userQuery;
        $this->userStatusRepo = $userStatusRepo;
        $this->roleRepo       = $roleRepo;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date     = $this->argument('date') ? Carbon::parse($this->argument('date')) : now();
        $period   = setting('member_qualification_period');
        $dateTo   = $date->format('Y-m-d');
        $dateFrom = $date->subDays($period)->format('Y-m-d');

        $users            = User::members()->active()->get();
        $terminatedStatus = $this->userStatusRepo->findBySlug($this->userStatusRepo::TERMINATED);

        DB::beginTransaction();

        try {
            foreach ($users as $user) {
                $goldmineRankCount = $user->goldmineRankHistorySnapshots->whereBetween('run_date', [$dateFrom, $dateTo])->count();
                $leaderRankCount   = $user->leaderBonusRankHistorySnapshots->whereBetween('run_date', [$dateFrom, $dateTo])->count();

                // Neglect the termination if the member is new
                $isNewUser = $this->isNewUser($user->id, $dateTo);
                if ($isNewUser) {
                    continue;
                }

                if ($goldmineRankCount == 0 && $leaderRankCount == 0) {
                    $this->userStatusRepo->changeHistory($user, $terminatedStatus);

                    $activeRole     = $this->roleRepo->findBySlug(Role::MEMBER);
                    $terminatedRole = $this->roleRepo->findBySlug(Role::MEMBER_TERMINATED);
                    $user->retract($activeRole);
                    $user->assign($terminatedRole);

                    $this->logUserTermination("Member " . $user->member_id . ' is terminated');
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            $this->logUserTermination($e);
            DB::rollback();
        }
    }

    protected function isNewUser($userId, $date)
    {
        $user                 = User::find($userId);
        $userRegistrationDate = Carbon::parse($user->created_at)->subDay();
        $date                 = Carbon::parse($date);
        $dateDiff             = $date->diffInDays($userRegistrationDate);

        return $dateDiff < setting('member_qualification_period');
    }
}
