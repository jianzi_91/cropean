<?php

namespace Modules\User\Queries\Member;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Rbac\Models\Role;
use Modules\User\Queries\UserQuery as BaseQuery;

class UserQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize, FromCollection
{
    use Exportable;
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'users',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'status_id' => [
            'filter'    => 'select',
            'table'     => 'user_statuses',
            'column'    => 'id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'kyc_status_id' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'document_verification_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->whereIn('roles.name', Role::members()->get()->pluck('name'))
            ->where('users.is_system_generated', 0);
    }

    public function map($user): array
    {
        return [
            $user->created_at,
            $user->name,
            $user->member_id,
            $user->email,
            __('s_user_statuses.' . $user->status_name),
            __('s_user_document_statuses.' . $user->document_status),
        ];
    }

    public function headings(): array
    {
        return [
            __('a_member_management.date'),
            __('a_member_management.full name'),
            __('a_member_management.member id'),
            __('a_member_management.email address'),
            __('a_member_management.status'),
            __('a_member_management.kyc status'),
        ];
    }

    public function collection()
    {
        return $this->get();
    }
}
