<?php

namespace Modules\User\Queries;

use Modules\User\Models\User;
use QueryBuilder\QueryBuilder;

class UserQuery extends QueryBuilder
{
    protected $locale = 'cn';

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $languageId = get_translator_language($this->locale)->id;

        #Todo: Update once modules are added
        $query = User::join('user_status_histories', function ($join) {
            $join->on('user_status_histories.user_id', '=', 'users.id')
                ->where('user_status_histories.is_current', 1)
                ->whereNull('user_status_histories.deleted_at');
        })
        ->join('user_statuses', 'user_statuses.id', '=', 'user_status_histories.user_status_id')
        ->join('assigned_roles', function ($join) {
            $join->on('assigned_roles.entity_id', '=', 'users.id')
                ->where('assigned_roles.entity_type', 'Modules\User\Models\User');
        })
        ->join('roles', function ($join) {
            $join->on('roles.id', '=', 'assigned_roles.role_id');
        })
        ->join('role_i18ns', function ($join) use ($languageId) {
            $join->on('role_i18ns.role_id', '=', 'roles.id')
                ->where('translator_language_id', $languageId)
                ->whereNull('role_i18ns.deleted_at');
        })
        ->leftjoin('user_document_statuses', function ($join) {
            $join->on('user_document_statuses.id', '=', 'users.document_verification_status_id');
        })
        ->orderBy('users.created_at', 'desc')
        ->select(
            [
                'users.*',
                'role_i18ns.name as role_name',
                'user_statuses.name_translation as status_name',
                'user_document_statuses.name_translation as document_status'
            ]
        );

        return $query;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->builder = $this->query();
        return $this;
    }
}
