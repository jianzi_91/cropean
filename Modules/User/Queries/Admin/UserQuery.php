<?php

namespace Modules\User\Queries\Admin;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Rbac\Models\Role;
use Modules\User\Queries\UserQuery as BaseQuery;

class UserQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize, FromCollection
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'email' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'email'
        ],
        'role_id' => [
            'filter' => 'select',
            'table'  => 'roles',
            'column' => 'id'
        ],
        'status_id' => [
            'filter' => 'select',
            'table'  => 'user_statuses',
            'column' => 'id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder
            ->where(function ($query) {
                $query->whereIn('roles.name', Role::admins(true)->get()->pluck('name'))
                    ->orWhere('users.id', Auth::id());
            });
    }

    public function map($user): array
    {
        return [
            $user->created_at,
            $user->name,
            $user->email,
            $user->role_name,
            __('s_user_statuses.' . $user->status_name),
        ];
    }

    public function headings(): array
    {
        return [
            __('a_admin_management.date'),
            __('a_admin_management.name'),
            __('a_admin_management.email address'),
            __('a_admin_management.role'),
            __('a_admin_management.status')
        ];
    }

    public function collection()
    {
        return $this->get();
    }
}
