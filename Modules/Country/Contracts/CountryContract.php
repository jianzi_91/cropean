<?php

namespace Modules\Country\Contracts;

use Plus65\Base\Repositories\Contracts\ActiveContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface CountryContract extends CrudContract, SlugContract, ActiveContract
{
}
