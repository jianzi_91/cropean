<?php

return [
    'name'     => 'Country',
    'bindings' => [
        Modules\Country\Contracts\CountryContract::class => Modules\Country\Repositories\CountryRepository::class
    ],
];
