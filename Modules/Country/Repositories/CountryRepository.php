<?php

namespace Modules\Country\Repositories;

use Illuminate\Foundation\Application;
use Plus65\Base\Repositories\Concerns\HasActive;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;
use Modules\Country\Contracts\CountryContract;
use Modules\Country\Models\Country;

class CountryRepository extends Repository implements CountryContract
{
    use HasCrud, HasSlug, HasActive;

    /**
     * Class constructor
     *
     * @param Application $app
     * @param Country $model
     */
    public function __construct(Country $model)
    {
        parent::__construct($model);
    }
}
