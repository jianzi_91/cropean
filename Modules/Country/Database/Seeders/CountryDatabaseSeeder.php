<?php

namespace Modules\Country\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Country\Models\Country;
use Modules\Translation\Contracts\TranslatorTranslationContract;
use Modules\Translation\Models\TranslatorLanguage;
use Modules\Translation\Models\TranslatorPage;

class CountryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        $translatorTranslationRepo = resolve(TranslatorTranslationContract::class);

        $handle = fopen(__DIR__ . '/../../Data/countries.csv', "r");
        $header = true;

        $language = TranslatorLanguage::where('code', 'cn')->first();

        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
            } else {
                $name                   = $csvLine[0];
                $country                = new Country();
                $country->name          = $name;
                $country->code          = $csvLine[1];
                $country->dialling_code = $csvLine[2];
                //Block countries
                if (in_array($name, $this->getBlockedCountries())) {
                    $country->is_active = 0;
                }
                $country->save();

                //insert into translations table if cn exists in translation
                $page = TranslatorPage::where('name', 's_countries')->first();
                if ($page && $language) {
                    $data = [
                        'language_id'        => $language->id,
                        'translator_page_id' => $page->id,
                        'key'                => $csvLine[0],
                        'value'              => $csvLine[3]
                    ];
                    $translatorTranslationRepo->updateTranslation($data);
                }
            }
        }

        DB::commit();
    }

    private function getBlockedCountries()
    {
        return [
        ];
    }
}
