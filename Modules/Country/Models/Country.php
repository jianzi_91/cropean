<?php

namespace Modules\Country\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;
use Plus65\Base\Models\Scopes\HasActiveScope;
use Modules\Translation\Traits\Translatable;
use Spatie\Activitylog\Traits\LogsActivity;

class Country extends Model
{
    use SoftDeletes, Translatable, HasActiveScope, HasDropDown, LogsActivity;

    protected $fillable = [];

    protected $translatableAttributes = ['name'];

    public function country_exchange_rate()
    {
        return $this->hasOne(CountryExchangeRate::class, 'country_id');
    }
}
