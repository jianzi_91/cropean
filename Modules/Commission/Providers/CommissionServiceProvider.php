<?php

namespace Modules\Commission\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Commission\Console\GoldmineCommissionCommand;
use Modules\Commission\Console\LeaderBonusCommissionCommand;
use Modules\Commission\Console\LeaderBonusPayoutCommand;
use Modules\Commission\Console\SpecialBonusCommissionCommand;
use Modules\Commission\Console\SpecialBonusPayoutCommand;
use Modules\Commission\Console\UserSpecialBonusPercentageSnapshotCommand;

class CommissionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer    = false;
    protected $commands = [
        GoldmineCommissionCommand::class,
        UserSpecialBonusPercentageSnapshotCommand::class,
        SpecialBonusCommissionCommand::class,
        SpecialBonusPayoutCommand::class,
        LeaderBonusCommissionCommand::class,
        LeaderBonusPayoutCommand::class,
    ];

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->registerBindings();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/commission');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/commission';
        }, \Config::get('view.paths')), [$sourcePath]), 'commission');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/commission');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'commission');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'commission');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /**
     * bind interfaces to implementations
     * @return void
     */
    public function registerBindings()
    {
        foreach (config('commission.bindings') as $key => $binding) {
            $this->app->bind($key, $binding);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('commission.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php',
            'commission'
        );
    }

    protected function registerCommands()
    {
        $this->commands($this->commands);
    }
}
