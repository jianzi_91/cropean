<?php

namespace Modules\Commission\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Commission\Repositories\LeaderBonusCreditPayoutRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;

class LeaderBonusPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'payout:leader_bonus {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Release and Payout Credit for Leader Bonus Commission.';

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $payoutDate = new Carbon($this->argument('date'));

        $startTime = $this->start();
        $this->line("Leader bonus credit issue started at " . $startTime->toDateTimeString());

        $this->line('[Step 1] Get start date and end date');
        $startDate = $payoutDate->copy()->subMonth()->startOfMonth()->addDay()->toDateTimeString();
        $endDate   = $payoutDate->copy()->subMonth()->endOfMonth()->addDay()->toDateTimeString();

        $this->line('[Step 2] Get leader bonus payouts');
        $payouts = $this->getLeaderBonusPayouts($startDate, $endDate);
        $this->line('[Step 3] Get leader bonus payouts same rank');
        $sameRankPayouts = $this->getLeaderBonusPayouts($startDate, $endDate, true);

        if ($payouts->count() <= 0 &&  $sameRankPayouts->count() <= 0) {
            $this->error('Leader bonus same rank or normal payout not found');
            exit;
        }

        $this->line("[Step 3] Get payout settings");
        $payoutPercentage = [
            BankCreditTypeContract::CASH_CREDIT => setting('leader_bonus_payout_cash_credit_ratio', 1),
        ];

        $payoutCreditRates = ExchangeRate::where('currency', BankCreditTypeContract::CASH_CREDIT)
            ->pluck('buy_rate', 'currency')
            ->toArray();

        if (!$payoutCreditRates) {
            $this->error('No credit type rate');
            exit;
        }

        $this->line('[Step 4] Update special bonus payout credit issued');
        $payoutDate = $payoutDate->toDateString();

        $this->line('[Step 5] Get all special bonus payout id');
        $leaderBonusIds = [];
        foreach ($payouts as $payout) {
            $leaderBonusIds = array_merge($leaderBonusIds, explode(',', $payout->leader_bonus_ids));
        }

        foreach ($sameRankPayouts as $sameRankPayout) {
            $leaderBonusIds = array_merge($leaderBonusIds, explode(',', $sameRankPayout->leader_bonus_ids));
        }

        DB::beginTransaction();

        try {
            if ($payouts->count() > 0) {
                $this->line('[Step 6] For leader bonus');
                $leaderBonusPayoutRepo = new LeaderBonusCreditPayoutRepository($payouts, $payoutDate, $payoutPercentage, $payoutCreditRates, BankTransactionTypeContract::LEADER_BONUS);
                $this->line('[Step 7] Payout leader bonus credit');
                $leaderBonusPayoutRepo->payout();
            }

            if ($sameRankPayouts->count() > 0) {
                $this->line('[Step 8] For leader bonus same rank');
                $leaderBonusSameRankPayoutRepo = new LeaderBonusCreditPayoutRepository($sameRankPayouts, $payoutDate, $payoutPercentage, $payoutCreditRates, BankTransactionTypeContract::LEADER_BONUS_SAME_RANK);
                $this->line('[Step 9] Payout leader bonus same rank credit');
                $leaderBonusSameRankPayoutRepo->payout();
            }

            $this->line('[Step 10] Update payout date leader bonus payout');
            foreach (array_chunk($leaderBonusIds, 1000) as $leaderBonusId) {
                LeaderBonusPayout::whereIn('id', $leaderBonusId)
                    ->update([
                        'is_credit_issued' => 1,
                        'payout_date'      => $payoutDate
                    ]);
            }
            $this->line("Leader Bonus Credit Issued Done");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }

    private function getLeaderBonusPayouts($startDate, $endDate, $isSameRank = false)
    {
        return LeaderBonusPayout::whereBetween('created_at', [$startDate, $endDate])
            ->where('is_credit_issued', 0)
            ->whereNull('payout_date')
            ->where('is_same_rank', $isSameRank)
            ->selectRaw('user_id, sum(amount) as amounts, group_concat(id) as leader_bonus_ids')
            ->groupBy('user_id')
            ->get();
    }
}
