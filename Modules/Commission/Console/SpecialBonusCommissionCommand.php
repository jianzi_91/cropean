<?php

namespace Modules\Commission\Console;

use Artisan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Contracts\SpecialBonusPayoutContract;
use Modules\Commission\Repositories\SpecialBonusRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Models\BankAccountStatement;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Modules\Tree\Models\SponsorTreeSnapshot;
use Modules\User\Models\User;
use Plus65\Utility\Contracts\SnapshotContract;

class SpecialBonusCommissionCommand extends Command
{
    /**
     * The name and signature of the console command .
     *
     * @var string
     */
    protected $signature = 'commissions:special_bonus {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Special bonus command payout run every 1st of the month';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     *
     * @param SponsorTreeContract $sponsor
     * @param SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
    ) {
        parent::__construct();

        $this->sponsorTree         = $sponsor;
        $this->snapshotHistoryRepo = $snapshotHistoryContract;
    }

    /**
     * Execute the console command.
     *
     * @param SpecialBonusPayoutContract $payoutRepo
     * @param ExchangeRateContract $exchangeRateRepo
     * @return mixed
     * @throws \Exception
     */
    public function handle(
        SpecialBonusPayoutContract $payoutRepo,
        ExchangeRateContract $exchangeRateRepo
    ) {
        $payoutDate = new Carbon($this->argument('date'));

        ini_set('memory_limit', -1);
        set_time_limit(0);

        $dateFrom = $payoutDate->copy()->startOfDay()->toDateTimeString();
        $dateTo   = $payoutDate->copy()->endOfDay()->toDateTimeString();

        if ($payoutRepo->getModel()->whereBetween('created_at', [$dateFrom, $dateTo])->exists()) {
            $this->error("Special bonus already processed for {$payoutDate->toDateString()}");
            return;
        }

        // Validate tree
        if (!SponsorTreeSnapshot::isValidNestedSet()) {
            $this->error("Sponsor tree snapshot is invalid {$payoutDate->toDateString()}");
            exit;
        }

        $startTime = $this->start();
        $this->line("Special bonus started at " . $startTime->toDateTimeString());

        $this->line('Get deposit capital credit start date and end date');
        $startDate = $payoutDate->copy()->subDay()->startOfDay()->toDateTimeString();
        $endDate   = $payoutDate->copy()->subDay()->endOfDay()->toDateTimeString();

        $creditTypeId      = get_bank_credit_type(BankCreditTypeContract::CAPITAL_CREDIT)->id;
        $transactionTypeId = get_bank_transaction_type(BankTransactionTypeContract::DEPOSIT)->id;

        $this->line("[Step 1] Get previous day sales");
        $sales = $this->getTotalPersonalCapitalCredits($creditTypeId, $transactionTypeId, $startDate, $endDate);

        $getYesterdaySpecialBonusSnapshot = $payoutDate->copy()->subDay()->toDateString();

        $this->line("[Step 2] Get tree with percentage settings");
        $sponsorTreeSnapshot = $this->snapshotHistoryRepo->getModel()
            ->leftJoin('user_special_bonus_percentage_snapshots', function ($query) use ($getYesterdaySpecialBonusSnapshot) {
                $query->on('user_special_bonus_percentage_snapshots.user_id', 'sponsor_tree_snapshot_history.user_id')
                    ->where('user_special_bonus_percentage_snapshots.is_current', 1)
                    ->where('user_special_bonus_percentage_snapshots.run_date', $getYesterdaySpecialBonusSnapshot);
            })
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                'user_special_bonus_percentage_snapshots.percentage'
            ])
            ->toArray();

        if (!$sponsorTreeSnapshot) {
            $this->error('Snapshot is empty');
            exit;
        }

        $exchangeRate = $exchangeRateRepo->getModel()
            ->where('currency', BankCreditTypeContract::CAPITAL_CREDIT)
            ->first();

        DB::beginTransaction();

        try {
            $specialBonusRepo = new SpecialBonusRepository($sponsorTreeSnapshot, $sales);
            $this->line("[Step 3] Set payout date");
            $specialBonusRepo->setPayoutDate($payoutDate);

            $this->line("[Step 4] Set exchange rate");
            $specialBonusRepo->setExchangeRate($exchangeRate);

            $this->line("[Step 4] Set non eligible users");
            $specialBonusRepo->setNonEligibleUsers();

            $this->line("[Step 5] Calculate");
            $specialBonusRepo->calculate(credit_precision());

            //Update all special bonus payout user's can_view_special_bonus
            $payoutUserIds = $payoutRepo->getModel()->whereBetween('created_at', [$dateFrom, $dateTo])
                ->pluck('user_id')
                ->toArray();

            foreach (array_chunk($payoutUserIds, 100) as $userIds) {
                User::whereIn('id', $userIds)->update([
                    'view_special_bonus' => 1,
                ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));

        if (strtolower(config('app.env')) == 'uat' || strtolower(config('app.env')) == 'production') {
            // Run monthly payout on first
            if (now()->day === 1) {
                Artisan::call('payout:special_bonus');
            }
        }
    }

    protected function getTotalPersonalCapitalCredits($creditTypeId, $transactionTypeId, $startDate, $endDate)
    {
        return BankAccountStatement::join('bank_accounts', 'bank_accounts.id', 'bank_account_statements.bank_account_id')
            ->join('users', 'users.id', 'bank_accounts.reference_id')
            ->where('bank_account_statements.bank_credit_type_id', $creditTypeId)
            ->whereBetween('bank_account_statements.transaction_date', [$startDate, $endDate])
            ->where('bank_account_statements.bank_transaction_type_id', $transactionTypeId)
            ->groupBy('bank_account_statements.bank_account_id')
            ->selectRaw('users.id as user_id, sum(credit) as total_deposit')
            ->pluck('total_deposit', 'user_id')
            ->toArray();
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
