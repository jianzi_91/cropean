<?php

namespace Modules\Commission\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Contracts\DirectSponsorContract;
use Modules\Commission\Contracts\DirectSponsorPayoutContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;

class DirectSponsorCommand extends Command
{
    protected $signature = 'commissions:direct_sponsor {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'direct sponsor command payout';

    /**
     * The snapshot history repository
     *
     * @var SponsorTreeSnapshotHistoryContract
     */
    protected $snapshotHistoryRepo;

    /**
     * The direct sponsor payout repository
     *
     * @var DirectSponsorPayoutContract
     */
    protected $directSponsorPayoutRepo;

    /**
     * Create a new command instance.
     *
     * @param SponsorTreeSnapshotHistoryContract $historyContract
     * @param DirectSponsorPayoutContract $directSponsorContract
     */
    public function __construct(
        SponsorTreeSnapshotHistoryContract $historyContract,
        DirectSponsorPayoutContract $directSponsorContract
    ) {
        parent::__construct();

        $this->snapshotHistoryRepo     = $historyContract;
        $this->directSponsorPayoutRepo = $directSponsorContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $startTime = $this->start();
        $this->line('Direct sponsor started at ' . $startTime->toDateTimeString());

        $payoutDate = new Carbon($this->argument('date'));

        $dateFrom = $payoutDate->copy()->startOfDay()->subDay()->toDateTimeString();
        $dateTo   = $payoutDate->copy()->endOfDay()->subDay()->toDateTimeString();

        $this->line('[Step 1] Get list of member sales between ' . $dateFrom . ' to ' . $dateTo);

        //TODO
        $sales = [];

        if (!empty($sales)) {
            DB::beginTransaction();

            try {
                $this->line("[Step 2] Calculate payouts");
                $this->directSponsorPayoutRepo->setPayoutDate($payoutDate);
                $this->directSponsorPayoutRepo->setNonEligibleUsers();
                $this->directSponsorPayoutRepo->calculate($sales, credit_precision());

                $this->line('[Step 3] Proceed to Payout');
                $this->directSponsorPayoutRepo->preparePayout();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                throw $e;
            }
        } else {
            $this->line("No sales for these dates");
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
