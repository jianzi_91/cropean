<?php

namespace Modules\Commission\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Models\SpecialBonusPayout;
use Modules\Commission\Repositories\SpecialBonusCreditPayoutRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;

class SpecialBonusPayoutCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'payout:special_bonus {date?}';

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payout Commission Special Bonus.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $payoutDate = new Carbon($this->argument('date'));

        $startTime = $this->start();
        $this->line("Special bonus credit issue started at " . $startTime->toDateTimeString());

        $this->line('[Step 1] Get start date and end date');
        $startDate = $payoutDate->copy()->subMonth()->startOfMonth()->toDateString();
        $endDate   = $payoutDate->copy()->subMonth()->endOfMonth()->toDateString();

        $this->line('[Step 2] Get special bonus payouts');
        $payouts = $this->getSpecialBonusPayouts($startDate, $endDate);

        if ($payouts->count() <= 0) {
            $this->error('Special bonus payout already issued');
            exit;
        }

        $this->line("[Step 3] Get payout settings");
        $payoutPercentage = [
            BankCreditTypeContract::CASH_CREDIT => setting('special_bonus_payout_cash_credit_ratio', 1),
        ];

        $payoutCreditRates = ExchangeRate::where('currency', BankCreditTypeContract::CASH_CREDIT)
            ->pluck('buy_rate', 'currency')
            ->toArray();

        if (!$payoutCreditRates) {
            $this->error('No credit type rate');
            exit;
        }

        DB::beginTransaction();

        try {
            $specialBonusPayoutRepo = new SpecialBonusCreditPayoutRepository($payouts);
            $this->line("[Step 4] Set payout date");
            $specialBonusPayoutRepo->setPayoutDate($payoutDate);
            $this->line('[Step 5] Set payout credit and ratio');
            $specialBonusPayoutRepo->setPayoutCredit($payoutPercentage);
            $this->line('[Step 6] Set payout credit rate');
            $specialBonusPayoutRepo->setPayoutCreditRate($payoutCreditRates);
            $this->line('[Step 7] Payout credit');
            $specialBonusPayoutRepo->payout();

            $this->line('[Step 8] Update special bonus payout credit issued');
            $payoutDate = $payoutDate->toDateString();

            $this->line('[Step 9] Get all special bonus payout id');
            $specialBonusIds = [];
            foreach ($payouts as $payout) {
                $specialBonusIds = array_merge($specialBonusIds, explode(',', $payout->special_bonus_ids));
            }

            foreach (array_chunk($specialBonusIds, 1000) as $specialBonusId) {
                SpecialBonusPayout::whereIn('id', $specialBonusId)->update([
                    'is_credit_issued' => 1,
                    'payout_date'      => $payoutDate
                ]);
            }
            $this->line("Special Bonus Credit Issued Done");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }

    private function getSpecialBonusPayouts($startDate, $endDate)
    {
        return SpecialBonusPayout::whereBetween('created_at', [$startDate, $endDate])
            ->where('is_credit_issued', 0)
            ->whereNull('payout_date')
            ->selectRaw('user_id, sum(amount) as amounts, group_concat(id) as special_bonus_ids')
            ->groupBy('user_id')
            ->get();
    }
}
