<?php

namespace Modules\Commission\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignPayout;
use Modules\Commission\Contracts\GoldmineLevelPercentageContract;
use Modules\Commission\Models\GoldmineBonusPayout;
use Modules\Commission\Repositories\GoldmineBonusRepository;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\ExchangeRate\Models\ExchangeRate;
use Modules\Rank\Contracts\UserGoldmineLevelHistorySnapshotContract;
use Modules\Rank\Contracts\UserGoldmineRankHistorySnapshotContract;
use Modules\Rank\Models\GoldmineRank;
use Modules\Rbac\Models\Role;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Contracts\SnapshotContract;

class GoldmineCommissionCommand extends Command
{
    /**
     * The name and signature of the console command .
     *
     * @var string
     */
    protected $signature = 'commissions:goldmine_bonus {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Goldmine bonus command payout';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;

    /**
     * The goldmine level percentage repository
     *
     * @var GoldmineLevelPercentageContract
     */
    protected $levelPercentageRepo;
    protected $userRepo;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     *
     * @param SponsorTreeContract $sponsor
     * @param SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
     * @param GoldmineLevelPercentageContract $goldmineLevelPercentageContract
     * @param UserContract $userContract
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $snapshotHistoryContract,
        GoldmineLevelPercentageContract $goldmineLevelPercentageContract,
        UserContract $userContract
    ) {
        parent::__construct();

        $this->sponsorTree         = $sponsor;
        $this->snapshotHistoryRepo = $snapshotHistoryContract;
        $this->levelPercentageRepo = $goldmineLevelPercentageContract;
        $this->userRepo            = $userContract;
    }

    /**
     * Execute the console command.
     * @param UserGoldmineRankHistorySnapshotContract $userGoldmineRankHistorySnapshotRepo
     * @param UserGoldmineLevelHistorySnapshotContract $userGoldmineLevelHistorySnapshotRepo
     * @return mixed
     * @throws \Exception
     */
    public function handle(
        UserGoldmineRankHistorySnapshotContract $userGoldmineRankHistorySnapshotRepo,
        UserGoldmineLevelHistorySnapshotContract $userGoldmineLevelHistorySnapshotRepo
    ) {
        $payoutDate = new Carbon($this->argument('date'));

        // Validate tree
        if (!$this->sponsorTree->getModel()->isValidNestedSet()) {
            $this->error("Sponsor Tree is invalid $payoutDate");
            exit;
        }

        $dateFrom = $payoutDate->copy()->startOfDay();
        $dateTo   = $payoutDate->copy()->endOfDay();

        if (GoldmineBonusPayout::whereBetween('created_at', [$dateFrom, $dateTo])->exists()) {
            $this->error("Goldmine already processed for $payoutDate");
            exit;
        }

        $startTime = $this->start();
        $this->line("Goldmine Bonus started at " . $startTime->toDateTimeString());

        $this->line("[Step 1a] Get Daily ROI Between $dateFrom And $dateTo");
        $dailyRoiAmounts = $this->getDailyRoi($dateFrom, $dateTo);

        $this->line("[Step 1b] Get Campaign ROI on $payoutDate");
        $campaignRois    = $this->getCampaignRoi($payoutDate->toDateString());

        foreach ($campaignRois as $userId => $campaignRoi) {
            $dailyRoiAmounts[$userId] = bcadd($dailyRoiAmounts[$userId], $campaignRoi, 2);
        }

        if (empty($dailyRoiAmounts)) {
            $this->error("No daily roi for these dates");
            exit;
        }

        $this->line('[Step 2] Get Sponsor Tree Snapshot History');
        $sponsorTreeSnapshot = $this->snapshotHistoryRepo->getModel()
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
                DB::raw('0 as max_level'),
            ])
            ->toArray();

        //get user goldmine rank and goldmine level snapshot
        $snapshotDate                = $payoutDate->copy()->toDateString();
        $userGoldmineRankHistorySnap = $userGoldmineRankHistorySnapshotRepo->getModel()
            ->where('run_date', $snapshotDate)
            ->get()
            ->keyBy('user_id');

        $userGoldmineLevelHistorySnap = $userGoldmineLevelHistorySnapshotRepo->getModel()
            ->where('run_date', $snapshotDate)
            ->get()
            ->keyBy('user_id');

        if (!$sponsorTreeSnapshot) {
            $this->error('Snapshot is empty');
            exit;
        }

        $payoutPercentage = [
            BankCreditTypeContract::CASH_CREDIT => setting('goldmine_payout_cash_credit_ratio', 1),
        ];

        $payoutCreditRates = ExchangeRate::where('currency', BankCreditTypeContract::CASH_CREDIT)
            ->pluck('buy_rate', 'currency')
            ->toArray();

        $this->line('[Step 3] Get goldmine level percentage');
        $goldmineLevelPercentage = $this->levelPercentageRepo->getModel()
            ->pluck('percentage', 'level')
            ->toArray();

        $userGoldmineRanks = $this->userRepo->getModel()
            ->selectRaw('id, goldmine_rank_id, goldmine_level')
            ->get()
            ->keyBy('id')
            ->toArray();

        $goldmineRanks = GoldmineRank::all()->pluck('level', 'id')->toArray();

        $goldmine = new GoldmineBonusRepository($sponsorTreeSnapshot, $dailyRoiAmounts, $goldmineLevelPercentage);
        $this->line('[Step 4] Set payout date');
        $goldmine->setPayoutDate($payoutDate);
        $this->line('[Step 5] Set non eligible users');
        $goldmine->setNonEligibleUsers();
        $this->line('[Step 6] Set goldmine bonus rank and user goldmine bonus rank');
        $goldmine->setUserGoldmineRankLevel($goldmineRanks, $userGoldmineRanks, $userGoldmineRankHistorySnap, $userGoldmineLevelHistorySnap);

        DB::beginTransaction();

        try {
            $this->line('[Step 7] Calculate commission');
            $goldmine->calculate(credit_precision());

            $this->line('[Step 8] Proceed to Payout');
            $goldmine->payout($payoutPercentage, $payoutCreditRates);

            $this->line("Goldmine Payout Done");

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));
    }

    protected function getDailyRoi($dateFrom, $dateTo)
    {
        $roleIds = Role::members()->pluck('id')->toArray();

        return User::leftJoin('roi_payouts', function ($query) use ($dateFrom, $dateTo) {
            $query->on('roi_payouts.user_id', '=', 'users.id')
                ->whereBetween('roi_payouts.payout_date', [$dateFrom, $dateTo]);
        })->join('assigned_roles', 'assigned_roles.entity_id', 'users.id')
            ->whereIn('assigned_roles.role_id', $roleIds)
            ->where('users.is_system_generated', 0)
            ->selectRaw('users.id as user_id, if(roi_payouts.amount > 0, roi_payouts.amount, 0) as amount')
            ->pluck('amount', 'user_id')
            ->toArray();
    }

    protected function getCampaignRoi($payoutDate)
    {
        return CampaignPayout::where('payout_date', $payoutDate)
            ->groupBy('user_id')
            ->select(
                'user_id',
                DB::raw('SUM(amount) AS roi')
            )
            ->pluck('roi', 'user_id')
            ->toArray();
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
