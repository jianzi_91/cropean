<?php

namespace Modules\Commission\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Commission\Models\UserSpecialBonusPercentageHistory;
use Modules\Commission\Models\UserSpecialBonusPercentageSnapshots;

class UserSpecialBonusPercentageSnapshotCommand extends Command
{
    /**
     * The name and signature of the console command .
     *
     * @var string
     */
    protected $signature = 'snapshot:user_special_bonus_percentage {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take a snapshot of the user special bonus percentage histories';

    /**
     * Create a new command instance.
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $now = new Carbon($this->argument('date'));

        $date = $now->copy()->subDay()->endOfDay()->toDateTimeString();

        $history = UserSpecialBonusPercentageHistory::where('created_at', '<=', $date)
            ->where('is_current', 1)
            ->whereNull('deleted_at')
            ->select(['user_id', 'percentage', 'is_current', 'updated_by'])
            ->get()
            ->toArray();

        foreach ($history as $record) {
            $snapshot = array_merge($record, ['created_at' => Carbon::now(), 'updated_at' => Carbon::now(), 'run_date' => $date]);
            UserSpecialBonusPercentageSnapshots::insert($snapshot);
        }
    }
}
