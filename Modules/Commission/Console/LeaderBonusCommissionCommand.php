<?php

namespace Modules\Commission\Console;

use Artisan;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Campaign\Models\CampaignPayout;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Commission\Repositories\LeaderBonusCommissionRepository;
use Modules\Rank\Contracts\RankContract;
use Modules\Rebate\Models\RoiPayout;
use Modules\Tree\Contracts\SponsorTreeContract;
use Modules\Tree\Contracts\SponsorTreeSnapshotHistoryContract;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Plus65\Utility\Contracts\SnapshotContract;

class LeaderBonusCommissionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'commissions:leader_bonus {date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate leader bonus payout daily, without issue credit.';

    /**
     * The sponsor tree repository
     *
     * @var SponsorTreeContract
     */
    protected $sponsorTree;

    /**
     * The snapshot history repository
     *
     * @var SnapshotContract
     */
    protected $snapshotHistoryRepo;
    protected $rankRepo;
    protected $userRepo;

    /**
     * The start time
     * @var Carbon
     */
    protected $startTime;

    /**
     * Create a new command instance.
     *
     * @param SponsorTreeContract $sponsor
     * @param SponsorTreeSnapshotHistoryContract $snapshotHistoryContract
     * @param RankContract $rankRepo
     * @param UserContract $userRepo
     * @return void
     */
    public function __construct(
        SponsorTreeContract $sponsor,
        SponsorTreeSnapshotHistoryContract $snapshotHistoryContract,
        RankContract $rankRepo,
        UserContract $userRepo
    ) {
        parent::__construct();
        $this->sponsorTree         = $sponsor;
        $this->snapshotHistoryRepo = $snapshotHistoryContract;
        $this->rankRepo            = $rankRepo;
        $this->userRepo            = $userRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $payoutDate = new Carbon($this->argument('date'));

        // Validate tree
        if (!$this->sponsorTree->getModel()->isValidNestedSet()) {
            $this->error("Sponsor Tree is invalid $payoutDate");
            exit;
        }

        $dateFrom = $payoutDate->copy()->startOfDay();
        $dateTo   = $payoutDate->copy()->endOfDay();

        if (LeaderBonusPayout::whereBetween('created_at', [$dateFrom, $dateTo])->exists()) {
            $this->error("Leader bonus already processed for $payoutDate");
            exit;
        }

        $startTime = $this->start();
        $this->line("Leader Bonus started at " . $startTime->toDateTimeString());

        $this->line("[Step 1a] Get Daily ROI Between $dateFrom And $dateTo");
        $dailyRoiAmounts = $this->getDailyRoi($dateFrom, $dateTo);

        $this->line("[Step 1b] Get Campaign ROI on $payoutDate");
        $campaignRois    = $this->getCampaignRoi($payoutDate->toDateString());

        foreach ($campaignRois as $userId => $campaignRoi) {
            if (empty($dailyRoiAmounts[$userId])) {
                $dailyRoiAmounts[$userId] = $campaignRoi;
            } else {
                $dailyRoiAmounts[$userId] = bcadd($dailyRoiAmounts[$userId], $campaignRoi, 2);
            }
        }

        if (empty($dailyRoiAmounts)) {
            $this->error("No daily roi for these dates");
            exit;
        }

        $snapshotDate = $payoutDate->copy()->toDateString();
        $this->line('[Step 2] Get Sponsor Tree Snapshot History');
        $sponsorTreeSnapshot = $this->snapshotHistoryRepo->getModel()
            ->leftJoin('user_rank_history_snapshots', function ($query) use ($snapshotDate) {
                $query->on('user_rank_history_snapshots.user_id', 'sponsor_tree_snapshot_history.user_id')
                    ->where('user_rank_history_snapshots.is_current', 1)
                    ->where('user_rank_history_snapshots.run_date', $snapshotDate);
            })
            ->whereBetween('sponsor_tree_snapshot_history.run_date', [$dateFrom, $dateTo])
            ->orderBy('sponsor_tree_snapshot_history.level', 'desc')
            ->get([
                'sponsor_tree_snapshot_history.*',
                DB::raw('0 as percentage'),
                DB::raw('(case when user_rank_history_snapshots.rank_id > 0 then user_rank_history_snapshots.rank_id else 0 end) as rank_id'),
            ])
            ->toArray();

        if (!$sponsorTreeSnapshot) {
            $this->error('Snapshot is empty');
            exit;
        }

        $this->line('[Step 3] Get leader bonus rank');
        $leaderBonusRanks = $this->rankRepo->all()
            ->keyBy('id');

        $userLeaderBonusRanks = $this->userRepo->getModel()
            ->leftJoin('user_rank_history_snapshots', function ($query) use ($snapshotDate) {
                $query->on('user_rank_history_snapshots.user_id', 'users.id')
                    ->where('user_rank_history_snapshots.is_current', 1)
                    ->where('user_rank_history_snapshots.run_date', $snapshotDate);
            })
            ->selectRaw('users.id as user_id, user_rank_history_snapshots.rank_id')
            ->pluck('rank_id', 'user_id')
            ->toArray();

        $leaderBonus = new LeaderBonusCommissionRepository($sponsorTreeSnapshot, $dailyRoiAmounts);
        $this->line('[Step 4] Set payout date');
        $leaderBonus->setPayoutDate($payoutDate);
        $this->line('[Step 5] Set non eligible users');
        $leaderBonus->setNonEligibleUsers();
        $this->line('[Step 6] Set leader bonus rank and user leader bonus rank');
        $leaderBonus->setLeaderBonusRank($leaderBonusRanks, $userLeaderBonusRanks);
        $leaderBonus->setLeaderBonusManualRank($snapshotDate);

        DB::beginTransaction();

        try {
            $this->line('[Step 7] Calculate and insert record');
            $leaderBonus->calculation(credit_precision());

            $this->line("Leader Bonus Payout Calculate Done");

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        $this->line('Took ' . $this->stop()->format('%H:%I:%S seconds to finish'));

        if (strtolower(config('app.env')) == 'uat' || strtolower(config('app.env')) == 'production') {
            // Run monthly payout on first
            if (now()->day === 1) {
                Artisan::call('payout:leader_bonus');
            }
        }
    }

    protected function getDailyRoi($dateFrom, $dateTo)
    {
        return RoiPayout::whereBetween('payout_date', [$dateFrom, $dateTo])->pluck('amount', 'user_id')->toArray();
    }

    protected function getCampaignRoi($payoutDate)
    {
        return CampaignPayout::where('payout_date', $payoutDate)
            ->groupBy('user_id')
            ->select(
                'user_id',
                DB::raw('SUM(amount) AS roi')
            )
            ->pluck('roi', 'user_id')
            ->toArray();
    }

    /**
     * Start the timer
     *
     * @return Carbon
     * @throws \Exception
     */
    protected function start()
    {
        $this->startTime = new Carbon();
        return $this->startTime;
    }

    /**
     * Stop the timer
     *
     * @return \DateInterval
     * @throws \Exception
     */
    protected function stop()
    {
        $end = new Carbon();
        return $end->diff($this->startTime);
    }
}
