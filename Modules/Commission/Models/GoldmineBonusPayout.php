<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Rank\Models\GoldmineRank;
use Plus65\Base\Models\Relations\UserRelation;

class GoldmineBonusPayout extends Model
{
    use SoftDeletes, UserRelation;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'goldmine_bonus_payouts';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'payout_date',
        'user_id',
        'amount',
        'computed_amount',
        'issued_amount',
        'goldmine_rank_id',
        'goldmine_level'
    ];

    /**
     * Relation to credits.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany(GoldmineBonusPayoutCredit::class, 'goldmine_bonus_payout_id');
    }

    /**
     * Relation to goldmine rank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goldmineRank()
    {
        return $this->belongsTo(GoldmineRank::class, 'goldmine_rank_id');
    }
}
