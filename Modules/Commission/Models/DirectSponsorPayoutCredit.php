<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;

class DirectSponsorPayoutCredit extends Model
{
    protected $fillable = [
        'direct_sponsor_payout_id',
        'bank_credit_type_id',
        'percentage',
        'amount',
        'transaction_code',
    ];

    protected $table = 'direct_sponsor_payout_credits';
}
