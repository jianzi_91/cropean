<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class UserSpecialBonusPercentageHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'percentage',
        'is_current',
        'updated_by'
    ];

    protected $table = 'user_special_bonus_percentage_histories';

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * This model's relation to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
