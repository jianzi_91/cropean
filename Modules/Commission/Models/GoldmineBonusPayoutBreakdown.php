<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class GoldmineBonusPayoutBreakdown extends Model
{
    use SoftDeletes, UserRelation;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'goldmine_bonus_payout_breakdowns';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'goldmine_bonus_payout_id',
        'user_id',
        'level',
        'percentage',
        'amount',
    ];

    /**
     * Relation to payout.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payout()
    {
        return $this->belongsTo(GoldmineBonusPayout::class, 'goldmine_bonus_payout_id');
    }

    /**
     * Get percentage view
     *
     * @param  string  $value
     * @return string
     */
    public function getPercentageViewAttribute()
    {
        return amount_format(bcmul($this->percentage, 100, credit_precision()), credit_precision());
    }
}
