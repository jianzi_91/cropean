<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Concerns\HasDropDown;

class GoldmineLevelPercentage extends Model
{
    use SoftDeletes, HasDropDown;

    /**
     * Tablr name.
     *
     * @var string
     */
    protected $table = 'goldmine_level_percentages';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'percentage',
    ];
}
