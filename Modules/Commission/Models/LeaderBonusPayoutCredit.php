<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaderBonusPayoutCredit extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'leader_bonus_payout_id',
        'bank_credit_type_id',
        'bank_transaction_type_id',
        'percentage',
        'amount',
        'converted_amount',
        'transaction_code',
        'exchange_rate',
        'issued_amount',
    ];

    protected $table = 'leader_bonus_payout_credits';
}
