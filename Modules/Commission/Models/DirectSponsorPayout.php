<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;

class DirectSponsorPayout extends Model
{
    protected $fillable = [
        'payout_date',
        'user_id',
        'amount',
        'percentage',
        'computed_amount',
    ];

    protected $table = 'direct_sponsor_payouts';

    /**
    * Get admin fee display attribute
    *
    * @param unknown $query
    * @return unknown
    */
    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }
}
