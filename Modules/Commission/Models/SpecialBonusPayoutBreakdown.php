<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecialBonusPayoutBreakdown extends Model
{
    use SoftDeletes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'special_bonus_payout_breakdowns';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'special_bonus_payout_id',
        'user_id',
        'level',
        'percentage',
        'amount',
    ];

    /**
     * Relation to payout.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payout()
    {
        return $this->belongsTo(SpecialBonusPayout::class, 'special_bonus_payout_id');
    }

    /**
     * Get percentage view
     *
     * @param  string  $value
     * @return string
     */
    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }
}
