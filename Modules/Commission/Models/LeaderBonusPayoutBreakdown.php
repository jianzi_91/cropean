<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaderBonusPayoutBreakdown extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'leader_bonus_payout_id',
        'user_id',
        'percentage',
        'amount',
        'is_same_rank'
    ];

    protected $table = 'leader_bonus_payout_breakdowns';

    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }
}
