<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;

class DirectSponsorPayoutBreakdown extends Model
{
    protected $fillable = [
        'direct_sponsor_payout_id',
        'user_id',
        'level',
        'percentage',
        'amount',
    ];

    protected $table = 'direct_sponsor_payout_breakdowns';
}
