<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Rank\Models\Rank;
use Plus65\Base\Models\Relations\UserRelation;

class LeaderBonusPayout extends Model
{
    use SoftDeletes, UserRelation;

    protected $fillable = [
        'payout_date',
        'user_id',
        'amount',
        'percentage',
        'computed_amount',
        'rank_id',
        'is_credit_issued',
        'is_same_rank',
    ];

    protected $table = 'leader_bonus_payouts';

    /**
     * @return string
     */
    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }

    public function rank()
    {
        return $this->hasOne(Rank::class, 'id', 'rank_id');
    }
}
