<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plus65\Base\Models\Relations\UserRelation;

class SpecialBonusPayout extends Model
{
    use SoftDeletes, UserRelation;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'special_bonus_payouts';

    /**
     * Mass assignable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'payout_date',
        'user_id',
        'amount',
        'total_downline_sales',
        'percentage',
        'computed_amount',
        'is_credit_issued',
    ];

    /**
     * Relation to credits.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany(SpecialBonusPayoutCredit::class, 'special_bonus_payout_id');
    }

    /**
     * Get percentage view
     *
     * @param  string  $value
     * @return string
     */
    public function getPercentageDisplayAttribute()
    {
        return bcmul($this->percentage, 100, credit_precision());
    }
}
