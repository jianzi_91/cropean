<?php

namespace Modules\Commission\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Models\User;

class UserGoldmineLevelHistory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'level',
        'qualified_level',
        'is_current',
        'is_locked',
        'updated_by',
        'is_manual',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'id', 'updated_by');
    }

    public function scopeCurrent($query)
    {
        return $query->where('is_current', 1);
    }
}
