@extends('templates.admin.master')
@section('title', __('a_page_title.direct sponsor listing'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs', [
        'breadcrumbs'=>[
            ['name'=>__('s_breadcrumbs.home'), 'route'=>route('admin.dashboard')],
            ['name'=>__('a_report_sponsor.direct sponsor')],
        ],
        'header'=>__('a_report_sponsor.direct sponsor'),
    ])

    @component('templates.__fragments.components.filter')
        <div class="col-12 col-lg-6 col-xl-2 jv-label-color">
            {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_sponsor.payout date')) }}
        </div>
        <div class="col-12 col-lg-6 col-xl-2 jv-label-color">
            {{ Form::formText('member_id', request('member_id'), __('a_report_sponsor.member id')) }}
        </div>
    @endcomponent

    <div class="row">
        <!-- column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">{{__('a_report_sponsor.all direct sponsor')}}</h4>
                        @can('admin_commission_sponsor_bonus_export')
                            <button type="button" id="export" name="export" class="btn btn-primary waves-effect waves-light btn-lg">{{ __('a_report_sponsor.export') }}</button>
                        @endcan
                    </div>
                    <div class="table-responsive jv-table-responsive mt-20">
                        <table class="table">
                            <thead class="text-capitalize">
                            <tr>
                                <th>{{ __('a_report_sponsor.date') }}</th>
                                <th>{{ __('a_report_sponsor.member id') }}</th>
                                <th>{{ __('a_report_sponsor.contributed by') }}</th>
                                <th>{{ __('a_report_sponsor.direct sponsor %') }}</th>
                                <th>{{ __('a_report_sponsor.payout amount') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($payouts as $payout)
                                <tr>
                                    <td>{{ $payout->payout_date }}</td>
                                    <td>{{ $payout->member_id }}</td>
                                    <td>{{ $payout->contributed_by }}</td>
                                    <td>{{ $payout->percentage_display }}</td>
                                    <td>{{ amount_format($payout->payout_amount, credit_precision()) }}</td>
                                </tr>
                            @empty
                                @include('templates.__fragments.components.no-table-records', [ 'span' => 6,
                                'text' => __('a_report_sponsor.no records') ])
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $payouts->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(function () {
            $('#export').on('click', function() {
                window.location.href = '{!! route('admin.report.sponsor.export', array_merge(request()->except('page'))) !!}';
            });
        });
    </script>
@endpush
