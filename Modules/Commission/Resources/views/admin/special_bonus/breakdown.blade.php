@extends('templates.admin.master')
@section('title', __('a_page_title.special report breakdown'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.special bonus'), 'route'=>route('admin.report.special.index')],
        ['name'=>__('s_breadcrumb.special bonus breakdown')]
    ],
    'header'=>__('a_report_special_breakdown.special bonus breakdown'),
    'backRoute'=>route('admin.report.special.index')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_special_breakdown.payout date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formText('member_id', request('member_id'), __('a_report_special_breakdown.member id'), [] ,false) }}
</div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_special_breakdown.special bonus breakdown') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_special_breakdown.date') }}</th>
                <th>{{ __('a_report_special_breakdown.contributor member id') }}</th>
                <th>{{ __('a_report_special_breakdown.percentage') }}</th>
                <th>{{ __('a_report_special_breakdown.amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->created_at }}</td>
                <td>{{ $payout->member_id }}</td>
                <td>{{ $payout->percentage_display }}</td>
                <td>{{ amount_format($payout->amount, credit_precision()) }}</td>
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 4, 'text' => __('a_report_special_breakdown.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection