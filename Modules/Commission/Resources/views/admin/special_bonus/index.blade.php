@extends('templates.admin.master')
@section('title', __('a_page_title.special bonus report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.special bonus report')]
    ],
    'header'=>__('a_report_special.special bonus report')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_special.payout date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formText('member_id', request('member_id'), __('a_report_special.member id'), [] ,false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formText('email', request('email'), __('a_report_special.email'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
        <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_special.special bonus report') }}</h4>
        <div class="d-flex flex-row align-items-center">
            <div class="table-total-results mr-2">{{ __('a_report_special.total results:') }} {{ $payouts->total() }}</div>
            @can('admin_commission_special_bonus_export')
                {{ Form::formTabSecondary(__('a_report_special.export table'), route('admin.report.special.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_special.date') }}</th>
                <th>{{ __('a_report_special.member id') }}</th>
                <th>{{ __('a_report_special.email') }}</th>
                <th>{{ __('a_report_special.issued payout amount') }}</th>
                <th>{{ __('a_report_special.payout rate') }}</th>
                <th>{{ __('a_report_special.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->payout_date }}</td>
                <td>{{ $payout->member_id }}</td>
                <td>{{ $payout->email }}</td>
                <td>{{ amount_format($payout->issued_amount, credit_precision()) }}</td>
                <td>{{ amount_format(bcmul($payout->percentage, 100), credit_precision()) }} %</td>
                <td class="action">
                    <a href="{{route('admin.report.special.breakdown',$payout->id)}}"><i class="bx bx-show-alt"></i></a>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', ['span' => 6, 'text' => __('a_report_special.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>  
</div>
{!! $payouts->render() !!}

@endsection