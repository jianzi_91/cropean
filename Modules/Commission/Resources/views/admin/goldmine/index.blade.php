@extends('templates.admin.master')
@section('title', __('a_page_title.goldmine report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.goldmine report')]
    ],
    'header'=>__('a_report_goldmine.goldmine report')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-3">
        {{ Form::formDateRange('date', request('date'), __('a_report_goldmine.date'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('name', request('name'), __('a_report_goldmine.name'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('email', request('email'), __('a_report_goldmine.email'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formText('member_id', request('member_id'), __('a_report_goldmine.member id'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formSelect('goldmine_rank_id', filter_goldmine_rank_dropdown(), old('goldmine_rank_id', request('goldmine_rank_id')), __('a_report_goldmine.rank'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
        <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_goldmine.goldmine report') }}</h4>
        <div class="d-flex flex-row align-items-center">
            <div class="table-total-results mr-2">{{ __('a_report_goldmine.total results:') }} {{ $payouts->total() }}</div>
            @can('admin_commission_goldmine_bonus_export')
                {{ Form::formTabSecondary(__('a_report_goldmine.export table'), route('admin.report.goldmine.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_report_goldmine.date') }}</th>
                    <th>{{ __('a_report_goldmine.name') }}</th>
                    <th>{{ __('a_report_goldmine.member id') }}</th>
                    <th>{{ __('a_report_goldmine.email') }}</th>
                    <th>{{ __('a_report_goldmine.number of contributors') }}</th>
                    <th>{{ __('a_report_goldmine.issued payout amount') }}</th>
                    <th>{{ __('a_report_goldmine.rank') }}</th>
                    <th>{{ __('a_report_goldmine.action') }}</th>
                </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
                <tr>
                    <td>{{ $payout->created_at }}</td>
                    <td>{{ $payout->receiver_name }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->email }}</td>
                    <td>{{ $payout->contributors }}</td>
                    <td>{{ amount_format($payout->issued_amount, credit_precision()) }}</td>
                    <td>{{ $payout->goldmineRank->name }}</td>
                    <td class="action">
                        <a href="{{route('admin.report.goldmine.breakdown',$payout->id)}}"><i class="bx bx-show-alt"></i></a>
                    </td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 7, 'text' => __('a_report_goldmine.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection