@extends('templates.admin.master')
@section('title', __('a_page_title.leader bonus breakdown'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumbs.leader bonus report'), 'route'=>route('admin.report.leader.index')],
        ['name'=>__('s_breadcrumb.breakdown')]
    ],
    'header'=>__('a_report_leader_breakdown.leader bonus report breakdown'),
    'backRoute'=>route('admin.report.leader.index')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('a_report_leader_breakdown.contribution date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_leader_breakdown.contributor member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_leader_breakdown.contributor email'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_leader_breakdown.leader bonus report breakdown list') }}</h4>
            @can('admin_commission_leader_bonus_breakdown_export')
                {{ Form::formTabSecondary(__('a_report_leader_breakdown.export table'), route('admin.report.leader.breakdown.export', ['uid' => $userId, 'date' => $date, 'type' => $type]) . '?' . http_build_query(request()->except('page')) )}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_leader_breakdown.contribution date') }}</th>
                <th>{{ __('a_report_leader_breakdown.contributor member id') }}</th>
                <th>{{ __('a_report_leader_breakdown.contributor email') }}</th>
                <th>{{ __('a_report_leader_breakdown.percentage') }}</th>
                <th>{{ __('a_report_leader_breakdown.amount') }}</th>
                <th>{{ __('a_report_leader_breakdown.receiver member rank') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->created_at->format('Y-m-d') }}</td>
                <td>{{ $payout->name }}</td>
                <td>{{ $payout->member_id }}</td>
                <td>{{ $payout->email }}</td>
                <td>{{ $payout->percentage_display }}%</td>
                <td>{{ amount_format(bcmul($payout->amount, $payout->exchange_rate, credit_precision()), credit_precision()) }}</td>
                <td>{{ __('s_ranks.' . $payout->rank_name) }}</td>
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 6, 'text' => __('a_report_leader_breakdown.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection