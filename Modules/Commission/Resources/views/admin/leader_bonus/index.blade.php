@extends('templates.admin.master')
@section('title', __('a_page_title.leader bonus'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.leader bonus report')]
    ],
    'header'=>__('a_report_leader.leader bonus report')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-3">
    {{ Form::formDateRange('payout_date', request('payout_date'), __('a_report_leader.payout date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('name', request('name'), __('a_report_leader.name'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('member_id', request('member_id'), __('a_report_leader.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formText('email', request('email'), __('a_report_leader.email'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formSelect('type', filter_leader_bonus_type_dropdown(), old('type', request('type')), __('a_report_leader.type'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
        <h4 class="card-title filter-title m-0 p-0">{{ __('a_report_leader.leader bonus report') }}</h4>
        <div class="d-flex flex-row align-items-center">
            <div class="table-total-results mr-2">{{ __('a_report_leader.total results:') }} {{ $payouts->total() }}</div>
            @can('admin_commission_leader_bonus_export')
                {{ Form::formTabSecondary(__('a_report_leader.export table'), route('admin.report.leader.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
            <tr>
                <th>{{ __('a_report_leader.date') }}</th>
                <th>{{ __('a_report_leader.name') }}</th>
                <th>{{ __('a_report_leader.member id') }}</th>
                <th>{{ __('a_report_leader.email') }}</th>
                <th>{{ __('a_report_leader.type') }}</th>
                <th>{{ __('a_report_leader.issued payout amount') }}</th>
                <th>{{ __('a_report_leader.action') }}</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($payouts as $payout)
                <tr>
                    <td>{{ $payout->payout_date }}</td>
                    <td>{{ $payout->receiver_name }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->email }}</td>
                    <td>{{ __('s_bank_transaction_types.'.$payout->type) }}</td>
                    <td>{{ amount_format($payout->leader_bonus_amount, credit_precision()) }}</td>
                    <td class="action">
                        <a href="{{ route('admin.report.leader.breakdown', ['uid' => $payout->user_id, 'date' => $payout->payout_date, 'type' => $payout->is_same_rank]) }}"><i class="bx bx-show-alt"></i></a>
                    </td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 6, 'text' => __('a_report_leader.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}

@endsection