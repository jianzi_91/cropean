@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',
        [
        'breadcrumbs' => [
            ['name' => __('a_roi_setting.dashboard'), 'route' => route('admin.dashboard')],
            ['name' => __('a_roi_setting.roi tier setting')]
        ],
        'header'=>__('a_roi_setting.roi tier setting')
    ])

    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-content">
                    {{ Form::open(['route' => ['admin.roi.setting.update'], 'method'=>'put', 'id'=>'roi_setting', 'onsubmit' => 'btn_submit.disabled = true; return true;']) }}
                    @component('templates.__fragments.components.tables')
                    <thead class="text-capitalize">
                        <tr>
                            <th>{{ __('a_roi_setting.equity') }}</th>
                            <th>{{ __('a_roi_setting.daily roi payout rate') }}</th>
                            <th>{{ __('a_roi_setting.roll over credit payout ratio') }}</th>
                            <th>{{ __('a_roi_setting.cash credit payout ratio') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse ($roiTiers as $roiTier)
                        <tr>
                            @if ($roiTier->equity_min == $roiTier->equity_max)
                                <td>&gt;= {{ amount_format($roiTier->equity_min, 0) }}</td>
                            @else
                                <td>{{ amount_format($roiTier->equity_min, 0) }} - {{ amount_format($roiTier->equity_max, 0) }}</td>
                            @endif

                            <td>
                                {{ Form::formText('payout_percentage_' . $roiTier->id, old('payout_percentage') ?: bcmul($roiTier->payout_percentage, 100, credit_precision()), '', ['required'=>true], false) }}
                            </td>
                            <td>
                                {{ Form::formText('rollover_credit_' . $roiTier->id, old('rollover_credit') ?: bcmul(json_decode($roiTier->payout_credits)->roll_over_credit ?? 0.5, 100, credit_precision()), '', ['required'=>true], false) }}
                            </td>
                            <td>
                                {{ Form::formText('cash_credit_'  . $roiTier->id, old('cash_credit') ?: bcmul(json_decode($roiTier->payout_credits)->cash_credit ?? 0.5, 100, credit_precision()), '', ['required'=>true], false) }}
                            </td>
                        </tr>
                    @empty
                        @include('templates.__fragments.components.no-table-records', [ 'span' => 4, 'text' => __('a_roi_setting.no records') ])
                    @endforelse
                    </tbody>
                    @endcomponent
                    <div class="d-flex justify-content-end m-0 p-2">
                        {{ Form::formButtonPrimary('btn_submit', __('a_roi_setting.save')) }}
                    </div>
                    {{ Form::close() }}
                </div>       
            </div>  
        </div>  
    </div>
@endsection