@extends('templates.admin.master')
@section('title', __('a_page_title.user management'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',
        [
        'breadcrumbs' => [
            ['name' => __('a_member_management_goldmine.dashboard'), 'route' => route('admin.dashboard')],
            ['name' => __('a_member_management_goldmine.member management'), 'route' => route('admin.management.members.index')],
            ['name' => __('a_member_management_goldmine.goldmine rank and level')]
        ],
        'header'=>__('a_member_management_special_bonus.profile settings'),
        'backRoute'=>route('admin.management.members.index'),
    ])

    @include('templates.admin.includes._mm-nav', ['page' => 'goldmine rank and level', 'uid' => $user->id ])
    <br />

    <div class="row">
        @can('admin_user_member_management_goldmine_rank_edit')
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        {{ Form::open(['route' => ['admin.management.members.goldmine.rank.update', $user->id], 'method'=>'put', 'id'=>'update-rank','onsubmit' => 'btn_submit_updateRank.disabled = true; return true;']) }}
                        {{ Form::formSelect('manual_goldmine_rank_id', goldmine_rank_dropdown(), $userGoldmineRank && $userGoldmineRank->is_manual ? $userGoldmineRank->goldmine_rank_id : [], __('a_member_management_goldmine.manual goldmine rank'), ['required'=>true]) }}
                        <input type="hidden" value="false" name="is_locked_rank" id="is_locked_rank_hidden" />
                        {{ Form::formSelect('natural_goldmine_rank_id', goldmine_rank_dropdown(), $userGoldmineRank->qualified_goldmine_rank_id ?? old('natural_goldmine_rank_id'), __('a_member_management_goldmine.natural goldmine rank'), ['readonly'=> true, 'disabled' => true]) }}
                        @can ('admin_user_management_member_goldmine_rank_lock')
                            {{ Form::formCheckbox('is_locked_rank', 'is_locked_rank', '', __('a_member_management_goldmine.goldmine rank lock'), ['isChecked'=>$userGoldmineRank->is_locked ?? false, 'class'=>'js-checkbox']) }}
                        @endcan
                        <div style="width:100%;text-align:right;">
                            {{ Form::formButtonPrimary('btn_submit_updateRank', __('a_member_management_goldmine.save')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @endcan
        @can('admin_user_member_management_goldmine_level_edit')
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        {{ Form::open(['route' => ['admin.management.members.goldmine.level.update', $user->id], 'method'=>'put', 'id'=>'update-level', 'onsubmit' => 'btn_submit_updateLevel.disabled = true; return true;']) }}
                        {{ Form::formSelect('manual_goldmine_level', goldmine_level_dropdown(), $userGoldmineLevel && $userGoldmineLevel->is_manual ? $userGoldmineLevel->level : [], __('a_member_management_goldmine.manual goldmine level'), ['required'=>true]) }}
                        <input type="hidden" value="false" name="is_locked_level" id="is_locked_level_hidden" />
                        {{ Form::formSelect('natural_goldmine_level', goldmine_level_dropdown(), $userGoldmineLevel->qualified_level ?? old('natural_goldmine_level'), __('a_member_management_goldmine.natural goldmine level'), ['readonly'=> true, 'disabled' => true]) }}
                        @can ('admin_user_management_member_goldmine_level_lock')
                            {{ Form::formCheckbox('is_locked_level', 'is_locked_level', '', __('a_member_management_goldmine.goldmine level lock'), ['isChecked'=>$userGoldmineLevel->is_locked ?? false, 'class'=>'js-checkbox']) }}
                        @endcan
                        <div style="width:100%;text-align:right;">
                            {{ Form::formButtonPrimary('btn_submit_updateLevel', __('a_member_management_goldmine.save')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @endcan
    </div>
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
    //comment
        $(function() {
            @isset($userGoldmineRank->is_locked)
            $('#is_locked_rank_hidden').attr('disabled', true)
            @endisset

            @isset($userGoldmineLevel->is_locked)
            $('#is_locked_level_hidden').attr('disabled', true)
            @endisset

            var submitted = false;

            $('#btn_submit_updateRank').on('click', function() {
                var db = $('#is_locked_rank').is(':checked') ? true : false
                $('#is_locked_rank').val(db)
                if (db) $('#is_locked_rank_hidden').attr('disabled', true)
                else $('#is_locked_rank_hidden').attr('disabled', false)

                if(!submitted){
                    submitted = true;
                    $('#update-rank').submit()
                }
            })

            $('#btn_submit_updateLevel').on('click', function() {
                var db = $('#is_locked_level').is(':checked') ? true : false
                $('#is_locked_level').val(db)
                if (db) $('#is_locked_level_hidden').attr('disabled', true)
                else $('#is_locked_level_hidden').attr('disabled', false)

                if(!submitted){
                    submitted = true;
                    $('#update-level').submit()
                }
            })

            if ($('#manual_goldmine_rank_id').val() == "") {
                $('#is_locked_rank').attr('disabled', 'disabled');
            }

            $('#manual_goldmine_rank_id').on('change', function() {
                if ($(this).val() == "") {
                    $('#is_locked_rank').prop('checked', false);
                    $('#is_locked_rank').attr('disabled', 'disabled');
                } else {
                    $('#is_locked_rank').removeAttr('disabled');
                }
            });

            if ($('#manual_goldmine_level').val() == "") {
                $('#is_locked_level').attr('disabled', 'disabled');
            }

            $('#manual_goldmine_level').on('change', function() {
                if ($(this).val() == "") {
                    $('#is_locked_level').prop('checked', false);
                    $('#is_locked_level').attr('disabled', 'disabled');
                } else {
                    $('#is_locked_level').removeAttr('disabled');
                }
            });
        })
    </script>
@endpush
