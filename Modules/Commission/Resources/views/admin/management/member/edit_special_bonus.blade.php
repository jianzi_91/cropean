@extends('templates.admin.master')
@section('title', __('a_page_title.profile'))

@section('main')
    @include('templates.__fragments.components.breadcrumbs',
        [
        'breadcrumbs' => [
            ['name' => __('a_member_management_special_bonus.dashboard'), 'route' => route('admin.dashboard')],
            ['name' => __('a_member_management_special_bonus.member management'), 'route' => route('admin.management.members.index')],
            ['name' => __('a_member_management_special_bonus.special bonus')]
        ],
        'header'=>__('a_member_management_special_bonus.profile settings'),
        'backRoute'=>route('admin.management.members.index'),
    ])

    @include('templates.admin.includes._mm-nav', ['page' => 'special bonus', 'uid' => $user->id ])
    <br />

    <div class="row">
        <div class="col-xs-12 col-lg-6">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        {{ Form::open(['route' => ['admin.management.members.special.update', $user->id], 'method'=>'put', 'id'=>'profile']) }}
                        {{ Form::formText('special_bonus_percentage', bcmul($user->special_bonus_percentage, 100, credit_precision()) ?: old('special_bonus_percentage'), __('a_member_management_special_bonus.percentage value'), ['required'=>true]) }}
                        <div style="width:100%;text-align:right;">
                            {{ Form::formButtonPrimary('btn_submit', __('a_member_management_special_bonus.save')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection