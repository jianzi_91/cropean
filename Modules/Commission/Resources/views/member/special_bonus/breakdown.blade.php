@extends('templates.member.master')
@section('title', __('m_page_title.special bonus breakdown'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.special bonus report'), 'route'=>route('member.report.special.index')],
        ['name'=>__('s_breadcrumb.special bonus breakdown')]
    ],
    'header'=>__('m_report_special_breakdown.special bonus breakdown'),
    'backRoute'=>route('member.report.special.index')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('payout_date', old('payout_date', request('payout_date')), __('m_report_special_bonus.payout date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formText('member_id', old('member_id'), __('m_report_special_breakdown.member id'), [], false)}}
</div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_special_breakdown.special bonus report breakdown') }}</h4>
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
                <tr>
                    <th>{{ __('m_report_special_breakdown.date') }}</th>
                    <th>{{ __('m_report_special_breakdown.member id') }}</th>
                    <th>{{ __('m_report_special_breakdown.percentage') }}</th>
                    <th>{{ __('m_report_special_breakdown.amount') }}</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($payouts as $payout)
                <tr>
                    <td>{{ $payout->created_at }}</td>
                    <td>{{ $payout->member_id }}</td>
                    <td>{{ $payout->payout_date }}</td>
                    <td>{{ amount_format($payout->amount, credit_precision()) }}</td>
                </tr>
                @empty
                    @include('templates.__fragments.components.no-table-records', ['span' => 4, 'text' => __('m_report_special_breakdown.no records') ])
                @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection