@extends('templates.member.master')
@section('title', __('m_page_title.special bonus report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.special bonus report')]
    ],
    'header'=>__('m_report_special_bonus.special bonus report')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('payout_date', old('payout_date', request('payout_date')), __('m_report_special_bonus.payout date'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_special_bonus.special bonus report') }}</h4>
            @can('member_commission_special_bonus_export')
                {{ Form::formTabSecondary(__('m_report_special_bonus.export table'), route('member.report.special.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_special_bonus.date') }}</th>
                <th>{{ __('m_report_special_bonus.issued payout amount') }}</th>
                <th>{{ __('m_report_special_bonus.payout rate') }}</th>
                <th>{{ __('m_report_special_bonus.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->payout_date }}</td>
                <td>{{ amount_format($payout->issued_amount, credit_precision()) }}</td>
                <td>{{ amount_format(bcmul($payout->percentage, 100), credit_precision()) }} %</td>
                <td class="action">
                    <a href="{{route('member.report.special.breakdown', $payout->id)}}"><i class="bx bx-show-alt"></i></a>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', ['span' => 4, 'text' => __('m_report_special_bonus.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection