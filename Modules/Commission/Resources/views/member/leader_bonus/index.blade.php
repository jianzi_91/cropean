@extends('templates.member.master')
@section('title', __('m_page_title.leader bonus report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.leader bonus report')]
    ],
    'header'=>__('m_report_leader.leader bonus report')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-4">
        {{ Form::formDateRange('payout_date', request('payout_date'), __('m_report_leader.payout date'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-3">
        {{ Form::formSelect('type', filter_leader_bonus_type_dropdown(), old('type', request('type')), __('m_report_leader.type'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_leader.leader bonus report') }}</h4>
            @can('member_commission_leader_bonus_export')
                {{ Form::formTabSecondary(__('m_report_leader.export table'), route('member.report.leader.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_leader.date') }}</th>
                <th>{{ __('m_report_leader.type') }}</th>
                <th>{{ __('m_report_leader.payout amount') }}</th>
                <th>{{ __('m_report_leader.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->payout_date }}</td>
                <td>{{ __('s_bank_transaction_types.' . $payout->type) }}</td>
                <td>{{ amount_format($payout->leader_bonus_amount, credit_precision()) }}</td>
                <td class="action">
                    <a href="{{ route('member.leader.breakdown.report', ['uid' => auth()->user()->id, 'date' => $payout->payout_date, 'type' => $payout->is_same_rank]) }}"><i class="bx bx-show-alt"></i></a>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', ['span' => 4, 'text' => __('m_report_leader.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection