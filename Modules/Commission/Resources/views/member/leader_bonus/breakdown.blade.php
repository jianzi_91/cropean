@extends('templates.member.master')
@section('title', __('m_page_title.leader bonus breakdown'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumbs.leader bonus'), 'route'=>route('member.report.leader.index')],
        ['name'=>__('s_breadcrumb.leader bonus breakdown')]
    ],
    'header'=>__('m_report_leader_breakdown.leader bonus breakdown'),
    'backRoute'=>route('member.report.leader.index')
])

@component('templates.__fragments.components.filter')
    <div class="col-xs-12 col-lg-4">
        {{ Form::formDateRange('created_at', request('created_at'), __('m_report_leader.contribution date'), [], false) }}
    </div>
    <div class="col-xs-12 col-lg-4">
        {{ Form::formText('member_id', old('member_id'), __('m_report_leader_breakdown.member id'), [], false) }}
    </div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_leader_breakdown.leader bonus report breakdown') }}</h4>
            @can('member_commission_leader_bonus_breakdown_export')
                {{ Form::formTabSecondary(__('m_report_leader_breakdown.export table'), route('member.leader.breakdown.report.export', ['uid' => auth()->user()->id, 'date' => $date, 'type' => $type]) . '?' . http_build_query(request()->except('page')) )}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_leader_breakdown.contribution date') }}</th>
                <th>{{ __('m_report_leader_breakdown.contributor member id') }}</th>
                <th>{{ __('m_report_leader_breakdown.percentage') }}</th>
                <th>{{ __('m_report_leader_breakdown.amount') }}</th>
                <th>{{ __('m_report_leader_breakdown.receiver member rank') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->created_at }}</td>
                <td>{{ $payout->member_id }}</td>
                <td>{{ $payout->percentage_display }}%</td>
                <td>{{ amount_format(bcmul($payout->amount, $payout->exchange_rate, credit_precision()), credit_precision()) }}</td>
                <td>{{ __('s_ranks.' . $payout->rank_name) }}</td>
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('m_report_leader_breakdown.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}

@endsection