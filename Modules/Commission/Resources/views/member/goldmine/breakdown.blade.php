@extends('templates.member.master')
@section('title', __('m_page_title.goldmine breakdown report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.goldmine report'), 'route'=>route('member.report.goldmine.index')],
        ['name'=>__('s_breadcrumb.goldmine breakdown')]
    ],
    'header'=>__('m_report_goldmind_breakdown.goldmine breakdown'),
    'backRoute'=>route('member.report.goldmine.index')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('date', old('date', request('date')), __('m_report_goldmine.date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formText('member_id', old('member_id', request('member_id')), __('m_report_goldmine_breakdown.member id'), [], false) }}
</div>
<div class="col-xs-12 col-lg-3">
    {{ Form::formSelect('level', filter_goldmine_level_dropdown(), old('level', request('level')), __('m_report_goldmine_breakdown.level'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_goldmine_breakdown.goldmine report breakdown') }}</h4>
            @can('member_commission_goldmine_bonus_breakdown_export')
                {{ Form::formTabSecondary(__('m_report_goldmine_breakdown.export table'), route('member.report.goldmine.breakdown.export', $payoutId) . '?' . http_build_query(request()->except('page')) )}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_goldmine_breakdown.date') }}</th>
                <th>{{ __('m_report_goldmine_breakdown.member id') }}</th>
                <th>{{ __('m_report_goldmine_breakdown.level') }}</th>
                <th>{{ __('m_report_goldmine_breakdown.percentage') }}</th>
                <th>{{ __('m_report_goldmine_breakdown.amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->created_at }}</td>
                <td>{{ $payout->member_id }}</td>
                <td>{{ $payout->level }}</td>
                <td>{{ amount_format(bcmul($payout->percentage, 100, credit_precision())) }}%</td>
                <td>{{ amount_format($payout->amount, credit_precision()) }}</td>
            </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('m_report_goldmine_breakdown.no records') ])
            @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection