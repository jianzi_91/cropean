@extends('templates.member.master')
@section('title', __('m_page_title.goldmine report'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.goldmine report')]
    ],
    'header'=>__('m_report_goldmine.goldmine report')
])

@component('templates.__fragments.components.filter')
<div class="col-xs-12 col-lg-4">
    {{ Form::formDateRange('created_at', old('created_at', request('created_at')), __('m_report_goldmine.date'), [], false) }}
</div>
<div class="col-xs-12 col-lg-4">
    {{ Form::formSelect('goldmine_rank_id', filter_goldmine_rank_dropdown(), old('goldmine_rank_id', request('goldmine_rank_id')), __('m_report_goldmine.goldmine rank'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title m-0 p-0">{{ __('m_report_goldmine.goldmine report') }}</h4>
            @can('member_commission_goldmine_bonus_export')
                {{ Form::formTabSecondary(__('m_report_goldmine.export table'), route('member.report.goldmine.export', array_merge(request()->except('page'))))}}
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_report_goldmine.date') }}</th>
                <th>{{ __('m_report_goldmine.number of contributors') }}</th>
                <th>{{ __('m_report_goldmine.issued payout amount') }}</th>
                <th>{{ __('m_report_goldmine.rank') }}</th>
                <th>{{ __('m_report_goldmine.action') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($payouts as $payout)
            <tr>
                <td>{{ $payout->created_at }}</td>
                <td>{{ $payout->contributors }}</td>
                <td>{{ amount_format($payout->issued_amount, credit_precision()) }}</td>
                <td>{{ $payout->goldmineRank->name }}</td>
                <td class="action">
                    <a href="{{route('member.report.goldmine.breakdown',$payout->id)}}"><i class="bx bx-show-alt"></i></a>
                </td>
            </tr>
        @empty
            @include('templates.__fragments.components.no-table-records', ['span' => 5, 'text' => __('m_report_goldmine.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $payouts->render() !!}
@endsection