<?php

namespace Modules\Commission\Queries;

use QueryBuilder\QueryBuilder;
use Modules\Commission\Models\SponsorCommission;

class SponsorQuery extends QueryBuilder
{
    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = SponsorCommission::select('sponsor_commissions.*')
            ->join('users as receiving_users', 'receiving_users.id', 'sponsor_commissions.user_id')
            ->join('users as contributing_users', 'contributing_users.id', 'sponsor_commissions.contributor_user_id')
            ->with('user', 'contributor')
            ->orderBy('sponsor_commissions.created_at', 'desc');

        return $query;
    }
}
