<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Modules\Commission\Models\SpecialBonusPayoutBreakdown;
use QueryBuilder\QueryBuilder;

class SpecialBonusBreakdownQuery extends QueryBuilder
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = SpecialBonusPayoutBreakdown::select('special_bonus_payout_breakdowns.*')
            ->join('users as contributing_users', 'contributing_users.id', 'special_bonus_payout_breakdowns.user_id')
            ->select([
                'contributing_users.username',
                'contributing_users.member_id',
                'special_bonus_payout_breakdowns.*',
            ])
            ->orderBy('special_bonus_payout_breakdowns.created_at', 'desc');

        return $query;
    }
}
