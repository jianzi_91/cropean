<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Commission\Models\LeaderBonusPayoutBreakdown;
use QueryBuilder\QueryBuilder;

class LeaderBonusBreakdownQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        $query = LeaderBonusPayoutBreakdown::join('users as contributing_users', 'leader_bonus_payout_breakdowns.user_id', '=', 'contributing_users.id')
            ->join('leader_bonus_payouts', 'leader_bonus_payouts.id', 'leader_bonus_payout_breakdowns.leader_bonus_payout_id')
            ->join('leader_bonus_payout_credits', 'leader_bonus_payout_credits.leader_bonus_payout_id', 'leader_bonus_payouts.id')
            ->join('users as receiving_user', 'receiving_user.id', 'leader_bonus_payouts.user_id')
            ->join('ranks', 'ranks.id', 'leader_bonus_payouts.rank_id')
            ->selectRaw('leader_bonus_payout_breakdowns.*, ranks.name as rank_name, contributing_users.member_id, contributing_users.name, contributing_users.email, leader_bonus_payout_credits.exchange_rate')
            ->orderBy('leader_bonus_payout_breakdowns.created_at', 'desc')
            ->orderBy('leader_bonus_payout_breakdowns.user_id');

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
