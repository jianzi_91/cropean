<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Commission\Models\GoldmineBonusPayoutBreakdown;
use QueryBuilder\QueryBuilder;

class GoldmineBreakdownQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = GoldmineBonusPayoutBreakdown::select('goldmine_bonus_payout_breakdowns.*')
            ->join('users as contributing_users', 'contributing_users.id', 'goldmine_bonus_payout_breakdowns.user_id')
            ->select([
                'contributing_users.member_id',
                'contributing_users.name',
                'goldmine_bonus_payout_breakdowns.*',
            ])
            ->orderBy('goldmine_bonus_payout_breakdowns.created_at', 'desc');

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
