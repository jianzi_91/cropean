<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Modules\Credit\Models\BankAccountStatement;
use QueryBuilder\QueryBuilder;

class LeaderBonusQuery extends QueryBuilder
{
    use Exportable;

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        $query = BankAccountStatement::join('leader_bonus_payout_credits', 'leader_bonus_payout_credits.transaction_code', 'bank_account_statements.transaction_code')
            ->join('bank_transaction_types', 'bank_transaction_types.id', 'leader_bonus_payout_credits.bank_transaction_type_id')
            ->join('leader_bonus_payouts', 'leader_bonus_payouts.id', 'leader_bonus_payout_credits.leader_bonus_payout_id')
            ->join('users as receiving_users', 'receiving_users.id', 'leader_bonus_payouts.user_id')
            ->selectRaw('leader_bonus_payouts.payout_date, bank_transaction_types.name_translation as type, 
            bank_account_statements.credit as leader_bonus_amount, receiving_users.member_id, 
            receiving_users.email, leader_bonus_payouts.is_same_rank as is_same_rank, 
            receiving_users.id as user_id, receiving_users.name as receiver_name')
            ->whereNull('bank_account_statements.deleted_at')
            ->groupBy('leader_bonus_payout_credits.transaction_code')
            ->orderBy('leader_bonus_payouts.payout_date', 'desc')
            ->orderBy('receiving_users.id');

        return $query;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->get();
    }
}
