<?php

namespace Modules\Commission\Queries\Member;

use Modules\Commission\Queries\SponsorQuery as BaseQuery;

class SponsorQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'sponsor_commissions',
            'column' => 'payout_date'
        ],
        'contributed_by_username' => [
            'filter' => 'like',
            'table'  => 'contributing_users',
            'column' => 'username',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', auth()->user()->id);
    }
}
