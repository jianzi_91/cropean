<?php

namespace Modules\Portfolio\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Portfolio\Contracts\PortfolioStatusContract;
use Modules\Portfolio\Queries\PortfolioQuery as BaseQuery;
use QueryBuilder\FilterBroker;

class PortfolioQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'start_date' => [
            'filter' => 'date_range',
            'table'  => 'portfolios',
            'column' => 'start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'target_amount_from' => [
            'filter' => 'greater_than_equal',
            'table'  => 'portfolios',
            'column' => 'target_amount',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'target_amount_to' => [
            'filter' => 'less_than_equal',
            'table'  => 'portfolios',
            'column' => 'target_amount',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'min_amount' => [
            'filter' => 'less_than_equal',
            'table'  => 'portfolios',
            'column' => 'min_co_finance_amount',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'max_amount' => [
            'filter' => 'less_than_equal',
            'table'  => 'portfolios',
            'column' => 'total_amount',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'portfolios',
            'column' => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'portfolio_status_id' => [
            'filter' => 'multi_select',
            'table'  => 'portfolios',
            'column' => 'portfolio_status_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        // 'title' => [
        //     'filter'    => 'search_portfolio',
        //     'table'     => 'portfolio_i18ns',
        //     'namespace' => 'Modules\Portfolio\Queries\Filters',
        // ],
        'portfolio_id' => [
            'filter' => 'equal',
            'table'  => 'portfolios',
            'column' => 'id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'translator_language_id' => [
            'filter' => 'equal',
            'table'  => 'portfolio_i18ns',
            'column' => 'translator_language_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'published_date' => [
            'filter' => 'less_than_equal',
            'table'  => 'portfolios',
            'column' => 'start_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'user_id' => [
            'filter' => 'equal',
            'table'  => 'portfolio_investments',
            'column' => 'user_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'keyword' => [
            'filter' => 'search_keyword',
            'table'  => 'portfolios',
            'namespace' => 'Modules\Portfolio\Queries\Filters',
        ],
    ];

    public function __construct(FilterBroker $broker, array $parameters = [])
    {
        parent::__construct($broker, $parameters);
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        $portfolioStatusContract = resolve(PortfolioStatusContract::class);

        return $this->builder->where('portfolio_statuses.name', '<>', $portfolioStatusContract::DRAFT)
            ->select([
                'portfolios.*',
                'portfolio_i18ns.title as title_i18ns',
                'portfolio_i18ns.description as description_i18ns',
                'portfolio_attachments.id as attachment_id',
                'portfolio_attachments.path as attachment_path',
                'portfolio_attachments.filename as filename',
                'portfolio_statuses.name_translation as status_name',
            ]);
    }

    /**
     * Adhoc processes after build
     */
    public function afterBuild()
    {
        $languageId = get_translator_language_id($this->locale);

        return $this->builder->where('portfolio_i18ns.translator_language_id', $languageId);
    }

    /**
     * Data mapping
     * @param unknown $user
     * @return array
     */
    public function map($portfolio): array
    {
        return [

        ];
    }

    /**
     * The columns
     * @return array
     */
    public function headings(): array
    {
        return [

        ];
    }
}
