<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\LeaderBonusBreakdownQuery as BaseQuery;

class LeaderBonusBreakdownQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'user_id' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payouts',
            'column'    => 'user_id',
        ],
        'payout_date' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payouts',
            'column'    => 'payout_date',
        ],
        'is_same_rank' => [
            'filter'    => 'boolean',
            'table'     => 'leader_bonus_payout_breakdowns',
            'column'    => 'is_same_rank',
        ],
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'leader_bonus_payout_breakdowns',
            'column'    => 'created_at',
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'contributing_users',
            'column' => 'member_id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }

    /**
     * @param mixed $payout
     * @return array
     */
    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->member_id,
            $payout->percentage_display . '%',
            amount_format(bcmul($payout->amount, $payout->exchange_rate, credit_precision()), credit_precision()),
            __('s_ranks.' . $payout->rank_name)
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            __('m_report_leader_breakdown.contribution date'),
            __('m_report_leader_breakdown.contributor member id'),
            __('m_report_leader_breakdown.percentage'),
            __('m_report_leader_breakdown.amount'),
            __('m_report_leader_breakdown.receiver member rank')
        ];
    }
}
