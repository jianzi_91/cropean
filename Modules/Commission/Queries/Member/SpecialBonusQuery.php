<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\SpecialBonusQuery as BaseQuery;

class SpecialBonusQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'special_bonus_payouts',
            'column' => 'payout_date'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', auth()->user()->id);
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            amount_format($payout->issued_amount, credit_precision()),
            amount_format(bcmul($payout->percentage, 100), credit_precision()) . '%'
        ];
    }

    public function headings(): array
    {
        return [
            __('m_report_special_bonus.date'),
            __('m_report_special_bonus.issued payout amount'),
            __('m_report_special_bonus.payout rate')
        ];
    }
}
