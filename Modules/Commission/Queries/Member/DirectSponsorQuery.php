<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\DirectSponsorQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;

class DirectSponsorQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'direct_sponsor_payouts',
            'column' => 'payout_date'
        ],
        'member_id' => [
            'filter' => 'like',
            'table'  => 'contributing_users',
            'column' => 'member_id',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('receiving_users.id', auth()->user()->id)->select([
            'direct_sponsor_payouts.payout_date',
            'direct_sponsor_payouts.percentage',
            'direct_sponsor_payouts.amount as payout_amount',
            'contributing_users.member_id as contributed_by',
            'credit_conversions.converted_amount',
        ]);
    }

    public function map($payout): array
    {
        $bankCreditTypeContract = resolve(BankCreditTypeContract::class);

        return [
            $payout->payout_date,
            $payout->contributed_by,
            $payout->percentage_display,
            amount_format($payout->payout_amount, credit_precision($bankCreditTypeContract::PROFIT_FUND_CREDIT)),
            amount_format($payout->converted_amount, credit_precision($bankCreditTypeContract::CAMPAIGN_FUND_CREDIT)),
        ];
    }

    public function headings(): array
    {
        return [
            __('m_direct_sponsor_index.date'),
            __('m_direct_sponsor_index.contributed by'),
            __('m_direct_sponsor_index.direct sponsor %'),
            __('m_direct_sponsor_index.funded in amount'),
            __('m_direct_sponsor_index.payout amount'),
        ];
    }
}
