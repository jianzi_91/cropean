<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\SpecialBonusBreakdownQuery as BaseQuery;

class SpecialBonusBreakdownQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'special_bonus_payout_breakdowns',
            'column' => 'created_at'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'contributing_users',
            'column' => 'member_id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
    }

    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->member_id,
            amount_format($payout->percentage_display),
            amount_format($payout->amount, credit_precision()),
        ];
    }

    public function headings(): array
    {
        return [
            __('m_special_breakdown_report.date time'),
            __('m_special_breakdown_report.contributed by member id'),
            __('m_special_breakdown_report.payout rate - %'),
            __('m_special_breakdown_report.amount - cash credits'),
        ];
    }
}
