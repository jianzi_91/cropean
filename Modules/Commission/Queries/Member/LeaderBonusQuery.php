<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\LeaderBonusQuery as BaseQuery;

class LeaderBonusQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'leader_bonus_payouts',
            'column' => 'payout_date'
        ],
        'type' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payout_credits',
            'column'    => 'bank_transaction_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    public function beforeBuild()
    {
        return $this->builder->where('leader_bonus_payouts.user_id', auth()->id());
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            __('s_bank_transaction_types.' . $payout->type),
            amount_format($payout->leader_bonus_amount, credit_precision())
        ];
    }

    public function headings(): array
    {
        return [
            __('m_report_leader.date'),
            __('m_report_leader.type'),
            __('m_report_leader.payout amount')
        ];
    }
}
