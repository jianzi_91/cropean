<?php

namespace Modules\Commission\Queries\Member;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\GoldmineQuery as BaseQuery;

class GoldmineQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'goldmine_bonus_payouts',
            'column' => 'created_at'
        ],
        'goldmine_rank_id' => [
            'filter' => 'equal',
            'table'  => 'goldmine_bonus_payouts',
            'column' => 'goldmine_rank_id'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('receiving_users.id', auth()->user()->id);
    }

    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->contributors,
            amount_format($payout->issued_amount, credit_precision()),
            $payout->goldmineRank->name,

        ];
    }

    public function headings(): array
    {
        return [
            __('m_report_goldmine.date'),
            __('m_report_goldmine.number of contributors'),
            __('m_report_goldmine.issued payout amount'),
            __('m_report_goldmine.rank'),
        ];
    }
}
