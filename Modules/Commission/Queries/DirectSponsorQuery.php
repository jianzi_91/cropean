<?php

namespace Modules\Commission\Queries;

use Modules\Commission\Models\DirectSponsorPayout;
use QueryBuilder\QueryBuilder;

class DirectSponsorQuery extends QueryBuilder
{
    protected $paginate = false;

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        return DirectSponsorPayout::join('direct_sponsor_payout_breakdowns', 'direct_sponsor_payouts.id', '=', 'direct_sponsor_payout_breakdowns.direct_sponsor_payout_id')
            ->join('users as receiving_users', 'receiving_users.id', '=', 'direct_sponsor_payouts.user_id')
            ->join('users as contributing_users', 'contributing_users.id', '=', 'direct_sponsor_payout_breakdowns.user_id')
            ->whereNull('direct_sponsor_payouts.deleted_at')
            ->orderBy('direct_sponsor_payouts.created_at', 'desc');
    }
}
