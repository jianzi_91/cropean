<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Commission\Models\GoldmineBonusPayout;
use QueryBuilder\QueryBuilder;

class GoldmineQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = GoldmineBonusPayout::join('goldmine_bonus_payout_breakdowns', 'goldmine_bonus_payout_breakdowns.goldmine_bonus_payout_id', 'goldmine_bonus_payouts.id')
            ->join('goldmine_bonus_payout_credits', 'goldmine_bonus_payout_credits.goldmine_bonus_payout_id', 'goldmine_bonus_payouts.id')
            ->join('users as receiving_users', 'receiving_users.id', 'goldmine_bonus_payouts.user_id')
            ->with('goldmineRank')
            ->selectRaw('receiving_users.member_id, receiving_users.email, receiving_users.name as receiver_name, 
            count(goldmine_bonus_payout_breakdowns.id) as contributors,
            goldmine_bonus_payout_credits.issued_amount, goldmine_bonus_payouts.*')
            ->groupBy('goldmine_bonus_payouts.id')
            ->orderBy('goldmine_bonus_payouts.created_at', 'desc');

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
