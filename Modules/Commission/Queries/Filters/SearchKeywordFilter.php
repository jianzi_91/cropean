<?php

namespace Modules\Portfolio\Queries\Filters;

use QueryBuilder\QueryFilter;

class SearchKeywordFilter extends QueryFilter
{
    /**
     * Add filter to sql statement
     *
     * @param Illuminate\Database\Eloquent\Builder | Illuminate\Database\Query\Builder $builder
     * @return Illuminate\Database\Query\Builder
     * @throws \Exception
     */
    public function filter($builder)
    {
        $elName = $this->name;

        if (!$this->parameter->has($elName)
            || !$elName
            || !$this->parameter->get($elName)) {
            return $builder;
        }

        if ($this->alias) {
            if (false !== strpos($this->alias, '.')) {
                [$table, $colName] = explode('.', $this->alias);
            } else {
                $colName = $this->alias;
                $table   = $this->getTableName($builder);
            }
        } else {
            $table   = $this->getTableName($builder);
            $colName = $elName;
        }

        if (!$table) {
            throw new \Exception('Unknown instance of builder');
        }

        $value  = stripslashes($this->parameter->get($elName));
        $column = $table . '.' . $colName;

        return $builder->where(function ($builder) use ($table, $value) {
            $builder
                ->where('portfolios.id', 'like', '%'.$value.'%')
                ->orWhere('portfolio_i18ns.title', 'like', '%'.$value.'%')
                ->orWhere('portfolio_i18ns.description', 'like', '%'.$value.'%');
        });
    }
}
