<?php

namespace Modules\Commission\Queries;

use Illuminate\Support\Facades\DB;
use QueryBuilder\QueryBuilder;
use Modules\Commission\Models\SponsorCommission;

class GoldmineSponsorQuery extends QueryBuilder
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date',
            'table'  => 'sponsor_commissions',
            'column' => 'payout_date'
        ],
    ];

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $select = [
                DB::raw('sum(amount) total'),
                'sponsor_commissions.user_id',
            ];
        $query = SponsorCommission::join(
                    'user_status_histories',
                    function ($join) {
                        $join->on('sponsor_commissions.user_id', '=', 'user_status_histories.user_id')
                            ->where('user_status_histories.is_current', 1)
                            ->whereIn('user_status_histories.user_status_id', [1, 2])
                            ->whereNull('user_status_histories.deleted_at');
                    }
                )
                ->select($select)
                ->groupBy('sponsor_commissions.user_id');

        return $query;
    }
}
