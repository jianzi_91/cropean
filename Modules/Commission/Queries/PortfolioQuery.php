<?php

namespace Modules\Portfolio\Queries;

use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Portfolio\Models\Portfolio;
use QueryBuilder\QueryBuilder;

class PortfolioQuery extends QueryBuilder implements FromCollection
{
    protected $locale = 'cn';

    protected $paginate = false;

    /**
     * {@inheritDoc}
     *
     * @see \Modules\Base\Queries\QueryBuilder::query()
     */
    public function query()
    {
        $languageId   = get_translator_language_id($this->locale);
        $languageCode = $this->locale;

        $query = Portfolio::join('portfolio_i18ns', 'portfolio_i18ns.portfolio_id', 'portfolios.id')
            ->join('portfolio_statuses', 'portfolio_statuses.id', 'portfolios.portfolio_status_id')
            ->leftJoin(
                'portfolio_attachments',
                function ($join) use ($languageId) {
                    $join->on('portfolio_attachments.portfolio_id', '=', 'portfolios.id')
//                        ->where('portfolio_attachments.translator_language_id', $languageId)
                        ->where('portfolio_attachments.is_featured', 1)
                        ->whereNull('portfolio_attachments.deleted_at');
                }
            )
            ->leftJoin('portfolio_investments', 'portfolios.id', '=', 'portfolio_investments.portfolio_id')
            ->with('thumbnail')
            ->whereNull('portfolios.deleted_at')
            ->groupBy('portfolios.id')
            ->orderBy('portfolios.portfolio_status_id', 'asc');

        return $query;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        $this->builder = $this->query();
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->paginate ? $this->paginate() : $this->get();
    }

    public function getPaginate()
    {
        return $this->paginate;
    }
}
