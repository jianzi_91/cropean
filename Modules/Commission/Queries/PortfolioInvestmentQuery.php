<?php

namespace Modules\Portfolio\Queries;

use Modules\Portfolio\Models\PortfolioInvestment;
use QueryBuilder\QueryBuilder;

class PortfolioInvestmentQuery extends QueryBuilder
{
    protected $paginate = false;

    /**
     * The filters
     * @var array
     */
    protected $filters = [];

    /**
     * Generates the base query
     * @return Builder
     */
    public function query()
    {
        return PortfolioInvestment::join('users', 'users.id', '=', 'portfolio_investments.user_id')
            ->join('portfolios', 'portfolios.id', '=', 'portfolio_investments.portfolio_id')
            ->whereNull('portfolio_investments.deleted_at');
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process before building the query here
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }
}
