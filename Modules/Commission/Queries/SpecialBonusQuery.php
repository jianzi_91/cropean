<?php

namespace Modules\Commission\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Commission\Models\SpecialBonusPayout;
use QueryBuilder\QueryBuilder;

class SpecialBonusQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = SpecialBonusPayout::join('special_bonus_payout_credits', 'special_bonus_payout_credits.special_bonus_payout_id', 'special_bonus_payouts.id')
            ->join('users as receiving_users', 'receiving_users.id', 'special_bonus_payouts.user_id')
            ->orderBy('special_bonus_payouts.payout_date', 'desc')
            ->select([
                'special_bonus_payouts.*',
                'special_bonus_payout_credits.transaction_code',
                'special_bonus_payout_credits.issued_amount as issued_amount',
                'receiving_users.email',
                'receiving_users.member_id',
            ]);

        return $query;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Maatwebsite\Excel\Concerns\FromCollection::collection()
     */
    public function collection()
    {
        return $this->get();
    }
}
