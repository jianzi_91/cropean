<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\LeaderBonusBreakdownQuery as BaseQuery;

class LeaderBonusBreakdownQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'user_id' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payouts',
            'column'    => 'user_id',
        ],
        'payout_date' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payouts',
            'column'    => 'payout_date',
        ],
        'is_same_rank' => [
            'filter'    => 'boolean',
            'table'     => 'leader_bonus_payout_breakdowns',
            'column'    => 'is_same_rank',
        ],
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'leader_bonus_payout_breakdowns',
            'column'    => 'created_at',
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'contributing_users',
            'column' => 'member_id'
        ],
        'email' => [
            'filter' => 'text',
            'table'  => 'contributing_users',
            'column' => 'email'
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->created_at->format('Y-m-d'),
            $payout->member_id,
            $payout->email,
            $payout->percentage_display . '%',
            amount_format(bcmul($payout->amount, $payout->exchange_rate, credit_precision()), credit_precision()),
            __('s_ranks.' . $payout->rank_name)
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_leader_breakdown.contribution date'),
            __('a_report_leader_breakdown.contributor member id'),
            __('a_report_leader_breakdown.contributor email'),
            __('a_report_leader_breakdown.percentage'),
            __('a_report_leader_breakdown.amount'),
            __('a_report_leader_breakdown.receiver member rank')
        ];
    }
}
