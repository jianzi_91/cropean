<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\LeaderBonusQuery as BaseQuery;

class LeaderBonusQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'leader_bonus_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'member_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'type' => [
            'filter'    => 'equal',
            'table'     => 'leader_bonus_payout_credits',
            'column'    => 'bank_transaction_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    public function beforeBuild()
    {
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->receiver_name,
            $payout->member_id,
            $payout->email,
            __('s_bank_transaction_types.' . $payout->type),
            amount_format($payout->leader_bonus_amount, credit_precision())
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_leader.date'),
            __('a_report_leader.name'),
            __('a_report_leader.member id'),
            __('a_report_leader.email'),
            __('a_report_leader.type'),
            __('a_report_leader.issued payout amount')
        ];
    }
}
