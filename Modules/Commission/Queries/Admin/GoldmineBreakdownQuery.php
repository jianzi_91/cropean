<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\GoldmineBreakdownQuery as BaseQuery;

class GoldmineBreakdownQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'goldmine_bonus_payout_id' => [
            'filter' => 'equal',
            'table'  => 'goldmine_bonus_payout_breakdowns',
            'column' => 'goldmine_bonus_payout_id'
        ],
        'date' => [
            'filter' => 'date_range',
            'table'  => 'goldmine_bonus_payout_breakdowns',
            'column' => 'created_at'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'contributing_users',
            'column' => 'member_id'
        ],
        'level' => [
            'filter' => 'equal',
            'table'  => 'goldmine_bonus_payout_breakdowns',
            'column' => 'level'
        ],
    ];

    /**
     * Adhoc processes after build
     */
    public function afterBuild()
    {
        return $this->builder->orderBy('level');
    }

    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->member_id,
            $payout->level,
            amount_format(bcmul($payout->percentage, 100, credit_precision())) . '%',
            amount_format($payout->amount, credit_precision())
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_goldmine_breakdown.date'),
            __('a_report_goldmine_breakdown.contributed member id'),
            __('a_report_goldmine_breakdown.level'),
            __('a_report_goldmine_breakdown.payout percentage'),
            __('a_report_goldmine_breakdown.amount')
        ];
    }
}
