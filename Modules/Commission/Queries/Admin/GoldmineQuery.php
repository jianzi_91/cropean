<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\GoldmineQuery as BaseQuery;

class GoldmineQuery extends BaseQuery implements WithHeadings, WithMapping, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'date' => [
            'filter'    => 'date_range',
            'table'     => 'goldmine_bonus_payouts',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'name' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'name',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'receiving_users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'goldmine_rank_id' => [
            'filter'    => 'equal',
            'table'     => 'goldmine_bonus_payouts',
            'column'    => 'goldmine_rank_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->created_at,
            $payout->receiver_name,
            $payout->member_id,
            $payout->email,
            $payout->contributors,
            amount_format($payout->amount, credit_precision()),
            $payout->goldmineRank->name,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_goldmine.date time'),
            __('a_report_goldmine.name'),
            __('a_report_goldmine.member id'),
            __('a_report_goldmine.email'),
            __('a_report_goldmine.number of contributors'),
            __('a_report_goldmine.issued payout amount'),
            __('a_report_goldmine.rank'),
        ];
    }
}
