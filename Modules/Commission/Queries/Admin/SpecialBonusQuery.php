<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\SpecialBonusQuery as BaseQuery;

class SpecialBonusQuery extends BaseQuery implements WithMapping, WithHeadings, ShouldAutoSize
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter'    => 'date_range',
            'table'     => 'special_bonus_payouts',
            'column'    => 'payout_date',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'text',
            'table'     => 'receiving_users',
            'column'    => 'member_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->member_id,
            $payout->email,
            amount_format($payout->issued_amount ?? 0, credit_precision()),
            amount_format(bcmul($payout->percentage, 100), credit_precision()) . '%',
        ];
    }

    public function headings(): array
    {
        return [
            __('a_report_special.date'),
            __('a_report_special.member id'),
            __('a_report_special.email'),
            __('a_report_special.issued payout amount'),
            __('a_report_special.payout rate'),
       ];
    }
}
