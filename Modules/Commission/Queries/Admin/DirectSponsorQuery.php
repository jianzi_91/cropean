<?php

namespace Modules\Commission\Queries\Admin;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Commission\Queries\DirectSponsorQuery as BaseQuery;

class DirectSponsorQuery extends BaseQuery implements WithHeadings, WithMapping
{
    /**
     * The filters
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'sponsor_commissions',
            'column' => 'payout_date'
        ],
        'member_id' => [
            'filter' => 'like',
            'table'  => 'contributing_users',
            'column' => 'member_id',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        $this->builder->select([
            'direct_sponsor_payouts.payout_date',
            'direct_sponsor_payouts.percentage',
            'direct_sponsor_payouts.amount as payout_amount',
            'receiving_users.member_id as member_id',
            'contributing_users.member_id as contributed_by',
        ]);
    }

    /**
     * Adhoc process after build
     */
    public function afterBuild()
    {
        // Do extra process after building the query here
    }

    public function map($payout): array
    {
        return [
            $payout->payout_date,
            $payout->member_id,
            $payout->contributed_by,
            $payout->percentage,
            amount_format($payout->converted_amount, 2),
            amount_format($payout->payout_amount, 2),
        ];
    }

    public function headings(): array
    {
        return [
            __('a_direct_sponsor.date'),
            __('a_direct_sponsor.member id'),
            __('a_direct_sponsor.contributed by'),
            __('a_direct_sponsor.direct sponsor %'),
            __('a_direct_sponsor.funded in amount'),
            __('a_direct_sponsor.payout amount'),
        ];
    }
}
