<?php

namespace Modules\Commission\Queries\Admin;

use Modules\Commission\Queries\SponsorQuery as BaseQuery;

class SponsorQuery extends BaseQuery
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'payout_date' => [
            'filter' => 'date_range',
            'table'  => 'sponsor_commissions',
            'column' => 'payout_date'
        ],
        'received_by_username' => [
            'filter' => 'like',
            'table'  => 'receiving_users',
            'column' => 'username',
        ],
        'contributed_by_username' => [
            'filter' => 'like',
            'table'  => 'contributing_users',
            'column' => 'username',
        ],
    ];

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        // Do extra process befor building the query here
    }
}
