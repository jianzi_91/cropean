<?php

namespace Modules\Commission\Repositories;

use Affiliate\Commission\Goldmine;
use Carbon\Carbon;
use Modules\Commission\Models\GoldmineBonusPayout;
use Modules\Commission\Models\GoldmineBonusPayoutBreakdown;
use Modules\Commission\Models\GoldmineBonusPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;

class GoldmineBonusRepository extends Goldmine
{
    use CanPayoutCommission;

    protected $creditTransactionType        = BankTransactionTypeContract::GOLDMINE_BONUS;
    protected $bonus_payout_id              = 'goldmine_bonus_payout_id';
    protected $data                         = [];
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $payoutDate;
    protected $goldmineRanks;
    protected $userGoldmineRankLevels = [];
    protected $skip                   = true;

    public function __construct(array $tree = null, array $amounts = [], array $levels = [])
    {
        parent::__construct($tree, $amounts, $levels);
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function setUserGoldmineRankLevel($goldmineRanks, $userGoldmineRankLevels, $userGoldmineRankHistorySnap, $userGoldmineLevelHistorySnap)
    {
        $this->goldmineRanks          = $goldmineRanks;
        $this->userGoldmineRankLevels = $userGoldmineRankLevels;

        $this->setTreeMaxLevel($userGoldmineRankHistorySnap, $userGoldmineLevelHistorySnap);
    }

    public function calculate($precision = 2)
    {
        $this->commissions = [];

        if ($this->tree && $this->amounts) {
            foreach ($this->amounts as $userId => $amount) {
                $node = $this->getNodeByUser($userId);
                //nonEligibleContributionUsers didnt contribute to upline
                //no rank user also no contribute to upline
                if ($node && $node->maxLevel > 0 && !in_array($userId, $this->nonEligibleContributionUsers)) {
                    # set skipped level and get all uplines included it own self
                    $skippedLevel = 0;
                    $uplines      = $this->getUplines($node, $this->withSelf);

                    foreach ($uplines as $upline) {
                        # get upline user id set level
                        $uplineId = $upline->getUserId();
                        $maxLevel = $upline->maxLevel;
                        $current  = isset($this->commissions[$uplineId]) ? $this->commissions[$uplineId] : $upline;

                        if ($this->skip) {
                            # if there have skip (compression) in this project
                            # nonEligibleReceiveUsers terminated users need to compressed
                            # no rank user also need to be compressed
                            if ($maxLevel <= 0 || in_array($uplineId, $this->nonEligibleReceiveUsers)) {
                                $skippedLevel++;
                                continue;
                            }
                            $level = $current->getLevelFromNode($node, ($skippedLevel * -1));
                        } else {
                            # will keep execute this line
                            $level = $current->getLevelFromNode($node);
                        }
                        $level = $this->withSelf ? $level + 1 : $level;

                        # get goldmine level percentage rates
                        $percentage = isset($this->levelPercentage[$level]) ? $this->levelPercentage[$level] : 0;

                        if ($percentage && ($maxLevel > 0 && $maxLevel >= $level) || $maxLevel === null) {
                            $commission     = bcmul($amount, $percentage, $precision);
                            $computedAmount = $commission;
                            $cap            = $this->getCapAmount($current->getUserId());
                            $hitCap         = false;

                            # calculate total earned goldmine
                            if ($cap) {
                                $totalEarnings = bcadd($current->getCommission(), $this->getTotalEarnings($uplineId), $precision);
                                $goldmineLeft  = bcsub($cap, $totalEarnings, $precision);

                                if ($goldmineLeft > 0) {
                                    $newCommision = bcsub($cap, $totalEarnings, $precision);
                                    if ($newCommision <= $commission) {
                                        $commission = $newCommision;
                                        $hitCap     = true;
                                    }
                                } else {
                                    $commission = 0;
                                    $hitCap     = true;
                                }
                            } #end id cap
                            if ($commission >= 0) {
                                // replicate node
                                $breakdown = $node->clone();
                                $breakdown->setLevel($level)
                                    ->setCommission($commission)
                                    ->setComputedCommission($computedAmount)
                                    ->setAmount($amount)
                                    ->setPercentage($percentage);

                                if ($hitCap) {
                                    $current->setHitCap(true);
                                    $breakdown->setHitCap(true);
                                }
                                $this->commissions[$current->getUserId()] = $current->addBreakdown($breakdown);
                            } #end if commission > 0
                        } #end if have goldmine level rate and max level over 0 and others conditions
                    } #end foreach uplines
                } #end if node and non terminated user
            } #end foreach user id and amount
        } #end if have sponsor tree and amount of direct downlines

        $this->insertCommissionBreakdown();

        return $this;
    }

    public function insertCommissionBreakdown()
    {
        #insert payout
        $payouts     = [];
        $commissions = $this->getCommission();
        foreach ($commissions as $userId => $commission) {
            $payouts[] = [
                'user_id'          => $userId,
                'amount'           => $commission->getCommission(),
                'computed_amount'  => $commission->getCommission(),
                'created_at'       => Carbon::parse($this->payoutDate),
                'updated_at'       => Carbon::parse($this->payoutDate),
                'payout_date'      => Carbon::parse($this->payoutDate),
                'goldmine_level'   => $this->tree[$userId]->maxLevel ?? null,
                'goldmine_rank_id' => $this->userGoldmineRankLevels[$userId]['goldmine_rank_id'] ?? null,
            ];
        }

        # insert payout
        if ($payouts) {
            foreach (array_chunk($payouts, 1000) as $payout) {
                GoldmineBonusPayout::insert($payout);
            }
        }

        #get payout
        $this->payouts = GoldmineBonusPayout::whereBetween('payout_date', [Carbon::parse($this->payoutDate)->startOfDay(), Carbon::parse($this->payoutDate)->endOfDay()])
            ->get();

        #Insert into breakdowns
        $breakdownData = [];
        foreach ($this->payouts as $payout) {
            if (isset($commissions[$payout->user_id])) {
                $breakDowns = $commissions[$payout->user_id]->getBreakdown();
                foreach ($breakDowns as $breakdown) {
                    $breakdownData[] = [
                        'user_id'              => $breakdown->getUserId(),
                        'amount'               => $breakdown->getCommission(),
                        'percentage'           => $breakdown->getPercentage(),
                        'level'                => $breakdown->getLevel(),
                        $this->bonus_payout_id => $payout->id,
                        'created_at'           => Carbon::parse($this->payoutDate),
                        'updated_at'           => Carbon::parse($this->payoutDate),
                    ];
                }
            }
        }

        if ($breakdownData) {
            foreach (array_chunk($breakdownData, 1000) as $breakdown) {
                GoldmineBonusPayoutBreakdown::insert($breakdown);
            }
        }
    }

    private function setTreeMaxLevel($userGoldmineRankHistorySnap, $userGoldmineLevelHistorySnap)
    {
        foreach ($this->userGoldmineRankLevels as $userId => $goldmineRankLevel) {
            if (isset($this->tree[$userId]) && ($goldmineRankLevel['goldmine_rank_id'] > 0 || $goldmineRankLevel['goldmine_level'] > 0)) {
                //check and assign goldmine level
                $userGoldmineRank  = $userGoldmineRankHistorySnap[$userId] ?? null;
                $userGoldmineLevel = $userGoldmineLevelHistorySnap[$userId] ?? null;
                $maxLevel          = 0;

                if ($userGoldmineLevel && $userGoldmineLevel->is_locked && $userGoldmineRank && $userGoldmineRank->is_locked) {
                    //goldmine level take precedence
                    $maxLevel = $userGoldmineLevel->level;
                } elseif ($userGoldmineLevel && $userGoldmineLevel->is_locked) {
                    //goldmine level take precedence
                    $maxLevel = $userGoldmineLevel->level;
                } elseif ($userGoldmineRank && $userGoldmineRank->is_locked) {
                    //goldmine rank level take precedence
                    $maxLevel = $this->goldmineRanks[$userGoldmineRank->goldmine_rank_id] ?? 0;
                } else {
                    if ($userGoldmineLevel && $userGoldmineRank) {
                        //goldmine check which one higher take precedence
                        $userGoldmineRankLevel = $this->goldmineRanks[$userGoldmineRank->goldmine_rank_id] ?? 0;
                        if ($userGoldmineLevel->level >= $userGoldmineRankLevel) {
                            $maxLevel = $userGoldmineLevel->level;
                        } else {
                            $maxLevel = $userGoldmineRankLevel;
                        }
                    } elseif ($userGoldmineLevel) {
                        $maxLevel = $userGoldmineLevel->level;
                    } elseif ($userGoldmineRank) {
                        $maxLevel = $this->goldmineRanks[$userGoldmineRank->goldmine_rank_id] ?? 0;
                    }
                }

                $this->tree[$userId]->maxLevel = $maxLevel;
            }
        }
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            GoldmineBonusPayoutCredit::insert($record);
        }
    }
}
