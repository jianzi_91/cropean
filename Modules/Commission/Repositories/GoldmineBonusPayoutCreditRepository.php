<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\GoldmineBonusPayoutCreditContract;
use Modules\Commission\Models\GoldmineBonusPayoutCredit;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class GoldmineBonusPayoutCreditRepository extends Repository implements GoldmineBonusPayoutCreditContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param GoldmineBonusPayoutCredit $model
     */
    public function __construct(GoldmineBonusPayoutCredit $model)
    {
        parent::__construct($model);
    }
}
