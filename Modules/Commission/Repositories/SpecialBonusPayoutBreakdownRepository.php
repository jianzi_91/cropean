<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\SpecialBonusPayoutBreakdownContract;
use Modules\Commission\Models\SpecialBonusPayoutBreakdown;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SpecialBonusPayoutBreakdownRepository extends Repository implements SpecialBonusPayoutBreakdownContract
{
    use HasCrud, HasBulkAction;

    public function __construct(SpecialBonusPayoutBreakdown $model)
    {
        parent::__construct($model);
    }
}
