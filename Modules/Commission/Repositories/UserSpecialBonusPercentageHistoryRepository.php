<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\UserSpecialBonusPercentageHistoryContract;
use Modules\Commission\Models\UserSpecialBonusPercentageHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserSpecialBonusPercentageHistoryRepository extends Repository implements UserSpecialBonusPercentageHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserSpecialBonusPercentageHistory $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
