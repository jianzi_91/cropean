<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\SpecialBonusPayoutCreditContract;
use Modules\Commission\Models\SpecialBonusPayoutCredit;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SpecialBonusPayoutCreditRepository extends Repository implements SpecialBonusPayoutCreditContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param SpecialBonusPayoutCredit $model
     */
    public function __construct(SpecialBonusPayoutCredit $model)
    {
        parent::__construct($model);
    }
}
