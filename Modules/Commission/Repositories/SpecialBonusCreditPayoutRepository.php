<?php

namespace Modules\Commission\Repositories;

use Carbon\Carbon;
use Modules\Commission\Models\SpecialBonusPayout;
use Modules\Commission\Models\SpecialBonusPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Plus65\Base\Repositories\Repository;

class SpecialBonusCreditPayoutRepository extends Repository
{
    use CanPayoutCommission;

    protected $creditTransactionType = BankTransactionTypeContract::SPECIAL_BONUS;
    protected $bonus_payout_id       = 'special_bonus_payout_id';
    protected $payouts               = [];
    protected $creditType;
    protected $creditRate;
    protected $data;
    protected $payoutDate;

    /**
     * Class constructor.
     * @param $payouts
     */
    public function __construct($payouts)
    {
        parent::__construct();
        $this->payouts = $payouts;
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function setPayoutCredit($creditType)
    {
        $this->creditType = $creditType;
    }

    public function setPayoutCreditRate($creditRate)
    {
        $this->creditRate = $creditRate;
    }

    public function payout()
    {
        $this->initPayoutCommission($this->creditType, $this->creditRate);

        if (!$this->payouts->isEmpty()) {
            foreach ($this->payouts as $payout) {
                $calculatedAmount = $payout->amounts;
                $payoutCredits    = $this->calculateAllCommCredits($payout->user_id, $calculatedAmount, $this->creditTransactionTypeId, $this->payoutDate);

                $specialBonusPayouts = SpecialBonusPayout::whereIn('id', explode(',', $payout->special_bonus_ids))
                    ->pluck('amount', 'id')
                    ->toArray();

                foreach ($specialBonusPayouts as $specialBonusPayoutId => $amount) {
                    foreach ($payoutCredits as $bankAccountStatement) {
                        $this->data[] = [
                            'created_at'           => Carbon::parse($this->payoutDate),
                            'updated_at'           => Carbon::parse($this->payoutDate),
                            $this->bonus_payout_id => $specialBonusPayoutId,
                            'transaction_code'     => $bankAccountStatement['transaction_code'],
                            'percentage'           => $bankAccountStatement['percentage'],
                            'exchange_rate'        => $bankAccountStatement['exchange_rate'],
                            'amount'               => $amount,
                            'issued_amount'        => $bankAccountStatement['issued_amount'],
                            'bank_credit_type_id'  => $bankAccountStatement['bank_credit_type_id'],
                        ];
                    } #end foreach
                } #end foreach special bonus payouts
            } #end foreach commissions

            #payout in bank account statement
            $this->saveCommData();

            #payout credit
            $this->saveCommissionCredits();
        } #end if commissions
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            SpecialBonusPayoutCredit::insert($record);
        }
    }
}
