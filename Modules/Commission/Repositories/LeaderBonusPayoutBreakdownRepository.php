<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\LeaderBonusPayoutBreakdownContract;
use Modules\Commission\Models\LeaderBonusPayoutBreakdown;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class LeaderBonusPayoutBreakdownRepository extends Repository implements LeaderBonusPayoutBreakdownContract
{
    use HasBulkAction, HasCrud;

    /**
     * LeaderBonusPayoutBreakdownRepository constructor.
     * @param LeaderBonusPayoutBreakdown $model
     */
    public function __construct(LeaderBonusPayoutBreakdown $model)
    {
        parent::__construct($model);
    }
}
