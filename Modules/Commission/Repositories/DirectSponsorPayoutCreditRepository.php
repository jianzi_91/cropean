<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\DirectSponsorPayoutCreditContract;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DirectSponsorPayoutCreditRepository extends Repository implements DirectSponsorPayoutCreditContract
{
    use HasBulkAction, HasCrud;
    // implement functions here
}
