<?php

namespace Modules\Commission\Repositories;

use Carbon\Carbon;
use Modules\Commission\Contracts\DirectSponsorPayoutBreakdownContract;
use Modules\Commission\Contracts\DirectSponsorPayoutContract;
use Modules\Commission\Contracts\DirectSponsorPayoutCreditContract;
use Modules\Commission\Models\DirectSponsorPayout;
use Modules\Commission\Models\DirectSponsorPayoutBreakdown;
use Modules\Commission\Models\DirectSponsorPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Core\Traits\HasBulkAction;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Models\CreditConversionTransaction;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DirectSponsorPayoutRepository extends Repository implements DirectSponsorPayoutContract
{
    use HasCrud, HasBulkAction, CanPayoutCommission;

    protected $bankTransactionTypeRepo;
    protected $directSponsorPayoutCreditRepo;
    protected $directSponsorPayoutBreakdownRepo;
    protected $payoutDate;
    protected $amounts;
    protected $payouts                      = [];
    protected $breakdowns                   = [];
    protected $data                         = [];
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $bonus_payout_id              = 'direct_sponsor_payout_id';
    protected $creditTransactionType        = BankTransactionTypeContract::SPONSOR_BONUS;

    public function __construct(
        CreditConversionTransaction $model,
        BankTransactionTypeContract $bankTransactionTypeContract,
        DirectSponsorPayoutCreditContract $directSponsorPayoutCreditContract,
        DirectSponsorPayoutBreakdownContract $breakdownContract
    ) {
        parent::__construct($model);

        $this->bankTransactionTypeRepo          = $bankTransactionTypeContract;
        $this->directSponsorPayoutCreditRepo    = $directSponsorPayoutCreditContract;
        $this->directSponsorPayoutBreakdownRepo = $breakdownContract;
    }

    /**
     * @param int $precision
     * @return mixed|void
     */
    public function calculate($sales)
    {
        $percentage = setting('direct_sponsor_percentage');

        $this->amounts    = [];
        $this->breakdowns = [];

        foreach ($sales as $sale) {
            $amount = bcmul($sale['converted_amount'], $percentage, credit_precision());

            if (isset($this->amounts[$sale['sponsor_user_id']])) {
                $this->amounts[$sale['sponsor_user_id']] = bcadd($this->amounts[$sale['sponsor_user_id']], $amount, credit_precision());
            } else {
                $this->amounts[$sale['sponsor_user_id']] = $amount;
            }

            $this->breakdowns[$sale['sponsor_user_id']][$sale['user_id']] = $sale['converted_amount'];
        }

        $this->insertCommissionBreakdown();
    }

    /**
     * @param Carbon $payoutDate
     */
    public function setPayoutDate(Carbon $payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    /**
     * @return mixed|void
     */
    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function preparePayout()
    {
        $this->payout();
    }

    public function insertCommissionBreakdown()
    {
        $directSponsorPercentage = setting('direct_sponsor_percentage');

        #insert payout
        $payouts = [];
        foreach ($this->amounts as $sponsorUserId => $amount) {
            $payouts[] = [
                'user_id'         => $sponsorUserId,
                'amount'          => $amount,
                'computed_amount' => $amount,
                'created_at'      => Carbon::parse($this->payoutDate),
                'updated_at'      => Carbon::parse($this->payoutDate),
                'payout_date'     => Carbon::parse($this->payoutDate),
                'percentage'      => $directSponsorPercentage
            ];
        }

        # insert payout
        if ($payouts) {
            foreach (array_chunk($payouts, 1000) as $directSponsorPayout) {
                DirectSponsorPayout::insert($directSponsorPayout);
            }
        }

        #get payout
        $this->payouts = DirectSponsorPayout::whereBetween(
            'payout_date',
            [Carbon::parse($this->payoutDate)->startOfDay(), Carbon::parse($this->payoutDate)->endOfDay()]
        )->get();

        $payouts = $this->payouts->pluck('id', 'user_id');

        #Insert into breakdowns
        $breakdownData = [];
        foreach ($this->breakdowns as $sponsorUserId => $breakdowns) {
            foreach ($breakdowns as $userId => $breakdownAmount) {
                $breakdownAmountAfterPercentage = bcmul($breakdownAmount, $directSponsorPercentage, 2);

                $breakdownData[] = [
                    'user_id'              => $userId,
                    'amount'               => $breakdownAmountAfterPercentage,
                    'percentage'           => $directSponsorPercentage,
                    $this->bonus_payout_id => $payouts[$sponsorUserId],
                    'created_at'           => Carbon::parse($this->payoutDate),
                    'updated_at'           => Carbon::parse($this->payoutDate),
                ];
            }
        }

        if ($breakdownData) {
            foreach (array_chunk($breakdownData, 1000) as $breakdown) {
                DirectSponsorPayoutBreakdown::insert($breakdown);
            }
        }
    }

    /**
     * @return string
     */
    protected function getPayoutSchemeSettingName()
    {
        return 'direct_sponsor_payout_scheme';
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            DirectSponsorPayoutCredit::insert($record);
        }
    }
}
