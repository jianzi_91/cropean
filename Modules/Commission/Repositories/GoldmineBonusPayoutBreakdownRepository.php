<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\GoldmineBonusPayoutBreakdownContract;
use Modules\Commission\Models\GoldmineBonusPayoutBreakdown;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class GoldmineBonusPayoutBreakdownRepository extends Repository implements GoldmineBonusPayoutBreakdownContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param GoldmineBonusPayoutBreakdown $model
     */
    public function __construct(GoldmineBonusPayoutBreakdown $model)
    {
        parent::__construct($model);
    }
}
