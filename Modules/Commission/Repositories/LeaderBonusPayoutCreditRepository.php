<?php

namespace Modules\Commission\Repositories;

use Illuminate\Database\Eloquent\Model;
use Modules\Commission\Contracts\LeaderBonusPayoutCreditContract;
use Modules\Commission\Models\LeaderBonusPayoutCredit;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class LeaderBonusPayoutCreditRepository extends Repository implements LeaderBonusPayoutCreditContract
{
    use HasBulkAction, HasCrud;

    /**
     * LeaderBonusPayoutCreditRepository constructor.
     * @param LeaderBonusPayoutCredit $model
     */
    public function __construct(LeaderBonusPayoutCredit $model)
    {
        parent::__construct($model);
    }
}
