<?php

namespace Modules\Commission\Repositories;

use Carbon\Carbon;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Commission\Models\LeaderBonusPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Plus65\Base\Repositories\Repository;

class LeaderBonusCreditPayoutRepository extends Repository
{
    use CanPayoutCommission;

    protected $creditTransactionType;
    protected $bonus_payout_id = 'leader_bonus_payout_id';
    protected $payouts         = [];
    protected $creditType;
    protected $creditRate;
    protected $data;
    protected $payoutDate;

    /**
     * Class constructor.
     * @param $payouts
     * @param $payoutDate
     * @param $payoutRatioPercentage
     * @param $payoutCreditRates
     * @param $creditTransactionType
     */
    public function __construct($payouts, $payoutDate, $payoutRatioPercentage, $payoutCreditRates, $creditTransactionType)
    {
        parent::__construct();
        $this->creditTransactionType = $creditTransactionType;
        $this->payouts               = $payouts;
        $this->setPayoutDate($payoutDate);
        $this->setPayoutCredit($payoutRatioPercentage);
        $this->setPayoutCreditRate($payoutCreditRates);
    }

    public function payout()
    {
        $this->initPayoutCommission($this->creditType, $this->creditRate);

        if (!$this->payouts->isEmpty()) {
            foreach ($this->payouts as $payout) {
                $calculatedAmount = $payout->amounts;
                $payoutCredits    = $this->calculateAllCommCredits($payout->user_id, $calculatedAmount, $this->creditTransactionTypeId, $this->payoutDate);

                $leaderBonusPayouts = LeaderBonusPayout::whereIn('id', explode(',', $payout->leader_bonus_ids))
                    ->pluck('amount', 'id')
                    ->toArray();

                foreach ($leaderBonusPayouts as $leaderBonusPayoutId => $amount) {
                    foreach ($payoutCredits as $bankAccountStatement) {
                        $this->data[] = [
                            'created_at'               => Carbon::parse($this->payoutDate),
                            'updated_at'               => Carbon::parse($this->payoutDate),
                            $this->bonus_payout_id     => $leaderBonusPayoutId,
                            'transaction_code'         => $bankAccountStatement['transaction_code'],
                            'percentage'               => $bankAccountStatement['percentage'],
                            'exchange_rate'            => $bankAccountStatement['exchange_rate'],
                            'amount'                   => $amount,
                            'converted_amount'         => bcmul($amount, $bankAccountStatement['exchange_rate'], credit_precision()),
                            'issued_amount'            => $bankAccountStatement['issued_amount'],
                            'bank_credit_type_id'      => $bankAccountStatement['bank_credit_type_id'],
                            'bank_transaction_type_id' => $bankAccountStatement['bank_transaction_type_id'],
                        ];
                    } #end foreach
                } #end foreach special bonus payouts
            } #end foreach commissions

            #payout in bank account statement
            $this->saveCommData();

            #payout credit
            $this->saveCommissionCredits();
        } #end if commissions
    }

    private function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    private function setPayoutCredit($creditType)
    {
        $this->creditType = $creditType;
    }

    private function setPayoutCreditRate($creditRate)
    {
        $this->creditRate = $creditRate;
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            LeaderBonusPayoutCredit::insert($record);
        }
    }
}
