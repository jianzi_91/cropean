<?php

namespace Modules\Commission\Repositories\Concerns;

use Carbon\Carbon;
use Modules\Credit\Contracts\BankAccountContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Credit\Core\Generators\SimpleGenerator;
use Modules\Credit\Models\BankAccountStatement;
use Modules\ExchangeRate\Contracts\ConvertContract;
use Modules\ExchangeRate\Contracts\ExchangeRateContract;

trait CanPayoutCommission
{
    protected $creditTransactionTypeId;
    protected $creditTypes        = [];
    protected $creditTypeRates    = [];
    protected $userCommissions    = [];
    protected $creditTransactions = [];
    protected $bankAccounts       = [];
    protected $userBankAccounts   = [];
    protected $txnCodes           = [];

    /** @var ExchangeRateContract $exchangeRateContract */
    protected $exchangeRateContract;

    /** @var BankCreditTypeContract $bankCreditTypeContract */
    protected $bankCreditTypeContract;

    /** @var BankTransactionTypeContract $bankTransactionTypeContract */
    protected $bankTransactionTypeContract;

    protected $convertContract;

    public function initPayoutCommission($creditType = [BankCreditTypeContract::CASH_CREDIT => 1], $creditTypeRate = [BankCreditTypeContract::CASH_CREDIT => 1])
    {
        $this->bankCreditTypeContract      = resolve(BankCreditTypeContract::class);
        $this->bankTransactionTypeContract = resolve(BankTransactionTypeContract::class);
        $this->bankAccounts                = resolve(BankAccountContract::class);
        $this->exchangeRateContract        = resolve(ExchangeRateContract::class);
        $this->convertContract             = resolve(ConvertContract::class);

        $this->creditTransactionTypeId = $this->bankTransactionTypeContract->findBySlug($this->creditTransactionType)->id;

        $this->userBankAccounts = $this->bankAccounts
            ->all([], 0, ['id', 'reference_id'])
            ->pluck('id', 'reference_id');

        $this->creditTypes     = $creditType;
        $this->creditTypeRates = $creditTypeRate;
    }

    public function calculateAllCommCredits(int $userId, float $amount, $creditTransactionType = null, $payoutDate = null)
    {
        $data          = [];
        $payoutCredits = [];

        if ($userId && $amount) {
            $data['user_id'] = $userId;

            foreach ($this->creditTypes as $creditType => $percentage) {
                $rewardAmount    = bcmul($amount, $percentage, config('credit.precision.calculation', 20));
                $creditRate      = isset($this->creditTypeRates[$creditType]) ? $this->creditTypeRates[$creditType] : 1;
                $convertData     = ['to' => $creditType, 'rate' => $creditRate, 'amount' => $rewardAmount];
                $convertedAmount = $this->convertContract->convertToCredit($convertData);

                $data['amount']      = $convertedAmount['converted_amount'];
                $data['credit_type'] = $this->bankCreditTypeContract->findBySlug($creditType)->id;
                $commTransactionData = $this->persistCommData($data, $creditTransactionType, $payoutDate);

                $commTransactionData['percentage']    = $percentage;
                $commTransactionData['exchange_rate'] = $creditRate;
                $commTransactionData['amount']        = $rewardAmount;
                $commTransactionData['issued_amount'] = $convertedAmount['converted_amount'];
                $payoutCredits[]                      = $commTransactionData;
            }
        }
        return $payoutCredits;
    }

    public function persistCommData(array $data, $creditTransactionTypeId, $payoutDate)
    {
        $generator = new SimpleGenerator();
        do {
            $transactionCode = $generator->generate('TXN');
        } while (isset($this->txnCodes[$transactionCode]));

        $this->txnCodes[$transactionCode] = $transactionCode;

        $creditTransactionData = [
            'created_at'               => Carbon::now(),
            'updated_at'               => Carbon::now(),
            'transaction_date'         => $payoutDate,
            'transaction_code'         => $transactionCode,
            'bank_transaction_type_id' => $creditTransactionTypeId,
            'bank_credit_type_id'      => $data['credit_type'],
            'bank_account_id'          => $this->userBankAccounts[$data['user_id']],
            'done_by'                  => $this->userBankAccounts[$data['user_id']],
            'credit'                   => $data['amount'],
        ];
        $this->creditTransactions[] = $creditTransactionData;
        return $creditTransactionData;
    }

    public function saveCommData()
    {
        foreach (array_chunk($this->creditTransactions, 1000) as $record) {
            BankAccountStatement::insert($record);
        }
    }

    public function payout($creditType = [BankCreditTypeContract::CASH_CREDIT => 1], $creditTypeRate = [BankCreditTypeContract::CASH_CREDIT => 1])
    {
        $this->initPayoutCommission($creditType, $creditTypeRate);

        if (!$this->payouts->isEmpty()) {
            foreach ($this->payouts as $payout) {
                $calculatedAmount = $payout->amount;
                $payoutCredits    = $this->calculateAllCommCredits($payout->user_id, $calculatedAmount, $this->creditTransactionTypeId, $this->payoutDate);

                foreach ($payoutCredits as $bankAccountStatement) {
                    $this->data[] = [
                        'created_at'           => Carbon::parse($this->payoutDate),
                        'updated_at'           => Carbon::parse($this->payoutDate),
                        $this->bonus_payout_id => $payout->id,
                        'transaction_code'     => $bankAccountStatement['transaction_code'],
                        'percentage'           => $bankAccountStatement['percentage'],
                        'amount'               => $bankAccountStatement['amount'],
                        'issued_amount'        => $bankAccountStatement['issued_amount'],
                        'bank_credit_type_id'  => $bankAccountStatement['bank_credit_type_id'],
                        'exchange_rate'        => $bankAccountStatement['exchange_rate'],
                    ];
                } #end foreach
            } #end foreach commissions

            #payout in bank account statement
            $this->saveCommData();

            #payout credit
            $this->saveCommissionCredits();
        } #end if commissions
    }
}
