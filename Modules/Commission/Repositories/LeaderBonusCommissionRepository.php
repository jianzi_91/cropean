<?php

namespace Modules\Commission\Repositories;

use Affiliate\Commission\Concerns\HasNestedSet;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Commission\Models\LeaderBonusPayoutBreakdown;
use Modules\Commission\Models\LeaderBonusPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Rank\Models\UserRankHistorySnapshot;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;
use Plus65\Base\Repositories\Repository;

class LeaderBonusCommissionRepository extends Repository
{
    use HasNestedSet, CanPayoutCommission;

    protected $creditTransactionType        = BankTransactionTypeContract::LEADER_BONUS;
    protected $bonus_payout_id              = 'leader_bonus_payout_id';
    protected $data                         = [];
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $payoutDate;
    protected $leaderBonusRanks;
    protected $userLeaderBonusRanks          = [];
    protected $commissionsBreakdowns         = [];
    protected $sameRankCommissionsBreakdowns = [];
    protected $userCommissions               = [];
    protected $sameRankCommissions           = [];
    protected $sameRankPayouts               = [];
    protected $amounts;
    protected $precision;
    protected $withSelf = false;
    protected $manualRankUsers = [];
    /**
     * The percentage column
     *
     * @var string
     */
    protected $percent = 'percentage';

    public function __construct(array $sponsorTrees = [], array $amounts = [])
    {
        parent::__construct();
        $this->amounts = $amounts;
        if (!is_null($sponsorTrees)) {
            $this->buildTree($sponsorTrees);
        }
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function setLeaderBonusRank($leaderBonusRanks, $userLeaderBonusRanks)
    {
        $this->leaderBonusRanks     = $leaderBonusRanks;
        $this->userLeaderBonusRanks = $userLeaderBonusRanks;
    }

    public function setLeaderBonusManualRank($snapshotDate)
    {
        $this->manualRankUsers = UserRankHistorySnapshot::where('is_current', 1)
            ->where('run_date', $snapshotDate)
            ->where('is_manual', 1)
            ->pluck('user_id')
            ->toArray();
    }

    public function calculation($precision = 2)
    {
        $this->precision = $precision;

        //calculate
        $this->rankCalculation();
        $this->sameRankCalculation();

        //insert commission records
        $this->insertCommission($this->userCommissions, $this->commissionsBreakdowns);
        $this->insertCommission($this->sameRankCommissions, $this->sameRankCommissionsBreakdowns, true);
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            LeaderBonusPayoutCredit::insert($record);
        }
    }

    private function rankCalculation()
    {
        foreach ($this->amounts as $userId => $amount) {
            $node = $this->getNodeByUser($userId);
            //nonEligibleContributionUsers didnt contribute to upline
            if ($node && !in_array($userId, $this->nonEligibleContributionUsers)) {
                $previousPercentage  = 0;
                $first               = true;
                $uplines             = $this->getUplines($node, $this->withSelf);
                $currentUserRankId   = isset($this->userLeaderBonusRanks[$userId]) ? $this->userLeaderBonusRanks[$userId] : 0;
                $sameRankContributed = false;

                // calculate and contribute to upline
                foreach ($uplines as $upline) {
                    #get parent user
                    $uplineUserId = $upline->getUserId();

                    if (isset($this->userLeaderBonusRanks[$uplineUserId]) && $this->userLeaderBonusRanks[$uplineUserId] > 0 && !in_array($uplineUserId, $this->nonEligibleReceiveUsers)) {
                        $leaderBonusCommissionAmount = 0;
                        $uplineRankId                = $this->userLeaderBonusRanks[$uplineUserId];
                        $percentage                  = $this->leaderBonusRanks[$uplineRankId]->leader_bonus_percentage;
                        $override                    = $percentage;

                        if (!$first) {
                            # for multi-level upline
                            if ($percentage > $previousPercentage) {
                                $override                    = bcsub($percentage, $previousPercentage, $this->precision);
                                $leaderBonusCommissionAmount = bcmul($override, $amount, $this->precision);
                            } else {
                                $percentage = $previousPercentage;
                            }
                        } else {
                            # for immediate upline
                            $leaderBonusCommissionAmount = bcmul($override, $amount, $this->precision);
                        } #end if check first

                        #current user same rank
                        # manual up rank no contribute and also no receive
                        if ($currentUserRankId > 0
                            && $currentUserRankId == $uplineRankId
                            && $this->leaderBonusRanks[$uplineRankId]->same_rank_percentage > 0
                            && !$sameRankContributed
                            && !in_array($userId, $this->manualRankUsers)
                            && !in_array($uplineUserId, $this->manualRankUsers)) {
                            # same rank percentage
                            $this->sameRankPayouts[$uplineUserId][] = $userId;
                            $sameRankContributed                    = true;
                        }

                        if ($leaderBonusCommissionAmount > 0) {
                            $this->commissionsBreakdowns[$uplineUserId][] = [
                                'user_id'             => $uplineUserId,
                                'contributor_user_id' => $userId,
                                'amount'              => $leaderBonusCommissionAmount,
                                'percentage'          => $override,
                            ];

                            if (isset($this->userCommissions[$uplineUserId])) {
                                $this->userCommissions[$uplineUserId] = bcadd($this->userCommissions[$uplineUserId], $leaderBonusCommissionAmount, $this->precision);
                            } else {
                                $this->userCommissions[$uplineUserId] = $leaderBonusCommissionAmount;
                            }
                        }

                        $previousPercentage = $percentage;
                        $first              = false;
                    } #end if check isset($this->userRanks[$uplineUserId]) && $this->userRanks[$uplineUserId]
                } #end foreach $uplines
            } #end if $node && !in_array($userId, $this->nonEligibleContributionUsers)
        } #end foreach $this->amounts
    }

    private function sameRankCalculation()
    {
        foreach ($this->sameRankPayouts as $userId => $downlines) {
            foreach ($downlines as $downlineUserId) {
                $userRankId = $this->userLeaderBonusRanks[$userId];
                $percentage = $this->leaderBonusRanks[$userRankId]->same_rank_percentage;
                #get downline leader bonus earning
                if (isset($this->userCommissions[$downlineUserId])) {
                    $commissionAmt = bcmul($this->userCommissions[$downlineUserId], $percentage, $this->precision);

                    if ($commissionAmt > 0) {
                        $this->sameRankCommissionsBreakdowns[$userId][] = [
                            'user_id'             => $userId,
                            'contributor_user_id' => $downlineUserId,
                            'amount'              => $commissionAmt,
                            'percentage'          => $percentage,
                        ];

                        if (isset($this->sameRankCommissions[$userId])) {
                            $this->sameRankCommissions[$userId] = bcadd($this->sameRankCommissions[$userId], $commissionAmt, $this->precision);
                        } else {
                            $this->sameRankCommissions[$userId] = $commissionAmt;
                        }
                    } #end if commissionAmt
                } #end if user commission downline user id
            } #end foreach downlines
        } #end foreach $sameRankPayouts
    }

    private function insertCommission($commissions = [], $commissionBreakdown = [], $isSameRank = false)
    {
        $payouts    = [];
        $breakdowns = [];

        foreach ($commissions as $userId => $amount) {
            $percentage = $this->leaderBonusRanks[$this->userLeaderBonusRanks[$userId]]->{$isSameRank ? 'same_rank_percentage' : 'leader_bonus_percentage'} ?? 0;
            $payouts[]  = [
                'user_id'      => $userId,
                'amount'       => $amount,
                'created_at'   => $this->payoutDate,
                'updated_at'   => $this->payoutDate,
                'rank_id'      => $this->userLeaderBonusRanks[$userId],
                'percentage'   => $percentage,
                'is_same_rank' => $isSameRank,
            ];
        }

        if ($payouts) {
            foreach (array_chunk($payouts, 1000) as $payout) {
                LeaderBonusPayout::insert($payout);
            }
        }

        $payouts = LeaderBonusPayout::whereNull('payout_date')
            ->where('is_credit_issued', 0)
            ->whereBetween('created_at', [$this->payoutDate->copy()->startOfDay(), $this->payoutDate->copy()->endOfDay()])
            ->where('is_same_rank', $isSameRank)
            ->get();

        foreach ($payouts as $payout) {
            if (isset($commissionBreakdown[$payout->user_id])) {
                foreach ($commissionBreakdown[$payout->user_id] as $breakdown) {
                    $breakdowns[] = [
                        'user_id'                => $breakdown['contributor_user_id'],
                        'amount'                 => $breakdown['amount'],
                        'percentage'             => $breakdown['percentage'],
                        'leader_bonus_payout_id' => $payout->id,
                        'is_same_rank'           => $isSameRank,
                        'created_at'             => $this->payoutDate,
                        'updated_at'             => $this->payoutDate,
                    ];
                }
            }
        }

        if ($breakdowns) {
            foreach (array_chunk($breakdowns, 500) as $breakdown) {
                LeaderBonusPayoutBreakdown::insert($breakdown);
            }
        }
    }
}
