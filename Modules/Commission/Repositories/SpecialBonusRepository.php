<?php

namespace Modules\Commission\Repositories;

use Affiliate\Commission\Overriding;
use Carbon\Carbon;
use Modules\Commission\Models\SpecialBonusPayout;
use Modules\Commission\Models\SpecialBonusPayoutBreakdown;
use Modules\Commission\Models\SpecialBonusPayoutCredit;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\User\Contracts\UserStatusContract;
use Modules\User\Models\User;

class SpecialBonusRepository extends Overriding
{
    use CanPayoutCommission;

    protected $creditTransactionType        = BankTransactionTypeContract::SPECIAL_BONUS;
    protected $bonus_payout_id              = 'special_bonus_payout_id';
    protected $data                         = [];
    protected $nonEligibleContributionUsers = [];
    protected $nonEligibleReceiveUsers      = [];
    protected $payoutDate;
    protected $exchangeRate;

    /**
     * Class constructor.
     *
     * @param array|null $tree
     * @param array $amounts
     */
    public function __construct(array $tree = null, array $amounts = [])
    {
        parent::__construct($tree, $amounts);
    }

    public function setPayoutDate($payoutDate)
    {
        $this->payoutDate = $payoutDate;
    }

    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate->sell_rate ?? 1;
    }

    public function setNonEligibleUsers()
    {
        $this->nonEligibleContributionUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED])
            ->pluck('users.id')
            ->toArray();

        $this->nonEligibleReceiveUsers = User::join('user_statuses', 'user_statuses.id', '=', 'users.user_status_id')
            ->whereIn('user_statuses.name', [UserStatusContract::TERMINATED, UserStatusContract::SUSPENDED])
            ->pluck('users.id')
            ->toArray();
    }

    /**
     * Calculate commission
     *
     * @param int $precision
     * @return void
     */
    public function calculate($precision = 2)
    {
        $this->commissions = [];
        if ($this->tree && $this->amounts) {
            foreach ($this->amounts as $userId => $amountInCapital) {
                // convert exchange rate
                $amount = bcdiv($amountInCapital, $this->exchangeRate, $precision);
                $node   = $this->getNodeByUser($userId);

                //nonEligibleContributionUsers didnt contribute to upline
                if ($node && !in_array($userId, $this->nonEligibleContributionUsers)) {
                    $previousPercentage = 0;
                    $first              = true;
                    $uplines            = $this->getUplines($node);

                    foreach ($uplines as $upline) {
                        $uplineId = $upline->getUserId();
                        $current  = isset($this->commissions[$uplineId]) ? $this->commissions[$uplineId] : $upline;

                        //nonEligibleReceiveUsers will get 0 percentage
                        $percentage = in_array($uplineId, $this->nonEligibleReceiveUsers) ? 0 : $upline->getPercentage();

                        $override = $percentage;
                        $level    = $current->getLevelFromNode($node);
                        if (!$first) {
                            if ($percentage > $previousPercentage) {
                                $override   = bcsub($percentage, $previousPercentage, $precision);
                                $commission = bcmul($override, $amount, $precision);
                            } else {
                                $override   = $commission   = 0;
                                $percentage = $previousPercentage;
                            }
                        } else {
                            $commission = bcmul($percentage, $amount, $precision);
                        }
                        $computedAmount = $commission;

                        if ($commission > 0) {
                            // replicate node
                            $breakdown = $node->clone();
                            $breakdown->setLevel($level)
                                ->setCommission($commission)
                                ->setComputedCommission($computedAmount)
                                ->setAmount($amount)
                                ->setPercentage($override);
                            $this->commissions[$current->getUserId()] = $current->addBreakdown($breakdown);
                        }

                        $previousPercentage = $percentage;
                        $first              = false;
                    }
                }
            }
        }

        $this->insertCommissionBreakdown();
    }

    public function insertCommissionBreakdown()
    {
        #insert payout
        $payouts     = [];
        $commissions = $this->getCommission();
        foreach ($commissions as $userId => $commission) {
            $payouts[] = [
                'user_id'         => $userId,
                'amount'          => $commission->getCommission(),
                'computed_amount' => $commission->getCommission(),
                'percentage'      => $commission->getPercentage(),
                'created_at'      => Carbon::parse($this->payoutDate),
                'updated_at'      => Carbon::parse($this->payoutDate),
            ];
        }

        # insert payout
        if ($payouts) {
            foreach (array_chunk($payouts, 1000) as $specialBonusPayout) {
                SpecialBonusPayout::insert($specialBonusPayout);
            }
        }

        #get payout
        $this->payouts = SpecialBonusPayout::whereBetween('created_at', [Carbon::parse($this->payoutDate)->startOfDay(), Carbon::parse($this->payoutDate)->endOfDay()])
            ->whereNull('payout_date')
            ->get();

        #Insert into breakdowns
        $breakdownData = [];
        foreach ($this->payouts as $payout) {
            if (isset($commissions[$payout->user_id])) {
                $breakDowns = $commissions[$payout->user_id]->getBreakdown();
                foreach ($breakDowns as $breakdown) {
                    $breakdownData[] = [
                        'user_id'              => $breakdown->getUserId(),
                        'amount'               => $breakdown->getCommission(),
                        'percentage'           => $breakdown->getPercentage(),
                        $this->bonus_payout_id => $payout->id,
                        'created_at'           => Carbon::parse($this->payoutDate),
                        'updated_at'           => Carbon::parse($this->payoutDate),
                    ];
                }
            }
        }

        if ($breakdownData) {
            foreach (array_chunk($breakdownData, 1000) as $breakdown) {
                SpecialBonusPayoutBreakdown::insert($breakdown);
            }
        }
    }

    private function saveCommissionCredits()
    {
        foreach (array_chunk($this->data, 1000) as $record) {
            SpecialBonusPayoutCredit::insert($record);
        }
    }
}
