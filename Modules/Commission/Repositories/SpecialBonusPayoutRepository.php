<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\SpecialBonusPayoutContract;
use Modules\Commission\Models\SpecialBonusPayout;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class SpecialBonusPayoutRepository extends Repository implements SpecialBonusPayoutContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param SpecialBonusPayout $model
     */
    public function __construct(SpecialBonusPayout $model)
    {
        parent::__construct($model);
    }
}
