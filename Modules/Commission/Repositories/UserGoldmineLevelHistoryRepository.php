<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\UserGoldmineLevelHistoryContract;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class UserGoldmineLevelHistoryRepository extends Repository implements UserGoldmineLevelHistoryContract
{
    use HasCrud;

    protected $model;

    public function __construct(UserGoldmineLevelHistory $model)
    {
        $this->model = $model;

        parent::__construct($model);
    }
}
