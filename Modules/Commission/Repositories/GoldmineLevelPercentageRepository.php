<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\GoldmineLevelPercentageContract;
use Modules\Commission\Models\GoldmineLevelPercentage;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class GoldmineLevelPercentageRepository extends Repository implements GoldmineLevelPercentageContract
{
    use HasCrud, HasSlug, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param GoldmineLevelPercentage $model
     */
    public function __construct(GoldmineLevelPercentage $model)
    {
        $this->slug = 'level';

        parent::__construct($model);
    }
}
