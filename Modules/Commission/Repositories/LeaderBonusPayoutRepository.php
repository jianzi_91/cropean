<?php

namespace Modules\Commission\Repositories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class LeaderBonusPayoutRepository extends Repository implements LeaderBonusPayoutContract
{
    use HasCrud, HasBulkAction;

    /**
     * LeaderBonusPayoutRepository constructor.
     * @param LeaderBonusPayout $model
     */
    public function __construct(LeaderBonusPayout $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @param bool $uncreditedOnly
     * @return mixed
     */
    public function findBetween(Carbon $from, Carbon $to)
    {
        $query = $this->model
            ->whereBetween('created_at', [$from->toDateTimeString(), $to->toDateTimeString()]);

        return $query->get();
    }
}
