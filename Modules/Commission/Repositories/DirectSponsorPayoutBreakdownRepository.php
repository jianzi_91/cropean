<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\DirectSponsorPayoutBreakdownContract;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class DirectSponsorPayoutBreakdownRepository extends Repository implements DirectSponsorPayoutBreakdownContract
{
    use HasBulkAction, HasCrud;
    // implement functions here
}
