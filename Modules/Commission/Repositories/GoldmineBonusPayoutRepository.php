<?php

namespace Modules\Commission\Repositories;

use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Models\GoldmineBonusPayout;
use Modules\Core\Traits\HasBulkAction;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Repository;

class GoldmineBonusPayoutRepository extends Repository implements GoldmineBonusPayoutContract
{
    use HasCrud, HasBulkAction;

    /**
     * Class constructor.
     *
     * @param GoldmineBonusPayout $model
     */
    public function __construct(GoldmineBonusPayout $model)
    {
        parent::__construct($model);
    }
}
