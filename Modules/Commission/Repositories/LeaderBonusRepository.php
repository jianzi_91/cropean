<?php

namespace Modules\Commission\Repositories;

use Affiliate\Commission\Overriding;
use Carbon\Carbon;
use Modules\Commission\Models\LeaderBonusPayout;
use Modules\Commission\Models\LeaderBonusPayoutBreakdown;
use Modules\Commission\Repositories\Concerns\CanPayoutCommission;
use Modules\Core\Contracts\BulkActionContract;
use Modules\Core\Traits\HasBulkAction;
use Modules\Credit\Contracts\BankAccountStatementContract;
use Modules\Credit\Core\Generators\SimpleGenerator;
use Modules\Rank\Models\Rank;
use Modules\Rank\Models\UserRankHistory;
use Modules\User\Contracts\UserContract;

class LeaderBonusRepository extends Overriding
{
    use HasBulkAction, CanPayoutCommission;

    protected $payoutDate;

    protected $bankTransactionTypeId;
    protected $commissionData = [];
    protected $userRank       = [];
    protected $data           = [];
    protected $ranks;
    protected $bonus_payout_id = 'leader_bonus_payout_id';

    /**
     * LeaderBonusRepository constructor.
     * @param Carbon $payoutDate
     * @param array|null $tree
     * @param array $amounts
     * @param int $bankTransactionTypeId
     */
    public function __construct(Carbon $payoutDate, array $tree = null, array $amounts = [], int $bankTransactionTypeId)
    {
        parent::__construct($tree, $amounts);
        $this->payoutDate            = $payoutDate;
        $this->bankTransactionTypeId = $bankTransactionTypeId;
        $this->userRank              = UserRankHistory::current()->pluck('rank_id', 'user_id');
    }

    /**
     * Calculate profit payout
     * @param int $precision
     */
    public function calculate($precision = 2)
    {
        $userContract = resolve(UserContract::class);
        $userRankList = $userContract->all()->pluck('rank_id', 'id');

        $userCommission = [];
        $this->ranks    = Rank::orderBy('level', 'desc')->get()->keyBy('id');
        $startDate      = $this->payoutDate->copy()->startOfDay();
        $endDate        = $this->payoutDate->copy()->endOfDay();
        //TODO['user_id']=>amount
        $sales = [];

        if (!LeaderBonusPayout::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])->exists()) {
            foreach ($sales as $userId => $salesAmount) {
                $node = $this->getNodeByUser($userId);

                if ($node) {
                    $previousPercentage = 0;
                    $first              = true;
                    $uplines            = $this->getUplines($node);
                    foreach ($uplines as $upline) {
                        $uplineUserId = $upline->getUserId();

                        if (isset($this->userRank[$uplineUserId]) && $this->userRank[$uplineUserId] > 0) {
                            $uplineRankId = $this->userRank[$uplineUserId];

                            $percentage = $this->ranks[$uplineRankId]->leader_bonus_percentage;

                            $override = $percentage;

                            if (!$first) {
                                if ($percentage > $previousPercentage) {
                                    $override      = bcsub($percentage, $previousPercentage, 2);
                                    $commissionAmt = bcmul($override, $salesAmount, 2);
                                } else {
                                    $percentage = $previousPercentage;
                                }
                            } else {
                                $commissionAmt = bcmul($override, $salesAmount, 2);
                            }

                            if ($commissionAmt > 0) {
                                $this->commissionData[$uplineUserId][] = [
                                  'user_id'             => $uplineUserId,
                                  'contributor_user_id' => $userId,
                                  'amount'              => $commissionAmt,
                                  'percentage'          => $override
                                ];
                            }

                            if (isset($userCommission[$uplineUserId])) {
                                $userCommission[$uplineUserId] = bcadd($userCommission[$uplineUserId], $commissionAmt, 2);
                            } else {
                                $userCommission[$uplineUserId] = $commissionAmt;
                            }

                            $commissionAmt      = 0;
                            $previousPercentage = $percentage;
                            $first              = false;
                        }
                    }
                }
            }

            //Insert into payouts
            $LeaderBonusPayouts = [];
            foreach ($userCommission as $userId => $commission) {
                $LeaderBonusPayouts[] = [
                    'user_id'     => $userId,
                    'amount'      => $commission,
                    'created_at'  => Carbon::parse($this->payoutDate)->toDateString() . ' ' . date("H:i:s"),
                    'updated_at'  => Carbon::now(),
                    'payout_date' => $this->payoutDate->toDateString(),
                ];
            }

            if ($LeaderBonusPayouts) {
                foreach (array_chunk($LeaderBonusPayouts, 500) as $LeaderBonusPayout) {
                    LeaderBonusPayout::insert($LeaderBonusPayout);
                }
            }

            $payouts = LeaderBonusPayout::whereBetween('created_at', [$startDate->toDateTimeString(), $endDate->toDateTimeString()])
                ->get();

            //Insert into breakdowns
            $breakdownData = [];
            foreach ($payouts as $payout) {
                if (isset($this->commissionData[$payout->user_id])) {
                    foreach ($this->commissionData[$payout->user_id] as $breakdown) {
                        $breakdownData[] = [
                            'amount'               => $breakdown['amount'],
                            'percentage'           => $breakdown['percentage'],
                            $this->bonus_payout_id => $payout->id,
                            'user_id'              => $breakdown['user_id'],
                            'created_at'           => Carbon::now(),
                            'updated_at'           => Carbon::now(),
                        ];
                    }
                }
            }

            if ($breakdownData) {
                foreach (array_chunk($breakdownData, 500) as $breakdown) {
                    LeaderBonusPayoutBreakdown::insert($breakdown);
                }
            }
        }
    }

    /**
     * @param BulkActionContract $payoutRepo
     * @param BulkActionContract $payoutCreditRepo
     * @param Carbon $payoutDate
     * @param Carbon|null $payoutStartDate
     * @param Carbon|null $payoutEndDate
     */
    public function issuePayouts(BulkActionContract $payoutRepo, BulkActionContract $payoutCreditRepo, Carbon $payoutDate, Carbon $payoutStartDate = null, Carbon $payoutEndDate = null)
    {
        if ($payoutStartDate && $payoutEndDate) {
            $startDate = ($payoutStartDate->copy())->startOfDay();
            $endDate   = ($payoutEndDate->copy())->endOfDay();
        } else {
            $startDate = ($payoutDate->copy())->startOfDay();
            $endDate   = ($payoutDate->copy())->endOfDay();
        }

        $dateString = $payoutDate->copy()->toDateTimeString();

        $payouts = $payoutRepo->findBetween($startDate, $endDate, true);

        if (!$payouts->isEmpty()) {
            $statementRepo = resolve(BankAccountStatementContract::class);

            $generator          = resolve(SimpleGenerator::class);
            $commissionTxTypeId = $this->getPayoutTransactionTypeId();

            $txnCodes = [];

            $payoutSchemeBreakdowns = json_decode(setting($this->getPayoutSchemeSettingName()), true);
            $payoutScheme           = [];

            foreach ($payoutSchemeBreakdowns as $creditTypeSlug => $percentage) {
                $payoutScheme[bank_credit_type_id($creditTypeSlug)] = $percentage;
            }

            foreach ($payouts as $payout) {
                foreach ($payoutScheme as $bankCreditTypeId => $percentage) {
                    do {
                        $transactionCode = $generator->generate($this->getTransactionPrefix());
                    } while (isset($txnCodes[$transactionCode]));

                    $txnCodes[$transactionCode] = $transactionCode;

                    $payoutAmount = bcmul($payout->amount, $percentage, credit_precision());

                    $statementRepo->addToBulkInsert([
                            'created_at'               => $dateString,
                            'updated_at'               => $dateString,
                            'transaction_date'         => $dateString,
                            'transaction_code'         => $transactionCode,
                            'bank_transaction_type_id' => $commissionTxTypeId,
                            'bank_credit_type_id'      => $bankCreditTypeId,
                            'bank_account_id'          => $this->getBankAccountId($payout->user_id),
                            'done_by'                  => $this->getBankAccountId($payout->user_id),
                            'credit'                   => $payoutAmount,
                        ]);

                    if ($transactionCode) {
                        $payoutCreditRepo->addToBulkInsert([
                                'created_at'           => $dateString,
                                'updated_at'           => $dateString,
                                $this->bonus_payout_id => $payout->id,
                                'transaction_code'     => $transactionCode,
                                'amount'               => $payoutAmount,
                                'percentage'           => $percentage,
                                'bank_credit_type_id'  => $bankCreditTypeId,
                            ]);
                    }
                }
            }

            $statementRepo->executeBulkInsert();
            $payoutCreditRepo->executeBulkInsert();
        }
    }

    /**
     * Return payout scheme setting name
     * @return string
     */
    protected function getPayoutSchemeSettingName()
    {
        return 'leader_bonus_payout_scheme';
    }

    /**
     * Get transaction prefix
     * @return string
     */
    protected function getTransactionPrefix()
    {
        return 'LB';
    }

    /**
     * @return int
     */
    protected function getPayoutTransactionTypeId()
    {
        return $this->bankTransactionTypeId;
    }
}
