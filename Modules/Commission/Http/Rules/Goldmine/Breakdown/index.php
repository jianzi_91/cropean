<?php

namespace Modules\Commission\Http\Requests\Member\Goldmine\Breakdown;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $goldmineBonusContract = resolve(GoldmineBonusPayoutContract::class);
        if ($goldmineBonusContract->find($this->payout_id)->user_id == auth()->user()->id) {
            return true;
        }
        return false;
    }
}
