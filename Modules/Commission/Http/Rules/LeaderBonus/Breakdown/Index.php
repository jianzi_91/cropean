<?php

namespace Modules\Commission\Http\Requests\Member\LeaderBonus\Breakdown;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $leaderBonusContract = resolve(LeaderBonusPayoutContract::class);
        $leaderBonus         = $leaderBonusContract->getModel()->where('user_id', $this->uid)->first();
        if ($leaderBonus && $leaderBonus->user_id == auth()->user()->id) {
            return true;
        }
        return false;
    }
}
