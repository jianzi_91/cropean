<?php

namespace Modules\Commission\Http\Requests\Member\SpecialBonus\Breakdown;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Commission\Contracts\GoldmineBonusPayoutBreakdownContract;
use Modules\Commission\Contracts\SpecialBonusPayoutBreakdownContract;
use Modules\Commission\Contracts\SpecialBonusPayoutContract;

class Index extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $specialBonusContract = resolve(SpecialBonusPayoutContract::class);
        if ($specialBonusContract->find($this->payout_id)->user_id == auth()->user()->id) {
            return true;
        }
        return false;
    }
}
