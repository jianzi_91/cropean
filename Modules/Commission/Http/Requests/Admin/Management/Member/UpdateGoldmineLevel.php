<?php

namespace Modules\Commission\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateGoldmineLevel extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'manual_goldmine_level' => [
                'nullable',
                'exists:goldmine_level_percentages,id'
            ],
            'is_locked_level' => ['required', Rule::in(['true', 'false'])],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'manual_goldmine_level' => __('a_member_management_goldmine.manual rank'),
            'is_locked_level'       => __('a_member_management_goldmine.is locked'),
        ];
    }
}
