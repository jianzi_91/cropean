<?php

namespace Modules\Commission\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateGoldmineRank extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'manual_goldmine_rank_id' => [
                'nullable',
                'exists:goldmine_ranks,id'
            ],
            'is_locked_rank' => ['required', Rule::in(['true', 'false'])],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'manual_goldmine_rank_id' => __('a_member_management_goldmine.manual rank'),
            'is_locked_rank'          => __('a_member_management_goldmine.is locked'),
        ];
    }
}
