<?php

namespace Modules\Commission\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class Update extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'manual_rank_id' => [
                'nullable',
                'exists:ranks,id'
            ],
            'is_locked'            => ['required', Rule::in(['true', 'false'])],
            'goldmine_manual_rank' => [
                'nullable',
                'integer',
                new NotScientificNotation(),
                'exists:goldmine_ranks,id'
            ],
            'is_override'              => ['required', Rule::in(['true', 'false'])],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation error messages.
     *
     * @return string
     */
    public function messages()
    {
        return [
        ];
    }

    public function attributes()
    {
        return [
            'manual_rank_id'                   => __('a_member_management_rank_commission_configurables.manual rank'),
            'is_locked'                        => __('a_member_management_rank_commission_configurables.is locked'),
            'goldmine_manual_rank'             => __('a_member_management_rank_commission_configurables.goldmine manual ranks'),
            'is_override'                      => __('a_member_management_rank_commission_configurables.is override'),
        ];
    }
}
