<?php

namespace Modules\Commission\Http\Requests\Admin\Management\Member;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Credit\Http\Rules\MaxDecimalPlaces;
use Modules\Credit\Http\Rules\NotScientificNotation;

class UpdateSpecialBonus extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'special_bonus_percentage' => [
                'nullable',
                new NotScientificNotation(),
                new MaxDecimalPlaces(credit_precision()),
                'gte:0',
                'max:100'
            ],
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'special_bonus_percentage' => __('a_member_management_special_bonus.special bonus percentage'),
        ];
    }
}
