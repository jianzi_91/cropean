<?php

namespace Modules\Commission\Http\Controllers\Admin\SpecialBonus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\SpecialBonusQuery;
use QueryBuilder\Concerns\CanExportTrait;

class SpecialBonusExportController extends Controller
{
    use CanExportTrait;

    /**
     * The leader bonus query
     *
     * @var unknown
     */
    protected $query;

    /**
     * Class constructor
     *
     * @param SpecialBonusQuery $query
     */
    public function __construct(SpecialBonusQuery $query)
    {
        $this->middleware('permission:admin_commission_special_bonus_export')->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('a_special.special bonus') . now() . '.xlsx', auth()->user(), __('a_special.special bonus') . Carbon::now()->toDateString());
    }
}
