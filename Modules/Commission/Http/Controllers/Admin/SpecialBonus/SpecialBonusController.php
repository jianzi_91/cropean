<?php

namespace Modules\Commission\Http\Controllers\Admin\SpecialBonus;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\SpecialBonusQuery;

class SpecialBonusController extends Controller
{
    protected $query;

    /**
     * SpecialBonusController constructor.
     * @param SpecialBonusQuery $query
     */
    public function __construct(SpecialBonusQuery $query)
    {
        $this->query = $query;
        $this->middleware('permission:admin_commission_special_bonus_list')->only('index');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.special_bonus.index', compact('payouts'));
    }
}
