<?php

namespace Modules\Commission\Http\Controllers\Admin\SpecialBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\SpecialBonusPayoutContract;
use Modules\Commission\Queries\Admin\SpecialBonusBreakdownQuery;

class SpecialBreakdownController extends Controller
{
    /**
     * The special breakdown query
     *
     * @var unknown
     */
    protected $specialQuery;

    /**
     * The special payout contract
     *
     * @var unknown
     */
    protected $specialPayoutContract;

    /**
     * Class constructor
     *
     * @param SpecialBonusBreakdownQuery $specialQuery
     */
    public function __construct(SpecialBonusBreakdownQuery $specialQuery, SpecialBonusPayoutContract $contract)
    {
        $this->middleware('permission:admin_commission_special_bonus_list')->only('index');

        $this->specialQuery          = $specialQuery;
        $this->specialPayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $payoutId)
    {
        $request->merge([
            'special_bonus_payout_id' => $payoutId,
        ]);

        $payout = $this->specialPayoutContract->find($payoutId);

        $payouts = $this->specialQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.special_bonus.breakdown', compact('payouts', 'payoutId', 'payout'));
    }
}
