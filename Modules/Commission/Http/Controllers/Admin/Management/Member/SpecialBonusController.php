<?php

namespace Modules\Commission\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Contracts\UserSpecialBonusPercentageHistoryContract;
use Modules\Commission\Http\Requests\Admin\Management\Member\Edit;
use Modules\Commission\Http\Requests\Admin\Management\Member\UpdateSpecialBonus;
use Modules\User\Contracts\UserContract;
use Modules\User\Queries\Admin\UserQuery;
use Plus65\Utility\Exceptions\WebException;

class SpecialBonusController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The user query
     *
     * @var unknown
     */
    protected $userQuery;

    /**
     * The rank qualification repository
     *
     * @var unknown
     */
    protected $rankRepository;

    /**
     * The user goldmine level repository
     *
     * @var unknown
     */
    protected $userGoldmineLevelHistoryRepository;

    /**
     * The goldmine level percentage repository
     *
     * @var unknown
     */
    protected $goldmineLevelPercentageRepository;

    /**
     * The user special bonus percentage history repository
     *
     * @var unknown
     */
    protected $userSpecialBonusPercentageHistoryRepository;

    /**
     * The class constructor
     *
     * @param UserContract     $userContract
     * @param UserQuery        $userQuery
     * @param UserSpecialBonusPercentageHistoryContract $userSpecialBonusPercentageHistoryContract
     */
    public function __construct(
        UserContract $userContract,
        UserQuery $userQuery,
        UserSpecialBonusPercentageHistoryContract $userSpecialBonusPercentageHistoryContract
    ) {
        $this->middleware('permission:admin_user_member_management_special_bonus_edit')->only('edit', 'update');

        $this->userRepository                              = $userContract;
        $this->userQuery                                   = $userQuery;
        $this->userSpecialBonusPercentageHistoryRepository = $userSpecialBonusPercentageHistoryContract;
    }

    /**
     * Show the form for editing the specified resource.
     * @param Edit $request
     * @param int $id
     * @return Response
     */
    public function edit(Edit $request, $id)
    {
        $user = $this->userRepository->find($id);
        return view('commission::admin.management.member.edit_special_bonus', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateSpecialBonus $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateSpecialBonus $request, $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user) {
            return back()->with('error', __('a_member_management.user not exist'));
        }

        DB::beginTransaction();

        try {
            if (empty($request->special_bonus_percentage)) {
                $this->userRepository->edit($user->id, ['special_bonus_percentage' => null]);
                if ($userSpecialBonusPercentage = $this->userSpecialBonusPercentageHistoryRepository->getModel()->where('user_id', $user->id)->where('is_current', 1)->first()) {
                    $userSpecialBonusPercentage->update([
                        'is_current' => 0
                    ]);
                }
            } else {
                $this->userRepository->edit($user->id, ['special_bonus_percentage' => bcdiv($request->special_bonus_percentage, 100, config('credit.precision.percentage_decimal'))]);
                if ($userSpecialBonusPercentage = $this->userSpecialBonusPercentageHistoryRepository->getModel()->where('user_id', $user->id)->where('is_current', 1)->first()) {
                    $userSpecialBonusPercentage->update([
                        'is_current' => 0
                    ]);
                }
                $this->userSpecialBonusPercentageHistoryRepository->add([
                    'user_id'    => $user->id,
                    'percentage' => bcdiv($request->special_bonus_percentage, 100, config('credit.precision.percentage_decimal')),
                    'is_current' => 1,
                    'updated_by' => auth()->user()->id
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.commissions.edit', $user->id))->withMessage(__('a_member_management.failed to update user special bonus'));
        }

        return back()->with('success', __('a_member_management.user updated successfully'));
    }
}
