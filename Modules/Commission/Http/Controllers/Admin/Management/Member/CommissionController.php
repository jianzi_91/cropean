<?php

namespace Modules\Commission\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Contracts\GoldmineLevelPercentageContract;
use Modules\Commission\Contracts\UserGoldmineLevelHistoryContract;
use Modules\Commission\Contracts\UserSpecialBonusPercentageHistoryContract;
use Modules\Commission\Http\Requests\Admin\Management\Member\Edit;
use Modules\Commission\Http\Requests\Admin\Management\Member\Update;
use Modules\Commission\Models\UserGoldmineLevelHistory;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Contracts\RankContract;
use Modules\Rank\Models\UserRankHistory;
use Modules\User\Contracts\UserContract;
use Modules\User\Queries\Admin\UserQuery;
use Plus65\Utility\Exceptions\WebException;

class CommissionController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The user query
     *
     * @var unknown
     */
    protected $userQuery;

    /**
     * The rank qualification repository
     *
     * @var unknown
     */
    protected $rankRepository;

    /**
     * The user goldmine level repository
     *
     * @var unknown
     */
    protected $userGoldmineLevelHistoryRepository;

    /**
     * The goldmine level percentage repository
     *
     * @var unknown
     */
    protected $goldmineLevelPercentageRepository;

    /**
     * The user special bonus percentage history repository
     *
     * @var unknown
     */
    protected $userSpecialBonusPercentageHistoryRepository;

    protected $goldmineRankRepository;

    /**
     * The class constructor
     *
     * @param UserContract $userContract
     * @param UserQuery $userQuery
     * @param RankContract $rankContract
     * @param GoldmineRankContract $goldmineRankContract
     * @param UserGoldmineLevelHistoryContract $userGoldmineLevelHistoryContract
     * @param GoldmineLevelPercentageContract $goldmineLevelPercentageContract
     * @param UserSpecialBonusPercentageHistoryContract $userSpecialBonusPercentageHistoryContract
     */
    public function __construct(
        UserContract $userContract,
        UserQuery $userQuery,
        RankContract $rankContract,
        GoldmineRankContract $goldmineRankContract,
        UserGoldmineLevelHistoryContract $userGoldmineLevelHistoryContract,
        GoldmineLevelPercentageContract $goldmineLevelPercentageContract,
        UserSpecialBonusPercentageHistoryContract $userSpecialBonusPercentageHistoryContract
    ) {
        $this->middleware('permission:admin_user_member_management_update_commission_settings')->only('edit', 'update');

        $this->userRepository                              = $userContract;
        $this->userQuery                                   = $userQuery;
        $this->rankRepository                              = $rankContract;
        $this->goldmineRankRepository                      = $goldmineRankContract;
        $this->userGoldmineLevelHistoryRepository          = $userGoldmineLevelHistoryContract;
        $this->goldmineLevelPercentageRepository           = $goldmineLevelPercentageContract;
        $this->userSpecialBonusPercentageHistoryRepository = $userSpecialBonusPercentageHistoryContract;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Edit $request, $id)
    {
        $with = [
            'rank',
            'goldmine'
        ];

        $user             = $this->userRepository->find($id, $with);
        $userRank         = UserRankHistory::where('user_id', $id)->where('is_current', 1)->first();
        $userGoldmineRank = UserGoldmineLevelHistory::where('user_id', $id)->where('is_current', 1)->first();

        $ranksDropdown         = $this->rankRepository->all([], 0, ['*'], ['id' => 'asc'])->dropDown('name', 'id', __('s_ranks.default to system'));
        $goldmineRanksDropdown = $this->goldmineRankRepository->all([], 0, ['*'], ['id' => 'asc'])->dropDown('name', 'id', __('s_ranks.default to system'));

        return view('commission::admin.management.member.edit', compact('user', 'userRank', 'userGoldmineRank', 'ranksDropdown', 'goldmineRanksDropdown'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Update $request, $id)
    {
        $user = $this->userRepository->find($id);

        DB::beginTransaction();

        try {
            $userRank = UserRankHistory::where('user_id', $id)->where('is_current', 1)->first();

            if (empty($request->manual_rank_id)) {
                //go back to natural rank
                if (!empty($userRank)) {
                    $this->userRepository->edit($id, ['rank_id' => $userRank->qualified_rank_id]);
                    if ($userRank) {
                        $userRank->update([
                            'is_current'        => 0,
                            'qualified_rank_id' => $userRank->qualified_rank_id
                        ]);

                        $rank = $this->rankRepository->find($userRank->qualified_rank_id);
                        $this->rankRepository->changeHistory($user, $rank, [
                            'updated_by'        => auth()->user()->id,
                            'is_manual'         => false,
                            'is_locked'         => filter_var($request->is_locked, FILTER_VALIDATE_BOOLEAN),
                            'qualified_rank_id' => $userRank->qualified_rank_id,
                        ]);
                    }
                }
            } else {
                $rank = $this->rankRepository->find($request->manual_rank_id);
                $this->userRepository->edit($id, ['rank_id' => $request->manual_rank_id]);
                $this->rankRepository->changeHistory($user, $rank, [
                    'updated_by'        => auth()->user()->id,
                    'is_manual'         => true,
                    'is_locked'         => filter_var($request->is_locked, FILTER_VALIDATE_BOOLEAN),
                    'qualified_rank_id' => $userRank->qualified_rank_id ?? null,
                ]);
            }

            $userGoldmineRank = UserGoldmineLevelHistory::where('user_id', $id)->where('is_current', 1)->first();

            if (empty($request->goldmine_manual_rank)) {
                //go back to natural goldmine level
                if ($userGoldmineRank) {
                    $this->userRepository->edit($id, ['goldmine_rank_id' => $userGoldmineRank->qualified_rank_id, 'goldmine_level' => $userGoldmineRank->qualified_level]);
                }

                if ($userGoldmine = $this->userGoldmineLevelHistoryRepository->getModel()->where('user_id', $id)->where('is_current', 1)->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);

                    $goldmineRank        = $this->goldmineRankRepository->find($request->goldmine_manual_rank);
                    $defaultGoldmineRank = $this->goldmineRankRepository->findBySlug(GoldmineRankContract::A1);
                    if (empty($goldmineRank)) {
                        $goldmineRank = $defaultGoldmineRank;
                    }
                    $this->userGoldmineLevelHistoryRepository->add([
                        'user_id'                    => $id,
                        'goldmine_rank_id'           => $goldmineRank->id,
                        'level'                      => $goldmineRank->level,
                        'qualified_goldmine_rank_id' => $userGoldmine->qualified_goldmine_rank_id ?? null,
                        'qualified_level'            => $userGoldmine->qualified_level ?? null,
                        'is_override'                => filter_var($request->is_override, FILTER_VALIDATE_BOOLEAN),
                        'is_current'                 => 1,
                        'updated_by'                 => auth()->user()->id,
                        'is_manual'                  => false
                    ]);
                }
            } else {
                $goldmineRank = $this->goldmineRankRepository->find($request->goldmine_manual_rank);
                $this->userRepository->edit($id, ['goldmine_rank_id' => $request->goldmine_manual_rank, 'goldmine_level' => $goldmineRank->level]);
                if ($userGoldmine = $this->userGoldmineLevelHistoryRepository->getModel()->where('user_id', $id)->where('is_current', 1)->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);
                }

                $this->userGoldmineLevelHistoryRepository->add([
                    'user_id'                    => $id,
                    'goldmine_rank_id'           => $request->goldmine_manual_rank,
                    'level'                      => $goldmineRank->level,
                    'qualified_goldmine_rank_id' => $userGoldmineRank->qualified_goldmine_rank_id ?? null,
                    'qualified_level'            => $userGoldmineRank->qualified_level ?? null,
                    'is_override'                => filter_var($request->is_override, FILTER_VALIDATE_BOOLEAN),
                    'is_current'                 => 1,
                    'updated_by'                 => auth()->user()->id,
                    'is_manual'                  => true,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.commissions.edit', $id))->withMessage(__('a_member_management.failed to update user rank and commissions'));
        }

        return back()->with('success', __('a_member_management.user updated successfully'));
    }
}
