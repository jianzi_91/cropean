<?php

namespace Modules\Commission\Http\Controllers\Admin\Management\Member;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Commission\Contracts\GoldmineLevelPercentageContract;
use Modules\Commission\Contracts\UserGoldmineLevelHistoryContract;
use Modules\Commission\Http\Requests\Admin\Management\Member\UpdateGoldmineLevel;
use Modules\Commission\Http\Requests\Admin\Management\Member\UpdateGoldmineRank;
use Modules\Rank\Contracts\GoldmineRankContract;
use Modules\Rank\Contracts\UserGoldmineRankHistoryContract;
use Modules\User\Contracts\UserContract;
use Plus65\Utility\Exceptions\WebException;

class GoldmineController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * The user goldmine level repository
     *
     * @var unknown
     */
    protected $userGoldmineLevelHistoryRepository;
    protected $userGoldmineRankHistoryRepository;

    protected $goldmineRankRepository;
    protected $goldmineLevelPercentageRepository;

    /**
     * The class constructor
     *
     * @param UserContract $userContract
     * @param GoldmineRankContract $goldmineRankContract
     * @param UserGoldmineRankHistoryContract $userGoldmineRankHistoryContract
     * @param UserGoldmineLevelHistoryContract $userGoldmineLevelHistoryContract
     * @param GoldmineLevelPercentageContract $goldmineLevelPercentageContract
     */
    public function __construct(
        UserContract $userContract,
        GoldmineRankContract $goldmineRankContract,
        UserGoldmineRankHistoryContract $userGoldmineRankHistoryContract,
        UserGoldmineLevelHistoryContract $userGoldmineLevelHistoryContract,
        GoldmineLevelPercentageContract $goldmineLevelPercentageContract
    ) {
        $this->middleware('permission:admin_user_member_management_goldmine_rank_edit')->only('updateRank');
        $this->middleware('permission:admin_user_member_management_goldmine_level_edit')->only('updateLevel');

        $this->userRepository                     = $userContract;
        $this->goldmineRankRepository             = $goldmineRankContract;
        $this->userGoldmineLevelHistoryRepository = $userGoldmineLevelHistoryContract;
        $this->userGoldmineRankHistoryRepository  = $userGoldmineRankHistoryContract;
        $this->goldmineLevelPercentageRepository  = $goldmineLevelPercentageContract;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        if (!$user || ($user && !$user->is_member)) {
            return redirect(route('admin.management.members.index'))->with('error', __('a_member_management_goldmine.user not found'));
        }
        $userGoldmineRank = $this->userGoldmineRankHistoryRepository->getModel()
            ->current()
            ->where('user_id', $user->id)
            ->first();

        $userGoldmineLevel = $this->userGoldmineLevelHistoryRepository->getModel()
            ->current()
            ->where('user_id', $user->id)
            ->first();

        return view('commission::admin.management.member.edit', compact('user', 'userGoldmineLevel', 'userGoldmineRank'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateGoldmineRank $request
     * @param int $id
     * @return Response
     */
    public function updateRank(UpdateGoldmineRank $request, $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user || ($user && !$user->is_member)) {
            return redirect(route('admin.management.members.index'))->with('error', __('a_member_management_goldmine.user not found'));
        }
        $userId           = $user->id;
        $userGoldmineRank = $this->userGoldmineRankHistoryRepository->getModel()
            ->current()
            ->where('user_id', $id)
            ->first();

        DB::beginTransaction();

        try {
            if (empty($request->manual_goldmine_rank_id)) {
                //go back to natural goldmine level
                if ($userGoldmineRank) {
                    $this->userRepository->edit($userId, ['goldmine_rank_id' => $userGoldmineRank->qualified_goldmine_rank_id]);
                }

                if ($userGoldmine = $this->userGoldmineRankHistoryRepository->getModel()->where('user_id', $userId)->where('is_current', 1)->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);

                    if ($userGoldmine->qualified_goldmine_rank_id > 0) {
                        $this->userGoldmineRankHistoryRepository->add([
                            'user_id'                    => $id,
                            'goldmine_rank_id'           => $userGoldmine->qualified_goldmine_rank_id,
                            'qualified_goldmine_rank_id' => $userGoldmine->qualified_goldmine_rank_id ?? null,
                            'is_locked'                  => filter_var(false, FILTER_VALIDATE_BOOLEAN),
                            'is_current'                 => 1,
                            'updated_by'                 => auth()->user()->id,
                            'is_manual'                  => false
                        ]);
                    }
                }
            } else {
                $goldmineRank = $this->goldmineRankRepository->find($request->manual_goldmine_rank_id);
                $this->userRepository->edit($userId, ['goldmine_rank_id' => $goldmineRank->id]);
                if ($userGoldmine = $this->userGoldmineRankHistoryRepository->getModel()->where('user_id', $userId)->current()->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);
                }

                $this->userGoldmineRankHistoryRepository->add([
                    'user_id'                    => $id,
                    'goldmine_rank_id'           => $goldmineRank->id,
                    'qualified_goldmine_rank_id' => $userGoldmineRank->qualified_goldmine_rank_id ?? null,
                    'is_locked'                  => filter_var($request->is_locked_rank, FILTER_VALIDATE_BOOLEAN),
                    'is_current'                 => 1,
                    'updated_by'                 => auth()->user()->id,
                    'is_manual'                  => true,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.goldmine.edit', $userId))->withMessage(__('a_member_management_goldmine.failed to update user goldmine rank'));
        }

        return back()->with('success', __('a_member_management_goldmine.user goldmine rank updated successfully'));
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateGoldmineLevel $request
     * @param int $id
     * @return Response
     */
    public function updateLevel(UpdateGoldmineLevel $request, $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user || ($user && !$user->is_member)) {
            return redirect(route('admin.management.members.index'))->with('error', __('a_member_management_goldmine.user not found'));
        }
        $userId            = $user->id;
        $userGoldmineLevel = $this->userGoldmineLevelHistoryRepository->getModel()
            ->current()
            ->where('user_id', $id)
            ->first();
        DB::beginTransaction();

        try {
            if (empty($request->manual_goldmine_level)) {
                //go back to natural goldmine level
                if ($userGoldmineLevel) {
                    $this->userRepository->edit($userId, ['goldmine_level' => $userGoldmineLevel->level]);
                }

                if ($userGoldmine = $this->userGoldmineLevelHistoryRepository->getModel()->where('user_id', $userId)->current()->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);

                    if ($userGoldmine->qualified_level > 0) {
                        $this->userGoldmineLevelHistoryRepository->add([
                            'user_id'         => $id,
                            'level'           => $userGoldmine->qualified_level,
                            'qualified_level' => $userGoldmine->qualified_level ?? null,
                            'is_locked'       => filter_var(false, FILTER_VALIDATE_BOOLEAN),
                            'is_current'      => 1,
                            'updated_by'      => auth()->user()->id,
                            'is_manual'       => false
                        ]);
                    }
                }
            } else {
                $goldmineLevel = $this->goldmineLevelPercentageRepository->find($request->manual_goldmine_level);
                $this->userRepository->edit($userId, ['goldmine_level' => $goldmineLevel->level]);
                if ($userGoldmine = $this->userGoldmineLevelHistoryRepository->getModel()->where('user_id', $userId)->current()->first()) {
                    $userGoldmine->update([
                        'is_current' => 0
                    ]);
                }

                $this->userGoldmineLevelHistoryRepository->add([
                    'user_id'         => $id,
                    'level'           => $goldmineLevel->level,
                    'qualified_level' => $userGoldmineLevel->qualified_level ?? null,
                    'is_locked'       => filter_var($request->is_locked_level, FILTER_VALIDATE_BOOLEAN),
                    'is_current'      => 1,
                    'updated_by'      => auth()->user()->id,
                    'is_manual'       => true,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('admin.management.members.goldmine.edit', $userId))->withMessage(__('a_member_management_goldmine.failed to update user goldmine level'));
        }

        return back()->with('success', __('a_member_management_goldmine.user goldmine level updated successfully'));
    }
}
