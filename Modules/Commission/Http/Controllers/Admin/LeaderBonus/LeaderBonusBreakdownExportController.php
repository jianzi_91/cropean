<?php

namespace Modules\Commission\Http\Controllers\Admin\LeaderBonus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;
use Modules\Commission\Queries\Admin\LeaderBonusBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class LeaderBonusBreakdownExportController extends Controller
{
    use CanExportTrait;
    protected $query;
    protected $leaderBonusPayoutContract;

    /**
     * Class constructor
     *
     * @param LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery
     * @param LeaderBonusPayoutContract $leaderBonusPayoutContract
     * @param GoldmineBonusPayoutContract $contract
     */
    public function __construct(LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery, LeaderBonusPayoutContract $leaderBonusPayoutContract)
    {
        $this->middleware('permission:admin_commission_leader_bonus_breakdown_export')->only('index');

        $this->query                     = $leaderBonusBreakdownQuery;
        $this->leaderBonusPayoutContract = $leaderBonusPayoutContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $userId, $type, $date)
    {
        $request->merge([
            'user_id'      => $userId,
            'payout_date'  => $date,
            'is_same_rank' => $type
        ]);

        return $this->exportReport($this->query->setParameters($request->all()), __('a_report_leader_breakdown.leader bonus breakdown') . now() . '.xlsx', auth()->user(), __('a_goldmine.goldmine breakdown') . Carbon::now()->toDateString());
    }
}
