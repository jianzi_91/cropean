<?php

namespace Modules\Commission\Http\Controllers\Admin\LeaderBonus;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;
use Modules\Commission\Queries\Admin\LeaderBonusBreakdownQuery;

class LeaderBonusBreakdownController extends Controller
{
    protected $query;
    protected $leaderBonusPayoutContract;

    /**
     * LeaderBonusBreakdownController constructor.
     * @param LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery
     * @param LeaderBonusPayoutContract $leaderBonusPayoutContract
     */
    public function __construct(LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery, LeaderBonusPayoutContract $leaderBonusPayoutContract)
    {
        $this->middleware('permission:admin_commission_leader_bonus_list')->only('index');

        $this->query                     = $leaderBonusBreakdownQuery;
        $this->leaderBonusPayoutContract = $leaderBonusPayoutContract;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $userId
     * @param $type
     * @param $date
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $userId, $type, $date)
    {
        $request->merge([
            'user_id' => $userId,
            'payout_date' => $date,
            'is_same_rank' => $type
        ]);

        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.leader_bonus.breakdown', compact('payouts', 'userId', 'type', 'date'));
    }
}
