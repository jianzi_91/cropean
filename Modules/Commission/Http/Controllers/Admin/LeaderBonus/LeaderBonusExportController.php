<?php

namespace Modules\Commission\Http\Controllers\Admin\LeaderBonus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\LeaderBonusQuery;
use QueryBuilder\Concerns\CanExportTrait;

class LeaderBonusExportController extends Controller
{
    use CanExportTrait;

    protected $query;

    /**
     * @param LeaderBonusQuery $leaderBonusQuery
     */
    public function __construct(LeaderBonusQuery $leaderBonusQuery)
    {
        $this->middleware('permission:admin_commission_leader_bonus_export')->only('index');

        $this->query = $leaderBonusQuery;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->query->setParameters($request->all()), __('a_report_leader.leader bonus') . $now . '.xlsx', auth()->user(), __('a_report_leader.leader bonus') . $now);
    }
}
