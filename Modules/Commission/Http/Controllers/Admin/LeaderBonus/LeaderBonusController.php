<?php

namespace Modules\Commission\Http\Controllers\Admin\LeaderBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\LeaderBonusQuery;

class LeaderBonusController extends Controller
{
    protected $leaderBonusQuery;

    /**
     * LeaderBonusController constructor.
     * @param LeaderBonusQuery $leaderBonusQuery
     */
    public function __construct(LeaderBonusQuery $leaderBonusQuery)
    {
        $this->leaderBonusQuery = $leaderBonusQuery;

        $this->middleware('permission:admin_commission_leader_bonus_list')->only('index');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->leaderBonusQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.leader_bonus.index', compact('payouts'));
    }
}
