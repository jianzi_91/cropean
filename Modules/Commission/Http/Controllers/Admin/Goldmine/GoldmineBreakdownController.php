<?php

namespace Modules\Commission\Http\Controllers\Admin\Goldmine;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Queries\Admin\GoldmineBreakdownQuery;

class GoldmineBreakdownController extends Controller
{
    /**
     * The goldmine breakdown query
     *
     * @var unknown
     */
    protected $goldmineQuery;

    /**
     * The goldmine payout contract
     *
     * @var unknown
     */
    protected $goldminePayoutContract;

    /**
     * Class constructor
     *
     * @param GoldmineBreakdownQuery $goldmineQuery
     * @param GoldmineBonusPayoutContract $contract
     */
    public function __construct(GoldmineBreakdownQuery $goldmineQuery, GoldmineBonusPayoutContract $contract)
    {
        $this->middleware('permission:admin_commission_goldmine_bonus_list')->only('index');

        $this->goldmineQuery          = $goldmineQuery;
        $this->goldminePayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $payoutId)
    {
        $request->merge([
            'goldmine_bonus_payout_id' => $payoutId,
        ]);

        $payouts = $this->goldmineQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.goldmine.breakdown', compact('payouts', 'payoutId'));
    }
}
