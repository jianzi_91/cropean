<?php

namespace Modules\Commission\Http\Controllers\Admin\Goldmine;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Queries\Admin\GoldmineBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class GoldmineBreakdownExportController extends Controller
{
    use CanExportTrait;

    /**
     * The goldmine breakdown query
     *
     * @var unknown
     */
    protected $query;

    /**
     * The goldmine payout contract
     *
     * @var unknown
     */
    protected $goldminePayoutContract;

    /**
     * Class constructor
     *
     * @param GoldmineBreakdownQuery $goldmineQuery
     * @param GoldmineBonusPayoutContract $contract
     */
    public function __construct(GoldmineBreakdownQuery $goldmineQuery, GoldmineBonusPayoutContract $contract)
    {
        $this->middleware('permission:admin_commission_goldmine_bonus_breakdown_export')->only('index');

        $this->query                  = $goldmineQuery;
        $this->goldminePayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $payoutId)
    {
        $request->merge([
            'goldmine_bonus_payout_id' => $payoutId,
        ]);

        return $this->exportReport($this->query->setParameters($request->all()), __('a_goldmine.goldmine breakdown') . now() . '.xlsx', auth()->user(), __('a_goldmine.goldmine breakdown') . Carbon::now()->toDateString());
    }
}
