<?php

namespace Modules\Commission\Http\Controllers\Admin\Goldmine;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\GoldmineQuery;

class GoldmineBonusController extends Controller
{
    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $goldmineQuery;

    /**
     * Class constructor
     *
     * @param GoldmineQuery $goldmineQuery
     */
    public function __construct(GoldmineQuery $goldmineQuery)
    {
        $this->middleware('permission:admin_commission_goldmine_bonus_list')->only('index');

        $this->goldmineQuery = $goldmineQuery;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->goldmineQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.goldmine.index', compact('payouts'));
    }
}
