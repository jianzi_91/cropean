<?php

namespace Modules\Commission\Http\Controllers\Admin\DirectSponsor;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\DirectSponsorQuery;
use QueryBuilder\Concerns\CanExportTrait;

class DirectSponsorExportController extends Controller
{
    use CanExportTrait;

    protected $query;

    public function __construct(DirectSponsorQuery $directSponsorQuery)
    {
        $this->middleware('permission:admin_commission_sponsor_bonus_export')->only('index');

        $this->query = $directSponsorQuery;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('a_direct_sponsor.direct sponsor') . now() . '.xlsx', auth()->user(), __('a_direct_sponsor.direct sponsor') . now()->toDateString());
    }
}
