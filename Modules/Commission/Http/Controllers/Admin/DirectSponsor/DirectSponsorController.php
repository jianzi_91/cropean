<?php

namespace Modules\Commission\Http\Controllers\Admin\DirectSponsor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Admin\DirectSponsorQuery;

class DirectSponsorController extends Controller
{
    protected $directSponsorQuery;

    public function __construct(DirectSponsorQuery $directSponsorQuery)
    {
        $this->middleware('permission:admin_commission_sponsor_bonus_list')->only('index');

        $this->directSponsorQuery = $directSponsorQuery;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->directSponsorQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::admin.direct_sponsor.index', compact('payouts'));
    }
}
