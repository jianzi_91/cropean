<?php

namespace Modules\Commission\Http\Controllers\Member\DirectSponsor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Member\DirectSponsorQuery;

class DirectSponsorController extends Controller
{
    protected $directSponsorQuery;

    public function __construct(DirectSponsorQuery $sponsorQuery)
    {
        $this->middleware('permission:member_commission_sponsor_bonus_list')->only('index');

        $this->directSponsorQuery = $sponsorQuery;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $payouts = $this->directSponsorQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.direct_sponsor.index', compact('payouts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('commission::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('commission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('commission::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    }
}
