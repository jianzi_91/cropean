<?php

namespace Modules\Commission\Http\Controllers\Member\DirectSponsor;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Member\DirectSponsorQuery;
use QueryBuilder\Concerns\CanExportTrait;

class DirectSponsorExportController extends Controller
{
    use CanExportTrait;

    protected $query;

    public function __construct(DirectSponsorQuery $directSponsorQuery)
    {
        $this->middleware('permission:member_commission_sponsor_bonus_export')->only('index');

        $this->query = $directSponsorQuery;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('m_direct_sponsor.direct sponsor') . now() . '.xlsx', auth()->user(), __('m_direct_sponsor.direct sponsor') . now()->toDateString());
    }
}
