<?php

namespace Modules\Commission\Http\Controllers\Member\LeaderBonus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;
use Modules\Commission\Http\Requests\Member\LeaderBonus\Breakdown\Index;
use Modules\Commission\Queries\Member\LeaderBonusBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class LeaderBonusBreakdownExportController extends Controller
{
    use CanExportTrait;

    protected $query;
    protected $payoutContract;

    public function __construct(LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery, LeaderBonusPayoutContract $payoutContract)
    {
        $this->middleware('permission:member_commission_leader_bonus_list')->only('index');

        $this->query          = $leaderBonusBreakdownQuery;
        $this->payoutContract = $payoutContract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Index $request, $uid, $type, $date)
    {
        $request->merge([
            'user_id'      => auth()->user()->id,
            'payout_date'  => $date,
            'is_same_rank' => $type
        ]);

        return $this->exportReport($this->query->setParameters($request->all()), __('m_report_leader_breakdown.leader bonus breakdown') . now() . '.xlsx',  auth()->user(), __('m_goldmine.goldmine breakdown') . Carbon::now()->toDateString());
    }
}
