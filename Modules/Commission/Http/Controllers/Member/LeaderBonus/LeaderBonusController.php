<?php

namespace Modules\Commission\Http\Controllers\Member\LeaderBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Member\LeaderBonusQuery;

class LeaderBonusController extends Controller
{
    protected $query;

    /**
     * LeaderBonusController constructor.
     * @param LeaderBonusQuery $leaderBonusQuery
     */
    public function __construct(LeaderBonusQuery $leaderBonusQuery)
    {
        $this->middleware('permission:member_commission_leader_bonus_list')->only('index');

        $this->query = $leaderBonusQuery;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.leader_bonus.index', compact('payouts'));
    }
}
