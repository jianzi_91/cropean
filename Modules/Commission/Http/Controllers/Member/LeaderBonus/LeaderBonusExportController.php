<?php

namespace Modules\Commission\Http\Controllers\Member\LeaderBonus;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Member\LeaderBonusQuery;
use QueryBuilder\Concerns\CanExportTrait;

class LeaderBonusExportController extends Controller
{
    use CanExportTrait;

    protected $query;

    public function __construct(LeaderBonusQuery $leaderBonusQuery)
    {
        $this->middleware('permission:member_commission_leader_bonus_export')->only('index');

        $this->query = $leaderBonusQuery;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        $now = Carbon::now()->toDateTimeString();

        return $this->exportReport($this->query->setParameters($request->all()), __('m_report_leader.leader bonus') . $now . '.xlsx', auth()->user(), __('m_report_leader.leader bonus') . $now);
    }
}
