<?php

namespace Modules\Commission\Http\Controllers\Member\LeaderBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\LeaderBonusPayoutContract;
use Modules\Commission\Http\Requests\Member\LeaderBonus\Breakdown\Index;
use Modules\Commission\Queries\Member\LeaderBonusBreakdownQuery;

class LeaderBonusBreakdownController extends Controller
{
    protected $query;
    protected $payoutContract;

    public function __construct(LeaderBonusBreakdownQuery $leaderBonusBreakdownQuery, LeaderBonusPayoutContract $payoutContract)
    {
        $this->middleware('permission:member_commission_leader_bonus_list')->only('index');

        $this->query          = $leaderBonusBreakdownQuery;
        $this->payoutContract = $payoutContract;
    }

    /**
     * Display a listing of the resource.
     * @param Index $request
     * @param $uid
     * @param $date
     * @param $type
     * @return Response
     */
    public function index(Index $request, $uid, $type, $date)
    {
        $request->merge([
            'user_id'      => auth()->user()->id,
            'payout_date'  => $date,
            'is_same_rank' => $type
        ]);

        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.leader_bonus.breakdown', compact('payouts', 'date', 'type'));
    }
}
