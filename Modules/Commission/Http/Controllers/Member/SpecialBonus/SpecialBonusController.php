<?php

namespace Modules\Commission\Http\Controllers\Member\SpecialBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Http\Middleware\UserViewSpecialBonus;
use Modules\Commission\Queries\Member\SpecialBonusQuery;

class SpecialBonusController extends Controller
{
    protected $query;

    /**
     * Class constructor
     * @param SpecialBonusQuery $query
     */
    public function __construct(SpecialBonusQuery $query)
    {
        $this->middleware('permission:member_commission_special_bonus_list')->only('index');
        $this->middleware(UserViewSpecialBonus::class)->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $payouts = $this->query
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.special_bonus.index', compact('payouts'));
    }
}
