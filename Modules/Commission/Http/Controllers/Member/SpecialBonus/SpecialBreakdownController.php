<?php

namespace Modules\Commission\Http\Controllers\Member\SpecialBonus;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\SpecialBonusPayoutContract;
use Modules\Commission\Http\Requests\Member\SpecialBonus\Breakdown\Index;
use Modules\Commission\Queries\Member\SpecialBonusBreakdownQuery;

class SpecialBreakdownController extends Controller
{
    /**
     * The special breakdown query
     *
     * @var unknown
     */
    protected $specialQuery;

    /**
     * The special payout contract
     *
     * @var unknown
     */
    protected $specialPayoutContract;

    /**
     * Class constructor
     *
     * @param SpecialBonusBreakdownQuery $specialQuery
     */
    public function __construct(SpecialBonusBreakdownQuery $specialQuery, SpecialBonusPayoutContract $contract)
    {
        $this->middleware('permission:member_commission_special_bonus_list')->only('index');

        $this->specialQuery          = $specialQuery;
        $this->specialPayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @param $payoutId
     * @return Response
     */
    public function index(Index $request, $payoutId)
    {
        $request->merge([
            'special_bonus_payout_id' => $payoutId,
        ]);

        $payout = $this->specialPayoutContract->find($payoutId);

        $payouts = $this->specialQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.special_bonus.breakdown', compact('payouts', 'payoutId', 'payout'));
    }
}
