<?php

namespace Modules\Commission\Http\Controllers\Member\Goldmine;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Http\Requests\Member\Goldmine\Breakdown\Index;
use Modules\Commission\Queries\Member\GoldmineBreakdownQuery;
use QueryBuilder\Concerns\CanExportTrait;

class GoldmineBreakdownExportController extends Controller
{
    use CanExportTrait;

    /**
     * The goldmine breakdown query
     *
     * @var unknown
     */
    protected $query;

    /**
     * The goldmine payout contract
     *
     * @var unknown
     */
    protected $goldminePayoutContract;

    /**
     * Class constructor
     *
     * @param GoldmineBreakdownQuery $query
     *
     */

    /**
     * Class constructor
     *
     * @param GoldmineBreakdownQuery $goldmineQuery
     */
    public function __construct(GoldmineBreakdownQuery $goldmineQuery, GoldmineBonusPayoutContract $contract)
    {
        $this->middleware('permission:member_commission_goldmine_bonus_export')->only('index');

        $this->query                  = $goldmineQuery;
        $this->goldminePayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Index $request, $payoutId)
    {
        $request->merge([
            'goldmine_bonus_payout_id' => $payoutId,
        ]);

        return $this->exportReport($this->query->setParameters($request->all()), __('m_goldmine.goldmine breakdown') . now() . '.xlsx', auth()->user(), __('m_goldmine.goldmine breakdown') . Carbon::now()->toDateString());
    }
}
