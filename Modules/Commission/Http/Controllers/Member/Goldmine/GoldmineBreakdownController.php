<?php

namespace Modules\Commission\Http\Controllers\Member\Goldmine;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Contracts\GoldmineBonusPayoutContract;
use Modules\Commission\Http\Requests\Member\Goldmine\Breakdown\Index;
use Modules\Commission\Queries\Member\GoldmineBreakdownQuery;

class GoldmineBreakdownController extends Controller
{
    /**
     * The goldmine breakdown query
     *
     * @var unknown
     */
    protected $goldmineQuery;

    /**
     * The goldmine payout contract
     *
     * @var unknown
     */
    protected $goldminePayoutContract;

    /**
     * Class constructor
     *
     * @param GoldmineBreakdownQuery $goldmineQuery
     */
    public function __construct(GoldmineBreakdownQuery $goldmineQuery, GoldmineBonusPayoutContract $contract)
    {
        $this->middleware('permission:member_commission_goldmine_bonus_list')->only('index');

        $this->goldmineQuery          = $goldmineQuery;
        $this->goldminePayoutContract = $contract;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Index $request
     * @param $payoutId
     * @return Response
     */
    public function index(Index $request, $payoutId)
    {
        $request->merge([
            'goldmine_bonus_payout_id' => $payoutId,
        ]);

        $payout = $this->goldminePayoutContract->find($payoutId);

        $payouts = $this->goldmineQuery
            ->setParameters($request->all())
            ->paginate();

        return view('commission::member.goldmine.breakdown', compact('payouts', 'payoutId', 'payout'));
    }
}
