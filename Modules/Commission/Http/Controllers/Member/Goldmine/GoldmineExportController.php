<?php

namespace Modules\Commission\Http\Controllers\Member\Goldmine;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Commission\Queries\Member\GoldmineQuery;
use QueryBuilder\Concerns\CanExportTrait;

class GoldmineExportController extends Controller
{
    use CanExportTrait;

    /**
     * The goldmine query
     *
     * @var unknown
     */
    protected $query;

    /**
     * Class constructor
     *
     * @param GoldmineQuery $query
     *
     */
    public function __construct(GoldmineQuery $query)
    {
        $this->middleware('permission:member_commission_goldmine_bonus_export')->only('index');

        $this->query = $query;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        return $this->exportReport($this->query->setParameters($request->all()), __('m_goldmine.goldmine') . now() . '.xlsx', auth()->user(), __('m_goldmine.goldmine') . Carbon::now()->toDateString());
    }
}
