<?php

return [
    'name'     => 'Commission',
    'bindings' => [
        \Modules\Commission\Contracts\GoldmineLevelPercentageContract::class           => \Modules\Commission\Repositories\GoldmineLevelPercentageRepository::class,
        \Modules\Commission\Contracts\GoldmineBonusPayoutContract::class               => \Modules\Commission\Repositories\GoldmineBonusPayoutRepository::class,
        \Modules\Commission\Contracts\GoldmineBonusPayoutCreditContract::class         => \Modules\Commission\Repositories\GoldmineBonusPayoutCreditRepository::class,
        \Modules\Commission\Contracts\GoldmineBonusPayoutBreakdownContract::class      => \Modules\Commission\Repositories\GoldmineBonusPayoutBreakdownRepository::class,
        \Modules\Commission\Contracts\SpecialBonusPayoutContract::class                => \Modules\Commission\Repositories\SpecialBonusPayoutRepository::class,
        \Modules\Commission\Contracts\SpecialBonusPayoutCreditContract::class          => \Modules\Commission\Repositories\SpecialBonusPayoutCreditRepository::class,
        \Modules\Commission\Contracts\SpecialBonusPayoutBreakdownContract::class       => \Modules\Commission\Repositories\SpecialBonusPayoutBreakdownRepository::class,
        \Modules\Commission\Contracts\DirectSponsorPayoutContract::class               => \Modules\Commission\Repositories\DirectSponsorPayoutRepository::class,
        \Modules\Commission\Contracts\DirectSponsorPayoutCreditContract::class         => \Modules\Commission\Repositories\DirectSponsorPayoutCreditRepository::class,
        \Modules\Commission\Contracts\DirectSponsorPayoutBreakdownContract::class      => \Modules\Commission\Repositories\DirectSponsorPayoutBreakdownRepository::class,
        \Modules\Commission\Contracts\UserGoldmineLevelHistoryContract::class          => \Modules\Commission\Repositories\UserGoldmineLevelHistoryRepository::class,
        \Modules\Commission\Contracts\UserSpecialBonusPercentageHistoryContract::class => \Modules\Commission\Repositories\UserSpecialBonusPercentageHistoryRepository::class,
        \Modules\Commission\Contracts\LeaderBonusPayoutContract::class                 => \Modules\Commission\Repositories\LeaderBonusPayoutRepository::class,
        \Modules\Commission\Contracts\LeaderBonusPayoutBreakdownContract::class        => \Modules\Commission\Repositories\LeaderBonusPayoutBreakdownRepository::class,
        \Modules\Commission\Contracts\LeaderBonusPayoutCreditContract::class           => \Modules\Commission\Repositories\LeaderBonusPayoutCreditRepository::class,
    ],
    'roi' => [
        'ratio' => 1
    ]
];
