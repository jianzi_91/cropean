<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class CommissionPayoutSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::beginTransaction();
        /*
         * Setting Category
         */
        $category = [
            'name'      => 'commission_payout_ratio',
            'is_active' => 1,
        ];

        (new SettingCategory($category))->save();

        $settings = [
            [
                'name'                => 'special_bonus_payout_cash_credit_ratio',
                'setting_category_id' => SettingCategory::where('name', 'commission_payout_ratio')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_payout_default_rollover_credit_ratio',
                'setting_category_id' => SettingCategory::where('name', 'commission_payout_ratio')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'roi_payout_default_cash_credit_ratio',
                'setting_category_id' => SettingCategory::where('name', 'commission_payout_ratio')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'goldmine_payout_cash_credit_ratio',
                'setting_category_id' => SettingCategory::where('name', 'commission_payout_ratio')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'leader_bonus_payout_cash_credit_ratio',
                'setting_category_id' => SettingCategory::where('name', 'commission_payout_ratio')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'special_bonus_payout_cash_credit_ratio')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_payout_default_rollover_credit_ratio')->first()->id,
                'value'      => '0.5',
            ],
            [
                'setting_id' => Setting::where('name', 'roi_payout_default_cash_credit_ratio')->first()->id,
                'value'      => '0.5',
            ],
            [
                'setting_id' => Setting::where('name', 'goldmine_payout_cash_credit_ratio')->first()->id,
                'value'      => '1',
            ],
            [
                'setting_id' => Setting::where('name', 'leader_bonus_payout_cash_credit_ratio')->first()->id,
                'value'      => '1',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        \DB::commit();
    }
}
