<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class GoldmineRankAndLevelTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_user_member_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Member Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_goldmine_rank_edit',
                        'title' => 'Admin User Member Management Goldmine Rank Edit',
                    ],
                    [
                        'name'  => 'admin_user_member_management_goldmine_level_edit',
                        'title' => 'Admin User Member Management Goldmine Level Edit',
                    ],
                    [
                        'name'  => 'admin_user_management_member_goldmine_level_lock',
                        'title' => 'Admin User Member Management Goldmine Level Lock',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_user_member_management_goldmine_rank_edit',
                'admin_user_member_management_goldmine_level_edit',
                'admin_user_management_member_goldmine_level_lock'
            ],
            Role::ADMIN => [
                'admin_user_member_management_goldmine_rank_edit',
                'admin_user_member_management_goldmine_level_edit',
                'admin_user_management_member_goldmine_level_lock'
            ],
            Role::MEMBER => [
            ],
            Role::MEMBER_SUSPENDED => [
            ],
            Role::MEMBER_TERMINATED => [
            ],
            Role::MEMBER_ON_HOLD => [
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
