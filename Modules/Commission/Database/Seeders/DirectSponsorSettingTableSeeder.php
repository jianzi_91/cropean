<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Contracts\SettingCategoryContract;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class DirectSponsorSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(SettingCategoryContract $categoryRepo)
    {
        DB::beginTransaction();

        $settingCategory = SettingCategory::firstOrCreate([
            'name'      => 'commissions',
            'is_active' => 1,
        ]);

        $settings = [
            [
                'name'                => 'direct_sponsor_percentage',
                'setting_category_id' => $settingCategory->id,
                'is_active'           => 1,
                'is_hidden'           => 0,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'direct_sponsor_percentage')->first()->id,
                'value'      => '0',
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
