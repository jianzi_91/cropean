<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Commission\Models\GoldmineLevelPercentage;

class GoldmineRankLevelPercentageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        $data = [
            [
                'level'      => 1,
                'percentage' => '0.15'
            ],
            [
                'level'      => 2,
                'percentage' => '0.1'
            ],
            [
                'level'      => 3,
                'percentage' => '0.08'
            ],
            [
                'level'      => 4,
                'percentage' => '0.06'
            ],
            [
                'level'      => 5,
                'percentage' => '0.06'
            ],
            [
                'level'      => 6,
                'percentage' => '0.05'
            ],
            [
                'level'      => 7,
                'percentage' => '0.05'
            ],
            [
                'level'      => 8,
                'percentage' => '0.03'
            ],
            [
                'level'      => 9,
                'percentage' => '0.03'
            ],
            [
                'level'      => 10,
                'percentage' => '0.03'
            ],
            [
                'level'      => 11,
                'percentage' => '0.02'
            ],
            [
                'level'      => 12,
                'percentage' => '0.02'
            ],
            [
                'level'      => 13,
                'percentage' => '0.02'
            ],
            [
                'level'      => 14,
                'percentage' => '0.02'
            ],
            [
                'level'      => 15,
                'percentage' => '0.01'
            ],
            [
                'level'      => 16,
                'percentage' => '0.01'
            ],
            [
                'level'      => 17,
                'percentage' => '0.01'
            ],
            [
                'level'      => 18,
                'percentage' => '0.01'
            ],
            [
                'level'      => 19,
                'percentage' => '0.01'
            ],
            [
                'level'      => 20,
                'percentage' => '0.005'
            ],
            [
                'level'      => 21,
                'percentage' => '0.005'
            ],
            [
                'level'      => 22,
                'percentage' => '0.005'
            ],
            [
                'level'      => 23,
                'percentage' => '0.005'
            ],
            [
                'level'      => 24,
                'percentage' => '0.005'
            ],
            [
                'level'      => 25,
                'percentage' => '0.005'
            ],
        ];

        foreach ($data as $d) {
            $grp = new GoldmineLevelPercentage($d);
            $grp->save();
        }
    }
}
