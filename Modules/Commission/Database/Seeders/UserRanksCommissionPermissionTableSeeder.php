<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class UserRanksCommissionPermissionTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_user_member_management' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'User Member Management',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_user_member_management_update_commission_settings',
                        'title' => 'Admin User Member Management Update Rank And Commissions Setting',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_user_member_management_update_commission_settings',
            ],
            Role::ADMIN => [
                'admin_user_member_management_update_commission_settings',
            ],
            Role::MEMBER => [
            ],
            Role::MEMBER_SUSPENDED => [
            ],
            Role::MEMBER_TERMINATED => [
            ],
            Role::MEMBER_ON_HOLD => [
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
