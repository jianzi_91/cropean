<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class CommissionPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_commission' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Commission',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_commission_goldmine_bonus_list',
                        'title' => 'Admin Commission Goldmine Bonus List',
                    ],
                    [
                        'name'  => 'admin_commission_goldmine_bonus_export',
                        'title' => 'Admin Commission Goldmine Bonus Export',
                    ],
                    [
                        'name'  => 'admin_commission_sponsor_bonus_list',
                        'title' => 'Admin Commission Sponsor Bonus List',
                    ],
                    [
                        'name'  => 'admin_commission_sponsor_bonus_export',
                        'title' => 'Admin Commission Sponsor Bonus Export',
                    ],
                    [
                        'name'  => 'admin_commission_special_bonus_list',
                        'title' => 'Admin Commission Special Bonus List',
                    ],
                    [
                        'name'  => 'admin_commission_special_bonus_export',
                        'title' => 'Admin Commission Special Bonus Export',
                    ],
                    [
                        'name'  => 'admin_commission_leader_bonus_list',
                        'title' => 'Admin Commission Leader Bonus List',
                    ],
                    [
                        'name'  => 'admin_commission_leader_bonus_export',
                        'title' => 'Admin Commission Leader Bonus Export',
                    ],
                ]
            ],
            'member_commission' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Commission',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_commission_goldmine_bonus_list',
                        'title' => 'Member Commission Goldmine Bonus List',
                    ],
                    [
                        'name'  => 'member_commission_goldmine_bonus_export',
                        'title' => 'Member Commission Goldmine Bonus Export',
                    ],
                    [
                        'name'  => 'member_commission_sponsor_bonus_list',
                        'title' => 'Member Commission Sponsor Bonus List',
                    ],
                    [
                        'name'  => 'member_commission_sponsor_bonus_export',
                        'title' => 'Member Commission Sponsor Bonus Export',
                    ],
                    [
                        'name'  => 'member_commission_special_bonus_list',
                        'title' => 'Member Commission Special Bonus List',
                    ],
                    [
                        'name'  => 'member_commission_special_bonus_export',
                        'title' => 'Member Commission Special Bonus Export',
                    ],
                    [
                        'name'  => 'member_commission_leader_bonus_list',
                        'title' => 'Member Commission Leader Bonus List',
                    ],
                    [
                        'name'  => 'member_commission_leader_bonus_export',
                        'title' => 'Member Commission Leader Bonus Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_commission_goldmine_bonus_list',
                'admin_commission_goldmine_bonus_export',
                'admin_commission_sponsor_bonus_list',
                'admin_commission_sponsor_bonus_export',
                'admin_commission_special_bonus_list',
                'admin_commission_special_bonus_export',
                'admin_commission_leader_bonus_list',
                'admin_commission_leader_bonus_export'

            ],
            Role::ADMIN => [
                'admin_commission_goldmine_bonus_list',
                'admin_commission_goldmine_bonus_export',
                'admin_commission_sponsor_bonus_list',
                'admin_commission_sponsor_bonus_export',
                'admin_commission_special_bonus_list',
                'admin_commission_special_bonus_export',
                'admin_commission_leader_bonus_list',
                'admin_commission_leader_bonus_export'
            ],
            Role::MEMBER => [
                'member_commission_goldmine_bonus_list',
                'member_commission_goldmine_bonus_export',
                'member_commission_sponsor_bonus_list',
                'member_commission_sponsor_bonus_export',
                'member_commission_special_bonus_list',
                'member_commission_special_bonus_export',
                'member_commission_leader_bonus_list',
                'member_commission_leader_bonus_export',

            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
