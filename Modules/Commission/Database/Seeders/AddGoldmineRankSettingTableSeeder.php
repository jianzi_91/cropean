<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Setting\Models\Setting;
use Modules\Setting\Models\SettingCategory;
use Modules\Setting\Models\SettingValue;

class AddGoldmineRankSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::beginTransaction();
        /*
         * Setting Category
         */
        $categories = [
            [
                'name'      => 'goldmine_setting',
                'is_active' => 1,
            ],
            [
                'name'      => 'leader_bonus_setting',
                'is_active' => 1,
            ]
        ];

        foreach ($categories as $category) {
            (new SettingCategory($category))->save();
        }

        $settings = [
            [
                'name'                => 'is_manual_goldmine_rankup_activated',
                'setting_category_id' => SettingCategory::where('name', 'goldmine_setting')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
            [
                'name'                => 'is_manual_leader_bonus_rankup_activated',
                'setting_category_id' => SettingCategory::where('name', 'leader_bonus_setting')->first()->id,
                'is_active'           => 1,
                'is_hidden'           => 1,
            ],
        ];

        foreach ($settings as $setting) {
            (new Setting($setting))->save();
        }

        $settingValues = [
            [
                'setting_id' => Setting::where('name', 'is_manual_goldmine_rankup_activated')->first()->id,
                'value'      => 0,
            ],
            [
                'setting_id' => Setting::where('name', 'is_manual_leader_bonus_rankup_activated')->first()->id,
                'value'      => 0,
            ],
        ];

        foreach ($settingValues as $settingValue) {
            (new SettingValue($settingValue))->save();
        }

        DB::commit();
    }
}
