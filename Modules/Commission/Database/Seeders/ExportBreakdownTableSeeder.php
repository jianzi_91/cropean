<?php

namespace Modules\Commission\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class ExportBreakdownTableSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_commission' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Commission',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_commission_goldmine_bonus_breakdown_export',
                        'title' => 'Admin Commission Goldmine Bonus Breakdown Export',
                    ],
                    [
                        'name'  => 'admin_commission_leader_bonus_breakdown_export',
                        'title' => 'Admin Commission Leader Bonus Breakdown Export',
                    ],
                ]
            ],
            'member_commission' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Commission',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_commission_goldmine_bonus_breakdown_export',
                        'title' => 'Member Commission Goldmine Bonus Breakdown Export',
                    ],
                    [
                        'name'  => 'member_commission_leader_bonus_breakdown_export',
                        'title' => 'Member Commission Leader Bonus Breakdown Export',
                    ],
                ],
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_commission_goldmine_bonus_breakdown_export',
                'admin_commission_leader_bonus_breakdown_export'
            ],
            Role::ADMIN => [
                'admin_commission_goldmine_bonus_breakdown_export',
                'admin_commission_leader_bonus_breakdown_export'
            ],
            Role::MEMBER => [
                'member_commission_goldmine_bonus_breakdown_export',
                'member_commission_leader_bonus_breakdown_export'
            ],
            Role::MEMBER_SUSPENDED => [
            ],
            Role::MEMBER_TERMINATED => [
            ],
            Role::MEMBER_ON_HOLD => [
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
