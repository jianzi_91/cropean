<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoiPayoutCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roi_payout_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('roi_payout_id');
            $table->unsignedInteger('bank_credit_type_id');
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);
            $table->string('transaction_code');
            $table->unsignedDecimal('exchange_rate', 20, 6)->default(1);
            $table->unsignedDecimal('issued_amount', 40, 20);

            $table->foreign('roi_payout_id')
                ->references('id')
                ->on('roi_payouts');

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('transaction_code')
                ->references('transaction_code')
                ->on('bank_account_statements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roi_payout_credits');
    }
}
