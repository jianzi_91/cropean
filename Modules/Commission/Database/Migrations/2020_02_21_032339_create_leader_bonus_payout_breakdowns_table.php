<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaderBonusPayoutBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_bonus_payout_breakdowns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('leader_bonus_payout_id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);

            $table->foreign('leader_bonus_payout_id', 'leader_bonus_payout_foreign')
                ->references('id')
                ->on('leader_bonus_payouts');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_bonus_payout_breakdowns');
    }
}
