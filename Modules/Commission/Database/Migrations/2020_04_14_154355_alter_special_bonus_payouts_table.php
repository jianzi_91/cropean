<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpecialBonusPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('special_bonus_payouts', function (Blueprint $table) {
            $table->boolean('is_credit_issued')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('special_bonus_payouts', function (Blueprint $table) {
            if (Schema::hasColumn('special_bonus_payouts', 'is_credit_issued')) {
                $table->dropColumn(['is_credit_issued']);
            }
        });
    }
}
