<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaderBonusPayoutCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leader_bonus_payout_credits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('leader_bonus_payout_id');
            $table->unsignedInteger('bank_credit_type_id');
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);
            $table->string('transaction_code');

            $table->foreign('leader_bonus_payout_id', 'leader_bonus_payout_credit_foreign')
                ->references('id')
                ->on('leader_bonus_payouts');

            $table->foreign('bank_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('transaction_code')
                ->references('transaction_code')
                ->on('bank_account_statements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leader_bonus_payout_credits');
    }
}
