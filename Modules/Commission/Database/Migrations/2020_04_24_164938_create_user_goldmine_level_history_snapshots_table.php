<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGoldmineLevelHistorySnapshotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_goldmine_level_history_snapshots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedBigInteger('level')->index();
            $table->unsignedBigInteger('qualified_level')->nullable();
            $table->boolean('is_current')->default(1);
            $table->boolean('is_manual')->default(0);
            $table->boolean('is_locked')->default(0);
            $table->unsignedInteger('updated_by')->nullable()->index();
            $table->date('run_date');
        });

        Schema::table('user_goldmine_level_history_snapshots', function ($table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_goldmine_level_history_snapshots');
    }
}
