<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGoldmineBonusPayoutCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goldmine_bonus_payout_credits', function (Blueprint $table) {
            $table->unsignedDecimal('exchange_rate', 20, 6)->default(1);
            $table->unsignedDecimal('issued_amount', 40, 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goldmine_bonus_payout_credits', function (Blueprint $table) {
            if (Schema::hasColumn('goldmine_bonus_payout_credits', 'exchange_rate')) {
                $table->dropColumn(['exchange_rate']);
            }
            if (Schema::hasColumn('goldmine_bonus_payout_credits', 'issued_amount')) {
                $table->dropColumn(['issued_amount']);
            }
        });
    }
}
