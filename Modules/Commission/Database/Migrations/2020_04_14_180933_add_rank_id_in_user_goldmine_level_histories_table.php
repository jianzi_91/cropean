<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRankIdInUserGoldmineLevelHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goldmine_level_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('goldmine_rank_id')->index()->after('user_id');
            $table->unsignedBigInteger('qualified_goldmine_rank_id')->nullable()->after('level');
            $table->unsignedBigInteger('qualified_level')->nullable()->after('qualified_goldmine_rank_id');
            $table->boolean('is_manual')->default(0)->after('qualified_level');

            $table->foreign('goldmine_rank_id')
                ->references('id')
                ->on('goldmine_ranks');

            $table->foreign('qualified_goldmine_rank_id')
                ->references('id')
                ->on('goldmine_ranks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goldmine_level_histories', function (Blueprint $table) {
            $table->dropForeign('goldmine_rank_id');
            $table->dropForeign('qualified_goldmine_rank_id');
            $table->dropForeign('qualified_level');

            $table->dropColumn('goldmine_rank_id');
            $table->dropColumn('qualified_goldmine_rank_id');
            $table->dropColumn('qualified_level');
            $table->dropColumn('is_manual');
        });
    }
}
