<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoldmineBonusPayoutBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goldmine_bonus_payout_breakdowns', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('goldmine_bonus_payout_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('level');
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);

            $table->foreign('goldmine_bonus_payout_id', 'goldmine_id_foreign')
                ->references('id')
                ->on('goldmine_bonus_payouts');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goldmine_bonus_payout_breakdowns');
    }
}
