<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGoldmineBonusPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goldmine_bonus_payouts', function (Blueprint $table) {
            $table->unsignedBigInteger('goldmine_rank_id')->nullable();
            $table->unsignedBigInteger('goldmine_level')->nullable();

            $table->foreign('goldmine_rank_id')->references('id')->on('goldmine_ranks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goldmine_bonus_payouts', function (Blueprint $table) {
            if (Schema::hasColumn('goldmine_bonus_payouts', 'goldmine_rank_id')) {
                $table->dropForeign(['goldmine_rank_id']);
                $table->dropColumn(['goldmine_rank_id']);
            }
            if (Schema::hasColumn('goldmine_bonus_payouts', 'goldmine_level')) {
                $table->dropColumn(['goldmine_level']);
            }
        });
    }
}
