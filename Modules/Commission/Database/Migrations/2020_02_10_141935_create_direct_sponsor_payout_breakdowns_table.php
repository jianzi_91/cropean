<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectSponsorPayoutBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_sponsor_payout_breakdowns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('direct_sponsor_payout_id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('amount', 40, 20);

            $table->foreign('direct_sponsor_payout_id', 'direct_sponsor_id_foreign')
                ->references('id')
                ->on('direct_sponsor_payouts');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_sponsor_payout_breakdowns');
    }
}
