<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLeaderBonusPayoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leader_bonus_payouts', function (Blueprint $table) {
            $table->unsignedBigInteger('rank_id')->nullable();
            $table->boolean('is_credit_issued')->default(0);
            $table->boolean('is_same_rank')->default(0);

            $table->foreign('rank_id')
                ->references('id')
                ->on('ranks');
        });

        Schema::table('leader_bonus_payout_breakdowns', function (Blueprint $table) {
            $table->boolean('is_same_rank')->default(0);
        });

        Schema::table('leader_bonus_payout_credits', function (Blueprint $table) {
            $table->unsignedDecimal('exchange_rate', 20, 6)->default(1);
            $table->unsignedDecimal('issued_amount', 40, 20);
            $table->unsignedInteger('bank_transaction_type_id')->after('bank_credit_type_id');

            $table->foreign('bank_transaction_type_id')
                ->references('id')
                ->on('bank_transaction_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leader_bonus_payouts', function (Blueprint $table) {
            if (Schema::hasColumn('leader_bonus_payouts', 'rank_id')) {
                $table->dropForeign(['rank_id']);
                $table->dropColumn(['rank_id']);
            }
            if (Schema::hasColumn('leader_bonus_payouts', 'is_credit_issued')) {
                $table->dropColumn(['is_credit_issued']);
            }
            if (Schema::hasColumn('leader_bonus_payouts', 'is_same_rank')) {
                $table->dropColumn(['is_same_rank']);
            }
        });

        Schema::table('leader_bonus_payout_breakdowns', function (Blueprint $table) {
            if (Schema::hasColumn('leader_bonus_payout_breakdowns', 'is_same_rank')) {
                $table->dropColumn(['is_same_rank']);
            }
        });

        Schema::table('leader_bonus_payout_credits', function (Blueprint $table) {
            if (Schema::hasColumn('leader_bonus_payout_credits', 'exchange_rate')) {
                $table->dropColumn(['exchange_rate']);
            }
            if (Schema::hasColumn('leader_bonus_payout_credits', 'issued_amount')) {
                $table->dropColumn(['issued_amount']);
            }
            if (Schema::hasColumn('leader_bonus_payout_credits', 'bank_transaction_type_id')) {
                $table->dropForeign(['bank_transaction_type_id']);
                $table->dropColumn(['bank_transaction_type_id']);
            }
        });
    }
}
