<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLeaderBonusPayoutCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leader_bonus_payout_credits', function (Blueprint $table) {
            $table->unsignedDecimal('converted_amount', 40, 20)->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leader_bonus_payout_credits', function (Blueprint $table) {
            if (Schema::hasColumn('leader_bonus_payout_credits', 'converted_amount')) {
                $table->dropColumn(['converted_amount']);
            }
        });
    }
}
