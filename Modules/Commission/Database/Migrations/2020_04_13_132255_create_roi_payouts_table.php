<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoiPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roi_payouts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('payout_date')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('total_equity', 40, 20);
            $table->unsignedDecimal('amount', 40, 20);
            $table->unsignedDecimal('percentage', 10, 6);
            $table->unsignedDecimal('computed_amount', 40, 20);
            $table->unsignedInteger('roi_tier_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('roi_tier_id')
                ->references('id')
                ->on('roi_tiers');

            // Useful for getting all payouts by date
            $table->index(['payout_date', 'user_id', 'amount'], 'date_payout_index');

            // Useful for getting all payouts by user
            $table->index(['user_id', 'payout_date', 'amount'], 'user_payout_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roi_payouts');
    }
}
