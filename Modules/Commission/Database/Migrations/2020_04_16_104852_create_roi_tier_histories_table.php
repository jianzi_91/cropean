<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoiTierHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roi_tier_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('roi_tier_id')->index();
            $table->decimal('payout_percentage', 10, 10);
            $table->text('payout_credits');
            $table->boolean('is_current')->default(1);
            $table->unsignedInteger('updated_by')->nullable();

            $table->foreign('roi_tier_id')
                ->references('id')
                ->on('roi_tiers');

            $table->foreign('updated_by')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roi_tier_histories');
    }
}
