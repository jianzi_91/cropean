<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectSponsorPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_sponsor_payouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();

            $table->date('payout_date')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('amount', 40, 20);
            $table->unsignedDecimal('percentage', 5, 4);
            $table->unsignedDecimal('computed_amount', 40, 20);

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            // Useful for getting all payouts by date
            $table->index(['payout_date', 'user_id', 'amount'], 'date_payout_index');

            // Useful for getting all payouts by user
            $table->index(['user_id', 'payout_date', 'amount'], 'user_payout_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_sponsor_payouts');
    }
}
