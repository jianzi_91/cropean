<?php

namespace Modules\Commission\Contracts;

use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface DirectSponsorPayoutBreakdownContract extends CrudContract, BulkActionContract
{
}
