<?php

namespace Modules\Commission\Contracts;

use Carbon\Carbon;
use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface DirectSponsorPayoutContract extends CrudContract, BulkActionContract
{
    /**
     * @param int $precision
     * @return mixed
     */
    public function calculate($sales);

    /**
     * @param Carbon $payoutDate
     * @return mixed
     */
    public function setPayoutDate(Carbon $payoutDate);

    /**
     * @return mixed
     */
    public function setNonEligibleUsers();
}
