<?php

namespace Modules\Commission\Contracts;

use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface GoldmineBonusPayoutBreakdownContract extends CrudContract, BulkActionContract
{
	// add functions here    
}