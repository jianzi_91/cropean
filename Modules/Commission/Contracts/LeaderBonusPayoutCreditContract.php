<?php

namespace Modules\Commission\Contracts;

use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;

interface LeaderBonusPayoutCreditContract extends CrudContract, BulkActionContract
{
    // add functions here
}
