<?php

namespace Modules\Commission\Contracts;

use Modules\Core\Contracts\BulkActionContract;
use Plus65\Base\Repositories\Contracts\CrudContract;
use Plus65\Base\Repositories\Contracts\SlugContract;

interface GoldmineLevelPercentageContract extends CrudContract, SlugContract, BulkActionContract
{
}
