<?php

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::group(['prefix' => 'reports'], function () {
            Route::get('goldmine/{payout_id}/breakdown/export', ['as' => 'admin.report.goldmine.breakdown.export', 'uses' => 'Admin\Goldmine\GoldmineBreakdownExportController@index']);
            Route::get('goldmine/{payout_id}/breakdown', ['as' => 'admin.report.goldmine.breakdown', 'uses' => 'Admin\Goldmine\GoldmineBreakdownController@index']);
            Route::get('goldmine/export', ['as' => 'admin.report.goldmine.export', 'uses' => 'Admin\Goldmine\GoldmineExportController@index']);
            Route::resource('goldmine', 'Admin\Goldmine\GoldmineBonusController', ['as' => 'admin.report'])->only(['index']);

            Route::get('special/{payout_id}/breakdown', ['as' => 'admin.report.special.breakdown', 'uses' => 'Admin\SpecialBonus\SpecialBreakdownController@index']);
            Route::get('special/export', ['as' => 'admin.report.special.export', 'uses' => 'Admin\SpecialBonus\SpecialBonusExportController@index']);
            Route::resource('special', 'Admin\SpecialBonus\SpecialBonusController', ['as' => 'admin.report'])->only(['index']);

            Route::get('leader/{uid}/{type}/{date}/breakdown', ['as' => 'admin.report.leader.breakdown', 'uses' => 'Admin\LeaderBonus\LeaderBonusBreakdownController@index']);
            Route::get('leader/{uid}/{type}/{date}/breakdown/export', ['as' => 'admin.report.leader.breakdown.export', 'uses' => 'Admin\LeaderBonus\LeaderBonusBreakdownExportController@index']);
            Route::get('leader/export', ['as' => 'admin.report.leader.export', 'uses' => 'Admin\LeaderBonus\LeaderBonusExportController@index']);
            Route::resource('leader', 'Admin\LeaderBonus\LeaderBonusController', ['as' => 'admin.report'])->only(['index']);
        });

        Route::get('sponsor/export', ['as' => 'admin.report.sponsor.export', 'uses' => 'Admin\DirectSponsor\DirectSponsorExportController@index']);
        Route::resource('sponsor', 'Admin\DirectSponsor\DirectSponsorController', ['as' => 'admin.report'])->only(['index']);

        Route::get('members/{id}/goldmine/edit', ['as' => 'admin.management.members.goldmine.edit', 'uses' => 'Admin\Management\Member\GoldmineController@edit']);
        Route::put('members/{id}/goldmine/level', ['as' => 'admin.management.members.goldmine.level.update', 'uses' => 'Admin\Management\Member\GoldmineController@updateLevel']);
        Route::put('members/{id}/goldmine/rank', ['as' => 'admin.management.members.goldmine.rank.update', 'uses' => 'Admin\Management\Member\GoldmineController@updateRank']);

        #admin manage user special bonus
        Route::get('members/{id}/special/edit', ['as' => 'admin.management.members.special.edit', 'uses' => 'Admin\Management\Member\SpecialBonusController@edit']);
        Route::put('members/{id}/special', ['as' => 'admin.management.members.special.update', 'uses' => 'Admin\Management\Member\SpecialBonusController@update']);
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('goldmine/{payout_id}/breakdown/export', ['as' => 'member.report.goldmine.breakdown.export', 'uses' => 'Member\Goldmine\GoldmineBreakdownExportController@index']);
        Route::get('goldmine/{payout_id}/breakdown', ['as' => 'member.report.goldmine.breakdown', 'uses' => 'Member\Goldmine\GoldmineBreakdownController@index']);
        Route::get('goldmine/export', ['as' => 'member.report.goldmine.export', 'uses' => 'Member\Goldmine\GoldmineExportController@index']);
        Route::resource('goldmine', 'Member\Goldmine\GoldmineBonusController', ['as' => 'member.report'])->only(['index']);

        Route::get('special/{payout_id}/breakdown', ['as' => 'member.report.special.breakdown', 'uses' => 'Member\SpecialBonus\SpecialBreakdownController@index']);
        Route::get('special/export', ['as' => 'member.report.special.export', 'uses' => 'Member\SpecialBonus\SpecialBonusExportController@index']);
        Route::resource('special', 'Member\SpecialBonus\SpecialBonusController', ['as' => 'member.report'])->only(['index']);

        Route::get('sponsor/export', ['as' => 'member.report.sponsor.export', 'uses' => 'Member\DirectSponsor\DirectSponsorExportController@index']);
        Route::resource('sponsor', 'Member\DirectSponsor\DirectSponsorController', ['as' => 'member.report'])->only(['index']);

        Route::get('leader/{uid}/{type}/{date}/breakdown', ['as' => 'member.leader.breakdown.report', 'uses' => 'Member\LeaderBonus\LeaderBonusBreakdownController@index']);
        Route::get('leader/{uid}/{type}/{date}/breakdown/export', ['as' => 'member.leader.breakdown.report.export', 'uses' => 'Member\LeaderBonus\LeaderBonusBreakdownExportController@index']);
        Route::get('leader/export', ['as' => 'member.report.leader.export', 'uses' => 'Member\LeaderBonus\LeaderBonusExportController@index']);
        Route::resource('leader', 'Member\LeaderBonus\LeaderBonusController', ['as' => 'member.report'])->only(['index']);
    });
});
