@extends('templates.member.master')
@section('title', __('m_page_title.deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
        ['name'=>__('s_breadcrumb.deposit usdt')]
    ],
    'header'=> __('m_wallet_deposit.usdt deposit')
])

@can('member_blockchain_deposit_create')
<div class="card">
    <div class="card-content">
        <div class="card-body">
            <h4 class="card-title filter-title p-0 m-0">{{ __('m_wallet_deposit.deposit wallet address') }}</h4>
            <div class="d-flex flex-row">
                <div class="col-10 d-flex flex-column justify-content-end pl-0">
                    <div class="widget-sublabel pb-1">{{ __('m_wallet_deposit.please be informed you will have to deposit at least :minimum amount', ['minimum' => setting('minimum_usdt_erc20_deposit_redirection_amount')]) }}</div>
                    <div class="row justify-self-center">
                        <div class="col-lg-9 col-xs-12">
                            {{ Form::formText('wallet_address', $wallet->address, __('m_wallet_deposit.deposit wallet address'), ['readonly'], false) }}
                        </div>
                        <div class="col-lg-3 col-xs-3 pt-lg-2">
                        <button id="copy_btn" class="btn btn-primary cro-btn-primary" onclick="copyToClipboard('#wallet_address')">{{__('m_wallet_deposit.copy to clipboard')}}</button>
                        </div>
                    </div>
                </div>
                <div class="col-2 p-0 d-flex justify-content-center align-items-center">
                    <img style="max-width:120px" src="{{ route('member.blockchain.deposits.qr', $wallet->bank_credit_type_id) }}"/>
                </div>
            </div>
        </div>
    </div>
</div>
@endcan

@component('templates.__fragments.components.filter')        
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at'), __('m_wallet_deposit.date'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('transaction_hash', request('transaction_hash'), __('m_wallet_deposit.transaction hash'), [], false) }}
</div>
<div class="col-12 col-sm-6 col-md-4 col-xl-3">
    {{ Form::formText('reference_number', request('reference_number'), __('m_wallet_deposit.reference number'), [], false) }}
</div>
@endcomponent


<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('m_wallet_deposit.usdt deposit statements')}}</h4>
            @can('member_blockchain_deposit_export')
                <a class="btn btn-primary" href="{{ route('member.blockchain.deposits.usdt_erc20.export', array_merge(request()->except('page'))) }}">{{ __('m_wallet_deposit.export table') }}</a>
            @endcan
        </div>
        @component('templates.__fragments.components.tables')
        <thead class="text-capitalize">
            <tr>
                <th>{{ __('m_wallet_deposit.date') }}</th>
                <th>{{ __('m_wallet_deposit.reference number') }}</th>
                <th>{{ __('m_wallet_deposit.transaction hash') }}</th>
                <th>{{ __('m_wallet_deposit.deposit amount usdt') }}</th>
                <th>{{ __('m_wallet_deposit.no of confirmations') }}</th>
            </tr>
        </thead>
        <tbody>
        @forelse ($deposits as $deposit)
            <tr>
                <td>{{ $deposit->created_at }}</td>
                <td>{{ isset($deposit->reference_number) ? $deposit->reference_number : '-' }}</td>
                <td>{{ $deposit->transaction_hash }}</td>
                <td>{{ amount_format($deposit->amount, credit_precision(\Modules\Credit\Contracts\BankCreditTypeContract::USDT_ERC20)) }}</td>
                <td>{{ $deposit->confirmations }}</td>
            </tr>
            @empty
            @include('templates.__fragments.components.no-table-records', [ 'span' => 5, 'text' => __('m_wallet_deposit.no records') ])
        @endforelse
        </tbody>
        @endcomponent
    </div>
</div>
{!! $deposits->render() !!}
@endsection


@push('scripts')
<script type="text/javascript">
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endpush