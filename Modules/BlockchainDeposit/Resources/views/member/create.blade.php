@extends('templates.member.master')
@section('title', __('m_page_title.deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',['breadcrumbs'=>[
    ['name'=>__('s_breadcrumb.home'), 'route' => route('member.dashboard')],
    ['name'=>__('s_breadcrumb.deposits'), 'route' => route('member.blockchain.deposits.index')],
    ['name'=>__('s_breadcrumb.cryptocurrency'), 'route' => route('member.blockchain.deposits.index')],
    ['name'=>__('s_breadcrumb.new deposit')]
]])

<div class="row mt-3">
    <div class="col-lg-8 col-md-7 col-xs-12 order-2 order-md-1">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h4 class="mt-0 mb-3 header-title">{{ __('m_wallet_deposit_create.deposit wallet address') }}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex align-items-center">
                        <div>
                            {{ Form::formText('wallet_address', "asd asd asd", __('m_wallet_deposit_create.please be informed you will have to deposit at least x amount'), ['readonly'=>true]) }}

                            <button class="btn btn-primary" onclick="CopyText('wallet_address')">{{ __('m_wallet_deposit_create.copy to clipboard') }}</button>
                        </div>
                        <div>
                            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate($value)) !!} " class="w-100" />
                        </div>
                        
                        {{--
                        @include('templates.__fragments.components.qrcode-widget', [
                        'address'=> $depositWallet->address,
                        'currency'=> $depositWallet->creditType->name,
                        'imgUrl'=> route('member.blockchain.deposits.qr', $depositWallet->creditType->id),
                        'disclaimer' => __('m_wallet_deposit_create.minimum deposit :minimum :currency', ['minimum' => setting('minimum_usdt_erc20_deposit_redirection_amount'), 'currency' => $depositWallet->creditType->name])
                        ])
                        --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-5 col-xs-12 order-1 order-md-2">
        @include('templates.__fragments.components.box', ['title'=>__('m_wallet_deposit_create.usd credit balance'), 'value'=>'xxx'])
    </div>
</div>
@endsection
