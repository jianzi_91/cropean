@extends('templates.admin.master')
@section('title', __('a_member_management_deposit_addresses.deposit wallet addresses'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',
    [
    'filter'=>'false',
    'breadcrumbs' => [
        ['name' => __('a_member_management_deposit_addresses.dashboard'), 'route' => route('admin.dashboard')],
        ['name' => __('a_member_management_deposit_addresses.member management'), 'route' => route('admin.management.members.index')],
        ['name' => __('a_member_management_deposit_addresses.deposit wallet addresses')]
    ],
    'header'=>__('a_member_management_deposit_addresses.deposit wallet addresses'),
    'backRoute'=>route('admin.management.members.index'),
])

@include('templates.admin.includes._mm-nav', ['page' => 'deposit address', 'uid' => $userId])

<br />




@foreach ($wallets as $wallet)
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body d-flex flex-row pl-1">
                        <div class="col-lg-9 col-xs-10">
                            <h4 class="card-title p-0 m-0">{{ $wallet->creditType->name }}</h4>
                            <div class="align-self-center pt-2">
                                {{ Form::formText($loop->index, $wallet->address, '', ['readonly'], false) }}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-2 d-flex flex-wrap-reverse pl-0">
                            <div class="d-flex align-items-lg-center pt-2">
                                <button id="copy_btn" class="btn btn-primary cro-btn-primary" onclick='copyToClipboard("#{{$loop->index}}")'>{{__('a_member_management_deposit_addresses.copy to clipboard')}}</button>
                            </div>
                            <div class="d-flex align-items-end justify-content-lg-end p-0 m-0">
                                <img style="max-width:120px" src="{{ route('admin.blockchain.deposit-addresses.member.qr', ['userId' => $userId, 'creditType' => $wallet->bank_credit_type_id]) }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <p>{{ $wallet->address }}</p> -->
    <!-- <p>{{ $wallet->creditType->name }}</p> -->
    <!-- <img style="max-width:120px" src="{{ route('admin.blockchain.deposit-addresses.member.qr', ['userId' => $userId, 'creditType' => $wallet->bank_credit_type_id]) }}"/> -->
@endforeach

@endsection

@push('scripts')
<script type="text/javascript">
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endpush

