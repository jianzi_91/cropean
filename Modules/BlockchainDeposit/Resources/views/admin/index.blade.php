@extends('templates.admin.master')
@section('title', __('a_page_title.deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.usdt deposit')]
    ],
    'header'=>__('a_wallet_deposit.usdt deposit')
])

@component('templates.__fragments.components.filter')        
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('created_at', old('created_at', request('created_at')),__('a_wallet_deposit.date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('transaction_hash', old('transaction_hash', request('transaction_hash')),__('a_wallet_deposit.transaction hash'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('reference_number', old('reference_number', request('reference_number')),__('a_wallet_deposit.reference number'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('sender_wallet', old('sender_wallet'), __('a_wallet_deposit.sender wallet address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('reciever_wallet', old('receiver_wallet'), __('a_wallet_deposit.receiver wallet address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('name', old('name'), __('a_wallet_deposit.full name'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('member_id', old('member_id'), __('a_wallet_deposit.member id'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('email', old('email'), __('a_wallet_deposit.email address'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_wallet_deposit.deposit transactions')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_wallet_deposit.total results:') }} {{ $deposits->total() }}</div>
                @can('admin_blockchain_deposit_export')
                    <a class="btn btn-secondary" href="{{ route('admin.blockchain.deposits.export', array_merge(request()->except('page'))) }}">{{ __('a_wallet_deposit.export table') }}</a>
                @endcan
            </div>
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_wallet_deposit.date') }}</th>
                    <th>{{ __('a_wallet_deposit.reference number') }}</th>
                    <th>{{ __('a_wallet_deposit.full name') }}</th>
                    <th>{{ __('a_wallet_deposit.member id') }}</th>
                    <th>{{ __('a_wallet_deposit.email address') }}</th>
                    <th>{{ __('a_wallet_deposit.sender wallet address') }}</th>
                    <th>{{ __('a_wallet_deposit.receiver wallet address') }}</th>
                    <th>{{ __('a_wallet_deposit.transaction hash') }}</th>
                    <th>{{ __('a_wallet_deposit.amount') }}</th>
                    <th>{{ __('a_wallet_deposit.no of confirmations') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                </tr>
                <!-- @include('templates.__fragments.components.no-table-records', [ 'span' => 5, 'text' => __('a_wallet_deposit.no records') ]) -->
            </tbody>
        @endcomponent
    </div>
</div>
{{-- {!! $deposits->render() !!} --}}
</div>
@endsection