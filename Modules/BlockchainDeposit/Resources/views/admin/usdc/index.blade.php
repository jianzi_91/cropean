@extends('templates.admin.master')
@section('title', __('a_page_title.deposits'))

@section('main')
@include('templates.__fragments.components.breadcrumbs',[
    'breadcrumbs'=>[
        ['name'=>__('s_breadcrumb.home'), 'route' => route('admin.dashboard')],
        ['name'=>__('s_breadcrumb.usdc deposit')]
    ],
    'header'=>__('a_wallet_usdc_deposit.usdc deposit')
])

@component('templates.__fragments.components.filter')        
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formDateRange('created_at', request('created_at', request('created_at')),__('a_wallet_usdc_deposit.date'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('transaction_hash', request('transaction_hash', request('transaction_hash')),__('a_wallet_usdc_deposit.transaction hash'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('reference_number', request('reference_number', request('reference_number')),__('a_wallet_usdc_deposit.reference number'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('sending_address', request('sending_address'), __('a_wallet_usdc_deposit.sender wallet address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('receiving_address', request('receiving_address'), __('a_wallet_usdc_deposit.receiver wallet address'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('name', request('name'), __('a_wallet_usdc_deposit.full name'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('member_id', request('member_id'), __('a_wallet_usdc_deposit.member id'), [], false) }}
</div>
<div class="col-12 col-lg-6 col-xl-3">
    {{ Form::formText('email', request('email'), __('a_wallet_usdc_deposit.email address'), [], false) }}
</div>
@endcomponent

<div class="card">
    <div class="card-content">
        <div class="card-body d-flex justify-content-between align-items-center p-2">
            <h4 class="card-title filter-title p-0 m-0">{{__('a_wallet_usdc_deposit.deposit transactions')}}</h4>
            <div class="d-flex flex-row align-items-center">
                <div class="table-total-results mr-2">{{ __('a_wallet_usdc_deposit.total results:') }} {{ $deposits->total() }}</div>
                @can('admin_blockchain_deposit_export')
                    <a class="btn btn-secondary" href="{{ route('admin.blockchain.deposits.usdc.export', array_merge(request()->except('page'))) }}">{{ __('a_wallet_usdc_deposit.export table') }}</a>
                @endcan
            </div>  
        </div>
        @component('templates.__fragments.components.tables')
            <thead class="text-capitalize">
                <tr>
                    <th>{{ __('a_wallet_usdc_deposit.date') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.reference number') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.full name') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.member id') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.email address') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.sender wallet address') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.receiver wallet address') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.transaction hash') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.amount') }}</th>
                    <th>{{ __('a_wallet_usdc_deposit.no of confirmations') }}</th>
                </tr>
            </thead>
            <tbody>
            @forelse($deposits as $deposit)
                <tr>
                    <td>{{ $deposit->created_at }}</td>
                    <td>{{ $deposit->reference_number }}</td>
                    <td>{{ $deposit->name }}</td>
                    <td>{{ $deposit->member_id }}</td>
                    <td>{{ $deposit->email }}</td>
                    <td>{{ $deposit->sending_address }}</td>
                    <td>{{ $deposit->receiving_address }}</td>
                    <td>{{ $deposit->transaction_hash }}</td>
                    <td>{{ amount_format($deposit->amount, 2) }}</td>
                    <td>{{ $deposit->confirmations }}</td>
                </tr>
            @empty
                @include('templates.__fragments.components.no-table-records', [ 'span' => 10, 'text' => __('a_wallet_usdc_deposit.no records') ])
            @endforelse
            </tbody>
        @endcomponent
    </div>
</div>
{{-- {!! $deposits->render() !!} --}}
</div>
@endsection

@push('scripts')
<script>
    $(function(){
        $('#external_transfer').on('click', function(e){
            e.preventDefault();
            window.location.href = "{{ route('admin.blockchain.withdrawals.index') }}"
        })
    })
</script>
@endpush