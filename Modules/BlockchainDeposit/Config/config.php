<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Repository and Contract bindings
    |
    |--------------------------------------------------------------------------
    */
    'bindings' => [
        \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::class        => \Modules\BlockchainDeposit\Repositories\BlockchainDepositRepository::class,
        \Modules\BlockchainDeposit\Contracts\BlockchainDepositInvoiceContract::class => \Modules\BlockchainDeposit\Repositories\BlockchainDepositInvoiceRepository::class,
    ],
    'invoice_storage' => 'blockchain_invoices',
];
