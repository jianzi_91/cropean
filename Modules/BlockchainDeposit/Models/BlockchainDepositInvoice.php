<?php

namespace Modules\BlockchainDeposit\Models;

use Illuminate\Database\Eloquent\Model;

class BlockchainDepositInvoice extends Model
{
    protected $table   = "blockchain_deposit_invoices";
    protected $guarded = [];
}
