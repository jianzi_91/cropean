<?php

namespace Modules\BlockchainDeposit\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Credit\Models\BankCreditType;
use Modules\User\Models\User;
use Plus65\Base\Models\Relations\UserRelation;

class BlockchainDeposit extends Model
{
    use UserRelation;

    protected $guarded = [];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * This model relationship to the user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Asset relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditType()
    {
        return $this->belongsTo(BankCreditType::class, 'bank_credit_type_id');
    }

    /**
     * Asset relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depositCreditType()
    {
        return $this->belongsTo(BankCreditType::class, 'deposit_credit_type_id');
    }

    /**
     * Asset relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->hasOne(BlockchainDepositInvoice::class, 'blockchain_deposit_id');
    }
}
