<?php

namespace Modules\BlockchainDeposit\Contracts;

use Plus65\Base\Repositories\Contracts\CrudContract;

interface BlockchainDepositContract extends CrudContract
{
    /**
     * Get uncredited deposits.
     *
     * @param int $userId
     * @param int $depositCreditTypeId
     * @return mixed
     */
    public function getUncreditedDeposits(int $userId, int $depositCreditTypeId);

    /**
     * Get deposits not redirected.
     *
     * @param int $userId
     * @param int $depositCreditTypeId
     * @return mixed
     */
    public function getUnredirectedDepositByUserId(int $userId, int $depositCreditTypeId);

    /**
     * Get unprocessed deposits.
     *
     * @return mixed
     */
    public function getUnprocessedDeposits();

    /**
     * Get unprocessed deposits.
     *
     * @return mixed
     */
    public function getUnredirectedDeposits();

    /**
     * Issue credit;
     *
     * @param int $depositId
     * @return mixed
     */
    public function issueCredit(int $depositId);

    /**
     * Update confirmations.
     *
     * @param int $depositId
     * @param int $confirmations
     * @param int $minConfirmations
     * @return mixed
     */
    public function updateConfirmations(int $depositId, int $confirmations, int $minConfirmations);
}
