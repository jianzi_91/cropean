<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::group(['prefix' => 'blockchain'], function () {
        Route::group(['domain' => config('core.admin_url')], function () {
            Route::get('/qr/image/{settingName}', ['as' => 'admin.blockchain.deposits.qr', 'uses' => 'Admin\BlockchainDepositController@qrImage']);
            Route::get('deposit-addresses/{userId}', 'Admin\BlockchainDepositController@getMemberDepositWallets')->name('admin.blockchain.deposit-addresses.member.index');
            Route::get('deposit-addresses/{userId}/qr/{creditType}', 'Admin\BlockchainDepositController@getMemberDepositWalletQrCode')->name('admin.blockchain.deposit-addresses.member.qr');

            Route::get('deposits/usdt_erc20/export', 'Admin\UsdtErc20\BlockchainDepositExportController@index')->name('admin.blockchain.deposits.usdt_erc20.export');
            Route::resource('deposits/usdt_erc20', 'Admin\UsdtErc20\BlockchainDepositController', ['as' => 'admin.blockchain.deposits'])->only(['index']);

            Route::get('deposits/usdc/export', 'Admin\Usdc\BlockchainDepositExportController@index')->name('admin.blockchain.deposits.usdc.export');
            Route::resource('deposits/usdc', 'Admin\Usdc\BlockchainDepositController', ['as' => 'admin.blockchain.deposits'])->only(['index']);

            Route::resource('deposits/invoices', 'Admin\BlockchainDepositInvoiceController', ['as' => 'admin.blockchain.deposits'])->only(['show']);
        });

        Route::group(['domain' => config('core.member_url')], function () {
            Route::get('/qr/image/{bankCreditTypeId}', ['as' => 'member.blockchain.deposits.qr', 'uses' => 'Member\BlockchainDepositController@qrImage']);
            Route::get('deposits/usdt_erc20/export', 'Member\UsdtErc20\BlockchainDepositExportController@index')->name('member.blockchain.deposits.usdt_erc20.export');
            Route::resource('deposits/usdt_erc20', 'Member\UsdtErc20\BlockchainDepositController', ['as' => 'member.blockchain.deposits'])->only(['index', 'create']);

            Route::get('deposits/usdc/export', 'Member\Usdc\BlockchainDepositExportController@index')->name('member.blockchain.deposits.usdc.export');
            Route::resource('deposits/usdc', 'Member\Usdc\BlockchainDepositController', ['as' => 'member.blockchain.deposits'])->only(['index', 'create']);
            Route::resource('deposits/invoices', 'Member\BlockchainDepositInvoiceController', ['as' => 'member.blockchain.deposits'])->only(['show']);
        });
    });
});
