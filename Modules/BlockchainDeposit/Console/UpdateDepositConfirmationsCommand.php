<?php

namespace Modules\BlockchainDeposit\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Modules\Blockchain\Repositories\CryptoCurrencies\BitcoinRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\EthereumRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdcRepository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtRepository;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\Core\Traits\LogCronOutput;
use Modules\Credit\Contracts\BankCreditTypeContract;

/**
 * Handle logic such as deposits by parsing blockchain blocks based on confimrations required.
 *
 * @package Modules\BlockchainParser\Console
 */
class UpdateDepositConfirmationsCommand extends Command
{
    use LogCronOutput;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'blockchain-parser:update-deposit-confirmations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update confirmations of deposits, and issue credits if necessary';

    /**
     * Blockchain deposit service.
     *
     * @var BlockchainDepositContract
     */
    protected $depositService;

    /**
     * Minimum confirmations required.
     *
     * @var array
     */
    protected $minConfirmations;

    protected $minDeposits;

    /**
     * Class constructor
     * .
     * @param BlockchainDepositContract $depositService
     */
    public function __construct(
        BlockchainDepositContract $depositService,
        BankCreditTypeContract $bankCreditTypeService
    ) {
        parent::__construct();
        $this->depositService        = $depositService;
        $this->bankCreditTypeService = $bankCreditTypeService;
    }

    /**
     * Setup variables
     */
    protected function setup()
    {
        // $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::BTC)]        = setting('deposit_btc_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::ETH)] = setting('deposit_eth_min_confirmation');
        // $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDT_OMNI)]  = setting('deposit_usdt_omni_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDT_ERC20)] = setting('deposit_usdt_erc20_min_confirmation');
        $this->minConfirmations[bank_credit_type_id(BankCreditTypeContract::USDC)]       = setting('deposit_usdc_min_confirmation');

        // $this->minDeposits[bank_credit_type_id(BankCreditTypeContract::BTC)]        = setting('minimum_btc_deposit_redirection_amount');
        $this->minDeposits[bank_credit_type_id(BankCreditTypeContract::ETH)] = setting('minimum_eth_deposit_redirection_amount');
        // $this->minDeposits[bank_credit_type_id(BankCreditTypeContract::USDT_OMNI)]  = setting('minimum_usdt_omni_deposit_redirection_amount');
        $this->minDeposits[bank_credit_type_id(BankCreditTypeContract::USDT_ERC20)] = setting('minimum_usdt_erc20_deposit_redirection_amount');
        $this->minDeposits[bank_credit_type_id(BankCreditTypeContract::USDC)]       = setting('minimum_usdc_deposit_redirection_amount');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->logBlockConfirmation('Start confirmation');

        DB::beginTransaction();

        $this->setup();

        $unprocessedDeposits = $this->depositService->getUnprocessedDeposits();

        $this->logBlockConfirmation('');
        $this->logBlockConfirmation('------------------------------');
        $this->logBlockConfirmation('Total Unprocessed Deposits: ' . $unprocessedDeposits->count());
        $this->logBlockConfirmation('------------------------------');

        try {
            foreach ($unprocessedDeposits as $deposit) {
                $this->logBlockConfirmation('');
                $this->logBlockConfirmation("Processing Deposit Hash $deposit->transaction_hash");
                $this->logBlockConfirmation('--------------------------------------');

                $confirmations = $this->getCyrptoCurrencyService($deposit->deposit_credit_type_id)
                        ->getConfirmations($deposit->transaction_hash);

                $minConfirmations = $this->minConfirmations[$deposit->deposit_credit_type_id];
                $this->logBlockConfirmation('   Minimum Confirmation ' . $minConfirmations);

                $this->logBlockConfirmation('       Update DB Confirmation To ' . $confirmations);
                $this->depositService->updateConfirmations($deposit->id, $confirmations, $minConfirmations);

                if (!$deposit->is_master_wallet) {
                    if ($minConfirmations <= $confirmations) {
                        // Check if user has met minimum deposit amount first, if not, cannot issue credit
                        $uncreditedDeposits = $this->depositService->getUncreditedDeposits($deposit->user_id, $deposit->deposit_credit_type_id);
                        $totalUncredited    = '0';

                        foreach ($uncreditedDeposits as $uncreditedDeposit) {
                            $totalUncredited = bcadd($totalUncredited, $uncreditedDeposit->amount, 8);
                        }

                        $minDeposit = $this->minDeposits[$deposit->deposit_credit_type_id];

                        $this->logBlockConfirmation('   Minimum deposit amount ' . $minDeposit);

                        $creditType = $this->bankCreditTypeService->find($deposit->deposit_credit_type_id);
                        if (1 != bccomp($minDeposit, $totalUncredited, credit_precision($creditType->rawname))) {
                            $this->logBlockConfirmation('       Crediting deposit amount' . $totalUncredited);
                            foreach ($uncreditedDeposits as $uncreditedDeposit) {
                                $this->depositService->issueCredit($uncreditedDeposit->id);
                            }

                            // Redirect to master wallet will be handled below in blockchain-parser:master-wallet-redirection command
                        }
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $this->logBlockConfirmation('       Failed to do redirection' . $e->getMessage());
            logger($e->getMessage());
            logger($e->getTraceAsString());
            throw $e;
        }

        Artisan::call('blockchain-parser:master-wallet-redirection');
    }

    /**
     * Get service.
     *
     * @param int $bank_credit_type_id
     * @return mixed
     */
    protected function getCyrptoCurrencyService(int $bank_credit_type_id)
    {
        $className = '';

        switch (bank_credit_type_name($bank_credit_type_id)) {
            // case BankCreditTypeContract::BTC:
            //     $className = BitcoinRepository::class;
            //     break;

            // case BankCreditTypeContract::USDT_OMNI:
            //     $className = UsdtRepository::class;
            //     break;

            case BankCreditTypeContract::USDC:
                $className = UsdcRepository::class;
                break;

            case BankCreditTypeContract::USDT_ERC20:
                $className = UsdtErc20Repository::class;
                break;

            case BankCreditTypeContract::ETH:
                $className = EthereumRepository::class;
                break;
        }

        return resolve($className);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
