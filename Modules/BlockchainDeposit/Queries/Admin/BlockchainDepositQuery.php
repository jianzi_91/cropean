<?php

namespace Modules\BlockchainDeposit\Queries\Admin;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\BlockchainDeposit\Queries\BlockchainDepositQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class BlockchainDepositQuery extends BaseQuery implements WithMapping, WithHeadings
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter'    => 'date_range',
            'table'     => 'blockchain_deposits',
            'column'    => 'created_at',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'transaction_hash' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_deposits',
            'column'    => 'transaction_hash',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'reference_number' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_deposits',
            'column'    => 'reference_number',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'transaction_code' => [
            'filter' => 'equal',
            'table'  => 'blockchain_deposits',
            'column' => 'transaction_code'
        ],
        'credit_type' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_deposits',
            'column'    => 'deposit_credit_type_id',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'member_id' => [
            'filter'    => 'downline',
            'table'     => 'users',
            'namespace' => 'Modules\Core\Queries\Filters',
        ],
        'sending_address' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_deposits',
            'column'    => 'sending_address',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'receiving_address' => [
            'filter'    => 'equal',
            'table'     => 'blockchain_deposits',
            'column'    => 'receiving_address',
            'namespace' => 'QueryBuilder\Filters',
        ],
        'email' => [
            'filter'    => 'equal',
            'table'     => 'users',
            'column'    => 'email',
            'namespace' => 'QueryBuilder\Filters',
        ],
    ];

    /**
     * Set parameters
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $creditTypeRepo   = resolve(BankCreditTypeContract::class);
        $this->parameters = $params;

        $params['credit_type'] = isset($params['credit_type']) ? $creditTypeRepo->findBySlug($params['credit_type'])->id : null;

        $this->filterBroker->setParameters($params);

        return $this;
    }

    public function map($deposit): array
    {
        return [
            $deposit->created_at,
            $deposit->reference_number,
            $deposit->name,
            $deposit->member_id,
            $deposit->email,
            $deposit->sending_address,
            $deposit->receiving_address,
            $deposit->transaction_hash,
            amount_format($deposit->amount),
            $deposit->confirmations,
        ];
    }

    public function headings(): array
    {
        return [
            __('a_wallet_deposit.date'),
            __('a_wallet_deposit.reference number'),
            __('a_wallet_deposit.name'),
            __('a_wallet_deposit.member id'),
            __('a_wallet_deposit.email'),
            __('a_wallet_deposit.sending address'),
            __('a_wallet_deposit.receiving address'),
            __('a_wallet_deposit.transaction hash'),
            __('a_wallet_deposit.amount'),
            __('a_wallet_deposit.confirmations'),
        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        // Force column C(Account Number) to be string format
        if (in_array($cell->getColumn(), ['C'])) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
