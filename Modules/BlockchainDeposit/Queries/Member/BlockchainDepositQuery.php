<?php

namespace Modules\BlockchainDeposit\Queries\Member;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\BlockchainDeposit\Queries\BlockchainDepositQuery as BaseQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class BlockchainDepositQuery extends BaseQuery implements WithMapping, WithHeadings, WithCustomValueBinder
{
    /**
     * The filters
     *
     * @var array
     */
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'blockchain_deposits',
            'column' => 'created_at'
        ],
        'transaction_hash' => [
            'filter' => 'equal',
            'table'  => 'blockchain_deposits',
            'column' => 'transaction_hash'
        ],
        'transaction_code' => [
            'filter' => 'equal',
            'table'  => 'blockchain_deposits',
            'column' => 'transaction_code'
        ],
        'reference_number' => [
            'filter' => 'equal',
            'table'  => 'blockchain_deposits',
            'column' => 'reference_number'
        ],
        'credit_type' => [
            'filter' => 'equal',
            'table'  => 'blockchain_deposits',
            'column' => 'deposit_credit_type_id'
        ],
    ];

    /**
     * Set parameters
     *
     * @param array $params
     */
    public function setParameters(array $params)
    {
        $creditTypeRepo   = resolve(BankCreditTypeContract::class);
        $this->parameters = $params;

        $params['credit_type'] = isset($params['credit_type']) ? $creditTypeRepo->findBySlug($params['credit_type'])->id : null;

        $this->filterBroker->setParameters($params);

        return $this;
    }

    /**
     * Adhoc processes before build
     */
    public function beforeBuild()
    {
        return $this->builder->where('user_id', Auth::user()->id);
    }

    public function map($deposit): array
    {
        return [
            $deposit->created_at,
            isset($deposit->reference_number) ? $deposit->reference_number : '-',
            $deposit->transaction_hash,
            amount_format($deposit->amount, credit_precision(BankCreditTypeContract::USDT_ERC20)),
            $deposit->confirmations,
        ];
    }

    public function headings(): array
    {
        return [
            __('m_wallet_deposit.date'),
            __('m_wallet_deposit.reference number'),
            __('m_wallet_deposit.transaction hash'),
            __('m_wallet_deposit.amount'),
            __('m_wallet_deposit.confirmations'),

        ];
    }

    public function bindValue(Cell $cell, $value)
    {
        // Force column C(Account Number) to be string format
        if (in_array($cell->getColumn(), ['B'])) {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);
            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
