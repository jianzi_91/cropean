<?php

namespace Modules\BlockchainDeposit\Queries;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\BlockchainDeposit\Models\BlockchainDeposit;
use QueryBuilder\QueryBuilder;

class BlockchainDepositQuery extends QueryBuilder implements FromCollection
{
    use Exportable;

    protected $paginate = false;

    /**
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryBuilder::query()
     */
    public function query()
    {
        $query = BlockchainDeposit::select('blockchain_deposits.*', 'users.email as email', 'users.name as name', 'bank_credit_types.name_translation as credit_type_name', 'users.member_id as member_id')
            ->leftJoin('users', 'users.id', '=', 'blockchain_deposits.user_id')
            ->join('bank_credit_types', 'bank_credit_types.id', '=', 'blockchain_deposits.deposit_credit_type_id')
            ->orderBy('blockchain_deposits.created_at', 'desc');

        return $query;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->paginate ? $this->paginate() : $this->get();
    }

    /**
     * Get the value of paginate
     */
    public function getPaginate()
    {
        return $this->paginate;
    }
}
