<?php

namespace Modules\BlockchainDeposit\Repositories;

use Illuminate\Support\Facades\Storage;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositInvoiceContract;
use Modules\BlockchainDeposit\Models\BlockchainDeposit;
use Modules\BlockchainDeposit\Models\BlockchainDepositInvoice;
use PDF;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BlockchainDepositInvoiceRepository extends Repository implements BlockchainDepositInvoiceContract
{
    use HasCrud, HasSlug;
    // implement functions here

    /**
     * Class constructor.
     */
    public function __construct(
        BlockchainDepositInvoice $model
    ) {
        $this->model = $model;
        $this->slug  = "reference_number";

        parent::__construct($model);
    }

    public function generate(BlockchainDeposit $deposit)
    {
        $data = [
            'blockchain_deposit_id' => $deposit->id,
            'name'                  => $deposit->user->name,
            'member_id'             => $deposit->user->member_id,
            'reference_number'      => $deposit->reference_number,
            'amount'                => amount_format($deposit->amount),
        ];

        $invoice = $this->add($data);

        $data['transaction_date'] = now()->format('d/m/Y');

        $view = "deposit::invoice_" . $deposit->user->locale;
        $pdf  = PDF::setOptions(['isRemoteEnabled' => true,  'isHtml5ParserEnabled' => true, 'defaultFont' => 'simhei', 'fontDir' => storage_path('fonts/')])
                ->loadView($view, compact('data'))
                ->setPaper('A4', 'portrait')
                ->download();

        $path     = config('blockchaindeposit.invoice_storage');
        $fullpath = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $path;

        if (config('filesystems.cloud_enable')) {
            Storage::cloud()->put($path . DIRECTORY_SEPARATOR . $deposit->reference_number . '.pdf', $pdf);
        }

        Storage::put($path . DIRECTORY_SEPARATOR . $deposit->reference_number . '.pdf', $pdf);

        return $invoice;
    }
}
