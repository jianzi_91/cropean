<?php

namespace Modules\BlockchainDeposit\Repositories;

use Mail;
use Modules\Blockchain\Emails\DepositFailed;
use Modules\Blockchain\Models\BlockchainWallet;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositContract;
use Modules\BlockchainDeposit\Contracts\BlockchainDepositInvoiceContract;
use Modules\BlockchainDeposit\Models\BlockchainDeposit;
use Modules\Credit\Contracts\BankAccountCreditContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\User\Models\UserStatus;
use Plus65\Base\Repositories\Concerns\HasCrud;
use Plus65\Base\Repositories\Concerns\HasSlug;
use Plus65\Base\Repositories\Repository;

class BlockchainDepositRepository extends Repository implements BlockchainDepositContract
{
    use HasCrud, HasSlug;

    /**
     * The model.
     *
     * @var unknown
     */
    protected $model;

    /** @var BankAccountCreditContract $bankAccountCreditContract */
    protected $creditRepo;

    /**
     * Class constructor.
     *
     * @param UserStatus  $model
     */
    public function __construct(
        BlockchainDeposit $model,
        BankCreditTypeContract $bankCreditTypeContract,
        BankAccountCreditContract $bankAccountCreditContract,
        BlockchainDepositInvoiceContract $blockchainDepositInvoiceContract
    ) {
        $this->model                  = $model;
        $this->bankCreditTypeContract = $bankCreditTypeContract;
        $this->creditRepo             = $bankAccountCreditContract;
        $this->invoiceRepo            = $blockchainDepositInvoiceContract;
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::getUnprocessedDeposits()
     */
    public function getUnprocessedDeposits()
    {
        return BlockchainDeposit::whereNull('transaction_code')
            ->where('is_confirmed', false)
            ->get();
    }

    /**
     * @inheritDoc
     * @throws \Exception
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::issueCredit()
     */
    public function issueCredit(int $depositId)
    {
        $deposit = BlockchainDeposit::findOrFail($depositId);

        if (!empty($deposit->transaction_code)) {
            throw new \Exception('Credit has already been issued, cannot issue again');
        }

        if (empty($deposit->user_id)) {
            return;
        }

        $wallet = BlockchainWallet::where('address', $deposit->receiving_address)
            ->where('bank_credit_type_id', $deposit->deposit_credit_type_id)
            ->first();

        if ($wallet) {
            $txHash = $this->creditRepo->add(
                $deposit->destination_credit_type_id,
                $deposit->amount,
                $wallet->user_id,
                BankTransactionTypeContract::BLOCKCHAIN_DEPOSIT
            );
            $deposit->update(['transaction_code' => $txHash]);
        } else {
            $deposit->update([
                'transaction_code'             => '-',
                'redirection_transaction_hash' => '-',
            ]);

            Mail::to(config('utility.exception.to.address'))->send(new DepositFailed($deposit));
        }
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::updateConfirmations()
     */
    public function updateConfirmations(int $depositId, int $confirmations, int $minConfirmations)
    {
        $updateData = [
            'confirmations' => $confirmations
        ];

        if ($minConfirmations <= $confirmations) {
            $updateData['is_confirmed'] = true;
        }

        BlockchainDeposit::where('id', $depositId)
            ->update($updateData);
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::getUncreditedDeposits()
     */
    public function getUncreditedDeposits(int $userId, int $depositCreditTypeId)
    {
        return BlockchainDeposit::where('user_id', $userId)
            ->where('is_confirmed', true)
            ->where('deposit_credit_type_id', $depositCreditTypeId)
            ->whereNull('transaction_code')
            ->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::getUnredirectedDeposits()
     */
    public function getUnredirectedDeposits()
    {
        return BlockchainDeposit::where('is_confirmed', true)
            ->whereNull('redirection_transaction_hash')
            ->groupBy('user_id', 'deposit_credit_type_id')
            ->where('is_master_wallet', 0)
            ->where('is_confirmed', true)
            ->whereNotNull('transaction_code')
            ->where('deposit_credit_type_id', '<>', bank_credit_type_id(BankCreditTypeContract::BTC))
            ->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\BlockchainDeposit\Contracts\BlockchainDepositContract::getUnredirectedDepositByUserId()
     */
    public function getUnredirectedDepositByUserId(int $userId, int $depositCreditTypeId)
    {
        return BlockchainDeposit::where('user_id', $userId)
            ->where('deposit_credit_type_id', $depositCreditTypeId)
            ->whereNull('redirection_transaction_hash')
            ->where('is_confirmed', true)
            ->whereNotNull('transaction_code')
            ->get();
    }

    /**
     * @inheritDoc
     * @see \Modules\Blockchain\Contracts\BlockchainFeeContract::getEthFees()
     */
    public function getUsdtErc20Fees(bool $isUpdated = false)
    {
        return $this->model
            ->where('deposit_credit_type_id', bank_credit_type_id(BankCreditTypeContract::USDT_ERC20))
            ->where('blockchain_fee', 0)
            ->get();
    }
}
