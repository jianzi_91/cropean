<?php

namespace Modules\BlockchainDeposit\Http\Controllers\Member\Usdc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\BlockchainDeposit\Queries\Member\BlockchainDepositQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainDepositExportController extends Controller
{
    use CanExportTrait;

    protected $depositQuery;

    /**
     * Class constructor
     *
     */
    public function __construct(
        BlockchainDepositQuery $depositQuery
    ) {
        $this->middleware('permission:member_blockchain_deposit_export')->only('index');

        $this->depositQuery = $depositQuery;
    }

    /*
        Export Deposit Report for member
        * @param Request $request
        * @param $type
    */
    public function index(Request $request)
    {
        $now = now()->toDateTimeString();

        return $this->exportReport($this->depositQuery->setParameters($request->all()), 'member_deposits_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
