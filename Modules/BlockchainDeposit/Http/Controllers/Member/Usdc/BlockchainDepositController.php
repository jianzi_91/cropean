<?php

namespace Modules\BlockchainDeposit\Http\Controllers\Member\Usdc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\BlockchainDeposit\Queries\Member\BlockchainDepositQuery;
use Modules\Credit\Contracts\BankCreditTypeContract;

class BlockchainDepositController extends Controller
{
    protected $depositQuery;
    protected $walletRepo;

    /**
     * Class constructor
     *
     */
    public function __construct(
        BlockchainDepositQuery $depositQuery,
        BlockchainWalletContract $walletContract,
        BankCreditTypeContract $creditTypeRepo
    ) {
        $this->middleware('permission:member_blockchain_deposit_list')->only('index');

        $this->depositQuery   = $depositQuery;
        $this->walletRepo     = $walletContract;
        $this->creditTypeRepo = $creditTypeRepo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if (!auth()->user()->epoaContract) {
            return redirect(route('member.epoa.create'));
        }

        $bankCreditType = $this->creditTypeRepo->findBySlug(BankCreditTypeContract::USDC);
        $request->merge([
            'credit_type' => BankCreditTypeContract::USDC,
        ]);

        $deposits = $this->depositQuery->setParameters($request->all())
            ->paginate();

        $wallet = $this->walletRepo->getModel()
            ->where('user_id', auth()->user()->id)
            ->where('bank_credit_type_id', $bankCreditType->id)
            ->first();

        return view('blockchaindeposit::member.usdc.index', compact('deposits', 'wallet'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $depositWallet = $this->walletRepo->getModel()
                                ->with('creditType')
                                ->where('bank_credit_type_id', bank_credit_type_id(BankCreditTypeContract::USDT_ERC20))
                                ->where('user_id', Auth::id())
                                ->firstOrFail();

        return view('blockchaindeposit::member.create', compact('depositWallet'));
    }

    /**
     * Render QR Image for frontend pop-up
     * @param $bankCreditTypeId
     */
    public function qrImage($bankCreditTypeId)
    {
        $wallet = $this->walletRepo->findByUserIdAndCreditType(auth()->user()->id, $bankCreditTypeId);

        return response()->file(storage_path('wallet/qr_code/') . basename($wallet->qr_code), ['Content-Type' => 'image/svg+xml']);
    }
}
