<?php

namespace Modules\BlockchainDeposit\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\BlockchainDeposit\Queries\Admin\BlockchainDepositQuery;
use QueryBuilder\Concerns\CanExportTrait;

class BlockchainDepositExportController extends Controller
{
    use CanExportTrait;

    protected $depositQuery;

    /**
     * Class constructor
     *
     */
    public function __construct(
        BlockchainDepositQuery $depositQuery
    ) {
        $this->depositQuery = $depositQuery;
        $this->middleware('permission:admin_blockchain_deposit_export')->only('index');
    }

    /*
        * @param Request $request
        * @param $type
    */
    public function index(Request $request)
    {
        $now = now()->toDateTimeString();

        return $this->exportReport($this->depositQuery->setParameters($request->all()), 'admin_blockchain_deposits_' . $now . '.xlsx', auth()->user(), 'Export on ' . $now);
    }
}
