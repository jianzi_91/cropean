<?php

namespace Modules\BlockchainDeposit\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Blockchain\Contracts\BlockchainWalletContract;
use Modules\Blockchain\Contracts\CryptoCurrencies\EthereumContract;
use Modules\Blockchain\Repositories\CryptoCurrencies\UsdtErc20Repository;
use Modules\BlockchainDeposit\Queries\Admin\BlockchainDepositQuery;
use Modules\User\Contracts\UserContract;

class BlockchainDepositController extends Controller
{
    protected $depositQuery;

    protected $usdtRepository;

    protected $ethRepository;

    /**
     * Class constructor
     *
     */
    public function __construct(
        BlockchainDepositQuery $depositQuery,
        UsdtErc20Repository $usdtErc20Repository,
        EthereumContract $ethereumContract,
        BlockchainWalletContract $walletRepository,
        UserContract $userRepository
    ) {
        $this->depositQuery     = $depositQuery;
        $this->ethRepository    = $ethereumContract;
        $this->usdtRepository   = $usdtErc20Repository;
        $this->walletRepository = $walletRepository;
        $this->userRepository   = $userRepository;

        $this->middleware('permission:admin_blockchain_deposit_addresses_list')->only('getMemberDepositWallets');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $usdtErc20MasterWallet = setting('master_wallet_usdt_erc20_address');
        $usdtEthMasterWallet   = setting('master_wallet_eth_address');
        // $usdtErc20Balance      = $this->usdtRepository->getBalance($usdtErc20MasterWallet);
        // $ethBalance            = $this->ethRepository->getBalance($usdtEthMasterWallet);
        $usdtErc20Balance = 0;
        $ethBalance       = 0;

        $deposits = $this->depositQuery->setParameters($request->all())
            ->paginate();

        return view(
            'blockchaindeposit::admin.index',
            compact(
                'deposits',
                'usdtErc20MasterWallet',
                'usdtEthMasterWallet',
                'usdtErc20Balance',
                'ethBalance'
            )
        );
    }

    public function getMemberDepositWallets(Request $request, $userId)
    {
        $wallets = $this->walletRepository->getModel()
            ->where('user_id', $userId)
            ->get();

        return view('blockchaindeposit::admin.management.index', compact('wallets', 'userId'));
    }

    public function getMemberDepositWalletQrCode(Request $request, $userId, $creditType)
    {
        $wallet = $this->walletRepository->findByUserIdAndCreditType($userId, $creditType);

        return response()->file(storage_path('wallet/qr_code/') . basename($wallet->qr_code), ['Content-Type' => 'image/svg+xml']);
    }

    /**
     * Render QR Image for frontend pop-up
     * @param $bankCreditTypeId
     */
    public function qrImage($setting)
    {
        $qrCode = setting($setting);

        return response()->file(storage_path('wallet/qr_code/') . basename($qrCode), ['Content-Type' => 'image/svg+xml']);
    }
}
