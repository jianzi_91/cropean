<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockchainDepositsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockchain_deposits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('reference_number')->unique();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('deposit_credit_type_id');
            $table->unsignedInteger('destination_credit_type_id')->nullable();
            $table->unsignedInteger('block');
            $table->string('transaction_hash')->unique();
            $table->string('sending_address')->nullable();
            $table->string('receiving_address');
            $table->boolean('is_master_wallet')->default(false);
            $table->decimal('amount', 40, 20);
            $table->decimal('blockchain_fee', 40, 20);
            $table->unsignedInteger('confirmations');
            $table->boolean('is_confirmed')->default(false);
            $table->string('transaction_code')->nullable();
            $table->string('redirection_transaction_hash')->nullable();

            $table->foreign('deposit_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('destination_credit_type_id')
                ->references('id')
                ->on('bank_credit_types');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockchain_deposits');
    }
}
