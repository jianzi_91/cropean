<?php

namespace Modules\BlockchainDeposit\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Modules\Core\Database\Seeders\PermissionBaseSeeder;
use Modules\Rbac\Contracts\AbilityCategoryContract;
use Modules\Rbac\Models\Role;

class BlockchainDepositPermissionsSeeder extends PermissionBaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();

            $this->abilities();
            $this->permissions();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Seed abilities
     *
     * @return void
     */
    protected function abilities()
    {
        $abilityCategoryContract = resolve(AbilityCategoryContract::class);

        $abilitySections = [
            'admin_blockchain_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::ADMIN),
                    'name'        => 'Blockchain Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'admin_blockchain_deposit_list',
                        'title' => 'Admin Blockchain Deposit List',
                    ],
                    [
                        'name'  => 'admin_blockchain_deposit_export',
                        'title' => 'Admin Blockchain Deposit Export',
                    ],
                ]
            ],
            'member_blockchain_deposit' => [
                'section' => [
                    'category_id' => get_ability_category_id($abilityCategoryContract::MEMBER),
                    'name'        => 'Blockchain Deposit',
                ],
                'abilities' => [
                    [
                        'name'  => 'member_blockchain_deposit_list',
                        'title' => 'Member Blockchain Deposit List',
                    ],
                    [
                        'name'  => 'member_blockchain_deposit_create',
                        'title' => 'Member Blockchain Deposit Create',
                    ],
                    [
                        'name'  => 'member_blockchain_deposit_export',
                        'title' => 'Member Blockchain Deposit Export',
                    ],
                ]
            ],
        ];

        $this->seedAbilities($abilitySections);
    }

    /**
     * Seed permissions
     *
     * @return void
     */
    protected function permissions()
    {
        $permissions = [
            Role::SYSADMIN => [
                'admin_blockchain_deposit_list',
                'admin_blockchain_deposit_export'
            ],
            Role::ADMIN => [
                'admin_blockchain_deposit_list',
                'admin_blockchain_deposit_export'
            ],
            Role::MEMBER => [
                'member_blockchain_deposit_list',
                'member_blockchain_deposit_create',
                'member_blockchain_deposit_export',
            ],
            Role::MEMBER_SUSPENDED => [
                #TBD
            ],
            Role::MEMBER_TERMINATED => [
                #TBD
            ],
            Role::MEMBER_ON_HOLD => [
                #TBD
            ],
        ];

        $this->seedPermissions($permissions);
    }
}
