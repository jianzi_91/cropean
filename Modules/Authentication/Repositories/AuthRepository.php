<?php

namespace Modules\Authentication\Repositories;

use Carbon\Carbon;
use Illuminate\Auth\AuthManager;
use Modules\Authentication\Contracts\AuthContract;
use Modules\Authentication\Models\LoginAsToken;
use Modules\User\Models\User;

class AuthRepository implements AuthContract
{
    /**
     * The auth
     *
     * @var unknown
     */
    protected $auth;

    /**
     * Class constructor.
     *
     * @param AuthManager  $auth
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \Modules\Authentication\Contracts\AuthContract::loginAs()
     */
    public function loginAs(string $token)
    {
        $token = LoginAsToken::where('token', $token)
                    ->valid()
                    ->where('created_at', '>=', Carbon::now()->subMinutes(config('authentication.login_as.validity_in_minutes')))
                    ->first();

        if (!$token) {
            return false;
        }

        $user = User::find($token->member_user_id);

        if (!$user) {
            return false;
        }

        $this->auth->login($user);

        $user->session_id = session()->getId();
        $user->last_login = Carbon::now();

        if ($user->save()) {
            return $token->markAsUsed();
        }

        return false;
    }
}
