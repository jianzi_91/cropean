<?php

namespace Modules\Authentication\Models;

use Illuminate\Database\Eloquent\Model;

class LoginAsToken extends Model
{
    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'admin_user_id',
        'member_user_id',
        'token',
        'is_used',
    ];

    /**
     * Scope to get valid token.
     *
     * @param unknown $query
     */
    public function scopeValid($query)
    {
        return $query->where('is_used', false);
    }

    /**
     * Mark the token as used.
     */
    public function markAsUsed()
    {
        $this->is_used = true;

        return $this->save();
    }
}
