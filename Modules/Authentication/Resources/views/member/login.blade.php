@extends('templates.member.wide')
@section('title', __('m_page_title.login'))

@section('main')
    <section id="auth-login" class="row flexbox-container">
        <div class="col-xl-8 col-11">
            <div class="wide lang-selector">
                @foreach(languages() as $locale=>$name)
                <div data-lang="{{ $locale }}" class="lang-item {{ app()->getLocale() == $locale ? 'active' : '' }}">{{ $name }}</div>
                @if (!$loop->last)
                <div>&nbsp;&nbsp;|&nbsp;&nbsp;</div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-xl-8 col-11">
            <div class="card bg-authentication mb-0">
                <div class="row m-0" style="background-color: white;">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="card-header pb-1 d-flex justify-content-center">
                                <img src="{{ asset('images/logo_text.svg') }}" class="img-fluid" alt="logo" />
                            </div>
                            <br/>
                            <div class="card-content">
                                <div class="card-body">
                                    
                                    {{ Form::open(['method'=>'post', 'id'=>'form', 'onsubmit' => 'login_btn.disabled = true; return true;']) }}

                                        <div class="form-group mb-50">
                                            {{ Form::formText('email', old('email'), __('m_login.email address'), [], false) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::formPassword('password', '', __('m_login.password'), [], false) }}
                                        </div>

                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                            {{ Form::formText('captcha', '', __('m_login.captcha'), ['autocomplete'=>'off'], false) }}
                                            </div>
                                            <a class="form-group refresh-captcha d-flex mt-32 ml-1 align-items-center" rel="tooltip" title="{{ __('m_login.refresh') }}" onclick="document.getElementById('captcha-code').src='captcha/flat?'+Math.random()">
                                                <img src="{!! captcha_src('flat') !!}" alt="{{ __('m_login.captcha') }}" id="captcha-code" />
                                                <i class="bx bx-revision" style="font-size: 20px;cursor: pointer;padding:5px;"></i>
                                            </a>
                                        </div>
                                        <div style="padding:0 0 10px;"><a href="{{ route('member.password.forgot.index') }}" class="a-text">{{ __('m_login.forgot password?') }}</a></div>
                                        <div class="text-center">
                                            <button type="submit" name="login_btn" class="btn btn-primary w-100 position-relative">{{ __('m_login.login') }}<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                        </div>
                                    {{ Form::close() }}
                                    <hr>
                                    <div class="text-center login-to-register"><span class="mr-25">{{ __('m_login.dont have an account?') }}</span><a href="{{ route('member.register') }}" class="a-text"><span>{{ __('m_login.register') }}</span></a></div>
                                    <br>
                                    <div class="text-center login-to-register">{{ __('m_login.email') }}: <a href="mailto:helpdesk@ecntradefx.com">helpdesk@ecntradefx.com</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3" style="padding: 0 !important;">
                        <div class="card-content">
                            <img class="img-fluid" src="{{ asset('images/bull.png') }}" alt="branding logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#form', {
        email: { presence: { message: __.validation.field_required } },
        password: { presence: { message: __.validation.field_required } },
        captcha: { presence: { message: __.validation.field_required } },
    })

    $('.lang-item').on('click', function() {
        var lang = $(this).data('lang')
        window.location.href = '/lang/' + lang
    })
})
</script>
@endpush