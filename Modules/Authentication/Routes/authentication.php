<?php

Route::group(['middleware' => 'web'], function () {
    Route::group(['domain' => config('core.admin_url')], function () {
        Route::redirect('/', '/login');
        Route::get('login', ['as' => 'admin.login', 'uses' => 'Admin\LoginController@index']);
        Route::post('login', ['as' => 'admin.attempt.login', 'uses' => 'Admin\LoginController@store']);

        Route::group(['middleware' => ['auth']], function () {
            Route::get('login_as/{id}', ['as' => 'admin.login_as', 'uses' => 'Admin\LoginController@adminLoginAs']);
            Route::post('logout', ['as' => 'admin.logout', 'uses' => 'Admin\LogoutController@destroy']);
        });
    });

    Route::group(['domain' => config('core.member_url')], function () {
        Route::redirect('/', '/login');
        Route::get('login', ['as' => 'member.login', 'uses' => 'Member\LoginController@index']);
        Route::post('login', ['as' => 'member.attempt.login', 'uses' => 'Member\LoginController@store']);
        Route::get('login_as', ['as' => 'member.login_as', 'uses' => 'Member\LoginController@memberLoginAs']);

        Route::group(['middleware' => ['auth']], function () {
            Route::post('logout', ['as' => 'member.logout', 'uses' => 'Member\LogoutController@destroy']);
        });
    });
});
