<?php

return [
    'name'     => 'Authentication',
    'bindings' => [
        Modules\Authentication\Contracts\AuthContract::class => Modules\Authentication\Repositories\AuthRepository::class
    ],
    'login_username' => env('LOGIN_USERNAME', 'email'),
    'login_as'       => [
        'validity_in_minutes' => 5
    ],
];
