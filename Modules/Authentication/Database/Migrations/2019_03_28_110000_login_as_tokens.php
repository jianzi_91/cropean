<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LoginAsTokens extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('login_as_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('admin_user_id')->index();
            $table->unsignedInteger('member_user_id')->index();
            $table->string('token')->index();
            $table->boolean('is_used')->default(0);

            $table->unique(['token', 'is_used']);
            $table->foreign('admin_user_id')->references('id')->on('users');
            $table->foreign('member_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('login_as_tokens', function (Blueprint $table) {
            $table->dropForeign(['admin_user_id']);
            $table->dropForeign(['member_user_id']);
        });

        Schema::dropIfExists('login_as_tokens');
    }
}
