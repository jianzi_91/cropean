<?php

namespace Modules\Authentication\Contracts;

interface AuthContract
{
    /**
     * Login as via token
     *
     * @param  string $token
     * @return unknown
     */
    public function loginAs(string $token);
}
