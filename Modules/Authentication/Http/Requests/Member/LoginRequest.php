<?php

namespace Modules\Authentication\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            config('authentication.login_username') => 'required|string|max:255|email',
            'password'                              => 'required|string|max:255',
            'captcha'                               => 'required|captcha',
        ];
    }

    public function attributes()
    {
        return [
            config('authentication.login_username') => __('m_login.email'),
            'password'                              => __('m_login.password'),
            'captcha'                               => __('m_login.captcha'),
        ];
    }

    public function messages()
    {
        return [
            'captcha.captcha' => __('m_login.invalid captcha')
        ];
    }
}
