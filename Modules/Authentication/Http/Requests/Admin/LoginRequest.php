<?php

namespace Modules\Authentication\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            config('authentication.login_username') => 'required|string|max:255|email',
            'password'                              => 'required|string|max:255',
            'captcha'                               => 'required|captcha',
        ];
    }

    public function attributes()
    {
        return [
            config('authentication.login_username') => __('a_login.username'),
            'password'                              => __('a_login.password'),
            'captcha'                               => __('a_login.captcha'),
        ];
    }

    public function messages()
    {
        return [
            'captcha.captcha' => __('a_login.invalid captcha')
        ];
    }
}
