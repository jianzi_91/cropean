<?php

namespace Modules\Authentication\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Modules\Authentication\Http\Requests\Admin\LoginRequest;
use Modules\Authentication\Models\LoginAsToken;
use Modules\User\Contracts\UserContract;

class LoginController extends Controller
{
    use RedirectsUsers, ThrottlesLogins;

    protected $redirectTo = '/dashboard';

    /**
     * The user
     *
     * @var  unknown
     */
    private $user;

    /**
     * The user repository
     *
     * @var  unknown
     */
    private $userRepository;

    /**
     * Class constructor.
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        $this->middleware('guest')->only('index', 'store');
        $this->userRepository = $userContract;
    }

    /**
     * Show login page
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        return view('authentication::admin.login');
    }

    /**
     * Attemp login
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(LoginRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $this->ensureUserCanLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return config('authentication.login_username');
    }

    public function ensureUserCanLogin(Request $request)
    {
        $this->user = $this->userRepository->findByEmail($request->get($this->username()));

        if ($this->user && $this->checkUserIsNotAMember()) {
            return true;
        }

        if (!$this->user || !$this->checkUserIsNotAMember()) {
            $error = [$this->username() => [__('s_auth.these credentials do not match our records')], 'password' => [__('s_auth.these credentials do not match our records')]];
        } else {
            $error = [$this->username() => [__(
                's_auth.login failed account status is currently :status',
                ['status' => __($this->user->accountStatus->name)]
            )]];
        }

        throw ValidationException::withMessages($error);
    }

    /**
     * Admin login as trigger in admin portal.
     *
     * @param Request $request
     * @param unknown $id
     * @return unknown|\Illuminate\Http\RedirectResponse
     */
    public function adminLoginAs(Request $request, $id)
    {
        $loginAsToken                 = new LoginAsToken();
        $loginAsToken->admin_user_id  = $request->user()->id;
        $loginAsToken->member_user_id = $id;
        $loginAsToken->token          = \Hash::make($id);
        $loginAsToken->is_used        = 0;

        if ($loginAsToken->save()) {
            return redirect(route('member.login_as') . '?token=' . $loginAsToken->token);
        }

        return back()->with('error', __('s_auth.failed to login as'));
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(config('authentication.login_username'), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages(
            [config('authentication.login_username') => [__('s_auth.these credentials do not match our records')],
                'password'                           => [__('s_auth.these credentials do not match our records')],
            ]
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    private function checkUserIsNotAMember()
    {
        return (!$this->user->is_member) && $this->user->canLogin();
    }
}
