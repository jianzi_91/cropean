<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;

if (!function_exists('nanoSeconds')) {
    /**
     * Get the nanoseconds portion of current time. Uses OS system function.
     *
     * @return mixed
     */
    function nanoSeconds()
    {
        if (PHP_OS == 'Darwin') {
            // In Mac OS X, need to do brew install coreutils, and the command is gdate, but for Linux, command is date
            $result = strval(`gdate +%N`);
        } else {
            $result = strval(`date +%N`);
        }

        return substr($result, 0, strlen($result) - 1); // Remove the last \n character
    }
}

// Global helpers
if (!function_exists('amount_format')) {
    function amount_format($value, $precision = 2)
    {
        // First, we force $value to become string, this also cause $value to have the exact $precision
        $value = bcmul($value, '1', $precision);

        // Split up the number at the decimal point
        $splitDecimalParts = explode('.', $value);

        // Now we format the front part of the number, using number_format here is OK because we are using whole number
        $splitDecimalParts[0] = number_format($splitDecimalParts[0]);

        // Now just join back the string, tada!
        return implode('.', $splitDecimalParts);
    }
}

if (!function_exists('blockchain_amount_format')) {
    function blockchain_amount_format($value, $precision = 8)
    {
        return bcmul($value, 1, $precision);
    }
}

if (!function_exists('percentage')) {
    function percentage($value, $precision = 2)
    {
        return bcdiv($value, 100, $precision);
    }
}

if (!function_exists('bcsum')) {
    function bcsum($values = [], $precision = 2)
    {
        if (empty($values)) {
            return number_format(0, $precision, '.', '');
        } else {
            return number_format(array_sum($values), $precision, '.', '');
        }
    }
}

if (!function_exists('special_calc_comm')) {
    function special_calc_comm($amount, $percentage, $precision)
    {
        return special_ceil(bcmul($amount, $percentage, 10), $precision);
    }
}

if (!function_exists('special_ceil')) {
    function special_ceil($value, $precision = 0)
    {
        $ceil_value = 1 / (pow(10, $precision));
        $ceil_value = amount_format($ceil_value, $precision);

        $precise_value   = bcmul($value, 1, $precision);
        $extra_precision = bcsub($value, $precise_value, ($precision + 1));

        if ($extra_precision > 0) {
            return bcadd($precise_value, $ceil_value, $precision);
        }
        return bcmul($precise_value, 1, $precision);
    }
}

if (!function_exists('generate_ref')) {
    /**
     * Generate transaction code
     *
     * @param string $prefix
     */
    function generate_ref($prefix = 'TXN', $limit = null)
    {
        $code = $prefix . microtime(true) . sprintf("%06d", mt_rand(2, 999999));
        $code = str_replace('.', '', $code);

        if ($limit) {
            $code = substr($code, 0, $limit);
        }

        return $code;
    }
}

if (!function_exists('random_string')) {
    function random_string($length = 30, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $factory   = new RandomLib\Factory;
        $generator = $factory->getGenerator(new SecurityLib\Strength(SecurityLib\Strength::LOW));
        return $generator->generateString($length, $keyspace);
    }
}

// Translation Helpers
if (!function_exists('languages')) {

    /**
     * Get translator languages list.
     *
     * @return unknown
     */
    function languages()
    {
        $translatorLanguageRepository = resolve(Modules\Translation\Contracts\TranslatorLanguageContract::class);

        return $translatorLanguageRepository->all()->pluck('name', 'code');
    }
}

if (!function_exists('get_translatable_text')) {
    /**
     * Get translatable text
     *
     * @param  unknown $locale
     * @param  unknown $pageId
     * @param  unknown $key
     * @return unknown
     */
    function get_translatable_text($locale, $pageId, $key)
    {
        $translatorTranslationTransRepository = resolve(Modules\Translation\Contracts\TranslatorTranslationContract::class);

        $translation = $translatorTranslationTransRepository->findByKey($locale, $pageId, $key);

        if ($translation) {
            return $translation->value;
        }

        return;
    }
}

if (!function_exists('get_translator_language')) {
    /**
     * Get translator language
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_translator_language($code)
    {
        $translatorLanguageRepository = resolve(Modules\Translation\Contracts\TranslatorLanguageContract::class);

        return $translatorLanguageRepository->findBySlug($code);
    }
}

if (!function_exists('get_translator_language_id')) {
    /**
     * Get translator language
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_translator_language_id($code)
    {
        return Cache::tags('translator_language_id')
            ->rememberForever("translator_language_id.$code", function () use ($code) {
                $translatorLanguageRepository = resolve(Modules\Translation\Contracts\TranslatorLanguageContract::class);
                if ($translatorLanguageRepository->findBySlug($code)) {
                    return $translatorLanguageRepository->findBySlug($code)->id;
                }
                return;
            });
    }
}

if (!function_exists('get_translator_page')) {
    /**
     * Get translator page
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_translator_page($name)
    {
        $translatorPageRepository = resolve(Modules\Translation\Contracts\TranslatorPageContract::class);

        return $translatorPageRepository->findBySlug($name);
    }
}

if (!function_exists('get_translator_page_id')) {
    /**
     * Get translator page
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_translator_page_id($name)
    {
        return Cache::tags('translator_page_id')
            ->rememberForever("translator_page_id.$name", function () use ($name) {
                $translatorPageRepository = resolve(Modules\Translation\Contracts\TranslatorPageContract::class);
                if ($translatorPageRepository->findBySlug($name)) {
                    return $translatorPageRepository->findBySlug($name)->id;
                }
                return;
            });
    }
}

// Credit Helpers
if (!function_exists('get_bank_credit_type')) {
    /**
     * Get bank credit type
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_bank_credit_type($name)
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Contracts\BankCreditTypeContract::class);

        return $bankCreditTypeRepository->findBySlug($name);
    }
}

if (!function_exists('get_bank_transaction_type')) {
    /**
     * Get bank transaction type
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_bank_transaction_type($name)
    {
        $bankTransactionTypeRepository = resolve(Modules\Credit\Contracts\BankTransactionTypeContract::class);

        return $bankTransactionTypeRepository->findBySlug($name);
    }
}

// RBAC Helpers
if (!function_exists('get_ability_category_id')) {
    /**
     * Get abiltity category id
     *
     * @param  unknown $name
     * @return unknown
     */
    function get_ability_category_id($name)
    {
        $abilityCategoryRepository = resolve(Modules\Rbac\Contracts\AbilityCategoryContract::class);

        $abilityCategory = $abilityCategoryRepository->findBySlug($name);

        if ($abilityCategory) {
            return $abilityCategory->id;
        }

        return;
    }
}

if (!function_exists('get_role_ids')) {
    function get_role_ids($is_admin = false)
    {
        $member_ids = [3, 4, 5, 6];

        if ($is_admin) {
            return \Modules\Rbac\Models\Role::whereNotIn('id', $member_ids)->pluck('id')->toArray();
        }
        return $member_ids;
    }
}

if (!function_exists('attribute_dot')) {
    /**
     * Convert arrayed attribute names to dot to be compatible with laravel
     *
     * @param  unknown $name
     * @return unknown
     */
    function attribute_dot($name)
    {
        return str_replace(['[', ']'], ['.', ''], $name);
    }
}

if (!function_exists('deposit_types_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function deposit_types_dropdown()
    {
        $statusRepo = resolve(Modules\Deposit\Contracts\DepositTypeContract::class);

        return $statusRepo->all()
            ->dropDown('name', 'id');
    }
}

if (!function_exists('date_range_extract')) {
    /**
     * Extract date range
     *
     * @param $date
     * @return array
     * @throws ValidationException
     */
    function date_range_extract($date)
    {
        $fromDate = null;
        $toDate   = null;
        if (false !== strpos($date, ' ')) {
            preg_match('/^(\d{4}-\d{2}-\d{2}).+(\d{4}-\d{2}-\d{2})$/', $date, $matches);

            if (0 < count($matches)) {
                try {
                    $fromDate = Carbon::parse($matches[1])->startOfDay();
                    $toDate   = Carbon::parse($matches[2])->endOfDay();
                } catch (\Exception $e) {
                    $error = ValidationException::withMessages([
                        'date' => [__('s_validation.invalid date range')],
                    ]);
                    throw $error;
                }
            }
        } else {
            try {
                $fromDate = Carbon::parse($date)->startOfDay();
                $toDate   = Carbon::parse($date)->endOfDay();
            } catch (\Exception $e) {
                $error = ValidationException::withMessages([
                    'date' => [__('s_validation.invalid date range')],
                ]);
                throw $error;
            }
        }

        return [$fromDate, $toDate];
    }
}
