<?php

use Modules\Campaign\Contracts\CampaignStatusContract;
use Modules\Credit\Contracts\BankCreditTypeContract;
use Modules\Credit\Contracts\BankTransactionTypeContract;
use Modules\Withdrawal\Models\WithdrawalBankName;

// Country Helpers
if (!function_exists('countries_dropdown')) {

    /**
     * Get countries list.
     *
     * @return unknown
     */
    function countries_dropdown()
    {
        $countryRepository = resolve(Modules\Country\Contracts\CountryContract::class);

        return $countryRepository->allActive([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');
    }
}

// Country Helpers
if (!function_exists('countries_prefix_dropdown')) {

    /**
     * Get countries prefix list.
     *
     * @return unknown
     */
    function countries_prefix_dropdown()
    {
        $countryRepository = resolve(Modules\Country\Contracts\CountryContract::class);

        return $countryRepository->allActive([], 0, ['*'], ['name' => 'asc'])->dropDown('dialling_code', 'id');
    }
}

// User Helpers
if (!function_exists('user_statuses_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function user_statuses_dropdown()
    {
        $userStatusRepository = resolve(Modules\User\Contracts\UserStatusContract::class);

        return $userStatusRepository->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');
    }
}

if (!function_exists('deposit_statuses_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function deposit_statuses_dropdown()
    {
        $depositStatusContract = resolve(Modules\Deposit\Contracts\DepositStatusContract::class);

        return $depositStatusContract->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');
    }
}

if (!function_exists('deposit_statuses')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function deposit_statuses()
    {
        $depositStatusContract = resolve(Modules\Deposit\Contracts\DepositStatusContract::class);

        return $depositStatusContract->all([], 0, ['*'], ['name' => 'asc'])->pluck('name', 'id');
    }
}

// User statuses
if (!function_exists('admin_statuses_dropdown')) {

    /**
     * Get admin statuses list.
     *
     * @return unknown
     */
    function admin_statuses_dropdown()
    {
        $userStatusRepository = resolve(Modules\User\Contracts\UserStatusContract::class);

        return $userStatusRepository
                ->getModel()
                ->whereIn('name', ['active', 'terminated'])
                ->orderBy('name', 'asc')
                ->active()
                ->get()
                ->dropDown('name', 'id');
    }
}

// Support ticket helpers
if (!function_exists('ticket_type_dropdown')) {
    /**
     * Get support ticket type dropdown
     *
     * @return unknown
     */
    function ticket_type_dropdown()
    {
        return resolve(Modules\SupportTicket\Contracts\SupportTicketTypeContract::class)
                    ->allActive()
                    ->dropDown('name', 'id');
    }
}

if (!function_exists('ticket_status_dropdown')) {
    /**
     * Get support ticket status dropdown
     *
     * @return unknown
     */
    function ticket_status_dropdown()
    {
        return resolve(Modules\SupportTicket\Contracts\SupportTicketStatusContract::class)
                ->allActive()
                ->dropDown('name', 'id');
    }
}

// Credit Helpers
if (!function_exists('credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Models\BankCreditType::class);

        return $bankCreditTypeRepository->all(['*'], 0, ['*'], ['name' => 'asc'])->where('is_active', 1)->dropDown('name', 'rawname');
    }
}

if (!function_exists('adjustable_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function adjustable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        return $bankCreditTypeRepository->getModel()->where('is_adjustable', 1)->get()->dropDown('name', 'rawname');
    }
}

if (!function_exists('transferable_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function transferable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        $bankCreditType = $bankCreditTypeRepository->getModel()->where('is_transferable', 1)->get()->dropDown('name', 'rawname');

        if (!auth()->user()->can('member_transfer_usdt_erc20') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDT_ERC20)->rawname]);
        }

        if (!auth()->user()->can('member_transfer_usdc') && auth()->user()->is_member) {
            unset($bankCreditType[$bankCreditTypeRepository->findBySlug($bankCreditTypeRepository::USDC)->rawname]);
        }

        return $bankCreditType;
    }
}

if (!function_exists('credit_conversion_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function credit_conversion_types_dropdown($types)
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Models\BankCreditType::class);

        return $bankCreditTypeRepository->getModel()->whereIn('name', $types)->where('is_active', 1)->get()->dropDown('name', 'rawname');
    }
}

if (!function_exists('system_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function system_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Models\BankCreditType::class);

        return $bankCreditTypeRepository->getModel()->where('can_deposit', 0)->where('can_withdraw', 0)->orderBy('name', 'asc')->get()->dropDown('name', 'rawname');
    }
}

if (!function_exists('crypto_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function crypto_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Models\BankCreditType::class);

        return $bankCreditTypeRepository->getModel()->where('can_withdraw', 1)->orderBy('name', 'asc')->get()->dropDown('name', 'rawname');
    }
}

if (!function_exists('withdrawal_address_credit_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function withdrawal_address_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(Modules\Credit\Models\BankCreditType::class);

        return $bankCreditTypeRepository->getModel()
            ->where('can_withdraw', 1)
            ->where('name', '<>', \Modules\Credit\Contracts\BankCreditTypeContract::DIO_CREDIT)
            ->orderBy('name', 'asc')
            ->get()
            ->dropDown('name', 'rawname');
    }
}

if (!function_exists('credit_transaction_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function credit_transaction_types_dropdown()
    {
        $bankTransactionTypeRepository = resolve(Modules\Credit\Models\BankTransactionType::class);
        $exclusions                    = [];

        if (auth()->user()->view_special_bonus == false && auth()->user()->is_member) {
            $specialBonusTransactionType = Modules\Credit\Models\BankTransactionType::where('name', BankTransactionTypeContract::SPECIAL_BONUS)->first();
            array_push($exclusions, $specialBonusTransactionType->id);
        }

        return $bankTransactionTypeRepository->all(['*'], 0, ['*'], ['name' => 'asc'])
            ->whereNotIn('id', $exclusions)
            ->where('is_active', true)
            ->dropDown('name', 'id');
    }
}

if (!function_exists('transaction_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function transaction_types_dropdown()
    {
        $bankTransactionTypeRepository = resolve(Modules\Credit\Models\BankTransactionType::class);
        $types                         = [
            'credit_adjustment',
            'credit_transfer_from',
            'credit_transfer_to',
            'pin_purchase',
            'promotion'
        ];

        $types = $bankTransactionTypeRepository->whereIn('name', $types)
            ->orderBy('name', 'asc')
            ->get()->dropDown('name', 'id');

        foreach ($types as $key => $type) {
            $types[$key] = str_replace(':member_id', '', $type);
        }

        return $types;
    }
}

if (!function_exists('bank_transaction_types_dropdown')) {
    /**
     * Get credit types list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function bank_transaction_types_dropdown()
    {
        $bankTransactionTypeRepository = resolve(Modules\Credit\Models\BankTransactionType::class);

        $types = $bankTransactionTypeRepository
            ->orderBy('name', 'asc')
            ->get()
            ->dropDown('name', 'id');

        foreach ($types as $key => $type) {
            $types[$key] = str_replace(':member_id', '', $type);
        }

        return $types;
    }
}

// Translator Helpers
if (!function_exists('translator_groups_dropdown')) {

    /**
     * Get translator group list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function translator_groups_dropdown()
    {
        $repository = resolve(\Modules\Translation\Contracts\TranslatorGroupContract::class);

        return $repository->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');
    }
}

if (!function_exists('translator_pages_dropdown')) {

    /**
     * Get translator page list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function translator_pages_dropdown()
    {
        $repository = resolve(\Modules\Translation\Contracts\TranslatorPageContract::class);

        return $repository->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id');
    }
}

if (!function_exists('languages_dropdown')) {

    /**
     * Get translator languages list.
     *
     * @return unknown
     */
    function languages_dropdown()
    {
        $translationLanguageRepository = resolve(Modules\Translation\Contracts\TranslatorLanguageContract::class);

        return $translationLanguageRepository->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'code');
    }
}

// Mobile Prefix Helpers
if (!function_exists('mobile_prefix_dropdowns')) {

    /**
     * Get mobile prefix list.
     *
     * @return unknown
     */
    function mobile_prefix_dropdowns()
    {
        $countryRepository = resolve(Modules\Country\Contracts\CountryContract::class);

        $mobilePrefix = new \Plus65\Base\Models\DropDownCollection([]);
        $countries    = $countryRepository->allActive([], 0, ['*'], ['id' => 'asc']);

        foreach ($countries as $country) {
            $mobilePrefix[] = new \Plus65\Base\Models\DropDownCollection(
                [
                   'name'          => $country->name,
                   'dialling_code' => $country->dialling_code,
                ]
            );
        }

        return $mobilePrefix->dropDown('name', 'dialling_code');
    }
}

// Withdrawal helpers
if (!function_exists('system_withdrawal_approval_statuses_dropdown')) {
    /**
     * Get withdrawal approval statuses
     * @param unknown $current
     * @return unknown
     */
    function system_withdrawal_approval_statuses_dropdown($current)
    {
        $withdrawalStatusRepository = resolve(Modules\Withdrawal\Contracts\WithdrawalStatusContract::class);
        $inclusions                 = [];

        switch ($current) {
            case $withdrawalStatusRepository::CANCELLED:
            case $withdrawalStatusRepository::PAID:
                $inclusions = [
                ];
                break;
            case $withdrawalStatusRepository::PENDING:
                $inclusions = [
                    $withdrawalStatusRepository::PROCESSING,
                    $withdrawalStatusRepository::APPROVED,
                    $withdrawalStatusRepository::REJECTED,
                    $withdrawalStatusRepository::PAID,
                ];
                break;
            case $withdrawalStatusRepository::PROCESSING:
                $inclusions = [
                    $withdrawalStatusRepository::PENDING,
                    $withdrawalStatusRepository::APPROVED,
                    $withdrawalStatusRepository::REJECTED,
                    $withdrawalStatusRepository::PAID,
                ];
                break;
            case $withdrawalStatusRepository::APPROVED:
                $inclusions = [
                    $withdrawalStatusRepository::PAID,
                ];
                break;
            default:
               break;
        }

        return $withdrawalStatusRepository->getModel()
            ->whereIn('name', $inclusions)
            ->get()
            ->dropDown('name', 'id');
    }
}

// Withdrawal helpers
if (!function_exists('blockchain_withdrawal_approval_statuses_dropdown')) {
    /**
     * Get withdrawal approval statuses
     * @param unknown $current
     * @return unknown
     */
    function blockchain_withdrawal_approval_statuses_dropdown($current)
    {
        $withdrawalStatusRepository = resolve(Modules\Withdrawal\Contracts\WithdrawalStatusContract::class);
        $inclusions                 = [];

        switch ($current) {
            case $withdrawalStatusRepository::CANCELLED:
            case $withdrawalStatusRepository::PAID:
                $inclusions = [
                ];
                break;
            case $withdrawalStatusRepository::PENDING:
                $inclusions = [
                    $withdrawalStatusRepository::PROCESSING,
                    $withdrawalStatusRepository::APPROVED,
                    $withdrawalStatusRepository::REJECTED,
                ];
                break;
            case $withdrawalStatusRepository::PROCESSING:
                $inclusions = [
                    $withdrawalStatusRepository::PENDING,
                    $withdrawalStatusRepository::APPROVED,
                    $withdrawalStatusRepository::REJECTED,
                ];
                break;
            case $withdrawalStatusRepository::APPROVED:
                $inclusions = [
                ];
                break;
            default:
                break;
        }

        return $withdrawalStatusRepository->getModel()
            ->whereIn('name', $inclusions)
            ->get()
            ->dropDown('name', 'id');
    }
}

if (!function_exists('system_withdrawal_statuses_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function system_withdrawal_statuses_dropdown($exclusions = [])
    {
        $withdrawalStatusRepository = resolve(Modules\Withdrawal\Contracts\WithdrawalStatusContract::class);

        return $withdrawalStatusRepository->getModel()
            ->whereNotIn('name', $exclusions)
            ->get()
            ->dropDown('name', 'id');
    }
}

if (!function_exists('blockchain_withdrawal_statuses_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function blockchain_withdrawal_statuses_dropdown($exclusions = [])
    {
        $withdrawalStatusRepository = resolve(Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract::class);

        return $withdrawalStatusRepository->getModel()
            ->whereNotIn('name', $exclusions)
            ->get()
            ->dropDown('name', 'id');
    }
}

// Wallet Addresses dropdown filter by user id and bank credit type slug
if (!function_exists('wallet_addresses_dropdown')) {
    /**
     * Get withdrawal wallet addresses list.
     * The format for this 'id' => 'address'
     *
     * @return unknown
     */
    function wallet_addresses_dropdown(int $userId, string $bankCreditType)
    {
        $walletAddresesRepository = resolve(Modules\Withdrawal\Contracts\WithdrawalAddressContract::class);
        $bankCreditTypeRepository = resolve(Modules\Credit\Contracts\BankCreditTypeContract::class);

        $wallets = $walletAddresesRepository->findByUserIdAndCreditType($userId, $bankCreditTypeRepository->findBySlug($bankCreditType)->id);

        return $wallets ? $wallets->get()->dropDown('name', 'id') : [];
    }
}

if (!function_exists('depositable_credit_types_dropdown')) {
    /**
     * Get withdrawable credit types.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function depositable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        return $bankCreditTypeRepository->getModel()
            ->where('can_deposit', true)
            ->where('is_active', 1)
            ->get()
            ->dropDown('name', 'rawname');
    }
}

if (!function_exists('withdrawable_credit_types_dropdown')) {
    /**
     * Get withdrawable credit types.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function withdrawable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        return $bankCreditTypeRepository->getModel()
            ->where('can_withdraw', true)
            ->where('is_active', 1)
            ->whereNull('network')
            ->get()
            ->dropDown('name', 'rawname');
    }
}

if (!function_exists('blockchain_withdrawable_credit_types_dropdown')) {
    /**
     * Get withdrawable credit types.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function blockchain_withdrawable_credit_types_dropdown()
    {
        $bankCreditTypeRepository = resolve(BankCreditTypeContract::class);

        return $bankCreditTypeRepository->getModel()
            ->where('can_withdraw', true)
            ->whereNotNull('network')
            ->get()
            ->dropDown('name', 'rawname');
    }
}

if (!function_exists('user_banks_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function user_banks_dropdown()
    {
        $userBankRepository = resolve(Modules\Withdrawal\Contracts\WithdrawalBankContract::class);

        $banks = $userBankRepository->getModel()
            ->where('user_id', auth()->user()->id)
            ->get();
        $dropdown = [];

        $dropdown[0] = __('m_withdrawal_create.select bank');
        foreach ($banks as $bank) {
            $dropdown[$bank->id] = $bank->name . ' (' . $bank->account_number . ')';
        }

        return $dropdown;
    }
}

if (!function_exists('user_deposit_methods_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function user_deposit_methods_dropdown()
    {
        $methodRepo = resolve(Modules\Deposit\Contracts\DepositMethodContract::class);

        $userDepositMethodsDropdown = $methodRepo->getUserEligibleMethods(auth()->id())
            ->pluck('name', 'id')->toArray(); // cannot use dropdown, because don't want the default option

        return $userDepositMethodsDropdown;
    }
}

if (!function_exists('deposit_methods_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return unknown
     */
    function deposit_methods_dropdown()
    {
        $methodRepo = resolve(Modules\Deposit\Contracts\DepositMethodContract::class);

        $depositMethod = $methodRepo->all()->
        where('name', $methodRepo::TELEGRAPHIC_TRANSFER)
            ->dropDown('name', 'id');
        unset($depositMethod['']);

        return $depositMethod;
    }
}

if (!function_exists('active_dropdown')) {
    /**
     * Get withdrawal statuses
     * @return array
     */
    function active_dropdown()
    {
        return [
            '0' => __('s_active_state.inactive'),
            '1' => __('s_active_state.active'),
        ];
    }
}

// User Helpers
if (!function_exists('filter_ranks_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function filter_ranks_dropdown()
    {
        $rankRepo = resolve(\Modules\Rank\Contracts\RankContract::class);

        return $rankRepo->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id', __('a_rank.-- select --'));
    }
}

// User Helpers
if (!function_exists('ranks_dropdown')) {

    /**
     * Get user statuses list.
     *
     * @return unknown
     */
    function ranks_dropdown()
    {
        $rankRepo = resolve(\Modules\Rank\Contracts\RankContract::class);

        return $rankRepo->all([], 0, ['*'], ['name' => 'asc'])->dropDown('name', 'id', __('a_rank.default to system'));
    }
}

// Goldmine Helpers
if (!function_exists('filter_goldmine_level_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function filter_goldmine_level_dropdown()
    {
        $goldmineLevelPercentageContract = resolve(Modules\Commission\Contracts\GoldmineLevelPercentageContract::class);

        return $goldmineLevelPercentageContract->all([], 0, ['*'], ['level' => 'asc'])->dropDown('level', 'level', __('a_goldmine.-- select --'));
    }
}

// Goldmine Helpers
if (!function_exists('goldmine_level_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function goldmine_level_dropdown()
    {
        $goldmineLevelPercentageContract = resolve(Modules\Commission\Contracts\GoldmineLevelPercentageContract::class);

        return $goldmineLevelPercentageContract->all([], 0, ['*'], ['level' => 'asc'])->dropDown('level', 'level', __('a_goldmine.default to system'));
    }
}

// Goldmine Helpers
if (!function_exists('filter_goldmine_rank_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function filter_goldmine_rank_dropdown()
    {
        $goldmineRankRepository = resolve(Modules\Rank\Contracts\GoldmineRankContract::class);

        return $goldmineRankRepository->all([], 0, ['*'], ['level' => 'asc'])->dropDown('name', 'id', __('a_goldmine.-- select --'));
    }
}

// Goldmine Helpers
if (!function_exists('goldmine_rank_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function goldmine_rank_dropdown()
    {
        $goldmineRankRepository = resolve(Modules\Rank\Contracts\GoldmineRankContract::class);

        return $goldmineRankRepository->all([], 0, ['*'], ['level' => 'asc'])->dropDown('name', 'id', __('a_goldmine.default to system'));
    }
}

// Goldmine Helpers
if (!function_exists('filter_leader_bonus_type_dropdown')) {
    /**
     * Get ranks list.
     * The format for this 'id' => 'name'
     *
     * @return unknown
     */
    function filter_leader_bonus_type_dropdown()
    {
        $leaderBonusType = resolve(BankTransactionTypeContract::class);

        return $leaderBonusType->getModel()
            ->whereIn('name', [BankTransactionTypeContract::LEADER_BONUS_SAME_RANK, BankTransactionTypeContract::LEADER_BONUS])
            ->get()
            ->dropDown('name', 'id', __('a_leader_bonus.-- select --'));
    }
}

// Adjustment Type Dropdowns
if (!function_exists('adjustment_types_dropdown')) {
    /**
     * Get adjustment type.
     *
     * @return unknown
     */
    function adjustment_types_dropdown()
    {
        return  [
            'all'    => __('a_admin_management_adjust_credit.all'),
            'debit'  => __('a_admin_management_adjust_credit.adjust out'),
            'credit' => __('a_admin_management_adjust_credit.adjust in'),
        ];
    }
}

if (!function_exists('penalty_days_dropdown')) {
    /**
     * Get adjustment type.
     *
     * @return unknown
     */
    function penalty_days_dropdown()
    {
        return  [
            '0'  => __('a_admin_management_adjust_credit.all'),
            'no' => __('a_admin_management_adjust_credit.no penalty'),
            '30' => __('a_admin_management_adjust_credit.30 days'),
            '90' => __('a_admin_management_adjust_credit.90 days'),
        ];
    }
}

if (!function_exists('withdrawal_bank_names_dropdown')) {
    /**
     * Get withdrawal bank names.
     *
     * @return unknown
     */
    function withdrawal_bank_names_dropdown()
    {
        $bankNames = WithdrawalBankName::all()->dropdown('name', 'name');

        return  $bankNames;
    }
}

if (!function_exists('withdrawal_users_bank_names_dropdown')) {
    /**
     * Get withdrawal bank names.
     *
     * @return unknown
     */
    function withdrawal_users_bank_names_dropdown($userId = null)
    {
        $bankNames = \Modules\Withdrawal\Models\WithdrawalBank::selectRaw('distinct name as name')
            ->whereNull('deleted_at')
            ->when($userId, function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })->get()->dropdown('name', 'name');

        return  $bankNames;
    }
}

if (!function_exists('gender_dropdown')) {
    /**
     * Get withdrawal bank names.
     *
     * @return unknown
     */
    function gender_dropdown()
    {
        return [
            null => __('m_my_profile.select gender'),
            'M'  => __('m_my_profile.male'),
            'F'  => __('m_my_profile.female'),
        ];
    }
}

if (!function_exists('campaign_statuses_dropdown')) {
    /**
     * Get withdrawable credit types.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function campaign_statuses_dropdown($exclusions = [])
    {
        $campaignStatusRepo = resolve(CampaignStatusContract::class);

        return $campaignStatusRepo->getModel()
            ->where('is_active', 1)
            ->whereNotIn('name', $exclusions)
            ->get()
            ->dropDown('name', 'id');
    }
}

if (!function_exists('blockchain_request_approval_status_dropdown')) {

    /**
     * Get source credit list.
     * The format for this 'slug' => 'rawname'
     *
     * @return unknown
     */
    function blockchain_request_approval_status_dropdown()
    {
        $repository = resolve(\Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract::class);
        $statuses   = [
            $repository->findBySlug($repository::PROCESSING)->id,
            $repository->findBySlug($repository::PENDING)->id,
        ];

        return $repository->getModel()->where('is_active', 1)->whereIn('id', $statuses)->get()->dropDown('name', 'id');
    }
}

if (!function_exists('blockchain_action_status_dropdown')) {

    /**
     * Get source credit list.
     * The format for this 'slug' => 'rawname'
     *
     * @param null $withdrawalId
     * @return unknown
     */
    function blockchain_action_status_dropdown($withdrawalId = null)
    {
        $blockchainWithdrawalRepo = resolve(\Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalContract::class);
        $repository               = resolve(\Modules\BlockchainWithdrawal\Contracts\BlockchainWithdrawalStatusContract::class);
        $processingStatusId       = $repository->findBySlug($repository::PROCESSING)->id;
        $statuses                 = [
            $processingStatusId,
            $repository->findBySlug($repository::APPROVED)->id,
            $repository->findBySlug($repository::REJECTED)->id,
        ];

        if ($withdrawalId) {
            $withdrawalRequest = $blockchainWithdrawalRepo->find($withdrawalId);
            if ($withdrawalRequest->blockchain_withdrawal_status_id === $processingStatusId) {
                unset($statuses[array_search($processingStatusId, $statuses)]);
            }
        }

        return $repository->getModel()->where('is_active', 1)->whereIn('id', $statuses)->get()->dropDown('name', 'id');
    }
}