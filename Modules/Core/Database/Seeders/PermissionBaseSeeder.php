<?php

namespace Modules\Core\Database\Seeders;

use Bouncer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Rbac\Contracts\AbilityContract;
use Modules\Rbac\Contracts\AbilitySectionContract;
use Modules\Rbac\Contracts\RoleContract;

class PermissionBaseSeeder extends Seeder
{
    /**
     * The ability section repository
     *
     * @var unknown
     */
    protected $abilitySectionRepository;

    /**
     * The ability repository
     *
     * @var unknown
     */
    protected $abilityRepository;

    /**
     * The role repository
     *
     * @var unknown
     */
    protected $roleRepository;

    /**
     * Class constructor
     *
     * @param AbilitySectionContract $abilitySectionContract
     * @param AbilityContract $abilityContract
     * @param RoleContract $roleContract
     */
    public function __construct(
        AbilitySectionContract $abilitySectionContract,
        AbilityContract $abilityContract,
        RoleContract $roleContract
    ) {
        $this->abilitySectionRepository = $abilitySectionContract;
        $this->abilityRepository        = $abilityContract;
        $this->roleRepository           = $roleContract;
    }

    /**
     * Seed ability section and abilities
     *
     * @return void
     */
    protected function seedAbilities($abilitySections)
    {
        foreach ($abilitySections as $abilitySection) {
            $sectionName      = $abilitySection['section']['name'];
            $categoryId       = $abilitySection['section']['category_id'];
            $sectionAbilities = $abilitySection['abilities'];

            $abilitySection = $this->abilitySectionRepository->getModel()
                ->where('name', $sectionName)
                ->where('ability_category_id', $categoryId)
                ->first();

            if (!$abilitySection) {
                $abilitySectionAttributes = [
                        'name'                => $sectionName,
                        'ability_category_id' => $categoryId
                    ];
                $abilitySection = $this->abilitySectionRepository->getModel()->newInstance($abilitySectionAttributes);
                $abilitySection->save();
            }

            foreach ($sectionAbilities as $sectionAbility) {
                $ability = $this->abilityRepository->findBySlug($sectionAbility['name']);
                if (!$ability) {
                    $abilityAttributes = [
                        'name'  => $sectionAbility['name'],
                        'title' => $sectionAbility['title'],
                        //'is_hidden' => isset($sectionAbility['is_hidden'])? $sectionAbility['is_hidden'] : 0
                    ];
                    $ability = $this->abilityRepository->getModel()->newInstance($abilityAttributes);
                    $ability->save();
                }

                // Attach new section for the ability
                if (!in_array($ability->id, $ability->sections()->pluck('id')->toArray())) {
                    $ability->sections()->attach($abilitySection->id);
                }
            }
        }
    }

    /**
     * Seed role permissions
     *
     * @return void
     */
    protected function seedPermissions($permissions)
    {
        foreach ($permissions as $roleName => $abilities) {
            $role = $this->roleRepository->findBySlug($roleName);

            foreach ($abilities as $abilityName) {
                $abilityAbilitySection = DB::table('ability_ability_section')
                    ->select('ability_ability_section.*')
                    ->join('abilities', 'abilities.id', '=', 'ability_ability_section.ability_id')
                    ->join('ability_sections', 'ability_sections.id', '=', 'ability_ability_section.ability_section_id')
                    ->where('abilities.name', $abilityName)
                    ->where('ability_sections.ability_category_id', $role->ability_category->id)
                    ->first();

                if ($abilityAbilitySection) {
                    $ability = $this->abilityRepository->find($abilityAbilitySection->ability_id);

                    Bouncer::allow($role)->to($ability);
                }
            }
        }
    }
}
