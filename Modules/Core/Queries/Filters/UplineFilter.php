<?php

namespace Modules\Core\Queries\Filters;

use Modules\Tree\Models\SponsorTree;
use QueryBuilder\QueryFilter;

class UplineFilter extends QueryFilter
{
    /**
     *
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryFilter::filter()
     */
    public function filter($builder)
    {
        $elName = $this->name;

        if (!$this->parameter->has($elName) || !$elName) {
            return $builder;
        }

        //This is the custom filters for username and its downline
        //The filter name will be xxx__downlines if u want to filter downlines
        //if xxx_downlines is passed as $elName, will get the xxx users and its downlines
        //if xxx is passed as $elName, will just filter xxx as normal (using like)
        $isUplineChecked   = false;
        $uplineElementName = $elName . '_uplines';
        if ($this->parameter->has($uplineElementName) && $this->parameter->get($uplineElementName) == 1) {
            $isUplineChecked = true;
        }

        if ($this->alias) {
            if (false !== strpos($this->alias, '.')) {
                [$table, $colName] = explode('.', $this->alias);
            } else {
                $colName = $this->alias;
                $table   = $this->getTableName($builder);
            }
        } else {
            $table   = $this->getTableName($builder);
            $colName = $elName;
        }

        if (!$table) {
            throw new \Exception('Unknown instance of builder');
        }

        $value  = $this->parameter->get($elName);
        $column = $table . '.' . $colName;

        //if xxx_downlines is selected, will get the xxx users and its downlines
        if ($isUplineChecked) {
            //get users in sponsor tree
            $user = SponsorTree::join('users', 'users.id', '=', 'sponsor_trees.user_id')
                ->where($elName, $this->parameter->get($elName))
                ->first();

            if ($user) {
                return $builder->join('sponsor_trees as ' . $elName . '_table', $table . '.id', '=', $elName . '_table.user_id')
                    ->where($elName . '_table.lft', '<=', $user->lft)
                    ->where($elName . '_table.rgt', '>=', $user->rgt);
            }

            return $builder->where($column, $value);
        }

        return $builder->where($column, 'like', '%' . $value . '%');
    }
}
