<?php

namespace Modules\Core\Queries\Filters;

use Modules\Tree\Models\SponsorTree;
use QueryBuilder\QueryFilter;

class DownlineFilter extends QueryFilter
{
    /**
     *
     * {@inheritDoc}
     *
     * @see \QueryBuilder\QueryFilter::filter()
     */
    public function filter($builder)
    {
        $elName = $this->name;

        if (!$this->parameter->has($elName) || !$elName) {
            return $builder;
        }

        //This is the custom filters for username and its downline
        //The filter name will be xxx__downlines if u want to filter downlines
        //if xxx_downlines is passed as $elName, will get the xxx users and its downlines
        //if xxx is passed as $elName, will just filter xxx as normal (using like)
        $isDownlineChecked   = false;
        $downlineElementName = $elName . '_downlines';
        if ($this->parameter->has($downlineElementName) && $this->parameter->get($downlineElementName) == 1) {
            $isDownlineChecked = true;
        }

        $isUplineChecked   = false;
        $uplineElementName = $elName . '_uplines';
        if ($this->parameter->has($uplineElementName) && $this->parameter->get($uplineElementName) == 1) {
            $isUplineChecked = true;
        }

        if ($this->alias) {
            if (false !== strpos($this->alias, '.')) {
                [$table, $colName] = explode('.', $this->alias);
            } else {
                $colName = $this->alias;
                $table   = $this->getTableName($builder);
            }
        } else {
            $table   = $this->getTableName($builder);
            $colName = $elName;
        }

        if (!$table) {
            throw new \Exception('Unknown instance of builder');
        }

        $value = $this->parameter->get($elName);
        $user  = null;

        if ($isDownlineChecked || $isUplineChecked) {
            //get users in sponsor tree
            $user = SponsorTree::join('users', 'users.id', '=', 'sponsor_trees.user_id')
                ->where(function ($builder) use ($table, $value) {
                    $builder->where('member_id', 'like', '%' . $value . '%');
                    //->orWhere('username', 'like', '%' . $value . '%')
                    //->orWhere('nickname', 'like', '%' . $value . '%');
                })
                ->first();
        }

        //if xxx_downlines is selected, will get the xxx users and its downlines
        if ($isDownlineChecked && $isUplineChecked && $user) {
            return $builder->join('sponsor_trees as ' . $elName . '_table', $table . '.id', '=', $elName . '_table.user_id')
                    ->where(function ($builder) use ($elName, $user) {
                        $builder
                        ->where($elName . '_table.lft', '>=', $user->lft)
                        ->where($elName . '_table.rgt', '<=', $user->rgt)
                        ->orWhere(function ($builder) use ($elName, $user) {
                            $builder->where($elName . '_table.lft', '<=', $user->lft)
                            ->where($elName . '_table.rgt', '>=', $user->rgt);
                        });
                    })->orderBy($elName . '_table.level')
                    ->orderBy($elName . '_table.user_id');
        /*->where($elName . '_table.lft', '>=', $user->lft)
        ->where($elName . '_table.rgt', '<=', $user->rgt);*/
        } elseif ($isDownlineChecked && $user) {
            return $builder->join('sponsor_trees as ' . $elName . '_table', $table . '.id', '=', $elName . '_table.user_id')
                ->where(function ($builder) use ($elName, $user) {
                    $builder
                        ->where($elName . '_table.lft', '>=', $user->lft)
                        ->where($elName . '_table.rgt', '<=', $user->rgt);
                })->orderBy($elName . '_table.level')
                ->orderBy($elName . '_table.user_id');
        } elseif ($isUplineChecked && $user) {
            return $builder->join('sponsor_trees as ' . $elName . '_table', $table . '.id', '=', $elName . '_table.user_id')
                ->where(function ($builder) use ($elName, $user) {
                    $builder
                        ->where($elName . '_table.lft', '<=', $user->lft)
                        ->where($elName . '_table.rgt', '>=', $user->rgt);
                })->orderBy($elName . '_table.level')
                ->orderBy($elName . '_table.user_id');
        }

        return $builder->where(function ($builder) use ($table, $value) {
            $builder->where($table . '.member_id', 'like', '%' . $value . '%');
            //->orWhere($table . '.username', 'like', '%' . $value . '%')
            //->orWhere($table . '.nickname', 'like', '%' . $value . '%');
        });
    }
}
