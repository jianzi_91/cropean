<?php

namespace Modules\Core\Contracts;

interface LockContract
{
    /**
     * Lock record.
     *
     * @param int $id
     * @return mixed
     */
    public function lock(int $id);
}
