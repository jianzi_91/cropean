<?php

namespace Modules\Core\Contracts;

use Illuminate\Database\Eloquent\Model;

interface BulkActionContract
{
    /**
     * Add to bulk data.
     *
     * @param array $data
     */
    public function addToBulkInsert(array $data);

    /**
     * Execute bulk insert.
     */
    public function executeBulkInsert();

    /**
     * Add to bulk update.
     *
     * @param string $updateColumn
     * @param string $caseColumn
     * @param array $data
     * @return mixed
     */
    public function addToBulkUpdate(string $updateColumn, string $caseColumn, array $data);

    /**
     * Execute bulk insert.
     *
     * @param string $whereColumn
     * @param array $whereValues
     */
    public function executeBulkUpdate(string $whereColumn, array $whereValues);

    /**
     * Get model
     *
     * @return Model
     */
    public function getModel();
}
