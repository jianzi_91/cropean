<?php

return [
    'name'        => 'Core',
    'member_url'  => env('MEMBER_URL', 'member.base-code.test'),
    'admin_url'   => env('ADMIN_URL', 'admin.base-code.test'),
    'website_url' => env('WEBSITE_URL', 'base-code.test'),
];
