<?php

namespace Modules\Core\Exceptions;

class UnsupportedMimeType extends \Exception
{
    /**
     * The current mime type
     *
     * @var unknown
     */
    protected $mime;

    /**
     * The supported mime type
     *
     * @var unknown
     */
    protected $supported;

    /**
     * Class's constructor
     *
     * @param unknown $mime
     * @param unknown $supported
     * @param unknown $message
     * @param unknown $code
     * @param unknown $previous
     */
    public function __construct($mime = null, $supported = null, $message = null, $code = null, $previous = null)
    {
        $this->mime      = $mime;
        $this->supported = $supported;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get mime type
     *
     * @return unknown
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Get supported
     *
     * @return unknown
     */
    public function getSupported()
    {
        return $this->supported;
    }
}
