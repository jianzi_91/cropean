<?php

namespace Modules\Core\Exceptions;

class MaxFileSize extends \Exception
{
    /**
     * The size limit
     *
     * @var unknown
     */
    protected $limit = 0;

    /**
     * The file size
     *
     * @var unknown
     */
    protected $size = 0;

    /**
     * Class's constructor
     *
     * @param unknown $size
     * @param unknown $limit
     * @param unknown $message
     * @param unknown $code
     * @param unknown $previous
     */
    public function __construct($size = null, $limit = null, $message = null, $code = null, $previous = null)
    {
        $this->size  = $size;
        $this->limit = $limit;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get file size
     *
     * @return unknown
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get file size limit
     *
     * @return unknown
     */
    public function getLimit()
    {
        return $this->limit;
    }
}
