<?php

namespace Modules\Core\Traits;

trait HasLock
{
    /**
     * Lock record.
     *
     * @param int $id
     * @return mixed
     */
    public function lock(int $id)
    {
        return $this->model
            ->where('id', $id)
            ->lockForUpdate()
            ->first();
    }
}