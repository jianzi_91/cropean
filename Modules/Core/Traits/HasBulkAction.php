<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\DB;

trait HasBulkAction
{
    /**
     * Chunk size.
     *
     * @var int
     */
    protected $chunkSize = 500;

    /**
     * Bulk insert data.
     *
     * @var array
     */
    protected $bulkInsertData = [];

    /**
     * Bulk update data.
     *
     * @var array
     */
    protected $bulkUpdateData = [];

    /**
     * Bulk update case columns.
     *
     * @var array
     */
    protected $bulkUpdateCaseColumns = [];

    /**
     * Add to bulk insert data.
     *
     * @param array $data
     */
    public function addToBulkInsert(array $data)
    {
        $this->bulkInsertData[] = $data;
    }

    /**
     * Add to bulk update data.
     *
     * @param string $updateColumn
     * @param string $caseColumn
     * @param array $data
     */
    public function addToBulkUpdate(string $updateColumn, string $caseColumn, array $data)
    {
        $this->bulkUpdateData[$updateColumn]        = $data;
        $this->bulkUpdateCaseColumns[$updateColumn] = $caseColumn;
    }

    /**
     * Execute bulk insert.
     */
    public function executeBulkInsert()
    {
        if (empty($this->bulkInsertData)) {
            return;
        }

        $now = now()->toDateTimeString();

        foreach ($this->bulkInsertData as &$row) {
            if (!array_key_exists('created_at', $row)) {
                $row['created_at'] = $now;
            }

            if (!array_key_exists('updated_at', $row)) {
                $row['updated_at'] = $now;
            }
        }

        foreach (array_chunk($this->bulkInsertData, $this->chunkSize) as $chunk) {
            $this->getModel()->insert($chunk);
        }

        $this->bulkInsertData = [];
    }

    /**
     * Execute bulk insert.
     *
     * @param string $whereColumn
     * @param array $whereValues
     */
    public function executeBulkUpdate(string $whereColumn, array $whereValues)
    {
        if (empty($this->bulkUpdateData)) {
            return;
        }

        $sql = "UPDATE {$this->getModel()->getTable()} SET ";

        foreach ($this->bulkUpdateData as $column => $updates) {
            if (!empty($updates)) {
                $sql .= $this->constructCaseSql($column, $this->bulkUpdateCaseColumns[$column], $updates);
            }
        }

        foreach ($whereValues as &$value) {
            $value = "'$value'";
        }

        $concatenatedWhereValues = implode(',', $whereValues);

        $sql .= "updated_at = NOW() WHERE $whereColumn IN ($concatenatedWhereValues)";

        DB::unprepared($sql);

        $this->bulkUpdateData = [];
    }

    /**
     * Construct SQL for the CASE-WHEN-THEN portion for the bulk update.
     *
     * @param string $column
     * @param $caseColumn
     * @param array $updates
     * @return string
     */
    protected function constructCaseSql(string $column, string $caseColumn, array $updates)
    {
        $sql = "$column = (CASE $caseColumn ";

        foreach ($updates as $caseValue => $newColumnValue) {
            $sql .= "WHEN '$caseValue' THEN '$newColumnValue' ";
            $quotedCaseValues[] = "'$caseValue'";
        }

        return $sql . ' END), ';
    }
}
