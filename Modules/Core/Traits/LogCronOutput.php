<?php

namespace Modules\Core\Traits;

use Illuminate\Support\Facades\Log;

/**
 * Trait LogCronOutput
 *
 * @package Trading\BlockchainParser\Traits
 * @author  Benson Liang <benson.liang@plus65.com.sg>
 * @access  public
 */
trait LogCronOutput
{
    /**
     * Logs for block processor cron.
     *
     * @param string $message
     */
    public function logBlockProcessor(string $message)
    {
        $this->writeLog('processBlocksCron', $message);
    }

    /**
     * Logs for AutoBot creator.
     *
     * @param string $message
     */
    public function logAutoBotCreator(string $message)
    {
        $this->writeLog('autoBotCreator', $message);
    }

    /**
     * Logs for AutoBot fulfiller.
     *
     * @param string $message
     */
    public function logAutoBotFulfiller(string $message)
    {
        $this->writeLog('autoBotFulfiller', $message);
    }

    /**
     * Logs for trading floor.
     *
     * @param string $message
     */
    public function logTrading(string $message)
    {
        $this->writeLog('trading', $message);
    }

    /**
     * Logs for transfer out
     *
     * @param string $message
     */
    public function logTransferOut(string $message)
    {
        $this->writeLog('transferOut', $message);
    }

    /**
     * Logs for blocks confirmation
     *
     * @param string $message
     */
    public function logBlockConfirmation(string $message)
    {
        $this->writeLog('processBlocksConfirmation', $message);
    }

    /**
     * Logs for blocks confirmation
     *
     * @param string $message
     */
    public function logErc20Processor(string $message)
    {
        $this->writeLog('erc20Processor', $message);
    }

    /**
     * Logs for master wallet redirection
     *
     * @param string $message
     */
    public function logMasterWalletRedirection(string $message)
    {
        $this->writeLog('masterWalletRedirection', $message);
    }

    /**
     * Logs for exchange rates
     *
     * @param string $message
     */
    public function logExchangeRate(string $message)
    {
        $this->writeLog('exchangeRate', $message);
    }

    /**
     * Logs for e contract pdf generation
     *
     * @param string $message
     */
    public function logEContract(string $message)
    {
        $this->writeLog('eContractPdf', $message);
    }

    /**
     * Logs for member termination cron
     *
     * @param string $message
     */
    public function logUserTermination(string $message)
    {
        $this->writeLog('userTermination', $message);
    }

    /**
     * Write log.
     *
     * @param string $channel
     * @param string $message
     */
    protected function writeLog(string $channel, string $message)
    {
        Log::channel($channel)->info($message);
    }
}
