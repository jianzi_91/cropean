<?php

Route::group(['middleware' => 'web'], function () {
    Route::group(['domain' => config('core.member_url')], function () {
        Route::get('/register', ['as' => 'member.register', 'uses' => 'Member\RegistrationController@create']);
        Route::post('/register', ['as' => 'member.register.submit', 'uses' => 'Member\RegistrationController@store']);
        Route::get('/register/{member_id}/qrcode', ['as' => 'member.register.qrcode', 'uses' => 'Member\RegistrationController@QrRegistration']);
        Route::get('/register/{member_id}/qrcode/download', ['as' => 'member.register.qrcode.download', 'uses' => 'Member\RegistrationController@QrDownload']);
    });
});
