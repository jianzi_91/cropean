<?php

namespace Modules\Registration\Events;

use Illuminate\Queue\SerializesModels;
use Modules\User\Models\User;

class NewUserRegistered
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @var unknown
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
