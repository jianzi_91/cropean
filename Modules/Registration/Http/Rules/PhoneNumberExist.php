<?php

namespace Modules\Registration\Http\Rules;

use Illuminate\Contracts\Validation\Rule;
use Modules\User\Models\User;

class PhoneNumberExist implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (request()->has('mobile_prefix') && !empty(request()->mobile_prefix)) {
            $existed = User::selectRaw('concat(mobile_prefix, mobile_number) as phone_number')
                ->having('phone_number', request()->mobile_prefix . request()->mobile_number)
                ->first();

            if ($existed) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('s_validation.the :attribute is existed');
    }
}
