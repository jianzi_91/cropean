<?php

namespace Modules\Registration\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Password\Http\Rules\PasswordRegex;
use Modules\Registration\Http\Rules\PhoneNumberExist;
use Modules\Tree\Http\Rules\ReferrerRelation;
use Modules\Tree\Http\Rules\SponsorExist;
use Modules\User\Http\Rules\UserIsActive;
use Modules\Tree\Http\Rules\SponsorReferrerRelation;

class Register extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                            => 'required|max:255|string',
            'id_number'                       => 'required|unique:users,id_number',
            'country_id'                      => 'required|exists:countries,id',
            'birth_date'                      => 'required|date|before:18 years ago',
            'gender'                          => ['bail', 'required', Rule::in(['M', 'F'])],
            'mobile_prefix'                   => 'required|numeric|exists:countries,dialling_code',
            'mobile_number'                   => ['required', 'numeric', 'digits_between:5,20', new PhoneNumberExist()],
            'email'                           => 'required|email|max:255|unique:users,email',
            'password'                        => ['required', 'min:' . config('password.primary_password_min_characters'), new PasswordRegex(), 'confirmed'],
            'password_confirmation'           => 'required|min:' . config('password.primary_password_min_characters'),
            'secondary_password'              => ['required', 'min:' . config('password.secondary_password_min_characters'), 'different:password', 'confirmed', new PasswordRegex()],
            'secondary_password_confirmation' => 'required|min:' . config('password.secondary_password_min_characters'),
            'sponsor_id'                      => ['bail', 'required', new SponsorExist(), new UserIsActive(), new SponsorReferrerRelation($this->referrer_id)],
            'address'                         => 'required|max:256',
            'referrer_id'                     => ['bail', 'required', new SponsorExist(), new ReferrerRelation($this->sponsor_id)],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'birth_date.before'                   => __('m_register.you must be at least 18 years old'),
            'captcha.captcha'                     => __('m_register.invalid captcha'),
            'password.min'                        => __('m_register.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')]),
            'password_confirmation.min'           => __('m_register.must contain no less than :min characters case sensitive and cannot be the same as secondary password', ['min' => config('password.primary_password_min_characters')]),
            'secondary_password.min'              => __('m_register.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')]),
            'secondary_password_confirmation.min' => __('m_register.must contain no less than :min characters case sensitive and cannot be the same as primary password', ['min' => config('password.secondary_password_min_characters')]),
        ];
    }

    /**
     * Set custom attributes' names.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'                            => __('m_register.full name'),
            'id_number'                       => __('m_register.national id'),
            'country_id'                      => __('m_register.country'),
            'birth_date'                      => __('m_register.date of birth'),
            'mobile_prefix'                   => __('m_register.mobile prefix'),
            'mobile_number'                   => __('m_register.mobile number'),
            'email'                           => __('m_register.email address'),
            'password'                        => __('m_register.password'),
            'password_confirmation'           => __('m_register.confirm password'),
            'secondary_password'              => __('m_register.secondary password'),
            'secondary_password_confirmation' => __('m_register.confirm secondary password'),
            'sponsor_id'                      => __('m_register.sponsor id'),
            'address'                         => __('m_register.address'),
            'referrer_id'                     => __('m_register.referrer id'),
            'gender'                          => __('m_register.gender')
        ];
    }
}
