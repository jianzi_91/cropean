<?php

namespace Modules\Registration\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Registration\Http\Requests\Member\Register;
use Modules\User\Contracts\UserContract;
use Modules\User\Models\User;
use Modules\User\Repositories\UserRepository;
use Plus65\Utility\Exceptions\WebException;

class RegistrationController extends Controller
{
    /**
     * The user repository
     *
     * @var unknown
     */
    protected $userRepository;

    /**
     * Class constructor
     *
     * @param UserContract $userContract
     */
    public function __construct(UserContract $userContract)
    {
        /** @var UserRepository $userContract */
        $this->userRepository = $userContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('registration::member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Register $request)
    {
        $user = null;
        DB::beginTransaction();
        try {
            $data = $request->only(['name', 'email', 'password', 'secondary_password', 'sponsor_id', 'referrer_id', 'id_number', 'country_id', 'birth_date', 'gender', 'address', 'mobile_prefix', 'mobile_number']);
            $user = $this->userRepository->register($data);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw (new WebException($e))->redirectTo(route('member.register'))->withMessage(__('m_register.fail to register'));
        }

        return redirect()->route('member.login')->with('success', __('m_register.successful register'));
    }

    public function QrRegistration(Request $request, $member_id)
    {
        $user = User::where('member_id', $member_id)->firstOrFail();

        return view('registration::member.qrcode')->with('user', $user);
    }

    public function QrDownload(Request $request, $member_id)
    {
    }
}
