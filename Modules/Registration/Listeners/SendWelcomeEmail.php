<?php

namespace Modules\Registration\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use Modules\Registration\Emails\RegistrationSuccess;
use Modules\Registration\Events\NewUserRegistered;

class SendWelcomeEmail implements ShouldQueue
{
    public $tries = 3;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        if (in_array(config('app.env'), ['testing', 'local']) || !config('app.welcome_email')) {
            return true;
        }

        // TODO: proxy mailer support
        Mail::to($event->user->email)->send(new RegistrationSuccess($event->user));
    }
}
