<?php

namespace Modules\Regisration\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationSuccess extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * User
     * @var unknown
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('registration::member.welcome_email')
            ->subject(__('m_registration_success.successfully registered!'))
            ->with([
                'name'     => $this->user->fullname,
                'memberId' => $this->user->member_id,
            ]);
    }
}
