@component('mail::panel')
Dear {{ $name }},

Your account details are as follows:


<b>Name: </b>{{ $name }}<br>
<b>Member ID: </b>{{ $memberId }}

Regards,<br>

------------------------------------

亲爱的 {{ $name }}

您的账户详情如下：<br>
姓名：{{ $name }}<br>
会员账号：{{ $memberId }}<br>

敬上<br>


@endcomponent