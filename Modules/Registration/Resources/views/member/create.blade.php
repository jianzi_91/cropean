@extends('templates.member.wide')
@section('title', __('m_page_title.register'))

@section('main')
    <section id="auth-login" class="row flexbox-container">
        <div class="col-xl-8 col-11">
            <div class="wide lang-selector">
                @foreach(languages() as $locale=>$name)
                <div data-lang="{{ $locale }}" class="lang-item {{ app()->getLocale() == $locale ? 'active' : '' }}">{{ $name }}</div>
                @if (!$loop->last)
                <div>&nbsp;&nbsp;|&nbsp;&nbsp;</div>
                @endif
                @endforeach
            </div>
        </div>
        <div class="col-xl-8 col-11">
            <div class="card bg-authentication mb-0">
                <div class="row m-0" style="background-color: white;">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 d-flex justify-content-center">
                            <div class="card-header pb-1 d-flex justify-content-center">
                                <div class="register-header pb-1">{{ __('m_register.sign up') }}</div>
                            </div>
                            <br/>
                            <div class="card-content">
                                <div class="card-body">

                                    {{ Form::open(['method'=>'post', 'route' => 'member.register.submit', 'id'=>'registration', 'onsubmit' => 'register_btn.disabled = true; return true;']) }}
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formText('name', old('name'), __('m_register.full name'), [], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formText('email', old('email'), __('m_register.email address'), [], false) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formText('id_number', old('id_number'), __('m_register.national id'), [], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formSelect('country_id', countries_dropdown(), old('country_id'), __('m_register.country'), [], false) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formSelect('mobile_prefix', mobile_prefix_dropdowns(), old('mobile_prefix'), __('m_register.mobile register country'), [], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formContactNumber('mobile_number', old('mobile_number'), __('m_register.contact number'), '', [], false) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formDate('birth_date', old('birth_date'), __('m_register.date of birth'), [], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formSelect('gender', gender_dropdown(), old('gender'), __('m_register.gender'), [], false) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formText('referrer_id', old('referrer_id'), __('m_register.referrer id'), [], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                            {{ Form::formText('sponsor_id', old('sponsor_id'), __('m_register.sponsor id'), [], false) }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formPassword('password', old('password'), __('m_register.password'),['autocomplete'=>'new-password'], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formPassword('password_confirmation', old('password_confirmation'), __('m_register.confirm password'),['autocomplete'=>'new-password'], false) }}
                                        </div>
                                    </div>
                                    <div class="register-note">{{ __('m_register.minimum 6 characters, case-sensitive & cannot be same as secondary password')}}</div>
                                    <div class="row">
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formPassword('secondary_password', old('secondary_password'), __('m_register.secondary password'),['autocomplete'=>'new-password'], false) }}
                                        </div>
                                        <div class="col-lg-6 col-xs-12">
                                        {{ Form::formPassword('secondary_password_confirmation', old('secondary_password_confirmation'), __('m_register.confirm secondary password'),['autocomplete'=>'new-password'], false) }}
                                        </div>
                                    </div>
                                    <div class="register-note">{{ __('m_register.minimum 6 characters, case-sensitive & cannot be same as login password') }}</div>
                                    <div class="row">
                                        <div class="col-12">
                                        {{ Form::formText('address', old('address'), __('m_register.address'), [], false) }}
                                        </div>
                                    </div>
                                    <div class="register-note center">{{ __('m_register.all fields are mandatory') }}</div>
                                    <div class="text-center">
                                        <button type="submit" name="register_btn" class="btn btn-primary w-100 position-relative">{{ __('m_register.sign up') }}<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                    </div>
                                    {{ Form::close() }}
                                    <hr>
                                    <div class="text-center login-to-register"><span class="mr-25">{{ __('m_register.already have an account?') }}</span><a href="{{ route('member.login') }}" class="a-text"><span>{{ __('m_register.login') }}</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3" style="padding: 0 !important;">
                        <div class="card-content">
                            <img class="img-fluid" src="{{ asset('images/checkmate.png') }}" alt="branding logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    CreateValidation('form#registration', {
        name: {
            presence: { message: __.validation.field_required },
        },
        id_number: {
            presence: { message: __.validation.field_required },
        },
        mobile_prefix: {
            presence: { message: __.validation.field_required },
        },
        mobile_number: {
            presence: { message: __.validation.field_required },
            format: {
                pattern: '^[0-9]+$',
                message: '{{ __("m_register.field has to be a number") }}'
            }
        },
        password: {
            length: { minimum: 6, message: __.validation.minimum_6_characters },
            presence: { message: __.validation.field_required },
        },
        password_confirmation: {
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "password",
                message: __.validation.password_not_match,
            }
        },
        secondary_password: {
            length: { minimum: 6, message: __.validation.minimum_6_characters },
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "password",
                message: __.validation.not_as_same_as_password,
                comparator: function(v1, v2) {
                    return JSON.stringify(v1) !== JSON.stringify(v2);
                }
            },
        },
        secondary_password_confirmation: {
            presence: { message: __.validation.field_required },
            equality: {
                attribute: "secondary_password",
                message: __.validation.secondary_password_not_match,
            }
        },
        email: {
            presence: { message: __.validation.field_required },
            email: { message: __.validation.email_only },
        },
        country_id: {
            presence: { message: __.validation.field_required },
        },
        address: {
            presence: { message: __.validation.field_required },
        },
        sponsor_id: {
            presence: { message: __.validation.field_required },
        },
    })

    $('.lang-item').on('click', function() {
        var lang = $(this).data('lang')
        window.location.href = '/lang/' + lang
    })

    $('#mobile_prefix').on('change', function() {
        $('div[name="mobile_number"] .prefix').html('+' + $(this).val())
    })
})
</script>
@endpush
