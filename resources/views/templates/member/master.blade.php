@extends('templates.__default.default')
@section('default-title')@yield('title')@endsection
@section('default-body-class')vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static @yield('body-class')@endsection

@push('default-styles')
    <!-- Include styles only for member-->
    @include('templates.__fragments.assets.styles')
    @stack('styles')
@endpush

@section('default-main')
    @include('templates.member.includes.header')
    @include('templates.member.includes.sidebar')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"> 
                @yield('main')
            </div>
        </div>
    </div>
    @include('templates.member.includes.footer') 
@endsection

@push('default-scripts')
    <!-- Include scripts only for member-->
    @include('templates.__fragments.assets.js')
    @stack('scripts')
@endpush