@extends('templates.__default.default')
@section('default-title')@yield('title')@endsection
@section('default-body-class')vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page @yield('body-class')@endsection

@push('default-styles')
    <!-- Include styles only for member-->
    @include('templates.__fragments.assets.styles')
    @stack('styles')
@endpush

@section('default-main')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('main')
            </div>
        </div>
    </div>
@endsection

@push('default-scripts')
    <!-- Include scripts only for member-->
    @include('templates.__fragments.assets.js')
    @stack('scripts')
@endpush