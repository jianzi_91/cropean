<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true"> <!-- Set bgcolor here -->
    <div class="navbar-header expanded">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('member.dashboard') }}">
                <div class="brand-logo"><img class="logo" src="{{ asset('images/logo_sidebar.svg') }}" /></div>
            </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style=""><!-- set bg to transparent -->
            <li class=" nav-item {{ HTML::htmlActiveMenu('dashboard') }}"><a href="{{ route('member.dashboard') }}"><i class="bx bx-layout"></i><span class="menu-title">{{ __('m_nav.dashboard') }}</span></a>
            </li>
            @canany(['member_campaign_list', 'member_campaign_portfolio_list'])
            <li class=" nav-item {{ HTML::htmlActiveMenu('campaign*') }}"><a href="#"><i class="bx bx-data"></i><span class="menu-title">{{ __("m_nav.special trades") }}</span><div class="nav_new">{{ __("m_nav.new")}}</div></a>
                <ul class="menu-content">
                    @can('member_campaign_list')
                    <li class="{{ HTML::htmlActiveMenu('campaign*') }}"><a href="{{ route('member.campaign.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.all campaigns") }}</span></a>
                    </li>
                    @endcan
                    @can('member_campaign_portfolio_list')
                    <li class="{{ HTML::htmlActiveMenu('campaign/portfolio*') }}"><a href="{{ route('member.campaign.portfolio') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.my portfolios") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcanany

            @if(auth()->user()->can('member_deposit_list')
            || (auth()->user()->document_verification_status_id == document_status_approve_id()) && auth()->user()->can('member_withdrawal_list')
            || auth()->user()->can('member_blockchain_deposit_list')
            || auth()->user()->can('member_conversion_list')
            || auth()->user()->can('member_blockchain_withdrawal_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('deposits*', 'open') }} {{ HTML::htmlActiveMenu('blockchain*', 'open') }} {{ HTML::htmlActiveMenu('withdrawals/*', 'open') }}"><a href="#"><i class="bx bx-wallet-alt"></i><span class="menu-title">{{ __("m_nav.funds") }}</span></a>
                <ul class="menu-content">
                    @can('member_deposit_list')
                    <li class="{{ HTML::htmlActiveMenu('deposits*') }}"><a href="{{ route('member.deposits.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.deposits") }}</span></a>
                    </li>
                    @endcan
                    @can('member_blockchain_deposit_list')
                    <li class="{{ HTML::htmlActiveMenu('blockchain/deposits/usdt_erc20') }}"><a href="{{ route('member.blockchain.deposits.usdt_erc20.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.usdt deposit") }}</span></a>
                    </li>
                    <li class="{{ HTML::htmlActiveMenu('blockchain/deposits/usdc') }}"><a href="{{ route('member.blockchain.deposits.usdc.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.usdc deposit") }}</span></a>
                    </li>
                    @endcan
                    @can('member_conversion_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/conversion*') }}"><a href="{{ route('member.credits.conversion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.conversions") }}</span></a>
                    </li>
                    @endcan
                    @if((auth()->user()->document_verification_status_id == document_status_approve_id()) && auth()->user()->can('member_withdrawal_list'))
                    <li class="{{ HTML::htmlActiveMenu('withdrawals*') }}"><a href="{{ route('member.withdrawals.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.withdrawals") }}</span></a>
                    </li>
                    @endif
                    @can('member_blockchain_withdrawal_list')
                    <li class="{{ HTML::htmlActiveMenu('blockchain/withdrawals*') }}"><a href="{{ route('member.blockchain.withdrawals.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.blockchain external transfer") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('member_credit_statement_capital_credit_list') || auth()->user()->can('member_credit_statement_roll_over_credit_list') || auth()->user()->can('member_credit_statement_cash_credit_list'))
            <li class=" nav-item"><a href="#"><i class="bx bx-money"></i><span class="menu-title">{{ __("m_nav.credits") }}</span></a>
                <ul class="menu-content">
                    @can('member_credit_statement_capital_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/capital*') }}"><a href="{{ route('member.credits.statements.capital-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.capital credits") }}</span></a>
                    </li>
                    @endcan
                    @can('member_credit_statement_roll_over_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/rollover*') }}"><a href="{{ route('member.credits.statements.rollover-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.roll-over credits") }}</span></a>
                    </li>
                    @endcan
                    @can('member_credit_statement_cash_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/cash*') }}"><a href="{{ route('member.credits.statements.cash-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.cash credits") }}</span></a>
                    </li>
                    @endcan
                    @can('member_credit_statement_usdt_erc20_credit_list')
                        @can('member_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/usdt*') }}"><a href="{{ route('member.credits.statements.usdt-erc20-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.usdt credits") }}</span></a>
                        @endcan
                    </li>
                    @endcan
                    @can('member_credit_statement_usdc_credit_list')
                        @can('member_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/usdc*') }}"><a href="{{ route('member.credits.statements.usdc-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.usdc credits") }}</span></a>
                        @endcan
                    </li>
                    @endcan
                    @can('member_credit_statement_promotion_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/promotion*') }}"><a href="{{ route('member.credits.statements.promotion-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.promo credits") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('member_commission_roi_equity_list')
            || auth()->user()->can('member_commission_goldmine_bonus_list')
            || auth()->user()->can('member_commission_leader_bonus_list')
            || (auth()->user()->can('member_commission_special_bonus_list') && auth()->user()->view_special_bonus == 1)
            || auth()->user()->can('member_campaign_report_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('reports*', 'open') }} {{ HTML::htmlActiveMenu('roi*', 'open') }} {{ HTML::htmlActiveMenu('goldmine*', 'open') }} {{ HTML::htmlActiveMenu('leader*', 'open') }} {{ HTML::htmlActiveMenu('special*', 'open') }}"><a href="#"><i class="bx bx-file"></i><span class="menu-title">{{ __("m_nav.reports") }}</span></a>
                <ul class="menu-content">
                    @can('member_commission_roi_equity_list')
                        <li class="{{ HTML::htmlActiveMenu('roi*') }}"><a href="{{ route('member.report.roi.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.my equity") }}</span></a>
                        </li>
                    @endcan
                    @can('member_campaign_report_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/campaign*') }}"><a href="{{ route('member.campaign.report.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.campaign") }}</span></a>
                    </li>
                    @endcan
                    @can('member_commission_goldmine_bonus_list')
                        <li class="{{ HTML::htmlActiveMenu('goldmine*') }}"><a href="{{ route('member.report.goldmine.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.goldmine payout") }}</span></a>
                        </li>
                    @endcan
                    @can('member_commission_leader_bonus_list')
                        <li class="{{ HTML::htmlActiveMenu('leader*') }}"><a href="{{ route('member.report.leader.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.leader bonus payout") }}</span></a>
                        </li>
                    @endcan
                    @if(auth()->user()->can('member_commission_special_bonus_list') && auth()->user()->view_special_bonus == 1)
                        <li class="{{ HTML::htmlActiveMenu('special*') }}"><a href="{{ route('member.report.special.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.special bonus payout") }}</span></a>
                        </li>
                    @endif
                    @php
                        $now = now();
                        $promotionEndingDate = Carbon\Carbon::parse('2020-10-01');
                        $promotionStartingDate = Carbon\Carbon::parse('2020-07-01');
                    @endphp
                    @can('member_june_promotion_report_view')
                    @if (auth()->user()->has_downlines && $now->lessThan($promotionEndingDate))
                        @if ($now->greaterThanOrEqualTo($promotionStartingDate))
                        @if (auth()->user()->promotionPayout)
                        <li class="{{ HTML::htmlActiveMenu('promotion') }}"><a href="{{ route('member.report.promotion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.june 2020 promotion") }}</span></a>
                        </li>
                        @endif
                        @else
                        <li class="{{ HTML::htmlActiveMenu('promotion/preview*') }}"><a href="{{ route('member.report.promotion.preview') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.june 2020 promotion preview") }}</span></a>
                        </li>
                        @endif
                    @endif
                    @endcan
                    @can('member_referral_bonus_report_view')
                    <li class="{{ HTML::htmlActiveMenu('referral*') }}"><a href="{{ route('member.report.referral.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.referral bonus") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @can('member_tree_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('sponsor*') }}"><a href="{{ route('member.sponsor.index') }}"><i class="bx bx-globe"></i><span class="menu-title">{{ __('m_nav.network') }}</span></a>
            </li>
            @endcan
            @can('member_announcement_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('announcement*') }}"><a href="{{ route('member.announcements.index') }}"><i class="bx bx-volume-full"></i><span class="menu-title">{{ __('m_nav.announcements') }}</span></a>
            </li>
            @endcan
            @can('member_support_ticket_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('support*') }}"><a href="{{ route('member.support.tickets.index') }}"><i class="bx bx-support"></i><span class="menu-title">{{ __('m_nav.customer service') }}</span></a>
            </li>
            @endcan
        </ul>
    </div>
</div>
