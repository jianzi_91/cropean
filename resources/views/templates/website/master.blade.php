<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="site-url" content="{{ URL::to('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('w_title.company_name') }} - @yield('title')</title>

    @stack('default-styles')
</head>
<body class="@yield('body-class')">
    <noscript>
        <h1 class="no-script">Warning! You need to enable JavaScript for application to work properly.</h1>
    </noscript>
    
    @include('templates.website.includes.header')
      @yield('main')
    @include('templates.website.includes.footer')

    @stack('scripts')
</body>
</html>
