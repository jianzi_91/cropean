<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="site-url" content="{{ URL::to('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/favicon.png') }}" />

    <title>{{ __('s_title.company_name') }} - @yield('default-title')</title>

    <style>
        @font-face {
            font-family: "IBM Plex";
            src: url("/fonts/IBM_Plex_Sans/IBMPlexSans-Regular.ttf");
        }

        @font-face {
            font-family: "IBM Plex Semi Bold";
            src: url("/fonts/IBM_Plex_Sans/IBMPlexSans-SemiBold.ttf");
        }

        @font-face {
            font-family: "IBM Plex Medium";
            src: url("/fonts/IBM_Plex_Sans/IBMPlexSans-Medium.ttf");
        }

        @font-face {
            font-family: "Rubik Medium";
            src: url("/fonts/Rubik/Rubik-Medium.ttf");
        }

        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
            }
            .lds-ripple div {
            position: absolute;
            border: 4px solid #cda674;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
            }
            .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
            }
            @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
        }

        .preloader {
            background-color: white;
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 9999;
            position: absolute;
            top: 0;
            left: 0;
        }
    </style>

    @stack('default-styles')
    <link rel="stylesheet" href="{{ mix('css/default.css') }}" />

</head>
<body class="@yield('default-body-class')" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    
@include('templates.__fragments.components.browser-warning')
<noscript>
    <h1 class="no-script">Warning! You need to enable JavaScript for application to work properly.</h1>
</noscript>

<div class="preloader">
    <div class="lds-ripple"><div></div><div></div></div>
</div>

@yield('default-main')

@include('templates.__translation.locale')
@include('templates.__fragments.assets.constant')

<script src="{{ mix('js/default.js') }}"></script>
@stack('default-scripts')

<script type="text/javascript">
    $(window).on('load', function() {
        $('.preloader').fadeOut('slow')
    })

    $(function() {
         // create notification based on status
         @if ($message = Session::get('success'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'success'
            });
        @endif

        @if ($message = Session::get('reset_success'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'success'
            });
            {{ Session::forget('reset_success') }}
        @endif

        @if ($message = Session::get('reset_error'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'error'
            });
            {{ Session::forget('reset_error') }}
        @endif

        @if ($message = Session::get('error'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'error'
            });
        @endif

        @if ($message = Session::get('warning'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'warning'
            });
        @endif

        @if ($message = Session::get('info'))
            CreateNoty({
                'text': '{{ $message }}',
                'type': 'info'
            });
        @endif

        $('form button[type="submit"]').on('click', function(e) {
            e.preventDefault()
            if ($(this).is(':disabled')) return
            $(this).attr('disabled', true)
            $(this).closest('form').submit()
        })
        
        $('.table-responsive').on('scroll', function() {
            $(this).removeClass('leftShadow')
            $(this).removeClass('rightShadow')
            $(this).removeClass('leftRightShadow')
            
            var end = Math.floor($(this).get(0).scrollWidth - $(this).innerWidth())
            var leftScroll = $(this).scrollLeft()
            if (leftScroll === 0) {
                $(this).addClass('rightShadow')
            } else if (leftScroll === end) {
                $(this).addClass('leftShadow')
            } else {
                $(this).addClass('leftRightShadow')
            }
        })

        $("body").on('click', '.custom-browse-btn', function(e) {
            e.preventDefault();
            var id = $(this).attr('id').replace('button-', '')
            $('#' + id).click();
        })
    })
</script>

</body>
</html>
