<div class="row">
  <div class="col-xs-12 col-sm-6 d-flex align-items-center pb-2">
    @isset($backRoute)<span class="breadcrumb-back mr-1"><a href="{{ $backRoute }}">&lt;</a></span>@endisset
    @isset($header)<span class="breadcrumb-header">{{ $header }}<span>@endisset
  </div>
  <div class="col-xs-12 col-sm-6 d-flex justify-content-end">
    <nav aria-label="breadcrumb" role="navigation" class="hidden-sm-down">
      <ol class="breadcrumb">
          @foreach($breadcrumbs as $breadcrumb)
              @if(isset($breadcrumb['route']))
                <li class="breadcrumb-item text-capitalize">
                  <a href="{{ $breadcrumb['route'] }}">
                      @isset($breadcrumb['icon'])
                        <i class="{{$breadcrumb['icon']}}"></i>
                      @endisset
                      {{ $breadcrumb['name'] }}
                  </a>
                </li>
              @else
                <li class="breadcrumb-item text-capitalize active" aria-current="page">
                  @isset($breadcrumb['icon'])
                    <i class="{{$breadcrumb['icon']}}"></i>
                  @endisset
                  {{ $breadcrumb['name'] }}
                </li>
              @endif
          @endforeach
      </ol>
    </nav>
  </div>
</div>