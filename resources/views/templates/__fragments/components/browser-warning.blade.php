<!-- Older IE warning message -->
<!--[if IE]>
    <div class="ie-warning">
        <h1>Warning!!</h1>
        <p>You are using an outdated version of Internet Explorer, please upgrade to any of the following web browsers to access this website.</p>

        <div class="ie-warning__downloads">
            <a href="http://www.google.com/chrome">
                chrome
            </a>

            <a href="https://www.mozilla.org/en-US/firefox/new">
                firefox
            </a>

            <a href="http://www.opera.com">
                opera
            </a>

            <a href="https://support.apple.com/downloads/safari">
                safari
            </a>

            <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
                ie edge
            </a>

            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                ie11
            </a>
        </div>
        <p>Sorry for the inconvenience!</p>
    </div>
<![endif]-->