<tfoot>
    <tr>
        <td colspan="{{ $span }}">
            <div class="text-center text-capitalize pt-10 pb-10 no-records-found">
                <span>{!! $text !!}</span>
            </div>
        </td>
    </tr>
</tfoot>