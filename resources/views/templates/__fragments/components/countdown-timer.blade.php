<div class="card">
  <div style="border-radius:10px">
    <div class="card-body widget-card-small">
      <div class="widget-label">{{ $label }}</div>
      <div class="d-flex flex-row">
        <div>
          <div class="days widget-value"></div>
          <div class="widget-value small">{{ __('m_countdown.days') }}</div>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div>
          <div class="hours widget-value"></div>
          <div class="widget-value small">{{ __('m_countdown.hours') }}</div>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div>
          <div class="mins widget-value"></div>
          <div class="widget-value small">{{ __('m_countdown.mins') }}</div>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <div>
          <div class="secs widget-value"></div>
          <div class="widget-value small">{{ __('m_countdown.seconds') }}</div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('scripts')
  <script type="text/javascript">
    var eventTime = moment("{{ $datetime }}", 'YYYY-MM-DD HH:mm:ss').format('X'); // replace "DD/MM/YYYY HH:mm:ss" for the exact format being passed in so 'moment' can handle it properly
    var currentTime = moment().format('X'); // current timestamp
    var diffTime = eventTime - currentTime;
    var duration = moment.duration(diffTime*1000, 'milliseconds');
    var interval = 1000;

    setInterval(function(){
      duration = moment.duration(duration - interval, 'milliseconds');
        $('.countdown').text(parseInt(duration.asDays()) + "    " + duration.hours() + "    " + duration.minutes() + "    " + duration.seconds() + "  ")
        $('.days').text(parseInt(duration.asDays()))
        $('.hours').text(duration.hours())
        $('.mins').text(duration.minutes())
        $('.secs').text(duration.seconds())
    }, interval);
  </script>
@endpush