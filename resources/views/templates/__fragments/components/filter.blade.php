<div class="card">
    {{ Form::open(['method'=>'get', 'id'=>'filter']) }}
    <div class="card-content">
        <div class="card-body">
            <h4 class="card-title filter-title pb-1">{{ __('s_filter.filter by') }}</h4>
            <div class="row">
            {{ $slot }}
            </div>
        </div>
    </div>
    <div class="card-footer d-flex justify-content-end" style="padding:0 20px 20px;">
        {{ Form::formButtonPrimary('btn_filter', __('s_filter.apply filter')) }}
        &nbsp;&nbsp;
        {{ Form::formTabSecondary(__('s_filter.reset'), url()->current()) }}
    </div>
    {{ Form::close() }}
</div>