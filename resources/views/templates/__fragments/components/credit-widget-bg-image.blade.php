<div class="card">
  <div class="{{ $backgroundClass }}" style="border-radius:10px">
    <div class="card-body widget-card-small">
      <div class="widget-label">{{ $label }}</div>
      <div class="widget-value">{{ $value }}</div>
    </div>
  </div>
</div>
