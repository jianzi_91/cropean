<div class="card">
  <div class="{{ $backgroundClass }}" style="border-radius:20px">
    <div class="card-body widget-card-small">
      <div class="d-flex flex-column">
        <div class="widget-label">{{ $label }}</div>
        <div class="widget-sublabel">{{ !empty($subLabel1) ? $subLabel1 : '' }}</div>
        <div class="widget-sublabel">{{ !empty($subLabel2) ? $subLable2 : '' }}</div>
      </div>
      <div class="widget-value">{{ $value }}</div>
    </div>
  </div>
</div>