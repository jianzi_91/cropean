<div class="card {{ $cardId }}">
  <div class="card-content">
    <div class="card-body d-flex flex-row widget-card-large">
      <div class="col-8 col-sm-9">
        <div class="card-title pt-1 m-0">{{ $title }}</div>
        <div class="widget-value">{{ $balance ?: 0 }}</div>
        <div class="align-self-end p-0 pt-1">
          {{ Form::formText($id, $address, '', ['readonly'], false) }}
        </div>
      </div>
      <div class="col-4 col-sm-3 d-flex flex-wrap-reverse pb-1 justify-content-xl-end">
        <button id="copy_btn" class="btn btn-primary cro-btn-primary" onclick='copyToClipboard("#{{ $id }}")'>{{ $copyBtn }}</button>         
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate($address)) !!}" style="max-width:125px"/>
      </div>
      <!-- <div class="col-lg-3 col-xs-2 d-flex flex-wrap-reverse pl-0 pb-1">
        <div class="d-flex align-items-end">
          <button id="copy_btn" class="btn btn-primary cro-btn-primary" onclick='copyToClipboard("#{{ $id }}")'>{{ $copyBtn }}</button>         
        </div>
        <div class="d-flex align-items-end justify-content-end p-0 m-0">
          <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate($address)) !!}" style="max-width:125px"/>
        </div>
      </div> -->
    </div>  
  </div>
</div>


@push('scripts')
<script type="text/javascript">
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).val()).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>
@endpush