<div class="table-responsive">
    <table class="table table-borderless mb-0">
        {{ $slot }}
    </table>
</div>

@push('scripts')
<script type="text/javascript">
$(function() {
    function showHideShadows() {
        $('.table-responsive').removeClass('rightShadow')
        $('.table-responsive').removeClass('leftShadow')
        $('.table-responsive').removeClass('leftRightShadow')
        if ($('.table-responsive').get(0).scrollWidth > Math.ceil($('.table-responsive').innerWidth())) {
            $('.table-responsive').addClass('rightShadow')
        }
    }

    $(window).on('resize', function() {
        showHideShadows()
    })

    showHideShadows()
})
</script>
@endpush