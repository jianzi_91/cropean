    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendors.min.js') }}"></script>
    <script src="{{ asset('js/LivIcons/LivIconsEvo.tools.js') }}"></script>
    <script src="{{ asset('js/LivIcons/LivIconsEvo.defaults.js') }}"></script>
    <script src="{{ asset('js/LivIcons/LivIconsEvo.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('js/apexcharts.min.js') }}"></script>
    <script src="{{ asset('js/swiper.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('js/vertical-menu-light.js') }}"></script>
    <script src="{{ asset('js/app-menu.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/components.js') }}"></script>
    <script src="{{ asset('js/footer.js') }}"></script>
    <!-- END: Theme JS-->
    
    <script src="{{ asset('third-party/select2/select2.min.js') }}"></script>
    <script src="{{ asset('third-party/x-editable/bootstrap-editable.min.js') }}"></script>
    <script src="{{ asset('third-party/signaturepad/signaturepad.min.js') }}"></script>