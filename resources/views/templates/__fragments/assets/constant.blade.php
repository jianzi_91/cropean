<script type="text/javascript">
    var __constant = {
        passwordPattern: '^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*)[a-zA-Z0-9]+',
        decimalPoint: 8,
        percentage: '^(100(\.00?)?|[1-9]?\d(\.\d\d?)?)$',
    }
</script>