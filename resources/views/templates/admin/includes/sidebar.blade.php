<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true"> <!-- Set bgcolor here -->
    <div class="navbar-header expanded">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('admin.dashboard') }}">
                <div class="brand-logo"><img class="logo" src="{{ asset('images/logo_sidebar.svg') }}" /></div>
            </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style=""><!-- set bg to transparent -->
            <li class=" nav-item {{ HTML::htmlActiveMenu('dashboard') }}"><a href="{{ route('admin.dashboard') }}"><i class="bx bx-layout"></i><span class="menu-title">{{ __('a_nav.dashboard') }}</span></a>
            </li>
            @if(auth()->user()->can('admin_user_member_management_list') || auth()->user()->can('admin_user_admin_management_list') || auth()->user()->can('admin_kyc_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('members*', 'open') }} {{ HTML::htmlActiveMenu('admins*', 'open') }}"><a href="#"><i class="bx bx-group"></i><span class="menu-title">{{ __("a_nav.user management") }}</span></a>
                <ul class="menu-content">
                    @can('admin_user_member_management_list')
                    <li class="{{ HTML::htmlActiveMenu('members*') }}"><a href="{{ route('admin.management.members.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.member") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_user_admin_management_list')
                    <li class="{{ HTML::htmlActiveMenu('admins*') }}"><a href="{{ route('admin.management.admins.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.admin") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_kyc_list')
                    <li class="{{ HTML::htmlActiveMenu('document*') }}"><a href="{{ route('admin.documents.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.kyc management") }}</span></a>
                    </li>
                    @endcan
                </ul>
                @endif
            </li>
            @can('admin_campaign_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('campaign*') }}"><a href="#"><i class="bx bx-data"></i><span class="menu-title">{{ __("a_nav.special trades") }}</span><div class="nav_new">{{ __("a_nav.new")}}</div></a>
                <ul class="menu-content">
                    <li class="{{ HTML::htmlActiveMenu('campaign*') }}"><a href="{{ route('admin.campaign.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.all campaigns") }}</span></a>
                    </li>
                </ul>
            </li>
            @endcan
            @if(auth()->user()->can('admin_deposit_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('deposits*', 'open') }} {{ HTML::htmlActiveMenu('blockchain*', 'open') }} {{ HTML::htmlActiveMenu('withdrawals*', 'open') }}"><a href="#"><i class="bx bx-wallet-alt"></i><span class="menu-title">{{ __("a_nav.funds") }}</span></a>
                <ul class="menu-content">
                    @can('admin_deposit_list')
                    <li class="{{ HTML::htmlActiveMenu('deposits*') }}"><a href="{{ route('admin.deposits.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.deposits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_blockchain_deposit_list')
                    <li class="{{ HTML::htmlActiveMenu('blockchain/deposits/usdt_erc20') }}"><a href="{{ route('admin.blockchain.deposits.usdt_erc20.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdt deposit") }}</span></a>
                    </li>
                    <li class="{{ HTML::htmlActiveMenu('blockchain/deposits/usdc') }}"><a href="{{ route('admin.blockchain.deposits.usdc.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdc deposit") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_conversion_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/conversion*') }}"><a href="{{ route('admin.credits.conversion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.conversions") }}</span></a>
                    </li>
                    @endcan
                    <li class="{{ HTML::htmlActiveMenu('withdrawals*') }}"><a href="{{ route('admin.withdrawals.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.withdrawals") }}</span></a>
                    </li>
                    @can('admin_blockchain_withdrawal_list')
                    <li class="{{ HTML::htmlActiveMenu('blockchain/withdrawals*') }}"><a href="{{ route('admin.blockchain.withdrawals.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("m_nav.blockchain external transfer") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('admin_credit_system_capital_credit_list') || auth()->user()->can('admin_credit_system_roll_over_credit_list') || auth()->user()->can('admin_credit_system_cash_credit_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('credits/system*', 'open') }}"><a href="#"><i class="bx bx-shape-square"></i><span class="menu-title">{{ __("a_nav.system credits summary") }}</span></a>
                <ul class="menu-content">
                    @can('admin_credit_system_capital_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/system/capital*') }}"><a href="{{ route('admin.credits.system.capital-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.capital credits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_credit_system_roll_over_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/system/rollover*') }}"><a href="{{ route('admin.credits.system.rollover-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.roll-over credits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_credit_system_cash_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/system/cash*') }}"><a href="{{ route('admin.credits.system.cash-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.cash credits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_credit_system_usdt_erc20_credit_list')
                        @can('admin_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/system/usdt*') }}"><a href="{{ route('admin.credits.system.usdt-erc20-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdt credits") }}</span></a></li>
                        @endcan
                    @endcan
                    @can('admin_credit_system_usdc_credit_list')
                        @can('admin_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/system/usdc*') }}"><a href="{{ route('admin.credits.system.usdc-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdc credits") }}</span></a></li>
                        @endcan
                    @endcan
                    @can('admin_credit_system_promotion_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/statement/promotion*') }}"><a href="{{ route('admin.credits.system.promotion-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.promo credits") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('admin_credit_statement_capital_credit_list') || auth()->user()->can('admin_credit_statement_roll_over_credit_list') || auth()->user()->can('admin_credit_statement_cash_credit_list'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('credits/statement*', 'open') }}"><a href="#"><i class="bx bx-money"></i><span class="menu-title">{{ __("a_nav.my credits statement") }}</span></a>
                <ul class="menu-content">
                    @can('admin_credit_statement_usdc_credit_list')
                        @can('admin_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/statement/usdc*') }}"><a href="{{ route('admin.credits.statements.usdc-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdc credits") }}</span></a></li>
                        @endcan
                    @endcan
                    @can('admin_credit_statement_usdt_erc20_credit_list')
                        @can('admin_blockchain_transactions_view')
                        <li class="{{ HTML::htmlActiveMenu('credits/statement/usdt*') }}"><a href="{{ route('admin.credits.statements.usdt-erc20-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.usdt credits") }}</span></a></li>
                        @endcan
                    @endcan
                    @can('admin_credit_statement_capital_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/statement/capital*') }}"><a href="{{ route('admin.credits.statements.capital-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.capital credits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_credit_statement_roll_over_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/statement/rollover*') }}"><a href="{{ route('admin.credits.statements.rollover-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.roll-over credits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_credit_statement_cash_credit_list')
                    <li class="{{ HTML::htmlActiveMenu('credits/statement/cash*') }}"><a href="{{ route('admin.credits.statements.cash-credit.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.cash credits") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('admin_report_balance_statement_list') || auth()->user()->can('admin_commission_roi_equity_list') || auth()->user()->can('admin_commission_goldmine_bonus_list') || auth()->user()->can('admin_commission_special_bonus_list') || auth()->user()->can('admin_campaign_report_list'))
                <li class=" nav-item {{ HTML::htmlActiveMenu('*report*', 'open') }}"><a href="#"><i class="bx bx-file"></i><span class="menu-title">{{ __("a_nav.reports") }}</span></a>
                <ul class="menu-content">
                    @can('admin_report_balance_statement_list')
                        <li class="{{ HTML::htmlActiveMenu('*report/balance*') }}"><a href="{{ route('admin.report.balance.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.member balance") }}</span></a>
                        </li>
                    @endcan
                    @can('admin_commission_roi_equity_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/roi*') }}"><a href="{{ route('admin.report.roi.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.equity") }}</span></a>
                        </li>
                    @endcan
                    @can('admin_campaign_report_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/campaign*') }}"><a href="{{ route('admin.campaign.report.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.campaign") }}</span></a>
                        </li>
                    @endif
                    @can('admin_commission_goldmine_bonus_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/goldmine*') }}"><a href="{{ route('admin.report.goldmine.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.goldmine payout") }}</span></a>
                        </li>
                    @endcan
                    @can('admin_commission_leader_bonus_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/leader*') }}"><a href="{{ route('admin.report.leader.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.leader bonus payout") }}</span></a>
                        </li>
                    @endcan
                    @can('admin_commission_special_bonus_list')
                        <li class="{{ HTML::htmlActiveMenu('*reports/special*') }}"><a href="{{ route('admin.report.special.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.special bonus payout") }}</span></a>
                        </li>
                    @endcan
                    @php
                        $now = now();
                        $promotionEndingDate = Carbon\Carbon::parse('2020-10-01');
                        $promotionStartingDate = Carbon\Carbon::parse('2020-07-01');
                    @endphp
                    @can('admin_june_promotion_report_view')
                    @if ($now->lessThan($promotionEndingDate))
                        @if ($now->greaterThanOrEqualTo($promotionStartingDate))
                        <li class="{{ HTML::htmlActiveMenu('*reports/promotion') }} {{ HTML::htmlActiveMenu('*reports/promotion*breakdown') }}"><a href="{{ route('admin.report.promotion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.june promotion 2020") }}</span></a>
                        </li>
                        @else
                        <li class="{{ HTML::htmlActiveMenu('*reports/promotion/preview*') }}"><a href="{{ route('admin.report.promotion.preview') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.june promotion 2020 preview") }}</span></a>
                        </li>
                        @endif
                    @endif
                    @endcan
                    @can('admin_july_promotion_report_view')
                        <li class="{{ HTML::htmlActiveMenu('*reports/july-promotion') }} {{ HTML::htmlActiveMenu('*reports/july-promotion') }}"><a href="{{ route('admin.report.july-promotion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.july promotion 2020") }}</span></a>
                        </li>
                    @endcan
                    @can('admin_august_promotion_report_view')
                    <li class="{{ HTML::htmlActiveMenu('*reports/august-promotion') }} {{ HTML::htmlActiveMenu('*reports/august-promotion') }}"><a href="{{ route('admin.report.august-promotion.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.august promotion 2020") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_referral_bonus_report_view')
                    <li class="{{ HTML::htmlActiveMenu('*reports/referral*') }}"><a href="{{ route('admin.report.referral.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.referral bonus") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @if(auth()->user()->can('admin_tree_list') || auth()->user()->can('admin_tree_upline_list'))
                <li class=" nav-item {{ HTML::htmlActiveMenu('sponsor*', 'open') }}"><a href="#"><i class="bx bx-globe"></i><span class="menu-title">{{ __("a_nav.network") }}</span></a>
                <ul class="menu-content">
                    @can('admin_tree_list')
                    <li class="{{ HTML::htmlActiveMenu('sponsors') }}"><a href="{{ route('admin.sponsor.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.sponsor tree") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_tree_upline_list')
                    <li class="{{ HTML::htmlActiveMenu('sponsors/uplines') }}"><a href="{{ route('admin.sponsor.upline') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.sponsor tree upline") }}</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endif
            @can('admin_announcement_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('announcement*') }}"><a href="{{ route('admin.announcements.index') }}"><i class="bx bx-volume-full"></i><span class="menu-title">{{ __('a_nav.announcements') }}</span></a>
            </li>
            @endcan
            @can('admin_support_ticket_list')
            <li class=" nav-item {{ HTML::htmlActiveMenu('support*') }}"><a href="{{ route('admin.support.tickets.index') }}"><i class="bx bx-support"></i><span class="menu-title">{{ __('a_nav.customer service') }}</span></a>
            </li>
            @endcan
            @if(auth()->user()->can('admin_deposit_bank_list') || auth()->user()->can('admin_role_list') || auth()->user()->can('admin_translation_list') || auth()->user()->can('admin_roi_tier_setting'))
            <li class=" nav-item {{ HTML::htmlActiveMenu('roi/setting*', 'open') }} {{ HTML::htmlActiveMenu('roles*', 'open') }} {{ HTML::htmlActiveMenu('credits/conversions*', 'open') }} {{ HTML::htmlActiveMenu('deposit-banks*', 'open') }} {{ HTML::htmlActiveMenu('settings*', 'open') }} {{ HTML::htmlActiveMenu('translations*', 'open') }} {{ HTML::htmlActiveMenu('withdrawal-settings*') }}"><a href="#"><i class="bx bx-cog"></i><span class="menu-title">{{ __("a_nav.settings") }}</span></a>
                <ul class="menu-content">
                    @can('admin_deposit_settings_list')
                    <li class="{{ HTML::htmlActiveMenu('deposit-settings*') }}"><a href="{{ route('admin.deposits.settings.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.deposits") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_conversion_setting_view')
                    <li class="{{ HTML::htmlActiveMenu('credits/conversions*') }}"><a href="{{ route('admin.conversions.settings.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.credit conversion") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_deposit_bank_list')
                    <li class="{{ HTML::htmlActiveMenu('deposit-banks*') }}"><a href="{{ route('admin.deposit-banks.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.company banks") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_withdrawal_settings_edit')
                    <li class="{{ HTML::htmlActiveMenu('withdrawal-settings*') }}"><a href="{{ route('admin.withdrawals.withdrawal-settings.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.bank withdrawals") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_role_list')
                    <li class="{{ HTML::htmlActiveMenu('roles*') }}"><a href="{{ route('admin.roles.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.roles") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_translation_list')
                    <li class="{{ HTML::htmlActiveMenu('translations*') }}"><a href="{{ route('admin.translations.index')}} "><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.translations") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_exchange_rate_list')
                    <li class="{{ HTML::htmlActiveMenu('exchange*') }}"><a href="{{ route('admin.exchange.rates.index')}} "><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.exchange rates") }}</span></a>
                    </li>
                    @endcan
                    @can('admin_roi_tier_setting')
                        <li class="{{ HTML::htmlActiveMenu('roi/setting*') }}"><a href="{{ route('admin.roi.setting.edit') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.roi settings") }}</span></a>
                    @endcan
                    @can('admin_blockchain_settings_edit')
                    <li class="{{ HTML::htmlActiveMenu('blockchain-settings*') }}"><a href="{{ route('admin.setting.blockchain-settings.index') }}"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.blockchain settings") }}</span></a>
                    @endcan
                    </li>
                    @if(false)
                    <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.fund settings") }}</span></a>
                        <ul class="menu-content">
                            @can('')
                            <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.deposit") }}</span></a>
                            </li>
                            @endcan
                            @can('')
                            <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item">{{ __("a_nav.withdrawal") }}</span></a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
        </ul>
    </div>
</div>
