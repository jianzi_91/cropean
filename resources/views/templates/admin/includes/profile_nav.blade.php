<div class="card">
    <div class="card-content">
      <div class="user-profile-images">
          <!-- user timeline image -->
          <img src="{{ asset('images/wide_bg.jpg') }}" class="img-fluid rounded-top user-timeline-image" alt="user timeline image" style="width:100%;height:240px;">
          <!-- user profile image -->
          <img src="{{ asset('images/user_icon.png') }}" class="user-profile-image rounded" alt="user profile image" height="140" width="140" style="padding:45px">
      </div>
      <div class="user-profile-text">
          <h4 class="mb-0 text-bold-500 profile-text-color">{{ Auth::user()->name }}</h4>
          {{--<small>Devloper</small>--}}
      </div>
      <!-- user profile nav tabs start -->
      <div class="card-body px-0">
          <ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
              @can('admin_profile_edit')
              <li class="nav-item pb-0">
              @if($page == 'profile')
                {{ Form::formTabPrimary(__('a_profile_navigation.my profile'), route('admin.profile.index')) }}
              @else
                {{ Form::formTabSecondary(__('a_profile_navigation.my profile'), route('admin.profile.index')) }}
              @endif
              </li>
              @endcan
              @if(auth()->user()->can('admin_profile_update_password') || auth()->user()->can('admin_profile_update_secondary_password') || auth()->user()->can('admin_profile_reset_secondary_password'))
              <li class="nav-item pb-0">
              @if($page == 'password')
                {{ Form::formTabPrimary(__('a_profile_navigation.my password'), route('admin.password.edit')) }}
              @else
                {{ Form::formTabSecondary(__('a_profile_navigation.my password'), route('admin.password.edit')) }}
              @endif
              </li>
              @endif
          </ul>
      </div>
      <!-- user profile nav tabs ends -->
  </div>
</div>