<ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
    @can ('admin_user_member_management_edit')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'profile')
                {{ Form::formTabPrimary(__('a_member_management_menu.profile'), route('admin.management.members.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.profile'), route('admin.management.members.edit', $uid)) }}
            @endif
        </li>
    @endcan

    @can('admin_user_member_management_update_password') @can('admin_user_member_management_update_secondary_password')
        <li>
            @if ($page == 'change password')
                {{ Form::formTabPrimary(__('a_member_management_menu.change password'), route('admin.management.members.password.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.change password'), route('admin.management.members.password.edit', $uid)) }}
            @endif
        </li>
    @endcan @endcan

    @can ('admin_user_member_management_show_qr')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'qr code')
                {{ Form::formTabPrimary(__('a_member_management_menu.QR code'), route('admin.management.members.qr', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.QR code'), route('admin.management.members.qr', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_update_tree')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'change network')
                {{ Form::formTabPrimary(__('a_member_management_menu.change network'), route('admin.management.members.network.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.change network'), route('admin.management.members.network.edit', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_adjust_credit')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'adjust credit')
                {{ Form::formTabPrimary(__('a_member_management_menu.adjust credit'), route('admin.management.members.credits.adjustment.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.adjust credit'), route('admin.management.members.credits.adjustment.edit', $uid)) }}
            @endif
        </li>
    @endcan

    {{--@can ('admin_user_member_management_transfer_credit')
    <li class="nav-item pb-0 mb-1">
        @if ($page == 'transfer credit')
        {{ Form::formTabPrimary(__('a_member_management_menu.transfer credit'), route('admin.management.members.credits.transfer.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_member_management_menu.transfer credit'), route('admin.management.members.credits.transfer.edit', $uid)) }}
        @endif
    </li>
    @endcan--}}

    @if (auth()->user()->can('admin_user_member_management_goldmine_rank_edit')
    || auth()->user()->can('admin_user_member_management_goldmine_level_edit'))
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'goldmine rank and level')
                {{ Form::formTabPrimary(__('a_member_management_menu.goldmine rank and level'), route('admin.management.members.goldmine.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.goldmine rank and level'), route('admin.management.members.goldmine.edit', $uid)) }}
            @endif
        </li>
    @endif

    @can ('admin_user_member_management_leader_bonus_edit')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'leader bonus rank')
                {{ Form::formTabPrimary(__('a_member_management_menu.leader bonus rank'), route('admin.management.members.leader.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.leader bonus rank'), route('admin.management.members.leader.edit', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_special_bonus_edit')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'special bonus')
                {{ Form::formTabPrimary(__('a_member_management_menu.special bonus'), route('admin.management.members.special.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.special bonus'), route('admin.management.members.special.edit', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_bank_list')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'bank')
                {{ Form::formTabPrimary(__('a_member_management_menu.bank'), route('admin.member-management.bank.index', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.bank'), route('admin.member-management.bank.index', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_update_permissions')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'permissions')
                {{ Form::formTabPrimary(__('a_member_management_menu.permissions'), route('admin.management.members.permissions.edit', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.permissions'), route('admin.management.members.permissions.edit', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_blockchain_deposit_addresses_list')
    <li class="nav-item pb-0 mb-1">
        @if ($page == 'deposit address')
            {{ Form::formTabPrimary(__('a_member_management_menu.deposit addresses'), route('admin.blockchain.deposit-addresses.member.index', $uid)) }}
        @else
            {{ Form::formTabSecondary(__('a_member_management_menu.deposit addresses'), route('admin.blockchain.deposit-addresses.member.index', $uid)) }}
        @endif
    </li>
    @endcan

    @can ('admin_blockchain_withdrawal_addresses_list')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'wallet addresses')
                {{ Form::formTabPrimary(__('a_member_management_menu.withdrawal wallet addresses'), route('admin.blockchain.withdrawal-addresses.show', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.withdrawal wallet addresses'), route('admin.blockchain.withdrawal-addresses.show', $uid)) }}
            @endif
        </li>
    @endcan

    @can ('admin_user_member_management_login_as')
        <li class="nav-item pb-0 mb-1">
            @if ($page == 'login as')
                {{ Form::formTabPrimary(__('a_member_management_menu.login as'), route('admin.management.members.login_as', $uid)) }}
            @else
                {{ Form::formTabSecondary(__('a_member_management_menu.login as'), route('admin.management.members.login_as', $uid)) }}
            @endif
        </li>
    @endcan
</ul>
