<ul class="nav user-profile-nav justify-content-center justify-content-md-start nav-tabs border-bottom-0 mb-0" role="tablist">
    @can('admin_user_admin_management_edit')
    <li class="nav-item pb-0 mb-2">
        @if ($page == 'profile')
        {{ Form::formTabPrimary(__('a_admin_management_menu.profile'), route('admin.management.admins.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_admin_management_menu.profile'), route('admin.management.admins.edit', $uid)) }}
        @endif
    </li>
    @endcan

    @if(auth()->user()->can('admin_user_admin_management_update_password') || auth()->user()->can('admin user admin management update secondary password'))
    <li class="nav-item pb-0 mb-2">
        @if ($page == 'change password')
        {{ Form::formTabPrimary(__('a_admin_management_menu.change password'), route('admin.management.admins.password.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_admin_management_menu.change password'), route('admin.management.admins.password.edit', $uid)) }}
        @endif
    </li>
    @endif

    @can('admin_user_admin_management_adjust_credit')
    <li class="nav-item pb-0 mb-2">
        @if ($page == 'adjust credit')
        {{ Form::formTabPrimary(__('a_admin_management_menu.adjust credit'), route('admin.management.admins.credits.adjustment.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_admin_management_menu.adjust credit'), route('admin.management.admins.credits.adjustment.edit', $uid)) }}
        @endif
    </li>
    @endcan

    {{--@can ('admin_user_member_management_update_tree')
    <li class="nav-item pb-0 mb-2">
        @if ($page == 'transfer credit')
        {{ Form::formTabPrimary(__('a_admin_management_menu.transfer credit'), route('admin.management.admins.credits.transfer.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_admin_management_menu.transfer credit'), route('admin.management.admins.credits.transfer.edit', $uid)) }}
        @endif
    </li>
    @endcan--}}

    @if(auth()->user()->can('admin_user_admin_management_update_permissions') && (auth()->user()->id != $uid))
    <li class="nav-item pb-0 mb-2">
        @if ($page == 'permissions')
        {{ Form::formTabPrimary(__('a_admin_management_menu.permissions'), route('admin.management.admins.permissions.edit', $uid)) }}
        @else
        {{ Form::formTabSecondary(__('a_admin_management_menu.permissions'), route('admin.management.admins.permissions.edit', $uid)) }}
        @endif
    </li>
    @endif
</ul>
