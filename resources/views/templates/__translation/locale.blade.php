<script>
    var __ = {
        validation: {
          field_required: '{{ __("s_validation.this field is required") }}',
          email_only: '{{ __("s_validation.invalid email format") }}',
          at_least_18: '{{ __("s_validation.you must be at least 18 years old to use this service") }}',
          secondary_password_not_match: '{{ __("s_validation.does not match the secondary password") }}',
          password_not_match: '{{ __("s_validation.does not match the password") }}',
          must_container_alphanumeric_uppercase_lowercase: '{{ __("s_validation.must contain alphaneumeric and at least 1 uppercase and 1 lowercase") }}',
          must_container_symbol_uppercase_lowercase: '{{ __("s_validation.must contain symbol and at least 1 uppercase and 1 lowercase, the symbol allowed are @$!%*#?&") }}',
          must_container_alphanumeric: '{{ __("s_validation.must contain only alphanumeric") }}',
          not_as_same_as_password: '{{ __("s_validation.should not be the same as password") }}',
          only_letters_numbers_period: '{{ __("s_validation.only small letters, numbers and period are allowed, period cannot be the first or last character") }}',
          minimum_5_characters: '{{ __("s_validation.too short, minimum is 5 characters") }}',
          maximum_30_characters: '{{ __("s_validation.too long, maximum is 30 characters") }}',
          greater_than_0: '{{ __("s_validation.must be greater than 0") }}',
          minimum_6_characters: '{{ __("s_validation.too short, minimum is 6 characters") }}',
          minimum_8_characters: '{{ __("s_validation.too short, minimum is 8 characters") }}'
        },
        message: {
          copy_success: '{{ __("s_validation.copied") }}',
          copy_fail: '{{ __("s_validation.copy fail") }}',
          success: '{{ __("s_validation.success") }}',
          error: '{{ __("s_validation.error") }}',
        }
    }
</script>
