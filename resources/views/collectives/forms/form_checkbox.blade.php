<!-- need to add form-group div in parent -->
<div class="custom-control custom-checkbox">
    {{  
        Form::checkbox(
        $name,
        $value,
        (isset($attributes['isChecked']) && $attributes['isChecked']) ? 'checked' : '',
        array_merge(Arr::except($attributes,['required', 'info']), ['class'=>'custom-control-input '.($attributes['class'] ?? ''), 'id' => $id, 'name'=> $name]))
    }}
    <label class="custom-control-label" for="{{$id}}">{{ $label }}</label>
</div>

