@if($isVerticalAligned)
    <div class="form-group">
        @if($label)
            <label class="form-parent-label" for="{{ $attributes['id']??$name }}">{!!  $label.($required ? ' <span class="required">*</span> ' : '')  !!}
                @isset($labelNote)
                    <span class="form-label-note">{{$labelNote}}</span>
                @endisset
            </label>
        @endif
        @isset ($icon)
            <div class="input-group">
                <div class="input-group-prepend">
                    <i class="{{$icon}}"></i>
                </div>
                <div class="input-group-container">
                    @endisset
                    <div class="d-flex flex-row">
                        <div class="custom-file">
                            {{
                                Form::input(
                                'file',
                                $name,
                                $value,
                                array_merge($attributes, ['class'=>'form-control custom-file-input block'.($errors->has(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) ? ' is-invalid ' : ''). ($attributes['class'] ?? ''), 'id' => $attributes['id']??$name]))
                            }}
                            <label class="custom-file-label" for="{{ $attributes['id'] ?? $name }}">{{ $placeholder }}</label>
                            <span class="invalid-feedback">{{ $errors->has(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) ? $errors->first(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) : 'b' }}</span>
                        </div>
                        <button id="button-{{ $attributes['id'] ?? $name }}" type="button" class="custom-browse-btn">{{ __('s_file_upload.find file') }}</button>
                    </div>
                    @isset ($icon)
                </div>
            </div>
        @endisset
        @if (isset($footerNote))
            <div class="form-foot-note w-full">{{ $footerNote }}</div>
        @endif
    </div>
@else
    <div class="form-group">
        <div class="row pb-1">
            <div class="col-xs-12 col-lg-4">
            @if($label)
                <label class="form-parent-label w-1/3" for="{{ $attributes['id']??$name }}">{!!  $label.($required ? ' <span class="required">*</span> ' : '')  !!}
                    @isset($labelNote)
                        <span class="form-header-note">{{$labelNote}}</span>
                    @endisset
                </label>
            @endif
            </div>
            <div class="col-xs-12 col-lg-8">
                @isset ($icon)
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <i class="{{$icon}}"></i>
                        </div>
                        <div class="input-group-container">
                            @endisset
                            <div class="d-flex flex-row">
                                <div class="custom-file">
                                {{
                                    Form::input(
                                    'file',
                                    $name,
                                    $value,
                                    array_merge($attributes, ['class'=>'form-control custom-file-input block'.($errors->has(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) ? ' is-invalid ' : ''). ($attributes['class'] ?? ''), 'id' => $attributes['id']??$name]))
                                }}
                                <label class="custom-file-label" for="{{ $attributes['id'] ?? $name }}">{{ $placeholder }}</label>
                                <span class="invalid-feedback w-full">{{ $errors->has(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) ? $errors->first(attribute_dot($attributes['dotId'] ?? $attributes['id'] ?? $name)) : '' }}</span>
                                </div>
                                <button id="button-{{ $attributes['id'] ?? $name }}" type="button" class="custom-browse-btn">{{ __('s_file_upload.find file') }}</button>
                            </div>
                            @isset ($icon)
                        </div>
                    </div>
                @endisset
                @if (isset($footerNote))
                    <div class="form-foot-note w-full">{{ $footerNote }}</div>
                @endif
            </div>          
        </div>
    </div>
@endif
