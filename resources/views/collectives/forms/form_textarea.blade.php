@push('styles')
    <style>
        /* To fix the textarea layout after being touched */
        .form-control.is-valid, .form-control.is-invalid {
            background-position: center bottom, center calc(100% - 1px) !important;
        }
    </style>
@endpush

<div class="form-group">
    @if($label)
        <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
    @endif
    {{  Form::textarea(
        $name,
        $value,
        array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
    }}
    @isset($attributes['info'])
        <div class="form-text-info">
            {!! $attributes['info'] !!}
        </div>
    @endisset
    <div class="invalid-feedback" role="alert">
        {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
    </div>
</div>