@isset($attributes['disabled'])
<button
  class="btn btn-secondary cro-btn-secondary"
  type="{{ $type }}"
  id="{{ $name }}"
  name="{{ $name }}"
  disabled="true"
>
  {{ $label }}
</button>
@else
<button
  class="btn btn-secondary cro-btn-secondary"
  type="{{ $type }}"
  id="{{ $name }}"
  name="{{ $name }}"
>
  {{ $label }}
</button>
@endisset