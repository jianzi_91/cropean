@if (!$isHorizontal)
<div class="form-group">
    @if($label)
        <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
    @endif
    {{  Form::input(
        'password',
        $name,
        $value,
        array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], Arr::except($attributes,['required', 'info'])))
    }}
    @isset($attributes['info']) {!! $attributes['info'] !!} @endisset
    <div name="{{$name}}" class="invalid-feedback" role="alert">
        {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
    </div>
</div>
@else
<div class="form-group">
    <div class="row pb-1">
        <div class="col-xs-12 col-lg-4">
            @if($label)
                <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
            @endif
        </div>
        <div class="col-xs-12 col-lg-8">
            {{  Form::input(
                'password',
                $name,
                $value,
                array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], Arr::except($attributes,['required', 'info'])))
            }}
            @isset($attributes['info']) {!! $attributes['info'] !!} @endisset
            <div name="{{$name}}" class="invalid-feedback" role="alert">
                {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
            </div>
        </div>
    </div>
</div>
@endif

@push('scripts')
<script>
$(function() {
    $('#{{ $name }}').on('keyup', function() {
      $('.invalid-feedback[name="{{ $name }}"]').hide()
      $('.invalid-feedback[name="{{ $name }}"]').html('')
    })
})
</script>
@endpush()
