<div class="file_upload_thumbnail">
    <div class="file_upload_thumbnail__label">{{ $label }}</div>
    <div class="file_upload_thumbnail__preview">
        @if (!empty($src))
          <div id="icon_{{ $id }}" style="display: none;"><i class="ti-image"></i></div>
          <img id="preview_{{ $id }}" src='{{ $src }}' />
        @else
          <div id="icon_{{ $id }}"><i class="ti-image"></i></div>
          <img id="preview_{{ $id }}" src='' style="display: none;" />
        @endif
    </div>
    @if ($can_upload)
    <input type="file" id="file_{{ $id }}" name="{{ $name }}" class="file_upload_thumbnail__input" />
    <label for="file_{{ $id }}" class="file_upload_thumbnail__upload">{{ __('s_file_upload_thumbnail.upload') }}</label>
    @endif
</div>

@push('default-scripts')
<script type="text/javascript">
$(function() {
  $('#file_{!! $id !!}').on('change', function() {
    var file = $(this)[0].files[0];
    var maxSize = {!! $max_size_in_mb !!};
    if (file) {
      if (maxSize > 0 && maxSize < (file.size / 1000000)) {
        Swal.fire({
	        title: '{{ ucfirst(__("s_file_upload_thumbnail.file too big")) }}',
	        text: '{{ ucfirst(__("s_file_upload_thumbnail.image has to be smaller than")) }}' + ' ' + maxSize + 'MB',
	        showCancelButton: false,
	        confirmButtonText: '{{ __("s_file_upload_thumbnail.close") }}',
	    });
        return;
      }
      $('#preview_{!! $id !!}').attr('src', URL.createObjectURL(file));
      $('#preview_{!! $id !!}').show();
      $('#icon_{!! $id !!}').hide();
    } 
  });

  $('#preview_{!! $id !!}').on('click', function() {
    var src = '{!! $src_big !!}';
    if (!src || src === null || src === '') src = $(this).attr('src');
    Swal.fire({
        title: '',
        imageUrl: src,
        showCancelButton: false,
        confirmButtonText: '{{ __("s_file_upload_thumbnail.close") }}',
    });
  });
});
</script>
@endpush