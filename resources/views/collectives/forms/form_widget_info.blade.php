<div class="row">
  <div class="{{ $label_class }}">
    @if($bold)
    <div class="widget-info-label"> {{ $label }} </div>
    @else
    <div class="widget-info-label no-bold"> {{ $label }} </div>
    @endif
  </div>
  <div class="{{ $value_class }}">
    @if(!$dashboard)
    @if($bold)
    <div class="widget-info-value" id="{{ $id }}"> {{ $value }} </div>
    @else
    <div class="widget-info-value no-bold" id="{{ $id }}"> {{ $value }} </div>
    @endif
    @else
    <div class="widget-info-value dashboard" id="{{ $id }}"> {{ $value }} </div>
    @endif
  </div>
</div>