@if (!$isHorizontal)
<div class="form-group">
    @if($label)
        <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
    @endif
    {{  Form::input(
        'number',
        $name,
        $value,
        array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name, 'min' => $min], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
    }}
    @isset($attributes['info'])
        <div class="form-text-info">
            {!! $attributes['info'] !!}
        </div>
    @endisset
    <div class="invalid-feedback" role="alert">
        {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
    </div>
</div>
@else
<div class="form-group">
    <div class="row pb-1">
        <div class="col-xs-12 col-lg-4">
            @if($label)
                <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
            @endif
        </div>
        <div class="col-xs-12 col-lg-8">
            {{  Form::input(
                'number',
                $name,
                $value,
                array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name, 'min' => $min], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
            }}
            @isset($attributes['info'])
                <div class="form-text-info">
                    {!! $attributes['info'] !!}
                </div>
            @endisset
            <div class="invalid-feedback" role="alert">
                {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
            </div>
        </div>
    </div>
</div>
@endif