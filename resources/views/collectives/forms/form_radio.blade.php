<!-- need to add form-group div in parent -->
<div class="custom-control custom-radio">
    {{  Form::radio(
        $name,
        $value,
        $checked,
        array_merge(['class'=>'custom-control-input', 'id' => $id, 'name'=> $name], $attributes))
    }}
    <label class="custom-control-label" for="{{$id}}">{{ $label }}</label>
</div>

