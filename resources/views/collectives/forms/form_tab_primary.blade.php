@if($new_tab === true)
<a class="nav-link d-flex px-1 active tab-primary" target="_blank" id="{{ $name }}" href="{{ $url }}"><span class="d-block">{{ $name }}</span></a>
@else
<a class="nav-link d-flex px-1 active tab-primary" id="{{ $name }}" href="{{ $url }}"><span class="d-block">{{ $name }}</span></a>
@endif