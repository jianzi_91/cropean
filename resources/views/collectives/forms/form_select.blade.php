@if (!$isHorizontal)
<div class="form-group">
    @if($label) <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset </label> @endif
    {{  Form::select(
        $name,
        $list,
        $value,
        array_merge(['class'=>'custom-select '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
    }}
    @isset($attributes['info'])
        <div class="form-text-info">
            {!! $attributes['info'] !!}
        </div> 
    @endisset
    <div name="{{ $name }}" class="invalid-feedback" role="alert">
        {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
    </div>
</div>
@else
<div class="form-group">
    <div class="row pb-1">
        <div class="col-xs-12 col-lg-4">
            @if($label)
                <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
            @endif
        </div>
        <div class="col-xs-12 col-lg-8">
            {{  Form::select(
                $name,
                $list,
                $value,
                array_merge(['class'=>'custom-select '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
            }}
            @isset($attributes['info'])
                <div class="form-text-info">
                    {!! $attributes['info'] !!}
                </div>
            @endisset
            <div name="{{$name}}" class="invalid-feedback" role="alert">
                {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
            </div>
        </div>
    </div>
</div>
@endif

@push('scripts')
<script type="text/javascript">
$(window).on('load', function() {
    $('#{{ $name }}').select2()
})

$(function() {
    @if (isset($value) && !empty($value))
    setTimeout(function() { 
        $('#{{ $name }}').val('{{ $value }}').trigger('change')
    }, 100)
    @endif

    @if($errors->has(attribute_dot($name)))
    $('[aria-labelledby="select2-{{ $name }}-container"]').addClass('is-invalid')
    @else
    $('[aria-labelledby="select2-{{ $name }}-container"]').removeClass('is-invalid')
    @endif

    $('#{{ $name }}').on('change', function() {
      $('.invalid-feedback[name="{{ $name }}"]').hide()
      $('.invalid-feedback[name="{{ $name }}"]').html('')
      $('[aria-labelledby="select2-{{ $name }}-container"]').removeClass('is-invalid')
    })
})
</script>
@endpush()