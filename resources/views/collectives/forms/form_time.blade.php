@if (!$isHorizontal)
<div class="form-group">
    @if($label)
        <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
    @endif
    {{  
        Form::input(
        'text',
        $name,
        $value,
        array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
    }}
    @isset($attributes['info']) {!! $attributes['info'] !!} @endisset
    <div class="invalid-feedback" role="alert">
        {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
    </div>
</div>
@else
<div class="form-group">
    <div class="row pb-1">
        <div class="col-xs-12 col-lg-4">
            @if($label)
                <label for="{{ $name }}">{!! $label !!} @isset($attributes['required']) <span class="required">*</span> @endisset</label>
            @endif
        </div>
        <div class="col-xs-12 col-lg-8">
            {{  
                Form::input(
                'text',
                $name,
                $value,
                array_merge(['class'=>'form-control '.($errors->has(attribute_dot($name)) ? 'is-invalid' : ''), 'id' => $name], \Illuminate\Support\Arr::except($attributes,['required', 'info'])))
            }}
            @isset($attributes['info']) {!! $attributes['info'] !!} @endisset
            <div class="invalid-feedback" role="alert">
                {{ ($errors->has(attribute_dot($name)) ? $errors->first(attribute_dot($name)) : '') }}
            </div>
        </div>
    </div>
</div>
@endif

@push('default-scripts')
    <script type="text/javascript">
        CreateDatetime({  
            element: '#{{ $name }}',
            enableTime: true,
            noCalendar: true,
            dateFormat: "Hi",
            time_24hr: true,
        });
    </script>
@endpush