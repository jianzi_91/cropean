@if($isVerticalAligned)
<div class="form-group w-full">
    @if($label)
    <label class="form-parent-label">{!! $label !!}
        @isset($labelNote)
            <span class="form-label-note">{{$labelNote}}</span>
        @endisset
    </label>
    @endif
    <ul class="form-language">
        <li class="form-language-btn">
            <a href="#" title="{{$current_language}}">{{ $current_language}}</a>
            <nav class="form-language-menu">
                <ul>
                    @isset($languages)
                        @foreach ($languages as $locale=>$name)
                            @if ($current_language != $locale)
                                <li>
                                    <a href="{{ url('/lang/'. $locale) }}" title="{{ $name }}">{{ $name }}
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="#" title="{{ $name }}" class="active">{{ $name }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @endisset
                </ul>
            </nav>
        </li>
    </ul>
    @if (isset($footerNote))
        <label class="form-foot-note">{{ $footerNote }}</label>
    @endif
</div>
@else
<div class="form-group form-group-inline flex flex-wrap">
    <div class="w-1/3">
    @if($label)
        <label class="form-parent-label">
            {!! $label !!}
        @isset($labelNote)
            <span class="form-label-note">{{$labelNote}}</span>
        @endisset
        </label>
    @endif
    </div>
    <div class="w-2/3">
        <ul class="form-language">
            <li class="form-language-btn">
                <a href="#" title="{{$current_language}}">{{ $current_language}}</a>
                <nav class="form-language-menu">
                    <ul>
                        @isset($languages)
                            @foreach ($languages as $locale=>$name)
                                @if ($current_language != $locale)
                                    <li>
                                        <a href="{{ url('/lang/'. $locale) }}" title="{{ $name }}">{{ $name }}
                                        </a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#" title="{{ $name }}" class="active">{{ $name }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        @endisset
                    </ul>
                </nav>
            </li>
        </ul>
        @if (isset($footerNote))
            <label class="form-foot-note w-full block">{{ $footerNote }}</label>
        @endif
    </div>
</div>
@endif

{{-- 
{{HTML::setLanguage('setLanguage', ['cn'=>'cn'], app()->getLocale(), [], false, 'setLanguage labelNote', 'setLanguage footerNote','fa fa-lock')}}
--}}