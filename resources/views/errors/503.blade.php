@extends('templates.member.wide')
@section('title', __('m_error.503'))

@push('styles')
<style>
.wrapper {
    display: flex;
    width: 100%;
    height: 100vh;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}

.code {
    font-size: 25rem;
    line-height: 30rem;
    color: #cda674;
    font-family: "Rubik Medium";
}

.text {
    font-size: 2.5rem;
    color: #58595B;
    font-family: "Rubik Medium";
    margin-bottom: 20px;
}

@media only screen and (max-width:900px) {
    .code {
        font-size: 20rem;
        line-height: 35rem;
    }

    .text {
        font-size: 2rem;
    }
}

@media only screen and (max-width:670px) {
    .code {
        font-size: 15rem;
        line-height: 20rem;
    }

    .text {
        font-size: 1.5rem;
    }
}

@media only screen and (max-width:500px) {
    .code {
        font-size: 10rem;
        line-height: 15rem;
    }

    .text {
        font-size: 1rem;
    }
}

@media only screen and (max-width:400px) {
    .code {
        font-size: 8rem;
        line-height: 10rem;
    }

    .text {
        font-size: 0.8rem;
    }
}
</style>
@endpush()

@section('main')
  <div class="wrapper">
    <div class="code">503</div>
    <div class="text">System is inaccessible at the moment due to scheduled maintenance. We will resume operations shortly.</div>
    <div class="text">服务器正在进行定期维护中。 系统很快就会恢复运作。</div>
  </div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#btn_back').on('click', function() {
        window.location.href = '/'
    })
})
</script>
@endpush