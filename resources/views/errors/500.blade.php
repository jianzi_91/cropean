@extends('templates.member.wide')
@section('title', __('m_error.500'))

@push('styles')
<style>
.wrapper {
    display: flex;
    width: 100%;
    height: 100vh;
    justify-content: center;
    align-items: center;
    flex-direction: column;
}

.code {
    font-size: 25rem;
    line-height: 30rem;
    color: #cda674;
    font-family: "Rubik Medium";
}

.text {
    font-size: 2.5rem;
    color: #58595B;
    font-family: "Rubik Medium";
    margin-bottom: 20px;
}

@media only screen and (max-width:900px) {
    .code {
        font-size: 20rem;
        line-height: 35rem;
    }

    .text {
        font-size: 2rem;
    }
}

@media only screen and (max-width:670px) {
    .code {
        font-size: 15rem;
        line-height: 20rem;
    }

    .text {
        font-size: 1.5rem;
    }
}

@media only screen and (max-width:500px) {
    .code {
        font-size: 10rem;
        line-height: 15rem;
    }

    .text {
        font-size: 1rem;
    }
}

@media only screen and (max-width:400px) {
    .code {
        font-size: 8rem;
        line-height: 10rem;
    }

    .text {
        font-size: 0.8rem;
    }
}
</style>
@endpush()

@section('main')
  <div class="wrapper">
    <div class="code">500</div>
    <div class="text">Something went wrong</div>
    <div class="text">很抱歉,网站似乎出现问题了. 请您与管理员联系.</div>
    <div>{{ Form::formButtonPrimary('btn_back', 'Back to home', 'button') }}</div>
  </div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#btn_back').on('click', function() {
        window.location.href = '/'
    })
})
</script>
@endpush