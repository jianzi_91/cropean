@extends('templates.__default.default')
@section('default-title') ui-docs @endsection
@section('default-body-class') ui--docs body-green @endsection

@push('default-styles')
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- FontAwesome JS -->
  <script defer src="https://use.fontawesome.com/releases/v5.8.2/js/all.js" integrity="sha384-DJ25uNYET2XCl5ZF++U8eNxPWqcKohUUBUpKGlNLMchM7q4Wjg2CUpjHLaL8yYPH" crossorigin="anonymous"></script>
  <!-- Plugins CSS -->    
  <link rel="stylesheet" href="{{ asset('ui-docs/plugins/prism/prism.css') }}">
  <link rel="stylesheet" href="{{ asset('ui-docs/plugins/elegant_font/css/style.css') }}">  
  <!-- Theme CSS -->
  <link id="theme-style" rel="stylesheet" href="{{ asset('ui-docs/css/styles.css') }}">
@endpush

@section('default-main')
<div class="page-wrapper">
  @include('ui-docs.includes.header')
    <div class="doc-wrapper">
      <div class="container">
        @include('ui-docs.includes.quick-update')
        <div class="doc-body row">
        <div class="doc-content col-md-9 col-12 order-1">
            <div class="content-inner">
              @include('ui-docs.sections.installation')
              @include('ui-docs.sections.form')          
              @include('ui-docs.sections.date_time')       
              @include('ui-docs.sections.notifications')       
              @include('ui-docs.sections.confirmation')       
              @include('ui-docs.sections.tables')        
              @include('ui-docs.sections.clipboard')      
              @include('ui-docs.sections.validations')      
              @include('ui-docs.sections.extras')      
              </div><!--//content-inner-->
          </div><!--//doc-content-->
          @include('ui-docs.includes.sidebar')
        </div>
      </div>
    </div>
  </div>
  @include('ui-docs.includes.footer')
@endsection

@push('default-scripts')
  <!-- Main Javascript -->   
  <script type="text/javascript" src="{{ asset('ui-docs/plugins/prism/prism.js') }}"></script>    
  <script type="text/javascript" src="{{ asset('ui-docs/plugins/jquery-scrollTo/jquery.scrollTo.min.js') }}"></script>      
  <script type="text/javascript" src="{{ asset('ui-docs/plugins/stickyfill/dist/stickyfill.min.js') }}"></script>                                                             
  <script type="text/javascript" src="{{ asset('ui-docs/js/main.js') }}"></script>
@endpush