<section id="notifications-section" class="doc-section">
    <h2 class="section-title">Notifications</h2>
    <br />
    <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
  CreateNoty(&lbrace;&quot;text&quot;: &quot;Success Message&quot;, &quot;type&quot;: &quot;success&quot; &rbrace;);
  CreateNoty(&lbrace;&quot;text&quot;: &quot;Error Message&quot;, &quot;type&quot;: &quot;error&quot; &rbrace;);
  CreateNoty(&lbrace;&quot;text&quot;: &quot;Warning Message&quot;, &quot;type&quot;: &quot;warning&quot; &rbrace;);
  CreateNoty(&lbrace;&quot;text&quot;: &quot;Info Message&quot;, &quot;type&quot;: &quot;info&quot; &rbrace;);
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">
      <button class="btn btn-success ns-js" data-type="success">Success</button>
      <button class="btn btn-danger ns-js" data-type="error">Error</button>
      <button class="btn btn-warning ns-js" data-type="warning">Warning</button>
      <button class="btn btn-info ns-js" data-type="info">Info</button>
    </div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <p>Notifications is options is <a target="_blank" href="https://ned.im/noty/#/">available here</a>.</p>
  </section><!--//doc-section-->

  @push('default-scripts')
    <script type="text/javascript">
      $(function() {
        $('.ns-js').click(function(){
          var nsjs = $(this).data('type');
          CreateNoty({ 'text': nsjs+' message', 'type': nsjs});
        })
      })
    </script>
@endpush