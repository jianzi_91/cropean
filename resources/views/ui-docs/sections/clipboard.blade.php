<section id="clipboard-section" class="doc-section">
    <h2 class="section-title">Clipboard</h2>
    <p>Copy clipboard options <a target="_blank" href="https://clipboardjs.com/">available here</a>.</p>
</section><!--//doc-section-->