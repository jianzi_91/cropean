<section id="form-section" class="doc-section">
  <h2 class="section-title">Forms</h2>
  <div class="section-block">
    <a href="https://laravelcollective.com/" target="_blank">Laravel Html Collective</a> is used as the component wrapper here.</<a> 
  </div><!--//section-block-->
  @include('ui-docs.sections.forms.text')
  @include('ui-docs.sections.forms.number')
  @include('ui-docs.sections.forms.password')
  @include('ui-docs.sections.forms.select')
  @include('ui-docs.sections.forms.checkbox')
  @include('ui-docs.sections.forms.radio')
  @include('ui-docs.sections.forms.textarea')
  @include('ui-docs.sections.forms.upload')
</section><!--//doc-section-->