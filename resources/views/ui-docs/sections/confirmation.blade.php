<section id="confirmation-section" class="doc-section">
    <h2 class="section-title">Confirmation</h2>
    <br />
    <p>Confirmation popup feature is being supported by swal package.</p>
      <!-- Sample -->
      <h6>Code Example</h6>
  <pre>
<code class="language-javascript">
  swal.fire(&lbrace;
        title: &quot;Title&quot;,
        text: &quot;body content lorem ipsum dela fuenta espanol&quot;,
        showCancelButton: true,
        cancelButtonText: &quot;Cancel&quot;,
        confirmButtonText: &quot;Confirm&quot;,
        preConfirm: function(e)&lbrace;
            $(&quot;#submit&quot;).submit();
        &rbrace;
    &rbrace;)
  </code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">
      <button class="btn btn-success swalfire-js">Fire</button>
    </div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <p>Confirmation options <a target="_blank" href="https://sweetalert2.github.io/">available here</a>.</p>
</section><!--//doc-section-->

@push('default-scripts')
    <script type="text/javascript">
      $(function() {
        $('.swalfire-js').click(function(){
          swal.fire({
              title: 'Title',
              text: 'body content lorem ipsum dela fuenta espanol',
              showCancelButton: true,
              cancelButtonText: 'Cancel',
              confirmButtonText: 'Confirm',
              preConfirm: function(e){
                  // Submit: $('#' + deleteform).submit();
              }
          })
        })
      })
    </script>
@endpush

