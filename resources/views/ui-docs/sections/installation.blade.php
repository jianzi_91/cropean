<section id="installation-section" class="doc-section">
  <h2 class="section-title">Installation</h2>
  <div id="packages"  class="section-block">
    <h3 class="block-title">Packages</h3>
    <p>Please run the script in your terminal to install all the dependencies.</p>
    <div class="code-block">
        <h6>Script:</h6>
        <p><code>yarn install</code> or <code>npm install</code></p>
    </div><!--//code-block-->
  </div><!--//section-block-->
  
  <div id="development"  class="section-block">
    <h3 class="block-title">Running assets in Development</h3>
    <div class="callout-block callout-success">
      <div class="icon-holder">
          <i class="fas fa-thumbs-up"></i>
      </div><!--//icon-holder-->
      <div class="content">
          <h4 class="callout-title">Important:</h4>
          <p>We have ignore ./public/css and ./public/js folders, so all css and js should be define in resources/js*, resources/sass*</p>
      </div><!--//content-->
  </div>
    <p>In development, Please run the script below:</p>
    <p><code>yarn watch</code> or <code>yarn dev</code></p>             
  </div><!--//section-block-->
  
  <div id="production"  class="section-block">
    <h3 class="block-title">Running assets in Production</h3>
    <p>In production, Please run the script below:</p>
    <p><code>yarn production</code></p>             
  </div><!--//section-block-->
</section><!--//doc-section-->