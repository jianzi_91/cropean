<section id="validations-section" class="doc-section">
    <h2 class="section-title">Form Validations</h2>
    <br />
    <p>Enter value in each field to see frontend validation. When form submitted there is backend validation level. Please take note that backend and frontend validation response should be the same message.</p>

    <h6>Code Example</h6>
  <pre>
<code class="language-javascript">
CreateValidation('element', &lbrace;
  username:  &lbrace;
    presence:  &lbrace; message: __.validation.field_required &rbrace;,
  &rbrace;,
  ...
&rbrace;)
  </code>
  </pre>

      <div class="section-block">
          <h6>Demo</h6>
          <div class="card">
            <div class="card-body">
              {{ Form::open(['method'=>'post', 'id'=>'form-validation-id', 'class'=>'form-horizontal']) }}
                  {{ Form::formText('username', old('username'), 'Username') }}
                  {{ Form::formText('email', old('email'), 'Email Address') }}
                  {{ Form::formPassword('password', '', 'Password') }}
                  {{ Form::formPassword('password_confirmation', '', 'Confirm Password') }}
                  {{ Form::formPassword('secondary_password', '', 'Secondary Password') }}
                  {{ Form::formPassword('secondary_password_confirmation', '', 'Confirm Secondary Password') }}
                  <button type="button" class="btn btn-primary prevent-spamming">Submit</button>
              {{ Form::close() }}
            </div>
          </div>

    </div><!--//section-block-->
    <p>Validation options <a target="_blank" href="https://validatejs.org/">available here</a>.</p>
</section><!--//doc-section-->

@push('default-scripts')
    <script type="text/javascript">
        CreateValidation('form#form-validation-id', {
            username: {
                presence: { message: __.validation.field_required },
            },
            email: {
                presence: { message: __.validation.field_required },
                email: { message: __.validation.email_only },
            },
            password: {
                length: { minimum: 8, message: __.minimum_8_characters },
                presence: {message: __.validation.field_required},
                format: {
                    pattern: __constant.passwordPattern,
                    message: __.validation.only_alphanumeric
                }
            },
            password_confirmation: {
                presence: { message: __.validation.field_required },
                equality: {
                    attribute: "password",
                    message: __.validation.password_not_match,
                }
            },
            secondary_password: {
                length: { minimum: 8, message: __.minimum_8_characters },
                presence: {message: __.validation.field_required},
                equality: {
                    attribute: "password",
                    message: __.validation.not_as_same_as_password,
                    comparator: function(v1, v2) {
                        return JSON.stringify(v1) !== JSON.stringify(v2);
                    }
                },
                format: {
                    pattern: __constant.passwordPattern,
                    message: __.validation.only_alphanumeric
                },
            },
            secondary_password_confirmation: {
                presence: { message: __.validation.field_required },
                equality: {
                    attribute: "secondary_password",
                    message: __.validation.secondary_password_not_match,
                }
            }
        });
    </script>
@endpush