<div id="extras_select2" class="section-block">
<h3 class="block-title">Select2 (Bootstrap)</h3>
<br />

  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-javascript">
// include package
&lt;link type=&quot;text/css&quot; rel=&quot;stylesheet&quot; href=&quot;&lbrace;&lbrace; asset(&apos;third-party/select2/select2.min.css&apos;) &rbrace;&rbrace;&quot; /&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lbrace;&lbrace; asset(&apos;third-party/select2/select2.min.js&apos;) &rbrace;&rbrace;&quot;&gt;&lt;/script&gt;

// javascript
$(&apos;select&apos;).select2();
</code>
  </pre>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <p>Select2 options <a target="_blank" href="https://select2.org/">available here</a>.</p>
</div><!--//section-block-->