<div id="extras_editable" class="section-block">
<h3 class="block-title">X-Editable (Bootstrap)</h3>
<br />

  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-javascript">
// include package
&lt;link type=&quot;text/css&quot; rel=&quot;stylesheet&quot; href=&quot;&lbrace;&lbrace; asset(&apos;third-party/x-editable/bootstrap-editable.css&apos;) &rbrace;&rbrace;&quot; /&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;&lbrace;&lbrace; asset(&apos;third-party/x-editable/bootstrap-editable.min.js&apos;) &rbrace;&rbrace;&quot;&gt;&lt;/script&gt;

//html
&lt;a href=&quot;#&quot; 
  data-name=&quot;&lbrace;&lbrace; $language->id &rbrace;&rbrace;&quot;
  data-pk=&quot;&lbrace;&lbrace; $localeTranslations[$language->id][$translation->translator_page_id][$translation->key]->id &rbrace;&rbrace;&quot;
  data-url=&quot;/translations/&lbrace;&lbrace; $localeTranslations[$language->id][$translation->translator_page_id][$translation->key]->id &rbrace;&rbrace;&quot;>...&lt;/a&gt;

// javascript
// set to inline
// $.fn.editable.defaults.mode = &apos;inline&apos;;

// editable button
$.fn.editableform.buttons = &apos;&lt;button type=&quot;submit&quot;&gt;&apos; +
    &apos;&lbrace;&lbrace; __(&apos;a_translations.submit&apos;) &rbrace;&rbrace;&apos; +
    &apos;&lt;/button&gt;&apos; +
    &apos;&lt;button type=&quot;button&quot; class=&quot; btn-light editable-cancel &quot;&gt;&apos; +
    &apos;&lbrace;&lbrace; __(&apos;a_translations.cancel&apos;) &rbrace;&rbrace;&apos; +
    &apos;&lt;/button&gt;&apos;;

// submit edit
$(&apos;.editable a&apos;).editable(&lbrace;
    type: "textarea",
    title: "&lbrace;&lbrace; __(&apos;a_translations.enter translation&apos;) &rbrace;&rbrace;",
    ajaxOptions: &lbrace;
        type: &apos;put&apos;,
        headers: &lbrace;
            &apos;X-CSRF-TOKEN&apos;: &apos;&lbrace;&lbrace; csrf_token() &rbrace;&rbrace;&apos;
        &rbrace;,
    &rbrace;,
    success: function (response, newValue) &lbrace;
        CreateNoty(&lbrace;
            &apos;text&apos;: response.message,
            &apos;type&apos;: &apos;success&apos;
        &rbrace;);
    &rbrace;,
    error: function (response, newValue) &lbrace;
        CreateNoty(&lbrace;
            &apos;text&apos;: &apos;&lbrace;&lbrace; __(&apos;a_translations.fail to update translation&apos;) &rbrace;&apos;,
            &apos;type&apos;: &apos;error&apos;
        &rbrace;);
    &rbrace;
&rbrace;);

</code>
  </pre>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <p>X-Editable options <a target="_blank" href="https://vitalets.github.io/x-editable/docs.html">available here</a>.</p>
</div><!--//section-block-->