<div id="form_upload" class="section-block">
  <h3 class="block-title">Upload File</h3>
  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formFileUploadThumbnail(&quot;name&quot;, &quot;id&quot;, &quot;value_image_url&quot;, &quot;Label&quot;, &quot;max_size_in_mb&quot;, &quot;src_big&quot;, &quot;can_upload&quot;) &rbrace;&rbrace; 
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">
      {{ Form::formFileUploadThumbnail("filename", "upload_id", null, "label", 25)}}
    </div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <table class="table table-bordered">
    <tr>
      <th>Variables</th>
      <th width="40%">Values</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>name</td>
      <td>"Name"</td>
      <td>Name of your form input</td>
    </tr>
    <tr>
      <td>id</td>
      <td>"file_id"</td>
      <td>Unique ID for your field</td>
    </tr>
    <tr>
      <td>value_image_url</td>
      <td>"asset('/test/test')"</td>
      <td>Image url</td>
    </tr>
    <tr>
      <td>label</td>
      <td>"Label"</td>
      <td>Label of your form input</td>
    </tr>

    <tr>
      <td>max_size_in_mb</td>
      <td>50</td>
      <td>Max size in MB</td>
    </tr>

    <tr>
      <td>src_big</td>
      <td>"src_big"</td>
      <td>Source big</td>
    </tr>

    <tr>
      <td>can_upload</td>
      <td>true/false</td>
      <td>User can upload</td>
    </tr>
  </table>
</div><!--//section-block-->