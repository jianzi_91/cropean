<div id="form_password" class="section-block">
<h3 class="block-title">Form Password</h3>
  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formPassword(&quot;name&quot;, &quot;****&quot;, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">{{ Form::formPassword("name", "password", "Label", []) }}</div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <p>Number is the same options as <a class="scrollto" href="#form_text">form text</a>.</p>
</div><!--//section-block-->