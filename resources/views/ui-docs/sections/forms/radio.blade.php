<div id="form_radio" class="section-block">
<h3 class="block-title">Form Radio</h3>

<div class="callout-block callout-info">
  <div class="icon-holder">
      <svg class="svg-inline--fa fa-info-circle fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="info-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 110c23.196 0 42 18.804 42 42s-18.804 42-42 42-42-18.804-42-42 18.804-42 42-42zm56 254c0 6.627-5.373 12-12 12h-88c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h12v-64h-12c-6.627 0-12-5.373-12-12v-24c0-6.627 5.373-12 12-12h64c6.627 0 12 5.373 12 12v100h12c6.627 0 12 5.373 12 12v24z"></path></svg><!-- <i class="fas fa-info-circle"></i> -->
  </div><!--//icon-holder-->
  <div class="content">
      <h4 class="callout-title">Alert!</h4>
      <p>Make sure radio button is being wrapped by <code>&lt;div class="form-group"&gt;</code> and you have to set the validation manually.</p>
  </div><!--//content-->
</div>
  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formRadio(&quot;name&quot;, &quot;id_1&quot;, &quot;value&quot;, &quot;is_set&quot;, &quot;label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
&lbrace;&lbrace; Form::formRadio(&quot;name&quot;, &quot;id_2&quot;, &quot;value&quot;, &quot;is_set&quot;, &quot;label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
                            
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">
        {{ Form::formRadio('name', 'radio_id_1', 'value_one', '', "Label One", []) }} 
        {{ Form::formRadio('name', 'radio_id_2', 'value_two', '', "Label Two", []) }}
    </div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <table class="table table-bordered">
    <tr>
      <th>Variables</th>
      <th width="40%">Values</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>name</td>
      <td>"Name"</td>
      <td>Name of your form input</td>
    </tr>
    <tr>
      <td>id</td>
      <td>"uniq_id"</td>
      <td>Required! should set the unique id for each radio</td>
    </tr>
    <tr>
      <td>value</td>
      <td>"radio_value"</td>
      <td>Value of the radio</td>
    </tr>
    <tr>
      <td>is_set</td>
      <td>true/false</td>
      <td>Set radio if either true/false</td>
    </tr>
    <tr>
      <td>attributes</td>
      <td>"disabled"=>"true/false"
        <br />...
      </td>
      <td>All attributes, if no attributes, just leave it empty array []</td>
    </tr>
  </table>
</div><!--//section-block-->