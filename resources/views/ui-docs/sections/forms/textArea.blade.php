<div id="form_text_area" class="section-block">
  <h3 class="block-title">Form TextArea</h3>
  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formTextArea(&quot;name&quot;, &quot;value&quot;, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">{{ Form::formTextArea("name", "value", "Label", []) }}</div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <table class="table table-bordered">
    <tr>
      <th>Variables</th>
      <th width="40%">Values</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>name</td>
      <td>"Name"</td>
      <td>Name of your form input</td>
    </tr>
    <tr>
      <td>value</td>
      <td>"Value"</td>
      <td>Value can be empty.</td>
    </tr>
    <tr>
      <td>label</td>
      <td>"Label"</td>
      <td>Label of your form input</td>
    </tr>
    <tr>
      <td>attributes</td>
      <td>
        "placeholder"=>"sample"
        <br />"info"=>"<code>&lt;p&gt;note here&lt;/p&gt;</code>"
        <br />"required"=>"true/false"
        <br />"disabled"=>"true/false"
        <br />...
      </td>
      <td>All attributes, if no attributes just leave it empty array []</td>
    </tr>
  </table>
</div><!--//section-block-->