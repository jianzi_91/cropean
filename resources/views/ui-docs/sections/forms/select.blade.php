<div id="form_select" class="section-block">
<h3 class="block-title">Form Select</h3>
  <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formSelect(&quot;name&quot;, [&quot;option_1&quot;=>&quot;Option 1&quot;], selected_value, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
                            
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">{{ Form::formSelect('name', ["1"=>"Option One", "2"=>"Option Two"], "", "Label", []) }}</div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <table class="table table-bordered">
    <tr>
      <th>Variables</th>
      <th width="40%">Values</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>name</td>
      <td>"Name"</td>
      <td>Name of your form input</td>
    </tr>
    <tr>
      <td>arrays</td>
      <td>"["option_1"=>"option one"]"</td>
      <td>List of options in select tag.</td>
    </tr>
    <tr>
      <td>selected_value</td>
      <td>"option_1"</td>
      <td>Populate data for selected item</td>
    </tr>
    <tr>
      <td>label</td>
      <td>"Label"</td>
      <td>Label of your form input</td>
    </tr>
    <tr>
      <td>attributes</td>
      <td>
        "required"=>"true/false"
        <br />"info"=>"<code>&lt;p&gt;note here&lt;/p&gt;</code>"
        <br />"disabled"=>"true/false"
        <br />...
      </td>
      <td>All attributes, if no attributes, just leave it empty array []</td>
    </tr>
  </table>
</div><!--//section-block-->