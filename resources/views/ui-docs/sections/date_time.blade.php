<section id="date_time-section" class="doc-section">
    <h2 class="section-title">Date and Time</h2>
    <br />
    <!-- Sample -->
  <h6>Code Example</h6>
  <pre>
<code class="language-php">
&lbrace;&lbrace; Form::formDate(&quot;name&quot;, &quot;value&quot;, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
&lbrace;&lbrace; Form::formDateRange(&quot;name&quot;, &quot;value&quot;, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
&lbrace;&lbrace; Form::formTime(&quot;name&quot;, &quot;value&quot;, &quot;Label&quot;, [&quot;attributes&quot;]) &rbrace;&rbrace; 
</code>
  </pre>

  <!-- Demo -->
  <br />
  <h6>Demo</h6>
  <div class="card">
    <div class="card-body">
      {{ Form::formDate('date_single_name', "2019-12-25", "Date", []) }}
      {{ Form::formDateRange('date_range_name', "2019-12-25 to 2019-12-30", "Date Range", []) }}
      {{ Form::formTime('time_name', "14:00", "Time", []) }}
    </div>
  </div>

  <!-- Options -->
  <br />
  <h6>Options</h6>
  <table class="table table-bordered">
    <tr>
      <th>Variables</th>
      <th width="40%">Values</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>name</td>
      <td>"Name"</td>
      <td>Name of your form input</td>
    </tr>
    <tr>
      <td>value</td>
      <td>"Value"</td>
      <td>Value should be a valid date.</td>
    </tr>
    <tr>
      <td>label</td>
      <td>"Label"</td>
      <td>Label of your form input</td>
    </tr>
    <tr>
      <td>attributes</td>
      <td>"required"=>"true/false"
        <br />"disabled"=>"true/false"
        <br />...
      </td>
      <td>All attributes, if no attributes just leave it empty array []</td>
    </tr>
  </table>
  </section><!--//doc-section-->