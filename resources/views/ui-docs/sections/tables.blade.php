<section id="tables-section" class="doc-section">
    <h2 class="section-title">Tables</h2>
    <br />
    <!-- Sample -->
    <h6>Code Example</h6>
  <pre>
<code class="language-markup">
&lt;div class=&quot;table-responsive&quot;&gt; 
Basic  Table
&lt;table class=&quot;table&quot;&gt; 
    &lt;tr&gt;
        &lt;td&gt;...&lt;/td&gt; 
    &lt;/tr&gt; 
&lt;/table&gt;

Striped  Table
&lt;table class=&quot;table table-striped&quot;&gt; 
    &lt;tr&gt;
        &lt;td&gt;...&lt;/td&gt; 
    &lt;/tr&gt; 
&lt;/table&gt;

Bordered  Table
&lt;table class=&quot;table table-bordered&quot;&gt; 
    &lt;tr&gt;
        &lt;td&gt;...&lt;/td&gt; 
    &lt;/tr&gt; 
&lt;/table&gt;
&lt;/div&gt;
</code>
  </pre>
      <div class="section-block">
          <h6>Basic Table</h6>
          <div class="table-responsive">
              <table class="table">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Username</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                      </tr>
                      <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                      </tr>
                      <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                      </tr>
                  </tbody>
              </table>
          </div><!--//table-responsive-->
          <h6>Striped Table</h6>
          <div class="table-responsive">
              <table class="table table-striped">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Username</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                      </tr>
                      <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                      </tr>
                      <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                      </tr>
                  </tbody>
              </table>
          </div><!--//table-responsive-->
          <h6>Bordered Table</h6>
          <div class="table-responsive">
              <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Username</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>Otto</td>
                          <td>@mdo</td>
                      </tr>
                      <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>Thornton</td>
                          <td>@fat</td>
                      </tr>
                      <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                      </tr>
                  </tbody>
              </table>
          </div><!--//table-responsive-->
      </div><!--//section-block-->
  </section><!--//doc-section-->