<!-- ******Header****** -->
<header id="header" class="header">
  <div class="container">
      <div class="branding">
          <h1 class="logo">
              <a href="index.html">
                  <span aria-hidden="true" class="icon_documents_alt icon"></span>
                  <span class="text-highlight">Developer UI </span><span class="text-bold">Docs</span>
              </a>
          </h1>
      </div><!--//branding-->
    </div><!--//container-->
</header><!--//header-->