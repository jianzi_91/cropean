<div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
    <div id="doc-nav" class="doc-nav">
      <nav id="doc-menu" class="nav doc-menu flex-column sticky">
          <a class="nav-link scrollto" href="#installation-section">Installation</a>
            <nav class="doc-sub-menu nav flex-column">
                <a class="nav-link scrollto" href="#packages">Packages</a>
                <a class="nav-link scrollto" href="#development">Development</a>
                <a class="nav-link scrollto" href="#production">Production</a>
            </nav><!--//nav-->
          <a class="nav-link scrollto" href="#form-section">Forms</a>
            <nav class="doc-sub-menu nav flex-column">
                <a class="nav-link scrollto" href="#form_text">Text</a>
                <a class="nav-link scrollto" href="#form_number">Number</a>
                <a class="nav-link scrollto" href="#form_password">Password</a>
                <a class="nav-link scrollto" href="#form_select">Select</a>
                <a class="nav-link scrollto" href="#form_checkbox">Checkbox</a>
                <a class="nav-link scrollto" href="#form_radio">Radio</a>
                <a class="nav-link scrollto" href="#form_text_area">TextArea</a>
                <a class="nav-link scrollto" href="#form_upload">Upload</a>
            </nav><!--//nav-->
            <a class="nav-link scrollto" href="#date_time-section">Date/Time</a>
            <a class="nav-link scrollto" href="#notifications-section">Notifications</a>
            <a class="nav-link scrollto" href="#confirmation-section">Confirmation</a>
            <a class="nav-link scrollto" href="#tables-section">Tables</a>
            <a class="nav-link scrollto" href="#clipboard-section">Clipboard</a>
            <a class="nav-link scrollto" href="#validations-section">Form Validation</a>
            <a class="nav-link scrollto" href="#extras-section">Extras</a>
            <nav class="doc-sub-menu nav flex-column">
                <a class="nav-link scrollto" href="#extras_editable">X-Editable (Bootstrap)</a>
                <a class="nav-link scrollto" href="#extras_select2">Select2 (Bootstrap)</a>
            </nav><!--//nav-->
      </nav><!--//doc-menu-->
    </div>
</div><!--//doc-sidebar-->