<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'        => __('s_validation.the :attribute must be accepted'),
    'active_url'      => __('s_validation.the :attribute is not a valid URL'),
    'after'           => __('s_validation.the :attribute must be a date after :date'),
    'after_or_equal'  => __('s_validation.the :attribute must be a date after or equal to :date'),
    'alpha'           => __('s_validation.the :attribute may only contain letters'),
    'alpha_dash'      => __('s_validation.the :attribute may only contain letters, numbers, and dashes'),
    'alpha_num'       => __('s_validation.the :attribute may only contain letters and numbers'),
    'array'           => __('s_validation.the :attribute must be an array'),
    'before'          => __('s_validation.the :attribute must be a date before :date'),
    'before_or_equal' => __('s_validation.the :attribute must be a date before or equal to :date'),
    'between'         => [
        'numeric' => __('s_validation.the :attribute must be between :min and :max'),
        'file'    => __('s_validation.the :attribute must be between :min and :max kilobytes'),
        'string'  => __('s_validation.the :attribute must be between :min and :max characters'),
        'array'   => __('s_validation.the :attribute must have between :min and :max items'),
    ],
    'boolean'        => __('s_validation.the :attribute field must be true or false'),
    'confirmed'      => __('s_validation.the :attribute confirmation does not match'),
    'date'           => __('s_validation.the :attribute is not a valid date'),
    'date_equals'    => __('s_validation.the :attribute must be a date equal to :date'),
    'date_format'    => __('s_validation.the :attribute does not match the format :format'),
    'different'      => __('s_validation.the :attribute and :other must be different'),
    'digits'         => __('s_validation.the :attribute must be :digits digits'),
    'digits_between' => __('s_validation.the :attribute must be between :min and :max digits'),
    'dimensions'     => __('s_validation.the :attribute has invalid image dimensions'),
    'distinct'       => __('s_validation.the :attribute field has a duplicate value'),
    'email'          => __('s_validation.the :attribute must be a valid email address'),
    'exists'         => __('s_validation.the selected :attribute is invalid'),
    'file'           => __('s_validation.the :attribute must be a file'),
    'filled'         => __('s_validation.the :attribute field must have a value'),
    'gt'             => [
        'numeric' => __('s_validation.the :attribute must be greater than :value'),
        'file'    => __('s_validation.the :attribute must be greater than :value kilobytes'),
        'string'  => __('s_validation.the :attribute must be greater than :value characters'),
        'array'   => __('s_validation.the :attribute must have more than :value items'),
    ],
    'gte' => [
        'numeric' => __('s_validation.the :attribute must be greater than or equal :value'),
        'file'    => __('s_validation.the :attribute must be greater than or equal :value kilobytes'),
        'string'  => __('s_validation.the :attribute must be greater than or equal :value characters'),
        'array'   => __('s_validation.the :attribute must have :value items or more'),
    ],
    'image'    => __('s_validation.the :attribute must be an image'),
    'in'       => __('s_validation.the selected :attribute is invalid'),
    'in_array' => __('s_validation.the :attribute field does not exist in :other'),
    'integer'  => __('s_validation.the :attribute must be an integer'),
    'ip'       => __('s_validation.the :attribute must be a valid IP address'),
    'ipv4'     => __('s_validation.the :attribute must be a valid IPv4 address'),
    'ipv6'     => __('s_validation.the :attribute must be a valid IPv6 address'),
    'json'     => __('s_validation.the :attribute must be a valid JSON string'),
    'lt'       => [
        'numeric' => __('s_validation.the :attribute must be less than :value'),
        'file'    => __('s_validation.the :attribute must be less than :value kilobytes'),
        'string'  => __('s_validation.the :attribute must be less than :value characters'),
        'array'   => __('s_validation.the :attribute must have less than :value items'),
    ],
    'lte' => [
        'numeric' => __('s_validation.the :attribute must be less than or equal :value'),
        'file'    => __('s_validation.the :attribute must be less than or equal :value kilobytes'),
        'string'  => __('s_validation.the :attribute must be less than or equal :value characters'),
        'array'   => __('s_validation.the :attribute must not have more than :value items'),
    ],
    'max' => [
        'numeric' => __('s_validation.the :attribute may not be greater than :max'),
        'file'    => __('s_validation.the :attribute may not be greater than :max kilobytes'),
        'string'  => __('s_validation.the :attribute may not be greater than :max characters'),
        'array'   => __('s_validation.the :attribute may not have more than :max items'),
    ],
    'mimes'     => __('s_validation.the :attribute must be a file of type: :values'),
    'mimetypes' => __('s_validation.the :attribute must be a file of type: :values'),
    'min'       => [
        'numeric' => __('s_validation.the :attribute must be at least :min'),
        'file'    => __('s_validation.the :attribute must be at least :min kilobytes'),
        'string'  => __('s_validation.the :attribute must be at least :min characters'),
        'array'   => __('s_validation.the :attribute must have at least :min items'),
    ],
    'not_in'               => __('s_validation.the selected :attribute is invalid'),
    'not_regex'            => __('s_validation.the :attribute format is invalid'),
    'numeric'              => __('s_validation.the :attribute must be a number'),
    'present'              => __('s_validation.the :attribute field must be present'),
    'regex'                => __('s_validation.the :attribute format is invalid'),
    'required'             => __('s_validation.the :attribute field is required'),
    'required_if'          => __('s_validation.the :attribute field is required when :other is :value'),
    'required_unless'      => __('s_validation.the :attribute field is required unless :other is in :values'),
    'required_with'        => __('s_validation.the :attribute field is required when :values is present'),
    'required_with_all'    => __('s_validation.the :attribute field is required when :values is present'),
    'required_without'     => __('s_validation.the :attribute field is required when :values is not present'),
    'required_without_all' => __('s_validation.the :attribute field is required when none of :values are present'),
    'same'                 => __('s_validation.the :attribute and :other must match'),
    'size'                 => [
        'numeric' => __('s_validation.the :attribute must be :size'),
        'file'    => __('s_validation.the :attribute must be :size kilobytes'),
        'string'  => __('s_validation.the :attribute must be :size characters'),
        'array'   => __('s_validation.the :attribute must contain :size items'),
    ],
    'starts_with' => __('s_validation.the :attribute must start with one of the following: :values'),
    'string'      => __('s_validation.the :attribute must be a string'),
    'timezone'    => __('s_validation.the :attribute must be a valid zone'),
    'unique'      => __('s_validation.the :attribute has already been taken'),
    'uploaded'    => __('s_validation.the :attribute failed to upload'),
    'url'         => __('s_validation.the :attribute format is invalid'),
    'uuid'        => __('s_validation.the :attribute must be a valid UUID'),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
