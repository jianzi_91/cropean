/**
 ** ! All packages and customise modules should be include in ./modules folder
 */
import {
    Datetime,
    Notification,
    FormApi,
    Validate,
    SelectCheckTable
    //PreventSubmitSpam
} from "./modules";

/**
 ** ! Initialise modules globally
 */
window.CreateDatetime = (opts = {}) => new Datetime(opts);
window.CreateNoty = (opts = {}) => new Notification().createNoty(opts);
window.CreateValidation = (formElement, opts = {}) =>
    new Validate(formElement, opts);
window.AutoPopulateError = (opts = {}) => new FormApi().populateError(opts);
window.LoadCheckboxTable = (opts = {}) =>
    new SelectCheckTable().createCheckbox(opts);
// window.PreventSubmitSpam = new PreventSubmitSpam();

/**
 ** ! Global Initialisation:
 ** ? jquery
 ** ? bootstrap
 ** ? lodash
 ** ? moment
 ** ? swal
 ** ? clipboard
 ** ? axios
 ** init.js initialise helpful function when page load.
 */
import "./bootstrap";
import "./modules/init";
