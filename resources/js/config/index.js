/**
 ** ! Global date format
 */
export const DateTime = {
  format:"Y-m-d"
}

/**
 ** ! Api error class
 */
export const form = {
  errorClass:"invalid-feedback"
}

/**
 ** ! Prevent double click issue
 */
export const preventSpam = {
  class : ".prevent-spamming",
  interval: 1500
}