import flatpickr from "flatpickr";
import "flatpickr/dist/l10n/index.js";
import { DateTime } from "../config";

class Datetime {
    constructor(opts) {
        opts = typeof opts !== "object" ? {} : opts;
        opts.element = opts.element || console.error("Element is required!");
        opts.dateFormat = opts.dateFormat || DateTime.format;

        this.CreateDate(opts);
    }

    CreateDate(opts) {
        flatpickr(opts.element, opts);
    }
}

export { Datetime };
