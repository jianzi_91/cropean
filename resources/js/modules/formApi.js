import { form } from '../config';

class FormApi {
  async populateError(opts) {
    opts = typeof opts !== "object" ? {} : opts;
    opts.errors = opts.errors || console.error('Error Field is missing!');
    const error_class = form.errorClass;
    const errors = opts.errors;

    const list = document.querySelectorAll(".is-invalid");

    if(list.length>0){
      list.forEach(el => {
        el.classList.remove("is-invalid");
      })
    }
    const feedbacks = document.querySelectorAll('.'+error_class);
    if(feedbacks.length>0){
      feedbacks.forEach(element => {
        element.parentNode.removeChild(element);
      });
    }

    let errorMsg = '';
    if(opts.message){
      errorMsg = '<b>'+opts.message+'</b>';
    }
    for (const key in errors) {
      const newKey= this.formatKey(key);
        var fieldID = document.getElementById(newKey) || document.getElementById(key);
        if(errors.hasOwnProperty(key)){
          if(fieldID!= null){
            fieldID.classList.add("is-invalid");
            fieldID.insertAdjacentHTML('afterend', '<div class="'+error_class+'" role="alert">'+errors[key]+'</div>')
          }else{
            if(errorMsg!=''){
                errorMsg += '<br/>';
            }
            errorMsg += errors[key];
          }
        } 
    }
    if(errorMsg!=''){
      CreateNoty({type:'error', text: errorMsg, timeout:10000});
    }
  }

  /**
   * For ajax type page return message.
   */
  
  initScreenError(errors) {
    errors = typeof errors !== "object" ? {} : errors;
    let errorMsg = '';
    if(errors.message){
        errorMsg = '<b>'+errors.message+'</b>';
    }
    for (var key in errors) {
        if(document.getElementById(key) == null && document.getElementById(this.formatKey(key))== null){
            if(errorMsg!=''){
                errorMsg += '<br/>';
            }
            errorMsg += errors[key];
        }
    }
    if(errorMsg!=''){
        CreateNoty({type:'error', text: errorMsg});
    }
  }

  /**
   * To find the direct post input key with array
   */

  formatKey= (key) =>{
    var arr = key.split(".");
    var newKey =arr[0];
    for(let i = 1; i< arr.length ; i++){
      newKey += '['+arr[i]+']';
    }
    return newKey;
  }
}

export { FormApi };