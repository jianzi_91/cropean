import { preventSpam } from "../config";

class PreventSubmitSpam {
    constructor() {
        this.btnElems = document.querySelectorAll(preventSpam.class);
        this.enableBtnInterval = preventSpam.interval;
        this.options = {leading: true, trailing: false};
        this.preventSpamming();
    }

    submitForm = (btn) => {
        btn.addEventListener('click', _.debounce(()=> this.closest(btn,'form').submit(), this.enableBtnInterval, this.options));
    }

    preventSpamming = () => {
        for(var i = 0; i< this.btnElems.length; i++){
            this.submitForm(this.btnElems[i])
        }
    }

    disableInput = (elemId) => {
        document.getElementById(elemId).disabled = true;
    }

    enableInput = (elemId) => {
        document.getElementById(elemId).disabled = false;
    }
    closest = (el, selector) => {
        var matchesFn;
    
        // find vendor prefix
        ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
            if (typeof document.body[fn] == 'function') {
                matchesFn = fn;
                return true;
            }
            return false;
        })
    
        var parent;
    
        // traverse parents
        while (el) {
            parent = el.parentElement;
            if (parent && parent[matchesFn](selector)) {
                return parent;
            }
            el = parent;
        }
    
        return null;
    }

}

export { PreventSubmitSpam };