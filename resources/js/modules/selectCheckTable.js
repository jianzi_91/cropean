class SelectCheckTable {
    createCheckbox(opts) {
        opts = typeof opts !== "object" ? {} : opts;
        // opts.element = opts.element || console.error('Element is required!');
        this.rows_selected = [];
        let checkboxs = document.querySelectorAll('tbody input[type="checkbox"]');
        checkboxs.forEach((checkbox) => {
            checkbox.addEventListener('click', this.onClickCheckbox);
        })
        let headerCheckboxs = document.querySelectorAll('thead input[type="checkbox"]');
        headerCheckboxs.forEach((checkbox) => {
            checkbox.addEventListener('click', this.onClickHeaderCheckbox);
        })
        return this.rows_selected;
    }

    onClickCheckbox = (e) => {
        const target = e.target;
        let row = this.closest(target, 'tr');
        let rowId = target.getAttribute('data-id');
        let index = this.rows_selected.indexOf(rowId);
        if (target.checked && index === -1) {
            this.rows_selected.push(rowId);
        } else if (!target.checked && index !== -1) {
            this.rows_selected.splice(index, 1);
        }
        if (target.checked) {
            row.classList.add('selected');
        } else {
            row.classList.remove('selected');
        }
        // Prevent click event from propagating to parent
        e.stopPropagation();
    }

    onClickHeaderCheckbox = (e) => {
        const target = e.target;
        if (target.checked) {
            let checkboxs = document.querySelectorAll('tbody input[type="checkbox"]:not(:checked)');
            checkboxs.forEach((checkbox) => {
                checkbox.click();
            })
            // $('.table tbody td input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            // $('.table tbody td input[type="checkbox"]:checked').trigger('click');
            let checkboxs = document.querySelectorAll('tbody input[type="checkbox"]:checked');
            checkboxs.forEach((checkbox) => {
                checkbox.click();
            })
        }
        // Prevent click event from propagating to parent
        e.stopPropagation();
    }

    closest = (el, selector) => {
        var matchesFn;
        // find vendor prefix
        ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
            if (typeof document.body[fn] == 'function') {
                matchesFn = fn;
                return true;
            }
            return false;
        })
        var parent;
        // traverse parents
        while (el) {
            parent = el.parentElement;
            if (parent && parent[matchesFn](selector)) {
                return parent;
            }
            el = parent;
        }
        return null;
    }
}

export { SelectCheckTable };
