import validate from 'validate.js';
import moment from 'moment';

class Validate {
  constructor(formElement, constraints, success, errors) {
    this.element = document.querySelector(formElement);
    this.getAllinput = "input[name], select[name], textarea[name], radio[name], checkbox[name]";
    this.success = success;
    this.errors = errors;


    this.customValidationInit();
    this.validateFormSubmit(constraints);
    this.validateSingle(constraints);
  }

  customValidationInit () {
    validate.extend(validate.validators.datetime, {
      parse: function(value, options) {
        return moment.utc(value);
      },
      format: function(value, options) {
        let format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
        return moment.utc(value).format(format);
      }
    });
  }

  validateFormSubmit(constraints) {
    this.element.addEventListener("submit", ev => {
      ev.preventDefault();
      this.handleFormSubmit(this.element, constraints);
    });
  }

  handleFormSubmit(form, constraints) {
    let errors = validate(form, constraints, { fullMessages: false });  // validate them against the constraints
    this.showErrors(form, errors || {}); // then we update the form to reflect the results
    if (!errors) {
      this.submitSuccess(this.element); // all constraints passed!
    }
  }

  showErrors(form, errors) {
    _.each(form.querySelectorAll(this.getAllinput), input => {
      this.showErrorsForInput(input, errors && errors[input.name]); // loop through all the inputs and show the errors for that input
    });
  }

  showErrorsForInput(input, errors) {
    let formGroup = this.closestParent(input.parentNode, "form-group"); // the root of the input
    if(!formGroup)
      return;

    let invalidFeedback = formGroup.querySelector(".invalid-feedback"); // Find where the error messages will be insert into

    this.resetFormGroup(input, formGroup); // reseting the form

    if (errors) { // If we have errors
      input.classList.add("is-invalid"); // we first mark the group has having errors
      _.each(errors, error => { // then we append all the errors
        this.addError(invalidFeedback, error);
      });
    } else {
      input.classList.add("is-valid"); // otherwise we simply mark it as success
    }
  }

  closestParent(child, className) {
    if (!child || child == document) {
      return null;
    }
    if (child.classList.contains(className)) {
      return child;
    } else {
      return this.closestParent(child.parentNode, className);
    }
  }

  resetFormGroup(input, formGroup) {
    // Remove the success and error classes
    input.classList.remove("is-invalid");
    input.classList.remove("is-valid");

    // and remove any old error
    _.each(formGroup.querySelectorAll(".fe-error"), function(el) {
      el.parentNode.removeChild(el);
    });

    // and remove any old error
    _.each(formGroup.querySelectorAll(".is-invalid"), function(el) {
      el.removeAttribute("style");
    });

  }

  addError(invalid, error) {
    let block = document.createElement("span");
    block.classList.add("fe-error");
    block.innerHTML = error;
    invalid.style.display = "block";
    invalid.appendChild(block);
  }

  submitSuccess(formSelector) {
    formSelector.submit(); // submit the form
  }

  validateSingle(constraints) {
    const inputs = this.element.querySelectorAll(this.getAllinput),
          context = this;
    for (let i = 0; i < inputs.length; ++i) {
      inputs.item(i).addEventListener("blur", function(ev) {
        let errors = validate(context.element, constraints, { fullMessages: false }) || {};
        context.showErrorsForInput(this, errors[this.name])
      });
    }
  }

  //Get all values from  the form
  collectFormValues(formSelector, options){
    return validate.collectFormValues(formSelector,options);
  }

  //Get a json list of invalid fields name and error messages
  getInvalidFields(formValue, constraints){
    return validate(formValue, constraints ,{fullMessages: false}); //Remove the prepended input name
  }

}

export { Validate }
