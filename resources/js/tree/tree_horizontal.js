import * as d3 from 'd3';

function tree_horizontal(treeData, opts) {

    var margin = { top: 20, right: 120, bottom: 20, left: 120 },
        node_attribute = { width: 100, height: 50 },
        translation = opts.translationKeys,
        duration = 500,
        i = 0,
        root

    // size of the diagram
    var viewerWidth = document.getElementById('tree-horizontal').offsetWidth,
        viewerHeight = window.innerHeight - $("#tree-horizontal").offset().top,
        rootStartPosX = viewerWidth * 0.1,
        rootStartPosY = viewerHeight * 0.5;

    var treemap = d3.tree()
        .nodeSize([node_attribute.height + 10, node_attribute.height])

    var div = d3.select("#tree-horizontal")
        .append("div")
        .attr("class", "tooltip")
        .style("padding", "10px")
        .style("color", "#ffffff")
        .style("background", '#000000')
        .style("opacity", 0);

    var viewportHeight = Math.max(500, viewerHeight + margin.top + margin.bottom);
    $("#tree-horizontal").css("height", viewportHeight)

    var svg = d3.select("#tree-horizontal")
        .append("svg")
        .attr("width", '100%')
        .attr("height", viewportHeight);
    
    // resize svg
    $(window).resize(function() {
        svg.attr("width", $("#tree-horizontal").width())
    });

    var zoomLayer = svg.append("g")
        .attr("transform", "translate(" + rootStartPosX + ", " + rootStartPosY + ")");

    root = d3.hierarchy(treeData);
    root.x = viewerHeight / 2;
    root.x0 = viewerHeight / 2;
    update(root);

    function update(source = null) {
        // Assigns the x and y position for the nodes
        var nodes_data = treemap(root);

        // Compute the new tree layout.
        var nodes = nodes_data.descendants(),
            links = nodes_data.descendants().slice(1)
    
        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * 180;
        })

        // ****************** Nodes section ***************************

        // Update the nodes...
        var node = zoomLayer.selectAll('g.node')
            .data(nodes, function (d) {
                return d.id || (d.id = ++i);
            })

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .on('click touch', click)
            .on("mouseenter", function (d) {
                show_tooltip(d);
            })
            .on("mousemove", move_tooltip)
            .on("mouseout", function () {
                // don't hide tooltip when hover on child elements
                var e = event.toElement || event.relatedTarget;
                if (e.parentNode == this || e == this) {
                    return;
                } else {
                    hide_tooltip()
                }
            });

        // Add design for the nodes
        nodeEnter
            .append("rect")
            .attr("width", node_attribute.width)
            .attr("height", node_attribute.height)
            .attr("x", -node_attribute.width / 2)
            .attr("y", -node_attribute.height / 2)
            .attr("stroke-width", 1)
            .attr("stroke", "#ffffff")
            .style("opacity", 1)
            

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("width", 100)
            .attr("height", 50)
            .text(function (d) {
                var text = d.data.name,
                    count = text.length,
                    limit = 9

                if (count > limit) text = text.slice(0, limit) + ".."
                
                return text;
            })
            .style('opacity', 1);
        
        
        // Add tooltip for the nodes
        // nodeEnter.append("foreignObject")
        //     .attr("width", 20)
        //     .attr("height", 20)
        //     .attr("x", function (d) { return (node_attribute.width / 2) - 20 })
        //     .attr("y", 0)
        //     .attr("opacity", 1)
        //     .attr("class", "tooltip-info")
            
        
        // UPDATE
        var nodeUpdate = nodeEnter.merge(node)

        // Transition to the proper position for the node
        nodeUpdate
            .transition()
            .duration(duration)
            .ease(d3.easeBackIn)
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")"
            })
            .attr('cursor', function (d) {
                return (_.isEmpty(d.children) && d.data.children_count > 0) ? "pointer" : "default";
            })

        // Update the node attributes and style
        nodeUpdate
            .select('rect')
            .style("fill", function (d) {
                return (_.isEmpty(d.children) && d.data.children_count > 0) ? "#160e27" : "#d5dbca";
            })
            .attr('cursor', function (d) {
                return (_.isEmpty(d.children) && d.data.children_count > 0) ? "pointer" : "default";
            })
        
        nodeUpdate
            .select('text')
            .style("fill", function (d) {
                return (_.isEmpty(d.children) && d.data.children_count > 0) ? "#ffffff" : "#000000";
            })
            
        // nodeUpdate
        //     .select('foreignObject')
        //     .html(function (d) {
        //         var extra_class = (_.isEmpty(d.children) && d.data.children_count > 0) ? "white" : "";
        //         var content = "<i class='fa fa-info-circle " + extra_class + "'></i>";
        //         return content;
        //     })


        // Remove any exiting nodes
        var nodeExit = node.exit()
            .transition()
            .duration(duration)
            .ease(d3.easeBackIn)
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")"
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit
            .select("rect, text")
            .style('opacity', 1e-6)

        // ****************** links section ***************************

        // Update the links...
        var link = zoomLayer.selectAll('path.tree-link')
            .data(links, function (d) {
                return d.id
            });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert('path', "g")
            .attr("class", "tree-link")
            .attr('d', function (d) {
                // var o = { x: source.x0, y: source.y0 }
                return elbow(d.parent, d.parent)
            })

        // UPDATE
        var linkUpdate = linkEnter.merge(link)

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(duration)
            .ease(d3.easeBackIn)
            .attr('d', function (d) { return elbow(d, d.parent) })

        // Remove any exiting links
        var linkExit = link.exit()
            .transition()
            .duration(duration)
            .ease(d3.easeBackIn)
            .attr('d', function(d) {
                var o = {x: source.x, y: source.y}
                return elbow(o, o)
            })
            .remove()

        // Store the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x
            d.y0 = d.y
        })

        // line pattern
        function elbow(d, i) {
            var path = "M" + d.y + "," + d.x +
                "H" + (((d.y - i.y) / 2) + i.y) +
                "V" + i.x + "H" + i.y;

            return path
        }

        var zoomed = function () {
            // zoomLayer.attr("transform", d3.event.transform)
            var data = d3.event.transform
            zoomLayer.attr("transform", "translate(" + ( data.x + (rootStartPosX * data.k) ) + "," + ( data.y + (rootStartPosY * data.k) ) + ")scale(" + data.k + ")");
        }

        svg.call(d3.zoom()
            .scaleExtent([1 / 2, 12])
            .on("zoom", zoomed))
        
        // Tooltip
        function populate_tooltip_content(data) {

            var content = "<div>" + translation.total + ": " + data.total_package + "<br/>" +
                translation.rank + ": " + data.my_rank + "<br/>" +
                translation.star_1 + ": " + data.total_1_star + "<br/>" +
                translation.star_2 + ": " + data.total_2_star + "<br/>" +
                translation.star_3 + ": " + data.total_3_star + "</div>" +
                translation.shareholder + ": " + data.total_shareholder + "</div>";
            
            div
                .html(content)
                .style("padding", "10px")
                .style("width", 'auto')
                .style("height", 'auto')
        
        }

        function show_tooltip(d) {
            const url = opts.treeType == 'sponsor' ? 'sponsor-tree' : 'placement-tree';
            // d3.json('/sponsor-tree/' + `${d.data.user_id}` + '/details')
            axios.get(`/${url}/${d.data.user_id}/details`)
                .then((data) => {
                    populate_tooltip_content(data.data.data);
                })
                .catch(err => {
                    console.log(err);
                });            
            
        }

        function move_tooltip() {
            var event = d3.event;

            var posX = event.pageX - $("#tree-horizontal").offset().left + 50,
                posY = event.pageY - $("#tree-horizontal").offset().top + 15;
            
            div
                .style('opacity', 1)  
                .style("left", posX + "px")
                .style("top", posY + "px");
        }

        function hide_tooltip() {
            div
                .style('opacity', 0)
                .html("")
                .style("padding", 0)
                .style("left", 0)
                .style("top", 0)
                .style("width", 0)
                .style("height", 0)
        }

        // Toggle children on click.
        async function click(d) {
            // if (div.style('opacity') == 0) hide_tooltip();
            
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                if (!d.children && !d._children && d.data.children_count > 0) {
                    
                    // await d3.json(`${opts.url}/${d.data.user_id}`)
                    await axios.get(`${opts.url}/${d.data.user_id}`)
                        .then(function (data) {
                            var child = []
                            d.children = []
                            d.data.children = []
                            var dataChild = data.data.data.children

                            dataChild.forEach(function (i) {
                                child = d3.hierarchy(i);
                                child.depth = d.depth + 1;
                                child.height = d.height - 1;
                                child.parent = d;
                                d.children.push(child);
                                d.data.children.push(child.data);
                                update(d);
                            })
                        })
                    
                } else {
                    d.children = d._children;
                    d._children = null;
                }
            }

            update(d);
        }
    }
}

export default tree_horizontal
