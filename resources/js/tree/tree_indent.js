import * as d3 from 'd3';

function tree_indent(treeData, opts) {
    
  var margin = {top: 30, right: 20, bottom: 30, left: 20},
    width = 1100,
    barHeight = 20,
    barWidth = (width - margin.left - margin.right) * 0.4;

  var i = 0,
    duration = 400,
    root;

  var diagonal = d3.linkHorizontal()
      .x(function(d) { return d.y; })
      .y(function(d) { return d.x; });
  
  var line = d3.line()
      .x(function(d) { return x(d.y); })
      .y(function(d) { return y(d.y); });

  var svg = d3.select("#tree-indent")
      .append("svg")
        .attr("width", width)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var root = d3.hierarchy(treeData);
    root.x0 = 0;
    root.y0 = 0;
    update(root);

  function update(source) {

    // Compute the flattened node list.
    var nodes = root.descendants();

    // Setting dynamic height
    var height = Math.max(50, nodes.length * barHeight + margin.top + margin.bottom);

    d3.select("#tree-indent svg").transition()
        .duration(duration)
        .attr("height", height)

    /* d3.select('#tree-indent svg').transition()
        .duration(duration)
        .style("height", height + "px") */

    // Compute the "layout". TODO https://github.com/d3/d3-hierarchy/issues/67
    var index = -1;
    root.eachBefore(function(n) {
      n.x = ++index * barHeight;
      n.y = n.depth * 20;
    });

    // Update the nodes…
    var node = svg.selectAll(".node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
        .style("opacity", 0);

    // Enter any new nodes at the parent's previous position.
    nodeEnter.append("rect")
        .attr("y", -barHeight / 2)
        .attr("height", barHeight)
        .attr("width", barWidth)
        .style("fill", color)
        .on("click", click);

    nodeEnter.append("text")
        .attr("dy", 3.5)
        .attr("dx", 5.5)
        .text(function(d) { 
          return `${d.depth + 1} - ${d.data.username} - ${d.data.name} - [${d.data.children_count === undefined ? 'master' : d.data.children_count}]`;
        });

    // Transition nodes to their new position.
    nodeEnter.transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
        .style("opacity", 1);

    node.transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
        .style("opacity", 1)
      .select("rect")
        .style("fill", color);

    // Transition exiting nodes to the parent's new position.
    node.exit().transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
        .style("opacity", 0)
        .remove();

    // Update the links…
    var link = svg.selectAll(".link")
      .data(root.links(), function(d) { return d.target.id; });

    // Enter any new links at the parent's previous position.
    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", function(d) {
          var o = {x: source.x0, y: source.y0};
          return diagonal({source: o, target: o});
        })
      .transition()
        .duration(duration)
        .attr("d", diagonal);

    // Transition links to their new position.
    link.transition()
        .duration(duration)
        .attr("d", diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
        .duration(duration)
        .attr("d", function(d) {
          var o = {x: source.x, y: source.y};
          return diagonal({source: o, target: o});
        })
        .remove();

    // Stash the old positions for transition.
    root.each(function(d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });
  }

  // Toggle children on click.
  async function click(d) {

    if (d.children) {
      // uncollapse with data
      d._children = d.children;
      d.children = null;
    } else {

      // collapase without empty children and get new data
      if(d.data.children_count > 0 && !d._children) {
        await d3.json(`${opts.url}/${d.data.user_id}`)
          .then(function (data) { 
              const { children } = data.data
              let child = [];
              d.children = []
              d.data.children = []

              children.forEach(function(item) {
                child = d3.hierarchy(item);
                child.depth = d.depth + 1;
                child.height = d.height - 1;
                child.parent = d;

                d.children.push(child);
                d.data.children.push(child.data);
              })
          })
        
      } else {
        // collapse with data
        d.children = d._children;
        d._children = null;
      }
    }
    update(d);
  } 

  function color(d) {
    return d._children ? "#ffffff" : d.children ? "#ffffff" : "#ECCBA7";
  }

  function responsivefy(svg) {
    // get container + svg aspect ratio
    var container = d3.select(svg.node().parentNode),
        width = parseInt(svg.style("width")),
        height = parseInt(svg.style("height")),
        aspect = width / height;
    
    // add viewBox and preserveAspectRatio properties,
    // and call resize so that svg resizes on inital page load
    svg.attr("viewBox", "0 0 " + width + " " + height)
        .attr("preserveAspectRatio", "xMinYMid")
        .call(resize);
  
    // to register multiple listeners for same event type,
    // you need to add namespace, i.e., 'click.foo'
    // necessary if you call invoke this function for multiple svgs
    // api docs: https://github.com/mbostock/d3/wiki/Selections#on
    d3.select(window).on("resize." + container.attr("id"), resize);
  
    // get width of container and resize svg to fit it
    function resize() {
        var targetWidth = parseInt(container.style("width"));
        svg.attr("width", targetWidth);
        svg.attr("height", Math.round(targetWidth / aspect));
    }
  }
}

export default tree_indent
