import tree_circle_packing from './tree_circle_packing';
import tree_indent from './tree_indent';
import tree_horizontal from './tree_horizontal';
import Tree from './tree';
  
window.tree_circle_packing = tree_circle_packing;
window.tree_indent = tree_indent;
window.tree_horizontal = tree_horizontal;
window.Tree = (id, treeData={}, opts={})=> new Tree(id, treeData, opts);
