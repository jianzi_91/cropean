const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
*/

mix
  /**
   * @tasking SCSS
   * convert sass to css native, save to public/css
   * base on the naming.
   **/
   .sass('resources/sass/default.scss', 'public/css')

   /**
   * @tasking Third Party CSS / JS
   * convert sass to css native, save to public/css
   * base on the naming.
   **/
  .copy('resources/js/third-party/', 'public/third-party/')

  /**
   * JS Entry Point
   */
  .js("resources/js/default.js", "public/js")
  .js("resources/js/tree/index.js", "public/js/tree.js")
  .version()
  /**
   * Overriding webpack configuration
   */
  .webpackConfig({
    module: {
      rules: [
        {
          test: /\.less$/,
          loader: "style-loader!css-loader!less-loader!sass-loader",
          exclude: [
            path.resolve(__dirname, "node-modules"),
            path.resolve(__dirname, "resources/less")
          ]
        }
      ]
    }
  })
  /**
   * @tasking Notification on desktop (On & Offf)
   *  Notification off
   */
  .disableNotifications();
