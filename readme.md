## Installation in local

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/6.x/installation)


Clone the repository

```
    git clone git@repo.plus65host.net:cropean/cropean-laravel-app.git
```
    
Switch to the repo folder

```
    cd cropean-laravel-app
```

Install all the dependencies using composer

```
    composer install
```

Copy the example env file and make the required configuration changes in the .env file

```
    cp .env.example .env
```

Set the database connection(main database and log) in .env
```
    DB_CONNECTION=
    DB_HOST=
    DB_PORT=
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=
```
```
    DB_LOG_CONNECTION=
    DB_LOG_HOST=
    DB_LOG_PORT=
    DB_LOG_DATABASE=
    DB_LOG_USERNAME=root
    DB_LOG_PASSWORD=
```

Set the application url in .env
```
    MEMBER_URL=
    ADMIN_URL=
    WEBSITE_URL=
```

Generate a new application key
```
    php artisan key:generate
```

Run config:cache
```
    php artisan config:cache
```

Run the database migrations and seeders, make sure database is created
```
    php artisan migrate --seed
```

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command
```
    php artisan migrate:refresh --seed
```

You can now access the server at the url that you set in .env

### Deployment

- Please make sure you have the access to the servers.
- Please make sure you Deployer installed in your local.

###Deployer Installation
```
    curl -LO https://deployer.org/deployer.phar
    mv deployer.phar /usr/local/bin/dep
    chmod +x /usr/local/bin/dep
```

###Deployment Command
```
    `dep deploy staging -vvv` for `staging`
    `dep deploy uat -vvv` for `uat`
    `dep deploy production -vvv` for `production`

```

